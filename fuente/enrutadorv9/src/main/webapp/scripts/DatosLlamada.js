
// **************************************************** //
// ***************** DATOS LLAMADA ******************** //
// **************************************************** //

function DatosLlamada() {	
	this.callId = cambiaID(session.avaya.ucid);
	this.numOrigen = session.connection.protocol.sip.from.user;
	this.numDestino = session.connection.protocol.sip.request.user;
	this.puerto = validaPuerto(session.avaya.telephone.callid);
	this.uui = "";
	this.uuiDefault = "";
	this.cti = "";
	this.uri = session.connection.protocol.sip.to.uri;
	this.sesAvaya = session.avaya.telephone.call_tag;
	
	//Se inicializan variables con las cabeceras leidas de Avaya
	
	//session.avaya.uui.shared.length arreglo con informacion del UUI
	var longitudArreglo = session.avaya.uui.shared.length;		
	for(var cont=0;cont<longitudArreglo;cont++){
		//Encabezado de UUI C8 es una llamada nueva o re entrante con informacion en el User to User Information 
		if( session.avaya.uui.shared[cont].id == "C8"){
			this.uui = session.avaya.uui.shared[cont].value;
		}
		//Encabezado de UUI FA es en donde se encuentra el UCID
		if( session.avaya.uui.shared[cont].id == "FA"  ){
			// ABALFARO_20170505 si viene relleno el dato, entonces lo guardamos
			// si no nos quedaremos con el callId inicial de avaya
			if(!session.avaya.uui.shared[cont].value){
				this.callId = session.avaya.uui.shared[cont].value;
			}
		}
	}
	
	//session.connection.protocol.sip.unknownhdr cabecera con el uui por defecto usado para transferencias con cti deshabilitado
//	longitudArreglo = session.connection.protocol.sip.unknownhdr.length;
//	for(var cont=0;cont<longitudArreglo;cont++){
//		if( session.connection.protocol.sip.unknownhdr[cont].name == "User-to-User" ){
//			this.uuiDefault = session.connection.protocol.sip.unknownhdr[cont].value;
//		}
//	}
	
}

function validaPuerto(numPuerto){
	var validaPuerto = numPuerto.toString();
	var puntero = '';
	var maquina = '';
	var pbx = '';
	var conexion = '';
	var puerto = '';
	var identificador='';

	puntero = validaPuerto.indexOf('-');
	maquina = validaPuerto.substring(0,puntero);
	validaPuerto= validaPuerto.substring(puntero + 1);
	
	puntero = validaPuerto.indexOf('-');
	pbx = validaPuerto.substring(0,puntero);
	validaPuerto= validaPuerto.substring(puntero + 1);

	puntero = validaPuerto.indexOf('-');
	conexion = validaPuerto.substring(0,puntero);
	validaPuerto= validaPuerto.substring(puntero + 1);

	puntero = validaPuerto.indexOf('-');
	puerto = validaPuerto.substring(0,puntero);

	identificador= validaPuerto.substring(puntero + 1);

	puerto = puerto.toString();

	if (puerto.length == 1){
		puerto = '0' + puerto;
	}

	return puerto;
}


function cambiaID(numeroID){
	var nuevoID  = ''; 
	var validaID = numeroID.toString();
	var fechaHoy = new Date();
	var cadenaFecha = fechaHoy.getTime() + "";
	nuevoID = cadenaFecha.substring( cadenaFecha.length - 4 ) + validaID.substring(4);

	if(nuevoID == ''){
		nuevoID = validaID;
	}

	return nuevoID
}