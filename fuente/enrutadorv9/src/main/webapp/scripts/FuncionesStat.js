// ******************************************************** //
// ****** FUNCIONES para ESTADISTICAS CUADRO MANDO ******** //
// ******************************************************** //


// **************************** //
// ***** INICIO LLAMADA ******* //
// **************************** //
function inicioLlamada(idLlamada, timeInicio, idServicio, numOrigen, numDestino) {
	// creo el objeto Stat que perdurara en la llamada
	var PRM_stat = new Stat();

	// guardo el callId
	PRM_stat.codLlamIVR = idLlamada;

	// guardo el codigo de servicio
	PRM_stat.codServIVR = idServicio;
	
	// guardo en milisegundos la fecha/hora de entrada
	PRM_stat.timeEntradaMilis = timeInicio;
	
	// guardo el callId nuevamente
	PRM_stat.codUcid = idLlamada;

	// guardo el numero destino
	PRM_stat.codVdn = numDestino;
	
	// (ES: "4")
	// PRM_stat.xtiTiplocut = "";

	// si es contingencia (ES: VG_esContingencia)
	// PRM_stat.xtiContitel = "";

	// guardo el numero de origen
	PRM_stat.desNumtfno = numOrigen;
	
	// (ES: VG_aplicacionCodigo)
	PRM_stat.codEntalfa = "msop";
	
	// aniado un contador del inicio de llamada
	PRM_stat = aniadeContador(PRM_stat, "CALL", "INIT");
	
	return PRM_stat;
}



//******************************* //
//***** GUARDA xtiContitel ****** //
//******************************* //
function guardaXtiContitel(PRM_stat, xtiContitel) {
	
	PRM_stat.xtiContitel = xtiContitel;
	
	return PRM_stat;
}

//******************************* //
//***** GUARDA xtiTiplocut ****** //
//******************************* //
function guardaXtiTiplocut(PRM_stat, xtiTiplocut) {
	
	PRM_stat.xtiTiplocut = xtiTiplocut;
	
	return PRM_stat;
}

//****************************** //
//***** GUARDA ID CLIENTE ****** //
//****************************** //
function guardaIdCliente(PRM_stat, idCliente) {
	
	PRM_stat.codPersctpn = idCliente;
	
	return PRM_stat;
}

//***************************************** //
//***** GUARDA TRANSFERENCIA LLAMADA ****** //
//***************************************** //
function guardaSegmentoTransferencia(PRM_stat, sgmtXfer) {
	
	PRM_stat.codSgmtxfer = sgmtXfer;
	
	return PRM_stat;
}

//************************************ //
//***** GUARDA SEGMENTO CLIENTE ****** //
//************************************ //
function guardaSegmentoCliente(PRM_stat, sgmtCliente) {
	
	PRM_stat.desSegmento = sgmtCliente;
	
	return PRM_stat;
}

//************************************ //
//***** GUARDA KEYFRONT CLIENTE ****** //
//************************************ //
function guardaKeyfrontCliente(PRM_stat, keyFrontCliente) {
	
	PRM_stat.codKeyfront = keyFrontCliente;
	
	return PRM_stat;
}


// **************************** //
// ******* FIN LLAMADA ******** //
// **************************** //
function finLlamada(PRM_stat, timeFin, codigoRetorno) {
	
	// guardo la hora de salida
	PRM_stat.timeSalidaMilis = timeFin;
	
	// aniado un contador del fin de llamada
	PRM_stat = aniadeContador(PRM_stat, "CALL", "END", codigoRetorno);
	
	
	return PRM_stat;
}

//**************************** //
//***** INICIO OPERACION ***** //
//**************************** //
function inicioOperacion(PRM_stat, timeInicio, idOperacion) {

	// creamos una nueva operacion en ultimo lugar
	var longitud = PRM_stat.operaciones.length;
	PRM_stat.operaciones[longitud] = new OperacionStat();
	
	// guardo el id de la operacion
	PRM_stat.operaciones[longitud].codOperIVR = idOperacion;

	// guardo en milisegundos la fecha/hora de entrada
	PRM_stat.operaciones[longitud].inicioOperacionMilis = timeInicio;

	return PRM_stat;
}

//**************************** //
//****** FIN OPERACION ******* //
//**************************** //
function finOperacion(PRM_stat, timeFin, idOperacion, resultadoOperacion, codRetorno) {
	
	// busco la operacion a finalizar desde el final
	for (var i = PRM_stat.operaciones.length -1; i >= 0 ; i--) { 
				
		if(PRM_stat.operaciones[i].codOperIVR == idOperacion){
			// es la operacion que buscamos finalizar
			
			// guardo el resultado de la operacion: OK --> 1 // KO --> 0
			if (resultadoOperacion == "OK"){
				PRM_stat.operaciones[i].flagRslIVR = "1";
			} else {
				PRM_stat.operaciones[i].flagRslIVR = "0";
			}	
					
			// guardo el resultado de la operacion
			PRM_stat.operaciones[i].codRslIVR = codRetorno;
			
			// guardo en milisegundos la fecha/hora de s
			PRM_stat.operaciones[i].finOperacionMilis = timeFin;
			
		}
	}
	
	return PRM_stat;
}

//**************************** //
//***** ANIADIR CONTADOR ***** //
//**************************** //
// Podremos llamar a este metodo con las siguientes listas de parametros:
// aniadeContador(PRM_stat, nombreContador, tipoContador)
// aniadeContador(PRM_stat, nombreContador, tipoContador, codRetorno)
// + los params que queramos
function aniadeContador(PRM_stat, nombreContador, tipoContador, codRetorno, param1, param2, param3, param4, param5) {
	
	// creamos un nuevo contador en ultimo lugar
	var longitud = PRM_stat.contadores.length;
	PRM_stat.contadores[longitud] = new ContadorStat();
	
	// asigno el nombre contador
	PRM_stat.contadores[longitud].nombre = nombreContador;
	
	// asigno el tipo de contador
	PRM_stat.contadores[longitud].tipo = tipoContador;
	
	if (typeof codRetorno != 'undefined'){
		// asigno el codigo retorno
		PRM_stat.contadores[longitud].codigoRetorno = codRetorno;
	} else {
		PRM_stat.contadores[longitud].codigoRetorno = "";
	}
	
	if (typeof param1 != 'undefined'){
		// asigno el parametro adicional 1
		PRM_stat.contadores[longitud].param1 = param1;
	} else {
		PRM_stat.contadores[longitud].param1 = "";
	}
	if (typeof param2 != 'undefined'){
		// asigno el parametro adicional 2
		PRM_stat.contadores[longitud].param2 = param2;
	} else {
		PRM_stat.contadores[longitud].param2 = "";
	}
	if (typeof param3 != 'undefined'){
		// asigno el parametro adicional 3
		PRM_stat.contadores[longitud].param3 = param3;
	} else {
		PRM_stat.contadores[longitud].param3 = "";
	}
	if (typeof param4 != 'undefined'){
		// asigno el parametro adicional 4
		PRM_stat.contadores[longitud].param4 = param4;
	} else {
		PRM_stat.contadores[longitud].param4 = "";
	}
	if (typeof param5 != 'undefined'){
		// asigno el parametro adicional 5
		PRM_stat.contadores[longitud].param5 = param5;
	} else {
		PRM_stat.contadores[longitud].param5 = "";
	}
		

	return PRM_stat;
}
