// **************************************************** //
// ******************* DATOS CTI ********************** //
// **************************************************** //

function DatosCTI() {
	this.ani = "";
	this.dnis = "";
	this.numeroIvr = "";
	this.puertoIvr = "";
	this.llave = "";
	this.field = ""; // temporal
	this.vdnOrigen = "";
	this.vdnDestino = "";
	this.skill = "";
	this.asa = "";
	this.idAplicacion = "";
	this.xferId = "";
	this.segmentoCliente = "";
	this.producto = "";
	this.llaveAccesoFront = "";
	this.campania = "";
	this.canal = "";
	this.numCliente = "";
	this.sector = "";
	this.nombreCliente = "";
	// [ABALFARO] aqui hay un parametro libre en el excel
	this.telefonoContacto = ""; // [ABALFARO] ??? no viene en excel documentacion
	this.telefonoDinamico = "";
	this.libre1 = ""; // comentario
	this.libre2 = ""; // metodoAutenticacion
	this.usuarioFront = "";
	this.transferenciaAsesor = "";
	this.transferenciaIvr = "";
	this.numeroSesion = "";
	this.tiempoLlamada = "";
	this.llaveSegmento = "";
	this.ipAsesor = "";
	this.libre3 = "";
	this.telCtePersonas = "";
	this.metodoAutenticacion = "";
	this.libre4 = "";
	this.timestamp = "";
	this.comentario = "";
}