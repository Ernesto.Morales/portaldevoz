
// **************************************************** //
// ****** DATOS para ESTADISTICAS CUADRO MANDO ******** //
// **************************************************** //

function Stat() {	
	
	// id de la llamada
	// BD = cd_llamivr
	this.codLlamIVR = "";

	// id del servicio
	// BD = cd_servivr
	this.codServIVR = "";
	
	// hora de entrada en el servicio
	// BD = fh_entrada
	this.timeEntradaMilis = "";

	// hora de salida en el servicio
	// BD = fh_salida
	this.timeSalidaMilis = "";
	
	// id de la llamada nuevamente
	// BD = cd_ucid
	this.codUcid = "";

	// numero destino
	// BD = cd_vdn
	this.codVdn = "";
	
	// si es contingencia
	// BD = cd_contitel
	this.xtiContitel = "";
	
	// (ES: "4")
	// BD = cd_tiplocut
	this.xtiTiplocut = "";
	
	// codigo del cliente
	// BD = cd_persctpn
	this.codPersctpn = "";
	
	// transferencia de la llamada
	// BD = cd_sgmtxfer
	this.codSgmtxfer = "";	
	
	// segmento del cliente
	// BD = nb_segmento
	this.desSegmento = "";		
	
	// elemento de identificacion del cliente
	// BD = nb_keyfront
	this.codKeyfront = "";	
	
	// numero de origen
	// BD = nb_numtfno
	this.desNumtfno = "";	
	
	// (ES: VG_aplicacionCodigo)
	// BD = cd_entalfa
	this.codEntalfa = "";
	
	// operaciones[i] = new OperacionStat();
	this.operaciones = new Array();

	
	this.fecAccioniv = "";
	// contadores[i] = new ContadorStat();
	this.contadores = new Array();
	
	
}

function OperacionStat() {	
	this.codOperIVR = "";
	this.codRslIVR = "";	// Posibles valores: 001, 002, 003, 004, 005
	this.flagRslIVR = "";	// Posibles valores: 1,0
	this.inicioOperacionMilis = "";
	this.finOperacionMilis = "";
}

function ContadorStat() {	
	this.nombre = "";
	this.tipo = "";
	this.codigoRetorno = "";
	this.param1 = "";
	this.param2 = "";
	this.param3 = "";
	this.param4 = "";
	this.param5 = "";
}

