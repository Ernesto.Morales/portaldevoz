
// ************************************************************************ //
// ***************** MeTODOS PARA CONSTRUIR UN SERVCIO ******************** //
// ************************************************************************ //

// ************** SERVICIO
function Servicio() {	
	this.codServIVR = "";
	// configuracion
	this.servActivo = "";
	this.cti = "";
	this.controlHorario = "";
	this.modoInteraccion = "";
	this.controlTransferActivo = "";
	this.controlContActivo = "";
	this.lbabActivo = "";
	this.tipifActivo = "";
	this.statCMActivo = "";
	this.encuestaActivo = "";
	this.encuestaDefecto = "";
	this.pregAbiertaActivo = "";
	this.numIntPregAbierta = "";
	this.servNumReintAsr = "";
	this.servNumReintAsrToDtmf = "";
	this.servNumReintDtmf = "";
	this.vdnTransferDef = "";
	this.bargeinAsrActivo = "";
	this.bargeinDtmfActivo = "";
	this.propertyTimeoutAsr = "";
	this.propertyMaxspeechTimeoutAsr = "";
	this.propertyIncompleteTimeoutAsr = "";
	this.propertyCompleteTimeoutAsr = "";
	this.propertyConfidenceLevelAsr = "";
	this.propertySensitivityAsr = "";
	this.timeoutDtmf = "";
	this.interdigitTimeoutDtmf = "";
	
	// horarios[i] = new Horario();
	this.horarios = new Array();
	
	// menus[i] = new Menu();
	this.menus = new Array();
	
	// schemas[i] = new Schema();
	this.schemas = new Array();
	
	// saludos[i] = new Saludo();
	this.saludos = new Array();
	
	// promociones[i] = new Promo();
	this.promociones = new Array();
	
}

// ************************************************* //
// ***************** HORARIO *********************** //
// ************************************************* //
function Horario() {	
	this.tipoHorario = "";
	this.valorHorario = "";
	this.franja1 = "";
	this.franja2 = "";
	this.franja3 = "";
}

// ************************************************* //
// ****************** MENU ************************* //
// ************************************************* //
function Menu() {	
	this.codMenu = "";
	
	// opciones[i] = new OpcionMenu();
	this.opciones = new Array();
}

// OPCION DE UN MENU
function OpcionMenu() {	
	this.idOpcion = "";
	this.descOpcion = "";
	this.contActivo = "";
}

// ************************************************* //
// ***************** SCHEMA ************************ //
// ************************************************* //
function Schema() {	
	this.codSchema = "";
	this.flagEsqDirecto = "";
}

// ************************************************* //
// ***************** SALUDO ************************ //
// ************************************************* //
function Saludo() {	
	this.fechaIni = "";
	this.fechaFin = "";
	this.modoReproduccion = "";
	this.wavSaludo = "";
}

// ************************************************ //
// ***************** PROMOCION ******************** //
// ************************************************ //
// PROMOCION DE UN SERVICIO
function Promo() {	
	this.fechaIni = "";
	this.fechaFin = "";
	// productos[i] = new Producto();
	this.productos = new Array();
}
// PRODUCTO DE UNA PROMOCION
function Producto() {		
	this.codProducto = "";
	// servPromo[i] = new ServPromo();
	this.servPromo = new Array();
}
// SERVICIO DE UN PRODUCTO
function ServPromo() {			
	this.codServicio = "";
	// tarifas[i] = new Tarifa();
	this.tarifas = new Array();
}
// TARIFA DE UN SERVICIO-PROMO
function Tarifa() {			
	this.codTarifa = "";
	// segmentos[i] = new Segmento();
	this.segmentos = new Array();
}
// SEGMENTO DE UNA TARIFA
function Segmento() {	
	this.codSegmento = "";	
	this.descPromo = "";
	this.promoActivo = "";
	this.codAccion = "";
	this.modoReproduccion = "";
	this.wavPromo = "";
	this.codTransferLabel = "";
}