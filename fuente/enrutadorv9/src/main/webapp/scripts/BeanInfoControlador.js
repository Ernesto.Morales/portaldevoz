// ******************************************************** //
// ************ DATOS BEAN-INFO-CONTROLADOR *************** //
// ******************************************************** //

function BeanInfoControlador() {
	this.nombre = "";
	this.ruta = "";
	this.isActivo = "";

	// transferencias
	this.activacionSegmento = "";
	this.segmentoTransfer = "";
	this.segmentoTransferDefault = "";
	
	// excepciones transferencias	
	// transferExcepciones[i] = new TransferExcepcion();
	this.transferExcepciones = new Array();
}
function TransferExcepcion() {	
	this.nombre = "";
	this.segmentoTransfer = "";
}