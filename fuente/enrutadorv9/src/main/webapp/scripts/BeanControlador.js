
// ******************************************************** //
// ****************** DATOS CONTROLADOR ******************* //
// ******************************************************** //

function BeanControlador() {	
	this.codServIVR = "";
	this.nombre = "";
	
	// identificacion
	this.isReqIdentificacion = "";
	this.tipoIdentificacion = ""; // cadena de los candados requeridos separados por |
	this.intentosIdentificacion = "";
	// para prestamos
	this.accionMaxintMenuIdentificacion = "";
	this.accionMaxintValidIdentificacion = "";
	this.accionOkIdentificacion = "";
	
	// autenticacion
	this.isReqAutenticacion = ""; 
	this.tipoAutenticacion = ""; // cadena de los candados requeridos separados por |
	this.intentosAutenticacion = "";	
	// acciones para autenticacion PRESTAMOS
	this.accionMaxintMenuAutenticacion = "";  
	this.accionMaxintValidAutenticacion = "";
	this.accionValidKoAutenticacion = "";
	this.accionAccesoNoPermitidoAutenticacion = "";
	// acciones para autenticacion LB
	this.accionClienteSinElementosAuth = "";
	this.accionClienteBloqueadoCandadoAuth = "";
	this.accionMaxintMenuAuth = "";
	this.accionMaxintValidAuth = "";
	this.accionClienteBloqueadoValidAuth = "";

}
