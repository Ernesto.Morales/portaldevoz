

// **************************************************** //
// ****** DATOS para ESTADISTICAS CUADRO MANDO ******** //
// **************************************************** //

function TransactionCTI() {	

	this.tipoOperacion = "";
	this.estatus = "";
	this.codigoRetorno = "";
	this.idIntentos  = 0;
	this.tipocamp="";
	this.valorcamp="";
	this.montocamp="";
	this.mensaje="";
	this.operaciones = new Array();
}

function OperacionStat() {	
	this.valor = "";
}

//**************************** //
//***** INICIO OPERACION ***** //
//**************************** //
function setOperacion(PRM_stat, idOperacion) {

	// creamos una nueva operacion en ultimo lugar
	var longitud = PRM_stat.operaciones.length;
	PRM_stat.operaciones[longitud] = new OperacionStat();
	PRM_stat.operaciones[longitud].valor = idOperacion;
	return PRM_stat;
}
function actualizar(PRM_stat,status,codigoRetorno) {

	// creamos una nueva operacion en ultimo lugar
	PRM_stat.estatus = status;
	PRM_stat.codigoRetorno = codigoRetorno;
	return PRM_stat;
}
