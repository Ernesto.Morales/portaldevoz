
// ***************************************************** //
// ************ DATOS BEAN-INFO-CONTROLADOR ************ //
// ***************************************************** //

function BeanInfoModulo() {	
	// guardo todas las etiquetas PA por las que se ha pasado hasta el modulo DIRECTO
	this.historicoEtiqueta = "";
	
	this.nombre = "";
	this.isActivo = "";
	this.rama = "";
	this.tipoSalida = "";
	this.ruta = "";
	this.menu = "";
	
	// identificacion
	this.isReqIdentificacion = "";
	this.tipoIdentificacion = ""; // cadena de los candados requeridos separados por |
	this.intentosIdentificacion = "";
	this.accionMaxintMenuIdentificacion = "";
	this.accionMaxintValidIdentificacion = "";
	
	// autenticacion
	this.isReqAutenticacion = ""; 
	this.tipoAutenticacion = ""; // cadena de los candados requeridos separados por |
	this.intentosAutenticacion = "";	
	// acciones para autenticacion PRESTAMOS
	this.accionMaxintValidAutenticacion = "";
	this.accionValidKoAutenticacion = "";
	this.accionAccesoNoPermitidoAutenticacion = "";
	// ABALFARO_20170504 integracion con MAU
	this.accionMaxintMenuAutenticacion = "";  
	this.accionClienteSinElementosAutenticacion = "";
	this.accionClienteBloqueadoCandadoAutenticacion = "";
	this.accionClienteBloqueadoValidAutenticacion = "";
	
	// acciones para autenticacion LB
	this.accionClienteSinElementosAuth = "";
	this.accionClienteBloqueadoCandadoAuth = "";
	this.accionMaxintMenuAuth = "";
	this.accionMaxintValidAuth = "";
	this.accionClienteBloqueadoValidAuth = "";
	
	// transferencias
	this.activacionSegmento="";
	this.segmentoTransfer="";
	this.segmentoTransferDefault="";
}
