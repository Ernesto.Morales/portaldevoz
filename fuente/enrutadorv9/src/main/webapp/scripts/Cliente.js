// ***************************************************** //
// ***************** DATOS CLIENTE ********************* //
// ***************************************************** //

function Cliente() {	
	
	this.tsec = "";
	this.tsecPrivado = "";
	this.deleteTsec = "KO";
	
	// LOAN-DOCUMENT 		-- identificacion AUTO
	// PROXY-SERVICE-WYH5 	-- autenticacion HIPOTECA
	// PROXY-SERVICE-WYH5 	-- autenticacion TARJETA
	this.nombreCliente = ""; 		// auto e hipoteca (en identif. hipoteca Proxy400 viene un titular, lo guardo) y tarjeta
	this.apellido1 = "";			// auto
	this.apellido2 = "";			// auto
	this.calle = "";				// auto, hipoteca y tarjeta
	this.numeroCalle = "";			// auto, hipoteca y tarjeta
	this.colonia = "";				// auto, hipoteca y tarjeta
	this.codigoPostal = "";			// auto, hipoteca y tarjeta
	this.municipio = "";
	this.rfc = ""; 
	this.fechaNacimiento = ""; // formato=YYYYMMDD
	this.idSession="";
	// PROXY-SERVICE-WYH5 	 carta Deducibilidad
	this.homoclave = ""; // Homoclave para el modulo de Carta de Deducibilidad
					     // a concatenar con el rfc del mismo WebService
	this.rfcWYH5 = "";
	
	// PROXY-SERVICE-WYH5 -- autenticacion AUTO e identificacion HIPOTECA y TARJETA
	this.numCliente = ""; // auto, hipoteca y tarjeta
	
	// GET-ELECTRONIC-INFORMATION
	this.telefonoContacto = "";
	this.companiaTelefono = "";
	this.email = "";
	
	
	
	// 20170608 almacenamos el contrato con el que se identifica 
	this.contratoIdentificacion = "";
	// 20170608 almacenamos la tarjeta con la que se identifica 
	this.tarjetaIdentificacion = "";
//	this.folio = "";
	
	this.segmento = "";
	this.producto = "";
	this.sector = "";
	this.campania = "";
	
	this.comentario = "";
	this.popup = "";
	
	this.isAutenticado = ""; // true/false
	this.isIdentificado = ""; // true/false
	
	this.ramaIdentificacion = ""; // indica si nos hemos identificado con un credito de AUTO o una HIPOTECA
	
	// tiposAutenticacion[i] = new TipoAutenticacion();
	this.tiposAutenticacion = new Array();
	
	// tiposIdentificacion[i] = new TipoIdentificacion();
	this.tiposIdentificacion = new Array();
	
	
	// PRODUCTO: TARJETAS
	// tarjetas[i] = new Tarjeta();
    this.tarjetas = new Array();
    this.tarjetaSeleccionada = "";
    // tarjetasPendientesActivar[i] = new Tarjeta();
    this.tarjetasPendientesActivar = new Array();
    
}

function TipoAutenticacion() {	
	this.tipo = "";
	this.resultado = "";
}
function TipoIdentificacion() {	
	this.tipo = "";
	this.resultado = "";
}


// CUENTA
function Cuenta() {	
	this.numeroCuenta = "";
    this.numeroClabe = "";
    this.tipoCuenta = ""; // cheque, ahorro, C E D, movil, corriente, libreton, nomina, patrimonial
    this.moneda = ""; //pesos, UDIS, VSM
    this.saldo = "";
    this.saldoBuenFin = "";
    
    // saldo Patrimoniales
    this.saldoRecuperado = "KO"; // Indica si se han recuperado correctamente los datos del WS GetHeritageStatement
    this.saldoEfectivo = "";
    this.saldoSocInversionComun = "";
    this.saldoSocInversion = "";
    this.saldoMercadoCapitales = "";
    this.saldoMercadoDinero = "";
    
    
    this.movimientosRecuperados = "KO"; // Indica si se han recuperado correctamente los datos del WS 
    // movimientosCuenta[i] = new MovimientosCuenta();
    this.movimientosCuenta = new Array();
    
}


//TARJETA
function Tarjeta() {       
       // Variables comunes a tarjetas de prepago y credito
       this.numeroTarjeta = "";
       this.tipoTarjeta = ""; // credito, prepago y debito
       this.tipoTarjetaLocucion = ""; // credito, prepago y debito
       this.detalleTarjeta = ""; // Introducido por locucion de MOVIMIENTOS TARJETA
	   
       this.numeroCuentaAsociada = "";
       this.numeroCuentaAsociadaRecuperada = "KO"; // Indica si se ha recuperado correctamente los datos del WS GetCardInformation
       
       this.saldo = "";
       this.moneda = ""; //pesos, UDIS, VSM

       // Variables usadas cuando la tarjeta es de credito
       
       this.sobregiro = "";
       this.saldoActual = "";
       this.saldoDisponible = "";
       this.pagoNoGenerarIntereses = "";
       this.pagoMinimo = "";
       this.pagoVencido = "";
       this.fechaPago = ""; // formato YYYYMMDD
       
       this.saldoCreditoRecuperado = "KO"; // Indica si se ha recuperado correctamente los datos del WS WYDI
       
       this.saldoInmediato = ""; // En funcion de si es cero o mayor que cero, entra a estudiar el tipo de factura
                                                 // o  tratar los errores, respectivamente.
       this.estadoTarj = "";         // variable para el tratamiento de errores, que puede tomar los siguientes valores:
       
       this.tarjetaAsociada = ""; // Booleano. En funcion de la variable se reproduce menu de pago SALDO-TARJETA-CREDITO-MENU-PAGO (si) o va a fin
	   this.tipoTarifa = "";      // Variable que evalua el tipo de tarifa (por ejemplo LBM3_EMP)
       
	   this.movimientosRecuperados = "KO"; // Indica si se han recuperado correctamente los datos del WS 
       // movimientosTarjeta[i] = new MovimientosTarjeta();
       this.movimientosTarjeta = new Array();
       // pagosTarjeta[i] = new PagosTarjeta();
       this.pagosTarjeta = new Array();
       
       this.limCredito = "";      // Variable utilizada para el modulo de resumen de pagos de tarjetas de credito en el servio LB
       
       this.saldoDisponibleRecuperado = "KO"; // Indica si se ha recuperado correctamente el saldo del WS de ListMovements
       
       // Variables relacionadas con el modulo de Limites de Uso para Tarjetas
       this.limDiarioCompras = ""; 
       this.limMensualCompras = ""; 
       this.limDiarioEfectivo = ""; 
       this.limMensualEfectivo = ""; 
       
}


// TARJETA ACTIVAR
// WS LIST CARD
function TarjetaActivar() {
	this.numeroTarjeta = "";
	this.tipoTarjeta = "";
	this.estado = "";
	this.activableIVR = "";
}


