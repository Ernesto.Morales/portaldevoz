<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-INTEGRACIONES-FIN-LLAMADA
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>


<form id="ENRUTADOR_GESTION_INTEGRACIONES_FIN_LLAMADA">
	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - ENRUTADOR-GESTION-INTEGRACIONES-FIN-LLAMADA'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'FIN - ENRUTADOR-GESTION-INTEGRACIONES-FIN-LLAMADA'"/></log>
		<!-- marco que se procede a finalizar la llamada -->
		<assign name="VG_procederFinLlamada" expr="'SI'"/>
		<!-- ***** STAT: fin llamada ***** -->
		<assign name="VG_stat" expr="finLlamada(VG_stat,'', VG_codigoRetorno)"/> 
		<!-- ***** STAT: fin llamada ***** -->
		
		<!-- ***** STAT: fin operacion ***** -->
		<assign name="VG_stat" expr="finOperacion(VG_stat, '${milisecFinOperacion}', 'CALL', PRM_resultadoOperacion, VG_codigoRetorno)"/> 
		<!-- ***** STAT: fin operacion ***** -->
		<log label="ENRUTADOR"><value expr="'VG_stat.timeSalidaMilis: ' + VG_stat.timeSalidaMilis"/></log>
		<log label="ENRUTADOR"><value expr="'VG_stat.codLlamIVR: ' + VG_stat.codLlamIVR"/></log>
		<log label="ENRUTADOR"><value expr="'VG_stat.codServIVR: ' + VG_stat.codServIVR"/></log>
		
		<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"
				namelist="VG_loggerServicio"/>
<%-- 		<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"  --%>
<!-- 				namelist="VG_loggerServicio VG_codigoRetorno VG_timeInicioLlam VG_transfer VG_datosUUI VG_servicioCtiActivo"/> -->
				
	</block>
</form>

</vxml>
