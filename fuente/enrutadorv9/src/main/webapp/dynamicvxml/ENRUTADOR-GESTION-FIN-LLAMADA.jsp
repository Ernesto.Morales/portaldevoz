<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-FIN-LLAMADA
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_terminarLlamadaCti" expr="'NO'"/>
<var name="PRM_integracionCM" expr="'NO'"/>
<var name="PRM_finLlamada" expr="'NO'"/>

	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="ENRUTADOR"><value expr="'Catch de error en GESTION-FIN-LLAMADA ENRUTADOR'"/></log>
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		<goto next="#GESTIONA_FLUJO"/>
	</catch>
	
	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<catch event="connection.disconnect.hangup">
		<log label="ENRUTADOR"><value expr="'Catch de cuelgue en GESTION-FIN-LLAMADA ENRUTADOR'"/></log>
		<!-- cambio el codigoRetorno final a Hangup -->
		<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
		<goto next="#GESTIONA_FLUJO"/>
	</catch>


<form id="ENRUTADOR_GESTION_FIN_LLAMADA">
	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - ENRUTADOR-GESTION-FIN-LLAMADA'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<goto next="#TERMINAR_LLAMADA_CTI"/>
	</block>
</form>


<form id="TERMINAR_LLAMADA_CTI">
	<block>
		<log label="ENRUTADOR"><value expr="'Voy a enviar fin de llamada a cti'"/></log>
		<assign name="PRM_terminarLlamadaCti" expr="'SI'"/>
	</block>
				
	<subdialog name="subTerminarLlamadaCti" src="${pageContext.request.contextPath}/subTerminarLlamadaCti" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio VG_codigoRetorno VG_datosUUI VG_servicioCtiActivo VG_RetornoCTI">
			
		<filled>
			<log label="ENRUTADOR"><value expr="'Vuelvo de enviar fin de llamada a cti'"/></log>
			<log label="ENRUTADOR"><value expr="'resultadoOperacion:' + subTerminarLlamadaCti.SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error:' + subTerminarLlamadaCti.SUB_error"/></log>
			
			<if cond="subTerminarLlamadaCti.SUB_resultadoOperacion == 'HANGUP'">
				<!-- solo si ha habido HANGUP cambio el codigoRetorno final -->
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
			</if>
						
			<!-- en cualquier caso continuo y voy a integrar con cuadro de mando -->
			<!-- ABALFARO_20170627 antes de ir a integrar el cuadro de mando compruebo que haya generado un tsec publico -->
			<if cond="VG_cliente.tsec != null &amp;&amp; VG_cliente.tsec != ''">
				<!-- tengo un tsec -->
				<goto next="#INTEGRACION_CUADRO_MANDO"/>
			<else/>
				<!-- primero debo generar un tsec -->
				<goto next="#GENERA_TSEC_PUBLICO"/>
			</if>
			
		</filled>
	</subdialog>	
</form>


<!-- ABALFARO_20170627 genera un tsec publico si no llegamos a pasar por el controlador -->
<form id="GENERA_TSEC_PUBLICO">
	<block>
		<log label="ENRUTADOR"><value expr="'Voy a generar un tsec publico'"/></log>
	</block>
	
	<subdialog name="subGeneraTsecPublico" src="${pageContext.request.contextPath}/subGeneraTsecPublico" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio">
			
		<filled>
			<log label="ENRUTADOR"><value expr="'Vuelvo de generar un tsec publico'"/></log>
			<log label="ENRUTADOR"><value expr="'resultadoOperacion:' + subGeneraTsecPublico.SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error:' + subGeneraTsecPublico.SUB_error"/></log>
			
			<if cond="subGeneraTsecPublico.SUB_resultadoOperacion == 'HANGUP'">
				<!-- solo si ha habido HANGUP cambio el codigoRetorno final -->
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
			</if>
			
			<!-- guardo el tsec generado en el cliente -->
			<assign name="VG_cliente.tsec" expr="subGeneraTsecPublico.SUB_tsec"/>
			<!-- voy a integrar con cuadro de mando -->
			<goto next="#INTEGRACION_CUADRO_MANDO"/>
		</filled>
	</subdialog>
</form>


<form id="INTEGRACION_CUADRO_MANDO">
	<block>
		<log label="ENRUTADOR"><value expr="'Voy a realizar la integracion con Cuadro de Mando'"/></log>
		<log label="ENRUTADOR"><value expr="'VG_cliente.tsec: ' + VG_cliente.tsec"/></log>
		<assign name="PRM_integracionCM" expr="'SI'"/>
	</block>
	
	<subdialog name="subIntegracionCuadroMando" src="${pageContext.request.contextPath}/subIntegracionCuadroMando" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio VG_cliente.tsec VG_stat VG_servicioStatCmActivo">
			
		<filled>
			<log label="ENRUTADOR"><value expr="'Vuelvo de realizar la integracion con Cuadro de Mando'"/></log>
			<log label="ENRUTADOR"><value expr="'resultadoOperacion:' + subIntegracionCuadroMando.SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error:' + subIntegracionCuadroMando.SUB_error"/></log>
			
			<if cond="subIntegracionCuadroMando.SUB_resultadoOperacion == 'HANGUP'">
				<!-- solo si ha habido HANGUP cambio el codigoRetorno final -->
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
			</if>
			
			<!-- en cualquier caso finalizaremos la llamada -->
			<!-- ya sea error, hangup, ok... -->
			<goto next="#FIN_LLAMADA"/>
		</filled>
	</subdialog>
</form>


<form id="FIN_LLAMADA">
	<block>
		<log label="ENRUTADOR"><value expr="'Voy a pintar la traza fin llamada'"/></log>
		<assign name="PRM_finLlamada" expr="'SI'"/>
	</block>
	
	<subdialog name="subTracearFinLlamada" src="${pageContext.request.contextPath}/subTracearFinLlamada" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio VG_codigoRetorno VG_timeInicioLlam VG_transfer">
			
		<filled>
			<log label="ENRUTADOR"><value expr="'Vuelvo de pintar la traza fin llamada'"/></log>
			<log label="ENRUTADOR"><value expr="'resultadoOperacion:' + subTracearFinLlamada.SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'duracionLlamada:' + subTracearFinLlamada.SUB_duracionLlamada"/></log>
			<log label="ENRUTADOR"><value expr="'error:' + subTracearFinLlamada.SUB_error"/></log>
			<!-- en cualquier caso finalizaremos la llamada -->
			<!-- ya sea error, hangup, ok... -->
			<goto next="#EXIT"/>
		</filled>
	</subdialog>
</form>


<form id="GESTIONA_FLUJO">
	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - GESTIONA_FLUJO'"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_terminarLlamadaCti: ' + PRM_terminarLlamadaCti"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_integracionCM: ' + PRM_integracionCM"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_finLlamada: ' + PRM_finLlamada"/></log>
		
		<if cond="PRM_terminarLlamadaCti == 'NO'">
			<assign name="PRM_terminarLlamadaCti" expr="'SI'"/>
			<goto next="#TERMINAR_LLAMADA_CTI"/>
		</if>
		
		<if cond="PRM_integracionCM == 'NO'">
			<assign name="PRM_integracionCM" expr="'SI'"/>
			<!-- ABALFARO_20170627 antes de ir a integrar el cuadro de mando compruebo que haya generado un tsec publico -->
			<if cond="VG_cliente.tsec != null &amp;&amp; VG_cliente.tsec != ''">
				<!-- tengo un tsec -->
				<goto next="#INTEGRACION_CUADRO_MANDO"/>
			<else/>
				<!-- primero debo generar un tsec -->
				<goto next="#GENERA_TSEC_PUBLICO"/>
			</if>
		</if>
		
		<if cond="PRM_finLlamada == 'NO'">
			<assign name="PRM_finLlamada" expr="'SI'"/>
			<goto next="#FIN_LLAMADA"/>
		</if>
		
		<goto next="#EXIT"/>			
	</block>
</form>


<form id="EXIT">
	<block>
		<log label="ENRUTADOR"><value expr="'FIN - ENRUTADOR-GESTION-FIN-LLAMADA'"/></log>
		<!-- finalizo la llamada -->
		<exit/>
	</block>
</form>

</vxml>
