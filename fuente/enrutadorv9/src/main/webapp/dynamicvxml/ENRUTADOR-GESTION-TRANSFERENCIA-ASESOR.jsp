<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<%-- <vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp"> --%>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="es-MX" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-TRANSFERENCIA-ASESOR
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="idLocucionInfoGrabacion"/>
<var name="idLocucionInfoTransferencia"/>

<var name="PRM_duracionLlamada"/>
<var name="PRM_destino"/>
<var name="PRM_uui"/>

<!--
****************************************************
*************** CAPTURA DE TRANSFER ****************
****************************************************
-->
<catch event="connection.disconnect.transfer">
	
	<!-- Capturamos el transfer y seguimos ejecutando las integraciones y estadisticas para finalizar la llamada -->
	<log label="ENRUTADOR"><value expr="'CAPTURA TRANSFER DISCONNECT'"/></log>
	
	<submit next="${pageContext.request.contextPath}/gestionIntegracionesFinLlamada" method="post" namelist="VG_loggerServicio"/>
	
</catch>


<form id="ENRUTADOR_GESTION_TRANSFERENCIA_ASESOR">

	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - GESTION-TRANSFERENCIA-ASESOR'"/></log>
		
		<assign name="VG_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="VG_error" expr="'${error}'"/>
		
		<log label="ENRUTADOR"><value expr="'VG_resultadoOperacion: ' + VG_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'VG_error: ' + VG_error"/></log>
		
		<assign name="PRM_duracionLlamada" expr="'${duracionLlamada}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_duracionLlamada: ' + PRM_duracionLlamada"/></log>
		
		<if cond="VG_resultadoOperacion == 'OK'">
			<!--  recupero los datos de la transferencia -->
			<assign name="VG_transfer.retornoTransfer" expr="'SI'"/>
			<assign name="VG_transfer.numTransfer" expr="'${vdnTransfer}'"/>
			<assign name="VG_transfer.cti" expr="'${ctiTransfer}'"/>
			<assign name="VG_transfer.isDentroHorario" expr="'${isDentroHorario}'"/>
			<assign name="VG_transfer.locucionFH" expr="'${locFH}'"/>
			<assign name="VG_transfer.modoLocFH" expr="'${modoLocFH}'"/>	
			<assign name="VG_transfer.segmento" expr="'${segmentoTransfer}'"/>
			<if cond="VG_servicioCtiActivo == 'ON' &amp;&amp; VG_transfer.cti == 'ON'">
				<assign name="VG_transfer.cti" expr="'ON'"/>
			<else/>
				<assign name="VG_transfer.cti" expr="'OFF'"/>
			</if>
		
		<else/>
			<!-- transfiero al vdn por defecto -->
			<assign name="VG_transfer.retornoTransfer" expr="'SI'"/>
			<assign name="VG_transfer.numTransfer" expr="VG_servicioVdnTransferDef"/>
			<assign name="VG_transfer.cti" expr="VG_servicioCtiActivo"/>
			<assign name="VG_transfer.isDentroHorario" expr="'OFF'"/>
			<assign name="VG_transfer.locucionFH" expr="''"/>
			<assign name="VG_transfer.modoLocFH" expr="''"/>	
			
			<!-- lo dejo marcado en el segmento -->
			<assign name="VG_transfer.segmento" expr="'DEFAULT_SERV'"/>	
			
		</if>
	
		<!-- ***** STAT: guarda segmento de transferencia ***** -->	
		<assign name="VG_stat" expr="guardaSegmentoTransferencia(VG_stat, VG_transfer.numTransfer)"/>
		<!-- ***** STAT: guarda segmento de transferencia ***** -->
					
		<log label="ENRUTADOR"><value expr="'VG_transfer.retornoTransfer: ' + VG_transfer.retornoTransfer"/></log>
		<log label="ENRUTADOR"><value expr="'VG_transfer.numTransfer: ' + VG_transfer.numTransfer"/></log>
		<log label="ENRUTADOR"><value expr="'VG_transfer.cti: ' + VG_transfer.cti"/></log>
		<log label="ENRUTADOR"><value expr="'VG_transfer.isDentroHorario: ' + VG_transfer.isDentroHorario"/></log>
		<log label="ENRUTADOR"><value expr="'VG_transfer.locucionFH: ' + VG_transfer.locucionFH"/></log>
		<log label="ENRUTADOR"><value expr="'VG_transfer.modoLocFH: ' + VG_transfer.modoLocFH"/></log>
		
				
		<log label="ENRUTADOR"><value expr="'FIN - GESTION-TRANSFERENCIA-ASESOR'"/></log>
		
		<goto next="#DAR_LOCUCIONES_TRANSFER"/>
				
	</block>

</form>

<form id="DAR_LOCUCIONES_TRANSFER">

	<block>		
		<!-- LOCUCIONES -->
		
		<if cond="VG_transfer.isDentroHorario == 'false'">
		
			<!-- FUERA DE HORARIO -->
			<if cond="VG_transfer.modoLocFH == 'WAV'">
				<prompt bargein="false">
					<audio expr="VG_transfer.locucionFH"/>
				</prompt>		
			<else/>
				<prompt bargein="false">
					<value expr="VG_transfer.locucionFH"/>
				</prompt>
			</if>
			
			<!-- despues de la locucion fuera horario debemos finalizar la llamada {OK o FIN}  -->
			<assign name="VG_codigoRetorno" expr="'FIN'"/>
			
			<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>	
			
		<else/>
			<!-- DENTRO DE HORARIO (ON/true), o SIN CONTROL HORARIO (OFF) -->
				
			<!-- TRANSFERENCIA -->

				<!-- Reproduzco locucion del servicio: COM-<cod_servivr>-INFO-GRABACION -->
				<prompt bargein="false">
					<c:set var="idLocucionInfoGrabacion" value="COM-${idServicio}-INFO-GRABACION" />
					<c:forEach items="${locComunes.getLocucionesByNombre(idLocucionInfoGrabacion)}" var="loc">					
						<c:choose>
						    <c:when test="${loc.modo != null && loc.modo == 'WAV'}">
						    	<!-- es WAV -->
						       	<audio expr="'${loc.value}'"/>
						    </c:when>
						    <c:otherwise>
						    	<!-- es TTS -->
						       	<value expr="'${loc.value}'"/>
						    </c:otherwise>
						</c:choose>
					</c:forEach>	
				</prompt>
					
				<!-- Reproduzco locucion del servicio: COM-<cod_servivr>-INFO-TRANSFERENCIA -->
				<prompt bargein="false">
					<c:set var="idLocucionInfoTransferencia" value="COM-${idServicio}-INFO-TRANSFERENCIA" />
					<c:forEach items="${locComunes.getLocucionesByNombre(idLocucionInfoTransferencia)}" var="loc">					
						<c:choose>
						    <c:when test="${loc.modo != null && loc.modo == 'WAV'}">
						    	<!-- es WAV -->
						       	<audio expr="'${loc.value}'"/>
						    </c:when>
						    <c:otherwise>
						    	<!-- es TTS -->
						       	<value expr="'${loc.value}'"/>
						    </c:otherwise>
						</c:choose>
					</c:forEach>	
				</prompt>		
			
		</if>

		
		<!-- Adjuntamos datos a la llamada -->
		<if cond="VG_transfer.cti == 'ON'">		
			<goto next="#SUB_SET_DATOS_CTI" />
		<else/>
			<assign name="PRM_uui" expr="VG_datosLlamada.uuiDefault"/>
			<log label="ENRUTADOR"><value expr="'PRM_uui: ' + PRM_uui"/></log>
		
			<assign name="PRM_destino" expr="'sip:' + VG_transfer.numTransfer + '@' + session.connection.protocol.sip.request.host"/>
			<log label="ENRUTADOR"><value expr="'PRM_destino: ' + PRM_destino"/></log>
			<goto next="#REALIZAR_TRANSFERENCIA" />
		</if>
		
	</block>
</form>

<form id="SUB_SET_DATOS_CTI">

	<block>	
			<!-- Preparo los datos a guardar en DatosCTI -->
			<assign name="VG_datosCTI.vdnDestino" expr="VG_transfer.numTransfer"/>

			<assign name="VG_datosCTI.xferId" expr="''"/>
			<assign name="VG_datosCTI.segmentoCliente" expr="''"/>
			<assign name="VG_datosCTI.producto" expr="''"/>

			<!-- comprobar este dato en caso de que el cliente no se identifique -->
			<assign name="VG_datosCTI.llaveAccesoFront" expr="VG_llaveAccesoFront"/> 
			
			<assign name="VG_datosCTI.campania" expr="''"/>		
			<assign name="VG_datosCTI.canal" expr="'MPVO'"/> 
				
			<assign name="VG_datosCTI.numCliente" expr="VG_cliente.numCliente"/>
			
			<assign name="VG_datosCTI.sector" expr="''"/>	
			
			<assign name="VG_datosCTI.nombreCliente" expr="VG_cliente.nombreCliente + ' ' + VG_cliente.apellido1 + ' ' + VG_cliente.apellido2"/>
			
			<assign name="VG_datosCTI.telefonoDinamico" expr="''"/>

			<assign name="VG_datosCTI.telCtePersonas" expr="VG_cliente.telefonoContacto"/>
			
			<assign name="VG_datosCTI.idAplicacion" expr="'1'"/>
						
			<if cond="VG_cliente.isAutenticado == 'true' &amp;&amp; VG_loggerServicio.idServicio.search('PRESTAMOS') != -1">	
				<!-- el cliente se ha autenticado -->
				<if cond="VG_cliente.ramaIdentificacion == 'AUTO'">	
					<!-- autenticado en la rama de auto. Candados: FECHA_NACIM_CLI|ANYO_CONTR_CRED|ANYO_MATRIC_COCHE -->					
					<assign name="VG_datosCTI.metodoAutenticacion" expr="'17'"/>
				<else/>
					<!-- autenticado en la rama de hipoteca. Candados: FECHA_NACIM_CLI|COD_POSTAL -->	
					<assign name="VG_datosCTI.metodoAutenticacion" expr="'18'"/>
				</if>
			</if>
			
			<!-- FALTA, confirmacion Bancomer -->
			<!-- <assign name="VG_datosCTI.numeroSesion" expr="''"/>  -->

			<assign name="VG_datosCTI.comentario" expr="VG_loggerServicio.idServicio"/>	
			
			
			<!-- Preparo los datos a guardar en DatosUUI -->
			<assign name="VG_datosUUI.llaveSegmento" expr="''"/> 
			<assign name="VG_datosUUI.xferId" expr="''"/> 
			<assign name="VG_datosUUI.segmentoCliente" expr="''"/> 
			
			<assign name="VG_datosUUI.canal" expr="'MPVO'"/> 
			<assign name="VG_datosUUI.numCliente" expr="VG_cliente.numCliente"/>
			<assign name="VG_datosUUI.llaveAccesoFront" expr="VG_llaveAccesoFront"/> 
			
			<if cond="VG_cliente.isAutenticado == 'true' &amp;&amp; VG_loggerServicio.idServicio.search('PRESTAMOS') != -1">		
				<!-- el cliente se ha autenticado -->
				<if cond="VG_cliente.ramaIdentificacion == 'AUTO'">	
					<!-- autenticado en la rama de auto. Candados: FECHA_NACIM_CLI|ANYO_CONTR_CRED|ANYO_MATRIC_COCHE -->					
					<assign name="VG_datosUUI.metodoAutenticacion" expr="'17'"/>
				<else/>
					<!-- autenticado en la rama de hipoteca. Candados: FECHA_NACIM_CLI|COD_POSTAL -->	
					<assign name="VG_datosUUI.metodoAutenticacion" expr="'18'"/>
				</if>
			</if>
			
			<!-- FALTA, confirmacion Bancomer -->
			<!-- <assign name="VG_datosUUI.numeroSesion" expr="''"/>  -->
			
			<assign name="VG_datosUUI.campania" expr="''"/> 
	
	</block>
		
	<subdialog name="subSetDatosCti" src="${pageContext.request.contextPath}/subSetDatosCti" method="post" namelist="VG_loggerServicio VG_datosCTI VG_datosUUI VG_datosLlamada.uri">
			
		<filled>
			
			<assign name="VG_resultadoOperacion" expr="subSetDatosCti.SUB_resultadoOperacion"/>	
			<assign name="VG_error" expr="subSetDatosCti.SUB_error"/>
			
			<assign name="PRM_destino" expr="subSetDatosCti.SUB_destino"/>
			<assign name="PRM_uui" expr="subSetDatosCti.SUB_uui"/>
			
			<if cond="VG_resultadoOperacion == 'HANGUP'">
				<!-- en caso de cuelgue finalizamos la llamada -->			
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>	
				<assign name="VG_resultadoOperacion" expr="'KO'"/>	
				<assign name="VG_error" expr="''"/>
								
				<submit next="${pageContext.request.contextPath}/gestionIntegracionesFinLlamada" method="post" namelist="VG_loggerServicio"/>
		
			<else/>
				<goto next="#REALIZAR_TRANSFERENCIA"/>
			</if>
					
			
		</filled>	
		
	</subdialog>	
</form>

<form id="REALIZAR_TRANSFERENCIA">

	<block>	
			<log label="ENRUTADOR"><value expr="'VOY A TRANSFERIR A: ' + VG_transfer.numTransfer"/></log>
			
			
			<!-- ABALFARO_20170315 -->
			<!-- ***** STAT: aniade contador ***** -->
			<assign name="VG_stat" expr="aniadeContador(VG_stat, 'CALL_OUT', 'INFO', '', 'OUT', VG_transfer.segmento)"/> 
			<!-- ***** STAT: aniade contador ***** -->	
			
	</block>
	
	<transfer destexpr="PRM_destino+'?User-To-User='+PRM_uui" connecttimeout="60s" bridge="false" ></transfer>
	
	<!--
	<transfer  destexpr="'tel:'+ VG_transfer.numTransfer" type="blind" />
	-->
	<!-- Capturamos el evento de transfer arriba para seguir operando -->
		
</form>



</vxml>
