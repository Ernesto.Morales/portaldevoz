<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-SALIDA
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>

<var name="PRM_controlTransferActivo" expr="''"/>
<var name="PRM_encuestaActivo" expr="''"/>
<var name="PRM_encuestaActivoDefecto" expr="''"/>
<var name="PRM_xferId" expr="''"/>


<var name="PRM_sessionSipHost" expr="session.connection.protocol.sip.request.host"/>


<form id="ENRUTADOR_GESTION_SALIDA">
	<block>		
		<log label="ENRUTADOR"><value expr="'INICIO - GESTION-SALIDA'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
				
		<if cond="PRM_resultadoOperacion != 'OK'">
			<!-- algo ha fallado en la ejecucion del servlet -->
			<goto next="#SUB_GESTION_EXCEPCIONES" />
			
		<else/>
			<assign name="PRM_controlTransferActivo" expr="'${controlTransferActivo}'"/>
			<assign name="PRM_encuestaActivo" expr="'${encuestaActivo}'"/>
			<assign name="PRM_encuestaActivoDefecto" expr="'${encuestaActivoDefecto}'"/>
			
			<log label="ENRUTADOR"><value expr="'PRM_controlTransferActivo: ' + PRM_controlTransferActivo"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_encuestaActivo: ' + PRM_encuestaActivo"/></log>
		
<!-- 			<if cond="VG_loggerServicio.tipo == 'MAU'"> -->
<!-- 				<assign name="PRM_resultadoOperacion" expr="'OK'"/> -->
<!-- 				<assign name="PRM_codigoRetorno" expr="'OK'"/> -->
<!-- 				<assign name="PRM_error" expr="''"/> -->
<!-- 			</if> -->
			
			
			<!-- Analizo el codigo de retorno obtenido -->
			<if cond="PRM_codigoRetorno == 'FIN'">
				
				<!-- CODIGO RETORNO = OK o FIN -->
				<!-- TODO activar cuando se desarrolle la encuesta, ahora iremos siempre a fin -->
<!-- 				<goto next="#FIN" /> -->


				<!--  ABALFARO_20170210 CONTINGENCIA evitar que PRESTAMOS pase por ENCUESTAS -->
				<if cond="VG_loggerServicio.idServicio == 'PRESTAMOSv1' || VG_loggerServicio.idServicio == 'PRESTAMOSv2' ||
							VG_loggerServicio.idServicio == 'PRESTAMOSv3' || VG_loggerServicio.idServicio == 'PRESTAMOSv4'">7
					<goto next="#FIN" />
				<else/>
					<goto next="#FIN" />
				</if>
				<!--  ABALFARO_20170210 CONTINGENCIA evitar que PRESTAMOS pase por ENCUESTAS -->

<!-- 			Verifica el c�digo de retorno -->
<!-- 			<prompt> -->
<!-- 				C�digo de retorno -->
<!-- 				<value expr="PRM_codigoRetorno"/> -->
<!-- 			</prompt> -->
			
			<elseif cond="PRM_codigoRetorno == 'OK'" />
				<goto next="#FIN" />
				
			<elseif cond="PRM_codigoRetorno == 'TRANSFER'" />
				<!-- CODIGO RETORNO = TRANSFER -->
<!-- 				Truqueado para pruebas de transferencias bny -->
<!-- 				<assign name="VG_transfer.segmentoId" expr="'TRANSFERENCIAS_SALDOS_MOVIMIENTOS'"/> -->
				
				
				<if cond="PRM_controlTransferActivo == 'ON'">
					<log label="ENRUTADOR"><value expr="'FIN - GESTION-SALIDA'"/></log>
					<submit next="${pageContext.request.contextPath}/gestionTransferenciaAsesor" method="post" 
							namelist="VG_loggerServicio VG_transfer.ligaRetorno VG_transfer.segmentoId VG_transfer.segmento VG_servicioVdnTransferDef VG_servicioCtiActivo 
								VG_timeInicioLlam VG_datosLlamada.uuiDefault PRM_sessionSipHost VG_stat"/>
				<else/>
					<assign name="VG_error" expr="'TRANSFER_DESACTIVADA'"/>
					<goto next="#SUB_GESTION_EXCEPCIONES" />
				</if>
				
			<elseif cond="PRM_codigoRetorno == 'HANGUP'" />
				
				<!-- CODIGO RETORNO = HANGUP -->
				<assign name="VG_codigoRetorno" expr="PRM_codigoRetorno"/>	
				<assign name="VG_resultadoOperacion" expr="'OK'"/>	
				<assign name="VG_error" expr="''"/>
								
				<goto next="#FIN"/>		
			
			<else/>
				<!-- codigoRetorno = ERROR u otra cosa -->
				<!-- ABALFARO_20170111 comentado -->
				<!-- <assign name="VG_gestionExcepcionesPrevia" expr="'SI'"/>  -->
								
				<goto next="#SUB_GESTION_EXCEPCIONES" />
			</if>
				
		</if>
		
	</block>

</form>

<form id="SUB_ENCUESTA_IRENE">

	<block>	
	
		<log label="ENRUTADOR"><value expr="'Inicio SUB_ENCUESTA_IRENE'"/></log>
		
		<assign name="PRM_xferId" expr="VG_datosCTI.xferId"/>
		
		<if cond="PRM_encuestaActivo != 'OFF'">
			<if cond="PRM_encuestaActivo.substring(0,1) != '#'">
				<!-- la encuesta debe ser transferida con el agente que se indica -->
				<assign name="VG_transfer.segmento" expr="PRM_encuestaActivo"/>
			</if>
		</if>
		
	</block>
	
	<subdialog name="subEjecucionEncuesta" src="${pageContext.request.contextPath}/subEjecucionEncuesta" method="post" 
		namelist="VG_loggerServicio PRM_encuestaActivo PRM_encuestaActivoDefecto PRM_xferId">
		
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>		
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_datosCTI" expr="VG_datosCTI"/>
		<param name="PRM_IN_datosUUI" expr="VG_datosUUI"/>
			
		<filled>
		
			<log label="ENRUTADOR"><value expr="'He vuelto a SUB_ENCUESTA_IRENE'"/></log>
			
			
			<assign name="VG_codigoRetorno" expr="subEjecucionEncuesta.PRM_codigoRetorno"/>	
			<assign name="VG_resultadoOperacion" expr="subEjecucionEncuesta.PRM_resultadoOperacion"/>	
			<assign name="VG_error" expr="subEjecucionEncuesta.PRM_error"/>
			<assign name="VG_cliente" expr="subEjecucionEncuesta.PRM_cliente"/>
			
			<log label="ENRUTADOR"><value expr="'CodigoRetorno de EncuestaIrene: ' + VG_codigoRetorno"/></log>
			<log label="ENRUTADOR"><value expr="'ResultadoOperacion de EncuestaIrene: ' + VG_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'Error de EncuestaIrene: ' + VG_error"/></log>
			<if cond="VG_codigoRetorno == 'FIN'">
				
				<goto next="#FIN" />
				
			<elseif cond="VG_codigoRetorno == 'OK'" />
				<goto next="#FIN" />
				
			<elseif cond="VG_codigoRetorno == 'TRANSFER'" />
				
				<!-- CODIGO RETORNO = TRANSFER -->
				<if cond="PRM_controlTransferActivo == 'ON'">
					<log label="ENRUTADOR"><value expr="'FIN - GESTION-SALIDA'"/></log>
					<submit next="${pageContext.request.contextPath}/gestionTransferenciaAsesor" method="post" 
							namelist="VG_loggerServicio VG_transfer.segmento VG_servicioVdnTransferDef VG_servicioCtiActivo 
								VG_timeInicioLlam VG_datosLlamada.uuiDefault PRM_sessionSipHost VG_stat"/>
				<else/>
					<assign name="VG_error" expr="'TRANSFER_DESACTIVADA'"/>
					<!-- <goto next="#SUB_GESTION_EXCEPCIONES" /> -->
					
					<!-- ABALFARO_20170210 si falla la encuesta siempre vamos a fin -->
					<goto next="#FIN" />
				</if>
				
			<elseif cond="VG_codigoRetorno == 'HANGUP'" />
				
				<!-- CODIGO RETORNO = HANGUP -->
<!-- 				<assign name="VG_codigoRetorno" expr="PRM_codigoRetorno"/>	 -->
				<assign name="VG_resultadoOperacion" expr="'OK'"/>	
				<assign name="VG_error" expr="''"/>
								
				<goto next="#FIN"/>		
			
			<else/>
				<!-- codigoRetorno = ERROR u otra cosa -->
				<!-- ABALFARO_20170111 comentado -->
				<!-- <assign name="VG_gestionExcepcionesPrevia" expr="'SI'"/>  -->
								
				<!-- <goto next="#SUB_GESTION_EXCEPCIONES" /> -->
				<!-- ABALFARO_20170210 si falla la encuesta siempre vamos a fin -->
				<goto next="#FIN" />
			</if>
	
		</filled>
	
	</subdialog>
	
</form>

<form id="SUB_GESTION_EXCEPCIONES">

	<block>	
		<!-- ABALFARO_20170111 -->
		<assign name="VG_gestionExcepcionesPrevia" expr="'SI'"/>
	
	</block>
	
	<subdialog name="subGestionExcepciones" src="${pageContext.request.contextPath}/subGestionExcepciones" method="post" namelist="VG_loggerServicio VG_error">
			
		<filled>
			<assign name="VG_codigoRetorno" expr="subGestionExcepciones.SUB_codigoRetorno"/>	
			<assign name="VG_resultadoOperacion" expr="subGestionExcepciones.SUB_resultadoOperacion"/>	
			<assign name="VG_error" expr="subGestionExcepciones.SUB_error"/>
			
			<log label="ENRUTADOR"><value expr="'CodigoRetorno de GestionExcepciones: ' + VG_codigoRetorno"/></log>
			<log label="ENRUTADOR"><value expr="'ResultadoOperacion de GestionExcepciones: ' + VG_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'Error de GestionExcepciones: ' + VG_error"/></log>
			
			<if cond="VG_codigoRetorno == 'TRANSFER'">
				<log label="ENRUTADOR"><value expr="'FIN - GESTION-SALIDA'"/></log>
			
				<submit next="${pageContext.request.contextPath}/gestionTransferenciaAsesor" method="post" 
						namelist="VG_loggerServicio VG_transfer.segmento VG_servicioVdnTransferDef VG_servicioCtiActivo 
								VG_timeInicioLlam VG_datosLlamada.uuiDefault PRM_sessionSipHost VG_stat"/>
			<else/>
				<goto next="#FIN"/>		
			
			</if>

	
		</filled>
	
	</subdialog>
</form>

<form id="FIN">

	<block>	
		<log label="ENRUTADOR"><value expr="'ResultadoOperacion de GestionSalida: ' + VG_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'CodigoRetorno de GestionSalida: ' + VG_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'Error de GestionSalida: ' + VG_error"/></log>
				
		<log label="ENRUTADOR"><value expr="'FIN - GESTION-SALIDA'"/></log>

		<submit next="${pageContext.request.contextPath}/gestionIntegracionesFinLlamada" method="post" namelist="VG_loggerServicio"/>
	
	</block>
</form>


</vxml>
