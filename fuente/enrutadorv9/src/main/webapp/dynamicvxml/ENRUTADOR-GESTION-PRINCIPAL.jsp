<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-PRINCIPAL
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="SUB_datosCTI"/>
<var name="SUB_datosUUI"/>
<form id="ENRUTADOR_GESTION_PRINCIPAL">
	<block>		
		<log label="ENRUTADOR"><value expr="'INICIO - GESTION-PRINCIPAL'"/></log>
				
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="ENRUTADOR"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'FIN - GESTION-PRINCIPAL'"/></log>
		
		<if cond="PRM_resultadoOperacion == 'OK'">
			
			<script>
				VG_infoControlador = new BeanInfoControlador();
			</script>
		
			<assign name="VG_infoControlador.nombre" expr="'${infoControlador.nombre}'"/>
			<assign name="VG_infoControlador.isActivo" expr="'${infoControlador.activo}'"/>
			<assign name="VG_infoControlador.ruta" expr="'${infoControlador.ruta}'"/>
			<assign name="VG_infoControlador.activacionSegmento" expr="'${infoControlador.transferencias.transfer.segmento}'"/>
			<assign name="VG_infoControlador.segmentoTransfer" expr="'${infoControlador.transferencias.transfer.value}'"/>
			<assign name="VG_infoControlador.segmentoTransferDefault" expr="'${infoControlador.transferencias.transferDefault}'"/>
			

			
			<!-- ABALFARO_20170117 obtengo transfer excepciones -->
			<c:if test="${(not empty infoControlador.transferencias.transferExcepciones)}">		
					<c:forEach items="${infoControlador.transferencias.transferExcepciones}" var="excep">	
						<script>
							var numElems = VG_infoControlador.transferExcepciones.length;
							VG_infoControlador.transferExcepciones[numElems] = new TransferExcepcion();
						</script>
						<assign name="VG_infoControlador.transferExcepciones[numElems].nombre" expr="'${excep.nombre}'"/>
						<assign name="VG_infoControlador.transferExcepciones[numElems].segmentoTransfer" expr="'${excep.value}'"/>
						
						<log label="ENRUTADOR"><value expr="'VG_infoControlador.transferExcepciones[numElems].nombre: ' + VG_infoControlador.transferExcepciones[numElems].nombre"/></log>
						<log label="ENRUTADOR"><value expr="'VG_infoControlador.transferExcepciones[numElems].segmentoTransfer: ' + VG_infoControlador.transferExcepciones[numElems].segmentoTransfer"/></log>
					</c:forEach>
			</c:if>
			<!-- ABALFARO_20170117 -->
			
			
			
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.nombre: ' + VG_infoControlador.nombre"/></log>
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.isActivo: ' + VG_infoControlador.isActivo"/></log>
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.ruta: ' + VG_infoControlador.ruta"/></log>
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.activacionSegmento: ' + VG_infoControlador.activacionSegmento"/></log>
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.segmentoTransfer: ' + VG_infoControlador.segmentoTransfer"/></log>
			<log label="ENRUTADOR"><value expr="'VG_infoControlador.segmentoTransferDefault: ' + VG_infoControlador.segmentoTransferDefault"/></log>
			
			<!-- actualizo el objeto de transferencias con la info del controlador, en caso de segmento=OFF -->
			<!-- en caso de segmento=ON seguire con el segmento de la entrada -->
			<if cond="VG_infoControlador.activacionSegmento == 'OFF'">
				<assign name="VG_transfer.segmento" expr="VG_infoControlador.segmentoTransfer"/>
			</if>
			
			
			<!-- Guardo el nombre del controlador como PRODUCTO -->
			<assign name="VG_producto" expr="VG_infoControlador.nombre"/>
			<log label="ENRUTADOR"><value expr="'VG_producto: ' + VG_producto"/></log>
			
			<!-- y tambien como ESQUEMA para inicializar -->
			<assign name="VG_esquema" expr="VG_infoControlador.nombre"/>
			<log label="ENRUTADOR"><value expr="'VG_esquema: ' + VG_esquema"/></log>
			
			<submit next="${pageContext.request.contextPath}/ejecucionControlador" method="post" namelist="VG_loggerServicio VG_infoControlador VG_esquema VG_producto VG_datosCTI VG_datosUUI"/>
					
		<else/>
		
			<assign name="VG_error" expr="PRM_error"/>
		
			<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>
		
		</if>
		
	</block>
</form>

</vxml>
