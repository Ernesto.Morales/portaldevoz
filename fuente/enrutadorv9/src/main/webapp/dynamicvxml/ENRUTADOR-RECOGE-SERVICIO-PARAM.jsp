<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-RECOGE-SERVICIO-PARAM
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<%-- <script src="${pageContext.request.contextPath}/scripts/BeanServicio.js"/> --%>

<var name="PRM_pruebaServicio" expr="''"/>
<var name="PRM_resultado" expr="''"/>

<var name="PRM_pruebaAna" expr="''"/>


<form id="ENRUTADOR_RECOGE_SERVICIO_PARAM">

	<block>				
		<log label="ENRUTADOR"><value expr="'Estoy en RECOGE-SERVICIO-PARAM'"/></log>
		
		<assign name="PRM_resultado" expr="'${resultado}'"/>
		
		<log label="ENRUTADOR"><value expr="'Resultado: ' + PRM_resultado"/></log>
		
		<assign name="PRM_pruebaAna" expr="'${sessionScope.pruebaAnaKey}'"/>	
				
		<log label="ENRUTADOR"><value expr="'PRM_pruebaAna: ' + PRM_pruebaAna"/></log>
		
				<log label="ENRUTADOR"><value expr="'Prueba sesion codigo servicio: ${sessionScope.beanServicio.codServIVR}'"/></log>
	
		
		<!--  Prueba asignación de objetos en VXML -->
		<assign name="PRM_pruebaServicio" expr="PRM_beanServicio"/>
		
		
		<log label="ENRUTADOR"><value expr="'******** SERVICIO PRUEBA ********'"/></log>
		<log label="ENRUTADOR"><value expr="'codServIVR: ' + PRM_pruebaServicio.codServIVR"/></log>
		
		                   	

		<log label="ENRUTADOR"><value expr="'EXIT'"/></log>
		<exit/>
		
	</block>

</form>


</vxml>
