<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">
<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *  FECHA:			5-12-2018
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


	<script src="${pageContext.request.contextPath}/scripts/validaPuerto.js"/>
	
	<script>
		VG_datosLlamada = new DatosLlamada();
		VG_loggerServicio = new LoggerServicio();
		VG_transfer = new Transfer();
		VG_cliente = new Cliente();		
		VG_RetornoCTI=new TransactionCTI();
		// inicializo ID A a false
		VG_cliente.isIdentificado = 'false';
		VG_cliente.isAutenticado = 'false'; 
	</script>
	
<form id="ENRUTADOR_INICIO_PARAMS">
	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - INICIO-PARAMS'"/></log>
		<log label="ENRUTADOR"><value expr="'callId: ' + VG_datosLlamada.callId"/></log>
		<log label="ENRUTADOR"><value expr="'numOrigen: ' + VG_datosLlamada.numOrigen"/></log>
		<log label="ENRUTADOR"><value expr="'numDestino: ' + VG_datosLlamada.numDestino"/></log>
		<log label="ENRUTADOR"><value expr="'puerto: ' + VG_datosLlamada.puerto"/></log>
		<log label="ENRUTADOR"><value expr="'uui: ' + VG_datosLlamada.uui"/></log>
		<log label="ENRUTADOR"><value expr="'cti: ' + VG_datosLlamada.cti"/></log>
		<log label="ENRUTADOR"><value expr="'uri: ' + VG_datosLlamada.uri"/></log>
		
		<!-- 
		***********************************************
		******* INICIALIZACION SERVICIO IVR ***********
		***********************************************
		-->
		
		<!-- ******************************************************************************* -->
		<assign name="VG_servicioIVR" expr="'${codServicio}'"/>
		<log label="ENRUTADOR"><value expr="'VG_servicioIVR: ' + VG_servicioIVR"/></log>
		
		<assign name="VG_esquema" expr="'DIRECTO'"/>
		<log label="ENRUTADOR"><value expr="'VG_esquema: ' + VG_esquema"/></log>
		
		<assign name="VG_segmento" expr="'${segmento}'"/>
		<log label="ENRUTADOR"><value expr="'VG_segmento: ' + VG_segmento"/></log>
		
		<assign name="VG_producto" expr="'DIRECTO'"/>
		<log label="ENRUTADOR"><value expr="'VG_producto: ' + VG_producto"/></log>

		<assign name="VG_tipo" expr="'${tipo}'"/>
		<log label="ENRUTADOR"><value expr="'VG_tipo: ' + VG_tipo"/></log>
		
		<assign name="VG_op" expr="'${op}'"/>
		<log label="ENRUTADOR"><value expr="'VG_op: ' + VG_op"/></log>
		<!-- ******************************************************************************* -->
				
		<!-- Me guardo el segmento de entrada como segmento de transferencia de momento -->
		<assign name="VG_transfer.segmento" expr="VG_segmento"/>
		<log label="ENRUTADOR"><value expr="'lang: ${sessionScope.lang}'"/></log>
		
		<assign name="VG_loggerServicio.idioma" expr="'${sessionScope.lang}'"/>
		
		<!-- 
		***********************************************
		******* INICIALIZACION DATOS DE LOGGER ********
		***********************************************
		-->
		<assign name="VG_loggerServicio.idLlamada" expr="VG_datosLlamada.callId"/>
		<assign name="VG_loggerServicio.idServicio" expr="VG_servicioIVR"/>
		<assign name="VG_loggerServicio.idElemento" expr="'ENRUTADOR'"/>
		<assign name="VG_loggerServicio.numOrigen" expr="VG_datosLlamada.numOrigen"/>
		<assign name="VG_loggerServicio.numDestino" expr="VG_datosLlamada.numDestino"/>
		<assign name="VG_loggerServicio.tipo" expr="VG_tipo"/>
		<assign name="VG_loggerServicio.op" expr="VG_op"/>
		<assign name="VG_loggerServicio.sesAvaya" expr="VG_datosLlamada.sesAvaya"/>
		<assign name="VG_timeInicioLlam" expr="'${milisecInicioLlam}'"/>
	
		<log label="ENRUTADOR"><value expr="'VG_timeInicioLlam: ' + VG_timeInicioLlam"/></log>
		<log label="ENRUTADOR"><value expr="'FIN - INICIO-PARAMS'"/></log>
		
		<submit next="${pageContext.request.contextPath}/getConfigServicio" method="post" namelist="VG_loggerServicio VG_esquema VG_segmento VG_producto VG_stat"/>
	</block>

</form>

</vxml>
