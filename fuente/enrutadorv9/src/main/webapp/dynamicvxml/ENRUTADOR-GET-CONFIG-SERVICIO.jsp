<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">


<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GET-CONFIG-SERVICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>

<form id="ENRUTADOR_GET_CONFIG_SERVICIO">

	<block>				
		<log label="ENRUTADOR"><value expr="'INICIO - GET-CONFIG-SERVICIO'"/></log>
		
		<!-- ABALFARO_20170505 subimos la traza por si el idLlamada se ha generado -->
		<assign name="VG_loggerServicio.idLlamada" expr="'${idLlamada}'"/>		
		<log label="ENRUTADOR"><value expr="'VG_loggerServicio.idLlamada: ' + VG_loggerServicio.idLlamada"/></log>
		
		
		<!-- ABALFARO_20170217 -->
		<!-- ***** STAT: inicio llamada ***** -->
		<assign name="VG_stat" expr="inicioLlamada(VG_loggerServicio.idLlamada, VG_timeInicioLlam, VG_loggerServicio.idServicio, 
 													VG_loggerServicio.numOrigen, VG_loggerServicio.numDestino)"/> 
		
		<assign name="VG_stat" expr="inicioOperacion(VG_stat, VG_timeInicioLlam, 'CALL')"/> 
		<!-- ***** STAT: inicio llamada ***** -->
		
		<log label="ENRUTADOR"><value expr="'VG_stat.codLlamIVR: ' + VG_stat.codLlamIVR"/></log>
		<log label="ENRUTADOR"><value expr="'VG_stat.codServIVR: ' + VG_stat.codServIVR"/></log>
		<log label="ENRUTADOR"><value expr="'VG_stat.timeEntradaMilis: ' + VG_stat.timeEntradaMilis"/></log>
		
		<!-- ABALFARO_20170315 -->
		<!-- ***** STAT: aniade contador ***** -->
<!-- 		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'CALL_IN', 'INFO', '', 'IN', VG_segmento)"/>  -->
		<!-- ***** STAT: aniade contador ***** -->
				
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>	
		<assign name="VG_error" expr="'${error}'"/>
		
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'VG_error: ' + VG_error"/></log>
				
		<if cond="PRM_resultadoOperacion != 'OK'">
			<!-- algo ha fallado en la ejecucion del servlet -->
			<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>		
			
		<else/>
		
			<assign name="VG_servicioCtiActivo" expr="'${ctiActivo}'"/>
			<assign name="VG_servicioVdnTransferDef" expr="'${vdnTransferDef}'"/>
			<assign name="VG_servicioStatCmActivo" expr="'${statCmActivo}'"/>
						
					
			<log label="ENRUTADOR"><value expr="'VG_servicioCtiActivo: ' + VG_servicioCtiActivo"/></log>
			<log label="ENRUTADOR"><value expr="'VG_servicioVdnTransferDef: ' + VG_servicioVdnTransferDef"/></log>
			<log label="ENRUTADOR"><value expr="'VG_servicioStatCmActivo: ' + VG_servicioStatCmActivo"/></log>
							
			<log label="ENRUTADOR"><value expr="'FIN - GET-CONFIG-SERVICIO'"/></log>
			
			<if cond="VG_codigoRetorno == 'OK'">

				<submit next="${pageContext.request.contextPath}/gestionInicioLlamada" method="post" namelist="VG_loggerServicio VG_servicioCtiActivo"/>
						
			<else/>
			
				<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>
			
			</if>
		
		</if>		
	</block>
</form>

</vxml>
