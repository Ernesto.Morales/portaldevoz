<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion" expr="''"/>
<var name="SUB_error" expr="''"/>
<var name="SUB_duracion" expr="''"/>


<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'CUELGUE - SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_duracion"/>
		
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'ERROR - SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'ERROR'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>		
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_duracion"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_ENRUTADOR_TRACEAR_FIN_LLAMADA">
	
	
		<block>				
			<log label="ENRUTADOR"><value expr="'INICIO - SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA'"/></log>
			
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_duracion" expr="'${duracion}'"/>	
			
			<log label="ENRUTADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'duracion: ' + SUB_duracion"/></log>

			<log label="ENRUTADOR"><value expr="'FIN - SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA'"/></log>
			
			<return namelist="SUB_resultadoOperacion SUB_error SUB_duracion"/>			
			
		</block>
	</form>

</vxml>

