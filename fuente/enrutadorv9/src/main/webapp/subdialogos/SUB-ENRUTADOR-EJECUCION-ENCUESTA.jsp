<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-EJECUCION-ENCUESTA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/scripts/Cliente.js"/>


<var name="VG_loggerServicio" expr="''"/>
<var name="PRM_cliente"/>
<var name="PRM_datosCTI"/>
<var name="PRM_datosUUI"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>


<var name="PRM_idEncuesta"/>
<var name="PRM_encuestaCompleta"/>
<var name="PRM_respuestaEncuesta"/>
<var name="PRM_activa"/>
<var name="PRM_dentroHorario"/>



<var name="PRM_ruta" expr="''"/>
<var name="PRM_idServicioEncuesta" expr="''"/>

<!-- PARAMETROS DE ENTRADA AL ENCUESTA -->
<var name="PRM_IN_ENCUESTA_nombre" expr="''"/>
<var name="PRM_IN_ENCUESTA_ruta" expr="''"/>
<var name="PRM_IN_ENCUESTA_isActivo" expr="''"/>
<var name="PRM_IN_ENCUESTA_isActivoDefecto" expr="''"/>
<var name="PRM_IN_ENCUESTA_cliente" expr="''"/>
<!-- ABALFARO_20170516 -->
<var name="PRM_IN_ENCUESTA_contextoEnrutador" expr="''"/>

<!-- PARAMETROS DE SALIDA DEL ENCUESTA -->
<var name="PRM_OUT_ENCUESTA_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_ENCUESTA_codigoRetorno" expr="''"/>
<var name="PRM_OUT_ENCUESTA_error" expr="''"/>
<var name="PRM_OUT_ENCUESTA_cliente" expr="''"/>
<var name="PRM_OUT_ENCUESTA_activa" expr="''"/>
<var name="PRM_OUT_ENCUESTA_dentroHorario" expr="''"/>


	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'Catch de cuelgue en SUB-ENRUTADOR-EJECUCION-ENCUESTA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'Catch de error en SUB-ENRUTADOR-EJECUCION-ENCUESTA'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente" />		
		
	</catch>


<form id="ENRUTADOR_EJECUCION_ENCUESTA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_datosCTI"/>
	<var name="PRM_IN_datosUUI"/>

	<block>				
		<log label="ENRUTADOR"><value expr="'INICIO - EJECUCION-ENCUESTA'"/></log>
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_datosCTI" expr="PRM_IN_datosCTI"/>
		<assign name="PRM_datosUUI" expr="PRM_IN_datosUUI"/>
		
		<goto next="#SUB_EJECUTAR_ENCUESTA" />
		
	</block>

</form>


<form id="SUB_EJECUTAR_ENCUESTA">

	<block>
		<log label="ENRUTADOR"><value expr="'VG_loggerServicio.idServicio: ' + VG_loggerServicio.idServicio"/></log>
		
		<!-- ABALFARO_20170516 enviamos el idServicio con la version de la encuesta -->
		<assign name="PRM_idServicioEncuesta" expr="'${idServicioEncuesta}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_idServicioEncuesta: ' + PRM_idServicioEncuesta"/></log>
<!-- 		<assign name="PRM_idServicioEncuesta" expr="'ENCUESTAIRENE'"/> -->
		
		<assign name="PRM_IN_ENCUESTA_nombre" expr="'${nombreEncuesta}'"/>
		
		<assign name="PRM_cliente.encuesta.idEncuesta" expr="PRM_IN_ENCUESTA_nombre"/>
		
		<assign name="PRM_IN_ENCUESTA_ruta" expr="'${rutaEncuesta}'"/>
		<assign name="PRM_IN_ENCUESTA_isActivo" expr="'${encuestaActiva}'"/>
		<assign name="PRM_IN_ENCUESTA_isActivoDefecto" expr="'${encuestaActivaDefecto}'"/>
		<assign name="PRM_IN_ENCUESTA_cliente" expr="PRM_cliente"/>
		
		

		<log label="ENRUTADOR"><value expr="'PRM_IN_ENCUESTA_nombre: ' + PRM_IN_ENCUESTA_nombre"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_IN_ENCUESTA_ruta: ' + PRM_IN_ENCUESTA_ruta"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_IN_ENCUESTA_isActivo: ' + PRM_IN_ENCUESTA_isActivo"/></log>
						
		<assign name="PRM_ruta" expr="'${pageContext.request.contextPath}' + PRM_IN_ENCUESTA_ruta"/>
		
		<log label="ENRUTADOR"><value expr="'PRM_ruta: ' + PRM_ruta"/></log>
		
		<!-- ABALFARO_20170516 -->
		<assign name="PRM_IN_ENCUESTA_contextoEnrutador" expr="'${pageContext.request.contextPath}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_IN_ENCUESTA_contextoEnrutador: ' + PRM_IN_ENCUESTA_contextoEnrutador"/></log>
				
	</block>
		
	<subdialog name="subEjecutarEncuesta" srcexpr="PRM_ruta" method="post"
			namelist="VG_datosLlamada VG_loggerServicio PRM_IN_ENCUESTA_nombre PRM_IN_ENCUESTA_ruta PRM_IN_ENCUESTA_isActivo 
			PRM_IN_ENCUESTA_isActivoDefecto PRM_idServicioEncuesta PRM_IN_ENCUESTA_contextoEnrutador">
	
		<param name="PRM_IN_ENCUESTA_loggerServicio" expr="VG_loggerServicio"/>
		
		<param name="PRM_IN_ENCUESTA_nombre" expr="PRM_IN_ENCUESTA_nombre"/>
		
		<param name="PRM_IN_ENCUESTA_cliente" expr="PRM_cliente"/>
		
		<param name="PRM_IN_ENCUESTA_datosCTI" expr="PRM_datosCTI"/>
		
		<param name="PRM_IN_ENCUESTA_datosUUI" expr="PRM_datosUUI"/>
		

				
		
		<filled>
			<log label="ENRUTADOR"><value expr="'HE VUELTO A ENRUTADOR-EJECUCION-ENCUESTA'"/></log>
			
			<assign name="PRM_OUT_ENCUESTA_resultadoOperacion" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_resultadoOperacion"/>
			<assign name="PRM_OUT_ENCUESTA_codigoRetorno" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_codigoRetorno"/>
			<assign name="PRM_OUT_ENCUESTA_error" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_error"/>
			<assign name="PRM_OUT_ENCUESTA_cliente" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_cliente"/>
			<assign name="PRM_OUT_ENCUESTA_activa" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_activa"/>
			<assign name="PRM_OUT_ENCUESTA_dentroHorario" expr="subEjecutarEncuesta.PRM_OUT_ENCUESTA_dentroHorario"/>
			
						
			<log label="ENRUTADOR"><value expr="'PRM_OUT_ENCUESTA_resultadoOperacion: ' + PRM_OUT_ENCUESTA_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_ENCUESTA_codigoRetorno: ' + PRM_OUT_ENCUESTA_codigoRetorno"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_ENCUESTA_error: ' + PRM_OUT_ENCUESTA_error"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_ENCUESTA_activa: ' + PRM_OUT_ENCUESTA_activa"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_ENCUESTA_dentroHorario: ' + PRM_OUT_ENCUESTA_dentroHorario"/></log>

						
			<assign name="PRM_resultadoOperacion" expr="PRM_OUT_ENCUESTA_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="PRM_OUT_ENCUESTA_codigoRetorno"/>
			<assign name="PRM_error" expr="PRM_OUT_ENCUESTA_error"/>
			<assign name="PRM_cliente" expr="PRM_OUT_ENCUESTA_cliente"/>
			<assign name="PRM_activa" expr="PRM_OUT_ENCUESTA_activa"/>
			<assign name="PRM_dentroHorario" expr="PRM_OUT_ENCUESTA_dentroHorario"/>

			
					
			<log label="ENRUTADOR"><value expr="'FIN - EJECUCION-ENCUESTA'"/></log>
			
			<goto next="#SUB_ESCRIBIR_RESULTADO"/>
			
		</filled>	
		
	</subdialog>		

</form>

<form id="SUB_ESCRIBIR_RESULTADO">

	<block>
	
		<log label="ENRUTADOR"><value expr="'Vamos a escribir el resultado de la Encuesta'"/></log>
		
		<assign name="PRM_idEncuesta" expr="PRM_cliente.encuesta.idEncuesta"/>
		<assign name="PRM_encuestaCompleta" expr="PRM_cliente.encuesta.encuestaCompleta"/>
		<assign name="PRM_respuestaEncuesta" expr="'['"/>
		
		<if	cond="PRM_cliente.encuesta.respuestaEncuesta.length != '0' ">
			<foreach item="pregunta" array="PRM_cliente.encuesta.respuestaEncuesta">
			
				<assign name="PRM_respuestaEncuesta" expr="PRM_respuestaEncuesta + '[' + pregunta.numPregunta +
					':' + pregunta.tipoRespuesta + ',' + pregunta.respuestaCliente + ']'"/>
				
				<log label="ENRUTADOR"><value expr="' ENCUESTA - NumPregunta: ' + pregunta.numPregunta +
				' - TipoRespuesta: ' + pregunta.tipoRespuesta +
				' - RespuestaCliente: ' + pregunta.respuestaCliente"/></log>					
			</foreach>
		<else/>
			<log label="ENRUTADOR"><value expr="' ENCUESTA - NO HAY DATOS'"/></log>			
		</if>
		
		<assign name="PRM_respuestaEncuesta" expr="PRM_respuestaEncuesta + ']'"/>

	</block>
	
	<subdialog name="subGestionSalidaEncuesta" src="${pageContext.request.contextPath}/subGestionSalidaEncuesta" method="post" 
			namelist="VG_loggerServicio PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_idEncuesta PRM_respuestaEncuesta 
				PRM_encuestaCompleta PRM_activa PRM_dentroHorario">
	
		<filled>
			<log label="ENRUTADOR"><value expr="'HE VUELTO A ENRUTADOR-EJECUCION-ENCUESTA'"/></log>
			
			<assign name="PRM_resultadoOperacion" expr="subGestionSalidaEncuesta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subGestionSalidaEncuesta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subGestionSalidaEncuesta.PRM_error"/>
			
			<goto next="#FIN"/>
		</filled>
		
	</subdialog>		
		
</form>	

<form id="FIN">

	<block>
		<log label="ENRUTADOR"><value expr="'Fin de SUB-ENRUTADOR-EJECUCION-ENCUESTA'"/></log>
		
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
	
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente" />
	</block>
</form>	
	


</vxml>
