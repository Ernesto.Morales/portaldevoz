<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-GET-DATOS-CTI
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/scripts/DatosCTI.js"/>
<script src="${pageContext.request.contextPath}/scripts/DatosUUI.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>

<var name="SUB_datosCTI"/>
<var name="SUB_datosUUI"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'CUELGUE - SUB-ENRUTADOR-GET-DATOS-CTI'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_datosCTI SUB_datosUUI"/>
		
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'ERROR - SUB-ENRUTADOR-GET-DATOS-CTI'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>		
		
				<return namelist="SUB_resultadoOperacion SUB_error SUB_datosCTI SUB_datosUUI"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_ENRUTADOR_GET_DATOS_CTI">
	
	
		<block>				
			<log label="ENRUTADOR"><value expr="'INICIO - SUB-ENRUTADOR-GET-DATOS-CTI'"/></log>
			
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>	
			
			<log label="ENRUTADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error: ' + SUB_error"/></log>
			
			<script>
				SUB_datosCTI = new DatosCTI();
				SUB_datosUUI = new DatosUUI();
			</script>
			
			<goto next="#ANALIZA_RESULTADO_CTI"/>	
		
		</block>
	
	</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
	<form id="ANALIZA_RESULTADO_CTI">
	
		<block>				
						
			<if cond="SUB_resultadoOperacion == 'OK'">
				<!-- Guardo la info obtenida -->
				<assign name="SUB_datosCTI.ani" expr="'${datosCti.ani}'"/>
				<assign name="SUB_datosCTI.dnis" expr="'${datosCti.dnis}'"/>
				<assign name="SUB_datosCTI.numeroIvr" expr="'${datosCti.numeroIvr}'"/>
				<assign name="SUB_datosCTI.puertoIvr" expr="'${datosCti.puertoIvr}'"/>
				<assign name="SUB_datosCTI.llave" expr="'${datosCti.llave}'"/>
				<assign name="SUB_datosCTI.field" expr="'${datosCti.field}'"/>
				<assign name="SUB_datosCTI.vdnOrigen" expr="'${datosCti.vdnOrigen}'"/>
				<assign name="SUB_datosCTI.vdnDestino" expr="'${datosCti.vdnDestino}'"/>
				<assign name="SUB_datosCTI.skill" expr="'${datosCti.skill}'"/>
				<assign name="SUB_datosCTI.asa" expr="'${datosCti.asa}'"/>
				<assign name="SUB_datosCTI.idAplicacion" expr="'${datosCti.idAplicacion}'"/>
				<assign name="SUB_datosCTI.xferId" expr="'${datosCti.xferId}'"/>
				<assign name="SUB_datosCTI.segmentoCliente" expr="'${datosCti.segmentoCliente}'"/>
				<assign name="SUB_datosCTI.producto" expr="'${datosCti.producto}'"/>
				<assign name="SUB_datosCTI.llaveAccesoFront" expr="'${datosCti.llaveAccesoFront}'"/>
				<assign name="SUB_datosCTI.campania" expr="'${datosCti.campania}'"/>
				<assign name="SUB_datosCTI.canal" expr="'${datosCti.canal}'"/>
				<assign name="SUB_datosCTI.numCliente" expr="'${datosCti.numCliente}'"/>
				<assign name="SUB_datosCTI.sector" expr="'${datosCti.sector}'"/>
				<assign name="SUB_datosCTI.nombreCliente" expr="'${datosCti.nombreCliente}'"/>
				<assign name="SUB_datosCTI.telefonoContacto" expr="'${datosCti.telefonoContacto}'"/>
				<assign name="SUB_datosCTI.telefonoDinamico" expr="'${datosCti.telefonoDinamico}'"/>
				<assign name="SUB_datosCTI.libre1" expr="'${datosCti.libre1}'"/>
				<assign name="SUB_datosCTI.libre2" expr="'${datosCti.libre2}'"/>
				<assign name="SUB_datosCTI.usuarioFront" expr="'${datosCti.usuarioFront}'"/>
				<assign name="SUB_datosCTI.transferenciaAsesor" expr="'${datosCti.transferenciaAsesor}'"/>
				<assign name="SUB_datosCTI.transferenciaIvr" expr="'${datosCti.transferenciaIvr}'"/>
				<assign name="SUB_datosCTI.numeroSesion" expr="'${datosCti.numeroSesion}'"/>
				<assign name="SUB_datosCTI.tiempoLlamada" expr="'${datosCti.tiempoLlamada}'"/>
				<assign name="SUB_datosCTI.llaveSegmento" expr="'${datosCti.llaveSegmento}'"/>
				<assign name="SUB_datosCTI.ipAsesor" expr="'${datosCti.ipAsesor}'"/>
				<assign name="SUB_datosCTI.libre3" expr="'${datosCti.libre3}'"/>
				<assign name="SUB_datosCTI.telCtePersonas" expr="'${datosCti.telCtePersonas}'"/>
				<assign name="SUB_datosCTI.metodoAutenticacion" expr="'${datosCti.metodoAutenticacion}'"/>
				<assign name="SUB_datosCTI.libre4" expr="'${datosCti.libre4}'"/>
				<assign name="SUB_datosCTI.timestamp" expr="'${datosCti.timestamp}'"/>
				<assign name="SUB_datosCTI.comentario" expr="'${datosCti.comentario}'"/>
					
				<log label="ENRUTADOR"><value expr="'SUB_datosCTI.vdnOrigen: ' + SUB_datosCTI.vdnOrigen"/></log>
				<log label="ENRUTADOR"><value expr="'SUB_datosCTI.xferId: ' +  SUB_datosCTI.xferId"/></log>
				
		
			</if>
			
			<goto next="#ANALIZA_RESULTADO_UUI"/>
			
		</block>
	
	</form>

	<form id="ANALIZA_RESULTADO_UUI">
	
		<block>				
						
			<if cond="SUB_resultadoOperacion == 'OK'">
			
				<!-- Guardo la info obtenida -->
				<assign name="SUB_datosUUI.llave" expr="'${datosUUI.llave}'"/>
				<assign name="SUB_datosUUI.llaveSegmento" expr="'${datosUUI.llaveSegmento}'"/>
				<assign name="SUB_datosUUI.xferId" expr="'${datosUUI.xferId}'"/>
				<assign name="SUB_datosUUI.segmentoCliente" expr="'${datosUUI.segmentoCliente}'"/>
				<assign name="SUB_datosUUI.canal" expr="'${datosUUI.canal}'"/>
				<assign name="SUB_datosUUI.numCliente" expr="'${datosUUI.numCliente}'"/>
				<assign name="SUB_datosUUI.llaveAccesoFront" expr="'${datosUUI.llaveAccesoFront}'"/>
				<assign name="SUB_datosUUI.metodoAutenticacion" expr="'${datosUUI.metodoAutenticacion}'"/>
				<assign name="SUB_datosUUI.numeroSesion" expr="'${datosUUI.numeroSesion}'"/>
				<assign name="SUB_datosUUI.campania" expr="'${datosUUI.campania}'"/>
					
			</if>
						
			<log label="ENRUTADOR"><value expr="'FIN - SUB-ENRUTADOR-GET-DATOS-CTI'"/></log>
			
			
			<return namelist="SUB_resultadoOperacion SUB_error SUB_datosCTI SUB_datosUUI"/>
			
			
		</block>
	</form>

</vxml>

