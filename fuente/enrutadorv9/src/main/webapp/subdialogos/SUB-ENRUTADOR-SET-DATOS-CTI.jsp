<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-SET-DATOS-CTI
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/scripts/DatosCTI.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="SUB_destino"/>
<var name="SUB_uui"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'CUELGUE - SUB-ENRUTADOR-SET-DATOS-CTI'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_destino SUB_uui"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'ERROR - SUB-ENRUTADOR-SET-DATOS-CTI'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_destino SUB_uui"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_ENRUTADOR_SET_DATOS_CTI">
	
	
		<block>				
			<log label="ENRUTADOR"><value expr="'INICIO - SUB-ENRUTADOR-SET-DATOS-CTI'"/></log>
			
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_destino" expr="'${destino}'"/>	
			<assign name="SUB_uui" expr="'${uui}'"/>
						
			<goto next="#ANALIZA_RESULTADO"/>	
		
		</block>
	
	</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
	<form id="ANALIZA_RESULTADO">
	
		<block>				
						
			<log label="ENRUTADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error: ' + SUB_error"/></log>
			<log label="ENRUTADOR"><value expr="'destino: ' + SUB_destino"/></log>
			<log label="ENRUTADOR"><value expr="'uui: ' + SUB_uui"/></log>
			
			<log label="ENRUTADOR"><value expr="'FIN - SUB-ENRUTADOR-SET-DATOS-CTI'"/></log>
			
			
			<return namelist="SUB_resultadoOperacion SUB_error SUB_destino SUB_uui"/>
			
		</block>
	
	</form>


</vxml>

