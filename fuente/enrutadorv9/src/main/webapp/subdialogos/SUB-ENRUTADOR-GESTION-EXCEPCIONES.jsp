<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-GESTION-EXCEPCIONES
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="SUB_codigoRetorno"/>
<var name="SUB_estadistica"/>
<var name="SUB_modoLocucion"/>
<var name="SUB_locucion"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'CUELGUE - SUB-ENRUTADOR-GESTION-EXCEPCIONES'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_codigoRetorno" expr="'HANGUP'"/>		
		<assign name="SUB_error" expr="''"/>	
		
		<return namelist="SUB_codigoRetorno SUB_error SUB_resultadoOperacion"/>		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'ERROR - SUB-ENRUTADOR-GESTION-EXCEPCIONES'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_codigoRetorno" expr="'ERROR'"/>		
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>	
		
		<return namelist="SUB_codigoRetorno SUB_error SUB_resultadoOperacion"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_ENRUTADOR_GESTION_EXCEPCIONES">
	
	
		<block>				
			<log label="ENRUTADOR"><value expr="'INICIO - SUB-ENRUTADOR-GESTION-EXCEPCIONES'"/></log>
			
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			
			<if cond="SUB_resultadoOperacion == 'KO'">
				<log label="ENRUTADOR"><value expr="'FIN - SUB-ENRUTADOR-GESTION-EXCEPCIONES'"/></log>
				<assign name="SUB_codigoRetorno" expr="'FIN'"/>
				<return namelist="SUB_codigoRetorno SUB_error SUB_resultadoOperacion"/>
			</if>
			
			<assign name="SUB_codigoRetorno" expr="'${excepcion.codigoRetorno}'"/>	
			<assign name="SUB_estadistica" expr="'${excepcion.estadistica}'"/>	
			<assign name="SUB_modoLocucion" expr="'${excepcion.prompt.modo}'"/>	
			<assign name="SUB_locucion" expr="'${excepcion.prompt.valuePrompt}'"/>	
			
			<log label="ENRUTADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'error: ' + SUB_error"/></log>
			<log label="ENRUTADOR"><value expr="'codigoRetorno: ' + SUB_codigoRetorno"/></log>
			<log label="ENRUTADOR"><value expr="'estadistica: ' + SUB_estadistica"/></log>
			<log label="ENRUTADOR"><value expr="'modoLocucion: ' + SUB_modoLocucion"/></log>
			<log label="ENRUTADOR"><value expr="'locucion: ' + SUB_locucion"/></log>
			
			<goto next="#ANALIZA_RESULTADO"/>	
		
		</block>
	
	</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
	<form id="ANALIZA_RESULTADO">
	
		<block>				
		
			<if cond="SUB_resultadoOperacion == 'OK'">
		
				<!-- reproduzco la locucion de la excepcion -->
				<if cond="SUB_modoLocucion == 'WAV'">
					<prompt bargein="false">
						<audio expr="SUB_locucion"/>
					</prompt>			
				<else/>
					<!-- es TTS -->
					<prompt bargein="false">
						<value expr="SUB_locucion"/>
					</prompt>
				</if>

			</if>
		
			
			<log label="ENRUTADOR"><value expr="'FIN - SUB-ENRUTADOR-GESTION-EXCEPCIONES'"/></log>
			
			
			<return namelist="SUB_codigoRetorno SUB_error SUB_resultadoOperacion"/>
			
		</block>
	
	</form>


</vxml>

