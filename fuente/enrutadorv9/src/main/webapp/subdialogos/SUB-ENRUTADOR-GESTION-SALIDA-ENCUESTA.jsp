<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-ENRUTADOR-GESTION-SALIDA-ENCUESTA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<var name="PRM_idEncuesta"/>
<var name="PRM_encuestaCompleta"/>
<var name="PRM_respuestasEncuesta"/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'Catch de cuelgue en SUB-ENRUTADOR-GESTION-SALIDA-ENCUESTA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="ENRUTADOR"><value expr="'Catch de error en SUB-ENRUTADOR-GESTION-SALIDA-ENCUESTA'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />		
		
	</catch>


<form id="ENRUTADOR_GESTION_SALIDA_ENCUESTA">

	<block>				
		<log label="ENRUTADOR"><value expr="'INICIO - GESTION-SALIDA-ENCUESTA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		
		<assign name="PRM_idEncuesta" expr="'${idEncuesta}'"/>
		<assign name="PRM_encuestaCompleta" expr="'${encuestaCompleta}'"/>
		<assign name="PRM_respuestasEncuesta" expr="'${respuestasEncuesta}'"/>
		
		<goto next="#FIN" />
		
	</block>

</form>


<form id="FIN">

	<block>
		<log label="ENRUTADOR"><value expr="'Fin de SUB-ENRUTADOR-GESTION-SALIDA-ENCUESTA'"/></log>
		
		<log label="ENRUTADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
		
		<log label="ENRUTADOR"><value expr="'PRM_idEncuesta: ' + PRM_idEncuesta"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_encuestaCompleta: ' + PRM_encuestaCompleta"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_respuestasEncuesta: ' + PRM_respuestasEncuesta"/></log>
	
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />
	</block>
</form>	
	


</vxml>
