<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-INICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->


	<meta http-equiv="Expires" content="0"/>
	<property name="fetchaudio" value="${pageContext.request.contextPath}/audio/SPA-silence.wav"/>
	
	
	<script src="${pageContext.request.contextPath}/scripts/BeanServicio.js"/>
	
	<script src="${pageContext.request.contextPath}/scripts/DatosLlamada.js"/>
	<script src="${pageContext.request.contextPath}/scripts/LoggerServicio.js"/>
	<script src="${pageContext.request.contextPath}/scripts/Transfer.js"/>
	<script src="${pageContext.request.contextPath}/scripts/Cliente.js"/>
	
	<script src="${pageContext.request.contextPath}/scripts/DatosCTI.js"/>
	<script src="${pageContext.request.contextPath}/scripts/DatosUUI.js"/>
	
	<script src="${pageContext.request.contextPath}/scripts/BeanInfoControlador.js"/>
	<script src="${pageContext.request.contextPath}/scripts/TransactionCTI.js"/>
	<!-- Cuadro de mando - STAT -->
	<script src="${pageContext.request.contextPath}/scripts/Stat.js"/>
	<script src="${pageContext.request.contextPath}/scripts/FuncionesStat.js"/>
	
	<!-- 
	****************************************************
	********* DEFINICION DE VARIABLES GLOBALES *********
	****************************************************
	-->
	<var name="VG_datosLlamada" expr="''"/>
		
	<var name="VG_resultadoOperacion" expr="''"/>
	<var name="VG_codigoRetorno" expr="''"/>
	<var name="VG_error" expr="''"/>
	
	<var name="VG_infoControlador" expr="''"/>
	
	<var name="VG_servicioIVR" expr="''"/>
	
	<!-- Caracteristicas para efectuar la transferencia -->
	<var name="VG_transfer" expr="''"/>
	
	<var name="VG_cliente" expr="''"/>
	
	<!-- Elementos de transferencia -->
	<var name="VG_esquema" expr="''"/>
	<var name="VG_segmento" expr="''"/>
	<var name="VG_producto" expr="''"/>

	<var name="VG_tipo" expr="''"/>
	<var name="VG_op" expr="''"/>
	
	<!-- Operativas que se han ido ejecutando a lo largo de la llamada -->
	<var name="VG_operativasIVR" expr="''"/>
	
	<!-- Guarda si el servicio IVR tiene activo cti en su configuracion -->
	<var name="VG_servicioCtiActivo" expr="''"/>
	<!-- Guarda el vdn de transferencia por defecto en el servicio IVR -->
	<var name="VG_servicioVdnTransferDef" expr="''"/>
	<!-- Guarda si el servicio IVR tiene activo el cuadro de mando en su configuracion -->
	<var name="VG_servicioStatCmActivo" expr="''"/>
	
	
	<!-- elemento de identif del cliente -->
	<var name="VG_llaveAccesoFront" expr="''"/>
	
	<!-- Controla que la gestion de excepciones no se quede en bucle -->
	<var name="VG_gestionExcepcionesPrevia" expr="'NO'"/>
	
	<!-- Marcar que se va a finalizar la llamada -->
	<var name="VG_procederFinLlamada" expr="'NO'"/>
	
	<!--
	****************************************************
	********* DEFINICION DE VARIABLES LOGGER ***********
	****************************************************
	-->
	<var name="VG_loggerServicio" expr="''"/>

	<!-- milisegundos del inicio de la llamada -->
	<var name="VG_timeInicioLlam" expr="''"/>

	<!-- 
	****************************************************
	*** DEFINICION DE VARIABLES STAT-CUADRO MANDO ******
	****************************************************
	-->
	<!-- ABALFARO_20170217 -->
	<var name="VG_stat" expr="''"/>
	
	<!--
	****************************************************
	********* DEFINICIoN DE PROPIEDADES CTI ************
	****************************************************
	-->
	
	<var name="VG_datosCTI" expr="''"/>
	<!--
	****************************************************
	********* DEFINICIoN DE PROPIEDADES RETORNO CTI ************
	****************************************************
	-->
	
	<var name="VG_RetornoCTI" expr="''"/>
		
	<!--
	****************************************************
	********* DEFINICIoN DE PROPIEDADES UUI ************
	****************************************************
	-->	
	
	<var name="VG_datosUUI" expr="''"/>
		

	<!-- 
	****************************************************
	** DEFINICIoN DE PROPIEDADES GLOBALES DEL SERVICIO *
	****************************************************
	-->

	<property name="inputmodes" value="dtmf voice"/>
	<property name="termchar" value=""/>
		

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="ENRUTADOR"><value expr="'Catch de cuelgue en INICIO-ENRUTADOR'"/></log>
		
		<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
		<assign name="VG_error" expr="''"/>
		
		<if cond="VG_gestionExcepcionesPrevia == 'SI'">	
			
			<!-- ABALFARO_20170117 evitamos volver a pintar el endcall si el cliente cuelga tras escribirlo -->
			<if cond="VG_procederFinLlamada == 'SI'">		
				<!-- ya se procedio a finalizar la llamada y se pinto el endcall -->
				<log label="ENRUTADOR"><value expr="'FIN LLAMADA- ENRUTADOR-INICIO'"/></log>
				<!-- finalizo la llamada -->
				<exit/>
			</if>
			<!-- marco que se procede a finalizar la llamada -->
			<assign name="VG_procederFinLlamada" expr="'SI'"/>
			<!-- ABALFARO_20170117 -->
			
			
			<!-- ***** STAT: fin llamada ***** -->
			<assign name="VG_stat" expr="finLlamada(VG_stat, '', VG_codigoRetorno)"/> 
			<!-- ***** STAT: fin llamada ***** -->
			
			
			<log label="ENRUTADOR"><value expr="'VG_stat.timeSalida: ' + VG_stat.timeSalida"/></log>
			<log label="ENRUTADOR"><value expr="'VG_stat.codLlamIVR: ' + VG_stat.codLlamIVR"/></log>
			<log label="ENRUTADOR"><value expr="'VG_stat.codServIVR: ' + VG_stat.codServIVR"/></log>
			
			<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"
				namelist="VG_loggerServicio"/>
<%-- 			<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"  --%>
<!-- 				namelist="VG_loggerServicio VG_codigoRetorno VG_timeInicioLlam VG_transfer VG_datosUUI VG_servicioCtiActivo"/> -->
		<else/>
			<submit next="${pageContext.request.contextPath}/gestionSalida" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>
		</if>
		
		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="ENRUTADOR"><value expr="'Catch de error en INICIO-ENRUTADOR'"/></log>
		
		<log label="ENRUTADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="ENRUTADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		
		<assign name="VG_codigoRetorno" expr="'ERROR'"/>
		<assign name="VG_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		
		<if cond="VG_gestionExcepcionesPrevia == 'SI'">		
			
			<!-- ABALFARO_20170117 evitamos volver a pintar el endcall si se produce un error tras escribirlo -->
			<if cond="VG_procederFinLlamada == 'SI'">		
				<!-- ya se procedio a finalizar la llamada y se pinto el endcall -->
				<log label="ENRUTADOR"><value expr="'FIN LLAMADA- ENRUTADOR-INICIO'"/></log>
				<!-- finalizo la llamada -->
				<exit/>
			</if>
			<!-- marco que se procede a finalizar la llamada -->
			<assign name="VG_procederFinLlamada" expr="'SI'"/>
			<!-- ABALFARO_20170117 -->
			
			
			<!-- ***** STAT: fin llamada ***** -->
			<assign name="VG_stat" expr="finLlamada(VG_stat, '', VG_codigoRetorno)"/> 
			<!-- ***** STAT: fin llamada ***** -->
			
			
			<log label="ENRUTADOR"><value expr="'VG_stat.timeSalida: ' + VG_stat.timeSalida"/></log>
			<log label="ENRUTADOR"><value expr="'VG_stat.codLlamIVR: ' + VG_stat.codLlamIVR"/></log>
			<log label="ENRUTADOR"><value expr="'VG_stat.codServIVR: ' + VG_stat.codServIVR"/></log>
		
			<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"
				namelist="VG_loggerServicio"/>
<%-- 			<submit next="${pageContext.request.contextPath}/gestionFinLlamada" method="post"  --%>
<!-- 				namelist="VG_loggerServicio VG_codigoRetorno VG_timeInicioLlam VG_transfer VG_datosUUI VG_servicioCtiActivo"/> -->
		<else/>
		
			<!-- volvemos a asignar a si, por si no se asigno antes -->
			<assign name="VG_gestionExcepcionesPrevia" expr="'SI'"/>
				
			<submit next="${pageContext.request.contextPath}/gestionSalida" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>
		</if>
	
	</catch>

	<form id="ENRUTADOR_INICIO">
		<block>
			<!-- esto NO se ejecuta, siempre se empezara en la pagina INICIO-PARAMS -->
			<submit next="${pageContext.request.contextPath}/inicioParams" namelist=""/>		 
		</block>	
	</form>

</vxml>





