<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-GESTION-INICIO-LLAMADA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="PRM_vdnOrigen" expr="''"/>
<var name="PRM_xferId" expr="''"/>


<form id="ENRUTADOR_GESTION_INICIO_LLAMADA">

	<block>				
		<log label="ENRUTADOR"><value expr="'INICIO - GESTION-INICIO-LLAMADA'"/></log>
		
		<!-- Compruebo si esta activo cti -->				
		<log label="ENRUTADOR"><value expr="'CTI servicio: ' + VG_servicioCtiActivo"/></log>
		<if cond="VG_servicioCtiActivo == 'ON'">		
					
			<goto next="#SUB_GET_DATOS_CTI" />
						
		<else/>
<!-- 		<goto next="#SUB_GET_DATOS_CTI" /> -->
			<log label="ENRUTADOR"><value expr="'FIN - GESTION-INICIO-LLAMADA'"/></log>
						
			<!-- OLD: si no esta activo el cti inicializamos el vdnOrigen con el numero destino -->
<!--		<assign name="PRM_vdnOrigen" expr="VG_loggerServicio.numDestino"/>			 -->

			<!-- NUEVO para Transferencias: si no esta activo el cti obtendremos el controlador por el segmento -->
			<assign name="PRM_vdnOrigen" expr="VG_segmento"/>	
			<assign name="PRM_xferId" expr="''"/>	
						
			<!-- TODO en este caso VG_datosCTI no se ha recuperado, controlar esto -->
<%-- 			<submit next="${pageContext.request.contextPath}/gestionPrincipal" method="post" namelist="VG_loggerServicio PRM_vdnOrigen PRM_xferId VG_stat"/> --%>
			<goto next="#ENRUTADOR_COMPROBAR_ENCUESTA_DIRECTA" />
		</if>	
	</block>

</form>

<form id="SUB_GET_DATOS_CTI">

	<block>
	
	</block>
	
	<subdialog name="subGetDatosCti" src="${pageContext.request.contextPath}/subGetDatosCti" method="post" namelist="VG_loggerServicio VG_datosLlamada">
			
		<filled>
		
			<assign name="VG_resultadoOperacion" expr="subGetDatosCti.SUB_resultadoOperacion"/>		
			<assign name="VG_error" expr="subGetDatosCti.SUB_error"/>
			
			<assign name="VG_datosCTI" expr="subGetDatosCti.SUB_datosCTI"/>
			<assign name="VG_datosUUI" expr="subGetDatosCti.SUB_datosUUI"/>
			<if cond="VG_resultadoOperacion == 'OK'">
			
					<log label="ENRUTADOR"><value expr="'FIN - GESTION-INICIO-LLAMADA'"/></log>
					
					<log label="ENRUTADOR"><value expr="'VG_datosCTI.vdnOrigen: ' + VG_datosCTI.vdnOrigen"/></log>
					<log label="ENRUTADOR"><value expr="'VG_datosCTI.xferId: ' +  VG_datosCTI.xferId"/></log>
					
					<!-- NUEVO para Transferencias: si esta activo el cti tambien obtendremos el controlador por el segmento -->
					<assign name="PRM_vdnOrigen" expr="VG_segmento"/>	
					<!-- OLD: si estaba activo el cti cogiamos el vdnOrigen que habiamos obtenido de cti -->
<!-- 					<assign name="PRM_vdnOrigen" expr="VG_datosCTI.vdnOrigen"/>		 -->
					
					<assign name="PRM_xferId" expr="VG_datosCTI.xferId"/>	
				
<%-- 					<submit next="${pageContext.request.contextPath}/gestionPrincipal" method="post" namelist="VG_loggerServicio PRM_vdnOrigen PRM_xferId VG_stat"/>		 --%>
					<goto next="#ENRUTADOR_COMPROBAR_ENCUESTA_DIRECTA" />
			
			<elseif cond="VG_resultadoOperacion == 'HANGUP'" />
			
					<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
										
					<log label="ENRUTADOR"><value expr="'FIN - GESTION-INICIO-LLAMADA'"/></log>
					
					<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>	
			
			<else/>
					<assign name="VG_codigoRetorno" expr="'ERROR'"/>
										
					<log label="ENRUTADOR"><value expr="'FIN - GESTION-INICIO-LLAMADA'"/></log>
					
					<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>	
					
<%-- 					<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>	 --%>
				
			</if>	
			
		</filled>	
	</subdialog>	
</form>



<form id="ENRUTADOR_COMPROBAR_ENCUESTA_DIRECTA">

	<block>		

		<!-- ABALFARO_20170516 compruebo que contenga encuestairene, pero puede haber varias versiones -->
		<if cond="VG_servicioIVR.indexOf('ENCUESTAIRENE') &gt; -1">
		<!-- <if cond="VG_servicioIVR == 'ENCUESTAIRENE'"> -->
		
			<!-- ABALFARO_20170516 compruebo primero si he tenido un cuelgue para no cambiar el codigoRetorno -->
			<if cond="VG_codigoRetorno == 'HANGUP'">
				<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error"/>
			</if>
			
			<!-- es una encuesta directa -->
			<assign name="VG_codigoRetorno" expr="'FIN'"/>
			<assign name="VG_error" expr="''"/>
			<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat"/>
		
		<else/>
			<!-- debe ir al modulo y luego se hara la encuesta -->
			<submit next="${pageContext.request.contextPath}/gestionPrincipal" method="post" namelist="VG_loggerServicio PRM_vdnOrigen  VG_stat VG_datosCTI VG_datosUUI"/>		
		</if>
	
	</block>
	
</form>	

</vxml>
