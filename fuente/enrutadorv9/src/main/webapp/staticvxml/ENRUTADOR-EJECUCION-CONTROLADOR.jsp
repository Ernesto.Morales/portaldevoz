<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/staticvxml/ENRUTADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ENRUTADOR-EJECUCION-CONTROLADOR
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="PRM_ruta" expr="''"/>
<!-- PARAMETROS DE ENTRADA AL CONTROLADOR -->
<var name="PRM_IN_CONTROLADOR_nombre" expr="''"/>
<var name="PRM_IN_CONTROLADOR_ruta" expr="''"/>
<var name="PRM_IN_CONTROLADOR_isActivo" expr="''"/>
<var name="PRM_IN_CONTROLADOR_cliente" expr="''"/>
<var name="PRM_IN_CONTROLADOR_VG_RetornoCTI" expr="''"/>
<!-- ABALFARO_20170516 -->
<var name="PRM_IN_CONTROLADOR_contextoEnrutador" expr="''"/>
<!-- PARAMETROS DE SALIDA DEL CONTROLADOR -->
<var name="PRM_OUT_CONTROLADOR_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_codigoRetorno" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_error" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_cliente" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_operativasIVR" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_esquema" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_segmento" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_producto" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_transfer" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_llaveAccesoFront" expr="''"/>
<var name="PRM_OUT_CONTROLADOR_VG_RetornoCTI" expr="''"/>
<!-- ABALFARO_20170217 -->
<var name="PRM_OUT_CONTROLADOR_stat" expr="''"/>


<form id="ENRUTADOR_EJECUCION_CONTROLADOR">
	<block>
		<log label="ENRUTADOR"><value expr="'INICIO - EJECUCION-CONTROLADOR'"/></log>
		<goto next="#SUB_EJECUTAR_CONTROLADOR" />
	</block>
</form>


<form id="SUB_EJECUTAR_CONTROLADOR">
	<block>
		<assign name="PRM_IN_CONTROLADOR_nombre" expr="VG_infoControlador.nombre"/>
		<assign name="PRM_IN_CONTROLADOR_ruta" expr="VG_infoControlador.ruta"/>
		<assign name="PRM_IN_CONTROLADOR_isActivo" expr="VG_infoControlador.isActivo"/>
		<param name="PRM_IN_CONTROLADOR_cliente" expr="VG_cliente"/>
		<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'DCKO')"/>
		<log label="ENRUTADOR"><value expr="'PRM_IN_CONTROLADOR_nombre: ' + PRM_IN_CONTROLADOR_nombre"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_IN_CONTROLADOR_ruta: ' + PRM_IN_CONTROLADOR_ruta"/></log>
		<log label="ENRUTADOR"><value expr="'PRM_IN_CONTROLADOR_isActivo: ' + PRM_IN_CONTROLADOR_isActivo"/></log>
		<assign name="PRM_ruta" expr="'${pageContext.request.contextPath}' + PRM_IN_CONTROLADOR_ruta"/>
		<log label="ENRUTADOR"><value expr="'PRM_ruta: ' + PRM_ruta"/></log>
		<!-- ABALFARO_20170516 -->
		<assign name="PRM_IN_CONTROLADOR_contextoEnrutador" expr="'${pageContext.request.contextPath}'"/>
		<log label="ENRUTADOR"><value expr="'PRM_IN_CONTROLADOR_contextoEnrutador: ' + PRM_IN_CONTROLADOR_contextoEnrutador"/></log>
	</block>
	<subdialog name="subEjecutarControlador" srcexpr="PRM_ruta" method="post"
			namelist="VG_datosLlamada VG_loggerServicio PRM_IN_CONTROLADOR_nombre PRM_IN_CONTROLADOR_ruta PRM_IN_CONTROLADOR_isActivo VG_segmento
			PRM_IN_CONTROLADOR_contextoEnrutador VG_datosCTI VG_datosUUI">
	
		<param name="PRM_IN_CONTROLADOR_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_CONTROLADOR_CTIVAR" expr="VG_datosCTI"/>
		<param name="PRM_IN_CONTROLADOR_VG_datosUUI" expr="VG_datosUUI"/>
		<param name="PRM_IN_CONTROLADOR_nombre" expr="PRM_IN_CONTROLADOR_nombre"/>
		<param name="PRM_IN_CONTROLADOR_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_CONTROLADOR_esquema" expr="VG_esquema"/>
		<param name="PRM_IN_CONTROLADOR_segmento" expr="VG_segmento"/>
		<param name="PRM_IN_CONTROLADOR_producto" expr="VG_producto"/>
		<!-- configuracion de transferencias en el controlador -->
		<param name="PRM_IN_CONTROLADOR_activacionSegmento" expr="VG_infoControlador.activacionSegmento"/>
		<param name="PRM_IN_CONTROLADOR_segmentoTransfer" expr="VG_infoControlador.segmentoTransfer"/>
		<param name="PRM_IN_CONTROLADOR_segmentoTransferDefault" expr="VG_infoControlador.segmentoTransferDefault"/>
		<param name="PRM_IN_CONTROLADOR_transfer" expr="VG_transfer"/>
		<!-- ABALFARO_20170217 -->
		<param name="PRM_IN_CONTROLADOR_stat" expr="VG_stat"/>
		<param name="PRM_IN_CONTROLADOR_VG_RetornoCTI" expr="VG_RetornoCTI"/>
		<!-- ABALFARO_20170118 -->
<!-- 		<param name="PRM_IN_CONTROLADOR_infoControlador" expr="VG_infoControlador"/>	 -->
		
		
		<filled>
			<log label="ENRUTADOR"><value expr="'HE VUELTO A ENRUTADOR'"/></log>
			<assign name="PRM_OUT_CONTROLADOR_resultadoOperacion" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_resultadoOperacion"/>
			<assign name="PRM_OUT_CONTROLADOR_codigoRetorno" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_codigoRetorno"/>
			<assign name="PRM_OUT_CONTROLADOR_error" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_error"/>
			<assign name="PRM_OUT_CONTROLADOR_cliente" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_cliente"/>
			<assign name="PRM_OUT_CONTROLADOR_operativasIVR" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_operativasIVR"/>
			<assign name="PRM_OUT_CONTROLADOR_esquema" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_esquema"/>
			<assign name="PRM_OUT_CONTROLADOR_segmento" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_segmento"/>
			<assign name="PRM_OUT_CONTROLADOR_producto" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_producto"/>
			<assign name="PRM_OUT_CONTROLADOR_transfer" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_transfer"/>
			<assign name="PRM_OUT_CONTROLADOR_llaveAccesoFront" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_llaveAccesoFront"/>
			<assign name="PRM_OUT_CONTROLADOR_VG_RetornoCTI" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_VG_RetornoCTI"/>
			<!-- ABALFARO_20170217 -->
			<assign name="PRM_OUT_CONTROLADOR_stat" expr="subEjecutarControlador.PRM_OUT_CONTROLADOR_stat"/>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_resultadoOperacion: ' + PRM_OUT_CONTROLADOR_resultadoOperacion"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_codigoRetorno: ' + PRM_OUT_CONTROLADOR_codigoRetorno"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_error: ' + PRM_OUT_CONTROLADOR_error"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_operativasIVR: ' + PRM_OUT_CONTROLADOR_operativasIVR"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_esquema: ' + PRM_OUT_CONTROLADOR_esquema"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_segmento: ' + PRM_OUT_CONTROLADOR_segmento"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_producto: ' + PRM_OUT_CONTROLADOR_producto"/></log>
			<log label="ENRUTADOR"><value expr="'PRM_OUT_CONTROLADOR_llaveAccesoFront: ' + PRM_OUT_CONTROLADOR_llaveAccesoFront"/></log>
			<assign name="VG_resultadoOperacion" expr="PRM_OUT_CONTROLADOR_resultadoOperacion"/>
			<assign name="VG_codigoRetorno" expr="PRM_OUT_CONTROLADOR_codigoRetorno"/>
			<assign name="VG_error" expr="PRM_OUT_CONTROLADOR_error"/>
			<assign name="VG_cliente" expr="PRM_OUT_CONTROLADOR_cliente"/>
			<assign name="VG_operativasIVR" expr="PRM_OUT_CONTROLADOR_operativasIVR"/>
			<assign name="VG_esquema" expr="PRM_OUT_CONTROLADOR_esquema"/>
			<assign name="VG_segmento" expr="PRM_OUT_CONTROLADOR_segmento"/>
			<assign name="VG_producto" expr="PRM_OUT_CONTROLADOR_producto"/>
			<assign name="VG_transfer" expr="PRM_OUT_CONTROLADOR_transfer"/>
			<assign name="VG_llaveAccesoFront" expr="PRM_OUT_CONTROLADOR_llaveAccesoFront"/>
			<assign name="VG_RetornoCTI" expr="PRM_OUT_CONTROLADOR_VG_RetornoCTI"/>
			
			<!-- ABALFARO_20170217 -->
			<assign name="VG_stat" expr="PRM_OUT_CONTROLADOR_stat"/>
			<!-- ABALFARO_20170111_INICIO  en caso de que no transfiramos la llamada, debemos actualizar UUI para enviar al CTI en gestionFinLlamada -->
			<!-- Preparo los datos a guardar en DatosUUI -->
			<assign name="VG_datosUUI.llaveSegmento" expr="''"/>
			<assign name="VG_datosUUI.xferId" expr="''"/>
			<assign name="VG_datosUUI.segmentoCliente" expr="''"/>
			<assign name="VG_datosUUI.canal" expr="'MPVO'"/> 
			<assign name="VG_datosUUI.numCliente" expr="VG_cliente.numCliente"/>
			<assign name="VG_datosUUI.llaveAccesoFront" expr="VG_llaveAccesoFront"/>
			<if cond="VG_cliente.isAutenticado == 'true' &amp;&amp; VG_loggerServicio.idServicio.search('PRESTAMOS') != -1">
				<!-- el cliente se ha autenticado -->
				<if cond="VG_cliente.ramaIdentificacion == 'AUTO'">
					<!-- autenticado en la rama de auto. Candados: FECHA_NACIM_CLI|ANYO_CONTR_CRED|ANYO_MATRIC_COCHE -->				
					<assign name="VG_datosUUI.metodoAutenticacion" expr="'17'"/>
				<else/>
					<!-- autenticado en la rama de hipoteca. Candados: FECHA_NACIM_CLI|COD_POSTAL -->
					<assign name="VG_datosUUI.metodoAutenticacion" expr="'18'"/>
				</if>
			</if>
			<!-- FALTA, confirmacion Bancomer -->
			<!-- <assign name="VG_datosUUI.numeroSesion" expr="''"/>  -->
			<assign name="VG_datosUUI.campania" expr="''"/>
			<!-- ABALFARO_20170111_FIN  en caso de que no transfiramos la llamada, debemos actualizar UUI para enviar al CTI en gestionFinLlamada -->
			
			<log label="ENRUTADOR"><value expr="'FIN - EJECUCION-CONTROLADOR'"/></log>
			
			<submit next="${pageContext.request.contextPath}/gestionSalida" method="post" namelist="VG_loggerServicio VG_codigoRetorno VG_error VG_stat VG_RetornoCTI"/>
			
		</filled>
	</subdialog>
</form>

</vxml>
