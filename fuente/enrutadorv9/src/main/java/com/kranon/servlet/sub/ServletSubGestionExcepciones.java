package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.excepserv.BeanExcepcion;
import com.kranon.bean.excepserv.BeanGestionExcepciones;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.singleton.SingletonGestionExcepciones;

/**
 * Servlet para la peticion de SUB-GESTION-EXCEPCIONES en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletSubGestionExcepciones extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	public ServletSubGestionExcepciones() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// aniaado el codigo retorno a devolver
			request.setAttribute("codigoRetorno", "ERROR");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_EXCEPCIONES).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GESTION-EXCEPCIONES";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		BeanExcepcion beanExcepcion = null;
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			// String numOrigen =
			// request.getParameter("VG_loggerServicio.numOrigen");
			// String numDestino =
			// request.getParameter("VG_loggerServicio.numDestino");

			String nombreError = request.getParameter("VG_error");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);


			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("nombreError", nombreError));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean de Gestion Excepciones en cuestion
			BeanGestionExcepciones gestionExcepciones = null;
			SingletonGestionExcepciones instance = SingletonGestionExcepciones.getInstance(log);
			if (instance != null) {
				gestionExcepciones = instance.getGestionExcepciones(idServicio);
			}
			if (gestionExcepciones == null || gestionExcepciones.getTablaExcepciones() == null
					|| gestionExcepciones.getTablaExcepciones().size() == 0) {
				// no hay excepciones definidas
				resultadoOperacion = "KO";
				error = "ERROR_EXT(NO_HAY_EXCEP)";
			} else {
				// busco la excepcion en cuestion
				beanExcepcion = gestionExcepciones.getTablaExcepciones().get(nombreError);

				if (beanExcepcion == null) {
					// no hay ninguna excepcion para ese error
					// recuperamos la excepcion generica IVR
					if (nombreError.contains("ERROR_IVR")) {
						// busco la excepcion generica de IVR
						beanExcepcion = gestionExcepciones.getTablaExcepciones().get("ERROR_IVR");

					} else {						
						// busco la excepcion generica de EXT
						beanExcepcion = gestionExcepciones.getTablaExcepciones().get("ERROR_EXT");
					}

					if (beanExcepcion == null) {
						// tampoco esta definida la excepcion generica
						resultadoOperacion = "KO";
						error = "ERROR_EXT(NO_EXISTE_EXCEP)";
					} else {
						// se ha recuperado correctamente
						resultadoOperacion = "OK";
						error = "";
						
						/** INICIO EVENTO - LOCUCION **/
						this.log.speechEvent(beanExcepcion.getPrompt().getModo(), beanExcepcion.getPrompt().getValuePrompt(), null);
						/** FIN EVENTO - LOCUCION **/
					}

				} else {
					// se ha recuperado correctamente
					resultadoOperacion = "OK";
					error = "";

					/** INICIO EVENTO - LOCUCION **/
					this.log.speechEvent(beanExcepcion.getPrompt().getModo(), beanExcepcion.getPrompt().getValuePrompt(), null);
					/** FIN EVENTO - LOCUCION **/
				}

			}

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("excepcion", beanExcepcion);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null & !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("excepcion", beanExcepcion.toString()));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_EXCEPCIONES).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}
			
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("excepcion", beanExcepcion);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("excepcion", (beanExcepcion== null)? "null": beanExcepcion.toString()));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_EXCEPCIONES).forward(request, response);
		}

	}
}
