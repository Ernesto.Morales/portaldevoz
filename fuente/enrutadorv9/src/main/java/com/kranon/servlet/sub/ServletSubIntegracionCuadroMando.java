package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.bean.stat.contador.BeanStatContador;
import com.kranon.bean.stat.contador.BeanStatInfoContador;
import com.kranon.bean.stat.llamada.BeanStatInfoOperacion;
import com.kranon.bean.stat.llamada.BeanStatLlamada;
import com.kranon.integracion.BeanResultadoIntegracionCM;
import com.kranon.integracion.IntegracionCuadroMando;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesCodigoSegmento;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para la peticion de INTEGRACION-CUADRO-MANDO en el ENRUTADOR
 * 
 * @author abalfaro
 * 
 */
public class ServletSubIntegracionCuadroMando extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ServletSubIntegracionCuadroMando() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_INTEGRACION_CUADRO_MANDO).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String op = "";
		String idModulo = "INTEGRACION_CUADRO_MANDO";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String idAccion = "EJECUCION_CONTROLADOR";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(idModulo);
		try {
			ArrayList<BeanStatInfoContador> listaContadores = new ArrayList<BeanStatInfoContador>();
			ArrayList<BeanStatInfoOperacion> listaOperaciones = new ArrayList<BeanStatInfoOperacion>();
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			op = request.getParameter("VG_loggerServicio.op");
			// Para integracion con cuadro de mando
			String tsec = request.getParameter("VG_cliente.tsec");
			
			String servicioStatCmActivo = request.getParameter("VG_servicioStatCmActivo");

		/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO MoDULO **/
			ArrayList<ParamEvent> parametrosAdicionalesMod = new ArrayList<ParamEvent>();
			parametrosAdicionalesMod.add(new ParamEvent("servicioStatCmActivo", servicioStatCmActivo));
			parametrosAdicionalesMod.add(new ParamEvent("tsec", UtilidadesBeans.cifrarTsec(tsec, 20)));
			this.log.initModuleService(idModulo, parametrosAdicionalesMod);
			/** FIN EVENTO - INICIO MoDULO **/
			//********************************INICIODELOGLLAMADA*****************************
//			UtilidadesLoggerLllamada utilidadesLoggerLllamada = new UtilidadesLoggerLllamada();
//			utilidadesLoggerLllamada.llenarBean(request);
//			utilidadesLoggerLllamada.verificarOK( );
//			utilidadesLoggerLllamada.doArchivo(this.log.getIdLlamada(), this.log.getIdElemento(),
//			this.log.getIdServicio());
//			//********************************************************************************
	
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (servicioStatCmActivo == null) {
				this.errorWarning = "<stat_cm_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			BeanStatLlamada llamadaStat = null;
			BeanStatContador contadorStat = null;

			// [NMB_2001705] Se obtiene toda la info del objeto JS VG_stat siempre, independientemente de si el CM esta activo o no
			// if (servicioStatCmActivo != null && servicioStatCmActivo.equalsIgnoreCase("ON")) {
			// Obtenemos toda la info del objeto JS VG_stat para convertirlo al BeanStatLlamada
			String codLlamIVRStat = request.getParameter("VG_stat.codLlamIVR");
			String codServIVRStat = request.getParameter("VG_stat.codServIVR");
			String timeEntradaMilisStat = request.getParameter("VG_stat.timeEntradaMilis");
			String timeSalidaMilisStat = request.getParameter("VG_stat.timeSalidaMilis");
			String codUcidStat = request.getParameter("VG_stat.codUcid");
			String codVdnStat = request.getParameter("VG_stat.codVdn");
			String xtiContitelStat = request.getParameter("VG_stat.xtiContitel");
			String xtiTiplocutStat = request.getParameter("VG_stat.xtiTiplocut");
			String codPersctpnStat = request.getParameter("VG_stat.codPersctpn");
			String codSgmtxferStat = request.getParameter("VG_stat.codSgmtxfer");
			String desSegmentoStat=null;
			desSegmentoStat = request.getParameter("VG_stat.desSegmento");
			String codKeyfrontStat = request.getParameter("VG_stat.codKeyfront");
			String desNumtfnoStat = request.getParameter("VG_stat.desNumtfno");
			String codEntalfaStat = request.getParameter("VG_stat.codEntalfa");

			// creo el objeto llamada y lo relleno
			llamadaStat = new BeanStatLlamada();
			llamadaStat.setCodLlamIVR(codLlamIVRStat);
			llamadaStat.setCodServIVR(codServIVRStat);
			llamadaStat.setTimeEntradaMilis(timeEntradaMilisStat);
			llamadaStat.setTimeSalidaMilis(timeSalidaMilisStat);
			llamadaStat.setCodUcid(codUcidStat);
			llamadaStat.setCodVdn(codVdnStat);
			llamadaStat.setXtiContitel(xtiContitelStat);
			llamadaStat.setXtiTiplocut(xtiTiplocutStat);
			llamadaStat.setCodPersctpn(codPersctpnStat);
			llamadaStat.setCodSgmtxfer(codSgmtxferStat);
			llamadaStat.setDesSegmento("");
//			if(desSegmentoStat != null || desSegmentoStat != ""){
//				desSegmentoStat=UtilidadesCodigoSegmento.obtenerSegmento(desSegmentoStat);
//				if(desSegmentoStat == "ERROR")
//					llamadaStat.setDesSegmento("DES");
//				else
//					llamadaStat.setDesSegmento(desSegmentoStat);
//			}
//			BeanStatInfoContador infoContadorseg = new BeanStatInfoContador();
//			infoContadorseg.setNombre("CALL_IN");
//			infoContadorseg.setTipo("INFO");
//			infoContadorseg.setCodigoRetorno("");
//			infoContadorseg.setParam1("IN");
//			infoContadorseg.setParam2(desSegmentoStat);
//			infoContadorseg.setParam3("");
//			infoContadorseg.setParam4("");
//			infoContadorseg.setParam5("");
			// asigno el codigo de servicio
//			infoContadorseg.setCodServIVR(llamadaStat.getCodServIVR());
//			listaContadores.add(infoContadorseg);
			
//			llamadaStat.setCodKeyfront(codKeyfrontStat);
//			llamadaStat.setDesNumtfno(desNumtfnoStat);
//			llamadaStat.setCodEntalfa(codEntalfaStat);

			// creo el objeto contador
			contadorStat = new BeanStatContador();

			
			
			llamadaStat.setCodKeyfront(codKeyfrontStat);
			llamadaStat.setDesNumtfno(desNumtfnoStat);
			llamadaStat.setCodEntalfa(codEntalfaStat);
			int numContadorActual = 0;
			int numOperacionActual = 0;

			// *** Recupero todo el objeto STAT de la llamada
			Enumeration<?> enumeration = request.getParameterNames();
			while (enumeration.hasMoreElements()) {
				String parameterName = (String) enumeration.nextElement();

				if (parameterName.contains("VG_stat.operaciones.")) {
					// el parametro es de operaciones

					if (parameterName.contains("VG_stat.operaciones.") && parameterName.contains(".codOperIVR")) {
						// es el parametro de operaciones codigo operacion, aprovecho y cojo toda su info

						// ABALFARO_20170615 obtengo el indice de la operacion actual
						int initIndex = parameterName.indexOf("VG_stat.operaciones.") + "VG_stat.operaciones.".length();
						int endIndex = parameterName.indexOf(".codOperIVR");
						numOperacionActual = Integer.parseInt(parameterName.substring(initIndex, endIndex));

						String codOperIVRStat = request.getParameter("VG_stat.operaciones." + numOperacionActual + ".codOperIVR");
						String codRslIVRStat = request.getParameter("VG_stat.operaciones." + numOperacionActual + ".codRslIVR");
						String flagRslIVRStat = request.getParameter("VG_stat.operaciones." + numOperacionActual + ".flagRslIVR");
						String inicioOperacionMilisStat = request.getParameter("VG_stat.operaciones." + numOperacionActual + ".inicioOperacionMilis");
						String finOperacionMilisStat = request.getParameter("VG_stat.operaciones." + numOperacionActual + ".finOperacionMilis");

						// creo el objeto del operador y lo relleno
						BeanStatInfoOperacion infoOperacion = new BeanStatInfoOperacion();
						infoOperacion.setCodOperIVR(codOperIVRStat);
						infoOperacion.setCodRslIVR(codRslIVRStat);
						infoOperacion.setFlagRslIVR(flagRslIVRStat);
						infoOperacion.setInicioOperacionMilis(inicioOperacionMilisStat);
						infoOperacion.setFinOperacionMilis(finOperacionMilisStat);

						// aado el operador a la lista
						listaOperaciones.add(infoOperacion);

						// ABALFARO_20170615
						// numOperacionActual = numOperacionActual + 1;
					}
				} else if (parameterName.contains("VG_stat.contadores.")) {
					// el parametro es de contadores

					if (parameterName.contains("VG_stat.contadores.") && parameterName.contains(".nombre")) {
						// es el parametro del nombre del contador, aprovecho y cojo toda su info

						// ABALFARO_20170615 obtengo el indice de la operacion actual
						int initIndex = parameterName.indexOf("VG_stat.contadores.") + "VG_stat.contadores.".length();
						int endIndex = parameterName.indexOf(".nombre");
						numContadorActual = Integer.parseInt(parameterName.substring(initIndex, endIndex));
						
						String nombreStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".nombre");
						String tipoStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".tipo");
						String codigoRetornoStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".codigoRetorno");
						String param1Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param1");
						String param2Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param2");
						String param3Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param3");
						String param4Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param4");
						String param5Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param5");

						// creo el objeto del contador y lo relleno
						BeanStatInfoContador infoContador = new BeanStatInfoContador();
						infoContador.setNombre(nombreStat);
						infoContador.setTipo(tipoStat);
						infoContador.setCodigoRetorno(codigoRetornoStat);
						infoContador.setParam1(param1Stat);
						infoContador.setParam2(param2Stat);
						infoContador.setParam3(param3Stat);
						infoContador.setParam4(param4Stat);
						infoContador.setParam5(param5Stat);
						// asigno el codigo de servicio
						infoContador.setCodServIVR(llamadaStat.getCodServIVR());

						// aado el contador a la lista
						listaContadores.add(infoContador);

						// ABALFARO_20170615
						// numContadorActual = numContadorActual + 1;
					}
				}
			}
			// aniado la lista de operaciones la llamada
			llamadaStat.setOperaciones(listaOperaciones);

			// aniado la lista de contadores al contador
			
			contadorStat.setContadores(listaContadores);
			// }

			// [NMB_201705] Se escribe una traza de STATISTICS de resumen de llamada
			/** INICIO EVENTO - STAT **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("CALLID", llamadaStat.getCodLlamIVR()));
			parametrosAdicionalesStat.add(new ParamEvent("ALIANZA_ENTRADA", llamadaStat.getCodVdn()));
			parametrosAdicionalesStat.add(new ParamEvent("TELEFONO_CLIENTE", llamadaStat.getDesNumtfno()));
			parametrosAdicionalesStat.add(new ParamEvent("KEYFRONT_CLIENTE", llamadaStat.getCodKeyfront()));
			parametrosAdicionalesStat.add(new ParamEvent("SEGMENTO_CLIENTE", llamadaStat.getDesSegmento()));
			parametrosAdicionalesStat.add(new ParamEvent("CODIGO_CLIENTE", llamadaStat.getCodPersctpn()));
			parametrosAdicionalesStat.add(new ParamEvent("VDN_TRANSFER", llamadaStat.getCodSgmtxfer()));
			// Se escriben en la traza todas las operaciones realizadas por el Cliente
			for (BeanStatInfoOperacion infoOperacion : listaOperaciones) {
				if(infoOperacion.getCodOperIVR().equalsIgnoreCase("CALL") && op != "" && op != null)
					infoOperacion.setCodOperIVR("CALL_" + op);
				parametrosAdicionalesStat.add(new ParamEvent(infoOperacion.getCodOperIVR(), infoOperacion.getFlagRslIVR()));
			}
			// Se escribe en la traza unicamente el codigo de retorno de la llamada (FIN, TRANSFER, HANGUP)

			for (BeanStatInfoContador infoContador : listaContadores) {
				if (infoContador.getNombre().equalsIgnoreCase("CALL") && op != "" && op != null)
					infoContador.setNombre("CALL_" + op);
				if (infoContador.getNombre().equalsIgnoreCase("CALL") && infoContador.getTipo().equalsIgnoreCase("END"))
					parametrosAdicionalesStat.add(new ParamEvent(infoContador.getNombre(), infoContador.getCodigoRetorno()));
			}
			this.log.statisticEvent("CALL_DETAIL", "OK", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT **/

			
			
			// / *********************************************
			// / **** INTEGRACION CON CUADRO DE MANDO
			// / *********************************************
			BeanResultadoIntegracionCM beanResultadoIntegracion = null;
			if (servicioStatCmActivo != null && servicioStatCmActivo.equalsIgnoreCase("ON")) {

				// INTEGRACIoN CUADRO DE MANDO DE ESTADISTICAS
				IntegracionCuadroMando integracion = new IntegracionCuadroMando(this.log.getIdLlamada(), this.log.getIdElemento(),
						this.log.getIdServicio());

//				beanResultadoIntegracion = integracion.realizarIntegracion(tsec, llamadaStat, contadorStat);
				
				beanResultadoIntegracion = integracion.realizarIntegracionServicio(tsec, llamadaStat, contadorStat);

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("beanResultadoIntegracion", beanResultadoIntegracion.toString()));
				listlogger.add("WS_CUADRO_MANDO");
				if(beanResultadoIntegracion!=null){
					listlogger.add(beanResultadoIntegracion.getResultadoOperacion());
				}
				this.log.actionEvent("WS_CUADRO_MANDO", beanResultadoIntegracion.getResultadoOperacion(), parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				resultadoOperacion = beanResultadoIntegracion.getResultadoOperacion();
				error = beanResultadoIntegracion.getError();

			} else {
				resultadoOperacion = "OK";
				error = "";

			}
			// / *********************************************

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (beanResultadoIntegracion != null) {
				parametrosSalida.add(new ParamEvent("beanResultadoIntegracion", beanResultadoIntegracion.toString()));
			}
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			listlogger.add(resultadoOperacion);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_INTEGRACION_CUADRO_MANDO).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio,
					UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, UtilidadesCuadroMando.getStat(this.log, request, idServicio)),
					error, "", null);
			
			listlogger.add(parametrosAdicionalesError.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(error);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_INTEGRACION_CUADRO_MANDO).forward(request, response);

		}
		
	
	}

}
