package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.singleton.SingletonServicio;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de GESTION-SALIDA en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGestionSalida extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletGestionSalida() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("controlTransferActivo", "");
			request.setAttribute("encuestaActivo", "");
			request.setAttribute("encuestaActivoDefecto", "");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_SALIDA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = "";
		String controlTransferActivo = "";
		String encuestaActivo = "";
		String encuestaActivoDefecto = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add("SALIDA_ENRUTADOR");
		try {

			// ** PARAMETROS DE ENTRADA
			codigoRetorno = request.getParameter("VG_codigoRetorno");
			error = request.getParameter("VG_error");

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			// Recuperamos el Bean del Servicio en cuestion
			BeanServicio servicio = null;
			SingletonServicio instance = SingletonServicio.getInstance(log);
			if (instance != null) {
				servicio = instance.getServicio(idServicio);
			}

			if (servicio == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(SERV_NULL)";
			} else {
				// hemos recuperado bien el servicio

				// recojo si esta activa la transferencia
				controlTransferActivo = servicio.getConfiguracion().getControlTransferActivo();
				// recojo si esta activa la encuesta
				encuestaActivo = servicio.getConfiguracion().getEncuestaActivo();
				encuestaActivoDefecto = servicio.getConfiguracion().getEncuestaDefecto();

				resultadoOperacion = "OK";
				// como codigoRetorno y error devolvere los que haya recibido como parametro
			}

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			parametrosAdicionales.add(new ParamEvent("controlTransferActivo", controlTransferActivo));
			parametrosAdicionales.add(new ParamEvent("encuestaActivo", encuestaActivo));
			parametrosAdicionales.add(new ParamEvent("encuestaActivoDefecto", encuestaActivoDefecto));
			this.log.actionEvent("GESTION_SALIDA", resultadoOperacion, parametrosAdicionales);
			listlogger.add(parametrosAdicionales.toString());
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("controlTransferActivo", controlTransferActivo);
			request.setAttribute("encuestaActivo", encuestaActivo);
			request.setAttribute("encuestaActivoDefecto", encuestaActivoDefecto);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (controlTransferActivo == null) {
				this.errorWarning = "<control_transfer_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
			}
			if (encuestaActivo == null) {
				this.errorWarning = "<encuesta_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
			}
			if (error != null && !error.equals("")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_SALIDA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", error));
			parametrosAdicionales.add(new ParamEvent("controlTransferActivo", controlTransferActivo));
			parametrosAdicionales.add(new ParamEvent("encuestaActivo", encuestaActivo));
			this.log.actionEvent("GESTION_SALIDA", resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("controlTransferActivo", controlTransferActivo);
			request.setAttribute("encuestaActivo", encuestaActivo);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			listlogger.add(parametrosAdicionales.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(codigoRetorno);
			listlogger.add(error);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_SALIDA).forward(request, response);

		}

	}
}
