package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de GESTION-SALIDA en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletSubGestionSalidaEncuesta extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletSubGestionSalidaEncuesta() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_SALIDA_ENCUESTA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		
		String idModulo = "ENCUESTA_IRENE";
		String idAccion = "RESULTADO_ENCUESTA";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = "";
		
		String idEncuesta = "";
		String encuestaCompleta = "";
		String respuestasEncuesta = "";
		String activa = "";
		String dentroHorario = "";
		
		try {

			// ** PARAMETROS DE ENTRADA
			resultadoOperacion = request.getParameter("PRM_resultadoOperacion");
			codigoRetorno = request.getParameter("PRM_codigoRetorno");
			error = request.getParameter("PRM_error");

			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			idEncuesta = request.getParameter("PRM_idEncuesta");
			encuestaCompleta = request.getParameter("PRM_encuestaCompleta");
			respuestasEncuesta = request.getParameter("PRM_respuestaEncuesta");
			activa = request.getParameter("PRM_activa");
			dentroHorario = request.getParameter("PRM_dentroHorario");		 
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
						
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("idEncuesta", idEncuesta));
			parametrosAdicionales.add(new ParamEvent("activa", activa));
			parametrosAdicionales.add(new ParamEvent("dentroHorario", dentroHorario));
			parametrosAdicionales.add(new ParamEvent("encuestaCompleta", encuestaCompleta));
			parametrosAdicionales.add(new ParamEvent("respuestasEncuesta", respuestasEncuesta));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/
					
			/** INICIO EVENTO - STAT ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesResulStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesResulStat.add(new ParamEvent("idEncuesta", idEncuesta));
			parametrosAdicionalesResulStat.add(new ParamEvent("activa", activa));
			parametrosAdicionalesResulStat.add(new ParamEvent("dentroHorario", dentroHorario));	
			parametrosAdicionalesResulStat.add(new ParamEvent("encuestaCompleta", encuestaCompleta));
			parametrosAdicionalesResulStat.add(new ParamEvent("respuestasEncuesta", respuestasEncuesta));			
			this.log.statisticEvent(idModulo, "RESULTADO_ENCUESTA", parametrosAdicionalesResulStat);
			/** FIN EVENTO - STAT ACCION **/
			
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (error != null && !error.equals("")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null & !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/		
			
			/** INICIO EVENTO - STAT FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosAdicionalesStat.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null && !error.equals("")) {
				parametrosAdicionalesStat.add(new ParamEvent("error", error));
			}
			this.log.statisticEvent(idModulo, "END", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT FIN DE MODULO **/
			
			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("idEncuesta", idEncuesta);
			request.setAttribute("encuestaCompleta", encuestaCompleta);
			request.setAttribute("respuestasEncuesta", respuestasEncuesta);	
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_SALIDA_ENCUESTA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent("GESTION_SALIDA", resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GESTION_SALIDA_ENCUESTA).forward(request, response);

		}

	}
}
