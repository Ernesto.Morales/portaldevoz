package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.cti.ProcesoCTI;
import com.kranon.servicios.cti.bean.BeanDatosCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servicios.cti.bean.ProcesoCTIOutDto;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesEnrutador;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de SUB-GET-DATOS-CTI en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletSubGetDatosCTI extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletSubGetDatosCTI() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("datosCti", "");
			request.setAttribute("datosUUI", "");
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GET_DATOS_CTI).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		
		String identificador = null;
		String numOrigen = null;
		String numDestino = null;
		String uui = null;


		String idAccion = "GET_DATOS_CTI";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";		
		BeanDatosCTI beanDatosCTI = null;
		BeanDatosUUI beanDatosUUI = null;
		ProcesoCTIOutDto respuesta = null;
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(UtilidadesLoggerKranon.obtiene+"BEAN Datos CTI");
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			logKranon.setIdServicio(idLlamada);
			identificador = request.getParameter("VG_datosLlamada.callId");
			numOrigen = request.getParameter("VG_datosLlamada.numOrigen");
			numDestino = request.getParameter("VG_datosLlamada.numDestino");
			uui = request.getParameter("VG_datosLlamada.uui");
//			listlogger.add("UUI"+uui);
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			// PROCESO GET DATOS CTI

			ProcesoCTI procCti = new ProcesoCTI(idLlamada, idElemento, idServicio);
			
			//Se verifica el UUI si no viene vacio se agrega a la peticion del CTI
			if( uui != null){
				uui = uui.trim();
				String cadenaAscii = "";
				if( uui.length() > 1  ){					
					String cadenaAux[] = uui.split(UtilidadesEnrutador.SEPARADOR_ENCODING);
					//Hay separacion de la cadena con el encoding
					if( cadenaAux.length > 1 ){
						int tamanio = Integer.parseInt(cadenaAux[0].substring(4, 6), 16) * 2;
						cadenaAscii = UtilidadesEnrutador.convertirHexadecimalString( cadenaAux[0].substring(6,6+tamanio) );						
					}
					else{
						//En la nueva implementacion la cadena viene sin encoding 
						cadenaAscii = UtilidadesEnrutador.convertirHexadecimalString( uui );						
					}
//					cadenaAscii="|0_2|5467|######kR|1MiQhz2otdIuUlQkbEyi|##|tIqVMiI16f|35########";
					beanDatosUUI = BeanDatosUUI.parseaCadena(cadenaAscii);
//					listlogger.add(beanDatosUUI.toString());
				}								
			}
						
			//Se setean datos del CTI		
			beanDatosCTI = new BeanDatosCTI();
			beanDatosCTI.setAni(numOrigen);
			beanDatosCTI.setDnis(numDestino);
			beanDatosCTI.setLlave(identificador);

			//Se invoca al servicio del CTI
			respuesta = procCti.iniciarLlamada(beanDatosCTI, beanDatosUUI);
			
			if(respuesta == null){
				resultadoOperacion = "KO";
				error = "ERROR_EXT(DATOS_CTI_NULL)";				
			}
			else{
				//Se obtienen las respuestas
//				beanDatosCTI = respuesta.getBeanDatosCTI();
				beanDatosUUI = respuesta.getBeanDatosUUI();
				resultadoOperacion = "OK";
				error = "";		
//				Truqueado para pruebas
				
			}
			
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (respuesta.getCodigoError() != null && respuesta.getCodigoError().contains("-")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCTI(idServicio, "", 
						UtilidadesEnrutador.ARRAY_ERRORES_CTI[Integer.parseInt((respuesta.getCodigoError()).replace("-", ""))], 
						respuesta.getCodigoError(), null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
				
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			parametrosAdicionales.add(new ParamEvent("beanDatosCTI", beanDatosCTI == null ? "null" : beanDatosCTI.toString()));
			parametrosAdicionales.add(new ParamEvent("beanDatosUUI", beanDatosUUI == null ? "null" : beanDatosUUI.toString()));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/
//			setPrecotizacion(beanDatosUUI);
			/***PARA PRUEBAS CTI UUI***/
			/***PARA PRUEBAS CTI para universal***/
//			
			// Aniado parametros de salida a la request
			resultadoOperacion = "OK";
			request.setAttribute("datosCti", beanDatosCTI);
			request.setAttribute("datosUUI", beanDatosUUI);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			listlogger.add(beanDatosCTI.toString());
			listlogger.add(beanDatosUUI.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();	
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GET_DATOS_CTI).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningCTI(idServicio, "", e.toString(), "-", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			parametrosAdicionales.add(new ParamEvent("beanDatosCTI", beanDatosCTI == null ? "null" : beanDatosCTI.toString()));
			parametrosAdicionales.add(new ParamEvent("beanDatosUUI", beanDatosUUI == null ? "null" : beanDatosUUI.toString()));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("error", error);
			listlogger.add(parametrosAdicionales.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GET_DATOS_CTI).forward(request, response);
		}

	}

}
