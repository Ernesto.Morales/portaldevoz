package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.bean.transfer.BeanTransfer;
import com.kranon.loccomunes.BeanLocucionesComunes;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.singleton.SingletonLocucionesComunes;
import com.kranon.singleton.SingletonTransfer;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesEnrutador;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para la peticion de GESTION-TRANSFERENCIA-ASESOR en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGestionTransferenciaAsesor extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	private CommonLoggerProcess logP;
	
	private String resultadoOperacion = "";
	private String error = "";
	// datos de la transferencia
	private  String ctiTransfer = "";
	private String vdnTransfer = "";
	private  String isDentroHorario = "";
	private  String locFH = "";
	private String modoLocFH = "";
	
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ServletGestionTransferenciaAsesor() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		try {
			this.doPost(request, response);

		} catch (final Exception e) {
			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("vdnTransfer", "");
			request.setAttribute("ctiTransfer", "");
			request.setAttribute("isDentroHorario", "");
			request.setAttribute("locFH", "");
			request.setAttribute("modoLocFH", "");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_TRANSFERENCIA_ASESOR).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GESTION_TRANSFERENCIA_ASESOR";
		// ** PARAMETROS DE SALIDA
		

		// NMB - Se declara aqui para poder tener vision para la parte de monitorizacion
		String controlHorarioTransfer = "";

		Long timeInicioLlam = new Long(0);
		try {
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			listlogger.add(UtilidadesLoggerKranon.envia+"TRANSFERENCIA");
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String idioma = request.getParameter("VG_loggerServicio.idioma");
			String segmento = request.getParameter("VG_transfer.segmento");

			String vdnTransferPorDefecto = request.getParameter("VG_servicioVdnTransferDef");
			String ctiActivoPorDefecto = request.getParameter("VG_servicioCtiActivo");

			String timeInicioLlamString = request.getParameter("VG_timeInicioLlam");
			timeInicioLlam = Long.parseLong(timeInicioLlamString);

			String uuiDefault = request.getParameter("VG_datosLlamada.uuiDefault");
			String sessionSipHost = request.getParameter("PRM_sessionSipHost");
			uuiDefault = (uuiDefault == null) ? "null" : uuiDefault;
			sessionSipHost = (sessionSipHost == null) ? "null" : sessionSipHost;

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("codServIvr", idServicio));
			parametrosEntrada.add(new ParamEvent("segmento", segmento));
			parametrosEntrada.add(new ParamEvent("timeInicioLlam", timeInicioLlamString));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			/** INICIO EVENTO - STAT INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("codServIvr", idServicio));
			parametrosAdicionalesStat.add(new ParamEvent("segmento", segmento));
			parametrosAdicionalesStat.add(new ParamEvent("timeInicioLlam", timeInicioLlamString));
			this.log.statisticEvent(idModulo, "INIT", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE MODULO **/

//			BeanTransfer transfer = null;

//			if (segmento.equals("DEFAULT_SERV")) {
//				// queremos transferir al por defecto, indico resultado KO para abajo calcular la transferencia
//				resultadoOperacion = "KO";
//
//			} 
//			else {
				String segmentoTransfer = request.getParameter("VG_transfer.ligaRetorno");
//				String segmentoId = "USAC";
				String segmentoId = request.getParameter("VG_transfer.segmentoId");
				String segmentoTmp = "";
				
//				segmentoTransfer="TRANSFERENCIAS_SALDOS_MOVIMIENTOS";

				//				request.getParameter("VG_transfer.segmentoId");
				// Recuperamos el Bean de la Transferencia en cuestion (idServicio - segmento)
				
//				if(segmentoTransfer != null && segmentoTransfer != "")
//				{
//					if(segmentoId != null && segmentoId != "")
//					{
//						segmentoTmp = "USAC";
//						if(segmentoTmp != "ERROR")
//						{
//							Integer posicion_ = segmentoTransfer.indexOf("_");
//							segmentoTransfer = segmentoTmp + segmentoTransfer.substring(posicion_, segmentoTransfer.length());
//							listlogger.add("segmentoTransfer: "+segmentoTransfer);
//						}
//					}
//				}
//				else 
//				{
//					segmentoTransfer = "DEFAULT";
//				}
				
//				transfer = null;
				
				
//				SingletonTransfer instanceTransfer = SingletonTransfer.getInstance(log);
//				if (instanceTransfer != null) {
//					transfer = instanceTransfer.getTransfer(idServicio, segmento);
//				}
//				
				
				
//			    segmentoTransfer=request.getParameter("VG_ligaRetorno");  
				BeanTransfer transfer = null;
				SingletonTransfer instanceTransfer = SingletonTransfer.getInstance(log);
				if (instanceTransfer != null) {
					transfer = instanceTransfer.getTransfer(idServicio, segmentoTransfer);
				}

//				if (transfer == null) {
//					// error al recuperar la transferencia
//					resultadoOperacion = "KO";
//					error = "ERROR_EXT(TRANSFER_NULL)";
//				}
//				else{
//					vdn = transfer.getConfiguracion().getDestinoTransfer();
//				}

				if (transfer == null) {
					// error al recuperar la transferencia
					resultadoOperacion = "KO";
					error = "ERROR_EXT(TRANSFER_NULL)";
					segmentoTransfer = "DEFAULT";
					transfer = null;
					instanceTransfer = SingletonTransfer.getInstance(log);
					if (instanceTransfer != null) {
						transfer = instanceTransfer.getTransfer(idServicio, segmentoTransfer);
						if(transfer!=null){
							listlogger.add("ARCHIVO_DEFAUTL");
							if(transfer.getConfiguracion().getDestinoTransfer() != null && transfer.getConfiguracion().getDestinoTransfer() != "")
							{
								vdnTransfer = transfer.getConfiguracion().getDestinoTransfer();
								ctiTransfer = transfer.getConfiguracion().getCtiActivo();
								resultadoOperacion = "OK";
								error = "";
							}
						}
						else{
							getDefault(idLlamada, idElemento);
//							logP = new CommonLoggerProcess("TRASFERENCIA_DEFAULT");
//							logP.inicializar(idLlamada, idElemento);
//							ArrayList<ParamEvent> parametrosEntradaP = new ArrayList<ParamEvent>();
//							parametrosEntradaP.add(new ParamEvent("DEFAULT","DEFAULT"));
//							logP.initModuleProcess("DEFAULT_TRANSFER", parametrosEntradaP);
//							UtilidadesBeans util = new UtilidadesBeans(logP);
//							vdnTransfer = util.leeParametroUnmarshall("TRANS_DEFAULT");
//							ctiTransfer = util.leeParametroUnmarshall("CTI_ACTIVO");
//							resultadoOperacion = "OK";
//							error = "";
//							listlogger.add("DEFAUTL_CONTEXTO");
						}
					}
					
					

				} else {
					if(transfer.getConfiguracion().getDescTransfer()!=null){
						listlogger.add("DescTransfer"+transfer.getConfiguracion().getDescTransfer());
					}
					
					// COMPROBAR HORARIO DE LA TRANSFERENCIA
					controlHorarioTransfer = transfer.getConfiguracion().getControlHorario();
					String horarioTransfer = "DH";
					if (controlHorarioTransfer != null && controlHorarioTransfer.equalsIgnoreCase("ON")) {
						// el control horario esta activo, miro si estoy DH
						// o FH
						horarioTransfer = UtilidadesEnrutador.comprobarHorario(this.log, transfer.getHorario());
						if (horarioTransfer != null && horarioTransfer.equalsIgnoreCase("DH")) {
							// estamos dentro de horario
							isDentroHorario = "true";
						} else {
							// fuera de horario
							isDentroHorario = "false";
							locFH = transfer.getConfiguracion().getWavTransferOutHour();
							modoLocFH = transfer.getConfiguracion().getModoReproduccion();							 
						}
					} else {
						// no tiene control horario
						isDentroHorario = "OFF";
					}
					listlogger.add("controlHorarioTransfer=" + controlHorarioTransfer + "|" + "isDentroHorario=" + isDentroHorario);
					/** INICIO EVENTO - COMENTARIO **/
					this.log.comment("controlHorarioTransfer=" + controlHorarioTransfer + "|" + "isDentroHorario=" + isDentroHorario);
					/** FIN EVENTO - COMENTARIO **/
					if(transfer.getConfiguracion().getDestinoTransfer() != null && transfer.getConfiguracion().getDestinoTransfer() != "")
					{
						vdnTransfer = transfer.getConfiguracion().getDestinoTransfer();
						ctiTransfer = transfer.getConfiguracion().getCtiActivo();
						resultadoOperacion = "OK";
						error = "";
					}
					else
					{
						resultadoOperacion = "KO";
						error = "ERROR_EXT(TRANSFER_NULL)";
					}
					
				}
//			}
				
			// Recuperamos las locuciones comunes al servicio
			BeanLocucionesComunes locComunes = null;
			SingletonLocucionesComunes instanceLocComunes = SingletonLocucionesComunes.getInstance(log);
			if (instanceLocComunes != null) {
				locComunes = instanceLocComunes.getLocucionesComunes(idServicio);
			}

			if (locComunes != null) {
				// hemos recuperado bien las locuciones comunes

				/**
				 * INICIO EVENTO - COMENTARIO this.log.comment("LocucionesComunes recuperadas: " + locComunes.toString()); FIN EVENTO - COMENTARIO
				 **/

				// Aniado parametros de salida a la request
				request.setAttribute("locComunes", locComunes);
			}

			// calculo duracion llamada para enviar a cti
			String duracionLlamada = this.getDuracionLlamada(timeInicioLlam) + "";

			request.setAttribute("idServicio", idServicio);

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("vdnTransfer", vdnTransfer);
			request.setAttribute("ctiTransfer", ctiTransfer);
			request.setAttribute("isDentroHorario", isDentroHorario);
			request.setAttribute("locFH", locFH);
			request.setAttribute("modoLocFH", modoLocFH);
			request.setAttribute("duracionLlamada", duracionLlamada);

			String cadenaTransferencia = "";

			if (resultadoOperacion.equalsIgnoreCase("OK")) {

				// [20161212-NMB] - Se incluye la comprobacion de que ctiTransfer sea distinto de null
				if (ctiActivoPorDefecto.equals("ON") && ctiTransfer != null && ctiTransfer.equals("ON")) {
					cadenaTransferencia = "sip:" + vdnTransfer + "@" + sessionSipHost + "?User-To-User=uuiCTI";
				} else {
					cadenaTransferencia = "sip:" + vdnTransfer + "@" + sessionSipHost + "?User-To-User=" + uuiDefault;
				}
				// SE TRANSFIERE AL VDN CONFIGURADO

				/** INICIO EVENTO - FIN DE MODULO **/
				ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
				if (error != null && !error.equals("")) {
					parametrosSalida.add(new ParamEvent("error", error));
				}
				parametrosSalida.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosSalida.add(new ParamEvent("ctiTransfer", ctiTransfer));
				parametrosSalida.add(new ParamEvent("segmentoTransfer", segmentoTransfer));				
				parametrosSalida.add(new ParamEvent("isDentroHorario", isDentroHorario));
				parametrosSalida.add(new ParamEvent("locFH", locFH));
				parametrosSalida.add(new ParamEvent("modoLocFH", modoLocFH));
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("segmento", segmento));
				parametrosAdicionales.add(new ParamEvent("duracionLlamada", duracionLlamada));
				parametrosAdicionales.add(new ParamEvent("cadenaTransferencia", cadenaTransferencia));
				this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
				/** FIN EVENTO - FIN DE MODULO **/

				/** INICIO EVENTO - STATISTICS **/
				ArrayList<ParamEvent> parametrosAdicStatEnd = new ArrayList<ParamEvent>();
				parametrosAdicStatEnd.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
				if (error != null && !error.equals("")) {
					parametrosAdicStatEnd.add(new ParamEvent("error", error));
				}
				parametrosAdicStatEnd.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosAdicStatEnd.add(new ParamEvent("ctiTransfer", ctiTransfer));
				parametrosAdicStatEnd.add(new ParamEvent("isDentroHorario", isDentroHorario));
				parametrosAdicStatEnd.add(new ParamEvent("locFH", locFH));
				parametrosAdicStatEnd.add(new ParamEvent("modoLocFH", modoLocFH));
				parametrosAdicStatEnd.add(new ParamEvent("segmento", segmento));
				parametrosAdicStatEnd.add(new ParamEvent("duracionLlamada", duracionLlamada));
				parametrosAdicStatEnd.add(new ParamEvent("cadenaTransferencia", cadenaTransferencia));
				this.log.statisticEvent(idModulo, "END", parametrosAdicStatEnd);
				/** FIN EVENTO - STATISTICS **/
			} else {

				// se transfiere al vdn por defecto
				if (ctiActivoPorDefecto.equals("ON")) {
					cadenaTransferencia = "sip:" + vdnTransferPorDefecto + "@" + sessionSipHost + "?User-To-User=uuiCTI";
				} else {
					cadenaTransferencia = "sip:" + vdnTransferPorDefecto + "@" + sessionSipHost + "?User-To-User=" + uuiDefault;
				}
				
				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				if (controlHorarioTransfer == null) {
					this.errorWarning = "<control_horario_transfer>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (controlHorarioTransfer.equalsIgnoreCase("ON") && transfer != null && transfer.getHorario() == null) {
					this.errorWarning = "<horario>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (controlHorarioTransfer.equalsIgnoreCase("ON") && modoLocFH == null) {
					this.errorWarning = "<modo_reproduccion>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (controlHorarioTransfer.equalsIgnoreCase("ON") && locFH == null) {
					this.errorWarning = "<wav_transfer_out_hour>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (vdnTransfer == null) {
					this.errorWarning = "<destino_transfer>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (ctiTransfer == null) {
					this.errorWarning = "<cti_activo>";
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
				}
				if (error != null && !error.equals("")) {
					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
					this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
							UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
				}
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				
				/** INICIO EVENTO - FIN DE MODULO **/
				ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
				if (error != null && !error.equals("")) {
					parametrosSalida.add(new ParamEvent("error", error));
				}
				parametrosSalida.add(new ParamEvent("vdnTransferPorDefecto", vdnTransferPorDefecto));
				parametrosSalida.add(new ParamEvent("ctiActivoPorDefecto", ctiActivoPorDefecto));
				parametrosSalida.add(new ParamEvent("isDentroHorario", "OFF"));
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("segmento", segmento));
				parametrosAdicionales.add(new ParamEvent("duracionLlamada", duracionLlamada));
				parametrosAdicionales.add(new ParamEvent("cadenaTransferencia", cadenaTransferencia));
				this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
				/** FIN EVENTO - FIN DE MODULO **/

				/** INICIO EVENTO - STATISTICS **/
				ArrayList<ParamEvent> parametrosAdicStatEnd = new ArrayList<ParamEvent>();
				parametrosAdicStatEnd.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
				if (error != null && !error.equals("")) {
					parametrosAdicStatEnd.add(new ParamEvent("error", error));
				}
				parametrosAdicStatEnd.add(new ParamEvent("vdnTransferPorDefecto", vdnTransferPorDefecto));
				parametrosAdicStatEnd.add(new ParamEvent("ctiActivoPorDefecto", ctiActivoPorDefecto));
				parametrosAdicStatEnd.add(new ParamEvent("isDentroHorario", "OFF"));
				parametrosAdicStatEnd.add(new ParamEvent("segmento", segmento));
				parametrosAdicStatEnd.add(new ParamEvent("duracionLlamada", duracionLlamada));
				parametrosAdicStatEnd.add(new ParamEvent("cadenaTransferencia", cadenaTransferencia));
				this.log.statisticEvent(idModulo, "END", parametrosAdicStatEnd);
				/** FIN EVENTO - STATISTICS **/
			}
			request.setAttribute("segmentoTransfer", segmentoTransfer);
			listlogger.add("vdnTransfer: "+vdnTransfer);
			
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_TRANSFERENCIA_ASESOR).forward(request, response);

		} catch (Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("vdnTransfer", vdnTransfer);
			request.setAttribute("ctiTransfer", ctiTransfer);
			request.setAttribute("segmentoTransfer", "");
			request.setAttribute("isDentroHorario", isDentroHorario);
			request.setAttribute("locFH", locFH);
			request.setAttribute("modoLocFH", modoLocFH);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("vdnTransfer", vdnTransfer));
			parametrosSalida.add(new ParamEvent("ctiTransfer", ctiTransfer));
			parametrosSalida.add(new ParamEvent("isDentroHorario", isDentroHorario));
			parametrosSalida.add(new ParamEvent("locFH", locFH));
			parametrosSalida.add(new ParamEvent("modoLocFH", modoLocFH));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			if(parametrosSalida!=null)
				listlogger.add(parametrosSalida.toString());
			if(e!=null)
				listlogger.add(e.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_TRANSFERENCIA_ASESOR).forward(request, response);

		}

	}

	private Long getDuracionLlamada(Long milisInicioLlamada) {

		Date fechaHoy = new Date();
		Long milisecFinLlam = fechaHoy.getTime();

		Long duracionLlamada = milisecFinLlam - milisInicioLlamada;
		/** INICIO EVENTO - COMENTARIO **/
		this.log.comment("duracionLlamada=" + duracionLlamada);
		/** FIN EVENTO - COMENTARIO **/

		duracionLlamada = duracionLlamada / 1000;

		/** INICIO EVENTO - COMENTARIO **/
		this.log.comment("duracionLlamadaSec=" + duracionLlamada);
		/** FIN EVENTO - COMENTARIO **/

		return duracionLlamada;

	}
	
	private void getDefault(String idLlamada,String idElemento){
		logP = new CommonLoggerProcess("TRASFERENCIA_DEFAULT");
		logP.inicializar(idLlamada, idElemento);
		ArrayList<ParamEvent> parametrosEntradaP = new ArrayList<ParamEvent>();
		parametrosEntradaP.add(new ParamEvent("DEFAULT","DEFAULT"));
		logP.initModuleProcess("DEFAULT_TRANSFER", parametrosEntradaP);
		UtilidadesBeans util = new UtilidadesBeans(logP);
		vdnTransfer = util.leeParametroUnmarshall("TRANS_DEFAULT");
		ctiTransfer = util.leeParametroUnmarshall("CTI_ACTIVO");
		resultadoOperacion = "OK";
		error = "";
		listlogger.add("DEFAUTL_CONTEXTO");
	}
}
