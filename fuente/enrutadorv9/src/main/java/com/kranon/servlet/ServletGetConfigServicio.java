package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.singleton.SingletonServicio;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesEnrutador;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de GET-CONFIG-SERVICIO en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGetConfigServicio extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;
	
	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	protected CommonLoggerKranon logKranon;
	protected List<String> listlogger = new ArrayList<String>();
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ServletGetConfigServicio() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("ctiActivo", "");
			request.setAttribute("vdnTransferDef", "");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GET_CONFIG_SERVICIO).forward(request, response);
		}
	}

	/**
	 * PARAMETROS DE SALIDA: - Servicio recuperado KO - resultadoOperacion = KO - codigoRetorno = ERROR - error = ERROR_EXT(SERV_NULL)
	 * 
	 * - Servicio recuperado OK, NO activo - resultadoOperacion = OK - codigoRetorno = ERROR - error = ERROR_IVR(SERV_IVR_NO_ACTIVO)
	 * 
	 * - Servicio recuperado OK, activo y SIN CONTROL de horario - resultadoOperacion = OK - codigoRetorno = OK
	 * 
	 * - Servicio recuperado OK, activo y fallo al comprobar el horario - resultadoOperacion = KO - codigoRetorno = ERROR - error =
	 * ERROR_IVR(SERV_IVR_HORARIO)
	 * 
	 * - Servicio recuperado OK, activo y FUERA de horario - resultadoOperacion OK - codigoRetorno = ERROR - error = ERROR_IVR(SERV_IVR_FUERA_HORARIO)
	 * 
	 * - Servicio recuperado OK, activo y DENTRO de horario - resultadoOperacion = OK - codigoRetorno = OK
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GET_CONFIG_SERVICIO";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		String ctiActivo = "";
		String vdnTransferDef = "";
		String statCmActivo = "";
		String operacion = "";
		// ** PARAMETROS ADICIONALES
		String servActivo = "";
		String controlHorario = "";
		String horario = "";
		try {
			// ** PARAMETROS DE ENTRADA
			operacion = request.getParameter("VG_loggerServicio.op");
			//limpiar cadena listLogger
			listlogger.clear();
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			listlogger.add("INICIO:" + operacion);
			
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String numOrigen = request.getParameter("VG_loggerServicio.numOrigen");
			String numDestino = request.getParameter("VG_loggerServicio.numDestino");
			String sesionAvaya = request.getParameter("VG_loggerServicio.sesAvaya");
			
			System.out.println("sesioooooooooon= " + sesionAvaya);
			
			// DATOS DE TRANSFERENCIA
			String esquema = request.getParameter("VG_esquema");
			String segmento = request.getParameter("VG_segmento");
			String producto = request.getParameter("VG_producto");
			
			
		
			
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
//			idllamada
			logKranon.setIdServicio(this.log.getIdLlamada());
			/** INICIO EVENTO - INICIO DE LLAMADA **/
			this.log.initCall(numOrigen, numDestino, null);
			this.listlogger.add(idServicio);
			this.listlogger.add(idElemento);
			this.listlogger.add("numOrigen:"+numOrigen);
			this.listlogger.add("numDestino"+numDestino);
			/** FIN EVENTO - INICIO DE LLAMADA **/

			/** INICIO EVENTO - STAT INICIO DE LLAMADA **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("numOrigen", numOrigen));
			parametrosAdicionalesStat.add(new ParamEvent("numDestino", numDestino));
			this.log.statisticEvent("INIT_CALL", "OK", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE LLAMADA **/
			
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesTransfer = new ArrayList<ParamEvent>();
			parametrosAdicionalesTransfer.add(new ParamEvent("esquema", esquema));
			parametrosAdicionalesTransfer.add(new ParamEvent("segmento", segmento));
			parametrosAdicionalesTransfer.add(new ParamEvent("producto", producto));
			this.log.actionEvent("GET_CONFIG_TRANSFER", "OK", parametrosAdicionalesTransfer);
			/** FIN EVENTO - ACCION **/

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("idServicio", idServicio));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean del Servicio en cuestion
			BeanServicio servicio = null;
			SingletonServicio instance = SingletonServicio.getInstance(log);
			if (instance != null) {
				servicio = instance.getServicio(idServicio);
			}

			if (servicio == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(SERV_NULL)";

			} else {
				// hemos recuperado bien el servicio

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("GET_SERVICIO=" + servicio.toString());
				/** FIN EVENTO - COMENTARIO **/

				servActivo = servicio.getConfiguracion().getServActivo();
				ctiActivo = servicio.getConfiguracion().getCti();
				vdnTransferDef = servicio.getConfiguracion().getVdnTransferDef();
				statCmActivo = servicio.getConfiguracion().getStatCmActivo();

				controlHorario = "";
				// compruebo si esta activo
				if (servActivo == null || !servActivo.equalsIgnoreCase("ON")) {
					// el servicio NO esta activo

					resultadoOperacion = "OK";
					codigoRetorno = "ERROR";
					error = "ERROR_IVR(SERV_IVR_NO_ACTIVO)";

					// [20161019] Kranon-ES
					// debemos dejar la transferencia preparada
					// por defecto

				} else {
					// el servicio esta activo

					/** INICIO EVENTO - COMENTARIO **/
					this.log.comment("ctiActivo=" + ctiActivo + "|vdnTransferDef=" + vdnTransferDef);
					/** FIN EVENTO - COMENTARIO **/

					// compruebo si tiene control de horario
					controlHorario = servicio.getConfiguracion().getControlHorario();
					if (controlHorario == null || (!controlHorario.equalsIgnoreCase("ON") && !controlHorario.equalsIgnoreCase("ON_SEGMENT"))) {
						// no hay control de horario
						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";
						
						horario = "OFF";

					} else {
						// hay control de horario (ON o ON_SEGMENT)

						// compruebo el horario del servicio
						// horario = UtilidadesEnrutador.comprobarHorario(log, servicio.getHorario());
						
						// ABALFARO_20170221 cambio horario condicional por segmento
						String segmentoHorario = "";
						if(controlHorario.equalsIgnoreCase("ON_SEGMENT")){
							// controlHorario = ON_SEGMENT			
							// se comprueba el horario para el segmento actual, en caso de no existir se evaluara el horario con param vacio
							horario = UtilidadesEnrutador.comprobarHorario(log, servicio.getHorario(), segmento);
							segmentoHorario = !segmento.equals("") ? ("_" + segmento) : "";
							
						} else {
							// controlHorario = ON
							// se comprueba si existe horario con parametrizacion a vacio o horario antiguo y se devuelve el resultado
							horario = UtilidadesEnrutador.comprobarHorario(log, servicio.getHorario());
							segmentoHorario = "";
						} 

						if (horario == null) {
							// la comprobacion ha fallado
							resultadoOperacion = "KO";
							codigoRetorno = "ERROR";
							error = "ERROR_IVR(SERV_IVR_HORARIO)";

						} else if (horario.equalsIgnoreCase("FH")) {
							// el servicio esta FUERA de HORARIO
							resultadoOperacion = "OK";
							codigoRetorno = "ERROR";
							// error = "ERROR_IVR(SERV_IVR_FUERA_HORARIO)";
							// ABALFARO_20170221 si el servicio tenia declarado un control horario parametrizado, buscaremos el error con el segmento
							error = "ERROR_IVR(SERV_IVR_FUERA_HORARIO" + segmentoHorario + ")";

						} else {
							// el servicio esta DENTRO de HORARIO
							resultadoOperacion = "OK";
							codigoRetorno = "OK";
							error = "";
						}
					}

				}
			}

			/** INICIO EVENTO - STAT HORARIO SERVICIO **/
			ArrayList<ParamEvent> parametrosAdicionalesStatServ = new ArrayList<ParamEvent>();
			parametrosAdicionalesStatServ.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionalesStatServ.add(new ParamEvent("controlHorario", controlHorario));
			parametrosAdicionalesStatServ.add(new ParamEvent("horario", (horario == null) ? "null" : horario));
			if (error != null && !error.equals("")) {
				parametrosAdicionalesStatServ.add(new ParamEvent("error", error));
			}
			this.log.statisticEvent("CONTROL_HORARIO", resultadoOperacion, parametrosAdicionalesStatServ);
			/** FIN EVENTO - STAT HORARIO SERVICIO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (servActivo == null) {
				this.errorWarning = "<serv_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);			
			}
			if (ctiActivo == null) {
				this.errorWarning = "<cti>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);			
			}
			if (vdnTransferDef == null) {
				this.errorWarning = "<vdn_transfer_def>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);			
			}
			if (controlHorario == null) {
				this.errorWarning = "<control_horario>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);			
			}
			if (controlHorario.equalsIgnoreCase("ON") && servicio.getHorario() == null) {
				this.errorWarning = "<horario>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);			
			}
			if (error != null && !error.equals("")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("ctiActivo", ctiActivo));
			parametrosSalida.add(new ParamEvent("vdnTransferDef", vdnTransferDef));
			parametrosSalida.add(new ParamEvent("statCmActivo", statCmActivo));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("servActivo", servActivo));
			parametrosAdicionales.add(new ParamEvent("controlHorario", controlHorario));
			parametrosAdicionales.add(new ParamEvent("horario", horario));		
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("ctiActivo", ctiActivo);
			request.setAttribute("vdnTransferDef", vdnTransferDef);
			request.setAttribute("statCmActivo", statCmActivo);

			// ABALFARO_20170117 devuelvo el idLlamada por si vino a null y se ha generado uno nuevo
			request.setAttribute("idLlamada", this.log.getIdLlamada());
			listlogger.add(parametrosSalida.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GET_CONFIG_SERVICIO).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("ctiActivo", ctiActivo));
			parametrosSalida.add(new ParamEvent("vdnTransferDef", vdnTransferDef));
			parametrosSalida.add(new ParamEvent("servActivo", servActivo));
			parametrosSalida.add(new ParamEvent("statCmActivo", statCmActivo));
			parametrosSalida.add(new ParamEvent("controlHorario", controlHorario));
			parametrosSalida.add(new ParamEvent("horario", horario));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("ctiActivo", ctiActivo);
			request.setAttribute("vdnTransferDef", vdnTransferDef);
			request.setAttribute("statCmActivo", statCmActivo);

			// ABALFARO_20170117 devuelvo el idLlamada por si vino a null y se ha generado uno nuevo
			request.setAttribute("idLlamada", this.log.getIdLlamada());
			listlogger.add(parametrosSalida.toString());
			listlogger.add(e.getMessage().substring(20));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GET_CONFIG_SERVICIO).forward(request, response);

		}

	}
}
