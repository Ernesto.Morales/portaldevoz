package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de TRACEAR-FIN-LLAMADA en el ENRUTADOR
 * 
 * @author abalfaro
 * 
 */
public class ServletSubTracearFinLlamada extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ServletSubTracearFinLlamada() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TRACEAR_FIN_LLAMADA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String numOrigen = "";
		String numDestino = "";
		String codigoRetorno = "";

		String idAccion = "TRACEAR_FIN_LLAMADA";
		Long timeInicioLlam = new Long(0);

		String timeInicioLlamString = "";
		String retornoTransfer = "";
		String segmentoTransfer = "";
		String vdnTransfer = "";
		String isDentroHorario = "";
		
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";

		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			numOrigen = request.getParameter("VG_loggerServicio.numOrigen");
			numDestino = request.getParameter("VG_loggerServicio.numDestino");

			codigoRetorno = request.getParameter("VG_codigoRetorno");
			timeInicioLlamString = request.getParameter("VG_timeInicioLlam");

			retornoTransfer = request.getParameter("VG_transfer.retornoTransfer");
			segmentoTransfer = request.getParameter("VG_transfer.segmento");
			vdnTransfer = request.getParameter("VG_transfer.numTransfer");
			isDentroHorario = request.getParameter("VG_transfer.isDentroHorario");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			timeInicioLlam = Long.parseLong(timeInicioLlamString);

			/** INICIO EVENTO - COMENTARIO **/
			this.log.comment("timeInicioLlamString=" + timeInicioLlamString + "|timeInicioLlam=" + timeInicioLlam);
			/** FIN EVENTO - COMENTARIO **/

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesAct = new ArrayList<ParamEvent>();
			parametrosAdicionalesAct.add(new ParamEvent("timeInicioLlamString", timeInicioLlamString));
			parametrosAdicionalesAct.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (retornoTransfer != null && retornoTransfer.equals("SI")) {
				// en algun momento se quiso transferir
				parametrosAdicionalesAct = new ArrayList<ParamEvent>();
				parametrosAdicionalesAct.add(new ParamEvent("intentoTransfer", retornoTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("isDentroHorario", isDentroHorario));
			}
			this.log.actionEvent(idAccion, "OK", parametrosAdicionalesAct);
			/** FIN EVENTO - ACCION **/
			
			String duracion = this.getDuracionLlamada(timeInicioLlam) + "";

			/** INICIO EVENTO - FIN DE LLAMADA **/
			this.log.endCall(numOrigen, numDestino, codigoRetorno, duracion, null);
			/** FIN EVENTO - FIN DE LLAMADA **/

			/** INICIO EVENTO - STAT FIN DE LLAMADA **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("numOrigen", numOrigen));
			parametrosAdicionalesStat.add(new ParamEvent("numDestino", numDestino));
			parametrosAdicionalesStat.add(new ParamEvent("duracion", duracion));
			if (retornoTransfer != null && retornoTransfer.equals("SI")) {
				// en algun momento se quiso transferir
				parametrosAdicionalesStat.add(new ParamEvent("intentoTransfer", retornoTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("isDentroHorario", isDentroHorario));
			}
			this.log.statisticEvent("END_CALL", codigoRetorno, parametrosAdicionalesStat);
			/** FIN EVENTO - STAT FIN DE LLAMADA **/
			
			
			resultadoOperacion = "OK";

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("duracion", duracion);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TRACEAR_FIN_LLAMADA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			String error = e.toString();
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", error));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesAct = new ArrayList<ParamEvent>();
			parametrosAdicionalesAct.add(new ParamEvent("timeInicioLlamString", timeInicioLlamString));
			parametrosAdicionalesAct.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (retornoTransfer != null && retornoTransfer.equals("SI")) {
				// en algun momento se quiso transferir
				parametrosAdicionalesAct = new ArrayList<ParamEvent>();
				parametrosAdicionalesAct.add(new ParamEvent("intentoTransfer", retornoTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
				parametrosAdicionalesAct.add(new ParamEvent("isDentroHorario", isDentroHorario));
			}
			this.log.actionEvent(idAccion, "OK", parametrosAdicionalesAct);
			/** FIN EVENTO - ACCION **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************			
			
			String duracion = this.getDuracionLlamada(timeInicioLlam) + "";

			/** INICIO EVENTO - FIN DE LLAMADA **/
			this.log.endCall(numOrigen, numDestino, codigoRetorno, duracion, null);
			/** FIN EVENTO - FIN DE LLAMADA **/

			/** INICIO EVENTO - STAT FIN DE LLAMADA **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("numOrigen", numOrigen));
			parametrosAdicionalesStat.add(new ParamEvent("numDestino", numDestino));
			parametrosAdicionalesStat.add(new ParamEvent("duracion", duracion));
			if (retornoTransfer != null && retornoTransfer.equals("SI")) {
				// en algun momento se quiso transferir
				parametrosAdicionalesStat.add(new ParamEvent("intentoTransfer", retornoTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("vdnTransfer", vdnTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
				parametrosAdicionalesStat.add(new ParamEvent("isDentroHorario", isDentroHorario));
			}
			this.log.statisticEvent("END_CALL", codigoRetorno, parametrosAdicionalesStat);
			/** FIN EVENTO - STAT FIN DE LLAMADA **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("duracion", duracion);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TRACEAR_FIN_LLAMADA).forward(request, response);

		}

	}

	private Long getDuracionLlamada(Long milisInicioLlamada) {

		Date fechaHoy = new Date();
		Long milisecFinLlam = fechaHoy.getTime();

		Long duracionLlamada = milisecFinLlam - milisInicioLlamada;
		/** INICIO EVENTO - COMENTARIO **/
		this.log.comment("duracionLlamada=" + duracionLlamada);
		/** FIN EVENTO - COMENTARIO **/

		duracionLlamada = duracionLlamada / 1000;

		/** INICIO EVENTO - COMENTARIO **/
		this.log.comment("duracionLlamadaSec=" + duracionLlamada);
		/** FIN EVENTO - COMENTARIO **/

		return duracionLlamada;

	}
}
