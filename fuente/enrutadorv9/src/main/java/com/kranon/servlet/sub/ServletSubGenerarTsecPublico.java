package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.grantingticket.GrantingTicket;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de GENERAR-TSEC-PUBLICO en el ENRUTADOR
 * 
 * @author abalfaro
 * 
 */
public class ServletSubGenerarTsecPublico extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletSubGenerarTsecPublico() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {	
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GENERAR_TSEC_PUBLICO).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GENERAR_TSEC_PUBLICO";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String tsec = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(idModulo);
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			logKranon.setIdServicio(idLlamada);
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			this.log.initModuleService(idModulo, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			// *****************************************************
			// ***** INTEGRACIoN WS OPERACIoN GrantingTicket
			// *****************************************************
			ConexionResponse conexionResponse = null;

			// Datos FIJOS para la llamada inicial cuando no se tienen datos
			// del
			// cliente
			String backendSessionId = null;
			String clientId = null;
			String userId = "";
			String accessCode = "";
			String dialogId = "";

			/** LLAMO AL SERVICIO **/
			// [20161205-NMB] SE INCLUYE EL idServicio PARA ESCRIBIR LAS TRAZAS DE MONITORIZACION
			GrantingTicket gt = new GrantingTicket(idLlamada, idServicio, idElemento);
			gt.preparaPeticion(userId, accessCode, dialogId, backendSessionId, clientId);
			conexionResponse = gt.ejecutaPeticion();

			if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {
				// la peticion ha ido bien

				if (conexionResponse.getCodigoRespuesta() == 200) {
					tsec = gt.dameTsec(conexionResponse.getHeaderRespuesta());

					/** INICIO EVENTO - ACCION **/
					// aqui si imprimo el tsec completo
//					this.log.actionEvent("NEW_TSEC", tsec, null);
//					/** FIN EVENTO - ACCION **/

					resultadoOperacion = "OK";
					error = "";
				} else {
					resultadoOperacion = "KO";
					error = "";
				}

			} else if (conexionResponse.getResultado().equalsIgnoreCase("KO") || resultadoOperacion.equals("KO")) {
				 //se ha recibido una respuesta de KO
				resultadoOperacion = "KO";
				error = "";

			} else {
				// ha habido un error
				resultadoOperacion = "ERROR";
				error = "ERROR_EXT(WS_GRANTING_TICKET)";
			}

			// ****************************************************************

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("tsec", (tsec != null && !tsec.equals("")) ? tsec.substring(0, 20) : tsec));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** INICIO EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
//			resultadoOperacion = "OK";
//			error = "";
//			tsec = "TSECPURADFVQEFFEFD9876";
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("tsec", tsec);
			if(conexionResponse!=null){
				listlogger.add(conexionResponse.getResultado());
				listlogger.add(""+conexionResponse.getCodigoRespuesta());
			}
			listlogger.add("tsec"+tsec);		
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GENERAR_TSEC_PUBLICO).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("tsec", (tsec != null && !tsec.equals("")) ? tsec.substring(0, 20) : tsec));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** INICIO EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("tsec", tsec);
			listlogger.add(e.getMessage().substring(20));
			listlogger.add(parametrosSalida.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_GENERAR_TSEC_PUBLICO).forward(request, response);

		}

	}

}
