package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.cti.ProcesoCTI;
import com.kranon.servicios.cti.bean.BeanDatosCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servicios.cti.bean.ProcesoCTIOutDto;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesEnrutador;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de SUB-SET-DATOS-CTI en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletSubSetDatosCTI extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletSubSetDatosCTI() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_SET_DATOS_CTI).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		
		String uui = "";
		String uri = null;
		int longitud = 0;
		String longitudUUI = "00";


		String idModulo = "SET_DATOS_CTI";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		BeanDatosCTI beanDatosCTI = new BeanDatosCTI();
		BeanDatosUUI beanDatosUUI = new BeanDatosUUI();
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add("SET BEAN Datos CTI");
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");			
			
			uri = request.getParameter("VG_datosLlamada.uri");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			// relleno el bean con los datos CTI
			
			beanDatosCTI.setAni(request.getParameter("VG_datosCTI.ani"));
			beanDatosCTI.setDnis(request.getParameter("VG_datosCTI.dnis"));
			beanDatosCTI.setNumeroIvr(request.getParameter("VG_datosCTI.numeroIvr"));
			beanDatosCTI.setPuertoIvr(request.getParameter("VG_datosCTI.puertoIvr"));
			beanDatosCTI.setLlave(request.getParameter("VG_datosCTI.llave"));
			beanDatosCTI.setField(request.getParameter("VG_datosCTI.field"));
			beanDatosCTI.setVdnOrigen(request.getParameter("VG_datosCTI.vdnOrigen"));
			beanDatosCTI.setVdnDestino(request.getParameter("VG_datosCTI.vdnDestino"));
			beanDatosCTI.setSkill(request.getParameter("VG_datosCTI.skill"));
			beanDatosCTI.setAsa(request.getParameter("VG_datosCTI.asa"));
			beanDatosCTI.setIdAplicacion(request.getParameter("VG_datosCTI.idAplicacion"));
			beanDatosCTI.setXferId(request.getParameter("VG_datosCTI.xferId"));
			beanDatosCTI.setSegmentoCliente(request.getParameter("VG_datosCTI.segmentoCliente"));
			beanDatosCTI.setProducto(request.getParameter("VG_datosCTI.producto"));
			beanDatosCTI.setLlaveAccesoFront(request.getParameter("VG_datosCTI.llaveAccesoFront"));
			beanDatosCTI.setCampania(request.getParameter("VG_datosCTI.campania"));
			beanDatosCTI.setCanal(request.getParameter("VG_datosCTI.canal"));
			beanDatosCTI.setNumCliente(request.getParameter("VG_datosCTI.numCliente"));
			beanDatosCTI.setSector(request.getParameter("VG_datosCTI.sector"));
			beanDatosCTI.setNombreCliente(request.getParameter("VG_datosCTI.nombreCliente"));
			beanDatosCTI.setTelefonoContacto(request.getParameter("VG_datosCTI.telefonoContacto"));
			beanDatosCTI.setTelefonoDinamico(request.getParameter("VG_datosCTI.telefonoDinamico"));
			beanDatosCTI.setLibre1(request.getParameter("VG_datosCTI.libre1"));
			beanDatosCTI.setLibre2(request.getParameter("VG_datosCTI.libre2"));
			beanDatosCTI.setUsuarioFront(request.getParameter("VG_datosCTI.usuarioFront"));
			beanDatosCTI.setTransferenciaAsesor(request.getParameter("VG_datosCTI.transferenciaAsesor"));
			beanDatosCTI.setTransferenciaIvr(request.getParameter("VG_datosCTI.transferenciaIvr"));
			beanDatosCTI.setNumeroSesion(request.getParameter("VG_datosCTI.numeroSesion"));
			beanDatosCTI.setTiempoLlamada(request.getParameter("VG_datosCTI.tiempoLlamada"));
			beanDatosCTI.setLlaveSegmento(request.getParameter("VG_datosCTI.llaveSegmento"));
			beanDatosCTI.setIpAsesor(request.getParameter("VG_datosCTI.ipAsesor"));
			beanDatosCTI.setLibre3(request.getParameter("VG_datosCTI.libre3"));
			beanDatosCTI.setTelCtePersonas(request.getParameter("VG_datosCTI.telCtePersonas"));
			beanDatosCTI.setMetodoAutenticacion(request.getParameter("VG_datosCTI.metodoAutenticacion"));
			beanDatosCTI.setLibre4(request.getParameter("VG_datosCTI.libre4"));
			beanDatosCTI.setTimestamp(request.getParameter("VG_datosCTI.timestamp"));
			beanDatosCTI.setComentario(request.getParameter("VG_datosCTI.comentario"));
			
			// relleno el bean con los datos UUI			
			beanDatosUUI.setLlave(request.getParameter("VG_datosUUI.llave"));
			beanDatosUUI.setLlaveSegmento(request.getParameter("VG_datosUUI.llaveSegmento"));
			beanDatosUUI.setXferId(request.getParameter("VG_datosUUI.xferId"));
			beanDatosUUI.setSegmentoCliente(request.getParameter("VG_datosUUI.segmentoCliente"));
			beanDatosUUI.setCanal(request.getParameter("VG_datosUUI.canal"));
			beanDatosUUI.setNumCliente(request.getParameter("VG_datosUUI.numCliente"));
			beanDatosUUI.setLlaveAccesoFront(request.getParameter("VG_datosUUI.llaveAccesoFront"));
			beanDatosUUI.setMetodoAutenticacion(request.getParameter("VG_datosUUI.metodoAutenticacion"));
			beanDatosUUI.setNumeroSesion(request.getParameter("VG_datosUUI.numeroSesion"));
			beanDatosUUI.setCampania(request.getParameter("VG_datosUUI.campania"));			

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("beanDatosCTI", beanDatosCTI.toString()));
			parametrosEntrada.add(new ParamEvent("beanDatosUUI", beanDatosUUI.toString()));
			parametrosEntrada.add(new ParamEvent("uri", uri));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// PROCESO SET DATOS CTI
			ProcesoCTI procCti = new ProcesoCTI(idLlamada, idElemento, idServicio);
			ProcesoCTIOutDto respuesta = procCti.transferirLlamada(beanDatosCTI, beanDatosUUI);
			uui = beanDatosUUI.construyeCadena(ProcesoCTI.caracterRellenoUUI);
					
		
			// recojo el resultado del proceso
			resultadoOperacion = respuesta.getResultadoOperacion();
			error = respuesta.getCodigoError();

			//TODO: Validar contra los VDN's , pendiente respuesta Bancomer			
			if( uui != null ){
				longitud = uui.getBytes().length;
			}

			//Se convierte la cadena UUI a formato hexadecimal
			uui = UtilidadesEnrutador.convertirStringHexadecimal(uui).toUpperCase();

			//Se convierte la longitud en bytes en una cadena hexadecimal
			longitudUUI = Integer.toHexString(longitud).toUpperCase();
			
			if( longitudUUI.length() < 2 ){
				longitudUUI = "0"+longitudUUI;
			}

			//Se arma la cadena de UUI para el sip referer
			uui = UtilidadesEnrutador.ENCABEZADO_TRANSFER_UUI + longitudUUI + uui + UtilidadesEnrutador.SEPARADOR_ENCODING + UtilidadesEnrutador.CADENA_ENCODING;
			
			//Se arma la cadena URI de transferencia
			uri = uri.substring(0,uri.indexOf(":")+1 ) + respuesta.getDestino() + uri.substring( uri.indexOf("@") );

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (respuesta.getCodigoError() != null && respuesta.getCodigoError().contains("-")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCTI(idServicio, "", 
						UtilidadesEnrutador.ARRAY_ERRORES_CTI[Integer.parseInt((respuesta.getCodigoError()).replace("-", ""))], 
						respuesta.getCodigoError(), null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
            request.setAttribute("destino", uri);            
            request.setAttribute("uui", uui);

            listlogger.add(beanDatosCTI.toString());
			listlogger.add(beanDatosUUI.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_SET_DATOS_CTI).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			uri = uri.substring(0,uri.indexOf(":")+1 ) + beanDatosCTI.getVdnDestino() + uri.substring( uri.indexOf("@") );

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningCTI(idServicio, "", e.toString(), "-", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
            request.setAttribute("destino", uri);            
            request.setAttribute("uui", uui);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_SET_DATOS_CTI).forward(request, response);
		}

	}
}
