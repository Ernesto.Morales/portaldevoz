package com.kranon.servlet;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.h2.java.lang.System;

/**
 * Servlet para la peticion de INICIO-PARAMS en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletInicioParams extends ServletBaseEnrutador {
	
	private static final long serialVersionUID = 1L;

	public ServletInicioParams() {

		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el idLlamada
				
			this.doPost(request, response);

		} catch (final Exception e) {

			// como el servlet no realiza ninguna operacion, saltamos a la parte VXML
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {		
			// http://<HOST>:<IP>/msop_pvo_mult_web_enrutadorv2_14/inicioParams?codServicio=SEGUROSOPlang=es-MX
			
			// Recojo los datos de codigo de servicio e idioma
			String tipo;
			String codServicio = request.getParameter("codServicio");
			String segmento = request.getParameter("segmento");
			String lang = request.getParameter("lang");
//			if(request.getParameter("tipo")==null||request.getParameter("tipo").trim().equals("")){
//				tipo = "OTHER";
//			}else{
//				tipo = request.getParameter("tipo");
//			}
//			
//			String op = request.getParameter("op");
			// incluyo en sesion el idioma
			HttpSession session = request.getSession();
			session.setAttribute("lang", lang);
		
			// recojo la fecha de hoy
			Date fechaHoy = new Date();
			Long milisecInicioLlam = fechaHoy.getTime();
			
			// Aniado parametros de salida a la request
			request.setAttribute("milisecInicioLlam", milisecInicioLlam);
			request.setAttribute("codServicio", codServicio);
			request.setAttribute("segmento", segmento);
//			request.setAttribute("tipo", tipo);
//			request.setAttribute("op", op);
//			System.out.println("INICIOOOOOOOOOOOOOOOOOOOOOOOOOO");
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_INICIO_PARAMS).forward(request, response);

		} catch (final Exception e) {

			// como el servlet no realiza ninguna operacion, saltamos a la parte VXML
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_INICIO_PARAMS).forward(request, response);

		}

	}
}
