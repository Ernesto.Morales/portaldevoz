package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servlet.ServletBaseEnrutador;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de EJECUCION-ENCUESTA en el ENRUTADOR
 *
 * @author aarce
 *
 */
public class ServletSubEjecucionEncuesta extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletSubEjecucionEncuesta() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// aniado el codigo retorno a devolver
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_EJECUCION_ENCUESTA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "ENCUESTA_IRENE";
		String idAccion = "EJECUCION_ENCUESTA";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			String encuestaActiva = request.getParameter("PRM_encuestaActivo");
			String encuestaActivaDefecto = request.getParameter("PRM_encuestaActivoDefecto");
			String xferId = request.getParameter("PRM_xferId");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("encuestaActiva", encuestaActiva));
			parametrosEntrada.add(new ParamEvent("encuestaActivaDefecto", encuestaActivaDefecto));
			parametrosEntrada.add(new ParamEvent("xferId", (xferId != null && !xferId.equals("")) ? xferId : ""));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			/** INICIO EVENTO - STAT INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("encuestaActiva", encuestaActiva));
			parametrosAdicionalesStat.add(new ParamEvent("encuestaActivaDefecto", encuestaActivaDefecto));
			parametrosAdicionalesStat.add(new ParamEvent("xferId", (xferId != null && !xferId.equals("")) ? xferId : ""));
			this.log.statisticEvent(idModulo, "INIT", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE MODULO **/

			// ABALFARO_20170516 Obtenemos la version de la encuesta a ejecutar o del servicio llamante
			String idServicioEncuesta = "";
			if (idServicio.contains("ENCUESTAIRENE")) {
				// es una llamada reentrante con idServicio=ENCUESTAIRENEvX
				idServicioEncuesta = idServicio;
			} else {
				// es un servicio (PRSTM o LB) que al final quiere ejecutar una encuesta
				// obtenemos la version y ejecutamos la misma a la encuesta
				String versionServicio = idServicio.substring(idServicio.length() - 2);
				idServicioEncuesta = "ENCUESTAIRENE" + versionServicio;
			}
			String versionServicioEncuesta = idServicioEncuesta.substring(idServicio.length() - 1);

			// ABALFARO_20170516 rellenamos la ruta a ejecutar directamente
			String rutaEncuesta = "";
			if (versionServicioEncuesta.equals("1")) {
				// version 1
				rutaEncuesta = "/../msop_pvo_mult_web_encuestasirene_13/ENCUESTA/inicioParams";
			} else if (versionServicioEncuesta.equals("2")) {
				// version 2
				rutaEncuesta = "/../msop_pvo_mult_web_encuestasirenev2_15/ENCUESTA/inicioParams";
			} else if (versionServicioEncuesta.equals("3")) {
				// version 3
				rutaEncuesta = "/../msop_pvo_mult_web_encuestasirenev3_16/ENCUESTA/inicioParams";
			} else {
				// version 4
				rutaEncuesta = "/../msop_pvo_mult_web_encuestasirenev4_17/ENCUESTA/inicioParams";
			}

			// ABALFARO_20170516 eliminamos ENCUESTAIRENE para permitir versionado
			// concatenaremos el idServicio en cuestion
			// String nombreEncuesta = "ENCUESTAIRENE-GENERAL";
			String nombreEncuesta = "GENERAL";
			if (!encuestaActiva.equalsIgnoreCase("OFF")) {
				// es una encuesta enviada por un agente
				if (encuestaActiva.equalsIgnoreCase("#ASESOR")) {
					// Hay que comprobar el xferId
					if (xferId.equalsIgnoreCase("90113")) {
						nombreEncuesta = "ASESOR";
						// nombreEncuesta = "ENCUESTAIRENE-ASESOR";
					}
				}
				// es una encuesta al final de un servicio ivr
				else {
					if (encuestaActiva.startsWith("#")) {
						// la encuesta hay que hacerla desde la IVR
						nombreEncuesta = "ENCUESTAIRENE-" + encuestaActiva.substring(1);
						nombreEncuesta = encuestaActiva.substring(1);
					} else {
						nombreEncuesta = "TRANSFER-" + encuestaActiva;
					}
				}

				// indicamos que hay que realizar la encuesta
				encuestaActiva = "ON";
			} else {
				if (encuestaActivaDefecto.equalsIgnoreCase("ON")) {
					// Hay que comprobar el xferId para distinguir las de asesor
					if (xferId.equalsIgnoreCase("90113")) {
						// nombreEncuesta = "ENCUESTAIRENE-DEFECTO-ASESOR";
						nombreEncuesta = "DEFECTO-ASESOR";
					} else {
						// nombreEncuesta = "ENCUESTAIRENE-DEFECTO-IVR";
						nombreEncuesta = "DEFECTO-IVR";
					}

				}
			}

			// ABALFARO_20170516 concatenamos el idServicioEncuesta al nombre final de la encuesta que vamos a ejecutar
			// por ejemplo quedaria ENCUESTAIRENEv3-DEFECTO-ASESOR
			nombreEncuesta = idServicioEncuesta + "-" + nombreEncuesta;

			resultadoOperacion = "OK";

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("idServicioEncuesta", idServicioEncuesta));
			parametrosAdicionales.add(new ParamEvent("nombreEncuesta", nombreEncuesta));
			parametrosAdicionales.add(new ParamEvent("rutaEncuesta", rutaEncuesta));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("nombreEncuesta", nombreEncuesta);
			request.setAttribute("encuestaActiva", encuestaActiva);
			request.setAttribute("encuestaActivaDefecto", encuestaActivaDefecto);
			request.setAttribute("idServicioEncuesta", idServicioEncuesta);
			request.setAttribute("rutaEncuesta", rutaEncuesta);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_EJECUCION_ENCUESTA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - COMENTARIO **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - COMENTARIO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_EJECUCION_ENCUESTA).forward(request, response);

		}

	}
}
