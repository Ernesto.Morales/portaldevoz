package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de GESTION-INICIO-LLAMADA en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGestionInicioLlamada extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletGestionInicioLlamada() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// como el servlet no realiza ninguna operacion, saltamos a la parte
			// VXML
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INICIO_LLAMADA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idAccion = "GESTION_INICIO_LLAMADA";
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String servicioCtiActivo = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			servicioCtiActivo = request.getParameter("VG_servicioCtiActivo");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - ACTION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("getDatosUUI", "ON"));
			parametrosAdicionales.add(new ParamEvent("getDatosCTI", servicioCtiActivo));
			this.log.actionEvent(idAccion, "OK", parametrosAdicionales);
			/** FIN EVENTO - ACTION **/

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INICIO_LLAMADA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesErr = new ArrayList<ParamEvent>();
			parametrosAdicionalesErr.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionalesErr, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - COMENTARIO **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("getDatosUUI", "ON"));
			parametrosAdicionales.add(new ParamEvent("getDatosCTI", servicioCtiActivo));
			this.log.actionEvent(idAccion, "KO", parametrosAdicionales);
			/** FIN EVENTO - COMENTARIO **/

			String error = "ERROR_EXT(" + e.getMessage() + ")";
			
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INICIO_LLAMADA).forward(request, response);
		}

	}
}
