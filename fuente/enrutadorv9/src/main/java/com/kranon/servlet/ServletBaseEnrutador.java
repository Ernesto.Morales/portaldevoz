package com.kranon.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.serv.CommonLoggerService;

public abstract class ServletBaseEnrutador extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA EL ENRUTADOR
	protected CommonLoggerService log; 
	protected static final String PAGE_INICIO_PARAMS = "/dynamicvxml/ENRUTADOR-INICIO-PARAMS.jsp";
	protected static final String PAGE_GET_CONFIG_SERVICIO = "/dynamicvxml/ENRUTADOR-GET-CONFIG-SERVICIO.jsp";
	protected static final String PAGE_GESTION_INICIO_LLAMADA = "/staticvxml/ENRUTADOR-GESTION-INICIO-LLAMADA.jsp";
		// SUBDIALOGOS - GESTION_INICIO_LLAMADA
		protected static final String PAGE_SUB_GET_DATOS_UUI = "/subdialogos/SUB-ENRUTADOR-GET-DATOS-UUI.jsp";
		protected static final String PAGE_SUB_GET_DATOS_CTI = "/subdialogos/SUB-ENRUTADOR-GET-DATOS-CTI.jsp";
		//
	protected static final String PAGE_GESTION_PRINCIPAL = "/dynamicvxml/ENRUTADOR-GESTION-PRINCIPAL.jsp";
	protected static final String PAGE_EJECUCION_CONTROLADOR = "/staticvxml/ENRUTADOR-EJECUCION-CONTROLADOR.jsp";
	protected static final String PAGE_ERROR_OPE = "/dynamicvxml/ENRUTADOR-ERROR-OPE.jsp";
	protected static final String PAGE_GESTION_SALIDA = "/dynamicvxml/ENRUTADOR-GESTION-SALIDA.jsp";
		// SUBDIALOGOS - GESTION_INICIO_LLAMADA
	protected static final String PAGE_SUB_GESTION_EXCEPCIONES = "/subdialogos/SUB-ENRUTADOR-GESTION-EXCEPCIONES.jsp";
		//
	protected static final String PAGE_GESTION_TRANSFERENCIA_ASESOR = "/dynamicvxml/ENRUTADOR-GESTION-TRANSFERENCIA-ASESOR.jsp";
			// SUBDIALOGOS - GESTION_TRANSFERENCIA_ASESOR
			public static final String PAGE_SUB_SET_DATOS_CTI = "/subdialogos/SUB-ENRUTADOR-SET-DATOS-CTI.jsp";
			public static final String PAGE_SUB_SET_DATOS_UUI = "/subdialogos/SUB-ENRUTADOR-SET-DATOS-UUI.jsp";
			//
	protected static final String PAGE_GESTION_INTEGRACIONES_FIN_LLAMADA = "/dynamicvxml/ENRUTADOR-GESTION-INTEGRACIONES-FIN-LLAMADA.jsp";
	protected static final String PAGE_GESTION_FIN_LLAMADA = "/dynamicvxml/ENRUTADOR-GESTION-FIN-LLAMADA.jsp";
		// 	SUBDIALOGOS - GESTION_FIN_LLAMADA
		protected static final String PAGE_SUB_TERMINAR_LLAMADA_CTI = "/subdialogos/SUB-ENRUTADOR-TERMINAR-LLAMADA-CTI.jsp";
		protected static final String  PAGE_SUB_GENERAR_TSEC_PUBLICO = "/subdialogos/SUB-ENRUTADOR-GENERAR-TSEC-PUBLICO.jsp";
		protected static final String PAGE_SUB_GESTION_INTEGRACION_CUADRO_MANDO = "/subdialogos/SUB-ENRUTADOR-INTEGRACION-CUADRO-MANDO.jsp";
		protected static final String PAGE_SUB_TRACEAR_FIN_LLAMADA = "/subdialogos/SUB-ENRUTADOR-TRACEAR-FIN-LLAMADA.jsp";
	
	
	// ENCUESTAS	
	protected static final String PAGE_SUB_EJECUCION_ENCUESTA = "/subdialogos/SUB-ENRUTADOR-EJECUCION-ENCUESTA.jsp";
	protected static final String PAGE_SUB_GESTION_SALIDA_ENCUESTA = "/subdialogos/SUB-ENRUTADOR-GESTION-SALIDA-ENCUESTA.jsp";

	


	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
