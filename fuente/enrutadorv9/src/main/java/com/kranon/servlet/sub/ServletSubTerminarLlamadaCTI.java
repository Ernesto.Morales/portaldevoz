package com.kranon.servlet.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.cti.ProcesoCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servlet.ServletBaseEnrutador;

/**
 * Servlet para la peticion de TERMINAR-LLAMADA-CTI en el ENRUTADOR
 * 
 * @author abalfaro
 * 
 */
public class ServletSubTerminarLlamadaCTI extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ServletSubTerminarLlamadaCTI() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TERMINAR_LLAMADA_CTI).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String codigoRetorno = "";
		int tamano=8;
		BeanDatosUUI beanDatosUUI = new BeanDatosUUI();
		int codigoTerminacion = 0;
		String[] aux= new String[50];
		String idModulo = "TERMINAR_LLAMADA_CTI";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "OK";

		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			codigoRetorno = request.getParameter("VG_codigoRetorno");

			String servicioCtiActivo = request.getParameter("VG_servicioCtiActivo");

			// relleno el bean con los datos UUI
			beanDatosUUI.setLlave(request.getParameter("VG_datosUUI.llave"));
			beanDatosUUI.setLlaveSegmento(request.getParameter("VG_datosUUI.llaveSegmento"));
			beanDatosUUI.setXferId(request.getParameter("VG_datosUUI.xferId"));
			beanDatosUUI.setSegmentoCliente(request.getParameter("VG_datosUUI.segmentoCliente"));
			beanDatosUUI.setCanal(request.getParameter("VG_datosUUI.canal"));
			beanDatosUUI.setNumCliente(request.getParameter("VG_datosUUI.numCliente"));
			beanDatosUUI.setLlaveAccesoFront(request.getParameter("VG_datosUUI.llaveAccesoFront"));
			beanDatosUUI.setMetodoAutenticacion(request.getParameter("VG_datosUUI.metodoAutenticacion"));
			beanDatosUUI.setNumeroSesion(request.getParameter("VG_datosUUI.numeroSesion"));
			beanDatosUUI.setCampania(request.getParameter("VG_datosUUI.campania"));
			aux[0] = request.getParameter("VG_RetornoCTI.tipoOperacion");
			aux[1] = request.getParameter("VG_RetornoCTI.estatus");
			aux[2] = request.getParameter("VG_RetornoCTI.codigoRetorno");
			aux[3] = request.getParameter("VG_RetornoCTI.idIntentos");
			aux[4] = request.getParameter("VG_RetornoCTI.tipocamp");
			aux[5] = request.getParameter("VG_RetornoCTI.valorcamp");
			aux[6] = request.getParameter("VG_RetornoCTI.montocamp");
			aux[7] = request.getParameter("VG_RetornoCTI.mensaje");
			for(int i=0;i<tamano;i++){
				System.out.println("ctiii"+i+": "+aux[i]);
				
			}
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO MoDULO **/
			ArrayList<ParamEvent> parametrosAdicionalesMod = new ArrayList<ParamEvent>();
			parametrosAdicionalesMod.add(new ParamEvent("servicioCtiActivo", servicioCtiActivo));
			parametrosAdicionalesMod.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionalesMod.add(new ParamEvent("beanDatosUUI", beanDatosUUI == null ? "null" : beanDatosUUI.toString()));
			this.log.initModuleService(idModulo, parametrosAdicionalesMod);
			/** FIN EVENTO - INICIO MoDULO **/

			// / **** INTEGRACIN CON CTI - TERMINAR LLAMADA
			if (servicioCtiActivo != null) {
				this.LlenarObjetoTranferencia(request);
				/** INTEGRACION CTI JUAN CARLOS **/

				// PROCESO FINALIZAR CTI
				ProcesoCTI procCti = new ProcesoCTI(idLlamada, idElemento, idServicio);
				String[] cadenaResultado= new String[tamano];
				for(int i=0;i<tamano;i++){
					aux[i]=aux[i]!=null?aux[i]:"0";
					cadenaResultado[i]=aux[i];
				}
				System.out.println("TAM LENGTH: = "+cadenaResultado.length);
				for (int i=0;i<cadenaResultado.length;i++) {
					System.out.println("CADENA["+i+"]."+cadenaResultado[i].toString());
				}
				/*Peticion Unblock*/
				String resultadoCti=procCti.UnBlockDTMF(beanDatosUUI.getLlave());
				System.out.println("BLOCK....."+resultadoCti);
				/*Retorno CTI*/
				resultadoCti=procCti.TransactionResult(beanDatosUUI.getLlave(), cadenaResultado);
				System.out.println("Resultado de la transaccion"+resultadoCti);
				if(resultadoCti=="OK"){
					
				}
				// FIN es Colgado_IVR
				// HANGUP es Colgado_Cliente
//				codigoTerminacion = (codigoRetorno.equals("FIN")) ? ProcesoCTI.COLGADO_IVR
//				: (codigoRetorno.equals("HANGUP")) ? ProcesoCTI.COLGADO_CLIENTE
//						: ProcesoCTI.TRANSFERIDA;
//				ProcesoCTIOutDto outCti = procCti.terminarLlamada(beanDatosUUI,
//						codigoTerminacion);
//
//				String resultadoCti = (outCti == null) ? "KO" : outCti.getResultadoOperacion();


				// ABALFARO_20170117 arreglo descuadres end_call tras cti
				if (!this.log.getIdLlamada().equals(idLlamada)) {
					this.log.actionEvent("CTI", "distintos_idLlamada|log.getIdLlamada()=" + this.log.getIdLlamada() + "|idLlamada=" + idLlamada, null);
					// si los idLlamada son distintos, vuelvo a inicializar el log
					/** INICIALIZAR LOG **/
					this.log = new CommonLoggerService(idElemento + "_" + idServicio);
					this.log.inicializar(idLlamada, idServicio, idElemento);
				}

				// guardo el resultado de la integracion
				resultadoOperacion = resultadoCti;
//				error = (outCti == null) ? "" : outCti.getCodigoError();

				
				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
//				if (outCti.getCodigoError() != null && outCti.getCodigoError().contains("-")) {
//					this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
//					this.logWarning.warningCTI(idServicio, "", 
//							UtilidadesEnrutador.ARRAY_ERRORES_CTI[Integer.parseInt((outCti.getCodigoError()).replace("-", ""))], 
//							outCti.getCodigoError(), null);
//				}
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				
			}
			// / *********************************************
		
			/** INICIO EVENTO - FIN MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoTerminacion", codigoTerminacion + ""));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MoDULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TERMINAR_LLAMADA_CTI).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningCTI(idServicio, "", e.toString(), "-", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoTerminacion", codigoTerminacion + ""));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MoDULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_SUB_TERMINAR_LLAMADA_CTI).forward(request, response);

		}

	}

	private void LlenarObjetoTranferencia(HttpServletRequest request){
		Enumeration<?> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = (String) enumeration.nextElement();

			if (parameterName.contains("VG_RetornoCTI.operaciones.")) {
				// el parametro es de operaciones
					// es el parametro de operaciones codigo operacion, aprovecho y cojo toda su info
				
					// ABALFARO_20170615 obtengo el indice de la operacion actual
					int initIndex = parameterName.indexOf("VG_stat.operaciones.") + "VG_stat.operaciones.".length();
//					int endIndex = parameterName.indexOf(".codOperIVR");
					//int numOperacionActual = Integer.parseInt(parameterName.substring(initIndex));

					String operacion = request.getParameter("VG_stat.operaciones." + initIndex + ".valor");
			}
		}
	}
}
