package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.serv.BeanServicio;
import com.kranon.integracion.BeanResultadoIntegracion;
import com.kranon.integracion.IntegracionTipificador;
import com.kranon.integracion.IntegracionTxLbab;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.singleton.SingletonServicio;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de GESTION-INTEGRACIONES-FIN-LLAMADA en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGestionIntegracionesFinLlamada extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletGestionIntegracionesFinLlamada() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INTEGRACIONES_FIN_LLAMADA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GESTION_INTEGRACIONES_FIN_LLAMADA";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
//		String error = "";

		String lbabActivo = "";
		String tipifActivo = "";
		try {
			Date fechaHoy = new Date();
			Long milisecFinOperacion = fechaHoy.getTime();
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);


			
			// Recuperamos el Bean del Servicio en cuestion
			BeanServicio servicio = null;
			SingletonServicio instance = SingletonServicio.getInstance(log);
			if (instance != null) {
				servicio = instance.getServicio(idServicio);
			}

			if (servicio == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
//				error = "ERROR_EXT(SERV_NULL)";
				
				/** INICIO EVENTO - INICIO MoDULO **/
				ArrayList<ParamEvent> parametrosAdicionalesMod = new ArrayList<ParamEvent>();
				parametrosAdicionalesMod.add(new ParamEvent("servicioRecuperado", "null"));
				this.log.initModuleService(idModulo, parametrosAdicionalesMod);
				/** FIN EVENTO - INICIO MoDULO **/	

			} else {
				// hemos recuperado bien el servicio

				/** INICIO EVENTO - COMENTARIO 
				this.log.comment("GET_SERVICIO=" + servicio.toString());
				FIN EVENTO - COMENTARIO **/

				lbabActivo = servicio.getConfiguracion().getLbabActivo();
				tipifActivo = servicio.getConfiguracion().getTipifActivo();
				
				/** INICIO EVENTO - INICIO MoDULO **/
				ArrayList<ParamEvent> parametrosAdicionalesMod = new ArrayList<ParamEvent>();
				parametrosAdicionalesMod.add(new ParamEvent("lbabActivo", lbabActivo));
				parametrosAdicionalesMod.add(new ParamEvent("tipifActivo", tipifActivo));
				this.log.initModuleService(idModulo, parametrosAdicionalesMod);
				/** FIN EVENTO - INICIO MoDULO **/				

				if (lbabActivo != null && lbabActivo.equalsIgnoreCase("ON")) {
					// INTEGRACIoN WEB SERVICE (TX LBAB)
					IntegracionTxLbab integracion = new IntegracionTxLbab(this.log.getIdLlamada(), this.log.getIdElemento());
					BeanResultadoIntegracion beanResultado = integracion.realizarIntegracion();

					/** INICIO EVENTO - ACCION **/
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					if(beanResultado.getError() != null && !beanResultado.getError().equals("")){
						parametrosAdicionales.add(new ParamEvent("error", beanResultado.getError()));
					}
					this.log.actionEvent("WS_TX_LBAB", beanResultado.getResultadoOperacion(), parametrosAdicionales);
					/** FIN EVENTO - ACCION **/

					resultadoOperacion = beanResultado.getResultadoOperacion();
				}


				if (tipifActivo != null && tipifActivo.equalsIgnoreCase("ON")) {
					// INTEGRACIoN TIPIFICADOR
					IntegracionTipificador integracion = new IntegracionTipificador(this.log.getIdLlamada(), this.log.getIdElemento());
					BeanResultadoIntegracion beanResultado = integracion.realizarIntegracion();

					/** INICIO EVENTO - ACCION **/
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					if(beanResultado.getError() != null && !beanResultado.getError().equals("")){
						parametrosAdicionales.add(new ParamEvent("error", beanResultado.getError()));
					}
					this.log.actionEvent("WS_TIPIFICADOR", beanResultado.getResultadoOperacion(), parametrosAdicionales);
					/** FIN EVENTO - ACCION **/

					resultadoOperacion = beanResultado.getResultadoOperacion().equalsIgnoreCase("KO") ? "KO" : resultadoOperacion;
				}

			}

			if (resultadoOperacion.equalsIgnoreCase("")) {
				resultadoOperacion = "OK";
			}

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);

			/** INICIO EVENTO - FIN MoDULO **/
			this.log.endModuleService(idModulo, resultadoOperacion, null, null);
			/** FIN EVENTO - FIN MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (lbabActivo == null) {
				this.errorWarning = "<lbab_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
			}
			if (tipifActivo == null) {
				this.errorWarning = "<tipif_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			request.setAttribute("milisecFinOperacion", milisecFinOperacion);
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INTEGRACIONES_FIN_LLAMADA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}
		
			resultadoOperacion = "KO";
			String error = e.toString();
	
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("lbabActivo", lbabActivo));
			parametrosSalida.add(new ParamEvent("tipifActivo", tipifActivo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MoDULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
	
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_INTEGRACIONES_FIN_LLAMADA).forward(request, response);

		}

	}
}
