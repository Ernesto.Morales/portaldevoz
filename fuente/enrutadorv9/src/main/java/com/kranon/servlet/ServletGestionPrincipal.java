package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.bean.gestioncontroladores.BeanConfigControlador;
import com.kranon.bean.gestioncontroladores.BeanGestionControladores;
import com.kranon.bean.gestioncontroladores.BeanInfoControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.singleton.SingletonGestionControladores;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de GESTION-PRINCIPAL en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletGestionPrincipal extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletGestionPrincipal() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_PRINCIPAL).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GESTION_PRINCIPAL";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String vdnOrigen = request.getParameter("PRM_vdnOrigen");
			String xferId = request.getParameter("PRM_xferId");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("codServIvr", idServicio));
			parametrosEntrada.add(new ParamEvent("vdnOrigen", vdnOrigen));
			parametrosEntrada.add(new ParamEvent("xferId", xferId));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean de Gestion Controladores en cuestion
			BeanGestionControladores gestionControladores = null;
			SingletonGestionControladores instance = SingletonGestionControladores.getInstance(log);
			if (instance != null) {
				gestionControladores = instance.getGestionControladores(idServicio);
			}

			String nombreControlador = "";
			if (gestionControladores == null) {
				// error al recuperar el controlador
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(GEST_CONTROL_NULL)";
			} else {
				// hemos recuperado bien la gestion de controladores
				// buscamos el controlador que nos interesa

				// busco su nombre por el segmento
				for (BeanConfigControlador config : gestionControladores.getConfiguracion()) {
					if (config.getVdnOrigen() != null && config.getVdnOrigen().equals(vdnOrigen)) {
						// es el controlador que queremos usar
						/**
						 * INICIO EVENTO - COMENTARIO
						 * this.log.comment("GET_CONFIG_CONTROLADOR=" + config);
						 * FIN EVENTO - COMENTARIO
						 **/

						nombreControlador = config.getNombreControlador();

						/** INICIO EVENTO - COMENTARIO **/
						this.log.comment("GET_NOMBRE_CONTROLADOR=" + nombreControlador);
						/** FIN EVENTO - COMENTARIO **/

						break;
					}
				}

				if (nombreControlador.equals("")) {
					// no hay ningun controlador definido para ese vdn y xfer
					// error al recuperar el controlador
					resultadoOperacion = "KO";
					codigoRetorno = "ERROR";
					error = "ERROR_EXT(CONTROL_NO_EXISTE)";
				} else {

					// busco el controlador por el nombre
					BeanInfoControlador infoControlador = null;
					for (BeanInfoControlador c : gestionControladores.getControladores()) {
						if (c.getNombre().equals(nombreControlador)) {
							// es el controlador que queremos usar
							infoControlador = c;
							break;
						}
					}
					if(infoControlador == null){
						// error al recuperar el infoControlador
						resultadoOperacion = "KO";
						codigoRetorno = "ERROR";
						error = "ERROR_EXT(INFO_CONTROL_NULL)";
					} else {
	
						// aado el controlador a la request
						request.setAttribute("infoControlador", infoControlador);
		
						/** INICIO EVENTO - COMENTARIO **/
						this.log.comment("GET_CONTROLADOR=" + infoControlador.toString());
						/** FIN EVENTO - COMENTARIO **/
		
						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";
					}
				}
			}

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (error != null && !error.equals("")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null & !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("nombreControlador", nombreControlador));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_PRINCIPAL).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_GESTION_PRINCIPAL).forward(request, response);

		}

	}
}
