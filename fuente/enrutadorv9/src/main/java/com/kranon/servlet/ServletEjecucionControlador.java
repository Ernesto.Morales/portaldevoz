package com.kranon.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de EJECUCION-CONTROLADORL en el ENRUTADOR
 *
 * @author abalfaro
 *
 */
public class ServletEjecucionControlador extends ServletBaseEnrutador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletEjecucionControlador() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			this.doPost(request, response);

		} catch (final Exception e) {

			// aniado el codigo retorno a devolver
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_EJECUCION_CONTROLADOR).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idAccion = "EJECUCION_CONTROLADOR";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(UtilidadesLoggerKranon.envia+UtilidadesLoggerKranon.linea);
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String nombreControlador = request.getParameter("VG_infoControlador.nombre");
			String rutaControlador = request.getParameter("VG_infoControlador.ruta");
			String isActivoControlador = request.getParameter("VG_infoControlador.isActivo");
			String esquema = request.getParameter("VG_esquema");
			String producto = request.getParameter("VG_producto");
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idElemento + "_" + idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("nombreControlador", nombreControlador));
			parametrosAdicionales.add(new ParamEvent("rutaControlador", rutaControlador));
			parametrosAdicionales.add(new ParamEvent("isActivoControlador", isActivoControlador));
			parametrosAdicionales.add(new ParamEvent("esquema", esquema));
			parametrosAdicionales.add(new ParamEvent("producto", producto));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			listlogger.add("rutaControlador: "+rutaControlador);
			listlogger.add(resultadoOperacion);
			listlogger.add(codigoRetorno);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
//			UNNUEVOPASO
//			UtilidadesLoggerLllamada.LlenarUnPasoNuevo("EJECUCION_CONTROLADOR", "_", codigoRetorno, resultadoOperacion, "VG_error:"+error);
//			UtilidadesLoggerLllamada.verificarOK(UtilidadesLoggerLllamada.ENRUTADOR);
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_EJECUCION_CONTROLADOR).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idElemento + "_" + idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - COMENTARIO **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - COMENTARIO **/

			// Aniado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_ENRUTADOR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			listlogger.add(parametrosAdicionales.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(codigoRetorno);
			listlogger.add(error);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_EJECUCION_CONTROLADOR).forward(request, response);

		}

	}
}
