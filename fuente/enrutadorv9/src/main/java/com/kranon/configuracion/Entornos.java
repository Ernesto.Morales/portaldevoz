package com.kranon.configuracion;


public class Entornos{

    private static String entorno = "pr";
    
    public Entornos(){
    	
    }
    
    public static String recuperaEntorno(){
        
    	/*Esta ruta devolvera algo como  /usr/local/xx/websphere_0800/profiles/nodo
    	 * Donde xx es el entorno
    	 */
    	String path = System.getProperties().getProperty("user.dir"); 
    	    	
    	if(path.contains("/de/")){
    		entorno = "de";
    	}else if(path.contains("/ei/")){
    		entorno = "ei";
    	}else if(path.contains("/pr/")){
    		entorno = "pr";
    	}else{
    		entorno = "local";
    	}
    	
    	return entorno;
    }
}
