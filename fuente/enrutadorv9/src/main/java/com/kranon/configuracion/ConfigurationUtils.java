package com.kranon.configuracion;

import java.io.InputStream;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

public class ConfigurationUtils implements ServletContextListener {

	private static final String cfgFile = "/gce_mx_web_ENRUTADOR.cfg";

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

		String value = "";

		try {
			Properties props = new Properties();

			InputStream inputStream = getClass().getResourceAsStream(cfgFile);

			props.load(inputStream);

			value = props.getProperty("log4jPath");

			inputStream.close();

			PropertyConfigurator.configure(value);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	public static String getCfgFile() {
		return cfgFile;
	}

	public static String getEnvironment(){

		String entorno = "de";
		try {
			Context ctx = new InitialContext();
			entorno = (String) ctx.lookup( "java:comp/env/ENTORNO" );
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		return entorno;
	}
}
