package com.kranon.integracion;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.kranon.bean.gestioncm.BeanEstadisticaCM;
import com.kranon.bean.gestioncm.BeanGestionCuadroMando;
import com.kranon.bean.gestioncm.BeanInfoContadorCM;
import com.kranon.bean.stat.contador.BeanStatContador;
import com.kranon.bean.stat.contador.BeanStatInfoContador;
import com.kranon.bean.stat.llamada.BeanStatInfoOperacion;
import com.kranon.bean.stat.llamada.BeanStatLlamada;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.marshall.Marshall;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.proxyMGCET001010101MX.ProxyMGCET001010101MX;
import com.kranon.singleton.SingletonGestionCuadroMando;
import com.kranon.util.UtilidadesCuadroMando;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Clase para realizar la Integracion con el Cuadro de Mando
 * 
 * @author abalfaro
 *
 */
public class IntegracionCuadroMando {

	private CommonLoggerProcess log;
	// private String idInvocacion;
	// private String idElemento;
	private String idServicio;
	
	private final String idProceso = "INTEGRACION_CUADRO_MANDO";

	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	
	public IntegracionCuadroMando(String idInvocacion, String idElemento, String idServicio) {
		// recogemos el idInvocacion e idElemento desde donde se invoca, pero
		// escribiremos en un fichero de Log nuevo
		if (log == null) {
			log = new CommonLoggerProcess("INTEGRACION_" + idServicio.toUpperCase());
		}
		this.log.inicializar(idInvocacion, idElemento);
		// this.idInvocacion = idInvocacion;
		// this.idElemento = idElemento;
		this.idServicio = idServicio;
	}

	
	
	public BeanResultadoIntegracionCM realizarIntegracionServicio(String tsecPublico, BeanStatLlamada llamadaStat, BeanStatContador contadorStat) 
	{
		// inicializo el resultado
		BeanResultadoIntegracionCM resultadoIntegracion = new BeanResultadoIntegracionCM();
		resultadoIntegracion.setResultadoOperacion("KO");
		resultadoIntegracion.setError("");
		String idLlamada="";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(idProceso);
		try {
			/** INICIO EVENTO - INICIO DE PROCESO **/
			log.initProcess(idProceso, "", null, null);
			/** FIN EVENTO - INICIO DE PROCESO **/
			logKranon.setIdServicio(llamadaStat.getCodLlamIVR());
			// convierto todos los datos de milisegundos al formato de fecha
			String formatDateLLamada = "yyyy-MM-dd HH:mm:ss,SSS";
			String timeEntrada = convertirMilis2Date(llamadaStat.getTimeEntradaMilis(), formatDateLLamada);
			llamadaStat.setTimeEntrada(timeEntrada);

			// recojo la fecha de salida, debo calcularla en java para que coja la del servidor igual que la de entrada
			Date fechaHoy = new Date();
			Long milisecFinLlam = fechaHoy.getTime();
			String timeSalida = convertirMilis2Date(milisecFinLlam + "", formatDateLLamada);
			// String timeSalida = convertirMilis2Date(llamadaStat.getTimeSalidaMilis(), formatDateLLamada);
			llamadaStat.setTimeSalida(timeSalida);
			log.comment("timeEntrada=" + timeEntrada);
			log.comment("timeSalida=" + timeSalida);

			if (llamadaStat.getOperaciones() != null) {
				for (BeanStatInfoOperacion infoOp : llamadaStat.getOperaciones()) {
					log.comment("operacion con nombre=" + infoOp.getCodOperIVR());
					String inicioOp = convertirMilis2Date(infoOp.getInicioOperacionMilis(), formatDateLLamada);
					infoOp.setInicioOperacion(inicioOp);
					String finOp = convertirMilis2Date(infoOp.getFinOperacionMilis(), formatDateLLamada);
					infoOp.setFinOperacion(finOp);

					log.comment("inicioOp=" + inicioOp);
					log.comment("finOp=" + finOp);
				}
			}

			// calculo la fecha de los contadores
			String feAccionIV = convertirMilis2Date((new Date()).getTime() + "", "yyyy-MM-dd");
			contadorStat.setFeAccionIV(feAccionIV);
			log.comment("feAccionIV=" + feAccionIV);

			// 1. Recuperamos el Bean de Gestion Cuadro Mando
			BeanGestionCuadroMando gestionCuadroMando = null;
			SingletonGestionCuadroMando instance = SingletonGestionCuadroMando.getInstance(log);
			if (instance != null) {
				gestionCuadroMando = instance.getGestionCuadroMando(llamadaStat.getCodServIVR());
			}

			if (gestionCuadroMando == null) {
				// no se ha recuperado bien el xml de gestion cuadro de mando
				throw new Exception("GEST_CUADRO_MANDO_NULL");
			}

			log.comment("contadorStat.getContadores()=" + contadorStat.getContadores() == null ? "null" : "no es null");

			// ABALFARO_20170615 me guardo el codigoRetorno de la llamada

			// 2.1 Traducimos cada contador
			if (contadorStat.getContadores() != null) {
				
				Iterator<BeanStatInfoContador> iterator = contadorStat.getContadores().iterator();
				while (iterator.hasNext()) {
//				for (BeanStatInfoContador infoCont : contadorStat.getContadores()) {
					BeanStatInfoContador infoCont=iterator.next();
					log.comment("busco estadistica con nombre=" + infoCont.getNombre());
					// 2.1.1. Recupero la info de la estadistica con el nombre del contador
					BeanEstadisticaCM beanEstadistica = gestionCuadroMando.getTablaEstadisticas().get(infoCont.getNombre());

					log.comment("beanEstadistica=" + beanEstadistica == null ? "null" : "no es null");

					if (beanEstadistica != null) {
						// he encontrado la estadistica

						// creo el objeto para buscar dentro de la estadistica con los datos del contador
						BeanInfoContadorCM infoEstadistica = new BeanInfoContadorCM();
						infoEstadistica.setType(infoCont.getTipo());
						infoEstadistica.setCodigoRetorno(infoCont.getCodigoRetorno());
						infoEstadistica.setParam1(infoCont.getParam1());
						infoEstadistica.setParam2(infoCont.getParam2());
						infoEstadistica.setParam3(infoCont.getParam3());
						infoEstadistica.setParam4(infoCont.getParam4());
						infoEstadistica.setParam5(infoCont.getParam5());

						log.comment("infoEstadistica=" + infoEstadistica.toString());

						for (Map.Entry<BeanInfoContadorCM, String> entry : beanEstadistica.getTablaContadores().entrySet()) {
							log.comment("clave=" + entry.getKey().toString() + ", valor=" + entry.getValue().toString());
						}

						// 2.1.2. Busco el contador dentro de la estadistica
						String codigoAccionContador = beanEstadistica.getTablaContadores().get(infoEstadistica);

						log.comment("codigoAccionContador=" + codigoAccionContador);

						if (codigoAccionContador == null || codigoAccionContador == "") {
							iterator.remove();
//							contadorStat.getContadores().remove(infoCont);
							// TODO como medida de contingencia para ver si algo falla, en caso de no encontrar el contador, imprimir los datos que
							// se han buscado
							// pero no se han encontrado, para ver si es error en los datos o que se debe aadir esa estadistica
//							codigoAccionContador = infoCont.getNombre() + "|" + infoEstadistica.getType() + "|" + infoEstadistica.getCodigoRetorno()
//									+ "|" + infoEstadistica.getParam1() + "|" + infoEstadistica.getParam2() + "|" + infoEstadistica.getParam3() + "|"
//									+ infoEstadistica.getParam4() + "|" + infoEstadistica.getParam5();
						}
						else{
						// 2.1.3. Guardo el cdigo del contador encontrado
						infoCont.setCodAccIVR(codigoAccionContador);
						listlogger.add(codigoAccionContador);
					}
				}
				}
				
			}
			
			// 2.2 Traducimos cada operacion
			if (llamadaStat.getOperaciones() != null) {
				
				for (BeanStatInfoOperacion infoOper : llamadaStat.getOperaciones()) {

					// 2.2.1. Recupero la info de la estadistica con el nombre del contador
					BeanEstadisticaCM beanEstadistica = gestionCuadroMando.getTablaEstadisticas().get(infoOper.getCodOperIVR());

					log.comment("beanEstadistica=" + beanEstadistica == null ? "null" : "no es null");

					if (beanEstadistica != null) {
						// he encontrado la estadistica

						// ABALFARO_20170626 nuevo codigo de operacion en la estadistica
						// creo el objeto para buscar dentro de la estadistica con los datos del contador de inicio
//						BeanInfoContadorCM infoEstadistica4CodOper = new BeanInfoContadorCM();
//						infoEstadistica4CodOper.setType("INIT");
//						infoEstadistica4CodOper.setCodigoRetorno("");
//						infoEstadistica4CodOper.setParam1("");
//						infoEstadistica4CodOper.setParam2("");
//						infoEstadistica4CodOper.setParam3("");
//						infoEstadistica4CodOper.setParam4("");
//						infoEstadistica4CodOper.setParam5("");

//						log.comment("infoEstadistica4CodOper=" + infoEstadistica4CodOper.toString());

						// creo el objeto para buscar dentro de la estadistica con los datos del contador de fin con el resultado almacenado
						BeanInfoContadorCM infoEstadistica4Result = new BeanInfoContadorCM();
						infoEstadistica4Result.setType("END");
						infoEstadistica4Result.setCodigoRetorno(infoOper.getCodRslIVR());
						infoEstadistica4Result.setParam1("");
						infoEstadistica4Result.setParam2("");
						infoEstadistica4Result.setParam3("");
						infoEstadistica4Result.setParam4("");
						infoEstadistica4Result.setParam5("");

//						log.comment("infoEstadistica4Result=" + infoEstadistica4Result.toString());

						for (Map.Entry<BeanInfoContadorCM, String> entry : beanEstadistica.getTablaContadores().entrySet()) {
							log.comment("clave=" + entry.getKey().toString() + ", valor=" + entry.getValue().toString());
						}

						// 2.2.2. Busco el contador dentro de la estadistica para la operacion y el resultado de la misma
						// ABALFARO_20170626 nuevo codigo de operacion en la estadistica
						String codigoOperacionIVR = beanEstadistica.getOperacion();
//						String codigoOperacionIVR = beanEstadistica.getTablaContadores().get(infoEstadistica4CodOper);
						String codigoResultadoIVR = beanEstadistica.getTablaContadores().get(infoEstadistica4Result);

						log.comment("codigoOperacionIVR=" + codigoOperacionIVR);
						log.comment("codigoResultadoIVR=" + codigoResultadoIVR);

						// ABALFARO_20170626 nuevo codigo de operacion en la estadistica, no se necesita tratar el codOperacion
						// 2.2.3. Para la operacion, en caso de que no sea null, se almacenan las dos primeras posiciones.
						// Ejemplo: codigoOperacionIVR = B00 --> B0
//						if (codigoOperacionIVR == null) {
							// TODO como medida de contingencia para ver si algo falla, en caso de no encontrar el contador, imprimir los datos que
							// se han buscado pero no se han encontrado, para ver si es error en los datos o que se debe aadir esa estadistica
//							codigoOperacionIVR = infoOper.getCodOperIVR();
//						} else {
//							codigoOperacionIVR = codigoOperacionIVR.substring(0, codigoOperacionIVR.length() - 1);
//							log.comment("codigoOperacionIVR=" + codigoOperacionIVR);
//						}

						// 2.2.4. Para el resultado, en caso de que no sea null, la ultima posicion precedida de 00.
						// Ejemplo: codigoResultadoIVR = B04 --> 004
						if (codigoResultadoIVR == null) {
							// TODO como medida de contingencia para ver si algo falla, en caso de no encontrar el contador, imprimir los datos que
							// se han buscado pero no se han encontrado, para ver si es error en los datos o que se debe aadir esa estadistica
							codigoResultadoIVR = infoOper.getCodRslIVR();
						} else {
							codigoResultadoIVR = "00".concat(codigoResultadoIVR.substring(codigoResultadoIVR.length() - 1));
							log.comment("codigoResultadoIVR=" + codigoResultadoIVR);
						}

						// 2.2.5. Guardo el cdigo del contador encontrado
						infoOper.setCodOperIVR(codigoOperacionIVR);
						infoOper.setCodRslIVR(codigoResultadoIVR);
					}
				}
				listlogger.add(llamadaStat.getOperaciones().toString());
			}

			// 3. Volcamos toda la informacin de la llamada en los XML llamada y contador
			Marshall procMarshal = new Marshall(log);
			procMarshal = new Marshall(log);
			// genero el XML de StatLlamada
			File fileStatLlamada = procMarshal.marshallStatLlamada(llamadaStat);
			// genero el XML de StatContador
			File fileStatContador = procMarshal.marshallStatContador(llamadaStat.getCodLlamIVR(), llamadaStat.getCodServIVR(), contadorStat);

			// 4. Comprobamos si debemos invocar al WS
			if (gestionCuadroMando.getConfiguracion().getInvocacionWS() == null
					|| !gestionCuadroMando.getConfiguracion().getInvocacionWS().equalsIgnoreCase("ON")) {
				// no debo invocar a WS de cuadro de mando
				resultadoIntegracion.setIntegracionWS("OFF");

			} else {
				// s debo invocar a WS de cuadro de mando

				// inicializo el resultado a KO
				resultadoIntegracion.setIntegracionWS("KO");

				// 5. Generar el base 64 de los ficheros
				FileInputStream fisStatLlamada = new FileInputStream(fileStatLlamada);
				byte[] fileContentStatLlamada = new byte[(int) fileStatLlamada.length()];
				fisStatLlamada.read(fileContentStatLlamada);
				byte[] encodedStatLlamada = Base64.encodeBase64(fileContentStatLlamada, false);
				String base64StatLlamada = new String(encodedStatLlamada);
				fisStatLlamada.close();

				log.comment("base64StatLlamada=" + base64StatLlamada);

				FileInputStream fisStatContador = new FileInputStream(fileStatContador);
				byte[] fileContentStatContador = new byte[(int) fileStatContador.length()];
				fisStatContador.read(fileContentStatContador);
				byte[] encodedStatContador = Base64.encodeBase64(fileContentStatContador, false);
				String base64StatContador = new String(encodedStatContador);
				fisStatContador.close();

				log.comment("base64StatContador=" + base64StatContador);

				// *****************************************************
				// ***** INTEGRACIoN WS OPERACIoN CreateInternalTransfer
				// *****************************************************
				ConexionResponse conexionResponse = null;

				/** INICIO EVENTO - ACCION **/
				this.log.actionEvent("EXECUTING_PROXY_MGCET001010101MX", "", null);
				/** FIN EVENTO - ACCION **/

				/** LLAMO AL SERVICIO **/
				// [20161216-NMB] SE INCLUYE EL idServicio PARA ESCRIBIR LAS TRAZAS DE MONITORIZACION
				ProxyMGCET001010101MX proxyMGCET001010101MX = new ProxyMGCET001010101MX(log.getIdInvocacion(), idServicio, log.getIdElemento());
				proxyMGCET001010101MX.preparaPeticion(tsecPublico, llamadaStat.getCodLlamIVR(), base64StatLlamada, base64StatContador);
				conexionResponse = proxyMGCET001010101MX.ejecutaPeticion();

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionalesAccion = new ArrayList<ParamEvent>();
				parametrosAdicionalesAccion.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
				if (conexionResponse.getErrorResponse() != null) {
					parametrosAdicionalesAccion.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
				}
				this.log.actionEvent("EXECUTED_PROXY_MGCET001010101MX", conexionResponse.getResultado(), parametrosAdicionalesAccion);
				/** FIN EVENTO - ACCION **/

				// La peticion ha ido OK
				if (conexionResponse.getResultado().equals("OK")) {
					// la peticion ha ido OK-200

					// ProxyMGCET001010101MXOutDto resultadoCM = proxyMGCET001010101MX.parserSalida(conexionResponse.getMensajeRespuesta());
					// this.log.actionEvent("RESULTADO_CM", resultadoCM.toString(), null);

					this.log.actionEvent("RESULTADO_CM", conexionResponse.getMensajeRespuesta(), null);

					// buscamos la cadena de OK directamente en el string sin parsear
					if (conexionResponse.getMensajeRespuesta().contains("Archivos procesados correctamente")) {
						// marcamos que la integracin ha ido bien
						resultadoIntegracion.setIntegracionWS("OK");
					} else {
						// hemos recibido un 200-OK pero el json de respuesta indica respuesta KO
						resultadoIntegracion.setIntegracionWS("KO");
					}
				} else if (conexionResponse.getResultado().equals("KO")) {
					// la peticion ha fallado
					resultadoIntegracion.setIntegracionWS("KO");

					if (conexionResponse.getErrorResponse() != null) {
						resultadoIntegracion.setIntegracionWSCodigoError(conexionResponse.getErrorResponse().getErrorCode());
						resultadoIntegracion.setIntegracionWSMensajeError(conexionResponse.getErrorResponse().getErrorMessage());

						this.log.actionEvent("RESULTADO_CM", conexionResponse.getErrorResponse().toString(), null);
					}

					if (conexionResponse.getMensajeRespuesta() != null) {
						resultadoIntegracion.setIntegracionWSCodigoError(conexionResponse.getCodigoRespuesta() + "");
						resultadoIntegracion.setIntegracionWSMensajeError(conexionResponse.getMensajeRespuesta());

						this.log.actionEvent("RESULTADO_CM", conexionResponse.getMensajeRespuesta().toString(), null);
					}

				} else {
					// ha habido un error - timeout

					resultadoIntegracion.setIntegracionWS("KO");
					resultadoIntegracion.setIntegracionWSCodigoError("0");
					resultadoIntegracion.setIntegracionWSMensajeError("Timeout");
				}

			}

			// ABALFARO_20170615
			// 5. Comprobamos si debemos respaldar los ficheros de llamadas y contadores, si ha fallado el WS de integracion
			if (resultadoIntegracion.getIntegracionWS().equalsIgnoreCase("KO")) {
				// la integracion ha fallado, debemos renombrar los ficheros

				UtilidadesBeans util = new UtilidadesBeans(log);
				String path = util.leeParametroMarshall("PATH_XML");

				// INICIO - RESPALDO LLAMADA
				String pathLlamada = fileStatLlamada.getAbsolutePath();
				String nombreFileLlamada = fileStatLlamada.getName();
				
				File pathllamadaFile = new File(pathLlamada);
				log.comment("EXISTE FICH LLAMADA ORIGEN=" + (pathllamadaFile.exists()?"SI": "NO"));

				log.comment("nombreFileLlamada=" + nombreFileLlamada);

				String resultStatLlamadaOK = util.leeParametroMarshall("RESULT_OK_XML_STAT_LLAMADA");
				String resultStatLlamadaKO = util.leeParametroMarshall("RESULT_KO_XML_STAT_LLAMADA");
				String nombreFileLlamadaRespaldo = nombreFileLlamada.replace(resultStatLlamadaOK, resultStatLlamadaKO);

				log.comment("nombreFileLlamadaRespaldo=" + nombreFileLlamadaRespaldo);

				String pathRespaldoLlam = util.leeParametroMarshall("PATH_XML_STAT_LLAMADA_RESP");
				String filePathRespaldoLlam = path + pathRespaldoLlam + nombreFileLlamadaRespaldo;
				if (filePathRespaldoLlam.contains("<cod_servivr>")) {
					filePathRespaldoLlam = filePathRespaldoLlam.replace("<cod_servivr>", llamadaStat.getCodServIVR().toUpperCase());
				}
				log.comment("filePathRespaldoLlam=" + filePathRespaldoLlam);

				// // Copio el fichero de llamadas a su respaldo
				UtilidadesCuadroMando.copiaFicheroStat(log, pathLlamada, filePathRespaldoLlam);
				
				log.comment("EXISTE FICH LLAMADA ORIGEN=" + (pathllamadaFile.exists()?"SI": "NO"));
				// FIN - RESPALDO LLAMADA

				// INICIO - RESPALDO CONTADORES
				String pathContadores = fileStatContador.getAbsolutePath();
				String nombreFileContadores = fileStatContador.getName();

				File pathContadoresFile = new File(pathContadores);
				log.comment("EXISTE FICH CONTADOR ORIGEN=" + (pathContadoresFile.exists()?"SI": "NO"));
				
				log.comment("nombreFileContadores=" + nombreFileContadores);

				// ABALFARO_20170615
				// nuevoNombreFile = nombreFile.replace("contador", "resp_contador");
				String resultStatContadoresOK = util.leeParametroMarshall("RESULT_OK_XML_STAT_CONTADOR");
				String resultStatContadoresKO = util.leeParametroMarshall("RESULT_KO_XML_STAT_CONTADOR");
				String nombreFileContadoresRespaldo = nombreFileContadores.replace(resultStatContadoresOK, resultStatContadoresKO);

				log.comment("nombreFileContadoresRespaldo=" + nombreFileContadoresRespaldo);

				String pathRespaldoCont = util.leeParametroMarshall("PATH_XML_STAT_CONTADOR_RESP");
				String filePathRespaldoCont = path + pathRespaldoCont + nombreFileContadoresRespaldo;
				if (filePathRespaldoCont.contains("<cod_servivr>")) {
					filePathRespaldoCont = filePathRespaldoCont.replace("<cod_servivr>", llamadaStat.getCodServIVR().toUpperCase());
				}
				log.comment("filePathRespaldoCont=" + filePathRespaldoCont);

				// Copio el fichero de llamadas a su respaldo
				UtilidadesCuadroMando.copiaFicheroStat(log, pathContadores, filePathRespaldoCont);
				
				log.comment("EXISTE FICH CONTADOR ORIGEN=" + (pathContadoresFile.exists()?"SI": "NO"));
				// FIN - RESPALDO CONTADORES

			}

			// 6. Comprobamos si debemos eliminar el fichero de llamadas
			if (gestionCuadroMando.getConfiguracion().getBorradoFicheroLlamadas() == null
					|| !gestionCuadroMando.getConfiguracion().getBorradoFicheroLlamadas().equalsIgnoreCase("ON")) {
				// no hay que borrar el archivo de llamadas
				resultadoIntegracion.setBorradoFicheroLlamadas("OFF");
			} else {
				// el borrado esta activo

				// inicializo el resultado a KO
				resultadoIntegracion.setBorradoFicheroLlamadas("KO");

				if (!resultadoIntegracion.getIntegracionWS().equalsIgnoreCase("KO")) {
					// la integracion estaba deshabilitada o se ha realizado correctamente podemos borrar los ficheros
					boolean deleteStatLlamada = fileStatLlamada.delete();
					fileStatLlamada.deleteOnExit();

					log.comment("deleteStatLlamada=" + deleteStatLlamada);

					if (deleteStatLlamada) {
						// el borrado ha ido bien
						resultadoIntegracion.setBorradoFicheroLlamadas("OK");
					}

				}
			}

			// 7. Comprobamos si debemos eliminar el fichero de contadores
			if (gestionCuadroMando.getConfiguracion().getBorradoFicheroContadores() == null
					|| !gestionCuadroMando.getConfiguracion().getBorradoFicheroContadores().equalsIgnoreCase("ON")) {
				// no hay que borrar el archivo de contadores
				resultadoIntegracion.setBorradoFicheroContadores("OFF");

			} else {
				// el borrado esta activo

				// inicializo el resultado a KO
				resultadoIntegracion.setBorradoFicheroContadores("KO");

				// el borrado esta activo
				if (!resultadoIntegracion.getIntegracionWS().equalsIgnoreCase("KO")) {
					// la integracion estaba deshabilitada o se ha realizado correctamente podemos borrar los ficheros
					boolean deleteStatContador = fileStatContador.delete();
					fileStatContador.deleteOnExit();

					log.comment("deleteStatContador=" + deleteStatContador);

					if (deleteStatContador) {
						// el borrado ha ido bien
						resultadoIntegracion.setBorradoFicheroContadores("OK");
					}
				}

			}

			// 8. Calculamos finalmente el resultado de operacion
			if (resultadoIntegracion.getIntegracionWS().equals("OFF")) {
				// la integracion estaba deshabilitada
				if (resultadoIntegracion.getBorradoFicheroContadores().equals("OFF")
						&& resultadoIntegracion.getBorradoFicheroContadores().equals("OFF")) {
					// el borrado esta deshabilitado
					resultadoIntegracion.setResultadoOperacion("OK");
				} else {
					// el borrado esta habilitado
					if (resultadoIntegracion.getBorradoFicheroLlamadas().contains("OK")
							&& resultadoIntegracion.getBorradoFicheroContadores().contains("OK")) {
						// el respaldo ha ido bien
						resultadoIntegracion.setResultadoOperacion("OK");
					}
					// si el respaldo fallo, mantengo resultado KO
				}

			} else {
				// la integracion estaba habilitada
				if (resultadoIntegracion.getIntegracionWS().equals("OK")) {
					// la integracion ha ido bien
					if (resultadoIntegracion.getBorradoFicheroContadores().equals("OFF")
							&& resultadoIntegracion.getBorradoFicheroContadores().equals("OFF")) {
						// el borrado esta deshabilitado
						resultadoIntegracion.setResultadoOperacion("OK");
					} else {
						// el borrado esta habilitado
						if (resultadoIntegracion.getBorradoFicheroLlamadas().contains("OK")
								&& resultadoIntegracion.getBorradoFicheroContadores().contains("OK")) {
							// el respaldo ha ido bien
							resultadoIntegracion.setResultadoOperacion("OK");
						}
						// si el respaldo fallo, mantengo resultado KO
					}
				}
				// si la integracion fallo, mantengo el resultado KO
			}

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("integracionWS", resultadoIntegracion.getIntegracionWS()));
			parametrosSalida.add(new ParamEvent("borradoFicheroLlamadas", resultadoIntegracion.getBorradoFicheroLlamadas()));
			parametrosSalida.add(new ParamEvent("BorradoFicheroContadores", resultadoIntegracion.getBorradoFicheroContadores()));
			if (resultadoIntegracion.getError() != null && !resultadoIntegracion.getError().equals("")) {
				parametrosSalida.add(new ParamEvent("error", resultadoIntegracion.getError()));
			}
			log.endProcess(idProceso, resultadoIntegracion.getResultadoOperacion(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE PROCESO **/

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			log.error(e.toString(), idProceso, null, e);
			/** FIN EVENTO - ERROR **/

			resultadoIntegracion.setResultadoOperacion("KO");
			resultadoIntegracion.setError("ERROR_EXT(" + e.getMessage() + ")");

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", resultadoIntegracion.getError()));
			log.endProcess(idProceso, resultadoIntegracion.getResultadoOperacion(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE PROCESO **/
		}
		logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
		listlogger.clear();
		return resultadoIntegracion;
	}

	private String convertirMilis2Date(String milis, String format) {

		log.comment("milis=" + milis);
		log.comment("Double.valueOf=" + Double.valueOf(milis));
		log.comment("longValue=" + Double.valueOf(milis).longValue());

		// convierto el string de milisegundos a tipo long
		// long milisLong = Long.parseLong(milis);
		long milisLong = Double.valueOf(milis).longValue();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milisLong);

		// obtengo la fecha formateada
		DateFormat formatter = new SimpleDateFormat(format);
		String date = formatter.format(calendar.getTime());

		log.comment("date=" + date);

		return date;
	}
}
