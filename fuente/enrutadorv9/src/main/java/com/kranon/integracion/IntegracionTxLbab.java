package com.kranon.integracion;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;

/**
 * Clase para realizar la Integracion con TX LBAB
 * 
 * @author abalfaro
 *
 */
public class IntegracionTxLbab {

	private CommonLoggerProcess log;

	private final String idProceso = "IntegracionTxLbab";

	public IntegracionTxLbab(String idInvocacion, String idElemento) {
		// recogemos el idInvocacion e idElemento desde donde se invoca, pero
		// escribiremos en un fichero de Log nuevo
		if (log == null) {
			log = new CommonLoggerProcess("INTEGRACION");
		}
		this.log.inicializar(idInvocacion, idElemento);
	}

	public BeanResultadoIntegracion realizarIntegracion() {

		BeanResultadoIntegracion resul = null;
		try {
			/** INICIO EVENTO - INICIO DE PROCESO **/
			log.initProcess(idProceso, "", null, null);
			/** FIN EVENTO - INICIO DE PROCESO **/

			// TODO implementar integracion
			resul = new BeanResultadoIntegracion();
			resul.setResultadoOperacion("OK");
			resul.setError("");

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", resul.getError()));
			log.endProcess(idProceso, resul.getResultadoOperacion(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE PROCESO **/

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			log.error(e.toString(), idProceso, null, e);
			/** FIN EVENTO - ERROR **/

			if (resul == null) {
				resul = new BeanResultadoIntegracion();

				resul.setResultadoOperacion("KO");
				resul.setError("ERROR_EXT(" + e.getMessage() + ")");
			}

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", resul.getError()));
			log.endProcess(idProceso, resul.getResultadoOperacion(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE PROCESO **/
		}
		return resul;
	}
}
