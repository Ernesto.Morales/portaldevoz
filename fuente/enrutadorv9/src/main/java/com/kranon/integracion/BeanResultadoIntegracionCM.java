package com.kranon.integracion;

public class BeanResultadoIntegracionCM {

	private String resultadoOperacion;

	private String error;

	// posibles valores {OFF (esta deshabilitado), OK (estaba habilitado y ha ido bien), KO (estaba habilitado y ha fallado)}
	private String integracionWS;
	// 409, 500...
	private String integracionWSCodigoError;
	private String integracionWSMensajeError;

	// posibles valores {OFF (esta deshabilitado), OK (WS fue OK, estaba habilitado y ha ido bien), KO (WS fue OK, estaba habilitado y ha fallado)
	// RESP_OK (WS fue KO, estaba habilitado y se hizo OK el rename del fichero) , RESP_KO (WS fue KO, estaba habilitado y fallo el rename del
	// fichero) }
	private String borradoFicheroLlamadas;

	// posibles valores {OFF (esta deshabilitado), OK (WS fue OK, estaba habilitado y ha ido bien), KO (WS fue OK, estaba habilitado y ha fallado)
	// RESP_OK (WS fue KO, estaba habilitado y se hizo OK el rename del fichero) , RESP_KO (WS fue KO, estaba habilitado y fallo el rename del
	// fichero) }
	private String borradoFicheroContadores;

	public BeanResultadoIntegracionCM() {
		this.resultadoOperacion = "";
		this.error = "";
		this.integracionWS = "";
		this.integracionWSCodigoError = "";
		this.integracionWSMensajeError = "";
		this.borradoFicheroLlamadas = "";
		this.borradoFicheroContadores = "";
	}

	public String getResultadoOperacion() {
		return resultadoOperacion;
	}

	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getIntegracionWS() {
		return integracionWS;
	}

	public void setIntegracionWS(String integracionWS) {
		this.integracionWS = integracionWS;
	}

	public String getBorradoFicheroLlamadas() {
		return borradoFicheroLlamadas;
	}

	public void setBorradoFicheroLlamadas(String borradoFicheroLlamadas) {
		this.borradoFicheroLlamadas = borradoFicheroLlamadas;
	}

	public String getBorradoFicheroContadores() {
		return borradoFicheroContadores;
	}

	public void setBorradoFicheroContadores(String borradoFicheroContadores) {
		this.borradoFicheroContadores = borradoFicheroContadores;
	}

	public String getIntegracionWSCodigoError() {
		return integracionWSCodigoError;
	}

	public void setIntegracionWSCodigoError(String integracionWSCodigoError) {
		this.integracionWSCodigoError = integracionWSCodigoError;
	}

	public String getIntegracionWSMensajeError() {
		return integracionWSMensajeError;
	}

	public void setIntegracionWSMensajeError(String integracionWSMensajeError) {
		this.integracionWSMensajeError = integracionWSMensajeError;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BeanResultadoIntegracionCM [resultadoOperacion=");
		builder.append(resultadoOperacion);
		builder.append(", error=");
		builder.append(error);
		builder.append(", integracionWS=");
		builder.append(integracionWS);
		if (integracionWS.equals("KO")) {
			builder.append(", integracionWSCodigoError=");
			builder.append(integracionWSCodigoError);
			builder.append(", integracionWSMensajeError=");
			builder.append(integracionWSMensajeError);
		}
		builder.append(", borradoFicheroLlamadas=");
		builder.append(borradoFicheroLlamadas);
		builder.append(", borradoFicheroContadores=");
		builder.append(borradoFicheroContadores);
		builder.append("]");
		return builder.toString();
	}

}
