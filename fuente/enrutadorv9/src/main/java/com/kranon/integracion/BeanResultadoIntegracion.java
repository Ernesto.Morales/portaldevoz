package com.kranon.integracion;

public class BeanResultadoIntegracion {

	private String resultadoOperacion;

	private String error;

	public String getResultadoOperacion() {
		return resultadoOperacion;
	}

	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "BeanResultadoIntegracion [resultadoOperacion=" + resultadoOperacion + ", error=" + error + "]";
	}

}
