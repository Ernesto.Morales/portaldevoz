package com.kranon.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.marshall.Marshall;
import com.kranon.bean.loggerllamada.DatosLogerLlamada;
import com.kranon.bean.loggerllamada.LoggerLlamadaTemp;
import com.kranon.bean.loggerllamada.Paso;

public class UtilidadesLoggerLllamada {

	public  LoggerLlamadaTemp loggerLlamadaTemp = new LoggerLlamadaTemp();
	public  DatosLogerLlamada datosloggerLlamada=new DatosLogerLlamada();
	public static final Integer ENRUTADOR=1;
	public static final Integer SEGUROSOP=2;
	public static final Integer ENRUTADOR2=3;
	public static final Integer ENRUTADOR3=4;
	
	public UtilidadesLoggerLllamada() {
		super();
	}

	public  void llenarBean(HttpServletRequest request){
		datosloggerLlamada.setIdLlamada(request.getParameter("VG_loggerServicio.idLlamada"));
		datosloggerLlamada.setIdServicio(request.getParameter("VG_loggerServicio.idServicio"));
		datosloggerLlamada.setIdElemento(request.getParameter("VG_loggerServicio.idElemento"));
		datosloggerLlamada.setNumOrigen(request.getParameter("VG_loggerServicio.numOrigen"));
		datosloggerLlamada.setNumDestino(request.getParameter("VG_loggerServicio.numDestino"));
		datosloggerLlamada.setIdioma(request.getParameter("VG_loggerServicio.idioma"));
		datosloggerLlamada.setTipo(request.getParameter("VG_loggerServicio.tipo"));
		String inicio = request.getParameter("VG_datosLogerLlamadaTemp.inicio");
		datosloggerLlamada.setInicio((new Date().toString()));
		Integer numeroarreglo =Integer.parseInt( request.getParameter("VG_datosLogerLlamadaTemp.numeroPaso"));
		for(int i=0;i <= numeroarreglo;i++){
			String nombre1 = "VG_datosLogerLlamadaTemp.pasos." + i + ".nombre";
			String nombre =request.getParameter(nombre1);
			String nombrefrom1 = "VG_datosLogerLlamadaTemp.pasos." + i + ".nombrefrom";
			String nombrefrom =request.getParameter(nombrefrom1);
			String codrespuesta1 = "VG_datosLogerLlamadaTemp.pasos." + i + ".codrespuesta";
			String codrespuesta =request.getParameter(codrespuesta1);
			String resultadoOperacion1 = "VG_datosLogerLlamadaTemp.pasos." + i + ".resultadoOperacion";
			String resultadoOperacion =request.getParameter(resultadoOperacion1);
			String variables1 = "VG_datosLogerLlamadaTemp.pasos." + i + ".variables";
			String variables =request.getParameter(variables1);
			Paso paso= new Paso(nombre,nombrefrom,codrespuesta,resultadoOperacion,variables);
			loggerLlamadaTemp.getPasos().add(paso);
			
		}
		loggerLlamadaTemp.setNumeroPaso(numeroarreglo);
		loggerLlamadaTemp.setInicio(inicio);
		
	}
	
	public  void LlenarUnPasoNuevo(String nombre,String nombrefrom,String codrespuesta,String resultadoOperacion,String variables){
		Paso paso= new Paso(nombre,nombrefrom,codrespuesta,resultadoOperacion,variables);
		loggerLlamadaTemp.getPasos().add(paso);
		
	}
	public  void verificarOK( ){
		int contador=0;
		int contador2=0;
		int tamLinea=loggerLlamadaTemp.getPasos().size()-2;
		if(loggerLlamadaTemp.getPasos().size()<5){
			obtenerCadenaKo(0,loggerLlamadaTemp.getPasos().size());
			
		}
		if(loggerLlamadaTemp.getPasos().size()>=5){
			
			contador=verificarOKK(0,5);
			contador2=contador2+verificarOKK(5,tamLinea);
			if(contador==5) {		
				datosloggerLlamada.getPasos().add(obtenerCadenaOk(ENRUTADOR));
				
			}
			else{
				obtenerCadenaKo(0,5);
				
			}
			if(contador2==(tamLinea)) {		
				datosloggerLlamada.getPasos().add(obtenerCadenaOk(SEGUROSOP));
				
			}
			else{
				obtenerCadenaKo(5,tamLinea);
				
			}
			obtenerCadenaKo(loggerLlamadaTemp.getPasos().size()-2,loggerLlamadaTemp.getPasos().size());
		}
		
	}
	
	private  int verificarOKK(int inicio, int fin){
		int contador=inicio;
		
		for (int i = inicio; i < fin; i++) {
			Paso paso =loggerLlamadaTemp.getPasos().get(i);
			if(paso.getResultadoOperacion().equals("OK")){
				
				contador++;
			}
		}
		
		return contador;
	}
	public  Paso obtenerCadenaOk(int origen){
		if(origen==ENRUTADOR){
			return new Paso("ENRUTADOR","01","OK","OK","");
		}
		else if(origen==SEGUROSOP){
			return new Paso("SEGUROSOP","OK","OK","OPC","");
		}
//		else if(origen==ENRUTADOR2){
//			return new Paso("ENRUTADOR","03","OK","OK","");
//		}
//		else if(origen==ENRUTADOR3){
//			return new Paso("ENRUTADOR","04","OK","OK","");
//		}
		else
			return new Paso();
	}
	
	public  void obtenerCadenaKo(int inicio, int fin){
	
		for (int i = inicio; i < fin; i++) {
			Paso paso =loggerLlamadaTemp.getPasos().get(i);
			datosloggerLlamada.getPasos().add(paso);
		}
		
	}
	
	public  void doArchivo(String idInvocacion, String idElemento, String idServicio) {
	    CommonLoggerProcess log = null;
		// recogemos el idInvocacion e idElemento desde donde se invoca, pero
		// escribiremos en un fichero de Log nuevo
		if (log == null) {
			log = new CommonLoggerProcess("INTEGRACION_" + idServicio.toUpperCase());
		}
		log.inicializar(idInvocacion, idElemento);
		Marshall procMarshal = new Marshall(log);
		procMarshal = new Marshall(log);
		// genero el XML de StatLlamada
		 try {
			procMarshal.marshallDatosLoggerLamada(datosloggerLlamada,idServicio);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
