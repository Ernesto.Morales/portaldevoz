package com.kranon.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
// import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.kranon.bean.gestioncm.BeanEstadisticaCM;
import com.kranon.bean.gestioncm.BeanGestionCuadroMando;
import com.kranon.bean.gestioncm.BeanInfoContadorCM;
import com.kranon.bean.stat.contador.BeanStatContador;
import com.kranon.bean.stat.contador.BeanStatInfoContador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.singleton.SingletonGestionCuadroMando;

public class UtilidadesCuadroMando {

	/**
	 * Traduce el objeto VG_stat que se envia en la request a Objeto JAVA BeanStatContador
	 * 
	 * @param log
	 * @param request
	 * @param codServIVR
	 * @return
	 */
	public static BeanStatContador getStat(CommonLoggerService log, HttpServletRequest request, String codServIVR) {

		BeanStatContador contadorStat = new BeanStatContador();

		ArrayList<BeanStatInfoContador> listaContadores = new ArrayList<BeanStatInfoContador>();
		int numContadorActual = 0;
		Enumeration<?> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = (String) enumeration.nextElement();

			// log.comment("parameterName" + parameterName);

			if (parameterName.contains("VG_stat.contadores.")) {
				// el parametro es de contadores

				if (parameterName.contains("VG_stat.contadores.") && parameterName.contains(".nombre")) {
					// es el parametro del nombre del contador, aprovecho y cojo toda su info

					String nombreStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".nombre");
					String tipoStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".tipo");
					String codigoRetornoStat = request.getParameter("VG_stat.contadores." + numContadorActual + ".codigoRetorno");
					String param1Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param1");
					String param2Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param2");
					String param3Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param3");
					String param4Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param4");
					String param5Stat = request.getParameter("VG_stat.contadores." + numContadorActual + ".param5");

					// creo el objeto del contador y lo relleno
					BeanStatInfoContador infoContador = new BeanStatInfoContador();
					infoContador.setNombre(nombreStat);
					infoContador.setTipo(tipoStat);
					infoContador.setCodigoRetorno(codigoRetornoStat);
					infoContador.setParam1(param1Stat);
					infoContador.setParam2(param2Stat);
					infoContador.setParam3(param3Stat);
					infoContador.setParam4(param4Stat);
					infoContador.setParam5(param5Stat);
					// asigno el codigo de servicio
					infoContador.setCodServIVR(codServIVR);

					// aado el contador a la lista
					listaContadores.add(infoContador);

					numContadorActual = numContadorActual + 1;

					// log.comment("nombreStat" + nombreStat);
				}
			}
		}

		contadorStat.setContadores(listaContadores);

		// log.comment("contadorStat.getContadores().size()" + String.valueOf(contadorStat.getContadores().size()));

		return contadorStat;
	}

	/**
	 * Metodo que permite obtener el nombre del ultimo contador para informar el LOG de MONITORIZACION.
	 * 
	 * @param log
	 * @param codServIVR
	 * @param contadorStat
	 * @return
	 */
	public static String getContadorCuadroMando(CommonLoggerService log, String codServIVR, BeanStatContador contadorStat) {

		String contadorCM = "";

		try {
			// 1. Recuperamos el Bean de Gestion Cuadro Mando
			BeanGestionCuadroMando gestionCuadroMando = null;
			SingletonGestionCuadroMando instance = SingletonGestionCuadroMando.getInstance(log);
			if (instance != null) {
				gestionCuadroMando = instance.getGestionCuadroMando(codServIVR);
			}

			if (gestionCuadroMando == null) {
				// no se ha recuperado bien el xml de gestion cuadro de mando
				throw new Exception("GEST_CUADRO_MANDO_NULL");
			}

			// 2. Traducimos el ultimo contador
			// ABALFARO_20170627 comprobamos tambien que no haya cero contadores, porque podriamos no tener ninguno todavia
			if (contadorStat.getContadores() != null && contadorStat.getContadores().size() != 0) {

				// 2.1. Se obtiene la informacion correspondiente al ultimo contador para detectar el modulo en el se esta invocando al metodo
				BeanStatInfoContador infoCont = contadorStat.getContadores().get(contadorStat.getContadores().size() - 1);

				// 2.2. Recupero la info de la estadistica con el nombre del contador
				BeanEstadisticaCM beanEstadistica = gestionCuadroMando.getTablaEstadisticas().get(infoCont.getNombre());

				if (beanEstadistica != null) {
					// he encontrado la estadistica

					// creo el objeto para buscar dentro de la estadistica con los datos del contador
					BeanInfoContadorCM infoEstadistica = new BeanInfoContadorCM();
					infoEstadistica.setType(infoCont.getTipo());
					infoEstadistica.setCodigoRetorno(infoCont.getCodigoRetorno());
					infoEstadistica.setParam1(infoCont.getParam1());
					infoEstadistica.setParam2(infoCont.getParam2());
					infoEstadistica.setParam3(infoCont.getParam3());
					infoEstadistica.setParam4(infoCont.getParam4());
					infoEstadistica.setParam5(infoCont.getParam5());

					// for (Map.Entry<BeanInfoContadorCM, String> entry : beanEstadistica.getTablaContadores().entrySet()) {
					// log.comment("clave=" + entry.getKey().toString() + ", valor=" + entry.getValue().toString());
					// }

					// 2.3. Busco el contador dentro de la estadistica
					contadorCM = beanEstadistica.getTablaContadores().get(infoEstadistica);

					/** INICIO EVENTO - ACCION **/
					String idAccion = "GET_CONTADOR_CM";
					String resultadoOperacion = "OK";
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					parametrosAdicionales.add(new ParamEvent("IN_BUSCA_STATS", infoCont.getNombre()));
					parametrosAdicionales.add(new ParamEvent("OUT_CONTADOR_CM", contadorCM));
					log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
					/** FIN EVENTO - ACCION **/
				}
			}
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "GET_CONTADOR_CUADRO_MANDO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;
		}
		return contadorCM;
	}

	/**
	 * Crea una copia del fichero origen al fichero destino, indicando las rutas
	 * 
	 * @param log
	 * @param pathOrigen
	 * @param pathDestino
	 */
	public static void copiaFicheroStat(CommonLoggerProcess log, String pathOrigen, String pathDestino) {

		InputStream inStream = null;
		OutputStream outStream = null;

		try {

			File fileOrigen = new File(pathOrigen);
			File fileDestino = new File(pathDestino);

			inStream = new FileInputStream(fileOrigen);
			outStream = new FileOutputStream(fileDestino);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = inStream.read(buffer)) > 0) {

				outStream.write(buffer, 0, length);

			}

			inStream.close();
			outStream.close();

			log.actionEvent("COPIA_RESPALDO", "OK", null);

		} catch (IOException e) {

			log.actionEvent("COPIA_RESPALDO", "KO", null);

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COPIA_RESPALDO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		}
	}
	
	
}
