package com.kranon.util;


public class UtilidadesTransferencias {

	private static final String EMPRESARIAL = "EMPRESARIAL";
	private static final String PREFERENTE = "PREFERENTE";
	private static final String PREFERENTEH = "PREFERENTEH";
	private static final String PREFERENTEB = "PREFERENTEB";
	private static final String PATRIMONIAL = "PATRIMONIAL";
	private static final String BANCARIO = "BANCARIO";
	private static final String EXPRESS = "EXPRESS";
	private static final String ERROR = "ERROR";
	
	
	public static String obtenerSegmento(String segmentoId)
	{
		String segmento = "";
		
		if(segmentoId.equals("E0"))
			segmento = EMPRESARIAL;
		else if(segmentoId.equals("F2") || segmentoId.equals("F3") || segmentoId.equals("F4") ||
					segmentoId.equals("F5") || segmentoId.equals("F6") || segmentoId.equals("F7") || 
					segmentoId.equals("F8") || segmentoId.equals("F9"))
			segmento = PREFERENTE;
		else if(segmentoId.equals("F1"))
			segmento = PREFERENTEH;
		else if(segmentoId.equals("S"))
			segmento = PREFERENTEB;
		else if(segmentoId.equals("P1") || segmentoId.equals("P2") || segmentoId.equals("P3"))
			segmento = PATRIMONIAL;
		else if(segmentoId.equals("B1") || segmentoId.equals("B2") || segmentoId.equals("B3") ||
					segmentoId.equals("B4") || segmentoId.equals("C0") || segmentoId.equals("G0") ||
					segmentoId.equals("S0"))
			segmento = BANCARIO;
		else if(segmentoId.equals("R1") || segmentoId.equals("R2") || segmentoId.equals("R3") || 
					segmentoId.equals("R3"))
			segmento = EXPRESS;	
		else
			segmento = ERROR;
		return segmento;
	}
}
