package com.kranon.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.kranon.bean.BeanControlHorario;
import com.kranon.bean.BeanHorario;
import com.kranon.bean.BeanTipoHorario;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

public class UtilidadesEnrutador {

	public static final String SEPARADOR_ENCODING = ";";
	public static final String ENCABEZADO_TRANSFER_UUI = "04C8";
	public static final String CADENA_ENCODING = "encoding=hex";

	// Array con la correspondencia de codigos de error con la descripcion asociada
	// Ejemplo: Codigo de Error CTI = -5 --> Descripcion Error CTI = ARRAY_ERRORES_CTI[5] = SOCKET SEND FAIL
	public static String [] ARRAY_ERRORES_CTI = {
		"",							// No Disponible
		"SOCKET GENERAL FAILURE",	// -1
		"NO ADDRESS",				// -2
		"SOCKET OPEN FAIL",			// -3 
		"SOCKET CONNECTION FAIL",	// -4
		"SOCKET SEND FAIL",			// -5 
		"SOCKET RECEIVED FAILED",	// -6 
		"NOT 512 BYTES",			// -7 
		"NOT OFFERED",				// -8 
		"TIME OUT",					// -9
		"XFER BAD",					// -10
		"UNKNOWN ERROR",			// -11
		};
	
	public static String comprobarHorario(CommonLoggerService log, BeanControlHorario controlHorario) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO", "", null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";
		try {
			resulHorario = UtilidadesEnrutador.comprobarHorario(log, controlHorario, "");

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	public static String comprobarHorario(CommonLoggerService log, BeanControlHorario controlHorario, String param) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO", param, null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";
		try {
			if (controlHorario == null) {
				// no hay horario definido
				resulHorario = UtilidadesEnrutador.comprobarHorarioInterno(log, null);

			} else {
				if (controlHorario.getHorarioParam() != null) {
					// hay un horario con parametrizacion definido, llamo al de por defecto
					// se devolver el resultado del horario asociado a <param> o a vacio (default) en caso de no encontrarse
					resulHorario = UtilidadesEnrutador.comprobarHorarioParam(log, controlHorario.getHorarioParam(), param);

				} else {
					// hay un horario sin parametrizacion definido
					resulHorario = UtilidadesEnrutador.comprobarHorarioInterno(log, controlHorario.getHorario());
				}
			}
		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	private static String comprobarHorarioParam(CommonLoggerService log, BeanTipoHorario[] controlHorarioParam, String param) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO_PARAM", param, null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";

		try {
			if (controlHorarioParam == null || controlHorarioParam.length == 0) {
				// no hay control horario
				return "DH";
			}
			boolean encontrado = false;
			for (BeanTipoHorario tipoHorario : controlHorarioParam) {
				if (tipoHorario.getParam() != null && !tipoHorario.getParam().equals("")) {
					if (tipoHorario.getParam().equalsIgnoreCase(param)) {
						// es el horario que estamos buscando para el segmento dado
						resulHorario = UtilidadesEnrutador.comprobarHorarioInterno(log, tipoHorario.getHorario());
						encontrado = true;
						break;
					}
				}
			}

			if (!encontrado) {
				// si no he encontrado el segmento, busco si hay uno por defecto
				/** INICIO EVENTO - ACCION **/
				log.actionEvent("BUSCA_HORARIO_PARAM", "DEFAULT", null);
				/** FIN EVENTO - ACCION **/
				
				for (BeanTipoHorario tipoHorario : controlHorarioParam) {
					if (tipoHorario.getParam() != null && tipoHorario.getParam().equals("")) {
						// es el horario por defecto
						resulHorario = UtilidadesEnrutador.comprobarHorarioInterno(log, tipoHorario.getHorario());
						encontrado = true;
						break;
					}
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO_PARAM", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	/**
	 * Comprueba si el dia y hora actual esta dentro de horario o no Si el control horario es vacio, devuelvo DH porque no se controla
	 * 
	 * @param {@link CommonLoggerService} logger para el traceo
	 * @param {@link BeanHorario[]} control de horario
	 * @return {@link String} DH o FH, para dentro y fuera de horario
	 * @throws Exception
	 */
	private static String comprobarHorarioInterno(CommonLoggerService log, BeanHorario[] controlHorario) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO_INTERNO", "", null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";

		try {
			if (controlHorario == null || controlHorario.length == 0) {
				// no hay control horario
				return "DH";
			}

			// datos de hoy
			String[] dias = { "D", "L", "M", "X", "J", "V", "S" };

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			int diaSemanaInt = calendar.get(Calendar.DAY_OF_WEEK);

			String diaSemana = dias[diaSemanaInt - 1];
			String anyoMesYDia = (new SimpleDateFormat("yyyy/MM/dd")).format(new Date());
			String mesYDia = (new SimpleDateFormat("MM/dd")).format(new Date());
			String horaHoy = (new SimpleDateFormat("HH:mm")).format(new Date());

			log.comment("anyoMesYDia=" + anyoMesYDia);
			log.comment("mesYDia=" + mesYDia);

			boolean encontrado = false;
			int i = 0;
			// BUSCO UN HORARIO CON LA FECHA CONCRETA DE HOY
			while (!encontrado && i < controlHorario.length) {
				BeanHorario horario = controlHorario[i];

				if (horario.getTipoHorario().equals("F")) {
					// es una fecha concreta
					if (horario.getValorHorario().length() > 5) {
						// es una fecha en formato YYYY/MM/DD
						if (horario.getValorHorario().equals(anyoMesYDia)) {
							// la fecha de hoy coincide
							encontrado = true;
						}
					} else {
						// es una fecha en formato MM/DD
						if (horario.getValorHorario().equals(mesYDia)) {
							// la fecha de hoy coincide
							encontrado = true;
						}
					}
				}

				// si coincide la fecha, miro las franjas
				if (encontrado) {

					String[] franjas = new String[3];
					franjas[0] = horario.getFranja1();
					franjas[1] = horario.getFranja2();
					franjas[2] = horario.getFranja3();

					resulHorario = UtilidadesEnrutador.comprobarFranjas(franjas, horaHoy);

				}

				// paso al siguiente horario
				i = i + 1;
			} // while

			if (!encontrado) {
				// el dia actual no coincide con ninguna fecha del horario,
				// busco por dia de la semana
				encontrado = false;
				i = 0;
				while (!encontrado && i < controlHorario.length) {
					BeanHorario horario = controlHorario[i];

					if (horario.getTipoHorario().equals("D")) {
						// es un dia de la semana
						if (horario.getValorHorario().equals(diaSemana)) {
							// coincide el dia de la semana
							encontrado = true;
						}
					}

					// si coincide el dia de la semana, miro las franjas
					if (encontrado) {

						String[] franjas = new String[3];
						franjas[0] = horario.getFranja1();
						franjas[1] = horario.getFranja2();
						franjas[2] = horario.getFranja3();

						resulHorario = UtilidadesEnrutador.comprobarFranjas(franjas, horaHoy);

					}
					i = i + 1;
				} // while

			} // if !encontrado

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO_INTERNO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;

	}

	/**
	 * Comprueba si las franjas estan dentro de la hora actual Si alguna de las franjas es CLOSED, se devuelve FH, independientemente de las horas
	 * 
	 * @param {@link String[]} franjas
	 * @return {@link String} DH o FH
	 */
	private static String comprobarFranjas(String[] franjas, String horaActual) {
		String resul = "FH";

		for (int i = 0; i < franjas.length; i++) {
			if (franjas[i] != null && !franjas[i].equals("")) {
				if (franjas[i].equals("CLOSED")) {
					// si alguna de las franjas es CLOSED, hemos acabado y el
					// resultado es fuera de horario
					resul = "FH";
					return resul;
				}
			}
		}

		// ninguna franja es closed, miro las horas
		for (int i = 0; i < franjas.length; i++) {
			if (franjas[i] != null && !franjas[i].equals("")) {
				// hay dos horas en la franjas
				String[] horasFranja = franjas[i].split("\\-");

				String[] horasInit = horasFranja[0].split("\\:");
				String[] horasFin = horasFranja[1].split("\\:");
				String[] horasHoy = horaActual.split("\\:");

				Calendar calInit = Calendar.getInstance();
				calInit.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasInit[0]));
				calInit.set(Calendar.MINUTE, Integer.parseInt(horasInit[1]));

				Calendar calFin = Calendar.getInstance();
				calFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasFin[0]));
				calFin.set(Calendar.MINUTE, Integer.parseInt(horasFin[1]));

				Calendar calHoy = Calendar.getInstance();
				calHoy.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasHoy[0]));
				calHoy.set(Calendar.MINUTE, Integer.parseInt(horasHoy[1]));

				if (calInit.before(calHoy) && calHoy.before(calFin)) {
					// si la hora actual esta entre las horas de alguna franja,
					// devuelvo dentro de horario
					resul = "DH";
					break;
				}
			}
		}

		return resul;
	}

	/**
	 * Convierte una cadena en hexadecimal a su corresponencia decimal
	 * 
	 * @param {@link String} cadena en formato hexadecimal
	 * @return {@link String} representacion de la cadena hexadecimal a su correspondencia decimal
	 */
	public static String convertirHexadecimalString(String hexadecimal) {
		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexadecimal.length(); i += 2) {
			String str = hexadecimal.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return output.toString();
	}

	/**
	 * Convierte una cadena en decimal a su corresponencia hexadecimal
	 * 
	 * @param {@link String} cadena en formato decimal
	 * @return {@link String} representacion de la cadena decimal a su correspondencia hexadecimal
	 */
	public static String convertirStringHexadecimal(String cadena) {
		char[] chars = cadena.toCharArray();
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}
		return hex.toString();
	}

}
