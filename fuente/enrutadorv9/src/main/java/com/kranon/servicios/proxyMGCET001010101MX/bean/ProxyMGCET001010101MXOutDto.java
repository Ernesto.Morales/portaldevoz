package com.kranon.servicios.proxyMGCET001010101MX.bean;


public class ProxyMGCET001010101MXOutDto {

	private String archivollamada;

	private String archivocontador;

	private String resultadooperacion;

	private String cifrascontrol;

	private String fechaoperacion;

	private String horaoperacion;

	public String getArchivollamada() {
		return archivollamada;
	}

	public void setArchivollamada(String archivollamada) {
		this.archivollamada = archivollamada;
	}

	public String getArchivocontador() {
		return archivocontador;
	}

	public void setArchivocontador(String archivocontador) {
		this.archivocontador = archivocontador;
	}

	public String getResultadooperacion() {
		return resultadooperacion;
	}

	public void setResultadooperacion(String resultadooperacion) {
		this.resultadooperacion = resultadooperacion;
	}

	public String getCifrascontrol() {
		return cifrascontrol;
	}

	public void setCifrascontrol(String cifrascontrol) {
		this.cifrascontrol = cifrascontrol;
	}

	public String getFechaoperacion() {
		return fechaoperacion;
	}

	public void setFechaoperacion(String fechaoperacion) {
		this.fechaoperacion = fechaoperacion;
	}

	public String getHoraoperacion() {
		return horaoperacion;
	}

	public void setHoraoperacion(String horaoperacion) {
		this.horaoperacion = horaoperacion;
	}

	@Override
	public String toString() {
		return String
				.format("ProxyMGCET001010101MXOutDto [archivollamada=%s, archivocontador=%s, resultadooperacion=%s, cifrascontrol=%s, fechaoperacion=%s, horaoperacion=%s]",
						archivollamada, archivocontador, resultadooperacion, cifrascontrol, fechaoperacion, horaoperacion);
	}

}
