package com.kranon.servicios.proxyMGCET001010101MX.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "format_1", namespace = "urn:com:bbva:mzic:mgcet001010101mx:facade:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProxyMGCET001010101MXOutDtoXML {

	@XmlElement
	private String archivollamada;
	@XmlElement
	private String archivocontador;
	@XmlElement
	private String resultadooperacion;
	@XmlElement
	private String cifrascontrol;
	@XmlElement
	private String fechaoperacion;
	@XmlElement
	private String horaoperacion;

	public String getArchivollamada() {
		return archivollamada;
	}

	public void setArchivollamada(String archivollamada) {
		this.archivollamada = archivollamada;
	}

	public String getArchivocontador() {
		return archivocontador;
	}

	public void setArchivocontador(String archivocontador) {
		this.archivocontador = archivocontador;
	}

	public String getResultadooperacion() {
		return resultadooperacion;
	}

	public void setResultadooperacion(String resultadooperacion) {
		this.resultadooperacion = resultadooperacion;
	}

	public String getCifrascontrol() {
		return cifrascontrol;
	}

	public void setCifrascontrol(String cifrascontrol) {
		this.cifrascontrol = cifrascontrol;
	}

	public String getFechaoperacion() {
		return fechaoperacion;
	}

	public void setFechaoperacion(String fechaoperacion) {
		this.fechaoperacion = fechaoperacion;
	}

	public String getHoraoperacion() {
		return horaoperacion;
	}

	public void setHoraoperacion(String horaoperacion) {
		this.horaoperacion = horaoperacion;
	}

	@Override
	public String toString() {
		return String
				.format("ProxyMGCET001010101MXOutDtoXML [archivollamada=%s, archivocontador=%s, resultadooperacion=%s, cifrascontrol=%s, fechaoperacion=%s, horaoperacion=%s]",
						archivollamada, archivocontador, resultadooperacion, cifrascontrol, fechaoperacion, horaoperacion);
	}

}
