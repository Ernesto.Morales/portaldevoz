package com.kranon.servicios.grantingticket.utils;

import java.io.EOFException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.grantingticket.utils.bean.BeanInformacionAudio;
import com.kranon.servicios.grantingticket.utils.bean.BeanResultadoAudio;
import com.kranon.servicios.grantingticket.utils.bean.BeanResultadoAudio.ErrorFormateo;
//import javax.sound.sampled.LineEvent;

public class AudioUtils {

	private CommonLoggerProcess log;
//	private String idInvocacion;
//	private String idElemento;
	
	private static HashMap<AudioFormat.Encoding,String> conversionMap;
	
	static {
		conversionMap = new HashMap<AudioFormat.Encoding, String>();
		conversionMap.put(	Encoding.ALAW, 			"pcm_alaw");
		conversionMap.put(	Encoding.ULAW, 			"pcm_mulaw");
		conversionMap.put(	Encoding.PCM_SIGNED, 	"pcm_s16le");
    }
	
	/**
	 * Constructor. Inicializa el log para guardar las trazas de la Toolkit
	 * @param idInvocacion Identificador de la traza
	 * @param idElemento Identificador del cliente
	 */
	public AudioUtils (String idInvocacion, String idElemento)
	{
		// Inicializa el log para guardar las trazas de la Toolkit
		if(log == null)
		{
			log = new CommonLoggerProcess("AUDIO_UTILS");
		}
		log.inicializar(idInvocacion, idElemento);
//		this.idInvocacion=idInvocacion;
//		this.idElemento=idElemento;		
	}
	
	
	/**
	 * Formatea el audio con el formato que se especifica
	 * @param inputFilename Ruta+nombre del audio que se quiere formatear
	 * @param outputFilename Ruta+nombre del nuevo audio con el formato adecuado
	 * @param formato {@link AudioFormat} Nuevo formato del audio
	 * @return  <p>
	 * 			<i>null</i> si el formateo ha funcionado correctamente. <p>  
	 *          {@link ErrorFormateo} si ha ido mal. Campos: <p>
	 *  			- codigoError (punto donde ha dado el error) <p>
	 *				- nombreWav (path+nombreAudio) <p>
	 * 				- mensajeError (excepcion capturada)
	 */
	public ErrorFormateo formatearAudio(String inputFilename,
			String outputFilename, AudioFormat formato) {

		ErrorFormateo resultadoFormateo = null;
		String puntoError = "";

		int nDesiredSampleSizeInBits = AudioSystem.NOT_SPECIFIED;
		int nDesiredChannels = AudioSystem.NOT_SPECIFIED;
		boolean bDesiredBigEndian = false;
		float fDesiredSampleRate = AudioSystem.NOT_SPECIFIED;
		AudioFormat.Encoding desiredEncoding = null;

		puntoError = "CREAR_PATH_DESTINO_AUDIO";
		log.actionEvent("FORMATEAR_AUDIO", "Crear nombre destino audio", null);
		File sourceFile = new File(inputFilename);
		File destFile = new File(outputFilename);

		AudioInputStream stream = null;

		try {

			puntoError = "GET_AUDIO_INPUT_STREAM";
			stream = AudioSystem.getAudioInputStream(sourceFile);

			log.actionEvent("FORMATEAR_AUDIO", "Plantilla de formato", null);
			AudioFormat.Encoding encodingEjemplo = formato.getEncoding();
			desiredEncoding = formato.getEncoding();
			nDesiredChannels = formato.getChannels();
			bDesiredBigEndian = formato.isBigEndian();
			fDesiredSampleRate = formato.getSampleRate();

			log.actionEvent("FORMATO_AUDIO_OBJETIVO", formato.toString(), null);
			log.actionEvent("FORMATO_AUDIO_ORIGEN", stream.getFormat()
					.toString(), null);

			/* Paso 1: Convertir a PCM si es necesario. */
			puntoError = "CONVERTIR_A_PCM";
			log.actionEvent("FORMATEAR_AUDIO", "Convertir a PCM - Origen: "
					+ stream.getFormat().getEncoding(), null);
			if (!((encodingEjemplo == AudioFormat.Encoding.PCM_UNSIGNED) & (encodingEjemplo == AudioFormat.Encoding.PCM_SIGNED))) {
				/*
				 * The following is a heuristics: normally (but not always), 8
				 * bit audio data are unsigned, while 16 bit data are signed.
				 */
				AudioFormat.Encoding targetEncoding = (formato
						.getSampleSizeInBits() == 8) ? AudioFormat.Encoding.PCM_UNSIGNED
						: AudioFormat.Encoding.PCM_SIGNED;
				stream = convertEncoding(targetEncoding, stream);
				/*
				 * Here, we handle a special case: some compressed formats do
				 * not state a sample size (but AudioSystem.NOT_SPECIFIED)
				 * because its unknown how long the samples are after decoding.
				 * If no sample size has been requested with a command line
				 * option, In this case, nDesiredSampleSizeInBits still has the
				 * value AudioSystem.NOT_SPECIFIED despite the filling with
				 * default values above.
				 */
				if (nDesiredSampleSizeInBits == AudioSystem.NOT_SPECIFIED) {
					nDesiredSampleSizeInBits = formato.getSampleSizeInBits();
				}
			}
			// log.actionEvent("FORMATO_AUDIO_PASO1",
			// stream.getFormat().toString(), null);

			/* Paso 2: Convertir el numero de canales */
			puntoError = "CONVERTIR_CANALES";
			log.actionEvent("FORMATEAR_AUDIO", "Convertir canales - Origen: "
					+ stream.getFormat().getChannels(), null);
			if (stream.getFormat().getChannels() != nDesiredChannels) {
				stream = convertChannels(nDesiredChannels, stream);
			}
			// log.actionEvent("FORMATO_AUDIO_PASO2",
			// stream.getFormat().toString(), null);

			/* Paso 3: Convertir el sample size y endian */
			puntoError = "CONVERTIR_SAMPLESIZE_ENDIAN";
			log.actionEvent("FORMATEAR_AUDIO",
					"Convertir sampleSize y endian - Origen: "
							+ stream.getFormat().getSampleSizeInBits() + "-"
							+ stream.getFormat().isBigEndian(), null);
			boolean bDoConvertSampleSize = (stream.getFormat()
					.getSampleSizeInBits() != nDesiredSampleSizeInBits);
			boolean bDoConvertEndianess = (stream.getFormat().isBigEndian() != bDesiredBigEndian);

			if (bDoConvertSampleSize || bDoConvertEndianess) {
				stream = convertSampleSizeAndEndianess(
						nDesiredSampleSizeInBits, bDesiredBigEndian, stream);
			}
			// log.actionEvent("FORMATO_AUDIO_PASO3",
			// stream.getFormat().toString(), null);

			/* Paso 4: Convertir sample rate. */
			puntoError = "CONVERTIR_SAMPLERATE";
			log.actionEvent("FORMATEAR_AUDIO",
					"Convertir sampleRate - Origen: "
							+ stream.getFormat().getSampleRate(), null);
			if (!(stream.getFormat().getSampleRate() == fDesiredSampleRate)) {
				stream = convertSampleRate(fDesiredSampleRate, stream);
			}
			// log.actionEvent("FORMATO_AUDIO_PASO4",
			// stream.getFormat().toString(), null);

			/* Paso 5: Convertir a non-PCM encoding */
			puntoError = "CONVERTIR_A_NON_PCM";
			log.actionEvent("FORMATEAR_AUDIO",
					"Convertir PCM si es necesario - Origen: "
							+ stream.getFormat().getEncoding(), null);
			if (!stream.getFormat().getEncoding().equals(desiredEncoding)) {
				stream = convertEncoding(desiredEncoding, stream);
			}
			// log.actionEvent("FORMATO_AUDIO_PASO5",
			// stream.getFormat().toString(), null);

			puntoError = "ESCRIBIR_AUDIO_FORMATEADO";
			log.actionEvent("FORMATEAR_AUDIO", "Escribir audio destino", null);
			AudioSystem.write(stream, AudioFileFormat.Type.WAVE, destFile);

			// log.actionEvent("FORMATEAR_AUDIO", "FIN formatear", null);

			// Si ha llegado a este punto es que ha funcionado correctamente por
			// lo que no hay error
		} catch (Exception e) {
			resultadoFormateo = new BeanResultadoAudio().new ErrorFormateo();
			resultadoFormateo.setCodigoError(puntoError);
			resultadoFormateo.setNombrewav(inputFilename);
			resultadoFormateo.setMensajeError(e.getMessage());
		}

		return resultadoFormateo;
	}
	
	
	/**
	 * Cambia la codificacion del audio
	 * @param targetEncoding {@link AudioFormat.Encoding} Nueva codificacion
	 * @param sourceStream {@link AudioInputStream} Audio actual
	 * @return {@link AudioInputStream} Audio con la codificacion modificada
	 * @throws Exception
	 */
	private AudioInputStream convertEncoding (
			AudioFormat.Encoding targetEncoding,
			AudioInputStream sourceStream)  throws Exception
	{
		AudioInputStream resultado = null;
		try
		{
			AudioSystem.isConversionSupported(targetEncoding,sourceStream.getFormat());
			//boolean cambioPermitido = AudioSystem.isConversionSupported(targetEncoding,sourceStream.getFormat());
			//log.comment("CONVERT_ENCODING ******* ESTA PERMITIDO " + Boolean.toString(cambioPermitido));
			
			resultado = AudioSystem.getAudioInputStream(targetEncoding,
					   sourceStream);		
		}
		catch (Exception e) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "CONVERT_ENCODING", parametrosSalida,e);
			throw e;   
		}  
		return resultado;
	}
	
	/**
	 * Cambia el numero de canales del audio
	 * @param nChannels Nuevo numero de canales
	 * @param sourceStream {@link AudioInputStream} Audio actual
	 * @return {@link AudioInputStream} Audio con el numero de canales modificado
	 * @throws Exception
	 */
	private AudioInputStream convertChannels(
			int nChannels,
			AudioInputStream sourceStream)  throws Exception
	{
		AudioInputStream resultado = null;
		try
		{
			AudioFormat sourceFormat = sourceStream.getFormat();
			AudioFormat targetFormat = new AudioFormat(
				sourceFormat.getEncoding(),
				sourceFormat.getSampleRate(),
				sourceFormat.getSampleSizeInBits(),				
				nChannels, //sourceFormat.getChannels(),
				calculateFrameSize(nChannels,
								   sourceFormat.getSampleSizeInBits()),
				sourceFormat.getFrameRate(),
				sourceFormat.isBigEndian());
			AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//boolean cambioPermitido = AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//log.comment("CONVERT_CHANNELS ******* ESTA PERMITIDO " + Boolean.toString(cambioPermitido));
			
			resultado = AudioSystem.getAudioInputStream(targetFormat,
												   sourceStream);
		}
		catch (Exception e) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "CONVERT_CHANNELS", parametrosSalida,e);
			throw e;   
		}  
		return resultado;	
	}
	
	/**
	 * Calcula el tamao del Frame del audio
	 * @param nChannels Numero de canales
	 * @param nSampleSizeInBits Tamao de la muestra en bits
	 * @return Tamao del Frame
	 * @throws Exception
	 */
	public int calculateFrameSize(int nChannels, int nSampleSizeInBits)  throws Exception
	{
		int resultado = 0;
		try
		{
			resultado = ((nSampleSizeInBits + 7) / 8) * nChannels;
		}
		catch (Exception e) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "CALCULATE_FRAME_SIZE", parametrosSalida,e);
			throw e;   
		}  
		return resultado;	
	}
	
	/**
	 * Cambia el tamao de la muestra y el Endian del audio
	 * @param nSampleSizeInBits Nuevo tamao de la muestra en bits
	 * @param bBigEndian Nuevo Endian
	 * @param sourceStream {@link AudioInputStream} Audio actual
	 * @return {@link AudioInputStream} Nuevo audio con el SampleSize y el Endian modificado
	 * @throws Exception
	 */
	private AudioInputStream convertSampleSizeAndEndianess(
			int nSampleSizeInBits,
			boolean bBigEndian,
			AudioInputStream sourceStream)  throws Exception
	{
		AudioInputStream resultado = null;
		try
		{
			AudioFormat sourceFormat = sourceStream.getFormat();
			AudioFormat targetFormat = new AudioFormat(
				sourceFormat.getEncoding(),
				sourceFormat.getSampleRate(),
				nSampleSizeInBits,
				sourceFormat.getChannels(),
				calculateFrameSize(sourceFormat.getChannels(),
								   nSampleSizeInBits),
				sourceFormat.getFrameRate(),
				bBigEndian);
			
			AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//boolean cambioPermitido = AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//log.comment("CONVERT_SAMPLESIZE_ENDIANESS ******* ESTA PERMITIDO " + Boolean.toString(cambioPermitido));
			
			resultado = AudioSystem.getAudioInputStream(targetFormat,
												   sourceStream);
		}
		catch (Exception e) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "CONVERT_SAMPLESIZE_ENDIANESS", parametrosSalida,e);
			throw e;   
		}  
		return resultado;		
	}
	
	/**
	 * Cambia la frecuencia de muestreo de un audio
	 * @param fSampleRate Nueva frecuencia
	 * @param sourceStream {@link AudioInputStream} Audio actual
	 * @return {@link AudioInputStream} Audio con la frecuencia modificada
	 * @throws Exception
	 */
	private AudioInputStream convertSampleRate(
			float fSampleRate,
			AudioInputStream sourceStream)  throws Exception
	{
		AudioInputStream resultado = null;
		try
		{	
			
			AudioFormat sourceFormat = sourceStream.getFormat();
			AudioFormat targetFormat = new AudioFormat(
				sourceFormat.getEncoding(),
				fSampleRate,
				sourceFormat.getSampleSizeInBits(),
				sourceFormat.getChannels(),
				sourceFormat.getFrameSize(),
				sourceFormat.getFrameRate(), //fSampleRate,
				sourceFormat.isBigEndian());
			
			AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//boolean cambioPermitido = AudioSystem.isConversionSupported(targetFormat,sourceFormat);
			//log.comment("CONVERT_SAMPLERATE ******* ESTA PERMITIDO " + Boolean.toString(cambioPermitido));
			
			resultado = AudioSystem.getAudioInputStream(targetFormat,sourceStream);
		}
		catch (Exception e) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "CONVERT_SAMPLERATE", parametrosSalida,e);
			throw e;   
		}  
		return resultado;		
	}
	
	
	
	/**
	 * Calcula la duracion en segundos de un audio. <p>
	 * <b>Ejemplo:</b>
	 * <pre>
	 * AudioUtils pruebaAudio = new AudioUtils(idInvocacion, idElemento);
	 * String ficheroAudioOrigen = "C://pruebasToolkit//audio1.wav";
	 * float duracion = pruebaAudio.duracionAudio(ficheroAudioOrigen);
	 * 
	 * </pre>
	 * <b>Traza Toolkit:</b>
	 * <pre>
	 * 20160708095813843|PRUEBAS_AUDIOS|INIT_MODULE|DURACION_AUDIO|IN=
	 * [Fichero=C://pruebasToolkit//audio1-OUT.wav]
	 * 20160708095813843|PRUEBAS_AUDIOS|END_MODULE|DURACION_AUDIO|OK|OUT=
	 * [Duracion=99.0]
	 * </pre>
	 * @param fichero Path + nombre del fichero de audio
	 * @return Segundos que dura el audio
	 * @throws Exception
	 */
	public float duracionAudio(String fichero)  throws Exception
	{
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("Fichero", fichero));
		log.initModuleProcess("DURACION_AUDIO",parametrosEntrada);
		
		float duracion = 0;
		
		try {
			File sourceFile = new File(fichero);
			AudioInputStream stream = AudioSystem.getAudioInputStream(sourceFile);
			AudioFormat format = stream.getFormat();

			DataLine.Info info = new DataLine.Info(Clip.class, stream.getFormat(),
                    ((int) stream.getFrameLength() * format.getFrameSize()));

			Clip clip = (Clip) AudioSystem.getLine(info);
            clip.close();
            duracion = clip.getBufferSize() / (clip.getFormat().getFrameSize() * clip.getFormat().getFrameRate()) ;
            
            duracion = Math.round(duracion * 100);
            duracion = duracion / 100;

			stream.close();
            
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Duracion", Float.toString(duracion)));
			log.endModuleProcess("DURACION_AUDIO", "OK", parametrosSalida, null);
		}
		catch (Exception e) {
			//e.printStackTrace();
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "DURACION_AUDIO", parametrosSalida,e);
			log.endModuleProcess("DURACION_AUDIO", "KO", parametrosSalida, null);
			throw e;
		}
		
		return duracion;
	}
	
	public float duracionAudioFile(File sourceFile)  throws Exception
	{
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("File", sourceFile.getAbsolutePath()));
		log.initModuleProcess("DURACION_AUDIO",parametrosEntrada);
		
		float duracion = 0;
		
		try {
			//File sourceFile = new File(fichero);
			AudioInputStream stream = AudioSystem.getAudioInputStream(sourceFile);
			AudioFormat format = stream.getFormat();

			DataLine.Info info = new DataLine.Info(Clip.class, stream.getFormat(),
                    ((int) stream.getFrameLength() * format.getFrameSize()));

			Clip clip = (Clip) AudioSystem.getLine(info);
            clip.close();
            duracion = clip.getBufferSize() / (clip.getFormat().getFrameSize() * clip.getFormat().getFrameRate()) ;
            
            duracion = Math.round(duracion * 100);
            duracion = duracion / 100;

			stream.close();
            
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Duracion", Float.toString(duracion)));
			log.endModuleProcess("DURACION_AUDIO", "OK", parametrosSalida, null);
		}
		catch (Exception e) {
			//e.printStackTrace();
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "DURACION_AUDIO", parametrosSalida,e);
			log.endModuleProcess("DURACION_AUDIO", "KO", parametrosSalida, null);
			throw e;
		}
		
		return duracion;
	}
	
	
	/**
	 * Informacion del audio. <p>
	 * <b>Ejemplo:</b>
	 * <pre>
	 * AudioUtils pruebaAudio = new AudioUtils(idInvocacion, idElemento);
	 * String ficheroAudioOrigen = "C://pruebasToolkit//audio1-OUT.wav";
	 * BeanInformacionAudio info = pruebaAudio.getInformacionAudio(ficheroAudioOrigen);
	 * 
	 * Mostraria por pantalla: 
	 * Formato archivo: WAVE
	 * Tamano en bytes: 3168078
	 * Tamano en frames: 1584000
	 * Codificacion: PCM_SIGNED
	 * Num canales: 1
	 * Frecuencia: 16000.0
	 * Num Bits: 16.0
	 * Frame size: 2
	 * Frame rate: 16000.0
	 * Endian: false
	 * Duracion: 99.0
	 * </pre>
	 * <b>Traza Toolkit:</b>
	 * <pre>
	 * 20160708100227582|PRUEBAS_AUDIOS|INIT_MODULE|INFORMACION_AUDIO|IN=
	 * [Fichero=C://pruebasToolkit//audio1-OUT.wav]
	 * 20160708100227582|PRUEBAS_AUDIOS|INIT_MODULE|DURACION_AUDIO|IN=
	 * [Fichero=C://pruebasToolkit//audio1-OUT.wav]
	 * 20160708100227582|PRUEBAS_AUDIOS|END_MODULE|DURACION_AUDIO|OK|OUT=
	 * [Duracion=99.0]
	 * 20160708100227582|PRUEBAS_AUDIOS|END_MODULE|INFORMACION_AUDIO|OK|OUT=
	 * [Informacion=BeanInformacionAudio [tipoArchivo=WAVE, codificacion=PCM_SIGNED, 
	 * canales=1, frecuencia=16000.0, numBitsMuestra=16.0, frameRate=16000.0, 
	 * frameSize=2, esBigEndian=false, duracion=99.0]]
	 * </pre>
	 * @param ficheroAudio Audio del que queremos obtener la informacion
	 * @return {@link BeanInformacionAudio} Objeto con la informacion del audio
	 * @throws Exception
	 */
	public BeanInformacionAudio getInformacionAudio (String ficheroAudio) throws Exception
	{
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("Fichero", ficheroAudio));
		log.initModuleProcess("INFORMACION_AUDIO",parametrosEntrada);
		
		BeanInformacionAudio resultado=new BeanInformacionAudio();
		try {
			
			File sourceFile = new File(ficheroAudio);
			AudioInputStream stream = AudioSystem.getAudioInputStream(sourceFile);
			AudioFormat sourceFormat = stream.getFormat();
			
			resultado.setTipoArchivo(AudioSystem.getAudioFileFormat(sourceFile).getType());
			resultado.setTamanoBytes(AudioSystem.getAudioFileFormat(sourceFile).getByteLength());
			resultado.setTamanoFrames(AudioSystem.getAudioFileFormat(sourceFile).getFrameLength());
			resultado.setCodificacion(sourceFormat.getEncoding());
			resultado.setCanales(sourceFormat.getChannels());
			resultado.setFrecuencia(sourceFormat.getSampleRate());
			resultado.setNumBitsMuestra(sourceFormat.getSampleSizeInBits());
			resultado.setFrameRate(sourceFormat.getFrameRate());
			resultado.setFrameSize(sourceFormat.getFrameSize());
			resultado.setEsBigEndian(sourceFormat.isBigEndian());
			resultado.setDuracion(duracionAudio(ficheroAudio));
			
			stream.close();
			
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Informacion", resultado.toString()));
			log.endModuleProcess("INFORMACION_AUDIO", "OK", parametrosSalida, null);
		}
		catch (Exception e) {
			//e.printStackTrace();
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "INFORMACION_AUDIO", parametrosSalida,e);
			log.endModuleProcess("INFORMACION_AUDIO", "KO", parametrosSalida, null);
			throw e;
		}
		return resultado;
	}
	
	public BeanInformacionAudio getInformacionAudioFile (File sourceFile) throws Exception
	{
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("File", sourceFile.getAbsolutePath()));
		log.initModuleProcess("INFORMACION_AUDIO",parametrosEntrada);
		
		BeanInformacionAudio resultado=new BeanInformacionAudio();
		try {
			
			
			AudioInputStream stream = null;
			try
			{
				stream = AudioSystem.getAudioInputStream(sourceFile);
			}
			catch (EOFException ex1) {
			     log.comment("salimos por EOFException");
			     stream = AudioSystem.getAudioInputStream(sourceFile);
			}
		
			AudioFormat sourceFormat = stream.getFormat();
			
			resultado.setTipoArchivo(AudioSystem.getAudioFileFormat(sourceFile).getType());
			resultado.setTamanoBytes(AudioSystem.getAudioFileFormat(sourceFile).getByteLength());
			resultado.setTamanoFrames(AudioSystem.getAudioFileFormat(sourceFile).getFrameLength());
			resultado.setCodificacion(sourceFormat.getEncoding());
			resultado.setCanales(sourceFormat.getChannels());
			resultado.setFrecuencia(sourceFormat.getSampleRate());
			resultado.setNumBitsMuestra(sourceFormat.getSampleSizeInBits());
			resultado.setFrameRate(sourceFormat.getFrameRate());
			resultado.setFrameSize(sourceFormat.getFrameSize());
			resultado.setEsBigEndian(sourceFormat.isBigEndian());
			resultado.setDuracion(duracionAudioFile(sourceFile));
			
			stream.close();
			
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Informacion", resultado.toString()));
			log.endModuleProcess("INFORMACION_AUDIO", "OK", parametrosSalida, null);
		}
		catch (Exception e) {
			//e.printStackTrace();
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "INFORMACION_AUDIO", parametrosSalida,e);
			log.endModuleProcess("INFORMACION_AUDIO", "KO", parametrosSalida, null);
			throw e;
		}
		return resultado;
	}
	
	public BeanInformacionAudio getInformacionAudioStream (File sourceFile, AudioInputStream stream) throws Exception
	{
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("File", sourceFile.getAbsolutePath()));
		parametrosEntrada.add(new ParamEvent("Stream",""));
		log.initModuleProcess("INFORMACION_AUDIO",parametrosEntrada);
		
		BeanInformacionAudio resultado=new BeanInformacionAudio();
		try {
			
			
			AudioFormat sourceFormat = stream.getFormat();
			
			resultado.setTipoArchivo(AudioSystem.getAudioFileFormat(sourceFile).getType());
			resultado.setTamanoBytes(AudioSystem.getAudioFileFormat(sourceFile).getByteLength());
			resultado.setTamanoFrames(AudioSystem.getAudioFileFormat(sourceFile).getFrameLength());
			resultado.setCodificacion(sourceFormat.getEncoding());
			resultado.setCanales(sourceFormat.getChannels());
			resultado.setFrecuencia(sourceFormat.getSampleRate());
			resultado.setNumBitsMuestra(sourceFormat.getSampleSizeInBits());
			resultado.setFrameRate(sourceFormat.getFrameRate());
			resultado.setFrameSize(sourceFormat.getFrameSize());
			resultado.setEsBigEndian(sourceFormat.isBigEndian());
			resultado.setDuracion(duracionAudioFile(sourceFile));
			
			stream.close();
			
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Informacion", resultado.toString()));
			log.endModuleProcess("INFORMACION_AUDIO", "OK", parametrosSalida, null);
		}
		catch (Exception e) {
			//e.printStackTrace();
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("Error", e.toString()));
			log.error(e.getMessage(), "INFORMACION_AUDIO", parametrosSalida,e);
			log.endModuleProcess("INFORMACION_AUDIO", "KO", parametrosSalida, null);
			throw e;
		}
		return resultado;
	}
	
	
	
	
	
	
	
}
