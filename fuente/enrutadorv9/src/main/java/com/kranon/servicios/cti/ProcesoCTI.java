package com.kranon.servicios.cti;

import ivrtrans.ivrTrans;

import java.util.ArrayList;

import userdefine.UDConstans;
import java.util.Arrays;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.cti.bean.BeanDatosCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servicios.cti.bean.ProcesoCTIOutDto;
import com.kranon.singleton.SingletonWebServices;

public class ProcesoCTI {

	private CommonLoggerProcess log;
	private final String loggerFile = "CTI";
	private String url;
	private int timeout;
	private int maxtimeout;
	private int port;
	private int maxIntentos;
	public static String caracterRellenoCTI;
	public static String caracterRellenoUUI;
	private String cadenaCTI;
	private String cadenaUUI;
	// ABALFARO_20170627 nuevo paso de idServicio para poder recuperar el XML de SERVICIOS correspondiente
	private String idServicio;
	
	public static final int GET_CTI = ivrTrans.GET;
	public static final int TRANS_CTI = ivrTrans.XFER;
	public static final int END = ivrTrans.ENDOFSEGMENT;
	public static final int COLGADO_IVR = 1;
	public static final int COLGADO_CLIENTE = 2;
	public static final int TRANSFERIDA = 3;



	public ProcesoCTI(String idInvocacion, String idElemento, String idServicio) {
		// recogemos el idInvocacion e idElemento desde donde se invoca, pero
		// escribiremos en un fichero de Log nuevo
		if (log == null) {
			log = new CommonLoggerProcess(loggerFile + "_" + (idServicio.toUpperCase()));
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idServicio = idServicio;
	}

	public ProcesoCTIOutDto iniciarLlamada(BeanDatosCTI beanDatos,BeanDatosUUI beanDatosU){

		String idModuloLog = "OBTENER_CTI";
		String resultadoOperacion = "KO";

		// Objeto retorno de la peticion
		BeanDatosCTI datosCTI = null;
		BeanDatosUUI datosUUI = null;
		ProcesoCTIOutDto respuesta = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			prepararPeticion(beanDatos,beanDatosU);
			configurarServicio();
			
			String cadenaResultado = invocarCTI(GET_CTI);	
//String			cadenaResultado=null;
			
//			#################2_15467######kR1MiQhz2otdIuUlQkbEyi##tIqVMiI16f35########
	//String		cadenaResultado = "001e535Fme01000013_1199530X9iKPGL4sNGidirOiaOERsDenI  U0pc######227#######|2035|299054|#|#|69051111811583445185|#|#|#|#|#|#|00001|2_1|#|L4sNGidirOiaOERsDenI|227#######|1995|30X9iKPG|#|#|#|#|#|#|#|#|1|U0pc######|2020-03-0503.53.09|01|#|#|#|#|#|#|#";
//			cadenaResultado = "001e58NAIL  901322_16649UpJ1P8RYtKvin8gaqVDG5ar5lAFz  V1mP######69########|602|5687|#|#|10001000381510272534|#|#|#|#|#|#|#####|###|#|####################|##########|####|########|#|#|#|#|#|#|#|#|1|##########|2017-11-0906.05.45|01|#|#|#|##|#|#|#";			
//String			cadenaResultado = "001e51REhm01000012_12075bp7OmTStC4HNfvDMGguBvcA0o4el  V2U0######336#######|254335|299154|#|#|22291094761580158052|#|#|#|#|#|#|00001|2_1|#|C4HNfvDMGguBvcA0o4el|336#######|2075|bp7OmTSt|#|#|#|#|#|#|#|#|1|V2U0######|2020-01-2702.47.34|01|#|#|#|#|#|#|#";
	//System.out.println("**************************************CADENA CTI EN DURO*****************************************");		
	//System.out.println(cadenaResultado);
	if( cadenaResultado == null ){
				datosUUI = (cadenaUUI.equals("") ) ? new BeanDatosUUI() :  BeanDatosUUI.parseaCadena(cadenaUUI);				
				datosCTI = BeanDatosCTI.parseaCadena( cadenaCTI.trim() );
			}
			else{ //Si la respuesta fue exitosa se parsea la cadena y construye los objetos
				int index = cadenaResultado.indexOf("|");	

				// parseo la cadena de salida
				datosUUI = BeanDatosUUI.parseaCadena( cadenaResultado.substring(0,index) );
				datosCTI = BeanDatosCTI.parseaCadena( cadenaResultado.substring(index+1) );
			}
			
			resultadoOperacion= "OK";
			
			respuesta = new ProcesoCTIOutDto();
			respuesta.setBeanDatosCTI(datosCTI);
			respuesta.setBeanDatosUUI(datosUUI);
			respuesta.setResultadoOperacion(resultadoOperacion);


		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("respuesta", (respuesta==null)?"null":respuesta.toString() ) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		
		return respuesta;
	}
	
	public ProcesoCTIOutDto transferirLlamada(BeanDatosCTI beanDatos,BeanDatosUUI beanDatosU){
		String idModuloLog = "TRANSFERIR_CTI";
		String resultadoOperacion = "KO";

		// Objeto retorno de la peticion
		ProcesoCTIOutDto respuesta = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			prepararPeticion(beanDatos,beanDatosU);
			configurarServicio();
			
			String cadenaResultado = invocarCTI(TRANS_CTI);
			
			//Si hubo problemas de invocacion al CTI  se utiliza el VDN enviado
			respuesta = new ProcesoCTIOutDto();
			if( cadenaResultado == null ){
				respuesta.setCodigoError("1");
				respuesta.setDestino( beanDatos.getVdnDestino() );
			}
			else{ //Si la respuesta fue exitosa se parsea la cadena y construye los objetos con el VDN devuelto
				int index = cadenaResultado.indexOf("|");					
				String[] arrResultado = cadenaResultado.substring(index+1).split("\\|");

				if( arrResultado.length == 2){
					respuesta.setCodigoError(arrResultado[0]);
					respuesta.setDestino(arrResultado[1]);					
					resultadoOperacion= "OK";
					
					respuesta.setResultadoOperacion(resultadoOperacion);
				}
			}
			
			respuesta.setResultadoOperacion(resultadoOperacion);
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("respuesta", (respuesta==null)?"null":respuesta.toString() ) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		
		return respuesta;
	}

	
	public ProcesoCTIOutDto terminarLlamada(BeanDatosUUI beanDatosU,int modoTermino){
		String idModuloLog = "TERMINAR_CTI";
		String resultadoOperacion = "KO";
		String esp=new String(new char[612]);
		esp=esp.replace("\0", " ");

		// Objeto retorno de la peticion
		ProcesoCTIOutDto respuesta = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("beanDatosUUI", (beanDatosU==null)?"null":beanDatosU.toString() ) );
			parametrosEntrada.add(new ParamEvent("modoTermino", modoTermino + ""));
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Crear la cadena de envo hacia el CTI a partir del bean
			if (modoTermino <= 0 || modoTermino > 3) {
				throw new Exception("El modo de termino no existe");
			}
			
			configurarServicio();
			cadenaCTI = String.valueOf(modoTermino);
			cadenaCTI = cadenaCTI + esp;
			cadenaCTI = cadenaCTI.substring('\u0000', 612);			
			cadenaUUI = (beanDatosU==null) ? "" : beanDatosU.construyeCadena(caracterRellenoUUI);

						
			String cadenaResultado = invocarCTI(END);	
			int index = cadenaResultado.indexOf("|");	
			
			String[] arrResultado = cadenaResultado.substring(index+1).split("\\|");

			if( arrResultado.length == 1){
				resultadoOperacion= "OK";
				
				respuesta = new ProcesoCTIOutDto();
				respuesta.setCodigoError(arrResultado[0]);
				respuesta.setResultadoOperacion(resultadoOperacion);
			}				

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
			
			resultadoOperacion= "KO";
			
			respuesta = new ProcesoCTIOutDto();
			respuesta.setResultadoOperacion(resultadoOperacion);
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("respuesta", (respuesta==null)?"null":respuesta.toString() ) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		
		return respuesta;
	}

	/**
	 * Recoge la informacin de CTI
	 * 
	 * @return {@link ProcesoCTIOutDto}
	 * @throws Exception
	 */
	private String invocarCTI(int modo) throws Exception {		
		String idModuloLog = "INVOCAR_PROCESO_CTI";
		String resultadoOperacion = "KO";
		
		ivrTrans ivrtrans = new ivrTrans(url,port,timeout,maxtimeout);
		int resultado = 0;
		int intentos = 0;
		String cadenaResultado = null;
		try {
			// 1.- Escribir traza de inicio de modulo
			
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("modo", modo + ""));
			parametrosEntrada.add(new ParamEvent("url", url));
			parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
			parametrosEntrada.add(new ParamEvent("port", port + ""));
			parametrosEntrada.add(new ParamEvent("cadenaCTI", cadenaCTI + ""));
			parametrosEntrada.add(new ParamEvent("cadenaUUI", cadenaUUI + ""));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			do{
				this.log.comment("Invocacion CTI intento["+(intentos+1)+"]");
				resultado = ivrtrans.SendData(cadenaUUI, modo, cadenaCTI);
				intentos++;
			}while( resultado!=1 && intentos < maxIntentos );
			
			
			if(resultado != 1){
				throw new Exception("Error al invocar CTI: " + resultado);
			}
			cadenaResultado = ivrtrans.getDdrData();	
			resultadoOperacion = "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);			
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("cadenaResultado", cadenaResultado) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		
		return cadenaResultado;
	}
	
	
	private void configurarServicio(){

		String idModuloLog = "CONFIGURAR_CTI";
		String resultadoOperacion = "KO";		

		try {
			// 1.- Escribir traza de inicio de modulo
			/** INICIO EVENTO - INICIO MODULO **/		
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url el timeout y el puerto del Xml		
			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}

			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}

			url = datosEntorno.getIntegracionCTI().getUrl();
			timeout = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("timeout") );
			port = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("port") );
			maxtimeout = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("maxtimeout") );
			maxIntentos = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("maxintentos") );
			
			caracterRellenoCTI = datosEntorno.getIntegracionCTI().getValueParametro("caracterRellenoCTI");
			caracterRellenoUUI = datosEntorno.getIntegracionCTI().getValueParametro("caracterRellenoUUI"); 
			
			resultadoOperacion = "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			// 5.- Escribir traza de Fin
			
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("url", url));
			parametrosSalida.add(new ParamEvent("timeout", String.valueOf(timeout) ));
			parametrosSalida.add(new ParamEvent("port", String.valueOf( port) ));
			parametrosSalida.add(new ParamEvent("maxtimeout", String.valueOf( maxtimeout) ));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}

	}

	
	/**
	 * Metodo que prepara los parametros url, port, timeout, y cadena de envo para el servicio del CTI
	 * 
	 * @param beanDatos Objeto {@link BeanDatosCTI} con los valores de entrada para el servicio CTI
	 */
	private void prepararPeticion(BeanDatosCTI beanDatos,BeanDatosUUI beanDatosU) {
		String idModuloLog = "PREPARA_PROCESO_CTI";
		String resultadoOperacion = "KO";		

		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();		
			parametrosEntrada.add( new ParamEvent("CTI", (beanDatos==null)?"null":beanDatos.toString() ) );			
			parametrosEntrada.add( new ParamEvent("UUI", (beanDatosU==null)?null:beanDatosU.toString() ) );
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/
			

			// 3.- Crear la cadena de envo hacia el CTI a partir del bean
			beanDatos = ( beanDatos == null ) ? ( new BeanDatosCTI() ) : beanDatos;
			cadenaCTI = beanDatos.construyeCadena(caracterRellenoCTI);		
			cadenaUUI = (beanDatosU==null) ? "" : beanDatosU.construyeCadena(caracterRellenoUUI);
			
			resultadoOperacion = "OK";
			
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		} finally {
			// 5.- Escribir traza de Fin			
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("cadenaCTI", cadenaCTI));
			parametrosSalida.add(new ParamEvent("cadenaUUI", cadenaUUI));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
	}
//Metodo para desbloquear los tonos del asesor
//	recive UUIID
	public String UnBlockDTMF(String idLlamada){
		int resultado=0;
		String idModuloLog = "UNBLOCKDTMF_CTI";
		String resultadoOperacion = "KO";
		int intentos = 0;
		String cadenaResultado = null;
		// Objeto retorno de la peticion
		BeanDatosCTI datosCTI = null;
		BeanDatosUUI datosUUI = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			
//			prepararPeticion(beanDatos,beanDatosU);
			configurarServicio();
			ivrTrans ivrtrans = new ivrTrans(url,port,timeout,maxtimeout);
//			String cadenaResultado = invocarCTI(GET_CTI);	
//			cadenaResultado=null;
			do{
				this.log.comment("Invocacion CTI intento["+(intentos+1)+"]");
				resultado = ivrtrans.UnBlockDTMF(port,idLlamada);
				intentos++;
			}while( resultado!=1 && intentos < maxIntentos );
			if(resultado==1)
				resultadoOperacion= "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("respuesta", ""+resultado) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		return resultadoOperacion;	
	}
//	Medoto para la trasferencia de datos de regreso con el asesor
//	Recive UUIID de la llamda y cadena a enviar
	public String TransactionResult(String llamada,String [] cadanaResultado){
		int resultado=0;
		String idModuloLog = "TRANSACCIONRESULTDTMF_CTI";
		String resultadoOperacion = "KO";
		int intentos = 0;
		String cadenaResultado = null;
		// Objeto retorno de la peticion
		BeanDatosCTI datosCTI = null;
		BeanDatosUUI datosUUI = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			//prepararPeticion(beanDatos,beanDatosU);
			configurarServicio();
			ivrTrans ivrtrans = new ivrTrans(url,port,timeout,maxtimeout);
			cadenaResultado = invocarCTI(GET_CTI);	
//			cadenaResultado=null;
			do{
				this.log.comment("Invocacion CTI intento["+(intentos+1)+"]");
				System.out.println("TAMANIO: " + cadanaResultado.length);
				System.out.println("CADENOTA RESULT: " + cadanaResultado.toString());
				System.out.println("NO SE LLAMADA: " + llamada);
				System.out.println("PUERTO: " + port);
				System.out.println("VARIABLE: "+UDConstans.Commands.CMD_IVRTRANSACTION);
				resultado = ivrtrans.TransactionResult(port,llamada, UDConstans.Commands.CMD_IVRTRANSACTION, true, "Transacction OK", cadanaResultado);
				intentos++;
			}while( resultado!=1 && intentos < maxIntentos );
			if(resultado==1)
				resultadoOperacion= "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			e.printStackTrace();
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
//			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, ));
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("respuesta", ""+((resultado==0)?"null":resultado)) );
			parametrosSalida.add(new ParamEvent("datos",Arrays.toString(cadanaResultado)));
			
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		return resultadoOperacion;	
	}
}