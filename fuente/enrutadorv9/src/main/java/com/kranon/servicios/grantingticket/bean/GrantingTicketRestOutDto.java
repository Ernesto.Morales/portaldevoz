package com.kranon.servicios.grantingticket.bean;

public class GrantingTicketRestOutDto {

	private AuthenticationResponse authentication;
	private BackendUserResponse backendUserResponse;

	public AuthenticationResponse getAuthentication() {
		return authentication;
	}

	public void setAuthentication(AuthenticationResponse authentication) {
		this.authentication = authentication;
	}

	public BackendUserResponse getBackendUserResponse() {
		return backendUserResponse;
	}

	public void setBackendUserResponse(BackendUserResponse backendUserResponse) {
		this.backendUserResponse = backendUserResponse;
	}

	@Override
	public String toString() {
		return String.format("GrantingTicketRestOutDto [authentication=%s, backendUserResponse=%s]", authentication, backendUserResponse);
	}

}
