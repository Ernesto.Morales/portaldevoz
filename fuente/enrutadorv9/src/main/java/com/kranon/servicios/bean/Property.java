package com.kranon.servicios.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Objeto de Respuesta de la peticion HTTP
 * 
 * @author aarce
 *
 */
public class Property {

	@JsonProperty("value")
	private String value;
	
	@JsonProperty("name")
	private String name;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	@Override
	public String toString() {
		return "Property [value=" + value + ", name=" + name + "]";
	}
	
	
	

	
}
