package com.kranon.servicios.cti.bean;

/**
 * Bean con los Datos de UUI
 * 
 * @author abalfaro
 *
 */
public class BeanDatosUUI {

	private String llave;
	private String llaveSegmento;
	private String xferId;
	private String segmentoCliente;
	private String canal;
	private String numCliente;
	private String llaveAccesoFront;
	private String metodoAutenticacion;
	private String numeroSesion;
	private String campania;

	private static final int LONGITUD_LLAVE=10;
	private static final int LONGITUD_LLAVE_SEGMENTO=2;
	private static final int LONGITUD_XFERID=5;
	private static final int LONGITUD_SEGMENTO_CLIENTE=3;
	private static final int LONGITUD_CANAL=4;
	private static final int LONGITUD_NUM_CLIENTE=8;
	private static final int LONGITUD_LLAVE_ACCESO_FRONT=20;
	private static final int LONGITUD_METODO_AUTENTICACION=2;
	private static final int LONGITUD_NUMERO_SESION=10;
	private static final int LONGITUD_CAMPANIA=10;
	
	private static final int LONGITUD_CADENA = 74;

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getLlaveSegmento() {
		return llaveSegmento;
	}

	public void setLlaveSegmento(String llaveSegmento) {
		this.llaveSegmento = llaveSegmento;
	}

	public String getXferId() {
		return xferId;
	}

	public void setXferId(String xferId) {
		this.xferId = xferId;
	}

	public String getSegmentoCliente() {
		return segmentoCliente;
	}

	public void setSegmentoCliente(String segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getNumCliente() {
		return numCliente;
	}

	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	public String getLlaveAccesoFront() {
		return llaveAccesoFront;
	}

	public void setLlaveAccesoFront(String llaveAccesoFront) {
		this.llaveAccesoFront = llaveAccesoFront;
	}

	public String getMetodoAutenticacion() {
		return metodoAutenticacion;
	}

	public void setMetodoAutenticacion(String metodoAutenticacion) {
		this.metodoAutenticacion = metodoAutenticacion;
	}

	public String getNumeroSesion() {
		return numeroSesion;
	}

	public void setNumeroSesion(String numeroSesion) {
		this.numeroSesion = numeroSesion;
	}

	public String getCampania() {
		return campania;
	}

	public void setCampania(String campania) {
		this.campania = campania;
	}

	@Override
	public String toString() {
		return String.format("BeanDatosUUI [llave=%s, llaveSegmento=%s, xferId=%s, segmentoCliente=%s, canal=%s, numCliente=%s, llaveAccesoFront=%s, metodoAutenticacion=%s, numeroSesion=%s, campania=%s]",
				llave, llaveSegmento, xferId, segmentoCliente, canal, numCliente, llaveAccesoFront, metodoAutenticacion, numeroSesion, campania);
	}

	
	
	/**
	 * Construye la cadena UUI a partir de los atributos del objeto
	 * 
	 * @return {@link String} cadena UUI a guardar
	 */
	public String construyeCadena(String caracterRelleno) {
		String resultado="";

		caracterRelleno = (caracterRelleno == null || caracterRelleno.equals("") ) ? "#" : caracterRelleno;
				
		resultado += obtenerValor(caracterRelleno,LONGITUD_LLAVE,llave);
		resultado += obtenerValor(caracterRelleno,LONGITUD_LLAVE_SEGMENTO,llaveSegmento);
		resultado += obtenerValor(caracterRelleno,LONGITUD_XFERID,xferId);
		resultado += obtenerValor(caracterRelleno,LONGITUD_SEGMENTO_CLIENTE,segmentoCliente);
		resultado += obtenerValor(caracterRelleno,LONGITUD_CANAL,canal);
		resultado += obtenerValor(caracterRelleno,LONGITUD_NUM_CLIENTE,numCliente);
		resultado += obtenerValor(caracterRelleno,LONGITUD_LLAVE_ACCESO_FRONT,llaveAccesoFront);
		resultado += obtenerValor(caracterRelleno,LONGITUD_METODO_AUTENTICACION,metodoAutenticacion);
		resultado += obtenerValor(caracterRelleno,LONGITUD_NUMERO_SESION,numeroSesion);
		resultado += obtenerValor(caracterRelleno,LONGITUD_CAMPANIA,campania);
		
		return resultado;
	}

	/**
	 * Parsea la cadena UUI al objeto 
	 * 
	 * @param {@link String} cadena UUI
	 * @return {@link BeanDatosUUI} objeto relleno con la cadena cti
	 */
	public static BeanDatosUUI parseaCadena(String cadena) {

		BeanDatosUUI bean = null;
		int indice = 0;
		if ( cadena != null && cadena.length()>=LONGITUD_CADENA) {
			cadena = cadena.substring(0, LONGITUD_CADENA);
			bean = new BeanDatosUUI();
			bean.setLlave( cadena.substring(indice, indice += LONGITUD_LLAVE) );
			bean.setLlaveSegmento( cadena.substring(indice, indice += LONGITUD_LLAVE_SEGMENTO ) );
			bean.setXferId( cadena.substring(indice, indice += LONGITUD_XFERID ) );
			bean.setSegmentoCliente( cadena.substring(indice, indice += LONGITUD_SEGMENTO_CLIENTE ) );
			bean.setCanal( cadena.substring(indice, indice += LONGITUD_CANAL ) );
			bean.setNumCliente( cadena.substring(indice, indice += LONGITUD_NUM_CLIENTE ) );
			bean.setLlaveAccesoFront( cadena.substring(indice, indice += LONGITUD_LLAVE_ACCESO_FRONT ) );
			bean.setMetodoAutenticacion( cadena.substring(indice, indice += LONGITUD_METODO_AUTENTICACION ) );
			bean.setNumeroSesion( cadena.substring(indice, indice += LONGITUD_NUMERO_SESION ) );
			bean.setCampania( cadena.substring(indice, indice += LONGITUD_CAMPANIA ) );
		}

		return bean;

	}

	private String obtenerValor(String caracterRelleno,int longitud,String valor){
		
		int longitudValor = 0;
		valor = ( valor==null)?"":valor;
		valor = ( valor.length() > longitud )?valor.substring(0, longitud):valor;
		
		longitudValor = valor.length();
		for(int cont=0;cont<(longitud-longitudValor);cont++){
			valor += caracterRelleno;
		}
		
		return valor;		
	}

}
