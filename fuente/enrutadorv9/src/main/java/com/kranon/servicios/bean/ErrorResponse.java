package com.kranon.servicios.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Objeto de Respuesta de la peticion HTTP
 * 
 * @author aarce
 *
 */
public class ErrorResponse {

	@JsonProperty("version")
	private String version;
	
	@JsonProperty("severity")
	private String severity;
	
	@JsonProperty("http-status")
	private String httpStatus;
	
	@JsonProperty("error-code")
	private String errorCode;
	
	@JsonProperty("error-message")
	private String errorMessage;
	
	@JsonProperty("consumer-request-id")
	private String consumerRequestId;
	
	@JsonProperty("system-error-code")
	private String systemErrorCode;
	
	@JsonProperty("system-error-description")
	private String systemErrorDescription;

	@JsonProperty("error-metainfo")
	private ErrorMetainfo errorMetainfo;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getConsumerRequestId() {
		return consumerRequestId;
	}

	public void setConsumerRequestId(String consumerRequestId) {
		this.consumerRequestId = consumerRequestId;
	}

	public String getSystemErrorCode() {
		return systemErrorCode;
	}

	public void setSystemErrorCode(String systemErrorCode) {
		this.systemErrorCode = systemErrorCode;
	}

	public String getSystemErrorDescription() {
		return systemErrorDescription;
	}

	public void setSystemErrorDescription(String systemErrorDescription) {
		this.systemErrorDescription = systemErrorDescription;
	}

	public ErrorMetainfo getErrorMetainfo() {
		return errorMetainfo;
	}

	public void setErrorMetainfo(ErrorMetainfo errorMetainfo) {
		this.errorMetainfo = errorMetainfo;
	}

	@Override
	public String toString() {
		return "ErrorResponse [version=" + version + ", severity=" + severity
				+ ", httpStatus=" + httpStatus + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", consumerRequestId="
				+ consumerRequestId + ", systemErrorCode=" + systemErrorCode
				+ ", systemErrorDescription=" + systemErrorDescription
				+ ", errorMetainfo=" + errorMetainfo + "]";
	}
	
	

	
}
