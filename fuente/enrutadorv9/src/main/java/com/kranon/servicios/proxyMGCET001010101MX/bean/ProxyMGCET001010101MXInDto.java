package com.kranon.servicios.proxyMGCET001010101MX.bean;

public class ProxyMGCET001010101MXInDto {
	
	private String identificador;
	private String archivollamada;
	private String archivocontador;
	private String usuariomodifica;
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getArchivollamada() {
		return archivollamada;
	}
	public void setArchivollamada(String archivollamada) {
		this.archivollamada = archivollamada;
	}
	public String getArchivocontador() {
		return archivocontador;
	}
	public void setArchivocontador(String archivocontador) {
		this.archivocontador = archivocontador;
	}
	public String getUsuariomodifica() {
		return usuariomodifica;
	}
	public void setUsuariomodifica(String usuariomodifica) {
		this.usuariomodifica = usuariomodifica;
	}
	@Override
	public String toString(){
		return String.format("ProxyMGCET001010101MXInDto [identificador=%s, archivollamada=%s, archivocontador=%s, usuariomodifica=%s]"
				, identificador, archivollamada, archivocontador, usuariomodifica);
	}
	
	
	
	
	

}
