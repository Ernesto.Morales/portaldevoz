package com.kranon.servicios.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Objeto de Respuesta de la peticion HTTP
 * 
 * @author aarce
 *
 */
public class ErrorMetainfo {

	@JsonProperty("property")
	private List<Property> property;
	

	public List<Property> getProperty() {
		return property;
	}

	public void setProperty(List<Property> property) {
		this.property = property;
	}

	@Override
	public String toString() {
		return "ErrorMetainfo [property=" + property + "]";
	}
	
	
	
	

	
}
