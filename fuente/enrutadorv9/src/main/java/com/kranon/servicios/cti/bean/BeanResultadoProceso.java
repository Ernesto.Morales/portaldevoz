package com.kranon.servicios.cti.bean;

public class BeanResultadoProceso {

	private String resultadoOperacion;

	private String error;

	public String getResultadoOperacion() {
		return resultadoOperacion;
	}

	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "BeanResultadoProceso [resultadoOperacion=" + resultadoOperacion + ", error=" + error + "]";
	}

}
