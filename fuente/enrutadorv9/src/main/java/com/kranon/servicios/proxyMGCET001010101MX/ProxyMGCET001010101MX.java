package com.kranon.servicios.proxyMGCET001010101MX;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.ConexionHttp;
import com.kranon.servicios.UtilidadesResponse;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.bean.ErrorResponse;
import com.kranon.servicios.proxyMGCET001010101MX.bean.ProxyMGCET001010101MXInDto;
import com.kranon.servicios.proxyMGCET001010101MX.bean.ProxyMGCET001010101MXOutDto;
import com.kranon.servicios.proxyMGCET001010101MX.bean.ProxyMGCET001010101MXOutDtoXML;
import com.kranon.singleton.SingletonWebServices;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;

/**
 * Clase utilizada para la invocacion del servicio de ProxyMGCET001010101MX en la integracion del CUADRO de MANDO
 *
 */
public class ProxyMGCET001010101MX {

	private CommonLoggerProcess log;
	private String url;
	private int timeout;
	private String inputJson;
	private HashMap<String, String> header;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String idInvocacion;
	private String idServicio;
	private String idElemento;
	private String nombre;
	private String version;
	private String idWebServices = "PROXY_MGCET001010101MX";
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public ProxyMGCET001010101MX(String idInvocacion, String idServicio, String idElemento) {

		if (this.log == null) {
			this.log = new CommonLoggerProcess(this.idWebServices.concat("_").concat(idServicio.toUpperCase()));
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idServicio = idServicio;
		this.idElemento = idElemento;
		this.idInvocacion = idInvocacion;
	}

	/**
	 * Mtodo que prepara el JSON de entrada al Servicio de Proxy Service ProxyMGCET001010101MX con los parmetros de entrada
	 * 
	 * @param tsec
	 *            Token de seguridad devuelto por Granting Ticket
	 * @param identificador
	 *            callid de la llamada
	 * @param archivollamada
	 *            base64 del fichero de llamada
	 * @param archivocontador
	 *            base64 del fichero de contador
	 * @param usuariomodifica
	 *            (fijo)
	 */
	public void preparaPeticion(String tsec, String identificador, String archivollamada, String archivocontador) {

		String idModuloLog = "PREPARA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";

		try {
			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("identificador", identificador));
			parametrosEntrada.add(new ParamEvent("archivollamada", archivollamada));
			parametrosEntrada.add(new ParamEvent("archivocontador", archivocontador));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url y el timeout del Xml

			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}

			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}
			url = datosEntorno.getProxyMGCET001010101MX().getUrl();
			timeout = beanWebServices.getTimeout();
			// [201704_NMB] Se obtiene el nombre y la version del WS para las trazas de monitorizacion
			nombre = datosEntorno.getProxyMGCET001010101MX().getNombre();
			version = datosEntorno.getProxyMGCET001010101MX().getVersion();

			// Recupero parametros fijos
			String usuariomodifica = datosEntorno.getProxyMGCET001010101MX().getValueParametro("usuariomodifica");

			// 3.- Crear jSon con los parametros de entrada del servicio
			ProxyMGCET001010101MXInDto proxyMGCET001010101MXInDto = new ProxyMGCET001010101MXInDto();
			proxyMGCET001010101MXInDto.setIdentificador(identificador);
			proxyMGCET001010101MXInDto.setArchivollamada(archivollamada);
			proxyMGCET001010101MXInDto.setArchivocontador(archivocontador);
			proxyMGCET001010101MXInDto.setUsuariomodifica(usuariomodifica);

			// Parseo el objeto a JSON, SIN el wrapper
			inputJson = this.parserEntrada(proxyMGCET001010101MXInDto);

			// 4.- Crear el header
			header = new HashMap<String, String>();
			if (tsec != null) {
				header.put("tsec", tsec);
			}

			resultadoOperacion = "OK";
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		} finally {
			// 5.- Escribir traza de Fin

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("inputJson", inputJson));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}

	}

	/**
	 * Realiza la peticin POST al servicio de Proxy Service PROXY_MGCET001010101MX
	 * 
	 * @return {@link com.kranon.servicios.bean.ConexionResponse} objeto con la respuesta
	 */
	public ConexionResponse ejecutaPeticion() {

		String idModuloLog = "EJECUTA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";

		// Objeto retorno de la peticin
		ConexionResponse conexionResponse = null;
		try {

			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("url", url));
			parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
			parametrosEntrada.add(new ParamEvent("inputJson", inputJson));
			parametrosEntrada.add(new ParamEvent("header", (header == null ? "null" : header.toString())));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/


			// 2.- Ejecuta la peticin

			// Creo el objeto de la conexin
			ConexionHttp conexionHttp = new ConexionHttp(this.log.getIdInvocacion(), this.log.getIdElemento());
			// Realizo la conexin
			conexionResponse = conexionHttp.executePost(url, timeout, inputJson, header);

			// 3.- Tratar la salida
			if (conexionResponse == null) {

				this.log.comment("WS_RESPONSE_IS_NULL");

				conexionResponse = new ConexionResponse();
				conexionResponse.setCodigoRespuesta(0);
				conexionResponse.setResultado("ERROR");
				conexionResponse.setMensajeError("WS_RESPONSE_IS_NULL");

				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, "MONITORIZACION_" + this.idServicio);
				this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************

			} else {

				if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {

					// la operacin ha ido bien
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeRespuesta=" + conexionResponse.getMensajeRespuesta());

				} else {
					// la operacin ha ido KO o ERROR
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeError=" + conexionResponse.getMensajeError());

					if (conexionResponse.getResultado().equalsIgnoreCase("KO")) {
						// si el resultado es KO, es decir, hay respuesta del WS pero no es 200-OK
						// parseo el mensaje de error
						ErrorResponse errorResponse = null;
						if(conexionResponse.getMensajeError() != null){
							UtilidadesResponse utils = new UtilidadesResponse(this.log);
							errorResponse = utils.parserError(conexionResponse.getMensajeError());
							conexionResponse.setErrorResponse(errorResponse);
						}

						// ****************************************************************
						// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
						// ****************************************************************
						if(errorResponse != null){
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, "MONITORIZACION_" + this.idServicio);
							// this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), null);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), errorResponse.getErrorCode(), errorResponse.getErrorMessage(), null);
						} else {
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, "MONITORIZACION_" + this.idServicio);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
						}
					}
					else {
						this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, "MONITORIZACION_" + this.idServicio);
						this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
					}
					// ****************************************************************
					// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
					// ****************************************************************
				}
			}

			resultadoOperacion = conexionResponse.getResultado();

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			if (conexionResponse == null) {
				conexionResponse = new ConexionResponse();
			}
			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());		
			conexionResponse.setCodigoRespuesta(0);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, "MONITORIZACION_" + this.idServicio);
			this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse == null ? "null" : conexionResponse.toString()));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}

		return conexionResponse;
	}

	/**
	 * Parsea el objeto de entrada a una cadena JSON, sin incluir el wrapper
	 * 
	 * @param object
	 *            , objeto de tipo {@link ProxyMGCET001010101MXInDto} a convertir a JSON sin el objeto wrapper
	 * @return cadena JSON
	 * @throws JsonProcessingException
	 */
	private String parserEntrada(ProxyMGCET001010101MXInDto proxyMGCET001010101MXInDto) throws JsonProcessingException {

		// Convierto el objeto JSON
		ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String inputJson = mapper.writeValueAsString(proxyMGCET001010101MXInDto);

		return inputJson;
	}

	/**
	 * Parsea la cadena JSON de salida de la peticin del servicio y la convierte en un objeto {@link ProxyServiceBGL3RestOutDto}
	 * 
	 * @param outputJson
	 *            cadena JSON recibida en la peticin
	 * @return {@link ProxyServiceBGL3RestOutDto} objeto resultado
	 */
	public ProxyMGCET001010101MXOutDto parserSalida(String outputJson) {
		ProxyMGCET001010101MXOutDto proxyMGCET001010101MXOutDto = null;
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			if (outputJson != null) {
				proxyMGCET001010101MXOutDto = mapper.readValue(outputJson, ProxyMGCET001010101MXOutDto.class);
			}

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("proxyMGCET001010101MXOutDto", proxyMGCET001010101MXOutDto == null ? "null"
					: proxyMGCET001010101MXOutDto.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

		} catch (Exception e) {
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputJson", outputJson == null ? "null" : outputJson));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

		}
		return proxyMGCET001010101MXOutDto;
	}

	public ProxyMGCET001010101MXOutDtoXML parserSalidaXML(String outputXml) {
		ProxyMGCET001010101MXOutDtoXML proxyMGCET001010101MXOutDto = null;

		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(ProxyMGCET001010101MXOutDtoXML.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(outputXml);
			proxyMGCET001010101MXOutDto = (ProxyMGCET001010101MXOutDtoXML) unmarshaller.unmarshal(reader);

		} catch (Exception e) {
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputXml", outputXml == null ? "null" : outputXml));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("PARSEAR_SALIDA_XML", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/


		}
		return proxyMGCET001010101MXOutDto;

	}

}
