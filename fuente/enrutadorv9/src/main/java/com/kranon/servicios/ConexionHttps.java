package com.kranon.servicios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.bean.ConexionResponse;
import javax.net.ssl.HttpsURLConnection;

/**
 * Metodos para consumir un servicio REST con operaciones GET/POST/PUT
 *
 */
public class ConexionHttps {

	private CommonLoggerProcess log;

	public ConexionHttps(String idInvocacion, String idElemento) {
		if (this.log == null) {
			this.log = new CommonLoggerProcess("WEB_SERVICES");
		}
		/** INICIALIZAR LOG **/
		this.log.inicializar(idInvocacion, idElemento);
	}

	public ConexionResponse executeGet(String url, int timeout, HashMap<String, String> header, HashMap<String, String> paramsUrl) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		parametrosEntrada.add(new ParamEvent("paramsUrl", (paramsUrl == null ? "null" : paramsUrl.toString())));
		this.log.initModuleProcess("CONEXION_HTTP_GET", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {
			String urlEnvio = url;
			if (paramsUrl != null) {
				urlEnvio = urlEnvio + "?";
				Iterator<Entry<String, String>> it = paramsUrl.entrySet().iterator();
				while (it.hasNext()) {
					Entry<String, String> e = (Map.Entry<String, String>) it.next();
					urlEnvio = urlEnvio + e.getKey() + "=" + e.getValue();
					if (it.hasNext()) {
						urlEnvio = urlEnvio + "&";
					}
				}
			}

			URL urlServicio = new URL(urlEnvio);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setRequestMethod("GET");

			// Configuracion
			conn.setRequestProperty("Accept", "application/json");		
			
			
			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio("");

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				String mensajeError = "";
				if (conn.getErrorStream()!= null)
				{	
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(mensajeError);
				}
				else
				{
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(null);
				}
				

				

			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}

			}

		} catch (Exception e) {
			// Posibles errores: Read timed out

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_GET", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());

			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_GET", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executePost(String url, int timeout, String body, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("body", body));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_POST", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {

			URL urlServicio = new URL(url);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(timeout);

			// Configuracion
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Content-enconding", "chunked");
			conn.setRequestProperty("Accept", "application/json");			
			
			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio(body);

			// Realizo la conexion
			os = new OutputStreamWriter(conn.getOutputStream());
			os.write(body);
			os.flush();

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				String mensajeError = "";
				if(conn.getErrorStream() != null)
				{	
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
				}
				conexionResponse.setResultado("KO");
				conexionResponse.setMensajeError(mensajeError);
				conexionResponse.setMensajeRespuesta("");
				
				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				
				if (map != null)
				{	
					for (Map.Entry<String, List<String>> entry : map.entrySet()) {
						if (conexionResponse.getHeaderRespuesta() == null) {
							conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
						}
						conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
					}
				}
			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_POST", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());
			conexionResponse.setMensajeRespuesta("");

//			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_POST", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executePut(String url, int timeout, String body, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("body", body));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_PUT", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {

			URL urlServicio = new URL(url);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");

			// Configuracion
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Content-enconding", "chunked");
			conn.setRequestProperty("Accept", "application/json");

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio(body);

			// Realizo la conexion
			os = new OutputStreamWriter(conn.getOutputStream());
			os.write(body);
			os.flush();

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
				String mensajeError = brError.readLine();

				conexionResponse.setResultado("KO");
				conexionResponse.setMensajeError(mensajeError);

			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_PUT", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("KO");
			conexionResponse.setMensajeError(e.getMessage());

			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_PUT", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}
}
