package com.kranon.servicios.grantingticket.bean;

import java.util.List;

public class AuthenticationData {

	private String idAuthenticationData;
	private List<String> authenticationData;

	public String getIdAuthenticationData() {
		return idAuthenticationData;
	}

	public void setIdAuthenticationData(String idAuthenticationData) {
		this.idAuthenticationData = idAuthenticationData;
	}

	public List<String> getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(List<String> authenticationData) {
		this.authenticationData = authenticationData;
	}

	@Override
	public String toString() {
		return String.format("AuthenticationData [idAuthenticationData=%s, authenticationData=%s]", idAuthenticationData, authenticationData);
	}

}
