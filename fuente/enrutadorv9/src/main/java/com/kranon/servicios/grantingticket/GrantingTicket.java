package com.kranon.servicios.grantingticket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.ConexionHttp;
import com.kranon.servicios.UtilidadesResponse;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.bean.ErrorResponse;
import com.kranon.servicios.grantingticket.bean.AuthenticationData;
import com.kranon.servicios.grantingticket.bean.AuthenticationRequest;
import com.kranon.servicios.grantingticket.bean.BackendUserRequest;
import com.kranon.servicios.grantingticket.bean.GrantingTicketRestInDto;
import com.kranon.servicios.grantingticket.bean.GrantingTicketRestOutDto;
import com.kranon.singleton.SingletonWebServices;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Metodos para consumir el Servicio REST de Granting Ticket
 *
 */
public class GrantingTicket {

	private CommonLoggerProcess log;
	private String url;
	private int timeout;
	private String inputJson;
	private String inputJsonCodificado;
	private HashMap<String, String> header;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String idInvocacion;
	private String idServicio;
	private String nombre;
	private String version;
	private String idWebServices = "GRANTING_TICKET";
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public GrantingTicket(String idInvocacion, String idServicio, String idElemento) {

		if (this.log == null) {
			this.log = new CommonLoggerProcess(idElemento + "_" + this.idWebServices + "_" + idServicio.toUpperCase());
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idServicio = idServicio;
		this.idInvocacion = idInvocacion;
	}

	/**
	 * Metodo que prepara los datos de entrada al Servicio de Granting Ticket con los parametros de entrada
	 * 
	 * @param userID
	 *            identificador de usuario + codigo de acceso
	 * @param consumerID
	 *            identificador AAP de representacion del canal
	 * @param authenticationType
	 *            tipo de autenticacion
	 * @param idAuthenticationData
	 *            identificador del dato de autenticacion
	 * @param autenticacionData
	 *            valor del dato de autenticacion
	 * @param userId
	 *            identificador del usuario
	 * @param accessCode
	 *            codigo de acceso
	 * @param dialogId
	 * @param backendSessionId
	 *            header de entrada al servicio. Si no se envia valor debe ser null
	 * @param clientId
	 *            header de entrada al servicio. Si no se envia valor debe ser null
	 * 
	 */
	public void preparaPeticion(String userId, String accessCode, String dialogId, String backendSessionIdHeader, String clientIdHeader) {
		// Esto corresponde con el GT PUBLICO
		// userAut es un dato fijo que vamos a leer del xml
		// producto = PUBLICO
		// datoIntroducido, numCliente, phoneNumber - para el GT publico no se necesita
		preparaPeticion("", userId, accessCode, dialogId, backendSessionIdHeader, clientIdHeader, "PUBLICO", "", "", "", "");

	}

	public void preparaPeticion(String userAut, String userId, String accessCode, String dialogId, String backendSessionIdHeader,
			String clientIdHeader, String producto, String datoIntroducido, String numCliente, String phoneNumber, String idSessionGenerado) {
		// Esto corresponde con el GT PRIVADO
		String idModuloLog = "PREPARA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";

		try {
			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("userAut", UtilidadesBeans.cifrar(userAut,5)));
			parametrosEntrada.add(new ParamEvent("userId", userId));
			parametrosEntrada.add(new ParamEvent("accessCode", UtilidadesBeans.cifrar(accessCode,5)));
			parametrosEntrada.add(new ParamEvent("dialogId", dialogId));
			parametrosEntrada.add(new ParamEvent("backendSessionId", backendSessionIdHeader));
			parametrosEntrada.add(new ParamEvent("clientId", clientIdHeader));
			parametrosEntrada.add(new ParamEvent("producto", producto));
			parametrosEntrada.add(new ParamEvent("datoIntroducido", UtilidadesBeans.cifrar(datoIntroducido, 0)));
			parametrosEntrada.add(new ParamEvent("numCliente", numCliente));
			parametrosEntrada.add(new ParamEvent("phoneNumber", phoneNumber));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url y el timeout del Xml

			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}

			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}
			url = datosEntorno.getGrantingTicket().getUrl();
			timeout = beanWebServices.getTimeout();
			// [201704_NMB] Se obtiene el nombre y la version del WS para las trazas de monitorizacion
			nombre = datosEntorno.getGrantingTicket().getNombre();
			version = datosEntorno.getGrantingTicket().getVersion();

			/** RECUPERO PARAMETROS FIJOS **/
			// consumerID : identificador AAP de representacion del canal
			String consumerID = (datosEntorno.getGrantingTicket().getValueParametro("consumerID", producto) != null) ? datosEntorno
					.getGrantingTicket().getValueParametro("consumerID", producto) : "";

					// authenticationType : tipo de autenticacion
					String authenticationType = (datosEntorno.getGrantingTicket().getValueParametro("authenticationType", producto) != null) ? datosEntorno
							.getGrantingTicket().getValueParametro("authenticationType", producto) : "";

							// idAuthenticationData : identificador del dato de autenticacion
							String idAuthenticationData = (datosEntorno.getGrantingTicket().getValueParametro("idAuthenticationData", producto) != null) ? datosEntorno
									.getGrantingTicket().getValueParametro("idAuthenticationData", producto) : "";

									// authenticationData : valor del dato de autenticacion
									String authenticationData = "";
									if (producto.equalsIgnoreCase("PUBLICO")) {
										// userID : identificador de usuario + codigo de acceso
										userAut = (datosEntorno.getGrantingTicket().getValueParametro("userID", producto) != null) ? datosEntorno.getGrantingTicket()
												.getValueParametro("userID", producto) : "";

												authenticationData = (datosEntorno.getGrantingTicket().getValueParametro("authenticationData", producto) != null) ? datosEntorno
														.getGrantingTicket().getValueParametro("authenticationData", producto) : "";
									} else if (producto.equalsIgnoreCase("HUELLA_VOZ")) {

										String idSession = "";
										if (idSessionGenerado != null && !idSessionGenerado.equals("")) {
											// ya he generado un anterior idSession
											idSession = idSessionGenerado;
										} else {
											idSession = (datosEntorno.getGrantingTicket().getValueParametro("idSession", producto) != null) ? datosEntorno
													.getGrantingTicket().getValueParametro("idSession", producto) : "";
										}

										String phoneType = (datosEntorno.getGrantingTicket().getValueParametro("phoneType", producto) != null) ? datosEntorno
												.getGrantingTicket().getValueParametro("phoneType", producto) : "";

												String phoneNumberFijo = (datosEntorno.getGrantingTicket().getValueParametro("phoneNumber", producto) != null) ? datosEntorno
														.getGrantingTicket().getValueParametro("phoneNumber", producto) : "";

														// ConvertirWavToBase64 convertirWavToBase64 = new ConvertirWavToBase64();
														// String audioEnBase64 = convertirWavToBase64.convertirWavToBase64(datoIntroducido);
														// authenticationData = audioEnBase64 + "|" + numCliente + "|" + phoneNumber + "|" + idSession + "|" + phoneType;

														// TODO: El WS deberia ir con el telefono del cliente, pero por el momento lo vamos a dejar con un valor fijo
														// authenticationData = datoIntroducido + "|" + numCliente + "|" + phoneNumber + "|" + idSession + "|" + phoneType;
														authenticationData = datoIntroducido + "|" + numCliente + "|" + phoneNumberFijo + "|" + idSession + "|" + phoneType;
									} else {
										// TOKEN, SOFT_TOKEN, NIP
										authenticationData = datoIntroducido + "|" + numCliente;
									}

									/** INICIO EVENTO - ACCION **/
									ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
									parametrosAdicionales.add(new ParamEvent("consumerID", consumerID));
									parametrosAdicionales.add(new ParamEvent("authenticationType", authenticationType));
									parametrosAdicionales.add(new ParamEvent("idAuthenticationData", idAuthenticationData));
									parametrosAdicionales.add(new ParamEvent("authenticationData", UtilidadesBeans.cifrar(authenticationData,0)));
									this.log.actionEvent("GET_PARAMS_FIJOS", "OK", parametrosAdicionales);
									/** FIN EVENTO - ACCION **/
									
									// 3.- Crear jSon con los parametros de entrada del servicio

									// Preparo el objeto de entrada al servicio
									GrantingTicketRestInDto grantingTicketRestInDto = new GrantingTicketRestInDto();
									AuthenticationRequest authentication = new AuthenticationRequest();
									BackendUserRequest backendUserRequest = new BackendUserRequest();

									String[] idDatos = idAuthenticationData.split("\\|");
									String[] datos = authenticationData.split("\\|");

									AuthenticationData[] beanAuthenticationData = new AuthenticationData[idDatos.length];
									for (int i = 0; i < idDatos.length; i++) {
										beanAuthenticationData[i] = new AuthenticationData();
										beanAuthenticationData[i].setIdAuthenticationData(idDatos[i]);
										List<String> authenData = new ArrayList<String>();
										authenData.add(datos[i]);
										beanAuthenticationData[i].setAuthenticationData(authenData);
									}

									authentication.setUserID(userAut);
									authentication.setConsumerID(consumerID);
									authentication.setAuthenticationType(authenticationType);
									authentication.setAuthenticationData(Arrays.asList(beanAuthenticationData));

									backendUserRequest.setUserId(userId);
									backendUserRequest.setAccessCode(accessCode);
									backendUserRequest.setDialogId(dialogId);

									grantingTicketRestInDto.setAuthentication(authentication);

									grantingTicketRestInDto.setBackendUserRequest(backendUserRequest);

									// Parseo el objeto a JSON, SIN el wrapper
									inputJson = this.parserEntrada(grantingTicketRestInDto);
			
			
			// Creamos un jSon codificado para no mostrar datos sensibles
			inputJsonCodificado = this.parserEntrada(grantingTicketRestInDto);
			inputJsonCodificado = inputJsonCodificado.replaceAll(userAut, UtilidadesBeans.cifrar(userAut,5));
			inputJsonCodificado = inputJsonCodificado.replaceAll(accessCode, UtilidadesBeans.cifrar(accessCode,5));
			inputJsonCodificado = inputJsonCodificado.replaceAll(datoIntroducido, UtilidadesBeans.cifrar(datoIntroducido,5));
			

									// 4.- Crear el header

									// preparo los header de entrada
									if (backendSessionIdHeader != null || clientIdHeader != null) {
										header = new HashMap<String, String>();
										if (userAut != null) {
											header.put("backendSessionId", backendSessionIdHeader);
										}
										if (userAut != null) {
											header.put("clientId", userAut);
										}
									}

									resultadoOperacion = "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		} finally {
			// 5.- Escribir traza de Fin

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("inputJson", inputJsonCodificado));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
	}

	/**
	 * Realiza la peticion POST al servicio de Granting Ticket
	 * 
	 * @return {@link ConexionResponse} objeto con la respuesta
	 */
	public ConexionResponse ejecutaPeticion() {

		String idModuloLog = "EJECUTA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";

		// Objeto retorno de la peticion
		ConexionResponse conexionResponse = null;
		try {
			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("url", url));
			parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
			parametrosEntrada.add(new ParamEvent("inputJson", inputJsonCodificado));
			parametrosEntrada.add(new ParamEvent("header", (header == null ? "null" : header.toString())));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Ejecuta la peticion

			// Creo el objeto de la conexion
			ConexionHttp conexionHttp = new ConexionHttp(this.log.getIdInvocacion(), this.log.getIdElemento());
			// Realizo la conexion
			conexionResponse = conexionHttp.executePost(url, timeout, inputJson, header);

			// 3.- Tratar la salida

			if (conexionResponse == null) {

				this.log.comment("WS_RESPONSE_IS_NULL");

				conexionResponse = new ConexionResponse();
				conexionResponse.setCodigoRespuesta(0);
				conexionResponse.setResultado("ERROR");
				conexionResponse.setMensajeError("WS_RESPONSE_IS_NULL");

				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
				this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************	

			} else {
				
				// en los metodos POST hay que codificar el mensaje de entrada
				conexionResponse.setMensajeEnvioCodificado(inputJsonCodificado);

				if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {
					
					// Parseo de los datos de salida que fueran necesarios
					GrantingTicketRestOutDto datosClienteCodificados = parserSalida(conexionResponse.getMensajeRespuesta());
					conexionResponse.setMensajeRespuestaCodificado(salida2Json(datosClienteCodificados));
					
					// la operacion ha ido bien
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeRespuesta=" + conexionResponse.getMensajeRespuesta());

				} else {
					// la operacion ha ido KO o ERROR
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeError=" + conexionResponse.getMensajeError());

					if (conexionResponse.getResultado().equalsIgnoreCase("KO")) {
						// si el resultado es KO, es decir, hay respuesta del WS
						// pero no es 200-OK
						// parseo el mensaje de error
						UtilidadesResponse utils = new UtilidadesResponse(this.log);
						ErrorResponse errorResponse = utils.parserError(conexionResponse.getMensajeError());
						conexionResponse.setErrorResponse(errorResponse);

						// ****************************************************************
						// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
						// ****************************************************************
						if(errorResponse != null){
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							// this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), null);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), errorResponse.getErrorCode(), errorResponse.getErrorMessage(), null);
						} else {
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
						}
					}
					else {
						this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
						this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
					}
					// ****************************************************************
					// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
					// ****************************************************************
				}
			}

			resultadoOperacion = conexionResponse.getResultado();

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			if (conexionResponse == null) {
				conexionResponse = new ConexionResponse();
			}
			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());		
			conexionResponse.setCodigoRespuesta(0);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
			this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************		

		} finally {
			// 4.- Escribir la traza de fin

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse == null ? "null" : conexionResponse.toString()));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/

		}

		return conexionResponse;
	}

	/**
	 * Parsea el objeto de entrada a una cadena JSON, SIN incluir el wrapper
	 * 
	 * @param {@link GrantingTicketRestInDto} objecto a convertir a JSON sin el objeto wrapper
	 * @return cadena JSON
	 * @throws JsonProcessingException
	 */
	private String parserEntrada(GrantingTicketRestInDto object) throws JsonProcessingException {

		// Convierto el objeto JSON, sin incluir el wrapper
		// GrantingTicketRestInDto
		ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String inputJson = mapper.writeValueAsString(object);

		return inputJson;
	}

	/**
	 * Parsea la cadena JSON de salida de la peticion del servicio
	 * 
	 * @param outputJson
	 *            cadena JSON recibida en la peticion
	 * @return {@link GrantingTicketRestOutDto} objeto parseado
	 * @throws Exception
	 */
	public GrantingTicketRestOutDto parserSalida(String outputJson) throws Exception {

		GrantingTicketRestOutDto grantingTicketRestOutDto = null;
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			if (outputJson != null) {
				grantingTicketRestOutDto = mapper.readValue(outputJson, GrantingTicketRestOutDto.class);
			} else {
				grantingTicketRestOutDto = null;
			}
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("grantingTicketRestOutDto", grantingTicketRestOutDto == null ? "null" : grantingTicketRestOutDto.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

		} catch (Exception e) {
			grantingTicketRestOutDto = null;
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputJson", outputJson == null ? "null" : outputJson));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			throw e;

		}
		return grantingTicketRestOutDto;
	}

	/**
	 * Devuelve el valor del parametro tsec que hay en el header respuesta
	 * 
	 * @param headerRespuesta
	 * @return valor de tsec
	 */
	public String dameTsec(HashMap<String, List<String>> headerRespuesta) {
		String tsec = "";
		if (headerRespuesta != null) {
			Iterator<Entry<String, List<String>>> it = headerRespuesta.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, List<String>> e = (Map.Entry<String, List<String>>) it.next();
				if (e.getKey() != null && (e.getKey().equalsIgnoreCase("tsec"))) {
					tsec = e.getValue().get(0);
				}
			}
		}
		return tsec;
	}

	// public static void main(String[] args)
	// {
	// String authenticationData="11111111| ";
	// String [] idDatos = authenticationData.split("\\|");
	// }
	
	
	/**
	 * Parsea el objeto de salida a una cadena JSON
	 * @param grantingTicketRestOutDto objeto de tipo {@link GrantingTicketRestOutDto} a convertir a JSON
	 * @return cadena JSON
	 * @throws JsonProcessingException
	 */
	private String salida2Json(GrantingTicketRestOutDto grantingTicketRestOutDto) throws JsonProcessingException {

		// Convierto el objeto JSON
		ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String outputJson = mapper.writeValueAsString(grantingTicketRestOutDto);

		return outputJson;
	}

}
