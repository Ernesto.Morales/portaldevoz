package com.kranon.marshall;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.kranon.bean.gestioncontroladores.BeanGestionControladores;
import com.kranon.bean.gestionmodulos.BeanGestionModulos;
import com.kranon.bean.gestionmodulos.BeanInfoModulo;
import com.kranon.unmarshall.Unmarshall;

public class MarshallTest {

	public static void main(String[] args) {

		try {
//			System.out.println("INICIO");
			Unmarshall un = new Unmarshall("idInv", "idEl");

			BeanGestionModulos bean = un.unmarshallGestionModulos("PRESTAMOS");

			if (bean == null) {
//				System.out.println("UNMARSHALL KO");
			} else {
//				System.out.println("UNMARSHALL OK");
//				System.out.println(bean.toString());

				for (BeanInfoModulo info : bean.getModulos()) {
					if (info != null && info.getNombre().equalsIgnoreCase("INTERBANCARIA_CLAVE")) {
//						System.out.println(info.toString());
//						System.out.println("CANDADOS ON IDENTIF: ");
//						System.out.println(info.getIdentificacion().getInstrumentosActivos());
//						System.out.println(info.getIdentificacion().getResultadoAccion("MAXINT_MENU"));
//
//						System.out.println("CANDADOS ON AUTENTIC: ");
//						System.out.println(info.getAutenticacion().getInstrumentosActivos());
//						System.out.println(info.getAutenticacion().getResultadoAccion("MAXINT_VALID"));
						break;
					}
				}
			}

//			System.out.println("FIN");
		} catch (Exception e) {
//			System.out.println("ERROR: " + e.toString());
		}

	}

	/**
	 * Convierte de BeanGestionControladores a XML
	 * 
	 * @throws Exception
	 */
	public void marshallGestionControladores(BeanGestionControladores bean) throws Exception {

		// CREACION OBJETO BeanGestionControladores
		// BeanGestionControladores bean = new BeanGestionControladores();
		//
		// BeanConfigControlador[] configuracion = new BeanConfigControlador[2];
		// configuracion[0] = new BeanConfigControlador();
		// configuracion[0].setVdnOrigen("90281");
		// configuracion[0].setXferId("");
		// configuracion[0].setNombreControlador("PATRIMONIAL");
		// configuracion[1] = new BeanConfigControlador();
		// configuracion[1].setVdnOrigen("");
		// configuracion[1].setXferId("00002");
		// configuracion[1].setNombreControlador("PATRIMONIAL");
		// bean.setConfiguracion(configuracion);
		//
		// BeanControlador[] controladores = new BeanControlador[2];
		// controladores[0] = new BeanControlador();
		// controladores[0].setNombre("PATRIMONIAL");
		// controladores[0].setActivo("ON");
		// controladores[0].setRuta("/../../CONT-PATRIMONIAL/dynamicvxml/INICIO-PARAMS.jsp");
		// controladores[1] = new BeanControlador();
		// controladores[1].setNombre("PERSONAL");
		// controladores[1].setActivo("ON");
		// controladores[1].setRuta("/../../CONT-PERSONAL/dynamicvxml/INICIO-PARAMS.jsp");
		// bean.setControladores(controladores);

		// UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica del servicio y prefijo para el fichero de
		// servicio
		// String path = util.leeParametroMarshall("PATH_XML");
		// String pathServ = util.leeParametroMarshall("PATH_XML_SERV");
		// String prefixServ = util.leeParametroMarshall("PREFIX_XML_SERV");
		// // nombre del fichero
		// String fileName = bean.getCodServIVR() + ".xml";
		//
		// // si no existe el path de Servicio lo crea
		// File filePath = new File(path + pathServ);
		// filePath.mkdirs();
		//
		// File file = new File(path + pathServ + prefixServ + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionControladores.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(bean, System.out);

	}

}
