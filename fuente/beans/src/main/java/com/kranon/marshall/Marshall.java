package com.kranon.marshall;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

//import org.h2.java.lang.System;

import com.kranon.bean.blacklist.BeanBlackList;
import com.kranon.bean.enc.BeanEncuesta;
import com.kranon.bean.menu.BeanMenu;
import com.kranon.bean.respuestas.BeanRespuestasPreguntas;
import com.kranon.bean.saldo.BeanLocSaldo;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.bean.stat.contador.BeanStatContador;
import com.kranon.bean.stat.llamada.BeanStatLlamada;
import com.kranon.bean.transfer.BeanTransfer;
import com.kranon.bean.transfer_schema.BeanTransferSchema;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.utilidades.UtilidadesBeans;
import com.kranon.bean.loggerllamada.DatosLogerLlamada;
import com.kranon.bean.maurespuestas.BeanRespuestasMau;
import com.kranon.bean.shortcut.BeanShortcuts;

/**
 * Clase que realiza el "Marshalling" de Beans a XML Clase usada por PARAMETRIZACION_SERVICIOS
 * 
 * @author abalfaro
 *
 */
public class Marshall {

	private CommonLoggerProcess log;

	// tipo principal del XML, para hacer corresponder con el DTD
	private String tipoPrincipal;
	// nombre del archivo DTD asociado
	private String nombreDTD;

	private String cabeceraXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	private String cabeceraDTD = "<!DOCTYPE " + tipoPrincipal + " SYSTEM \"" + nombreDTD + "\">";

	public Marshall(CommonLoggerProcess log) {
		this.log = log;
	}

	
    /**
     * Convierte de Bean de parametria a XML
     * 
     * @param codParamIVR El código de servicio para incluirlo en logger
     * @param bean - POJO que contiene los datos a guardar en XML
     * @param fullArch - ruta completa + nombre de archivo a respaldar y crear
     * @param dirResp - Directorio donde se respaldan los archivos
     * @param nomArchResp - nombre de archivo individual de respaldo
     * @param nomBean - Nombre del bean a instanciar en el método internamente
     * @throws Exception
     */
    public void marshall(String codParamIVR, Object bean, String fullArch, String dirResp , String nomArchResp, String nomBean) 
            throws Exception {

        String logString = Inet4Address.getLocalHost().getHostAddress() + "|PortalDeVoz|CFG|";
            logString += "|codServicio " + codParamIVR;
            logString += "|Modulo " + "PARAMETRIA|" + fullArch;
            
        try {

            // si no existe el path de Respaldo lo crea
            File pathResp = new File(dirResp);
            pathResp.mkdirs();
            
            // Para respaldo de archivos
            Date utilDate = new Date();
            long fecMil = utilDate.getTime();
            Timestamp fecTimestamp = new Timestamp(fecMil);
            String fecRespaldo = fecTimestamp.toString();
            
            fecRespaldo = fecRespaldo.replace("-", "");
            fecRespaldo = fecRespaldo.replace(":", "");
            fecRespaldo = fecRespaldo.replace(" ", "_");
            
            String fileNameResp = pathResp + "/" + nomArchResp + "." + fecRespaldo;
            
            File file = new File(fullArch);
            File fileResp = new File(fileNameResp);
            
            JAXBContext jaxbContext = JAXBContext.newInstance(Class.forName(nomBean));
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            try {
                    jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
            } catch (PropertyException pex) {
                    jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
            }
            if (file.exists()) {
            	respaldaParametria(file, fileResp);
            }
            
            jaxbMarshaller.marshal(bean, file);
        } catch (Exception e) {
            
            /** INICIO EVENTO - ERROR **/
            ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
            parametrosAdicionales.add(new ParamEvent(logString, e.toString()));
            log.error(e.getMessage(), "GENERA_PARAMETRIA", parametrosAdicionales, e);
            /** FIN EVENTO - ERROR **/

            /** INICIO EVENTO - FIN DE MoDULO **/
            log.endModuleProcess("CONVIERTE_CSV_ENCUESTA", "KO", null, null);
            /** FIN EVENTO - FIN DE MoDULO **/

        }
    }
    
    public static void respaldaParametria(File archOrigen, File archDestino) throws IOException {
        if(!archDestino.exists()) {
            archDestino.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(archOrigen).getChannel();
            destino = new FileOutputStream(archDestino).getChannel();

            long count = 0;
            long tamanio = origen.size();              
            while((count += destino.transferFrom(origen, count, tamanio-count)) < tamanio);
        }
        finally {
            if(origen != null) {
                origen.close();
                origen = null;
            }
            if(destino != null) {
                destino.close();
                destino = null;
            }
        }
    }
	
	
	/**
	 * Convierte de BeanBlackList a XML
	 * 
	 * @param bean
	 *            {@link BeanBlackList}
	 * @throws Exception
	 */
	public void marshallBlackList(BeanBlackList bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanBlackList", bean.toString()));
		this.log.initModuleProcess("MARSHALL_BLACKLIST", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de la blacklist y prefijo para el fichero
		// de blacklist
		String path = util.leeParametroMarshall("PATH_XML");
		String pathBlackList = util.leeParametroMarshall("PATH_XML_BLACKLIST");
		String prefixBlackList = util.leeParametroMarshall("PREFIX_XML_BLACKLIST");
		// nombre del fichero
		String fileName = "" + ".xml";

		// si no existe el path de BlackList lo crea
		File filePath = new File(path + pathBlackList);
		filePath.mkdirs();

		File file = new File(path + pathBlackList + prefixBlackList + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanBlackList.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathBlackList + prefixBlackList + fileName));
		log.endModuleProcess("MARSHALL_BLACKLIST", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanEncuesta a XML
	 * 
	 * @param bean
	 *            {@link BeanEncuesta}
	 * @throws JAXBException
	 */
	public void marshallEncuesta(BeanEncuesta bean) throws JAXBException {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanEncuesta", bean.toString()));
		log.initModuleProcess("MARSHALL_ENCUESTA", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de la encuesta y prefijo para el fichero de
		// encuesta
		String path = util.leeParametroMarshall("PATH_XML");
		String pathEnc = util.leeParametroMarshall("PATH_XML_ENC");
		String prefixEnc = util.leeParametroMarshall("PREFIX_XML_ENC");
		// nombre del fichero
		String fileName = bean.getCodEncuesta() + ".xml";

		// si no existe el path de Encuestas lo crea
		File filePath = new File(path + pathEnc);
		filePath.mkdirs();

		File file = new File(path + pathEnc + prefixEnc + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanEncuesta.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathEnc + prefixEnc + fileName));
		log.endModuleProcess("MARSHALL_ENCUESTA", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanLocSaldo a XML
	 * 
	 * @param bean
	 *            {@link BeanLocSaldo}
	 * @throws Exception
	 */
	public void marshallLocSaldo(BeanLocSaldo bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanLocSaldo", bean.toString()));
		log.initModuleProcess("MARSHALL_LOC_SALDO", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de las locus de saldo y prefijo para el
		// fichero de las locus de saldo
		String path = util.leeParametroMarshall("PATH_XML");
		String pathSLocSaldo = util.leeParametroMarshall("PATH_XML_LOC_SALDOS");
		String prefixLocSaldo = util.leeParametroMarshall("PREFIX_XML_LOC_SALDOS");
		// nombre del fichero
		String fileName = "" + ".xml";

		// si no existe el path de locSaldo lo crea
		File filePath = new File(path + pathSLocSaldo);
		filePath.mkdirs();

		File file = new File(path + pathSLocSaldo + prefixLocSaldo + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanLocSaldo.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathSLocSaldo + prefixLocSaldo + fileName));
		log.endModuleProcess("MARSHALL_LOC_SALDO", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanMenu a XML
	 * 
	 * @param bean
	 *            {@link BeanMenu}
	 * @throws JAXBException
	 */
	public void marshallMenu(String codServicio, BeanMenu bean) throws JAXBException {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanMenu", bean.toString()));
		log.initModuleProcess("MARSHALL_MENU", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica del menu y prefijo para el fichero de menu
		String path = util.leeParametroMarshall("PATH_XML");
		String pathMenu = util.leeParametroMarshall("PATH_XML_MENU");
		String prefixMenu = util.leeParametroMarshall("PREFIX_XML_MENU");
		// nombre del fichero
		// String fileName = bean.getCodMenu() + ".xml";

		String filePath = path + pathMenu;
		if (filePath.contains("<cod_servivr>")) {
			filePath = filePath.replace("<cod_servivr>", codServicio);
		}

		String fileName = filePath + prefixMenu + bean.getCodMenu() + ".xml";

		if (fileName.contains("<cod_servivr>")) {
			fileName = fileName.replace("<cod_servivr>", codServicio);
		}

		// si no existe el path de Servicio lo crea
		File fileCreate = new File(filePath);
		fileCreate.mkdirs();

		tipoPrincipal = util.leeParametroMarshall("TIPO_ROOT_XML_MENU");
		nombreDTD = util.leeParametroMarshall("NOMBRE_DTD_MENU");

		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanMenu.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML + "\n" + cabeceraDTD);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML + "\n" + cabeceraDTD);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathMenu + prefixMenu + fileName));
		log.endModuleProcess("MARSHALL_MENU", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanServicio a XML
	 * 
	 * @param bean
	 *            {@link BeanServicio}
	 * @throws Exception
	 */
	public void marshallServicio(BeanServicio bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanServicio", bean.toString()));
		log.initModuleProcess("MARSHALL_SERVICIO", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica del servicio y prefijo para el fichero de
		// servicio
		String path = util.leeParametroMarshall("PATH_XML");
		String pathServ = util.leeParametroMarshall("PATH_XML_SERV");
		// pathServ = pathServ.replace("<cod_serv>", bean.getCodServIVR());

		String prefixServ = util.leeParametroMarshall("PREFIX_XML_SERV");
		// nombre del fichero

		String filePath = path + pathServ;
		if (filePath.contains("<cod_servivr>")) {
			filePath = filePath.replace("<cod_servivr>", bean.getCodServIVR());
		}

		String fileName = filePath + prefixServ + bean.getCodServIVR() + ".xml";

		if (fileName.contains("<cod_servivr>")) {
			fileName = fileName.replace("<cod_servivr>", bean.getCodServIVR());
		}

		// si no existe el path de Servicio lo crea
		File fileCreate = new File(filePath);
		fileCreate.mkdirs();

		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanServicio.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathServ + prefixServ + fileName));
		log.endModuleProcess("MARSHALL_SERVICIO", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanTransfer a XML
	 * 
	 * @param bean
	 *            {@link BeanTransfer}
	 * @throws Exception
	 */
	public void marshallTransfer(BeanTransfer bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanTransfer", bean.toString()));
		log.initModuleProcess("MARSHALL_TRANSFER", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de transferencias y prefijo para el fichero
		// de transferencia
		String path = util.leeParametroMarshall("PATH_XML");
		String pathTransfer = util.leeParametroMarshall("PATH_XML_TRANSFER");
		String prefixTransfer = util.leeParametroMarshall("PREFIX_XML_TRANSFER");
		// nombre del fichero
		String fileName = bean.getCodTransfer() + ".xml";

		// si no existe el path de Transfer lo crea
		File filePath = new File(path + pathTransfer);
		filePath.mkdirs();

		File file = new File(path + pathTransfer + prefixTransfer + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanTransfer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathTransfer + prefixTransfer + fileName));
		log.endModuleProcess("MARSHALL_TRANSFER", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanTransferSchema a XML
	 * 
	 * @param bean
	 *            {@link BeanTransferSchema}
	 * @throws Exception
	 */
	public void marshallTransferSchema(BeanTransferSchema bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanTransferSchema", bean.toString()));
		log.initModuleProcess("MARSHALL_TRANSFER_SCHEMA", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de transferencias-schema y prefijo para el
		// fichero de transferencia-schema
		String path = util.leeParametroMarshall("PATH_XML");
		String pathTransferSchema = util.leeParametroMarshall("PATH_XML_TRANSFER_SCHEMA");
		String prefixTransferSchema = util.leeParametroMarshall("PREFIX_XML_TRANSFER_SCHEMA");
		// nombre del fichero
		String fileName = "" + ".xml";

		// si no existe el path de Transfe Schema lo crea
		File filePath = new File(path + pathTransferSchema);
		filePath.mkdirs();

		File file = new File(path + pathTransferSchema + prefixTransferSchema + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanTransferSchema.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathTransferSchema + prefixTransferSchema + fileName));
		log.endModuleProcess("MARSHALL_TRANSFER_SCHEMA", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanRespuestas a XML
	 * 
	 * @param bean
	 *            {@link BeanRespuestasPreguntas}
	 * @throws Exception
	 */
	public void marshallRespuestas(BeanRespuestasPreguntas bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanRespuestasPreguntas", bean.toString()));
		log.initModuleProcess("MARSHALL_RESPUESTAS_PREG", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de transferencias-schema y prefijo para el
		// fichero de transferencia-schema
		String path = util.leeParametroMarshall("PATH_XML");
		String pathRespuestas = util.leeParametroMarshall("PATH_XML_RESPUESTAS");
		String prefixRespuestas = util.leeParametroMarshall("PREFIX_XML_RESPUESTAS");
		// nombre del fichero
		String fileName = "" + ".xml";

		// si no existe el path de Transfe Schema lo crea
		File filePath = new File(path + pathRespuestas);
		filePath.mkdirs();

		File file = new File(path + pathRespuestas + prefixRespuestas + fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanRespuestasPreguntas.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", path + pathRespuestas + prefixRespuestas + fileName));
		log.endModuleProcess("MARSHALL_RESPUESTAS_PREG", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

	}

	/**
	 * Convierte de BeanStatLlamada a XML
	 * 
	 * @param bean
	 *            {@link BeanStatLlamada}
	 * @throws Exception
	 */
	public File marshallStatLlamada(BeanStatLlamada bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanStatLlamada", bean.toString()));
		log.initModuleProcess("MARSHALL_STAT_LLAMADA", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de transferencias-schema y prefijo para el
		// fichero de transferencia-schema
		String path = util.leeParametroMarshall("PATH_XML");
		String pathStatLlam = util.leeParametroMarshall("PATH_XML_STAT_LLAMADA");
		String prefixStatLlam = util.leeParametroMarshall("PREFIX_XML_STAT_LLAMADA");

		String filePath = path + pathStatLlam;
		if (filePath.contains("<cod_servivr>")) {
			filePath = filePath.replace("<cod_servivr>", bean.getCodServIVR().toUpperCase());
		}

		// ABALFARO_20170615 cambio en nomenclatura del fichero
		// PREFIX_XML_STAT_LLAMADA=MMGCE_D02_<YYYYMMDD>_<result>
		if (prefixStatLlam.contains("<YYYYMMDD>")) {
			Date hoy = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String hoyFormateado = sdf.format(hoy);
			prefixStatLlam = prefixStatLlam.replace("<YYYYMMDD>", hoyFormateado);
		}
		if (prefixStatLlam.contains("<result>")) {
			String resultOK = util.leeParametroMarshall("RESULT_OK_XML_STAT_LLAMADA");
			prefixStatLlam = prefixStatLlam.replace("<result>", resultOK);
		}

		// si no existe el path de stats lo crea
		File filePathStat = new File(filePath);
		filePathStat.mkdirs();

		String fileName = filePath + prefixStatLlam + bean.getCodLlamIVR() + ".xml";

		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanStatLlamada.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", fileName));
		log.endModuleProcess("MARSHALL_STAT_LLAMADA", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

		return file;

	}

	/**
	 * Convierte de BeanStatContador a XML
	 * 
	 * @param bean
	 *            {@link BeanStatContador}
	 * @throws Exception
	 */
	public File marshallStatContador(String idLlamada, String idServicio, BeanStatContador bean) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanStatContador", bean.toString()));
		log.initModuleProcess("MARSHALL_STAT_CONTADOR", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica de transferencias-schema y prefijo para el
		// fichero de transferencia-schema
		String path = util.leeParametroMarshall("PATH_XML");
		String pathStatCont = util.leeParametroMarshall("PATH_XML_STAT_CONTADOR");
		String prefixStatCont = util.leeParametroMarshall("PREFIX_XML_STAT_CONTADOR");

		String filePath = path + pathStatCont;
		if (filePath.contains("<cod_servivr>")) {
			filePath = filePath.replace("<cod_servivr>", idServicio.toUpperCase());
		}
		// ABALFARO_20170615 cambio en nomenclatura del fichero
		// PREFIX_XML_STAT_CONTADOR=MMGCE_D02_<YYYYMMDD>_<result>
		if (prefixStatCont.contains("<YYYYMMDD>")) {
			Date hoy = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String hoyFormateado = sdf.format(hoy);
			prefixStatCont = prefixStatCont.replace("<YYYYMMDD>", hoyFormateado);
		}
		if (prefixStatCont.contains("<result>")) {
			String resultOK = util.leeParametroMarshall("RESULT_OK_XML_STAT_CONTADOR");
			prefixStatCont = prefixStatCont.replace("<result>", resultOK);
		}				
				
		// si no existe el path de stats lo crea
		File filePathStat = new File(filePath);
		filePathStat.mkdirs();

		String fileName = filePath + prefixStatCont + idLlamada + ".xml";

		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanStatContador.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("fichero", fileName));
		log.endModuleProcess("MARSHALL_STAT_CONTADOR", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN DE MoDULO **/

		return file;

	}

	/**
	 * Convierte de BeanServicio a XML
	 * 
	 * @param bean
	 *            - POJO que contiene los datos a guardar en XML
	 * @param fullArch
	 *            - ruta completa + nombre de archivo a respaldar y crear
	 * @param dirResp
	 *            - Directorio donde se respaldan los archivos
	 * @param nomArchResp
	 *            - nombre de archivo individual de respaldo
	 * @param nomBean
	 *            - Nombre del bean a instanciar en el mtodo internamente
	 * @throws Exception
	 */
	public void marshall(Object bean, String fullArch, String dirResp, String nomArchResp, String nomBean) throws Exception {

		try {

			// si no existe el path de Respaldo lo crea
			File pathResp = new File(dirResp);
			pathResp.mkdirs();

			// Para respaldo de archivos
			Date utilDate = new Date();
			long fecMil = utilDate.getTime();
			Timestamp fecTimestamp = new Timestamp(fecMil);
			String fecRespaldo = fecTimestamp.toString();

			fecRespaldo = fecRespaldo.replace("-", "");
			fecRespaldo = fecRespaldo.replace(":", "");
			fecRespaldo = fecRespaldo.replace(" ", "_");

			String fileNameResp = pathResp + "/" + nomArchResp + "." + fecRespaldo;

			File file = new File(fullArch);
			File fileResp = new File(fileNameResp);

			JAXBContext jaxbContext = JAXBContext.newInstance(Class.forName(nomBean));
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			try {
				jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
				jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
			} catch (PropertyException pex) {
				jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
			}

			UtilidadesBeans.respaldarArchivo(file, fileResp);

			jaxbMarshaller.marshal(bean, file);
		} catch (Exception e) {
		}
	}
	 /* Convierte de BeanShorcuts a XML
	 * 
	 * @param bean
	 *            {@link BeanCCMX}
	 * @throws Exception
	 */
	public void marshallShortCuts(BeanShortcuts bean) throws Exception {
		//BeanShortcuts bean =new BeanShortcuts();
		
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanServicio", bean.toString()));
		//log.initModuleProcess("MARSHALL_CCMX", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica del servicio y prefijo para el fichero de
//		 ruta comun, ruta generica de transferencias-schema y prefijo para el
//		 fichero de beanCCXML
//		String path = util.leeParametroMarshall("PATH_XML");
//		String pathStatCont = util.leeParametroMarshall("PATH_XML_STAT_CONTADOR");
//		String prefixStatCont = util.leeParametroMarshall("PREFIX_XML_STAT_CONTADOR");
//
//		String filePath = path + pathStatCont;
//		if (filePath.contains("<cod_servivr>")) {
//			filePath = filePath.replace("<cod_servivr>", bean.getCod_llamivr().toUpperCase());
//		}
		String filePath = "W:/Desarrollo";
		// si no existe el path de Servicio lo crea
		File fileCreate = new File(filePath);
		fileCreate.mkdirs();
		String fileName = filePath + "/filePRUEBA"+".xml";
		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanShortcuts.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
//		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//		parametrosSalida.add(new ParamEvent("fichero", path + pathServ + prefixServ + fileName));
//		log.endModuleProcess("MARSHALL_BeanCCMX", "OK", parametrosSalida, null);
//		/** FIN EVENTO - FIN DE MoDULO **/

	}
	 /* Convierte de BeanShorcuts a XML
	 * 
	 * @param bean
	 *            {@link BeanCCMX}
	 * @throws Exception
	 */
	public void marshallBEANMAU(BeanRespuestasMau bean) throws Exception {
		//BeanShortcuts bean =new BeanShortcuts();
		
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanServicio", bean.toString()));
		//log.initModuleProcess("MARSHALL_CCMX", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		// ruta comun, ruta generica del servicio y prefijo para el fichero de
//		 ruta comun, ruta generica de transferencias-schema y prefijo para el
//		 fichero de beanCCXML
//		String path = util.leeParametroMarshall("PATH_XML");
//		String pathStatCont = util.leeParametroMarshall("PATH_XML_STAT_CONTADOR");
//		String prefixStatCont = util.leeParametroMarshall("PREFIX_XML_STAT_CONTADOR");
//
//		String filePath = path + pathStatCont;
//		if (filePath.contains("<cod_servivr>")) {
//			filePath = filePath.replace("<cod_servivr>", bean.getCod_llamivr().toUpperCase());
//		}
		String filePath = "W:/Desarrollo";
		// si no existe el path de Servicio lo crea
		File fileCreate = new File(filePath);
		fileCreate.mkdirs();
		String fileName = filePath + "/filePRUEBA"+".xml";
		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(BeanRespuestasMau.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
//		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//		parametrosSalida.add(new ParamEvent("fichero", path + pathServ + prefixServ + fileName));
//		log.endModuleProcess("MARSHALL_BeanCCMX", "OK", parametrosSalida, null);
//		/** FIN EVENTO - FIN DE MoDULO **/

	}
	/* Convierte de Datos LoggerLlamada a XML
	 * 
	 * @param bean
	 *            {@link BeanCCMX}
	 * @throws Exception
	 */
	public void marshallDatosLoggerLamada(DatosLogerLlamada bean,String codigoser) throws Exception {
		//BeanShortcuts bean =new BeanShortcuts();
		
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("BeanServicio", bean.toString()));
		//log.initModuleProcess("MARSHALL_CCMX", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);

		String path = util.leeParametroMarshall("PATH_XML");
		String pathStatCont = util.leeParametroMarshall("PATH_XML_STAT_CONTADOR");
		String prefixStatCont = util.leeParametroMarshall("PATH_XML_LOGLLA_CONTADOR_RESP");

		String filePath = path + prefixStatCont;
		if (filePath.contains("<cod_servivr>")) {
			filePath = filePath.replace("<cod_servivr>", codigoser);
		}
		// si no existe el path de Servicio lo crea
		File fileCreate = new File(filePath);
		fileCreate.mkdirs();
		String fileName = filePath+bean.getIdLlamada()+".xml";
		File file = new File(fileName);

//		String filePath = "W:/Desarrollo";
//		// si no existe el path de Servicio lo crea
//		File fileCreate = new File(filePath);
//		fileCreate.mkdirs();
//		String fileName = filePath + "/filePRUEBA"+".xml";
//		File file = new File(fileName);

		JAXBContext jaxbContext = JAXBContext.newInstance(DatosLogerLlamada.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		// ** Quitar standalone="yes" de la cabeceraXML
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			jaxbMarshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders", cabeceraXML);
		} catch (PropertyException pex) {
			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", cabeceraXML);
		}

		jaxbMarshaller.marshal(bean, file);

		/** INICIO EVENTO - FIN DE MoDULO **/
//		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//		parametrosSalida.add(new ParamEvent("fichero", path + pathServ + prefixServ + fileName));
//		log.endModuleProcess("MARSHALL_BeanCCMX", "OK", parametrosSalida, null);
//		/** FIN EVENTO - FIN DE MoDULO **/

	}
	
}
