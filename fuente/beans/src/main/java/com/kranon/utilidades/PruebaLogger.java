package com.kranon.utilidades;

import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;

public class PruebaLogger {
	
	public static void main(String[] args) {
		String idElemento = "PRUEBA_LOGGER";
		
		/*********************************************/
		/*********** LOGGER PROCESO JAVA *************/
		/*********************************************/
		
		/** INICIALIZO LOGGER **/
		// el parametro es nombre del fichero de log donde queremos que escriba (se corresponde con los declarados en el log4j.properties)
		CommonLoggerProcess log = new CommonLoggerProcess("PARAM_SERV"); 
		// al inicializar, si pasamos el primer argumento a null se genera solo el idInvocacion (con fecha/dia...)
		log.inicializar(null, idElemento);
		
//		log.initProcess(idProceso, rutaEjecucion, parametrosEntrada, parametrosAdicionales);
//		log.endProcess(idProceso, codigoRetorno, parametrosSalida, parametrosAdicionales);
		
//		log.initModuleProcess(idModulo, parametrosEntrada);
//		log.endModuleProcess(idModulo, codigoRetorno, parametrosSalida, parametrosAdicionales);
		
//		log.actionEvent(idAccion, resultado, parametrosAdicionales);
		
//		log.sistExternEvent(idSistemaExterno, rutaSistema, parametrosEntrada, parametrosSalida, codigoRetorno, parametrosAdicionales);
		
//		log.comment(texto);
		
//		log.error(idError, localizacion, parametrosAdicionales);
		
		
		
		
		/*********************************************/
		/*********** LOGGER SERVICIO IVR *************/
		/*********************************************/
		
		String idLlamada = "call_id_de_la_llamada";
		String idServicio = "SERVICIO_PRUEBA_LOGGER";
		
		/** INICIALIZO LOGGER **/
		// el parametro es nombre del fichero de log donde queremos que escriba (se corresponde con los declarados en el log4j.properties)
		CommonLoggerService logService = new CommonLoggerService("IVR_SERVICE");
		// al inicializar, pasamos el id de la llamada que es el CallID
		logService.inicializar(idLlamada, idServicio, idElemento);
		
//		logService.initCall(numOrigen, numDestino, parametrosAdicionales);
//		logService.endCall(numOrigen, numDestino, codigoRetorno, duracion, parametrosAdicionales);
		
//		logService.initModuleService(parametrosEntrada);
//		logService.endModuleService(codigoRetorno, parametrosSalida, parametrosAdicionales);
		
//		logService.dialogueE hvent(idMenu, recDisponible, recUtilizado, numIntento, respuesta, codigoRetorno, parametrosAdicionales);
		
//		logService.speechEvent(tipoLocucion, locucion, parametrosAdicionales);
		
//		logService.sistExternEvent(idSistemaExterno, rutaSistema, parametrosEntrada, parametrosSalida, codigoRetorno, parametrosAdicionales);
		
//		logService.statisticEvent(idEstadistica, resultado, parametrosAdicionales);
		
//		logService.actionEvent(idAccion, resultado, parametrosAdicionales);
		
//		logService.comment(texto);
		
//		logService.error(idError, localizacion, parametrosAdicionales);
		
		
		/****************************************************/
		/** Para los PARAMETROS ENTRADA/SALIDA/ADICIONALES **/
		/****************************************************/
		
		// EJEMPLO
//		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
//		parametrosEntrada.add(new ParamEvent("BeanBlackList", bean.toString()));
		
	}
	

}
