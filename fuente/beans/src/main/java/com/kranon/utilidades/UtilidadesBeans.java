package com.kranon.utilidades;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;

public class UtilidadesBeans {

	public CommonLoggerProcess log;

	public UtilidadesBeans(CommonLoggerProcess log) {
		this.log = log;
	}

	/**
	 * Asterisca los (longitud-salvarUltimos) caracteres iniciales de la cadena enviada
	 * 
	 * @param cadena
	 * @param salvarUltimos
	 * @return
	 */
	public static String cifrar(String cadena, int salvarUltimos) {
		if (cadena == null) {
			return null;
		}
		String cadenaCifrada = cadena;

		if (salvarUltimos > cadena.length()) {
			// nos piden salvar mayor cantidad que la que la cadena contiene
			// no asteriscamos nada
			return cadena;
		}
		cadenaCifrada = StringUtils.repeat("*", cadena.length() - salvarUltimos);
		cadenaCifrada = cadenaCifrada + cadena.subSequence(cadena.length() - salvarUltimos, cadena.length());

		return cadenaCifrada;
	}

	/**
	 * Nos devuelve solo los primeros N caracteres
	 * 
	 * @param cadena
	 *            tsec original
	 * @param numCaracteres
	 *            cantidad de caracteres que queremos mostrar
	 * @return
	 */
	public static String cifrarTsec(String cadena, int numCaracteres) {
		if (cadena == null) {
			return "null";
		}

		if (numCaracteres > cadena.length()) {
			// nos piden mostrar mayor cantidad que la que la cadena contiene
			// la devolvemos entera
			return cadena;
		}

		return cadena.substring(0, numCaracteres);
	}

	/**
	 * Sustituye los caracteres especiales del JSON
	 * 
	 * @param cadena
	 *            json a analizar
	 * @return
	 */
	public static String[] formateaJson(String cadena) {
		String resultado[] = new String[2];
		boolean modificado = false;
		if (cadena != null) {
			if (cadena.contains("&")) {
				// & -> &amp;
				cadena = cadena.replaceAll("&", "&amp;");
				modificado = true;
			}
			if (cadena.contains("'")) {
				// comillas simples -> vacio
				cadena = cadena.replaceAll("'", "");
				modificado = true;
			}
			if (cadena.contains("\\\\\"")) {
				// comillas dobles ESCAPADAS -> vacio
				// las comillas dobles no las puedo reemplazar directamente ya que el json viene con pares "clave":"valor"
				cadena = cadena.replaceAll("\\\\\"", "");
				modificado = true;
			}
		}
		resultado[0] = modificado + "";
		resultado[1] = cadena;
		return resultado;
	}

	public static boolean checkJsonCompatibility(String jsonStr) {

		ObjectMapper mapper = new ObjectMapper();

		try {

			mapper.readValue(jsonStr, JsonNode.class);

			return true;
		} catch (Exception e) {
			// JsonMappingException, JsonParseException, IOException
			return false;
		}

	}

	
	/**
	 * Lee el dato de configuracion del fichero FILE_CFG.cfg, con clave pasada
	 * como parametro, para el modulo de Marshall
	 * 
	 * @param {@link String} key
	 * @return {@link String}
	 */
	public String leeParametroMarshall(String key) {

		String value = "";

		try {

			/** ESPANIA **/
//			Properties props = new Properties();
//			InputStream inputStream = getClass().getResourceAsStream(Constantes.FILE_CFG);
//			props.load(inputStream);	
//			value = props.getProperty(key);
//			inputStream.close();
			
			/** MEXICO **/
			value = ArqSpringContext.getPropiedad(key);
			
			return value;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			this.log.error(e.getMessage(), "LEE_PARAMETRO_CFG", null, e);
			/** FIN EVENTO - ERROR **/

			return null;
		}
	}

	/**
	 * Lee el dato de configuracion del fichero FILE_CFG.cfg, con clave pasada
	 * como parametro, para el modulo de Unmarshall
	 * 
	 * @param {@link String} key
	 * @return {@link String}
	 */
	public String leeParametroUnmarshall(String key) {

		String value = "";

		try {
			/** ESPANIA **/
//			Properties props = new Properties();
//			InputStream inputStream = getClass().getResourceAsStream(Constantes.FILE_CFG);
//			props.load(inputStream);	
//			value = props.getProperty(key);
//			inputStream.close();
			
			/** MEXICO **/
			value = ArqSpringContext.getPropiedad(key);

			return value;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			this.log.error(e.getMessage(), "LEE_PARAMETRO_CFG", null, e);
			/** FIN EVENTO - ERROR **/

			return null;
		}
	}
	
    public static void respaldarArchivo(File archOrigen, File archDestino) throws IOException {
        if(!archDestino.exists()) {
            archDestino.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(archOrigen).getChannel();
            destino = new FileOutputStream(archDestino).getChannel();

            long count = 0;
            long size = origen.size();              
            while((count += destino.transferFrom(origen, count, size-count))< size);
        }
        catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			e.printStackTrace();
			/** FIN EVENTO - ERROR **/
			
		}
        finally {
            if(origen != null) {
                origen.close();
            }
            if(destino != null) {
                destino.close();
            }
        }
    }	

	/**
	 * Obtiene lista de arreglos de menus desde el archivo CSV de MENUS
	 * 
	 * @return {@link List<String[]>}
	 */
	
	
	
	public List<String[]> obtieneArreglosMenus() {

		UtilidadesBeans util = new UtilidadesBeans(log);

		String path = util.leeParametroUnmarshall("PATH_CSV");
		// nombre del archivo
		String fileMenuDinam = path + util.leeParametroUnmarshall("CSV_MENUDINAM");
		
		BufferedReader br = null;
		String linea = "";
		String splitBy = util.leeParametroUnmarshall("SEPARADOR_CSV");
		String compara ="";
		String temp = "";
		
		List<String> listaMenus = new ArrayList<String>();
		List<String[]> listaArreglosMenu = new ArrayList<String[]>();
		String[] arregloMenus = null;
		String[] arregloNombreMenus = null;

		try {
			
			br = new BufferedReader( new InputStreamReader(new FileInputStream(fileMenuDinam), "UTF-8"));
				
			while ((linea = br.readLine()) != null) {
				String[] values = linea.split(splitBy);
				listaMenus.add(linea);
				if(!(compara.equalsIgnoreCase(values[0]))&&(!(temp.contains(values[0])))){
					compara = values[0];
					temp=temp+","+compara;
				}
									
			}
			temp=temp.substring(temp.indexOf(",")+1);	
			arregloNombreMenus=temp.split(",");
			
			arregloMenus = new String[listaMenus.size()];
			arregloMenus = listaMenus.toArray(arregloMenus);
			
			listaArreglosMenu.add(arregloMenus);
			listaArreglosMenu.add(arregloNombreMenus);
			

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			this.log.error(e.getMessage(), "OBTIENE_ARREGLOS_MENU", null, e);
			/** FIN EVENTO - ERROR **/			
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
		return listaArreglosMenu;
	}
	
	
	/**
	 * Obtiene lista de arreglos de servicios desde el archivo CSV de SERVICIOS
	 * 
	 * @return {@link List<String[]>}
	 */
	
	
	
	public List<String[]> obtieneArreglosServicio() {

		UtilidadesBeans util = new UtilidadesBeans(log);

		String path = util.leeParametroUnmarshall("PATH_CSV");
		// nombre del archivo
		String fileParam = path + util.leeParametroUnmarshall("CSV_SERVICIO");
		
		BufferedReader br = null;
		String linea = "";
		String splitBy = util.leeParametroUnmarshall("SEPARADOR_CSV");
		String compara ="";
		String temp = "";
		
		List<String> listaServicios = new ArrayList<String>();
		List<String[]> listaArreglosServicio = new ArrayList<String[]>();
		String[] arregloServicios = null;
		String[] arregloNombreServicios = null;

		try {
			
			br = new BufferedReader( new InputStreamReader(new FileInputStream(fileParam), "UTF-8"));
				
			while ((linea = br.readLine()) != null) {
				String[] values = linea.split(splitBy);
				listaServicios.add(linea);
				if(!(compara.equalsIgnoreCase(values[0]))&&(!(temp.contains(values[0])))){
					compara = values[0];
					temp=temp+","+compara;
				}
				
									
			}
			temp=temp.substring(temp.indexOf(",")+1);
			arregloNombreServicios=temp.split(",");
			
			arregloServicios = new String[listaServicios.size()];
			arregloServicios = listaServicios.toArray(arregloServicios);
			
			listaArreglosServicio.add(arregloServicios);
			listaArreglosServicio.add(arregloNombreServicios);
			

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			this.log.error(e.getMessage(), "OBTIENE_ARREGLOS_SERVICIO", null, e);
			/** FIN EVENTO - ERROR **/			
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
		return listaArreglosServicio;
	}
	
	
	/**
	 * Obtiene el arreglo de los archivos CSV Comunes
	 * 
	 * @return {@link String[]}
	 */
	
	public String[] cargaArregloCSV(String fileCSV){
		
		BufferedReader br = null;
		String linea = "";
		
		
		List<String> listaCSV = new ArrayList<String>();
		String[] arregloCSV = null;

		try {
			
			
			br = new BufferedReader( new InputStreamReader(new FileInputStream(fileCSV), "UTF-8"));
				
			while ((linea = br.readLine()) != null) {
				listaCSV.add(linea);
										
			}
			
			arregloCSV = new String[listaCSV.size()];
			arregloCSV = listaCSV.toArray(arregloCSV);
			
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			this.log.error(e.getMessage(), "CARGA_ARREGLO_CSV", null, e);
			/** FIN EVENTO - ERROR **/			
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
					
		return arregloCSV;
	}
}
