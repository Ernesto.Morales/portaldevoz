package com.kranon.loccomunes;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.menu.BeanPromptGenerico;
import com.kranon.modulo.BeanLocucionModulo;

/**
 * Bean para la configuracion de Locuciones Comunes de un Servicio
 * 
 */
@XmlRootElement(name = "locuciones")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanLocucionesComunes {

	/* Codigo del Servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServIVR;


	/* Locuciones Comunes del Servicio */
	@XmlElement(name = "locucion")
	private BeanLocucionModulo[] locuciones;

	

	public String getCodServIVR() {
		return codServIVR;
	}

	public void setCodServIVR(String codServIVR) {
		this.codServIVR = codServIVR;
	}

	public BeanLocucionModulo[] getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocucionModulo[] locuciones) {
		this.locuciones = locuciones;
	}

	@Override
	public String toString() {
		return "BeanLocucionesComunes [codServIVR=" + codServIVR + ", locuciones=" + Arrays.toString(locuciones) + "]";
	}
	
	//***
	
	/**
	 * Devuelve todas las partes de una locucion con ese nombre
	 * 
	 * @param nombre
	 * @return {@link BeanPromptGenerico[]}
	 */
	public BeanPromptGenerico[] getLocucionesByNombre(String nombre){
		if(this.locuciones == null || this.locuciones.length == 0){
			// no hay locuciones para buscar
			return null;
		}
		
		for(BeanLocucionModulo loc: this.locuciones){
			if(loc.getNombre().equalsIgnoreCase(nombre)){
				// es la locucion que estamos buscando
				// ordeno todos sus prompt por id por si acaso
				BeanPromptGenerico[] promptOrdenados = this.ordenarPromptsPorId(loc.getPartesLoc());
				return promptOrdenados;
			}
		}
		// no he encontrado esa locucion
		return null;
	}
	
	/**
	 * Ordenar los prompt de un intento por id de menor a mayor
	 * 
	 * @param {@link BeanPromptGenerico[]} prompts
	 * @return {@link BeanPromptGenerico[]}
	 * @throws Exception
	 */
	private BeanPromptGenerico[] ordenarPromptsPorId(BeanPromptGenerico[] prompts) {

		BeanPromptGenerico[] promptsOrdenados; 
		
		try {
			promptsOrdenados = prompts;

			for (int i = 0; i < (promptsOrdenados.length - 1); i++) {
				for (int j = i + 1; j < promptsOrdenados.length; j++) {
					int posicionI = Integer.parseInt(promptsOrdenados[i].getId());
					int posicionJ = Integer.parseInt(promptsOrdenados[j].getId());
					if (posicionI > posicionJ) { 
						BeanPromptGenerico variableauxiliar = promptsOrdenados[i];
						promptsOrdenados[i] = promptsOrdenados[j];
						promptsOrdenados[j] = variableauxiliar;

					}
				}
			}
		} catch (Exception e) {
			return null;
		}

		return promptsOrdenados;
	}
	

}
