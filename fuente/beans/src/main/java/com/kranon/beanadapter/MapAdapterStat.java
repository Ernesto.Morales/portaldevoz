package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.kranon.bean.gestioncm.BeanEstadisticaCM;


public class MapAdapterStat extends XmlAdapter<MapAdapterStat.AdaptedBeanEstadisticaCM, Map<String, BeanEstadisticaCM>> {

	
	public static class AdaptedBeanEstadisticaCM {
		@XmlElement(name = "estadistica")
		public List<BeanEstadisticaCM> entry = new ArrayList<BeanEstadisticaCM>();
	}

	@Override
	public Map<String, BeanEstadisticaCM> unmarshal(AdaptedBeanEstadisticaCM adaptedBeanEstadisticaCM) throws Exception {
		if (null == adaptedBeanEstadisticaCM) {
			return null;
		}
		Map<String, BeanEstadisticaCM> map = new HashMap<String, BeanEstadisticaCM>(adaptedBeanEstadisticaCM.entry.size());

		for (BeanEstadisticaCM estadistica : adaptedBeanEstadisticaCM.entry) {
			map.put(estadistica.getNombre(), estadistica);
		}
		return map;
	}

	@Override
	public AdaptedBeanEstadisticaCM marshal(Map<String, BeanEstadisticaCM> map) throws Exception {

		AdaptedBeanEstadisticaCM wrapper = new AdaptedBeanEstadisticaCM();
		List<BeanEstadisticaCM> elements = new ArrayList<BeanEstadisticaCM>();
		if (map != null) {
			Iterator<Entry<String, BeanEstadisticaCM>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, BeanEstadisticaCM> e = (Map.Entry<String, BeanEstadisticaCM>) it.next();
				BeanEstadisticaCM infoOperativa = (BeanEstadisticaCM) e.getValue();
				elements.add(infoOperativa);

			}
		}
		wrapper.entry = elements;
		return wrapper;

	}
}