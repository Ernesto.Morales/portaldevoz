package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.kranon.bean.gestoperativas.BeanInfoOperativa;
import com.kranon.bean.gestoperativas.BeanMetadato;

public class MapAdapterMetadatos extends XmlAdapter<MapAdapterMetadatos.AdaptedBeanMetadatos, Map<BeanMetadato, BeanInfoOperativa>> {

	public static class AdaptedBeanMetadatos {
		@XmlElement(name = "metadato")
		public List<BeanInfoOperativa> entry = new ArrayList<BeanInfoOperativa>();
	}

	@Override
	public Map<BeanMetadato, BeanInfoOperativa> unmarshal(AdaptedBeanMetadatos AdaptedBeanMetadatos) throws Exception {
		if (null == AdaptedBeanMetadatos) {
			return null;
		}
		Map<BeanMetadato, BeanInfoOperativa> map = new HashMap<BeanMetadato, BeanInfoOperativa>(AdaptedBeanMetadatos.entry.size());

		for (BeanInfoOperativa infoMetadato : AdaptedBeanMetadatos.entry) {
			BeanMetadato datosClave = new BeanMetadato();
			datosClave.setAction(infoMetadato.getAction());
			datosClave.setBalance(infoMetadato.getBalance());
			datosClave.setData(infoMetadato.getData());
			datosClave.setFolio(infoMetadato.getFolio());
			datosClave.setInsurance(infoMetadato.getInsurance());
			datosClave.setType(infoMetadato.getType());
			map.put(datosClave, infoMetadato);
		}
		return map;
	}

	@Override
	public AdaptedBeanMetadatos marshal(Map<BeanMetadato, BeanInfoOperativa> map) throws Exception {

		AdaptedBeanMetadatos wrapper = new AdaptedBeanMetadatos();
		List<BeanInfoOperativa> elements = new ArrayList<BeanInfoOperativa>();
		if (map != null) {
			Iterator<Entry<BeanMetadato, BeanInfoOperativa>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<BeanMetadato, BeanInfoOperativa> e = (Map.Entry<BeanMetadato, BeanInfoOperativa>) it.next();
				BeanInfoOperativa infoMetadato = (BeanInfoOperativa) e.getValue();
				elements.add(infoMetadato);

			}
		}
		wrapper.entry = elements;
		return wrapper;

	}
}