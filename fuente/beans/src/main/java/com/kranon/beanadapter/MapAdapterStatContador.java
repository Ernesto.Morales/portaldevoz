package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.kranon.bean.gestioncm.BeanInfoContadorCM;


public class MapAdapterStatContador extends XmlAdapter<MapAdapterStatContador.AdaptedBeanInfoContadorCM, Map<BeanInfoContadorCM, String>> {

	
	public static class AdaptedBeanInfoContadorCM {
		@XmlElement(name = "contador")
		public List<BeanInfoContadorCM> entry = new ArrayList<BeanInfoContadorCM>();
	}

	@Override
	public Map<BeanInfoContadorCM, String> unmarshal(AdaptedBeanInfoContadorCM adaptedBeanInfoContadorCM) throws Exception {
		if (null == adaptedBeanInfoContadorCM) {
			return null;
		}
		Map<BeanInfoContadorCM, String> map = new HashMap<BeanInfoContadorCM, String>(adaptedBeanInfoContadorCM.entry.size());

		for (BeanInfoContadorCM estadistica : adaptedBeanInfoContadorCM.entry) {
			// recorro todos los datos y los que recupere a null los modifico a vaco
			estadistica.setType(estadistica.getType() == null? "": estadistica.getType());
			estadistica.setCodigoRetorno(estadistica.getCodigoRetorno() == null? "": estadistica.getCodigoRetorno());
			estadistica.setParam1(estadistica.getParam1() == null? "": estadistica.getParam1());
			estadistica.setParam2(estadistica.getParam2() == null? "": estadistica.getParam2());
			estadistica.setParam3(estadistica.getParam3() == null? "": estadistica.getParam3());
			estadistica.setParam4(estadistica.getParam4() == null? "": estadistica.getParam4());
			estadistica.setParam5(estadistica.getParam5() == null? "": estadistica.getParam5());
			
			map.put(estadistica, estadistica.getValor());
		}
		return map;
	}

	@Override
	public AdaptedBeanInfoContadorCM marshal(Map<BeanInfoContadorCM, String> map) throws Exception {

		AdaptedBeanInfoContadorCM wrapper = new AdaptedBeanInfoContadorCM();
		List<BeanInfoContadorCM> elements = new ArrayList<BeanInfoContadorCM>();
		if (map != null) {
			Iterator<Entry<BeanInfoContadorCM, String>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<BeanInfoContadorCM, String> e = (Map.Entry<BeanInfoContadorCM, String>) it.next();
				BeanInfoContadorCM infoContador = (BeanInfoContadorCM) e.getKey();
				infoContador.setValor(e.getValue());
				elements.add(infoContador);

			}
		}
		wrapper.entry = elements;
		return wrapper;

	}
}