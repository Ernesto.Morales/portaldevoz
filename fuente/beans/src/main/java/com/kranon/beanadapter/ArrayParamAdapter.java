package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
//import org.apache.xerces.dom.ElementNSImpl;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import com.kranon.bean.BeanParam;



/**
 * Adaptador para BeanParam[] Convierte sus atributos key y value en la
 * etiqueta: <key>value</key>
 * 
 */
public final class ArrayParamAdapter extends

XmlAdapter<ParamWrapper, BeanParam[]> {

	@Override
	public ParamWrapper marshal(BeanParam[] parametros) throws Exception {
		ParamWrapper wrapper = new ParamWrapper();
		List<JAXBElement<String>> elements = new ArrayList<JAXBElement<String>>();
		if (parametros != null) {
			for (BeanParam p : parametros) {
				elements.add(new JAXBElement<String>(new QName(p.getKey()),
						String.class, p.getValue()));
			}
		}
		wrapper.properties = elements;
		return wrapper;
	}

	@Override
	public BeanParam[] unmarshal(ParamWrapper wrapper) throws Exception {

		BeanParam[] parametros = new BeanParam[wrapper.properties.size()];
		try {

			int i = 0;
			Iterator<JAXBElement<String>> iter = wrapper.properties.iterator();
			while (iter.hasNext()) {

				Object obj = (Object) iter.next();

				// System.out.println(obj.toString());

				ElementNSImpl elem = (ElementNSImpl) obj;
				// System.out.println(i + ": " + elem.getNodeName() + " - " +
				// elem.getFirstChild().getTextContent());

				BeanParam p = new BeanParam();
				p.setKey(elem.getNodeName());
				if(elem.getFirstChild() != null){
					p.setValue(elem.getFirstChild().getTextContent());
				} else {
					p.setValue(null);
				}
				parametros[i] = p;
				i = i + 1;

			}

		} catch (Exception e) {
//			System.out.println("Error en adapter: " + e.getMessage());

		}
		return parametros;
	}
}
