package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAnyElement;

public class ParamWrapper {

	@XmlAnyElement
	public List<JAXBElement<String>> properties = new ArrayList<JAXBElement<String>>();

	@Override
	public String toString() {
		return "MapWrapper [properties=" + properties + "]";
	}
	
	      
}