package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.kranon.bean.excepserv.BeanExcepcion;

public class MapAdapter extends XmlAdapter<MapAdapter.AdaptedBeanExcepcion, Map<String, BeanExcepcion>> {

	public static class AdaptedBeanExcepcion {
		@XmlElement(name = "excepcion")
		public List<BeanExcepcion> entry = new ArrayList<BeanExcepcion>();
	}

	@Override
	public Map<String, BeanExcepcion> unmarshal(AdaptedBeanExcepcion adaptedBeanExcepcion) throws Exception {
		if (null == adaptedBeanExcepcion) {
			return null;
		}
		Map<String, BeanExcepcion> map = new HashMap<String, BeanExcepcion>(adaptedBeanExcepcion.entry.size());

		for (BeanExcepcion excep : adaptedBeanExcepcion.entry) {
			map.put(excep.getNombre(), excep);
		}
		return map;
	}

	@Override
	public AdaptedBeanExcepcion marshal(Map<String, BeanExcepcion> map) throws Exception {

		AdaptedBeanExcepcion wrapper = new AdaptedBeanExcepcion();
		List<BeanExcepcion> elements = new ArrayList<BeanExcepcion>();
		if (map != null) {
			Iterator<Entry<String, BeanExcepcion>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, BeanExcepcion> e = (Map.Entry<String, BeanExcepcion>) it.next();
				BeanExcepcion excep = (BeanExcepcion) e.getValue();
				elements.add(excep);

			}
		}
		wrapper.entry = elements;
		return wrapper;

	}
}