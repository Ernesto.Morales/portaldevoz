package com.kranon.beanadapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.kranon.bean.gestoperativas.BeanOperativa;

public class MapAdapterOperativa extends XmlAdapter<MapAdapterOperativa.AdaptedBeanOperativa, Map<String, BeanOperativa>> {

	
	public static class AdaptedBeanOperativa {
		@XmlElement(name = "operativa")
		public List<BeanOperativa> entry = new ArrayList<BeanOperativa>();
	}

	@Override
	public Map<String, BeanOperativa> unmarshal(AdaptedBeanOperativa AdaptedBeanOperativa) throws Exception {
		if (null == AdaptedBeanOperativa) {
			return null;
		}
		Map<String, BeanOperativa> map = new HashMap<String, BeanOperativa>(AdaptedBeanOperativa.entry.size());

		for (BeanOperativa infoOperativa : AdaptedBeanOperativa.entry) {
			map.put(infoOperativa.getNombre(), infoOperativa);
		}
		return map;
	}

	@Override
	public AdaptedBeanOperativa marshal(Map<String, BeanOperativa> map) throws Exception {

		AdaptedBeanOperativa wrapper = new AdaptedBeanOperativa();
		List<BeanOperativa> elements = new ArrayList<BeanOperativa>();
		if (map != null) {
			Iterator<Entry<String, BeanOperativa>> it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, BeanOperativa> e = (Map.Entry<String, BeanOperativa>) it.next();
				BeanOperativa infoOperativa = (BeanOperativa) e.getValue();
				elements.add(infoOperativa);

			}
		}
		wrapper.entry = elements;
		return wrapper;

	}
}