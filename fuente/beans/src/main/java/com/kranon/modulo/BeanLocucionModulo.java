package com.kranon.modulo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.menu.BeanPromptGenerico;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanLocucionModulo {

	/* Nombre de la Locucion */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Partes de las locuciones del Modulo */
	@XmlElement(name = "loc")
	private BeanPromptGenerico[] partesLoc;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BeanPromptGenerico[] getPartesLoc() {
		return partesLoc;
	}

	public void setPartesLoc(BeanPromptGenerico[] partesLoc) {
		this.partesLoc = partesLoc;
	}

	@Override
	public String toString() {
		return "BeanLocucionModulo [nombre=" + nombre + ", partesLoc=" + Arrays.toString(partesLoc) + "]";
	}

}
