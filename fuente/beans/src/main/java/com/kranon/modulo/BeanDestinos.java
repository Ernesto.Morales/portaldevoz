package com.kranon.modulo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"valor"})
public class BeanDestinos {

	/* Posible destino */
	@XmlAttribute(name = "valor")
	private String valor;

	/* Comision asociada al traspaso */
	@XmlValue
	private String comision;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getComision() {
		return comision;
	}

	public void setComision(String comision) {
		this.comision = comision;
	}

	@Override
	public String toString() {
		return "BeanDestinos [valor=" + valor + ", comision=" + comision + "]";
	}
	

}
