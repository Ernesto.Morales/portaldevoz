package com.kranon.modulo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPromociones {
	
	/* identificador del codigo */
	@XmlAttribute(name = "id")
	private String id;
	
	/* descripcion de la promocion */
	@XmlAttribute(name = "descripcion")
	private String descripcion;

	@XmlValue
	private String idPromoActiva;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdPromoActiva() {
		return idPromoActiva;
	}

	public void setIdPromoActiva(String idPromoActiva) {
		this.idPromoActiva = idPromoActiva;
	}

	@Override
	public String toString() {
		return "BeanPromociones [id=" + id + ", descripcion=" + descripcion
				+ ", idPromoActiva=" + idPromoActiva + "]";
	}


}


