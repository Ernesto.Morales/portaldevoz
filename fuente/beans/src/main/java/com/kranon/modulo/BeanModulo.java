package com.kranon.modulo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.menu.BeanPromptGenerico;

/**
 * Bean para la configuracion de un Modulo
 * 
 */
@XmlRootElement(name = "modulo")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanModulo {

	/* Nombre del Modulo */
	@XmlAttribute(name = "cd_modulo")
	private String codModulo;
	
	public String getCodModulo() {
		return codModulo;
	}

	public void setCodModulo(String codModulo) {
		this.codModulo = codModulo;
	}
	
	/*
	 * Numero de digitos en que termina el producto para listar en la seleccion
	 * de productos
	 */
	@XmlElement(name = "numDigitosTerminacion")
	private int numDigitosTerminacion;
	
	public int getNumDigitosTerminacion() {
		return numDigitosTerminacion;
	}

	public void setNumDigitosTerminacion(int numDigitosTerminacion) {
		this.numDigitosTerminacion = numDigitosTerminacion;
	}

	/* Numero de elementos a listar en la seleccion de producto */
	@XmlElement(name = "numElemListarSeleccProd")
	private int numElemListarSeleccProd;
	
	public int getNumElemListarSeleccProd() {
		return numElemListarSeleccProd;
	}

	public void setNumElemListarSeleccProd(int numElemListarSeleccProd) {
		this.numElemListarSeleccProd = numElemListarSeleccProd;
	}

	/* Numero de maximo de intentos permitidos para escuchar la info del modulo */
	@XmlElement(name = "maxNumIntentosEscucharInfo")
	private int maxNumIntentosEscucharInfo;

	public int getMaxNumIntentosEscucharInfo() {
		return maxNumIntentosEscucharInfo;
	}

	public void setMaxNumIntentosEscucharInfo(int maxNumIntentosEscucharInfo) {
		this.maxNumIntentosEscucharInfo = maxNumIntentosEscucharInfo;
	}
	
	/*
	 * Numero maximo de elementos sobre los que se escucha la info del modulo de
	 * una vez
	 */
	@XmlElement(name = "numTotalElemEscucharInfo")
	private int numTotalElemEscucharInfo;
	
	public int getNumTotalElemEscucharInfo() {
		return numTotalElemEscucharInfo;
	}

	public void setNumTotalElemEscucharInfo(int numTotalElemEscucharInfo) {
		this.numTotalElemEscucharInfo = numTotalElemEscucharInfo;
	}

	/*
	 * Si se debe activar el proceso interno del modulo, por ejemplo de envio
	 * email {ON,OFF}
	 */
	@XmlElement(name = "activacionProcesoInterno")
	private String activacionProcesoInterno;
	
	public String getActivacionProcesoInterno() {
		return activacionProcesoInterno;
	}

	public void setActivacionProcesoInterno(String activacionProcesoInterno) {
		this.activacionProcesoInterno = activacionProcesoInterno;
	}

	/*
	 * Si se debe activar o no la confirmacion del correo electronico del
	 * cliente {ON,OFF}
	 */
	@XmlElement(name = "confirmacionEmailProcesoInterno")
	private String confirmacionEmailProcesoInterno;

	public String getConfirmacionEmailProcesoInterno() {
		return confirmacionEmailProcesoInterno;
	}

	public void setConfirmacionEmailProcesoInterno(String confirmacionEmailProcesoInterno) {
		this.confirmacionEmailProcesoInterno = confirmacionEmailProcesoInterno;
	}
	
	/*
	 * Si se debe activar el proceso interno del modulo de envio de SMS
	 */
	@XmlElement(name = "activacionProcesoInternoSMS")
	private String activacionProcesoInternoSMS;
		
	public String getActivacionProcesoInternoSMS() {
		return activacionProcesoInternoSMS;
	}

	public void setActivacionProcesoInternoSMS(String activacionProcesoInternoSMS) {
		this.activacionProcesoInternoSMS = activacionProcesoInternoSMS;
	}

	/*
	 * Recorrer el string para saber si el bin de la tarjeta es de "credito empresarial"
	 */
	@XmlElement(name = "tipoBinCreditoEmpresarial")
	private String tipoBinCreditoEmpresarial;

	public String getTipoBinCreditoEmpresarial() {
		return tipoBinCreditoEmpresarial;
	}

	public void setTipoBinCreditoEmpresarial(String tipoBinCreditoEmpresarial) {
		this.tipoBinCreditoEmpresarial = tipoBinCreditoEmpresarial;
	}
	
	/*
	 * Recorrer el string para saber si el bin de la tarjeta es de tipo "credito"
	 */
	@XmlElement(name = "tipoBinCredito")
	private String tipoBinCredito;

	public String getTipoBinCredito() {
		return tipoBinCredito;
	}

	public void setTipoBinCredito(String tipoBinCredito) {
		this.tipoBinCredito = tipoBinCredito;
	}
	
	/*
	 * Recorrer el string para saber si el bin de la tarjeta es de tipo "debito"
	 */
	@XmlElement(name = "tipoBinDebito")
	private String tipoBinDebito;

	public String getTipoBinDebito() {
		return tipoBinDebito;
	}

	public void setTipoBinDebito(String tipoBinDebito) {
		this.tipoBinDebito = tipoBinDebito;
	}
	
	/*
	 * Recorrer el string para saber si el bin de la tarjeta es de "movil"
	 */
	@XmlElement(name = "tipoBinMovil")
	private String tipoBinMovil;

	public String getTipoBinMovil() {
		return tipoBinMovil;
	}

	public void setTipoBinMovil(String tipoBinMovil) {
		this.tipoBinMovil = tipoBinMovil;
	}
	
	/*
	 * Recorrer el string para saber si el bin de la tarjeta es de "prepago"
	 */
	@XmlElement(name = "tipoBinPrepago")
	private String tipoBinPrepago;

	public String getTipoBinPrepago() {
		return tipoBinPrepago;
	}

	public void setTipoBinPrepago(String tipoBinPrepago) {
		this.tipoBinPrepago = tipoBinPrepago;
	}
	
	/*
	 * Recorrer el string para saber si el bin de la tarjeta tiene "desborde"
	 */
	@XmlElement(name = "binConDesborde")
	private String binConDesborde;

	public String getBinConDesborde() {
		return binConDesborde;
	}

	public void setBinConDesborde(String binConDesborde) {
		this.binConDesborde = binConDesborde;
	}
	
	/*
	 * Verificar si el servicio de venta post-activacion esta activo
	 */
	@XmlElement(name = "servPostActivActivo")
	private String servPostActivActivo;

	public String getServPostActivActivo() {
		return servPostActivActivo;
	}

	public void setServPostActivActivo(String servPostActivActivo) {
		this.servPostActivActivo = servPostActivActivo;
	}
	
	/*
	 * Email al que se debe enviar el mail de Backoffice en caso de estar activo
	 * el proceso interno
	 */
	@XmlElement(name = "emailProcesoInterno")
	private String emailProcesoInterno;
	
	public String getEmailProcesoInterno() {
		return emailProcesoInterno;
	}

	public void setEmailProcesoInterno(String emailProcesoInterno) {
		this.emailProcesoInterno = emailProcesoInterno;
	}

	/*
	 * El numero de pagos vencidos debe ser inferior a maxPagosVencidos para dar
	 * la informacion
	 */
	@XmlElement(name = "maxPagosVencidos")
	private int maxPagosVencidos;

	public int getMaxPagosVencidos() {
		return maxPagosVencidos;
	}

	public void setMaxPagosVencidos(int maxPagosVencidos) {
		this.maxPagosVencidos = maxPagosVencidos;
	}
	
	/*
	 * Limite para dar la informacion de la Carta No Adeudo
	 */
	@XmlElement(name = "maxQuita")
	private float maxQuita;

	public float getMaxQuita() {
		return maxQuita;
	}

	public void setMaxQuita(float maxQuita) {
		this.maxQuita = maxQuita;
	}
	
	/*
	 * Cantidad a partir de la cual se le informa al cliente de una inversion
	 */
	@XmlElement(name = "cantidadInversion")
	private float cantidadInversion;

	public float getCantidadInversion() {
		return cantidadInversion;
	}

	public void setCantidadInversion(float cantidadInversion) {
		this.cantidadInversion = cantidadInversion;
	}

	/*
	 * Cantidad a partir de la cual se le informa al cliente de una inversion
	 */
	@XmlElement(name = "maxNumIntentosClave")
	private int maxNumIntentosClave;
	
	public int getMaxNumIntentosClave() {
		return maxNumIntentosClave;
	}

	public void setMaxNumIntentosClave(int maxNumIntentosClave) {
		this.maxNumIntentosClave = maxNumIntentosClave;
	}
	
	/*
	 * Numero de intentos maximo para introducir un cheque valido
	 */
	@XmlElement(name = "maxNumIntentosIntChequeValido")
	private int maxNumIntentosIntChequeValido;

	public int getMaxNumIntentosIntChequeValido() {
		return maxNumIntentosIntChequeValido;
	}

	public void setMaxNumIntentosIntChequeValido(int maxNumIntentosIntChequeValido) {
		this.maxNumIntentosIntChequeValido = maxNumIntentosIntChequeValido;
	}
	
	/*
	 * Numero maximo de cheques que el servicio permite suspender en una sola operacion
	 */
	@XmlElement(name = "maxChequesSuspender")
	private int maxChequesSuspender;
	
	public int getMaxChequesSuspender() {
		return maxChequesSuspender;
	}

	public void setMaxChequesSuspender(int maxChequesSuspender) {
		this.maxChequesSuspender = maxChequesSuspender;
	}

	/*
	 * Numero maximo de dias en los que un ticket de cliente se va a tener en cuenta
	 */
	@XmlElement(name = "maxDiferenciaDias")
	private int maxDiferenciaDias;

	public int getMaxDiferenciaDias() {
		return maxDiferenciaDias;
	}

	public void setMaxDiferenciaDias(int maxDiferenciaDias) {
		this.maxDiferenciaDias = maxDiferenciaDias;
	}
	
	/*
	 * Numero de intentos para introducir un numero de tarjeta valido
	 */
	@XmlElement(name = "maxNumIntentosTarjeta")
	private int maxNumIntentosTarjeta;
	
	public int getMaxNumIntentosTarjeta() {
		return maxNumIntentosTarjeta;
	}

	public void setMaxNumIntentosTarjeta(int maxNumIntentosTarjeta) {
		this.maxNumIntentosTarjeta = maxNumIntentosTarjeta;
	}

	/*
	 * Numero de intentos para introducir los datos de un traspaso
	 */
	@XmlElement(name = "maxNumIntentosTraspaso")
	private int maxNumIntentosTraspaso;
	
	public int getMaxNumIntentosTraspaso() {
		return maxNumIntentosTraspaso;
	}

	public void setMaxNumIntentosTraspaso(int maxNumIntentosTraspaso) {
		this.maxNumIntentosTraspaso = maxNumIntentosTraspaso;
	}
	
	/*
	 * Numero de intentos para introducir un dato multiplo de 50 para limites de uso
	 */
	@XmlElement(name = "maxNumIntentosMontoMultiplo")
	private int maxNumIntentosMontoMultiplo;
	
	public int getMaxNumIntentosMontoMultiplo() {
		return maxNumIntentosMontoMultiplo;
	}

	public void setMaxNumIntentosMontoMultiplo(int maxNumIntentosMontoMultiplo) {
		this.maxNumIntentosMontoMultiplo = maxNumIntentosMontoMultiplo;
	}
	
	/*
	 * Numero de intentos para introducir los datos de de montos lmite correctamente
	 */
	@XmlElement(name = "maxNumIntentosMontoDiarioMayorMes")
	private int maxNumIntentosMontoDiarioMayorMes;
	
	public int getMaxNumIntentosMontoDiarioMayorMes() {
		return maxNumIntentosMontoDiarioMayorMes;
	}

	public void setMaxNumIntentosMontoDiarioMayorMes(int maxNumIntentosMontoDiarioMayorMes) {
		this.maxNumIntentosMontoDiarioMayorMes = maxNumIntentosMontoDiarioMayorMes;
	}
	
	/*
	 * Numero de intentos para escuchar el numero de folio asociado
	 */
	@XmlElement(name = "maxNumIntentosInfoNumFolio")
	private int maxNumIntentosInfoNumFolio;
	
	public int getMaxNumIntentosInfoNumFolio() {
		return maxNumIntentosInfoNumFolio;
	}

	public void setMaxNumIntentosInfoNumFolio(int maxNumIntentosInfoNumFolio) {
		this.maxNumIntentosInfoNumFolio = maxNumIntentosInfoNumFolio;
	}
	
	/*
	 * Tipo de cuentas que son validas como origen de un traspaso
	 */
	@XmlElement(name = "tipoOrigenTraspaso")
	private String tipoOrigenTraspaso;
	
	public String getTipoOrigenTraspaso() {
		return tipoOrigenTraspaso;
	}

	public void setTipoOrigenTraspaso(String tipoOrigenTraspaso) {
		this.tipoOrigenTraspaso = tipoOrigenTraspaso;
	}
	
	/*
	 * Tipo de cuentas que son validas como destino de un traspaso
	 */
	@XmlElement(name = "tipoDestinoTraspaso")
	private String tipoDestinoTraspaso;
	
	public String getTipoDestinoTraspaso() {
		return tipoDestinoTraspaso;
	}

	public void setTipoDestinoTraspaso(String tipoDestinoTraspaso) {
		this.tipoDestinoTraspaso = tipoDestinoTraspaso;
	}
	
	/*
	 * Numeros de dias que vamos a recuperar los movimientos
	 */
	@XmlElement(name = "numDiasMovimientos")
	private int numDiasMovimientos;
	
	public int getNumDiasMovimientos() {
		return numDiasMovimientos;
	}

	public void setNumDiasMovimientos(int numDiasMovimientos) {
		this.numDiasMovimientos = numDiasMovimientos;
	}
	
	/*
	 * Maximo numero de movimientos que enviaremos por email
	 */
	@XmlElement(name = "maxMovPorEmail")
	private int maxMovPorEmail;
	
	public int getMaxMovPorEmail() {
		return maxMovPorEmail;
	}

	public void setMaxMovPorEmail(int maxMovPorEmail) {
		this.maxMovPorEmail = maxMovPorEmail;
	}
	
	/*
	 * Numeros de intentos para solicitar los datos electronicos (fijo, movil)
	 */
	@XmlElement(name = "numIntentosSolicitudDatosElect")
	private int numIntentosSolicitudDatosElect;
	
	public int getNumIntentosSolicitudDatosElect() {
		return numIntentosSolicitudDatosElect;
	}

	public void setNumIntentosSolicitudDatosElect(int numIntentosSolicitudDatosElect) {
		this.numIntentosSolicitudDatosElect = numIntentosSolicitudDatosElect;
	}
	
	/*
	 * Numero maximo de elementos sobre los que se escucha la info del modulo de
	 * una vez
	 */
	@XmlElement(name = "numMeses")
	private int numMeses;

	public int getNumMeses() {
		return numMeses;
	}
	public void setNumMeses(int numMeses) {
		this.numMeses = numMeses;
	}
	
	/*
	 * Numero maximo de pagos a locutar de todos los que se obtienen del WS ListPayments
	 * una vez
	 */
	@XmlElement(name = "numPagosLocutar")
	private int numPagosLocutar;

	public int getNumPagosLocutar() {
		return numPagosLocutar;
	}

	public void setNumPagosLocutar(int numPagosLocutar) {
		this.numPagosLocutar = numPagosLocutar;
	}

	/* Locuciones del Modulo */
	@XmlElementWrapper(name = "locuciones")
	@XmlElement(name = "locucion")
	private BeanLocucionModulo[] locuciones;

	public BeanLocucionModulo[] getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocucionModulo[] locuciones) {
		this.locuciones = locuciones;
	}

	/* Operaciones permitidas en los traspasos y pagos TDC */
	@XmlElementWrapper(name = "operacionesValidas")
	@XmlElement(name = "origen")
	private BeanDestinosTraspaso[] operacionesValidas;
	
	public BeanDestinosTraspaso[] getOperacionesValidas() {
		return operacionesValidas;
	}

	public void setOperacionesValidas(BeanDestinosTraspaso[] operacionesValidas) {
		this.operacionesValidas = operacionesValidas;
	}
	
	/* Relacion entre codigos de promocion y tipo de promocion*/
	@XmlElementWrapper(name = "promociones")
	@XmlElement(name = "promocion")
	private BeanPromociones[] promociones;	
	
	public BeanPromociones[] getPromociones() {
		return promociones;
	}

	public void setPromociones(BeanPromociones[] promociones) {
		this.promociones = promociones;
	}

	@Override
	public String toString() {
		return "BeanModulo [codModulo=" + codModulo
				+ ", numDigitosTerminacion=" + numDigitosTerminacion
				+ ", numElemListarSeleccProd=" + numElemListarSeleccProd
				+ ", maxNumIntentosEscucharInfo=" + maxNumIntentosEscucharInfo
				+ ", numTotalElemEscucharInfo=" + numTotalElemEscucharInfo
				+ ", activacionProcesoInterno=" + activacionProcesoInterno
				+ ", confirmacionEmailProcesoInterno="
				+ confirmacionEmailProcesoInterno
				+ ", activacionProcesoInternoSMS="
				+ activacionProcesoInternoSMS + ", tipoBinCreditoEmpresarial="
				+ tipoBinCreditoEmpresarial + ", tipoBinCredito="
				+ tipoBinCredito + ", tipoBinDebito=" + tipoBinDebito
				+ ", tipoBinMovil=" + tipoBinMovil + ", tipoBinPrepago="
				+ tipoBinPrepago + ", binConDesborde=" + binConDesborde
				+ ", servPostActivActivo=" + servPostActivActivo
				+ ", emailProcesoInterno=" + emailProcesoInterno
				+ ", maxPagosVencidos=" + maxPagosVencidos + ", maxQuita="
				+ maxQuita + ", cantidadInversion=" + cantidadInversion
				+ ", maxNumIntentosClave=" + maxNumIntentosClave
				+ ", maxNumIntentosIntChequeValido="
				+ maxNumIntentosIntChequeValido + ", maxChequesSuspender="
				+ maxChequesSuspender + ", maxDiferenciaDias="
				+ maxDiferenciaDias + ", maxNumIntentosTarjeta="
				+ maxNumIntentosTarjeta + ", maxNumIntentosTraspaso="
				+ maxNumIntentosTraspaso + ", maxNumIntentosMontoMultiplo="
				+ maxNumIntentosMontoMultiplo
				+ ", maxNumIntentosMontoDiarioMayorMes="
				+ maxNumIntentosMontoDiarioMayorMes
				+ ", maxNumIntentosInfoNumFolio=" + maxNumIntentosInfoNumFolio
				+ ", tipoOrigenTraspaso=" + tipoOrigenTraspaso
				+ ", tipoDestinoTraspaso=" + tipoDestinoTraspaso
				+ ", numDiasMovimientos=" + numDiasMovimientos
				+ ", maxMovPorEmail=" + maxMovPorEmail
				+ ", numIntentosSolicitudDatosElect="
				+ numIntentosSolicitudDatosElect + ", numMeses=" + numMeses
				+ ", locuciones=" + Arrays.toString(locuciones)
				+ ", operacionesValidas=" + Arrays.toString(operacionesValidas)
				+ ", promociones=" + Arrays.toString(promociones) + "]";
	}
	


	// ***

	/**
	 * Devuelve todas las partes de una locucion con ese nombre
	 * 
	 * @param nombre
	 * @return {@link BeanPromptGenerico[]}
	 */
	public BeanPromptGenerico[] getLocucionesByNombre(String nombre) {
		if (this.locuciones == null || this.locuciones.length == 0) {
			// no hay locuciones para buscar
			return null;
		}

		for (BeanLocucionModulo loc : this.locuciones) {
			if (loc.getNombre().equalsIgnoreCase(nombre)) {
				// es la locucion que estamos buscando
				// ordeno todos sus prompt por id por si acaso
				BeanPromptGenerico[] promptOrdenados = this.ordenarPromptsPorId(loc.getPartesLoc());
				return promptOrdenados;
			}
		}
		// no he encontrado esa locucion
		return null;
	}

	/**
	 * Ordenar los prompt de un intento por id de menor a mayor
	 * 
	 * @param {@link BeanPromptGenerico[]} prompts
	 * @return {@link BeanPromptGenerico[]}
	 * @throws Exception
	 */
	private BeanPromptGenerico[] ordenarPromptsPorId(BeanPromptGenerico[] prompts) {

		BeanPromptGenerico[] promptsOrdenados;

		try {
			promptsOrdenados = prompts;

			for (int i = 0; i < (promptsOrdenados.length - 1); i++) {
				for (int j = i + 1; j < promptsOrdenados.length; j++) {
					int posicionI = Integer.parseInt(promptsOrdenados[i].getId());
					int posicionJ = Integer.parseInt(promptsOrdenados[j].getId());
					if (posicionI > posicionJ) {
						BeanPromptGenerico variableauxiliar = promptsOrdenados[i];
						promptsOrdenados[i] = promptsOrdenados[j];
						promptsOrdenados[j] = variableauxiliar;

					}
				}
			}
		} catch (Exception e) {
			return null;
		}

		return promptsOrdenados;
	}
}
