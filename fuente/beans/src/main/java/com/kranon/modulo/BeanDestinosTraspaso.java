package com.kranon.modulo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanDestinosTraspaso {

	/* Nombre de la Locucion */
	@XmlAttribute(name = "valor")
	private String valor;

	/* Partes de las locuciones del Modulo */
	@XmlElement(name = "destino")
	private BeanDestinos[] destino;

	
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public BeanDestinos[] getDestino() {
		return destino;
	}

	public void setDestino(BeanDestinos[] destino) {
		this.destino = destino;
	}

	@Override
	public String toString() {
		return "BeanDestinosTraspaso [valor=" + valor + ", destino="
				+ Arrays.toString(destino) + "]";
	}

}
