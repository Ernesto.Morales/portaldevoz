
package com.kranon.unmarshall;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import com.kranon.bean.blacklist.BeanBlackList;
import com.kranon.bean.controlador.BeanControlador;
import com.kranon.bean.enc.BeanEncuesta;
import com.kranon.bean.excepserv.BeanGestionExcepciones;
import com.kranon.bean.gestioncm.BeanGestionCuadroMando;
import com.kranon.bean.gestioncontroladores.BeanGestionControladores;
import com.kranon.bean.gestionmodulos.BeanGestionModulos;
import com.kranon.bean.gestoperativas.BeanGestionOperativas;
import com.kranon.bean.menu.BeanMenu;
import com.kranon.bean.plantillanotifications.BeanConfiguracionNotifications;
import com.kranon.bean.respuestas.BeanRespuestasPreguntas;
import com.kranon.bean.saldo.BeanLocSaldo;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.bean.stat.contador.BeanStatContador;
import com.kranon.bean.stat.llamada.BeanStatLlamada;
import com.kranon.bean.transfer.BeanTransfer;
import com.kranon.bean.transfer_schema.BeanTransferSchema;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.loccomunes.BeanLocucionesComunes;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.modulo.BeanModulo;
import com.kranon.utilidades.UtilidadesBeans;
import com.kranon.bean.maurespuestas.BeanRespuestasMau;
import com.kranon.bean.mensajeserror.BeanMensajesError;
import com.kranon.bean.promp.BeanPromps;
import com.kranon.bean.shortcut.BeanShortcuts;

/**
 * Clase que realiza el "Unmarshalling" de XML a Beans Clase usada por todos los SERVICIOS IVR
 * 
 * @author abalfaro
 *
 */
public class Unmarshall {

	private CommonLoggerProcess log;

	// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String idInvocacion;
	private String idServicio;
	private String idElemento;
	private String idArchivo;
	private String rutaCompleta;

	// [20161207-NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION

	public Unmarshall(String idInvocacion, String idElemento) {

		if (log == null) {
			log = new CommonLoggerProcess("UNMARSHAL");
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idElemento = idElemento;
		this.idInvocacion = idInvocacion;
	}

	/**
	 * Convierte de un XML de servicio a BeanServicio
	 * 
	 * @param codServIVR
	 * @return {@link BeanServicio}
	 * @throws Exception
	 */
	public BeanServicio unmarshallServicio(String codServIVR) throws Exception {

		BeanServicio bean = null;
		try {
			/** INICIO EVENTO - INICIO DE MoDULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("codServIVR", codServIVR));
			log.initModuleProcess("UNMARSHALL_SERVICIO", parametrosEntrada);
			/** FIN EVENTO - INICIO DE MoDULO **/

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			log.comment("path=" + path);

			String pathServ = util.leeParametroUnmarshall("PATH_XML_SERV");
			log.comment("pathServ=" + pathServ);

			String prefixServ = util.leeParametroUnmarshall("PREFIX_XML_SERV");
			log.comment("prefixServ=" + prefixServ);
			// nombre del fichero
			String fileName = path + pathServ + prefixServ + codServIVR + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServIVR);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServIVR;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanServicio.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanServicio) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanServicio", bean.toString()));
			log.endModuleProcess("UNMARSHALL_SERVICIO", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_SERVICIO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_SERVICIO", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convierte de un XML de transfer a BeanTransfer
	 * 
	 * @param codTransfer
	 * @return {@link BeanTransfer}
	 * @throws Exception
	 */
	public BeanTransfer unmarshallTransfer(String codTransfer) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codTransfer", codTransfer));
		log.initModuleProcess("UNMARSHALL_TRANSFER", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanTransfer bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathTransfer = util.leeParametroUnmarshall("PATH_XML_TRANSFER");
			String prefixTransfer = util.leeParametroUnmarshall("PREFIX_XML_TRANSFER");
			Integer posicion_ = codTransfer.indexOf("_");
			String codServicio = codTransfer.substring(0,posicion_)+"/";
		
			// nombre del fichero
//			String fileName = path +codServicio+ pathTransfer + prefixTransfer + codTransfer + ".xml";
			String fileName = path + pathTransfer + prefixTransfer + codTransfer + ".xml";

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codTransfer.substring(0, codTransfer.indexOf("_"));
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanTransfer.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanTransfer) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanTransfer", bean.toString()));
			log.endModuleProcess("UNMARSHALL_TRANSFER", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_TRANSFER", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_TRANSFER", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}

	}

	/**
	 * Convierte de un XML de TransferSchema a BeanTransferSchema
	 * 
	 * @return {@link BeanTransferSchema}
	 * @throws Exception
	 */
	public BeanTransferSchema unmarshallTransferSchema() throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_TRANSFER_SCHEMA", null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanTransferSchema bean = null;
		try {
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathTransferSchema = util.leeParametroUnmarshall("PATH_XML_TRANSFER_SCHEMA");
			String prefixTransferSchema = util.leeParametroUnmarshall("PREFIX_XML_TRANSFER_SCHEMA");
			// nombre del fichero
			String fileName = "" + ".xml";

			File file = new File(path + pathTransferSchema + prefixTransferSchema + fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = "";
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanTransferSchema.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanTransferSchema) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanTransferSchema", bean.toString()));
			log.endModuleProcess("UNMARSHALL_TRANSFER_SCHEMA", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_TRANSFER_SCHEMA", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_TRANSFER_SCHEMA", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}

	}

	/**
	 * Convertir un XML de gestion controladores a BeanGestionControladores
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanGestionControladores unmarshallGestionControladores(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanGestionControladores bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_GEST_CONT");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_GEST_CONT");
			String sufixGestCont = util.leeParametroUnmarshall("SUFIX_XML_GEST_CONT");
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixGestCont + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionControladores.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanGestionControladores) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de controladores se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionControladores", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_CONTROLADORES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir un XML de Controlador a BeanControlador
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanControlador}
	 */
	public BeanControlador unmarshallControlador(String codServ, String nombreControlador) {
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServ));
		parametrosEntrada.add(new ParamEvent("nombreControlador", nombreControlador));
		log.initModuleProcess("UNMARSHALL_CONTROLADOR", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanControlador bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathCont = util.leeParametroUnmarshall("PATH_XML_CONTROLADOR");
			String prefixCont = util.leeParametroUnmarshall("PREFIX_XML_CONTROLADOR");
			String middleCont = util.leeParametroUnmarshall("MIDDLE_XML_CONTROLADOR");
			String sufixCont = util.leeParametroUnmarshall("SUFIX_XML_CONTROLADOR");
			// nombre del fichero
			String fileName = path + pathCont + prefixCont + codServ + middleCont + nombreControlador + sufixCont + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServ);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServ;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanControlador.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanControlador) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("El controlador se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanControlador", bean.toString()));
			log.endModuleProcess("UNMARSHALL_CONTROLADOR", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_CONTROLADOR", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_CONTROLADOR", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir un XML de gestion de modulos a BeanGestionModulos
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionModulos}
	 */
	public BeanGestionModulos unmarshallGestionModulos(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_MODULOS", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanGestionModulos bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestModulos = util.leeParametroUnmarshall("PATH_XML_GEST_MODULOS");
			String prefixGestModulos = util.leeParametroUnmarshall("PREFIX_XML_GEST_MODULOS");
			String sufixGestModulos = util.leeParametroUnmarshall("SUFIX_XML_GEST_MODULOS");
			// nombre del fichero
			String fileName = path + pathGestModulos + prefixGestModulos + codServicio + sufixGestModulos + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionModulos.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanGestionModulos) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de modulos se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionModulos", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_MODULOS", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_MODULOS", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_MODULOS", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir un XML de gestion de modulos a BeanGestionOperativas
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionOperativas}
	 */
	public BeanGestionOperativas unmarshallGestionOperativas(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_OPERATIVAS", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanGestionOperativas bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestOperativas = util.leeParametroUnmarshall("PATH_XML_GESTION_OPERATIVAS");
			String prefixGestOperativas = util.leeParametroUnmarshall("PREFIX_XML_GESTION_OPERATIVAS");
			String sufixGestOperativas = util.leeParametroUnmarshall("SUFIX_XML_GESTION_OPERATIVAS");
			// nombre del fichero
			String fileName = path + pathGestOperativas + prefixGestOperativas + codServicio + sufixGestOperativas + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionOperativas.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanGestionOperativas) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de operativas se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionOperativas", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_OPERATIVAS", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_OPERATIVAS", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_OPERATIVAS", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir un XML de Modulo a BeanModulo
	 * 
	 * @param codServ
	 * @param nombreModulo
	 * @return {@link BeanModulo}
	 */
	public BeanModulo unmarshallModulo(String codServ, String nombreModulo) {
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServ));
		parametrosEntrada.add(new ParamEvent("nombreModulo", nombreModulo));
		log.initModuleProcess("UNMARSHALL_MODULO", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanModulo bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathModulos = util.leeParametroUnmarshall("PATH_XML_MODULO");
			String prefixMod = util.leeParametroUnmarshall("PREFIX_XML_MODULO");
			String middleMod = util.leeParametroUnmarshall("MIDDLE_XML_MODULO");
			String sufixMod = util.leeParametroUnmarshall("SUFIX_XML_MODULO");
			// nombre del fichero
			String fileName = path + pathModulos + prefixMod + codServ + middleMod + nombreModulo + sufixMod + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServ);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServ;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanModulo.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanModulo) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("El modulo se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanModulo", bean.toString()));
			log.endModuleProcess("UNMARSHALL_MODULO", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_MODULO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_MODULO", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convierte de un XML de Menu a BeanMenu
	 * 
	 * @param codServicio
	 * @param codMenu
	 * @return {@link BeanMenu}
	 * @throws Exception
	 */
	public BeanMenu unmarshallMenu(String codServicio, String codMenu) throws Exception {

		BeanMenu bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codMenu", codMenu));
		log.initModuleProcess("UNMARSHALL_MENU", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathMenu = util.leeParametroUnmarshall("PATH_XML_MENU");
			String prefixMenu = util.leeParametroUnmarshall("PREFIX_XML_MENU");
			// nombre del fichero
			String fileName = path + pathMenu + prefixMenu + codMenu + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanMenu.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanMenu) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanMenu", bean.toString()));
			log.endModuleProcess("UNMARSHALL_MENU", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_MENU", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_MENU", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convierte de un XML de Menu Encuesta a BeanMenu
	 * 
	 * @param codServicio
	 * @param codMenu
	 * @return {@link BeanMenu}
	 * @throws Exception
	 */
	public BeanMenu unmarshallMenuEncuesta(String codServicioEncuesta, String codMenu) throws Exception {

		BeanMenu bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicioEncuesta", codServicioEncuesta));
		parametrosEntrada.add(new ParamEvent("codMenu", codMenu));
		log.initModuleProcess("UNMARSHALL_MENU_ENC", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathMenu = util.leeParametroUnmarshall("PATH_XML_MENU_ENC");
			String prefixMenu = util.leeParametroUnmarshall("PREFIX_XML_MENU_ENC");
			// nombre del fichero
			String fileName = path + pathMenu + prefixMenu + codMenu + ".xml";

			if (fileName.contains("<cod_servencuesta>")) {
				fileName = fileName.replace("<cod_servencuesta>", codServicioEncuesta);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicioEncuesta;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanMenu.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanMenu) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanMenu", bean.toString()));
			log.endModuleProcess("UNMARSHALL_MENU_ENC", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_MENU_ENC", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_MENU_ENC", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convertir un XML de LocucionesComunes a BeanLocucionesComunes
	 * 
	 * @param codServ
	 * @return {@link BeanLocucionesComunes}
	 */
	public BeanLocucionesComunes unmarshallLocucionesComunes(String codServ) {
		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServ));
		log.initModuleProcess("UNMARSHALL_LOCUCIONES_COMUNES", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanLocucionesComunes bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathLoc = util.leeParametroUnmarshall("PATH_XML_LOC_COMUNES");
			String prefixLoc = util.leeParametroUnmarshall("PREFIX_XML_LOC_COMUNES");
			String sufixLoc = util.leeParametroUnmarshall("SUFIX_XML_LOC_COMUNES");
			// nombre del fichero
			String fileName = path + pathLoc + prefixLoc + codServ + sufixLoc + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServ);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServ;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanLocucionesComunes.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanLocucionesComunes) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("Las locuciones comunes se han recuperadon a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			// parametrosSalida.add(new ParamEvent("BeanLocucionesComunes", bean.toString()));
			log.endModuleProcess("UNMARSHALL_LOCUCIONES_COMUNES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_LOCUCIONES_COMUNES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_LOCUCIONES_COMUNES", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir de XML a BeanWebServices
	 * 
	 * @return {@link BeanWebServices}
	 * @throws Exception
	 */
	public BeanWebServices unmarshallWebServices(String codServicio) throws Exception {

		String resultadoOperacion = "KO";
		BeanWebServices bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosAdic = new ArrayList<ParamEvent>();
		parametrosAdic.add(new ParamEvent("codServicio", codServicio));
		this.log.initModuleProcess("UNMARSHALL_WEBSERVICES", parametrosAdic);
		/** FIN EVENTO - INICIO DE MoDULO **/

		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathWs = util.leeParametroUnmarshall("PATH_XML_WS");
			String prefixWs = util.leeParametroUnmarshall("PREFIX_XML_WS");

			// nombre del fichero
			String fileName = path + pathWs + prefixWs + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			this.log.comment("fileName=" + fileName);

			File file = new File(fileName);

			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanWebServices.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanWebServices) jaxbUnmarshaller.unmarshal(file);

			resultadoOperacion = "OK";

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_WEBSERVICES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		} finally {
			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanWebServices", bean == null ? "null" : bean.toString()));
			this.log.endModuleProcess("UNMARSHALL_WEBSERVICES", resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/
		}
	}

	/**
	 * Convertir un XML de gestion excepciones a BeanGestionExcepciones
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionExcepciones}
	 */
	public BeanGestionExcepciones unmarshallGestionExcepciones(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_EXCEPCIONES", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanGestionExcepciones bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_GEST_EXCEP");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_GEST_EXCEP");
			String sufixGestCont = util.leeParametroUnmarshall("SUFIX_XML_GEST_EXCEP");
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixGestCont + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionExcepciones.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanGestionExcepciones) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de excepciones se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionExcepciones", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_EXCEPCIONES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_EXCEPCIONES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_EXCEPCIONES", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convertir de XML a BeanConfiguracionNotifications
	 * 
	 * @return {@link BeanConfiguracionNotifications}
	 * @throws Exception
	 */
	public BeanConfiguracionNotifications unmarshallConfiguracionNotifications(String codServicio) throws Exception {

		String resultadoOperacion = "KO";
		BeanConfiguracionNotifications bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosAdic = new ArrayList<ParamEvent>();
		parametrosAdic.add(new ParamEvent("codServicio", codServicio));
		this.log.initModuleProcess("UNMARSHALL_CONFIG_NOTIFICATIONS", parametrosAdic);
		/** FIN EVENTO - INICIO DE MoDULO **/

		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathWs = util.leeParametroUnmarshall("PATH_XML_NOTIFICATIONS");
			String prefixWs = util.leeParametroUnmarshall("PREFIX_XML_NOTIFICATIONS");
			// nombre del fichero
			String fileName = "" + ".xml";

			File file = new File(path + pathWs + prefixWs + fileName);

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			this.log.comment("fileName=" + fileName);

			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161207-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanConfiguracionNotifications.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanConfiguracionNotifications) jaxbUnmarshaller.unmarshal(file);

			resultadoOperacion = "OK";

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_CONFIG_NOTIFICATIONS", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;

		} finally {
			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanConfiguracionNotifications", bean == null ? "null" : bean.toString()));
			this.log.endModuleProcess("UNMARSHALL_CONFIG_NOTIFICATIONS", resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/
		}

	}

	/**
	 * Convertir de XML a BeanBlackList
	 * 
	 * @return {@link BeanBlackList}
	 * @throws Exception
	 */
	public BeanBlackList unmarshallBlackList() throws Exception {
		BeanBlackList bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_BLACKLIST", null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathBlackList = util.leeParametroUnmarshall("PATH_XML_BLACKLIST");
			String prefixBlackList = util.leeParametroUnmarshall("PREFIX_XML_BLACKLIST");
			// nombre del fichero
			String fileName = "" + ".xml";

			File file = new File(path + pathBlackList + prefixBlackList + fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = "";
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanBlackList.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanBlackList) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanBlackList", bean.toString()));
			log.endModuleProcess("UNMARSHALL_BLACKLIST", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_BLACKLIST", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_BLACKLIST", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convertir un XML de loc saldo a BeanLocSaldo
	 * 
	 * @return {@link BeanLocSaldo}
	 * @throws Exception
	 */
	public BeanLocSaldo unmarshallLocSaldos() throws Exception {

		BeanLocSaldo bean = null;

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_LOC_SALDOS", null);
		/** FIN EVENTO - INICIO DE MoDULO **/
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathSLocSaldo = util.leeParametroUnmarshall("PATH_XML_LOC_SALDOS");
			String prefixLocSaldo = util.leeParametroUnmarshall("PREFIX_XML_LOC_SALDOS");
			// nombre del fichero
			String fileName = "" + ".xml";

			File file = new File(path + pathSLocSaldo + prefixLocSaldo + fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = "";
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanLocSaldo.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanLocSaldo) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanLocSaldo", bean.toString()));
			log.endModuleProcess("UNMARSHALL_LOC_SALDOS", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_LOC_SALDOS", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_LOC_SALDOS", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convertir un XML de encuesta a BeanEncuesta
	 * 
	 * @param codEncuesta
	 * @return {@link BeanEncuesta}
	 * @throws Exception
	 */
	public BeanEncuesta unmarshallEncuesta(String codServicioEncuesta, String codEncuesta) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicioEncuesta", codServicioEncuesta));
		parametrosEntrada.add(new ParamEvent("codEncuesta", codEncuesta));
		log.initModuleProcess("UNMARSHALL_ENCUESTA", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanEncuesta bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathEnc = util.leeParametroUnmarshall("PATH_XML_ENC");
			String prefixEnc = util.leeParametroUnmarshall("PREFIX_XML_ENC");
			// nombre del fichero
			String fileName = path + pathEnc + prefixEnc + codEncuesta + ".xml";

			if (fileName.contains("<cod_servencuesta>")) {
				fileName = fileName.replace("<cod_servencuesta>", codServicioEncuesta);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codEncuesta;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanEncuesta.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanEncuesta) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La encuesta se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanEncuesta", bean.toString()));
			log.endModuleProcess("UNMARSHALL_ENCUESTA", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_ENCUESTA", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_ENCUESTA", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convierte de un XML de Respuestas a BeanRespuestas
	 * 
	 * @return {@link BeanRespuestasPreguntas}
	 * @throws Exception
	 */
	public BeanRespuestasPreguntas unmarshallRespuestas() throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_RESPUESTAS", null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanRespuestasPreguntas bean = null;
		try {
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathRespuestas = util.leeParametroUnmarshall("PATH_XML_RESPUESTAS");
			String prefixRespuestas = util.leeParametroUnmarshall("PREFIX_XML_RESPUESTAS");
			// nombre del fichero
			String fileName = "" + ".xml";

			File file = new File(path + pathRespuestas + prefixRespuestas + fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = "";
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanRespuestasPreguntas.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanRespuestasPreguntas) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanRespuestasPreguntas", bean.toString()));
			log.endModuleProcess("UNMARSHALL_RESPUESTAS", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_RESPUESTAS", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_RESPUESTAS", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convertir un XML de gestion de modulos a BeanGestionCuadroMando
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionCuadroMando}
	 */
	public BeanGestionCuadroMando unmarshallGestionCuadroMando(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_CUADRO_MANDO", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanGestionCuadroMando bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCM = util.leeParametroUnmarshall("PATH_XML_GESTION_CUADRO_MANDO");
			String prefixGestCM = util.leeParametroUnmarshall("PREFIX_XML_GESTION_CUADRO_MANDO");
			String sufixGestCM = util.leeParametroUnmarshall("SUFIX_XML_GESTION_CUADRO_MANDO");
			// nombre del fichero
			String fileName = path + pathGestCM + prefixGestCM + codServicio + sufixGestCM + ".xml";
			
			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionCuadroMando.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanGestionCuadroMando) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de cuadro mando se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionCuadroMando", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CUADRO_MANDO", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_CUADRO_MANDO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MODULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_CUADRO_MANDO", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}

	/**
	 * Convierte de un XML de StatLlamada a BeanStatLlamada
	 * 
	 * @return {@link BeanStatLlamada}
	 * @throws Exception
	 */
	public BeanStatLlamada unmarshallStatLlamada(String codServIVR, String idLlamada) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_STAT_LLAMADA", null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanStatLlamada bean = null;
		try {
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathStatLlam = util.leeParametroUnmarshall("PATH_XML_STAT_LLAMADA");
			String prefixStatLlam = util.leeParametroUnmarshall("PREFIX_XML_STAT_LLAMADA");
			// nombre del fichero
			String fileName = path + pathStatLlam + prefixStatLlam + idLlamada + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServIVR.toUpperCase());
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServIVR;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanStatLlamada.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanStatLlamada) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanStatLlamada", bean.toString()));
			log.endModuleProcess("UNMARSHALL_STAT_LLAMADA", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_STAT_LLAMADA", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_STAT_LLAMADA", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}

	/**
	 * Convierte de un XML de StatContador a BeanStatContador
	 * 
	 * @return {@link BeanStatContador}
	 * @throws Exception
	 */
	public BeanStatContador unmarshallStatContador(String codServIVR, String idLlamada) throws Exception {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess("UNMARSHALL_STAT_CONTADOR", null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanStatContador bean = null;
		try {
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathStatCont = util.leeParametroUnmarshall("PATH_XML_STAT_CONTADOR");
			String prefixStatCont = util.leeParametroUnmarshall("PREFIX_XML_STAT_CONTADOR");
			// nombre del fichero
			String fileName = path + pathStatCont + prefixStatCont + idLlamada + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServIVR.toUpperCase());
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServIVR;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanStatContador.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanStatContador) jaxbUnmarshaller.unmarshal(file);

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanStatContador", bean.toString()));
			log.endModuleProcess("UNMARSHALL_STAT_CONTADOR", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_STAT_CONTADOR", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_STAT_CONTADOR", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			throw e;
		}
	}
	
	/**
	 * Convertir un XML de gestion controladores a BeanShortcuts
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanShortcuts unmarshallShortCuts(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanShortcuts bean = null;
		try {

			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_SHORT_CONT");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_SHORT_CONT");
			String sufixGestCont = util.leeParametroUnmarshall("SUFIX_XML_SHORT_CONT");
//			String path = "W:/Desarrollo";
//			String pathGestCont = "<cod_servivr>/shortcuts/";
//			String prefixGestCont = "";
//			String sufixGestCont = "-GESTION-SHORTCUT";
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixGestCont + ".xml";

			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);

			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanShortcuts.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanShortcuts) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de controladores se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanGestionControladores", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_CONTROLADORES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}
	
	/**
	 * Convertir un XML de respuestas MAU a BeanMauRespuestas
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanRespuestasMau unmarshallMauRespuestas(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_RESPUESTAS_MAU", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanRespuestasMau bean = null;
		try {

//			UtilidadesBeans util = new UtilidadesBeans(log);
//
//			// ruta comun, ruta generica y prefijo para el fichero
//			String path = util.leeParametroUnmarshall("PATH_XML");
//			String pathModulos = util.leeParametroUnmarshall("PATH_XML_MODULO");
//			String prefixMod = util.leeParametroUnmarshall("PREFIX_XML_MODULO");
//			String middleMod = util.leeParametroUnmarshall("MIDDLE_XML_MODULO");
//			String sufixMod = "MAU-RESPUESTAS";
//			// nombre del fichero
//			String fileName = path + pathModulos + prefixMod + codServicio + middleMod + sufixMod + ".xml";
//
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_SHORT_CONT");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_SHORT_CONT");
			String sufixMod = "-MAU-RESPUESTAS";
//			String path = "W:/Desarrollo";
//			String pathGestCont = "<cod_servivr>/shortcuts/";
//			String prefixGestCont = "";
//			String sufixGestCont = "-GESTION-SHORTCUT";
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixMod + ".xml";

//
			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanRespuestasMau.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanRespuestasMau) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de controladores se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanMauRespuestas", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/
			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_GESTION_CONTROLADORES", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_RESPUESTAS_MAU", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}
	
	/**
	 * Convertir un XML de Proms a BeanProms
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanPromps unmarshallPromps(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_Proms", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanPromps bean = null;
		try {

//			UtilidadesBeans util = new UtilidadesBeans(log);
//
//			// ruta comun, ruta generica y prefijo para el fichero
//			String path = util.leeParametroUnmarshall("PATH_XML");
//			String pathModulos = util.leeParametroUnmarshall("PATH_XML_MODULO");
//			String prefixMod = util.leeParametroUnmarshall("PREFIX_XML_MODULO");
//			String middleMod = util.leeParametroUnmarshall("MIDDLE_XML_MODULO");
//			String sufixMod = "MAU-RESPUESTAS";
//			// nombre del fichero
//			String fileName = path + pathModulos + prefixMod + codServicio + middleMod + sufixMod + ".xml";
//
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_SHORT_CONT");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_SHORT_CONT");
			String sufixMod = "-PROMS";
//			String path = "W:/Desarrollo";
//			String pathGestCont = "<cod_servivr>/shortcuts/";
//			String prefixGestCont = "";
//			String sufixGestCont = "-GESTION-SHORTCUT";
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixMod + ".xml";

//
			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanPromps.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanPromps) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de controladores se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanProms", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/
			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_BeanPromps", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_BeanPromps", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}
	
	/**
	 * Convertir un XML de MENSAJE_ERROR a BeanMensajesERROR
	 * 
	 * @param codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanMensajesError unmarshallMensajesError(String codServicio) {

		/** INICIO EVENTO - INICIO DE MoDULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("codServicio", codServicio));
		log.initModuleProcess("UNMARSHALL_Proms", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MoDULO **/

		BeanMensajesError bean = null;
		try {

//			UtilidadesBeans util = new UtilidadesBeans(log);
//
//			// ruta comun, ruta generica y prefijo para el fichero
//			String path = util.leeParametroUnmarshall("PATH_XML");
//			String pathModulos = util.leeParametroUnmarshall("PATH_XML_MODULO");
//			String prefixMod = util.leeParametroUnmarshall("PREFIX_XML_MODULO");
//			String middleMod = util.leeParametroUnmarshall("MIDDLE_XML_MODULO");
//			String sufixMod = "MAU-RESPUESTAS";
//			// nombre del fichero
//			String fileName = path + pathModulos + prefixMod + codServicio + middleMod + sufixMod + ".xml";
//
			UtilidadesBeans util = new UtilidadesBeans(log);

			// ruta comun, ruta generica y prefijo para el fichero
			String path = util.leeParametroUnmarshall("PATH_XML");
			String pathGestCont = util.leeParametroUnmarshall("PATH_XML_SHORT_CONT");
			String prefixGestCont = util.leeParametroUnmarshall("PREFIX_XML_SHORT_CONT");
			String sufixMod = "-MENSAJES_ERROR";
//			String path = "W:/Desarrollo";
//			String pathGestCont = "<cod_servivr>/shortcuts/";
//			String prefixGestCont = "";
//			String sufixGestCont = "-GESTION-SHORTCUT";
			// nombre del fichero
			String fileName = path + pathGestCont + prefixGestCont + codServicio + sufixMod + ".xml";

//
			if (fileName.contains("<cod_servivr>")) {
				fileName = fileName.replace("<cod_servivr>", codServicio);
			}

			File file = new File(fileName);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.idServicio = codServicio;
			this.idArchivo = file.getName();
			this.rutaCompleta = file.getPath().substring(0, file.getPath().lastIndexOf(File.separator) + 1);
			// ****************************************************************
			// [20161208-NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			JAXBContext jaxbContext = JAXBContext.newInstance(BeanMensajesError.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			bean = (BeanMensajesError) jaxbUnmarshaller.unmarshal(file);

			if (bean == null) {
				throw new Exception("La gestion de controladores se ha recuperado a null");
			}

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("BeanProms", bean.toString()));
			log.endModuleProcess("UNMARSHALL_GESTION_CONTROLADORES", "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN DE MoDULO **/
			return bean;

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "UNMARSHALL_BEANMENSAJEERROR", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			log.endModuleProcess("UNMARSHALL_BEANMENSAJEERROR", "KO", null, null);
			/** FIN EVENTO - FIN DE MoDULO **/

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_ENRUTADOR + this.idServicio);
			this.logWarning.warningCFG(idServicio, idElemento, this.idArchivo, "fil", null);
			// e.toString().replaceAll("\n", "")
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************

			return null;
		}
	}
	
}
