package com.kranon.bean.transfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "descTransfer", "controlHorario", "ctiActivo", "codAppCti", "destinoTransfer", "modoReproduccion", "wavTransferOutHour" })
public class BeanConfigTransfer {

	/* Descripcion de la transferencia */
	@XmlElement(name = "desc_transfer")
	private String descTransfer;

	/* Control horario */
	@XmlElement(name = "control_horario_transfer")
	private String controlHorario;

	/* Activacion de cti */
	@XmlElement(name = "cti_activo")
	private String ctiActivo;

	/* Codigo app cti */
	@XmlElement(name = "cod_app_cti")
	private String codAppCti;

	/* Destino de la transferencia */
	@XmlElement(name = "destino_transfer")
	private String destinoTransfer;

	/* Modo reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locucion fuera de horario */
	@XmlElement(name = "wav_transfer_out_hour")
	private String wavTransferOutHour;

	public String getDescTransfer() {
		return descTransfer;
	}

	public void setDescTransfer(String descTransfer) {
		this.descTransfer = descTransfer;
	}

	public String getControlHorario() {
		return controlHorario;
	}

	public void setControlHorario(String controlHorario) {
		this.controlHorario = controlHorario;
	}

	public String getCtiActivo() {
		return ctiActivo;
	}

	public void setCtiActivo(String ctiActivo) {
		this.ctiActivo = ctiActivo;
	}

	public String getCodAppCti() {
		return codAppCti;
	}

	public void setCodAppCti(String codAppCti) {
		this.codAppCti = codAppCti;
	}

	public String getDestinoTransfer() {
		return destinoTransfer;
	}

	public void setDestinoTransfer(String destinoTransfer) {
		this.destinoTransfer = destinoTransfer;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavTransferOutHour() {
		return wavTransferOutHour;
	}

	public void setWavTransferOutHour(String wavTransferOutHour) {
		this.wavTransferOutHour = wavTransferOutHour;
	}

	@Override
	public String toString() {
		return "BeanConfigTransfer [descTransfer=" + descTransfer + ", controlHorario=" + controlHorario + ", ctiActivo=" + ctiActivo
				+ ", codAppCti=" + codAppCti + ", destinoTransfer=" + destinoTransfer + ", modoReproduccion=" + modoReproduccion
				+ ", wavTransferOutHour=" + wavTransferOutHour + "]";
	}

}
