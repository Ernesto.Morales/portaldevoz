package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.BeanGramatica;

/**
 * Bean para la configuracion DTMF de una opcion de un Menu
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "gramaticaDtmf", "promptIntroDtmf", "promptDtmf", "promptOutroDtmf" })
public class BeanConfigOpcDtmfMenu {

	/* Gramatica dtmf */
	@XmlElement(name = "grammar_dtmf")
	private BeanGramatica gramaticaDtmf;

	/* Prompt en primer lugar */
	@XmlElement(name = "prompt_intro_dtmf")
	private String promptIntroDtmf;

	/* Prompt en segundo lugar */
	@XmlElement(name = "prompt_dtmf")
	private String promptDtmf;

	/* Prompt en tercer lugar */
	@XmlElement(name = "prompt_outro_dtmf")
	private String promptOutroDtmf;

	public BeanGramatica getGramaticaDtmf() {
		return gramaticaDtmf;
	}

	public void setGramaticaDtmf(BeanGramatica gramaticaDtmf) {
		this.gramaticaDtmf = gramaticaDtmf;
	}

	public String getPromptIntroDtmf() {
		return promptIntroDtmf;
	}

	public void setPromptIntroDtmf(String promptIntroDtmf) {
		this.promptIntroDtmf = promptIntroDtmf;
	}

	public String getPromptDtmf() {
		return promptDtmf;
	}

	public void setPromptDtmf(String promptDtmf) {
		this.promptDtmf = promptDtmf;
	}

	public String getPromptOutroDtmf() {
		return promptOutroDtmf;
	}

	public void setPromptOutroDtmf(String promptOutroDtmf) {
		this.promptOutroDtmf = promptOutroDtmf;
	}

	@Override
	public String toString() {
		return "BeanConfigOpcDtmfMenu [gramaticaDtmf=" + gramaticaDtmf + ", promptIntroDtmf=" + promptIntroDtmf + ", promptDtmf=" + promptDtmf
				+ ", promptOutroDtmf=" + promptOutroDtmf + "]";
	}

}
