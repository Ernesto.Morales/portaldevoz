package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.BeanModificacion;

/**
 * Bean para la configuracion de una Promocion de un Servicio
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "descPromo", "promoActivo", "codAccion", "modoReproduccion", "wavPromo", "codTransferLabel" })
public class BeanConfigPromoServicio {

	/* Descripcion de la promocion */
	@XmlElement(name = "desc_promo")
	private String descPromo;

	/* Activacion de la promocion */
	@XmlElement(name = "promo_activo")
	private String promoActivo;

	/* Tipo de accion a realizar */
	@XmlElement(name = "cod_accion")
	private String codAccion;

	/* Modo de reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locucion de la promocion */
	@XmlElement(name = "wav_promo")
	private String wavPromo;

	/* Codigo de la parametrizacion de transferencia */
	@XmlElement(name = "cod_transfer_label")
	private String codTransferLabel;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getDescPromo() {
		return descPromo;
	}

	public void setDescPromo(String descPromo) {
		this.descPromo = descPromo;
	}

	public String getPromoActivo() {
		return promoActivo;
	}

	public void setPromoActivo(String promoActivo) {
		this.promoActivo = promoActivo;
	}

	public String getCodAccion() {
		return codAccion;
	}

	public void setCodAccion(String codAccion) {
		this.codAccion = codAccion;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavPromo() {
		return wavPromo;
	}

	public void setWavPromo(String wavPromo) {
		this.wavPromo = wavPromo;
	}

	public String getCodTransferLabel() {
		return codTransferLabel;
	}

	public void setCodTransferLabel(String codTransferLabel) {
		this.codTransferLabel = codTransferLabel;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanConfigPromoServicio [descPromo=" + descPromo + ", promoActivo=" + promoActivo + ", codAccion=" + codAccion
				+ ", modoReproduccion=" + modoReproduccion + ", wavPromo=" + wavPromo + ", codTransferLabel=" + codTransferLabel + ", modif=" + modif
				+ "]";
	}

}
