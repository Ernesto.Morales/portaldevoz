package com.kranon.bean.gestionmodulos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.BeanProcesoModulo;
import com.kranon.bean.configtransfer.BeanConfigTransferencias;

@XmlRootElement(name = "modulo")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "activo", "rama", "tipoSalida", "ruta", "menu", "locuciones", "transferencias", "identificacion", "autenticacion",
		"estadistica" })
public class BeanInfoModulo {

	/* Nombre del modulo */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Activacion del modulo */
	@XmlElement(name = "activo")
	private String activo;

	/* Rama permitida del modulo */
	@XmlElement(name = "rama")
	private String rama;

	/* Tipo de modulo */
	@XmlElement(name = "tipo_salida")
	private String tipoSalida;

	/* Ruta inicio del modulo */
	@XmlElement(name = "ruta")
	private String ruta;

	/* Nombre del menu para desambiguar si el tipo salida es DESAMBIGUAR */
	@XmlElement(name = "menu")
	private String menu;

	/* Locuciones en caso de tipoSalida=LOCUCION */
	@XmlElement(name = "locuciones")
	private BeanLocucionesGestionModulo locuciones;

	/* Configuracion de Transferencias */
	@XmlElement(name = "transferencias")
	private BeanConfigTransferencias transferencias;

	/* Proceso de Identificacion */
	@XmlElement(name = "identificacion")
	private BeanProcesoModulo identificacion;

	/* Proceso de Autenticacion */
	@XmlElement(name = "autenticacion")
	private BeanProcesoModulo autenticacion;

	/* Estadistica del del modulo */
	@XmlElement(name = "estadistica")
	private String estadistica;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getRama() {
		return rama;
	}

	public void setRama(String rama) {
		this.rama = rama;
	}

	public String getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(String tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public BeanConfigTransferencias getTransferencias() {
		return transferencias;
	}

	public void setTransferencias(BeanConfigTransferencias transferencias) {
		this.transferencias = transferencias;
	}

	public BeanProcesoModulo getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(BeanProcesoModulo identificacion) {
		this.identificacion = identificacion;
	}

	public BeanProcesoModulo getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(BeanProcesoModulo autenticacion) {
		this.autenticacion = autenticacion;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	public BeanLocucionesGestionModulo getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocucionesGestionModulo locuciones) {
		this.locuciones = locuciones;
	}

	@Override
	public String toString() {
		return String
				.format("BeanInfoModulo [nombre=%s, activo=%s, rama=%s, tipoSalida=%s, ruta=%s, menu=%s, locuciones=%s, transferencias=%s, identificacion=%s, autenticacion=%s, estadistica=%s]",
						nombre, activo, rama, tipoSalida, ruta, menu, locuciones, transferencias, identificacion, autenticacion, estadistica);
	}

}
