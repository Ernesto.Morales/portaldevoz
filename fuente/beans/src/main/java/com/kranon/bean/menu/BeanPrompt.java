package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPrompt {

	/* Modo reproduccion del prompt */
	@XmlAttribute(name = "modo")
	private String modo;

	/* Value del prompt */
	@XmlValue
	private String value;

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanPrompt [modo=" + modo + ", value=" + value
				+ "]";
	}

}
