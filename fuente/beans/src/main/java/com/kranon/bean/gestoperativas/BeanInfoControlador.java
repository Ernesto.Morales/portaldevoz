package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "estado")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "activo", "value" })
public class BeanInfoControlador {

	@XmlAttribute(name = "activo")
	private String activo;
	
	/* Valor de la accion */
	@XmlValue
	private String value;
	
	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanInfoControlador [activo=" + activo + ", value=" + value
				+ "]";
	}

}
