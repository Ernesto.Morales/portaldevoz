package com.kranon.bean.enc;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.BeanModificacion;

/**
 * Bean para la configuracion de las Preguntas de una Encuesta. Se corresponde
 * con la tabla IVR_CFG_PREGUNTAS
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"modoReproduccion", "prompts", "numReintASR", "numReintASR2DTMF", "numReintDTMF", "tipoRespuesta"})
public class BeanPreguntaEnc {

	/* Secuencial de la pregunta */
	@XmlAttribute(name = "secuencial")
	private String secuencial;

	/* Modo reproduccion de la pregunta */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locuciones de la pregunta */
	@XmlElement(name = "prompts")
	private BeanLocPregEnc[] prompts;

	/* Numero reintentos ASR */
	@XmlElement(name = "num_reint_asr")
	private String numReintASR;

	/* Numero reintentos ASR a DTMF */
	@XmlElement(name = "num_reint_asr_to_dtmf")
	private String numReintASR2DTMF;

	/* Numero reintentos DTMF */
	@XmlElement(name = "num_reint_dtmf")
	private String numReintDTMF;

	/* Tipo de la respuesta */
	@XmlElement(name = "tipo_respuesta")
	private String tipoRespuesta;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getNumReintASR() {
		return numReintASR;
	}

	public void setNumReintASR(String numReintASR) {
		this.numReintASR = numReintASR;
	}

	public String getNumReintASR2DTMF() {
		return numReintASR2DTMF;
	}

	public void setNumReintASR2DTMF(String numReintASR2DTMF) {
		this.numReintASR2DTMF = numReintASR2DTMF;
	}

	public String getNumReintDTMF() {
		return numReintDTMF;
	}

	public void setNumReintDTMF(String numReintDTMF) {
		this.numReintDTMF = numReintDTMF;
	}

	public String getTipoRespuesta() {
		return tipoRespuesta;
	}

	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	public BeanLocPregEnc[] getPrompts() {
		return prompts;
	}

	public void setPrompts(BeanLocPregEnc[] prompts) {
		this.prompts = prompts;
	}

	@Override
	public String toString() {
		return "BeanPreguntaEnc [secuencial=" + secuencial
				+ ", modoReproduccion=" + modoReproduccion + ", prompts="
				+ Arrays.toString(prompts) + ", numReintASR=" + numReintASR
				+ ", numReintASR2DTMF=" + numReintASR2DTMF + ", numReintDTMF="
				+ numReintDTMF + ", tipoRespuesta=" + tipoRespuesta
				+ ", modif=" + modif + "]";
	}

}
