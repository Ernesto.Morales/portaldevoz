package com.kranon.bean.gestioncontroladores;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.configtransfer.BeanConfigTransferencias;

@XmlRootElement(name = "controlador")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "activo", "ruta", "transferencias" })
public class BeanInfoControlador {

	/* Nombre del controlador */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Activacion del controlador */
	// @XmlElement(name = "activo")
	private String activo;

	/* Ruta inicio del controlador */
	// @XmlElement(name = "ruta")
	private String ruta;

	/* Configuracion de Transferencias */
	@XmlElement(name = "transferencias")
	private BeanConfigTransferencias transferencias;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public BeanConfigTransferencias getTransferencias() {
		return transferencias;
	}

	public void setTransferencias(BeanConfigTransferencias transferencias) {
		this.transferencias = transferencias;
	}

	@Override
	public String toString() {
		return String.format("BeanInfoControlador [nombre=%s, activo=%s, ruta=%s, transferencias=%s]", nombre, activo, ruta, transferencias);
	}

}
