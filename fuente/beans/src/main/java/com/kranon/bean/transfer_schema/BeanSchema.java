package com.kranon.bean.transfer_schema;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSchema {

	/* Codigo del esquema */
	@XmlAttribute(name = "cd_esquema")
	private String codEsquema;

	/* Array de segmentos dentro del esquema */
	@XmlElement(name = "segmento")
	private BeanSegmento[] segmentos;

	public String getCodEsquema() {
		return codEsquema;
	}

	public void setCodEsquema(String codEsquema) {
		this.codEsquema = codEsquema;
	}

	public BeanSegmento[] getSegmentos() {
		return segmentos;
	}

	public void setSegmentos(BeanSegmento[] segmentos) {
		this.segmentos = segmentos;
	}

	@Override
	public String toString() {
		return "BeanSchema [codEsquema=" + codEsquema + ", segmentos="
				+ Arrays.toString(segmentos) + "]";
	}

}
