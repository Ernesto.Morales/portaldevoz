package com.kranon.bean.plantillanotifications;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para la configuracion de las plantillas usadas en el WS Notifications
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "configuracionNotifications")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanConfiguracionNotifications {

	@XmlElement(name = "plantilla")
	private BeanPlantilla[] plantillas;

	public BeanPlantilla[] getPlantillas() {
		return plantillas;
	}

	public void setPlantillas(BeanPlantilla[] plantillas) {
		this.plantillas = plantillas;
	}

	@Override
	public String toString() {
		return String.format("BeanConfiguracionNotifications [plantillas=%s]", Arrays.toString(plantillas));
	}

	// *************************************//

	/**
	 * Obtiene los datos de una plantilla dado el nombre de un modulo
	 * 
	 * @param nombreModulo
	 * @return {@link BeanPlantilla}
	 */
	public BeanPlantilla getPlantilla(String nombreModulo) {
		BeanPlantilla plantilla = null;
		for (BeanPlantilla p : plantillas) {
			if (p.getModulo().equals(nombreModulo)) {
				plantilla = p;
			}
		}

		return plantilla;
	}
}
