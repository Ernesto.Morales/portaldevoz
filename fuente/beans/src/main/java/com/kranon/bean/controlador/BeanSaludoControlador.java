package com.kranon.bean.controlador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSaludoControlador {

	/* VDN de entrada para decidir el saludo */
	@XmlAttribute(name = "vdn")
	private String vdn;

	/* Tipo de Cliente al que se debe reproducir el saludo */
	@XmlAttribute(name = "tipoCliente")
	private String tipoCliente;

	/* Modo de reproduccion del wav */
	@XmlAttribute(name = "modo")
	private String modoReproduccion;

	/* Locucion a reproducir */
	@XmlValue
	private String value;

	public String getVdn() {
		return vdn;
	}

	public void setVdn(String vdn) {
		this.vdn = vdn;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanSaludoControlador [vdn=" + vdn + ", tipoCliente=" + tipoCliente + ", modoReproduccion=" + modoReproduccion + ", value=" + value
				+ "]";
	}

}
