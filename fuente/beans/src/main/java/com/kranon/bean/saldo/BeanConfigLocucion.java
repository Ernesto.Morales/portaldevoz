package com.kranon.bean.saldo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de una Locucion de Saldo
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "descObj", "modoReproduccion", "wavMsgInvers" })
public class BeanConfigLocucion {

	/* Descripcion del objeto */
	@XmlElement(name = "desc_obj")
	private String descObj;

	/* Modo Reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locucion */
	@XmlElement(name = "wav_msg_invers")
	private String wavMsgInvers;

	public String getDescObj() {
		return descObj;
	}

	public void setDescObj(String descObj) {
		this.descObj = descObj;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavMsgInvers() {
		return wavMsgInvers;
	}

	public void setWavMsgInvers(String wavMsgInvers) {
		this.wavMsgInvers = wavMsgInvers;
	}

	@Override
	public String toString() {
		return "BeanConfigLocucion [descObj=" + descObj + ", modoReproduccion=" + modoReproduccion + ", wavMsgInvers=" + wavMsgInvers + "]";
	}

}
