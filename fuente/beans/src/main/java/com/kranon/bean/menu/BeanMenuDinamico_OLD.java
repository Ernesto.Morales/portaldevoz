package com.kranon.bean.menu;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kranon.bean.BeanParam;
import com.kranon.beanadapter.ArrayParamAdapter;


/**
 * Bean para la configuracion de un Menu Dinamico
 * Se corresponde con la tabla IVR_CFG_MENU_DINAMICO
 * 
 */
@XmlRootElement(name = "menu_dinamico")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMenuDinamico_OLD {

	/* Codigo del menu */
	@XmlAttribute(name = "cd_menu")
	private String codMenu;
	
	/* Parametros de configuracion */
	@XmlJavaTypeAdapter(ArrayParamAdapter.class)
	@XmlElement(name = "configuracion")
	private BeanParam[] configuracion;
	
	/* Opciones del menu */
	@XmlElementWrapper(name = "opciones")
	@XmlElement(name = "opcion")
	private BeanOpcionMenu[] opciones;	

	public String getCodMenu() {
		return codMenu;
	}

	public void setCodMenu(String codMenu) {
		this.codMenu = codMenu;
	}

	public BeanParam[] getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanParam[] configuracion) {
		this.configuracion = configuracion;
	}

	public BeanOpcionMenu[] getOpciones() {
		return opciones;
	}

	public void setOpciones(BeanOpcionMenu[] opciones) {
		this.opciones = opciones;
	}

	@Override
	public String toString() {
		return "BeanMenuDinamico [codMenu=" + codMenu + ", configuracion="
				+ Arrays.toString(configuracion) + ", opciones="
				+ Arrays.toString(opciones) + "]";
	}
		
	
}
