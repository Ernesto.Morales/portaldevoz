package com.kranon.bean.shortcut;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shorcut")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanShortCut {
	/* Nombre del controlador */
	@XmlAttribute(name = "nombre")
	private String nombre;
	@XmlElement(name = "ligaRetorno")
	private String ligaRetorno;
	@XmlElement(name = "tipo")
	private String tipo;
	@XmlElement(name = "servicio")
	private String servicio;
	@XmlElement(name = "activo")
	private String activo;
	private String exep="";
	public BeanShortCut() {
		super();
	}

	public BeanShortCut(String nombre, String ligaRetorno, String tipo,
			String servicio, String activo) {
		super();	
		this.nombre = nombre;
		this.ligaRetorno=ligaRetorno;
		this.tipo = tipo;
		this.servicio = servicio;
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getLigaRetorno() {
		return ligaRetorno;
	}
	public void setLigaRetorno(String ligaRetorno) {
		String[]auxs=ligaRetorno.split("\\|");
		if(auxs.length>1){
			this.ligaRetorno = auxs[0];
			this.exep=auxs[1];
		}else{
			this.ligaRetorno =this.ligaRetorno;
		}
	}

	public void setLigaRetornoL() {
		String[]auxs=this.ligaRetorno.split("\\|");
		if(auxs.length>1){
			this.ligaRetorno = auxs[0];
			this.exep=auxs[1];
		}else{
			this.ligaRetorno =this.ligaRetorno;
		}
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getServicio() {
		return servicio;
	}



	public void setServicio(String servicio) {
		this.servicio = servicio;
	}



	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getExep() {
		this.setLigaRetornoL();
		return exep;
	}

	public void setExep(String exep) {
		this.exep = exep;
	}

	@Override
	public String toString() {
		return "BeanShortCut [nombre=" + nombre + ", ligaRetorno="
				+ ligaRetorno + ", tipo=" + tipo + ", servicio=" + servicio
				+ ", activo=" + activo + ", exep=" + exep + "]";
	}

}
