package com.kranon.bean;

/**
 * Bean para el control de la ultima modificacion de cada registro de bbdd 
 * se corresponde con los campos usu_mod y timestamp_mod de cada tabla.
 * 
 */
public class BeanModificacion {

	/* Usuario ultima modificacion */
	private String usuarioModif;

	/* Fecha/Hora ultima modificacion */
	/* Formato recuperado como String 2016-1-21.9.49. 12. 281000000 */
	private String timestampModif;

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public String getTimestampModif() {
		return timestampModif;
	}

	public void setTimestampModif(String timestampModif) {
		this.timestampModif = timestampModif;
	}

	@Override
	public String toString() {
		return "BeanModificacion [usuarioModif=" + usuarioModif
				+ ", timestampModif=" + timestampModif + "]";
	}

}
