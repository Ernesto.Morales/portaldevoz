package com.kranon.bean.configtransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTransferGeneral {

	/* Activacion del segmento */
	@XmlAttribute(name = "segmento")
	private String segmento;

	/* Valor nuevo del segmento en caso de segmento=ON */
	@XmlValue
	private String value;

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("BeanTransferGeneral [segmento=%s, value=%s]", segmento, value);
	}

}
