package com.kranon.bean.excepserv;

import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.kranon.unmarshall.Unmarshall;

public class TestMarshallGestionExcepciones {

	public static void main(String[] args) {

		try {

			
			HashMap<String, BeanExcepcion> tablaExcepciones = new HashMap<String, BeanExcepcion>();
				BeanExcepcion valueNoActivo = new BeanExcepcion();
				valueNoActivo.setCodigoRetorno("TRANSFER");
				valueNoActivo.setEstadistica("500");
				valueNoActivo.setNombre("ERROR_IVR(SERV_IVR_NO_ACTIVO)");
				BeanPromptExcepcion promptNoActivo = new BeanPromptExcepcion();
				promptNoActivo.setModo("TTS");
				promptNoActivo.setValuePrompt("Ha habido un error i v r de no activo");
				valueNoActivo.setPrompt(promptNoActivo);
			tablaExcepciones.put("ERROR_IVR(SERV_IVR_NO_ACTIVO)", valueNoActivo);
			
			
			BeanExcepcion valueFH = new BeanExcepcion();
			valueFH.setCodigoRetorno("FIN");
			valueFH.setEstadistica("500");
			valueFH.setNombre("ERROR_IVR(SERV_IVR_FUERA_HORARIO)");
			BeanPromptExcepcion promptFH = new BeanPromptExcepcion();
			promptFH.setModo("TTS");
			promptFH.setValuePrompt("Ha habido un error fuera horario");
			valueFH.setPrompt(promptFH);
		tablaExcepciones.put("ERROR_IVR(SERV_IVR_FUERA_HORARIO)", valueFH);
			
			BeanGestionExcepciones gestionExcepciones = new BeanGestionExcepciones();
			gestionExcepciones.setCodServivr("PRESTAMOS");
			gestionExcepciones.setTablaExcepciones(tablaExcepciones);			
			
			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionExcepciones.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(gestionExcepciones, System.out);
			
			
			Unmarshall unmarshallGestionExcepciones = new Unmarshall("", "");
			BeanGestionExcepciones beanGestionExcepciones = unmarshallGestionExcepciones.unmarshallGestionExcepciones("PRESTAMOS");
			
//			System.out.println(beanGestionExcepciones.toString());
			
			jaxbMarshaller.marshal(beanGestionExcepciones, System.out);
			

			
//			System.out.println("FIN");
		} catch (Exception e) {
//			System.out.println("ERROR: " + e.toString());
		}

	}

}
