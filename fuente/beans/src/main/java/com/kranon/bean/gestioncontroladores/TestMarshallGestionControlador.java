package com.kranon.bean.gestioncontroladores;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

public class TestMarshallGestionControlador {

	public static void main(String[] args) {

		try {
		
			BeanConfigControlador[] configuracion = new BeanConfigControlador[2];
				BeanConfigControlador c0 = new BeanConfigControlador();
				c0.setNombreControlador("PATRIMONIAL");
				c0.setVdnOrigen("90281");
				c0.setXferId("");
				BeanConfigControlador c1 = new BeanConfigControlador();
				c1.setNombreControlador("PERSONAL");
				c1.setVdnOrigen("");
				c1.setXferId("00002");		
			configuracion[0] = c0;
			configuracion[1] = c1;
			
			BeanInfoControlador[] controladores = new BeanInfoControlador[2];
				BeanInfoControlador control0 = new BeanInfoControlador();
				control0.setNombre("PATRIMONIAL");
				control0.setActivo("ON");
				control0.setRuta("/.../INICIO-PARAMS.jsp");
				BeanInfoControlador control1 = new BeanInfoControlador();
				control1.setNombre("PERSONAL");
				control1.setActivo("OFF");
				control1.setRuta("/.../INICIO-PARAMS-2.jsp");
			controladores[0]=control0;
			controladores[1]=control1;
			
			BeanGestionControladores gestionControl = new BeanGestionControladores();
			gestionControl.setConfiguracion(configuracion);
			gestionControl.setControladores(controladores);
			
			
			JAXBContext jaxbContext = JAXBContext.newInstance(BeanGestionControladores.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(gestionControl, System.out);

		} catch (Exception e) {
//			System.out.println("ERROR: " + e.toString());
		}

	}

}
