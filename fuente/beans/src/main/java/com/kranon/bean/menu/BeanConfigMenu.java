package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion general de un Menu.
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "idioma", "menuActivo", "menuDesc", "modoInteraccion", "modoReproduccion", "numReintAsr", "numReintAsrToDtmf", "numReintDtmf", "respuestaSensible"})
public class BeanConfigMenu {

	/* Idioma del menu */
	@XmlElement(name = "idioma")
	private String idioma;

	/* Activacion del menu */
	@XmlElement(name = "menu_activo")
	private String menuActivo;

	/* Descripcion del menu */
	@XmlElement(name = "menu_desc")
	private String menuDesc;

	/* Modo Interaccion */
	@XmlElement(name = "modo_interaccion")
	private String modoInteraccion;

	/* Modo Reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Numero de intentos ASR */
	@XmlElement(name = "num_reint_asr")
	private String numReintAsr;

	/* Numero de intentos de ASR a DTMF - NO SE USA */
	@XmlElement(name = "num_reint_asr_to_dtmf")
	private String numReintAsrToDtmf;

	/* Numero de intentos DTMF */
	@XmlElement(name = "num_reint_dtmf")
	private String numReintDtmf;

	/* Respuesta dato sensible */
	@XmlElement(name = "respuesta_sensible")
	private String respuestaSensible;

	public String getMenuDesc() {
		return menuDesc;
	}

	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}

	public String getMenuActivo() {
		return menuActivo;
	}

	public void setMenuActivo(String menuActivo) {
		this.menuActivo = menuActivo;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getModoInteraccion() {
		return modoInteraccion;
	}

	public void setModoInteraccion(String modoInteraccion) {
		this.modoInteraccion = modoInteraccion;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getNumReintAsr() {
		return numReintAsr;
	}

	public void setNumReintAsr(String numReintAsr) {
		this.numReintAsr = numReintAsr;
	}

	public String getNumReintAsrToDtmf() {
		return numReintAsrToDtmf;
	}

	public void setNumReintAsrToDtmf(String numReintAsrToDtmf) {
		this.numReintAsrToDtmf = numReintAsrToDtmf;
	}

	public String getNumReintDtmf() {
		return numReintDtmf;
	}

	public void setNumReintDtmf(String numReintDtmf) {
		this.numReintDtmf = numReintDtmf;
	}

	public String getRespuestaSensible() {
		return respuestaSensible;
	}

	public void setRespuestaSensible(String respuestaSensible) {
		this.respuestaSensible = respuestaSensible;
	}

	@Override
	public String toString() {
		return String
				.format("BeanConfigMenu [idioma=%s, menuActivo=%s, menuDesc=%s, modoInteraccion=%s, modoReproduccion=%s, numReintAsr=%s, numReintAsrToDtmf=%s, numReintDtmf=%s, respuestaSensible=%s]",
						idioma, menuActivo, menuDesc, modoInteraccion, modoReproduccion, numReintAsr, numReintAsrToDtmf, numReintDtmf,
						respuestaSensible);
	}

}
