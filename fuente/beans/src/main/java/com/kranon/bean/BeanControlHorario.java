package com.kranon.bean;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "control_horario")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanControlHorario {

	@XmlElement(name = "horario")
	private BeanHorario[] horario;

	@XmlElement(name = "horarios")
	private BeanTipoHorario[] horarioParam;

	public BeanHorario[] getHorario() {
		return horario;
	}

	public void setHorario(BeanHorario[] horario) {
		this.horario = horario;
	}

	public BeanTipoHorario[] getHorarioParam() {
		return horarioParam;
	}

	public void setHorarioParam(BeanTipoHorario[] horarioParam) {
		this.horarioParam = horarioParam;
	}

	@Override
	public String toString() {
		return String.format("BeanNuevoHorario [horario=%s, horarioParam=%s]", Arrays.toString(horario), Arrays.toString(horarioParam));
	}

}
