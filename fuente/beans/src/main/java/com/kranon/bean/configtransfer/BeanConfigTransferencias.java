package com.kranon.bean.configtransfer;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanConfigTransferencias {

	/* Transferencia General */
	@XmlElement(name = "transfer")
	private BeanTransferGeneral transfer;

	/* Transferencia por defecto */
	@XmlElement(name = "transferDefault")
	private String transferDefault;

	// ** ABALFARO_20170118 Excepciones transferencias (Previsto para NO_CLIENTE)
	/* Excepciones Transferencias */
	@XmlElementWrapper(name = "transferExcepciones")
	@XmlElement(name = "transferExcep")
	private BeanTransferExcepcion[] transferExcepciones;

	public BeanTransferGeneral getTransfer() {
		return transfer;
	}

	public void setTransfer(BeanTransferGeneral transfer) {
		this.transfer = transfer;
	}

	public String getTransferDefault() {
		return transferDefault;
	}

	public void setTransferDefault(String transferDefault) {
		this.transferDefault = transferDefault;
	}

	public BeanTransferExcepcion[] getTransferExcepciones() {
		return transferExcepciones;
	}

	public void setTransferExcepciones(BeanTransferExcepcion[] transferExcepciones) {
		this.transferExcepciones = transferExcepciones;
	}

	@Override
	public String toString() {
		return String.format("BeanConfigTransferencias [transfer=%s, transferDefault=%s, transferExcepciones=%s]", transfer, transferDefault,
				Arrays.toString(transferExcepciones));
	}

	// ******************************************//

	/**
	 * Devuelve el segmento de la transferencia en caso de la excepcin dada
	 * 
	 * @param {@link String} nombreExcep
	 * @return {@link String} segmento de la transfe
	 */
	public String getExcepcionTransferByNombre(String nombreExcep) {

		if (transferExcepciones == null) {
			// no hay excepciones
			return null;
		}

		for (BeanTransferExcepcion excep : transferExcepciones) {
			if (excep.getNombre() != null && excep.getNombre().equalsIgnoreCase(nombreExcep)) {
				return excep.getValue();
			}
		}
		// si no encuentro la accion
		return null;
	}

}
