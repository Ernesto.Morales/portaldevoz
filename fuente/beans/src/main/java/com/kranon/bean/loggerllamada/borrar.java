package com.kranon.bean.loggerllamada;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.marshall.Marshall;

public class borrar {
	public static final LoggerLlamadaTemp loggerLlamadaTemp = new LoggerLlamadaTemp();
	public static final DatosLogerLlamada datosloggerLlamada=new DatosLogerLlamada();
	public static final Integer ENRUTADOR=1;
	public static final Integer SEGUROSOP=2;
	public static void main(String[] args) {
		datosloggerLlamada.setIdLlamada("1234567");
		datosloggerLlamada.setIdioma("mx_es");
		datosloggerLlamada.setIdElemento("123");
		datosloggerLlamada.setIdServicio("12");
		datosloggerLlamada.setInicio("inicio");
		datosloggerLlamada.setNumDestino("5534248635");
		datosloggerLlamada.setNumOrigen("55342456785");
		datosloggerLlamada.setTipo("APP");
		datosloggerLlamada.setTipoOpc("applb");
		loggerLlamadaTemp.getPasos().add(new Paso("nombre1","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre2","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre3","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre4","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre5","nombrefrom","codrespuesta","KO","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre6","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre7","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre8","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre9","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre10","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre11","nombrefrom","codrespuesta","OK","variables"));
		loggerLlamadaTemp.getPasos().add(new Paso("nombre12","nombrefrom","codrespuesta","OK","variables"));
		verificarOK();
		
		CommonLoggerProcess log = null;
		Marshall marshall= new Marshall(log);
		try {
			marshall.marshallDatosLoggerLamada(datosloggerLlamada,"SEGUROSOPV9");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void verificarOK( ){
		int contador=0;
		int contador2=0;
		int tamLinea=loggerLlamadaTemp.getPasos().size()-2;
		if(loggerLlamadaTemp.getPasos().size()<5){
			obtenerCadenaKo(0,loggerLlamadaTemp.getPasos().size());
		}
		if(loggerLlamadaTemp.getPasos().size()>=5){
			contador=verificarOKK(0,5);
			contador2=verificarOKK(5,tamLinea);
			if(contador==5) {		
				datosloggerLlamada.getPasos().add(obtenerCadenaOk(ENRUTADOR));
			}
			else{
				obtenerCadenaKo(0,5);
			}
			if(contador2==(tamLinea)) {		
				datosloggerLlamada.getPasos().add(obtenerCadenaOk(SEGUROSOP));
			}
			else{
				obtenerCadenaKo(5,tamLinea);
			}
			obtenerCadenaKo(loggerLlamadaTemp.getPasos().size()-2,loggerLlamadaTemp.getPasos().size());
		}
	}
	
	private static int verificarOKK(int inicio, int fin){
		int contador=inicio;
	
		 String pattern = "(.*)(VG_opc)(.*)";

	      // Create a Pattern object
	      Pattern r = Pattern.compile(pattern);

	     
	      
		for (int i = inicio; i < fin; i++) {
			Paso paso =loggerLlamadaTemp.getPasos().get(i);
			if(paso.getResultadoOperacion().equals("OK")){
			
				contador++;
			}
			 // Now create matcher object.
		      Matcher m = r.matcher(paso.getVariables());
		      if (m.find( )) {
				
			}
		}
		
		return contador;
	}
	public static Paso obtenerCadenaOk(int origen){
		if(origen==ENRUTADOR){
			return new Paso("ENRUTADOR","01","OK","OK","");
		}
		else if(origen==SEGUROSOP){
			return new Paso("SEGUROSOP","OK","OK","OPC","");
		}
//		else if(origen==ENRUTADOR2){
//			return new Paso("ENRUTADOR","03","OK","OK","");
//		}
//		else if(origen==ENRUTADOR3){
//			return new Paso("ENRUTADOR","04","OK","OK","");
//		}
		else
			return new Paso();
	}
	
	public static void obtenerCadenaKo(int inicio, int fin){
		for (int i = inicio; i < fin; i++) {
			Paso paso =loggerLlamadaTemp.getPasos().get(i);
			datosloggerLlamada.getPasos().add(paso);
		}
	}
}