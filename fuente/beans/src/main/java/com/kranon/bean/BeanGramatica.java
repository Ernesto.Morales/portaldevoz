package com.kranon.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "tipo", "tonosActivos", "minLength", "maxLength" })
public class BeanGramatica {

	/* Id de la gramatica */
	@XmlAttribute(name = "id")
	private int id;

	/* Tipo de gramatica */
	@XmlAttribute(name = "tipo")
	private String tipo;

	/* Tonos activos */
	@XmlAttribute(name = "tonosActivos")
	private String tonosActivos;

	/* Minima longitud */
	@XmlAttribute(name = "minLength")
	private int minLength;

	/* Maxima longitud */
	@XmlAttribute(name = "maxLength")
	private int maxLength;

	/* Gramatica */
	@XmlValue
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTonosActivos() {
		return tonosActivos;
	}

	public void setTonosActivos(String tonosActivos) {
		this.tonosActivos = tonosActivos;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanGramatica [id=" + id + ", tipo=" + tipo + ", tonosActivos=" + tonosActivos + ", minLength=" + minLength + ", maxLength="
				+ maxLength + ", value=" + value + "]";
	}

}
