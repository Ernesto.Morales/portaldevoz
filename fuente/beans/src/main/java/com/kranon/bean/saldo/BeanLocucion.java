package com.kranon.bean.saldo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

/**
 * Bean para cada tipo de Locucion de Saldo. Se corresponde con la tabla
 * IVR_CFG_LOC_SALDO
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanLocucion {

	/* Codigo de la locucion */
	@XmlAttribute(name = "cd_obj")
	private String codObjeto;

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigLocucion configuracion;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getCodObjeto() {
		return codObjeto;
	}

	public void setCodObjeto(String codObjeto) {
		this.codObjeto = codObjeto;
	}

	public BeanConfigLocucion getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigLocucion configuracion) {
		this.configuracion = configuracion;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanLocucion [codObjeto=" + codObjeto + ", configuracion=" + configuracion + ", modif=" + modif + "]";
	}

}
