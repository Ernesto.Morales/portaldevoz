package com.kranon.bean.transfer_schema;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSegmento {

	/* Codigo del segmento */
	@XmlAttribute(name = "cd_segmento")
	private String codSegmento;

	/* Array de productos dentro del segmento */
	@XmlElement(name = "producto")
	private BeanProducto[] productos;

	public String getCodSegmento() {
		return codSegmento;
	}

	public void setCodSegmento(String codSegmento) {
		this.codSegmento = codSegmento;
	}

	public BeanProducto[] getProductos() {
		return productos;
	}

	public void setProductos(BeanProducto[] productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		return "BeanSegmento [codSegmento=" + codSegmento + ", productos="
				+ Arrays.toString(productos) + "]";
	}

}
