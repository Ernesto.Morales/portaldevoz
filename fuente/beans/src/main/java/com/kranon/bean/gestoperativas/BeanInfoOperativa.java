package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "metadato")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "action", "type", "balance", "folio", "insurance", "data", "estadistica",
		"preProceso", "tipoProceso", "proceso", "postProceso" })
public class BeanInfoOperativa {

	@XmlAttribute(name = "action")
	private String action;
	
	@XmlAttribute(name = "type")
	private String type;
	
	@XmlAttribute(name = "balance")
	private String balance;
	
	@XmlAttribute(name = "folio")
	private String folio;
	
	@XmlAttribute(name = "insurance")
	private String insurance;
	
	@XmlAttribute(name = "data")
	private String data;
	
	@XmlAttribute(name = "estadistica")
	private String estadistica;
	
	@XmlElement(name = "preProceso")
	private BeanInfoPreProceso preProceso;
	
	@XmlElement(name = "tipoProceso")
	private String tipoProceso;
	
	@XmlElement(name = "proceso")
	private BeanInfoProceso proceso;
	
	@XmlElement(name = "postProceso")
	private BeanInfoPostProceso postProceso;
	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	

	public BeanInfoPreProceso getPreProceso() {
		return preProceso;
	}

	public void setPreProceso(BeanInfoPreProceso preProceso) {
		this.preProceso = preProceso;
	}

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	

	public BeanInfoProceso getProceso() {
		return proceso;
	}

	public void setProceso(BeanInfoProceso proceso) {
		this.proceso = proceso;
	}

	public BeanInfoPostProceso getPostProceso() {
		return postProceso;
	}

	public void setPostProceso(BeanInfoPostProceso postProceso) {
		this.postProceso = postProceso;
	}

	@Override
	public String toString() {
		return "BeanInfoMetadato [action=" + action + ", type=" + type
				+ ", balance=" + balance + ", folio=" + folio + ", insurance="
				+ insurance + ", data=" + data + ", estadistica=" + estadistica
				+ ", preProceso=" + preProceso + ", tipoProceso=" + tipoProceso
				+ ", proceso=" + proceso + ", postProceso=" + postProceso + "]";
	}

	
}
