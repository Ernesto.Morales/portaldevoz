package com.kranon.bean.respuestas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanRespuesta {
	
	/* Salida de la gramatica */
	@XmlElement(name = "contestacion")
	private String contestacion;
	
	/* Codigo de la respuesta */
	@XmlElement(name = "codigo")
	private String codigo;

	public String getContestacion() {
		return contestacion;
	}

	public void setContestacion(String contestacion) {
		this.contestacion = contestacion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "BeanRespuesta [contestacion=" + contestacion + ", codigo="
				+ codigo + "]";
	}
	
}
