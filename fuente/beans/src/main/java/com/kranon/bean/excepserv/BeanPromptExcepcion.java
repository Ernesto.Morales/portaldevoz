package com.kranon.bean.excepserv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPromptExcepcion {

	/* Nombre de la excepcion */
	@XmlAttribute(name = "modo")
	private String modo;

	/* Descripcion de la opcion */
	@XmlValue
	private String valuePrompt;

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getValuePrompt() {
		return valuePrompt;
	}

	public void setValuePrompt(String valuePrompt) {
		this.valuePrompt = valuePrompt;
	}

	@Override
	public String toString() {
		return "BeanPromptExcepcion [modo=" + modo + ", valuePrompt=" + valuePrompt + "]";
	}

}
