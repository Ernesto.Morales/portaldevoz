package com.kranon.bean.webservices;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para la configuracion de Web Services
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "servicios")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanWebServices {

	/* Timeout de la peticion */
	@XmlElement(name = "timeout")
	private int timeout;

	/* Entorno */
	@XmlElement(name = "entorno")
	private BeanEntorno[] entornos;

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public BeanEntorno[] getEntornos() {
		return entornos;
	}

	public void setEntornos(BeanEntorno[] entornos) {
		this.entornos = entornos;
	}

	@Override
	public String toString() {
		return "BeanWebServices [timeout=" + timeout + ", entorno=" + Arrays.toString(entornos) + "]";
	}
	
	/******/
	
	public BeanEntorno getDatosEntorno(String nombreEntorno){
		for(BeanEntorno entorno: entornos){
			if(entorno.getNombreEntorno().equalsIgnoreCase(nombreEntorno)){
				// es el entorno que buscamos
				return entorno;
			}
		}
		return null;
	}

}
