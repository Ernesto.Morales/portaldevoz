package com.kranon.bean.maurespuestas;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "mau_respuestas")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanRespuestasMau {
	/* Configuracion de los shorcut appLineaBancomer */
	@XmlElementWrapper(name = "respuestas")
	@XmlElement(name = "respuesta")
	private List<BeanMauRespuesta> respuesta;
	
	public BeanRespuestasMau() {
		super();
	}

	public BeanRespuestasMau(List<BeanMauRespuesta> respuesta) {
		super();
		this.respuesta = respuesta;
	}

	public List<BeanMauRespuesta> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(List<BeanMauRespuesta> respuesta) {
		this.respuesta = respuesta;
	}

	@Override
	public String toString() {
		return "BeanRespuestasMau [respuesta=" + respuesta + "]";
	}
	
}
