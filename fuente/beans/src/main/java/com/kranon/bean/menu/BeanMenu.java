package com.kranon.bean.menu;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para la configuracion de un Menu Dinamico Se corresponde con la tabla IVR_CFG_MENU_DINAMICO
 * 
 */
@XmlRootElement(name = "menu")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMenu {

	/* Codigo del menu */
	@XmlAttribute(name = "cd_menu")
	private String codMenu;

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigMenu configuracion;

	/* Configuracion del reconocimiento ASR */
	@XmlElement(name = "asr")
	private BeanConfigAsr configAsr;

	/* Configuracion del reconocimiento DTMF */
	@XmlElement(name = "dtmf")
	private BeanConfigDtmf configDtmf;

	/* Opciones del menu */
	@XmlElementWrapper(name = "opciones")
	@XmlElement(name = "opcion")
	private BeanOpcionMenu[] opciones;

	/* Excepciones del menu */
	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanExcepcionMenu[] excepciones;

	public String getCodMenu() {
		return codMenu;
	}

	public void setCodMenu(String codMenu) {
		this.codMenu = codMenu;
	}

	public BeanConfigMenu getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigMenu configuracion) {
		this.configuracion = configuracion;
	}

	public BeanConfigAsr getConfigAsr() {
		return configAsr;
	}

	public void setConfigAsr(BeanConfigAsr configAsr) {
		this.configAsr = configAsr;
	}

	public BeanConfigDtmf getConfigDtmf() {
		return configDtmf;
	}

	public void setConfigDtmf(BeanConfigDtmf configDtmf) {
		this.configDtmf = configDtmf;
	}

	public BeanOpcionMenu[] getOpciones() {
		return opciones;
	}

	public void setOpciones(BeanOpcionMenu[] opciones) {
		this.opciones = opciones;
	}

	public BeanExcepcionMenu[] getExcepciones() {
		return excepciones;
	}

	public void setExcepciones(BeanExcepcionMenu[] excepciones) {
		this.excepciones = excepciones;
	}

	@Override
	public String toString() {
		return String.format("BeanMenu [codMenu=%s, configuracion=%s, configAsr=%s, configDtmf=%s, opciones=%s, excepciones=%s]", codMenu,
				configuracion, configAsr, configDtmf, Arrays.toString(opciones), Arrays.toString(excepciones));
	}

}
