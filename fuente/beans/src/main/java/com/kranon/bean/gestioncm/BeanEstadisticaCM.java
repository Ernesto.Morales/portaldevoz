package com.kranon.bean.gestioncm;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.kranon.beanadapter.MapAdapterStatContador;

/**
 * Bean para los datos de a estadistica en el cuadro de mando
 * 
 */
@XmlRootElement(name = "estadistica")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nombre", "activo", "operacion", "tablaContadores" })
public class BeanEstadisticaCM {

	/* Nombre de la Estadistica */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Activacion de la Estadistica */
	@XmlAttribute(name = "activo")
	private String activo;

	/* Codigo de operacion */
	@XmlElement(name = "operacion")
	private String operacion;

	@XmlJavaTypeAdapter(MapAdapterStatContador.class)
	@XmlElement(name = "contadores")
	private HashMap<BeanInfoContadorCM, String> tablaContadores;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public HashMap<BeanInfoContadorCM, String> getTablaContadores() {
		return tablaContadores;
	}

	public void setTablaContadores(HashMap<BeanInfoContadorCM, String> tablaContadores) {
		this.tablaContadores = tablaContadores;
	}

	@Override
	public String toString() {
		return String
				.format("BeanEstadisticaCM [nombre=%s, activo=%s, operacion=%s, tablaContadores=%s]", nombre, activo, operacion, tablaContadores);
	}

	// *********************************************************************************//

	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public boolean equals(Object obj) {
		// dos estadisticas son iguales si tienen el mismo nombre

		if (obj instanceof BeanEstadisticaCM) {
			BeanEstadisticaCM other = (BeanEstadisticaCM) obj;
			EqualsBuilder builder = new EqualsBuilder();
			builder.append(this.nombre, other.nombre);
			return builder.isEquals();
		}
		return false;
	}

	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(nombre);
		return builder.toHashCode();
	}

}
