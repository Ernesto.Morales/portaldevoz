package com.kranon.bean.saldo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para cada tipo de Locucion de Saldo. Se corresponde con la tabla
 * IVR_CFG_LOC_SALDO
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTipoLocucion {

	/* Tipo de la locucion del servicio */
	@XmlAttribute(name = "tipo")
	private String tipo;

	/* Lista de locuciones */
	@XmlElement(name = "loc")
	private BeanLocucion[] locuciones;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BeanLocucion[] getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocucion[] locuciones) {
		this.locuciones = locuciones;
	}

	@Override
	public String toString() {
		return "BeanTipoLocucion [tipo=" + tipo + ", locuciones="
				+ Arrays.toString(locuciones) + "]";
	}

}
