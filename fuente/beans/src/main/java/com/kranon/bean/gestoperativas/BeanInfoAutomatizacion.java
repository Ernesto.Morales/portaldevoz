package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "automatizacion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nombre", "activo", "ruta", "identificacion", "autenticacion", "proactivo",
		"accion", "excepcion", "estadistica"})
public class BeanInfoAutomatizacion {

	/* Nombre del modulo */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Activacion del modulo */
	@XmlAttribute(name = "activo")
	private String activo;
	
	/* Ruta del modulo */
	@XmlElement(name = "ruta")
	private String ruta;
	
	/* Proceso de Identificacion */
	@XmlElement(name = "identificacion")
	private BeanProcesoOperativa identificacion;

	/* Proceso de Autenticacion */
	@XmlElement(name = "autenticacion")
	private BeanProcesoOperativa autenticacion;
	
	@XmlElement(name = "proactivo")
	private String proactivo;
	
	@XmlElement(name = "accion")
	private String accion;
	
	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanInfoExcepcion[] excepcion;
	

	/* Nombre estadistica del modulo */
	@XmlAttribute(name = "estadistica")
	private String estadistica;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public BeanProcesoOperativa getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(BeanProcesoOperativa identificacion) {
		this.identificacion = identificacion;
	}

	public BeanProcesoOperativa getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(BeanProcesoOperativa autenticacion) {
		this.autenticacion = autenticacion;
	}

	
	public String getProactivo() {
		return proactivo;
	}

	public void setProactivo(String proactivo) {
		this.proactivo = proactivo;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public BeanInfoExcepcion[] getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(BeanInfoExcepcion[] excepcion) {
		this.excepcion = excepcion;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	@Override
	public String toString() {
		return "BeanInfoAutomatizacion [nombre=" + nombre + ", activo="
				+ activo + ", ruta=" + ruta + ", identificacion="
				+ identificacion + ", autenticacion=" + autenticacion
				+ ", proactivo=" + proactivo + ", accion=" + accion
				+ ", excepcion=" + Arrays.toString(excepcion)
				+ ", estadistica=" + estadistica + "]";
	}

}
