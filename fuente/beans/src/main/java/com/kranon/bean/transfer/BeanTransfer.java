package com.kranon.bean.transfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.BeanControlHorario;

/**
 * Bean para la configuracion de una Transferencia. Se corresponde con la tabla TGCE011_TRANSFER
 * 
 */
@XmlRootElement(name = "transferencia")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTransfer {

	/* Codigo de la transferencia */
	@XmlAttribute(name = "cd_transfer")
	private String codTransfer;

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigTransfer configuracion;

	/* Horarios de la transferencia */
	// @XmlElementWrapper(name = "control_horario")
	// @XmlElement(name = "horario")
	// private BeanHorario[] horario;

	/* Horarios de la transferencia */
	// @XmlElementWrapper(name = "control_horario")
	@XmlElement(name = "control_horario")
	private BeanControlHorario horario;

	public String getCodTransfer() {
		return codTransfer;
	}

	public void setCodTransfer(String codTransfer) {
		this.codTransfer = codTransfer;
	}

	public BeanConfigTransfer getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigTransfer configuracion) {
		this.configuracion = configuracion;
	}

	public BeanControlHorario getHorario() {
		return horario;
	}

	public void setHorario(BeanControlHorario horario) {
		this.horario = horario;
	}

	@Override
	public String toString() {
		return String.format("BeanTransfer [codTransfer=%s, configuracion=%s, horario=%s]", codTransfer, configuracion, horario);
	}

}
