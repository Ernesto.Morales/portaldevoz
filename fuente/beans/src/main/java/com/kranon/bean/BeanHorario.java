package com.kranon.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para el control de horario. Se corresponde con la tabla
 * IVR_CFG_HORARIO_SVC
 * 
 */
@XmlRootElement(name = "horario")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"tipoHorario", "valorHorario", "franja1", "franja2", "franja3"})
public class BeanHorario {

	/* Tipo del horario. D: dia de la semana, F_ fecha concreta */
	@XmlAttribute(name = "tipo")
	private String tipoHorario;

	/* Valor del tipo de horario */
	@XmlAttribute(name = "valor")
	private String valorHorario;

	/* Franja 1 de horario con formato HH:MM-HH:MM o CLOSED */
	@XmlElement(name = "franja1")
	private String franja1;

	/* Franja 2 de horario con formato HH:MM-HH:MM o CLOSED */
	@XmlElement(name = "franja2")
	private String franja2;

	/* Franja 3 de horario con formato HH:MM-HH:MM o CLOSED */
	@XmlElement(name = "franja3")
	private String franja3;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getTipoHorario() {
		return tipoHorario;
	}

	public void setTipoHorario(String tipoHorario) {
		this.tipoHorario = tipoHorario;
	}

	public String getValorHorario() {
		return valorHorario;
	}

	public void setValorHorario(String valorHorario) {
		this.valorHorario = valorHorario;
	}

	public String getFranja1() {
		return franja1;
	}

	public void setFranja1(String franja1) {
		this.franja1 = franja1;
	}

	public String getFranja2() {
		return franja2;
	}

	public void setFranja2(String franja2) {
		this.franja2 = franja2;
	}

	public String getFranja3() {
		return franja3;
	}

	public void setFranja3(String franja3) {
		this.franja3 = franja3;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanHorario [tipoHorario=" + tipoHorario + ", valorHorario="
				+ valorHorario + ", franja1=" + franja1 + ", franja2="
				+ franja2 + ", franja3=" + franja3 + ", modif=" + modif + "]";
	}

}
