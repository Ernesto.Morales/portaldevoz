package com.kranon.bean.stat.llamada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "operacion")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanStatInfoOperacion {

	@XmlElement(name = "cod_operivr")
	private String codOperIVR;

	@XmlElement(name = "cod_rslivr")
	private String codRslIVR;
	
	//@XmlElement(name = "flag_rslivr")
	@XmlTransient
	private String flagRslIVR;

	@XmlElement(name = "inicioOperacion")
	private String inicioOperacion;

	@XmlTransient
	private String inicioOperacionMilis;

	@XmlElement(name = "finOperacion")
	private String finOperacion;

	@XmlTransient
	private String finOperacionMilis;

	public String getCodOperIVR() {
		return codOperIVR;
	}

	public void setCodOperIVR(String codOperIVR) {
		this.codOperIVR = codOperIVR;
	}

	public String getCodRslIVR() {
		return codRslIVR;
	}

	public void setCodRslIVR(String codRslIVR) {
		this.codRslIVR = codRslIVR;
	}

	public String getFlagRslIVR() {
		return flagRslIVR;
	}

	public void setFlagRslIVR(String flagRslIVR) {
		this.flagRslIVR = flagRslIVR;
	}

	public String getInicioOperacion() {
		return inicioOperacion;
	}

	public void setInicioOperacion(String inicioOperacion) {
		this.inicioOperacion = inicioOperacion;
	}

	public String getFinOperacion() {
		return finOperacion;
	}

	public void setFinOperacion(String finOperacion) {
		this.finOperacion = finOperacion;
	}

	public String getInicioOperacionMilis() {
		return inicioOperacionMilis;
	}

	public void setInicioOperacionMilis(String inicioOperacionMilis) {
		this.inicioOperacionMilis = inicioOperacionMilis;
	}

	public String getFinOperacionMilis() {
		return finOperacionMilis;
	}

	public void setFinOperacionMilis(String finOperacionMilis) {
		this.finOperacionMilis = finOperacionMilis;
	}

	@Override
	public String toString() {
		return "BeanStatInfoOperacion [codOperIVR=" + codOperIVR
				+ ", codRslIVR=" + codRslIVR + ", flagRslIVR=" + flagRslIVR
				+ ", inicioOperacion=" + inicioOperacion
				+ ", inicioOperacionMilis=" + inicioOperacionMilis
				+ ", finOperacion=" + finOperacion + ", finOperacionMilis="
				+ finOperacionMilis + "]";
	}

//	@Override
//	public String toString() {
//		return String
//				.format("BeanStatInfoOperacion [codOperIVR=%s, codRslIVR=%s, inicioOperacion=%s,finOperacion=%s]",
//						codOperIVR, codRslIVR, inicioOperacion,
//						 finOperacion);
//	}

}
