package com.kranon.bean.mensajeserror;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMensajeError {
	@XmlAttribute(name = "opcion")
	private String opcion;

	/* Modo de reproduccion del wav */
	@XmlAttribute(name = "modo")
	private String modoReproduccion;
	/* Locucion a reproducir */
	@XmlValue
	private String value;
	
	
	public BeanMensajeError() {
		super();
	}


	public BeanMensajeError(String opcion, String modoReproduccion, String value) {
		super();
		this.opcion = opcion;
		this.modoReproduccion = modoReproduccion;
		this.value = value;
	}


	public String getOpcion() {
		return opcion;
	}


	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}


	public String getModoReproduccion() {
		return modoReproduccion;
	}


	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String toString() {
		return "BeanMensaje [opcion=" + opcion + ", modoReproduccion="
				+ modoReproduccion + ", value=" + value + "]";
	}
		
}
