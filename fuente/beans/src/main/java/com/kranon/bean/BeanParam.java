package com.kranon.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Bean para los pares de datos de configuracion <clave, valor> y su
 * descripcion. Incluye tambien la ultima modificacion
 * 
 */
public class BeanParam {

	/* Clave del parametro */
	@XmlElement
	private String key;

	/* Valor del parametro */
	@XmlElementWrapper
	private String value;

	/* Descripcion del parametro */
	private String desc;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanParam [key=" + key + ", value=" + value + ", desc=" + desc
				+ ", modif=" + modif + "]";
	}

}
