package com.kranon.bean.respuestas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "respuestasPreguntas")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanRespuestasPreguntas {

	@XmlElement(name = "respuesta")
	private BeanRespuesta[] r;

	public BeanRespuesta[] getR() {
		return r;
	}

	public void setR(BeanRespuesta[] r) {
		this.r = r;
	}

	@Override
	public String toString() {
		return "BeanRespuestasPreguntas [r=" + Arrays.toString(r) + "]";
	}

	
}
