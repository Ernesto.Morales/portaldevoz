package com.kranon.bean;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "activo", "acciones", "instrumentos" })
public class BeanProcesoModulo {

	/* Activacion del proceso */
	@XmlAttribute(name = "activo")
	private String activo;

	/* Acciones del proceso */
	@XmlElementWrapper(name = "acciones")
	@XmlElement(name = "accion")
	private BeanAccionesProcesoModulo[] acciones;

	/* Instrumentos del proceso */
	@XmlElementWrapper(name = "instrumentos")
	@XmlElement(name = "instrumento")
	private BeanInstrumento[] instrumentos;

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public BeanAccionesProcesoModulo[] getAcciones() {
		return acciones;
	}

	public void setAcciones(BeanAccionesProcesoModulo[] acciones) {
		this.acciones = acciones;
	}

	public BeanInstrumento[] getInstrumentos() {
		return instrumentos;
	}

	public void setInstrumentos(BeanInstrumento[] instrumentos) {
		this.instrumentos = instrumentos;
	}

	@Override
	public String toString() {
		return "BeanProcesoModulo [activo=" + activo + ", acciones=" + Arrays.toString(acciones) + ", instrumentos=" + Arrays.toString(instrumentos)
				+ "]";
	}

	// ******************************************//

	/**
	 * Devuelve la accion a realizar por el nombre recogido como parametro
	 * 
	 * @param {@link String} nombreAccion
	 * @return {@link String} accion a realizar 
	 */
	public String getResultadoAccion(String nombreAccion) {

		if (acciones == null) {
			// no hay acciones
			return null;
		}

		for (BeanAccionesProcesoModulo acc : acciones) {
			if (acc.getNombre() != null && acc.getNombre().equalsIgnoreCase(nombreAccion)) {
				return acc.getValue();
			}
		}
		// si no encuentro la accion
		return null;

	}
	
	/**
	 * Devuelve todos los intentos de los instrumentos de activos ordenados por id en formato XXX|XXX|XXX
	 * Devuelvo null si algo ha fallado. 
	 * Devuelvo vacio si no hay instrumentos activos.
	 * 
	 * @return {@link String} intentos instrumentos activos separados por '|'
	 */
	public String getNumIntentosInstrumentosActivos() {
		
		if(instrumentos == null){
			// no hay instrumentos
			return "";
		}

		// primero ordeno todos los instrumentos por id
		BeanInstrumento[] insOrdenados = this.ordenarInstrumentossPorId(instrumentos);
		if(insOrdenados == null){
			// algo ha fallado al ordenar
			return null;
		}
		String insActivos = "";
		for (BeanInstrumento ins : instrumentos) {
			// recorro todos los intrumentos y me guardo los activos
			if (ins.getValue() != null && ins.getValue().equalsIgnoreCase("ON")) {
				if(!insActivos.equals("")){
					insActivos = insActivos + "|";
				}
				insActivos = insActivos + ins.getNumIntentos();
			}
		}
		return insActivos;

	}
	
	/**
	 * Devuelve todos los instrumentos de activos ordenados por id en formato XXX|XXX|XXX
	 * Devuelvo null si algo ha fallado. 
	 * Devuelvo vacio si no hay instrumentos activos.
	 * 
	 * @return {@link String} instrumentos activos separados por '|'
	 */
	public String getInstrumentosActivos() {
		
		if(instrumentos == null){
			// no hay instrumentos
			return "";
		}

		// primero ordeno todos los instrumentos por id
		BeanInstrumento[] insOrdenados = this.ordenarInstrumentossPorId(instrumentos);
		if(insOrdenados == null){
			// algo ha fallado al ordenar
			return null;
		}
		String insActivos = "";
		for (BeanInstrumento ins : instrumentos) {
			// recorro todos los intrumentos y me guardo los activos
			if (ins.getValue() != null && ins.getValue().equalsIgnoreCase("ON")) {
				if(!insActivos.equals("")){
					insActivos = insActivos + "|";
				}
				insActivos = insActivos + ins.getNombre();
			}
		}
		return insActivos;

	}
	
	/**
	 * Ordenar los instrumentos por id de menor a mayor
	 * 
	 * @param {@link BeanInstrumento[]} instrumentos
	 * @return {@link BeanInstrumento[]}
	 * @throws Exception
	 */
	private BeanInstrumento[] ordenarInstrumentossPorId(BeanInstrumento[] ins) {

		BeanInstrumento[] insOrdenados; 
		
		try {
			insOrdenados = ins;

			for (int i = 0; i < (insOrdenados.length - 1); i++) {
				for (int j = i + 1; j < insOrdenados.length; j++) {
					int posicionI = insOrdenados[i].getId();
					int posicionJ = insOrdenados[j].getId();
					if (posicionI > posicionJ) { 
						BeanInstrumento variableauxiliar = insOrdenados[i];
						insOrdenados[i] = insOrdenados[j];
						insOrdenados[j] = variableauxiliar;

					}
				}
			}
		} catch (Exception e) {
			return null;
		}

		return insOrdenados;
	}

}
