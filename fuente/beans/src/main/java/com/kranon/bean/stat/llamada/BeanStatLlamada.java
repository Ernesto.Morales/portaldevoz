package com.kranon.bean.stat.llamada;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "llamada")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanStatLlamada {

	@XmlElement(name = "cod_llamivr")
	private String codLlamIVR;

	@XmlElement(name = "cod_servivr")
	private String codServIVR;
	
	@XmlElement(name = "tim_entrada")
	private String timeEntrada;

	@XmlTransient
	private String timeEntradaMilis;

	@XmlElement(name = "tim_salida")
	private String timeSalida;

	@XmlTransient
	private String timeSalidaMilis;
	
	@XmlElement(name = "cod_ucid")
	private String codUcid;

	@XmlElement(name = "nb_numtfno")
	private String desNumtfno;
	
	@XmlElement(name = "cod_vdn")
	private String codVdn;
	
	@XmlElement(name = "nb_keyfront")
	private String codKeyfront;	
	
	@XmlElement(name = "cod_persctpn")
	private String codPersctpn;

	@XmlElement(name = "nb_segmento")
	private String desSegmento;	
	
	@XmlElement(name = "cod_sgmtxfer")
	private String codSgmtxfer;	
	
	@XmlElement(name = "xti_contitel")
	private String xtiContitel;	
	
	@XmlElement(name = "xti_tiplocut")
	private String xtiTiplocut;
	
	@XmlElement(name = "cod_entalfa")
	private String codEntalfa;

	@XmlElement(name = "operacion")
	private ArrayList<BeanStatInfoOperacion> operaciones;

	public String getCodLlamIVR() {
		return codLlamIVR;
	}

	public void setCodLlamIVR(String codLlamIVR) {
		this.codLlamIVR = codLlamIVR;
	}

	public String getCodServIVR() {
		return codServIVR;
	}

	public void setCodServIVR(String codServIVR) {
		this.codServIVR = codServIVR;
	}

	public String getTimeEntrada() {
		return timeEntrada;
	}

	public void setTimeEntrada(String timeEntrada) {
		this.timeEntrada = timeEntrada;
	}

	public String getTimeEntradaMilis() {
		return timeEntradaMilis;
	}

	public void setTimeEntradaMilis(String timeEntradaMilis) {
		this.timeEntradaMilis = timeEntradaMilis;
	}

	public String getTimeSalida() {
		return timeSalida;
	}

	public void setTimeSalida(String timeSalida) {
		this.timeSalida = timeSalida;
	}

	public String getTimeSalidaMilis() {
		return timeSalidaMilis;
	}

	public void setTimeSalidaMilis(String timeSalidaMilis) {
		this.timeSalidaMilis = timeSalidaMilis;
	}

	public String getCodUcid() {
		return codUcid;
	}

	public void setCodUcid(String codUcid) {
		this.codUcid = codUcid;
	}

	public String getCodVdn() {
		return codVdn;
	}

	public void setCodVdn(String codVdn) {
		this.codVdn = codVdn;
	}

	public String getXtiContitel() {
		return xtiContitel;
	}

	public void setXtiContitel(String xtiContitel) {
		this.xtiContitel = xtiContitel;
	}

	public String getXtiTiplocut() {
		return xtiTiplocut;
	}

	public void setXtiTiplocut(String xtiTiplocut) {
		this.xtiTiplocut = xtiTiplocut;
	}

	public String getCodPersctpn() {
		return codPersctpn;
	}

	public void setCodPersctpn(String codPersctpn) {
		this.codPersctpn = codPersctpn;
	}

	public String getCodSgmtxfer() {
		return codSgmtxfer;
	}

	public void setCodSgmtxfer(String codSgmtxfer) {
		this.codSgmtxfer = codSgmtxfer;
	}

	public String getDesSegmento() {
		return desSegmento;
	}

	public void setDesSegmento(String desSegmento) {
		this.desSegmento = desSegmento;
	}

	public String getCodKeyfront() {
		return codKeyfront;
	}

	public void setCodKeyfront(String codKeyfront) {
		this.codKeyfront = codKeyfront;
	}

	public String getDesNumtfno() {
		return desNumtfno;
	}

	public void setDesNumtfno(String desNumtfno) {
		this.desNumtfno = desNumtfno;
	}

	public String getCodEntalfa() {
		return codEntalfa;
	}

	public void setCodEntalfa(String codEntalfa) {
		this.codEntalfa = codEntalfa;
	}

	public ArrayList<BeanStatInfoOperacion> getOperaciones() {
		return operaciones;
	}

	public void setOperaciones(ArrayList<BeanStatInfoOperacion> operaciones) {
		this.operaciones = operaciones;
	}

	@Override
	public String toString() {
		return String
				.format("BeanStatLlamada [codLlamIVR=%s, codServIVR=%s, timeEntrada=%s, timeEntradaMilis=%s, timeSalida=%s, timeSalidaMilis=%s, codUcid=%s, codVdn=%s, xtiContitel=%s, xtiTiplocut=%s, codPersctpn=%s, codSgmtxfer=%s, desSegmento=%s, codKeyfront=%s, desNumtfno=%s, codEntalfa=%s, operaciones=%s]",
						codLlamIVR, codServIVR, timeEntrada, timeEntradaMilis,
						timeSalida, timeSalidaMilis, codUcid, codVdn,
						xtiContitel, xtiTiplocut, codPersctpn, codSgmtxfer,
						desSegmento, codKeyfront, desNumtfno, codEntalfa,
						operaciones);
	}

		


}
