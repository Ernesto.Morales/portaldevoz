package com.kranon.bean.enc;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanControlHorario;
import com.kranon.bean.BeanModificacion;

/**
 * Bean para la configuracion de una Encuesta. Se corresponde con la tabla IVR_CFG_ENCUESTAS
 * 
 */
@XmlRootElement(name = "encuesta")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanEncuesta {

	/* Codigo de la encuesta */
	@XmlAttribute(name = "cd_enc")
	private String codEncuesta;

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigEncuesta configuracion;

	/* Horarios de la encuesta */
	// @XmlElementWrapper(name = "control_horario")
	// @XmlElement(name = "horario")
	// private BeanHorario[] horario;

	/* Horarios de la encuesta */
	// @XmlElementWrapper(name = "control_horario")
	@XmlElement(name = "control_horario")
	private BeanControlHorario horario;

	/* Preguntas de la encuesta */
	@XmlElementWrapper(name = "preguntas")
	@XmlElement(name = "pregunta")
	private BeanPreguntaEnc[] preguntas;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getCodEncuesta() {
		return codEncuesta;
	}

	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}

	public BeanConfigEncuesta getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigEncuesta configuracion) {
		this.configuracion = configuracion;
	}


	public BeanModificacion getModif() {
		return modif;
	}

	public BeanControlHorario getHorario() {
		return horario;
	}

	public void setHorario(BeanControlHorario horario) {
		this.horario = horario;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	public BeanPreguntaEnc[] getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(BeanPreguntaEnc[] preguntas) {
		this.preguntas = preguntas;
	}

	@Override
	public String toString() {
		return String.format("BeanEncuesta [codEncuesta=%s, configuracion=%s, horario=%s, preguntas=%s, modif=%s]", codEncuesta, configuracion,
				horario, Arrays.toString(preguntas), modif);
	}

}
