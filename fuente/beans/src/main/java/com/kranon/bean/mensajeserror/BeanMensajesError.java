package com.kranon.bean.mensajeserror;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "mensajesError")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMensajesError {
	@XmlElementWrapper(name = "menErrores")
	@XmlElement(name = "menError")
	private List<BeanMensajeError> respuesta;

	public BeanMensajesError() {
		super();
	}

	public BeanMensajesError(List<BeanMensajeError> respuesta) {
		super();
		this.respuesta = respuesta;
	}

	public List<BeanMensajeError> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(List<BeanMensajeError> respuesta) {
		this.respuesta = respuesta;
	}

	@Override
	public String toString() {
		return "BeanMensajes [respuesta=" + respuesta + "]";
	}
	
}