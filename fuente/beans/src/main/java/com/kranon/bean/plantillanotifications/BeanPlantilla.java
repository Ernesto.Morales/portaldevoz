package com.kranon.bean.plantillanotifications;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "modulo", "template", "eventCode", "camposCabecera", "camposTabla" })
public class BeanPlantilla {

	@XmlAttribute(name = "modulo")
	private String modulo;

	@XmlAttribute(name = "template")
	private String template;

	@XmlAttribute(name = "eventCode")
	private String eventCode;

	@XmlElementWrapper(name = "cabecera")
	@XmlElement(name = "campo")
	private BeanCampoPlantilla[] camposCabecera;

	@XmlElementWrapper(name = "tabla")
	@XmlElement(name = "campo")
	private BeanCampoPlantilla[] camposTabla;

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public BeanCampoPlantilla[] getCamposCabecera() {
		return camposCabecera;
	}

	public void setCamposCabecera(BeanCampoPlantilla[] camposCabecera) {
		this.camposCabecera = camposCabecera;
	}

	public BeanCampoPlantilla[] getCamposTabla() {
		return camposTabla;
	}

	public void setCamposTabla(BeanCampoPlantilla[] camposTabla) {
		this.camposTabla = camposTabla;
	}

	@Override
	public String toString() {
		return String.format("BeanPlantilla [modulo=%s, template=%s, eventCode=%s, camposCabecera=%s, camposTabla=%s]", modulo, template, eventCode,
				Arrays.toString(camposCabecera), Arrays.toString(camposTabla));
	}

	// *****************************************************//

	public BeanCampoPlantilla[] ordenarCamposPorId(BeanCampoPlantilla[] campos) {
		BeanCampoPlantilla[] camposOrdenados;

		try {
			camposOrdenados = campos;

			for (int i = 0; i < (camposOrdenados.length - 1); i++) {
				for (int j = i + 1; j < camposOrdenados.length; j++) {
					int posicionI = Integer.parseInt(camposOrdenados[i].getId());
					int posicionJ = Integer.parseInt(camposOrdenados[j].getId());
					if (posicionI > posicionJ) {
						BeanCampoPlantilla variableauxiliar = camposOrdenados[i];
						camposOrdenados[i] = camposOrdenados[j];
						camposOrdenados[j] = variableauxiliar;
					}
				}
			}
		} catch (Exception e) {
			return null;
		}

		return camposOrdenados;
	}

}
