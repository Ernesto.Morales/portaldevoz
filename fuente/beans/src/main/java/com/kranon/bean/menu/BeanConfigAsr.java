package com.kranon.bean.menu;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.BeanGramatica;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanConfigAsr {

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion_asr")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion_asr")
	private BeanConfigRecAsr configuracion;

	/* Gramaticas del tipo de reconomiento */
	@XmlElementWrapper(name = "grammars")
	@XmlElement(name = "grammar")
	private BeanGramatica[] grammars;

	/* Prompts del tipo de reconomiento */
	@XmlElement(name = "prompts")
	private BeanPrompts prompts;

	public BeanConfigRecAsr getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigRecAsr configuracion) {
		this.configuracion = configuracion;
	}

	public BeanGramatica[] getGrammars() {
		return grammars;
	}

	public void setGrammars(BeanGramatica[] grammars) {
		this.grammars = grammars;
	}

	public BeanPrompts getPrompts() {
		return prompts;
	}

	public void setPrompts(BeanPrompts prompts) {
		this.prompts = prompts;
	}

	@Override
	public String toString() {
		return "BeanConfigAsr [configuracion=" + configuracion + ", grammars=" + Arrays.toString(grammars) + ", prompts=" + prompts + "]";
	}

}
