package com.kranon.bean.gestionmodulos;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para contener la informacion del XML <codServ>-GESTION-MODULOS.xml
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "modulos")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanGestionModulos {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServivr;

	@XmlElement(name = "modulo")
	private BeanInfoModulo[] modulos;

	public String getCodServivr() {
		return codServivr;
	}

	public void setCodServivr(String codServivr) {
		this.codServivr = codServivr;
	}

	public BeanInfoModulo[] getModulos() {
		return modulos;
	}

	public void setModulos(BeanInfoModulo[] modulos) {
		this.modulos = modulos;
	}

	@Override
	public String toString() {
		return "BeanGestionModulos [codServivr=" + codServivr + ", modulos=" + Arrays.toString(modulos) + "]";
	}

}
