package com.kranon.bean.menu;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanIntento {

	/* Codigo del intento */
	@XmlAttribute(name = "id")
	private String id;

	/* Prompts del intento */
	@XmlElement(name = "prompt")
	private BeanPromptGenerico[] prompts;

	/* Prompt No Input del intento */
	@XmlElement(name = "prompt_noinput")
	private BeanPrompt promptNoInput;

	/* Prompt No Match del intento */
	@XmlElement(name = "prompt_nomatch")
	private BeanPrompt promptNoMatch;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BeanPromptGenerico[] getPrompts() {
		return prompts;
	}

	public void setPrompts(BeanPromptGenerico[] prompts) {
		this.prompts = prompts;
	}

	public BeanPrompt getPromptNoInput() {
		return promptNoInput;
	}

	public void setPromptNoInput(BeanPrompt promptNoInput) {
		this.promptNoInput = promptNoInput;
	}

	public BeanPrompt getPromptNoMatch() {
		return promptNoMatch;
	}

	public void setPromptNoMatch(BeanPrompt promptNoMatch) {
		this.promptNoMatch = promptNoMatch;
	}

	@Override
	public String toString() {
		return "BeanIntento [id=" + id + ", prompts="
				+ Arrays.toString(prompts) + ", promptNoInput=" + promptNoInput
				+ ", promptNoMatch=" + promptNoMatch + "]";
	}

}
