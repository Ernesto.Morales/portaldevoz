package com.kranon.bean.excepserv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "excepcion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "prompt", "codigoRetorno", "estadistica" })
public class BeanExcepcion {

	/* Nombre de la excepcion */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Descripcion de la opcion */
	@XmlElement(name = "prompt")
	private BeanPromptExcepcion prompt;

	/* Descripcion de la opcion */
	@XmlElement(name = "codigoRetorno")
	private String codigoRetorno;

	/* Descripcion de la opcion */
	@XmlElement(name = "estadistica")
	private String estadistica;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BeanPromptExcepcion getPrompt() {
		return prompt;
	}

	public void setPrompt(BeanPromptExcepcion prompt) {
		this.prompt = prompt;
	}

	public String getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	@Override
	public String toString() {
		return "BeanExcepcion [nombre=" + nombre + ", prompt=" + prompt + ", codigoRetorno=" + codigoRetorno + ", estadistica=" + estadistica + "]";
	}

}
