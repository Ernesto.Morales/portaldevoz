package com.kranon.bean.gestioncm;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kranon.beanadapter.MapAdapterStat;

/**
 * Bean la informacin del Cuadro de mando
 * 
 */
@XmlRootElement(name = "gestion")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanGestionCuadroMando {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServIVR;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigCuadroMando configuracion;

	@XmlJavaTypeAdapter(MapAdapterStat.class)
	@XmlElement(name = "estadisticas")
	private HashMap<String, BeanEstadisticaCM> tablaEstadisticas;

	public String getCodServIVR() {
		return codServIVR;
	}

	public void setCodServIVR(String codServIVR) {
		this.codServIVR = codServIVR;
	}

	public BeanConfigCuadroMando getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigCuadroMando configuracion) {
		this.configuracion = configuracion;
	}

	public HashMap<String, BeanEstadisticaCM> getTablaEstadisticas() {
		return tablaEstadisticas;
	}

	public void setTablaEstadisticas(HashMap<String, BeanEstadisticaCM> tablaEstadisticas) {
		this.tablaEstadisticas = tablaEstadisticas;
	}

	@Override
	public String toString() {
		return String.format("BeanGestionCuadroMando [codServIVR=%s, configuracion=%s, tablaEstadisticas=%s]", codServIVR, configuracion,
				tablaEstadisticas);
	}

}
