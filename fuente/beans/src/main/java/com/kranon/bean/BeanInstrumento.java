package com.kranon.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "nombre", "numIntentos" })
public class BeanInstrumento {

	/* Id para indicar el orden del instrumento */
	@XmlAttribute(name = "id")
	private int id;

	/* Nombre del instrumento */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Numero de intentos que se va a solicitar el instrumento si la validacion falla */
	@XmlAttribute(name = "numIntentos")
	private int numIntentos;

	/* Valor del instrumento */
	@XmlValue
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumIntentos() {
		return numIntentos;
	}

	public void setNumIntentos(int numIntentos) {
		this.numIntentos = numIntentos;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanInstrumento [id=" + id + ", nombre=" + nombre + ", numIntentos=" + numIntentos + ", value=" + value + "]";
	}

}
