package com.kranon.bean.blacklist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de un Objeto de la Lista Negra
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "accion", "modoReproduccion", "wavPreAccion", "esquema" })
public class BeanConfigBlackObj {

	/* Accion del objetoo */
	@XmlElement(name = "accion")
	private String accion;

	/* Modo de reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locucion pre accion del objeto */
	@XmlElement(name = "wav_pre_accion")
	private String wavPreAccion;

	/* Codigo del esquema */
	@XmlElement(name = "cd_esquema")
	private String esquema;

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavPreAccion() {
		return wavPreAccion;
	}

	public void setWavPreAccion(String wavPreAccion) {
		this.wavPreAccion = wavPreAccion;
	}

	public String getEsquema() {
		return esquema;
	}

	public void setEsquema(String esquema) {
		this.esquema = esquema;
	}

	@Override
	public String toString() {
		return "BeanConfigBlackObj [accion=" + accion + ", modoReproduccion=" + modoReproduccion + ", wavPreAccion=" + wavPreAccion + ", esquema="
				+ esquema + "]";
	}

}
