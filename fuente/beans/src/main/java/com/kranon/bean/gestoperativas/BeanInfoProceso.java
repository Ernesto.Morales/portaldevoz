package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "proceso")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "automatizacion", "informacion", "desambiguacion", "evaluacion"})
public class BeanInfoProceso {

	@XmlElement(name = "automatizacion")
	private BeanInfoAutomatizacion automatizacion;
	
	@XmlElement(name = "informacion")
	private BeanInfoInformacion informacion;
	
	@XmlElement(name = "desambiguacion")
	private BeanInfoDesambiguacion desambiguacion;
	
	@XmlElement(name = "evaluacion")
	private BeanInfoEvaluacion evaluacion;
	

	public BeanInfoAutomatizacion getAutomatizacion() {
		return automatizacion;
	}

	public void setAutomatizacion(BeanInfoAutomatizacion automatizacion) {
		this.automatizacion = automatizacion;
	}

	public BeanInfoInformacion getInformacion() {
		return informacion;
	}

	public void setInformacion(BeanInfoInformacion informacion) {
		this.informacion = informacion;
	}

	public BeanInfoDesambiguacion getDesambiguacion() {
		return desambiguacion;
	}

	public void setDesambiguacion(BeanInfoDesambiguacion desambiguacion) {
		this.desambiguacion = desambiguacion;
	}

	public BeanInfoEvaluacion getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(BeanInfoEvaluacion evaluacion) {
		this.evaluacion = evaluacion;
	}

	

	@Override
	public String toString() {
		return "BeanInfoProceso [automatizacion=" + automatizacion
				+ ", informacion=" + informacion + ", desambiguacion="
				+ desambiguacion + ", evaluacion=" + evaluacion + "]";
	}

}
