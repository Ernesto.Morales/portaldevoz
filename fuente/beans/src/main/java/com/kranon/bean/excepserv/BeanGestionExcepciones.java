package com.kranon.bean.excepserv;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kranon.beanadapter.MapAdapter;

@XmlRootElement (name = "gestion_excepciones")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanGestionExcepciones {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServivr;

	/* Tabla de excepciones */
	@XmlJavaTypeAdapter(MapAdapter.class)
	@XmlElement(name = "excepciones")
	private HashMap<String, BeanExcepcion> tablaExcepciones;

	public String getCodServivr() {
		return codServivr;
	}

	public void setCodServivr(String codServivr) {
		this.codServivr = codServivr;
	}

	public HashMap<String, BeanExcepcion> getTablaExcepciones() {
		return tablaExcepciones;
	}

	public void setTablaExcepciones(HashMap<String, BeanExcepcion> tablaExcepciones) {
		this.tablaExcepciones = tablaExcepciones;
	}

	@Override
	public String toString() {
		return "BeanExcepcionesServicio [codServivr=" + codServivr + ", tablaExcepciones=" + tablaExcepciones + "]";
	}

}
