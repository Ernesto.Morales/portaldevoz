package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMenuServ {

	/* Codigo del menu */
	@XmlAttribute(name = "cd_menu")
	private String codMenu;

	@XmlElement(name = "opcion")
	private BeanOpcionServ[] opciones;

	public String getCodMenu() {
		return codMenu;
	}

	public void setCodMenu(String codMenu) {
		this.codMenu = codMenu;
	}

	public BeanOpcionServ[] getOpciones() {
		return opciones;
	}

	public void setOpciones(BeanOpcionServ[] opciones) {
		this.opciones = opciones;
	}

	@Override
	public String toString() {
		return "BeanMenuServ [codMenu=" + codMenu + ", opciones="
				+ Arrays.toString(opciones) + "]";
	}

}
