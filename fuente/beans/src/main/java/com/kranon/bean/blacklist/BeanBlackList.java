package com.kranon.bean.blacklist;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para la lista negra. Se corresponde con la tabla
 * IVR_CFG_BLACKLIST
 * 
 */
@XmlRootElement(name = "blacklist")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanBlackList {

	/* Lista de tipos de objetos de la lista negra */
	@XmlElement(name = "black_obj")
	private BeanTipoBlackObj[] blackObj;

	public BeanTipoBlackObj[] getBlackObj() {
		return blackObj;
	}

	public void setBlackObj(BeanTipoBlackObj[] blackObj) {
		this.blackObj = blackObj;
	}

	@Override
	public String toString() {
		return "BeanBlackList [blackObj=" + Arrays.toString(blackObj) + "]";
	}

}
