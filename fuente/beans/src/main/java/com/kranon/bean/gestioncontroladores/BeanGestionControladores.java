package com.kranon.bean.gestioncontroladores;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para contener la informacion del XML <codServ>-GESTION-CONTROLADORES.xml
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "gestion")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanGestionControladores {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServivr;

	/* Configuracion de los controladores */
	@XmlElement(name = "configuracion")
	private BeanConfigControlador[] configuracion;

	@XmlElementWrapper(name = "controladores")
	@XmlElement(name = "controlador")
	private BeanInfoControlador[] controladores;

	public String getCodServivr() {
		return codServivr;
	}

	public void setCodServivr(String codServivr) {
		this.codServivr = codServivr;
	}

	public BeanConfigControlador[] getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigControlador[] configuracion) {
		this.configuracion = configuracion;
	}

	public BeanInfoControlador[] getControladores() {
		return controladores;
	}

	public void setControladores(BeanInfoControlador[] controladores) {
		this.controladores = controladores;
	}

	@Override
	public String toString() {
		return "BeanGestionControladores [codServivr=" + codServivr + ", configuracion=" + Arrays.toString(configuracion) + ", controladores="
				+ Arrays.toString(controladores) + "]";
	}

}
