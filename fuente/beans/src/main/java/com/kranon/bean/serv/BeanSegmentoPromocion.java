package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para los Segmentos de las Promociones de un Servicio. Se corresponde con
 * la tabla IVR_CFG_PROMO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSegmentoPromocion {

	/* Codigo del segmento de la promocion */
	@XmlAttribute(name = "cd_segmento")
	private String codSegmento;

	/* Configuracion de la promocion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigPromoServicio configuracion;

	public String getCodSegmento() {
		return codSegmento;
	}

	public void setCodSegmento(String codSegmento) {
		this.codSegmento = codSegmento;
	}

	public BeanConfigPromoServicio getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigPromoServicio configuracion) {
		this.configuracion = configuracion;
	}

	@Override
	public String toString() {
		return "BeanSegmentoPromocion [codSegmento=" + codSegmento + ", configuracion=" + configuracion + "]";
	}

	// *********************************** //

	/**
	 * Devuelve el valor del parametro <key> indicado, dentro del array de
	 * configuracion
	 * 
	 * @param {@link String} key
	 * @return {@link String} value
	 */
	// public String getKeyValueConfig(String key) {
	//
	// if (this.configuracion == null || this.configuracion.length == 0) {
	// return null;
	// }
	// String value = "";
	// // recorro los parametros
	// for (BeanParam param : this.configuracion) {
	// if (param.getKey().equals(key)) {
	// // he encontrado el parametro que busco, con la key indicada
	// value = param.getValue();
	// break;
	// }
	// }
	// return value;
	//
	// }

}
