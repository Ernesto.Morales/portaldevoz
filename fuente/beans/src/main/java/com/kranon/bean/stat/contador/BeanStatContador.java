package com.kranon.bean.stat.contador;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "llamada")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanStatContador {

	@XmlElement(name = "fe_accioniv")
	private String feAccionIV;

	@XmlElement(name = "contador")
	private ArrayList<BeanStatInfoContador> contadores;

	public String getFeAccionIV() {
		return feAccionIV;
	}

	public void setFeAccionIV(String feAccionIV) {
		this.feAccionIV = feAccionIV;
	}

	public ArrayList<BeanStatInfoContador> getContadores() {
		return contadores;
	}

	public void setContadores(ArrayList<BeanStatInfoContador> contadores) {
		this.contadores = contadores;
	}

	@Override
	public String toString() {
		return String.format("BeanEstLlamadaCont [feAccionIV=%s, contadores=%s]", feAccionIV, contadores);
	}

}
