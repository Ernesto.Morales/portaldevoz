package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de las excepciones de un menu
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "type", "active", "mode", "script", "function", "prompts" })
public class BeanExcepcionMenu {

	/* Id de la excepcin del menu */
	@XmlAttribute(name = "id")
	private String id;

	/* Tipo de la excepcin del menu */
	@XmlAttribute(name = "type")
	private String type;

	/* Activacin de la excepcin del menu = {ON, OFF} */
	@XmlAttribute(name = "active")
	private String active;

	/* Modo de la excepcin del menu = {ASR, DTMF, ASRDTMF} */
	@XmlAttribute(name = "mode")
	private String mode;

	/* Script para tratar la excepcion */
	@XmlElement(name = "script")
	private String script;

	/* Funcin a llamar dentro del script anterior */
	@XmlElement(name = "function")
	private String function;

	/* Prompts de la excepcion */
	@XmlElement(name = "prompts")
	private BeanPrompts prompts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public BeanPrompts getPrompts() {
		return prompts;
	}

	public void setPrompts(BeanPrompts prompts) {
		this.prompts = prompts;
	}

	@Override
	public String toString() {
		return String.format("BeanExcepcionMenu [id=%s, type=%s, active=%s, mode=%s, script=%s, function=%s, prompts=%s]", id, type, active, mode,
				script, function, prompts);
	}

}
