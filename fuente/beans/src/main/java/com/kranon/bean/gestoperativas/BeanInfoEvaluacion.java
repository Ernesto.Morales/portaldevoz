package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "evaluacion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "operativa", "metadato" })
public class BeanInfoEvaluacion {

	@XmlAttribute(name = "operativa")
	private String operativa;

	@XmlElement(name = "metadato")
	private BeanInfoEvaluacionMetadato metadato;

	public String getOperativa() {
		return operativa;
	}

	public void setOperativa(String operativa) {
		this.operativa = operativa;
	}

	public BeanInfoEvaluacionMetadato getMetadato() {
		return metadato;
	}

	public void setMetadato(BeanInfoEvaluacionMetadato metadato) {
		this.metadato = metadato;
	}

	@Override
	public String toString() {
		return "BeanInfoEvaluacion [operativa=" + operativa + ", metadato=" + metadato + "]";
	}

	// *****************************************************//

	/**
	 * Junta en una cadena toda la informacion de operativa con metadatos para obtener el formato: operativa#tipometadato#metadato
	 * 
	 * @return
	 */
	public String getOperativaConMetadatos() {
		String etiquetaFinal = this.operativa;

		if (this.getMetadato().getAction() != null && !this.getMetadato().getAction().equals("")) {
			// el metadato ACTION viene informado
			etiquetaFinal = etiquetaFinal + "#action#" + this.getMetadato().getAction();
		}
		if (this.getMetadato().getType() != null && !this.getMetadato().getType().equals("")) {
			// el metadato TYPE viene informado
			etiquetaFinal = etiquetaFinal + "#type#" + this.getMetadato().getType();
		}
		if (this.getMetadato().getBalance() != null && !this.getMetadato().getBalance().equals("")) {
			// el metadato BALANCE viene informado
			etiquetaFinal = etiquetaFinal + "#balance#" + this.getMetadato().getBalance();

		}
		if (this.getMetadato().getFolio() != null && !this.getMetadato().getFolio().equals("")) {
			// el metadato FOLIO viene informado
			etiquetaFinal = etiquetaFinal + "#folio#" + this.getMetadato().getFolio();

		}
		if (this.getMetadato().getInsurance() != null && !this.getMetadato().getInsurance().equals("")) {
			// el metadato INSURANCE viene informado
			etiquetaFinal = etiquetaFinal + "#insurance#" + this.getMetadato().getInsurance();

		}
		if (this.getMetadato().getData() != null && !this.getMetadato().getData().equals("")) {
			// el metadato DATA viene informado
			etiquetaFinal = etiquetaFinal + "#data#" + this.getMetadato().getData();

		}

		return etiquetaFinal;
	}

}
