package com.kranon.bean.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanParamWS {

	/* Nombre Parametro */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Producto Parametro */
	@XmlAttribute(name = "producto")
	private String producto;
	
	/* Valor del parametro */
	@XmlValue
	private String value;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("BeanParamWS [nombre=%s, producto=%s, value=%s]",
				nombre, (producto!=null)?producto:"", value);
	}



}
