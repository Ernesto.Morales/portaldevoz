package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para los Servicios de las Promociones de un Servicio. Se corresponde con
 * la tabla IVR_CFG_PROMO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanServicioPromocion {

	/* Codigo del servicio de la promocion */
	@XmlAttribute(name = "cd_servicio")
	private String codServicio;

	/* Array de tarifas del servicio de la promocion */
	@XmlElement(name = "tarifa")
	private BeanTarifaPromocion[] tarifas;

	public String getCodServicio() {
		return codServicio;
	}

	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}

	public BeanTarifaPromocion[] getTarifas() {
		return tarifas;
	}

	public void setTarifas(BeanTarifaPromocion[] tarifas) {
		this.tarifas = tarifas;
	}

	@Override
	public String toString() {
		return "BeanServicioPromocion [codServicio=" + codServicio
				+ ", tarifas=" + Arrays.toString(tarifas) + "]";
	}

}
