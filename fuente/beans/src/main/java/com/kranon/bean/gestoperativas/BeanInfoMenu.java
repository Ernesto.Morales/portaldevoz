package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "menu")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "controlador", "nombre"})
public class BeanInfoMenu {

	@XmlAttribute(name = "controlador")
	private String controlador;
	
	@XmlValue
	private String nombre;
	
	
	public String getControlador() {
		return controlador;
	}

	public void setControlador(String controlador) {
		this.controlador = controlador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "BeanInfoMenu [controlador=" + controlador
				+ ", nombre=" + nombre + "]";
	}

}
