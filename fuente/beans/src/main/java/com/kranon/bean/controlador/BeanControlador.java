package com.kranon.bean.controlador;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.BeanProcesoModulo;

/**
 * Bean para contener la informacion del XML <codServ>-CONTROLADOR-<nombreControlador>.xml
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "controlador")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanControlador {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServivr;

	/* Nombre del controlador */
	@XmlAttribute(name = "nombre")
	private String nombreControlador;

	/* Bienvenidas asociadas al controlador */
	@XmlElementWrapper(name = "bienvenidas")
	@XmlElement(name = "bienvenida")
	private BeanSaludoControlador[] bienvenidas;

	/* Despedidas asociadas al controlador */
	@XmlElementWrapper(name = "despedidas")
	@XmlElement(name = "despedida")
	private BeanSaludoControlador[] despedidas;

	/* Proceso de Identificacion */
	@XmlElement(name = "identificacion")
	private BeanProcesoModulo identificacion;

	/* Proceso de Autenticacion */
	@XmlElement(name = "autenticacion")
	private BeanProcesoModulo autenticacion;
	
	/* Indica si debe ofrecerse el menu de privacidad */
	@XmlElement(name = "activacionMenuAvisoPrivacidad")
	private String activacionMenuAvisoPrivacidad;
	
	/* Indica si debe ofrecerse la informacin de carta de deducibilidad */
	@XmlElement(name = "activacionLocucionCartaDeducibilidad")
	private String activacionLocucionCartaDeducibilidad;
	/* Indica si debe ofrecerse la informacin de carta de deducibilidad */
	@XmlElement(name = "intentosIdSession")
	private String intentosIdSession;
	
	public String getCodServivr() {
		return codServivr;
	}

	public void setCodServivr(String codServivr) {
		this.codServivr = codServivr;
	}

	public String getNombreControlador() {
		return nombreControlador;
	}

	public void setNombreControlador(String nombreControlador) {
		this.nombreControlador = nombreControlador;
	}

	public BeanSaludoControlador[] getBienvenidas() {
		return bienvenidas;
	}

	public void setBienvenidas(BeanSaludoControlador[] bienvenidas) {
		this.bienvenidas = bienvenidas;
	}

	public BeanSaludoControlador[] getDespedidas() {
		return despedidas;
	}

	public void setDespedidas(BeanSaludoControlador[] despedidas) {
		this.despedidas = despedidas;
	}

	public BeanProcesoModulo getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(BeanProcesoModulo identificacion) {
		this.identificacion = identificacion;
	}

	public BeanProcesoModulo getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(BeanProcesoModulo autenticacion) {
		this.autenticacion = autenticacion;
	}
	
	public String getActivacionMenuAvisoPrivacidad() {
		return activacionMenuAvisoPrivacidad;
	}

	public void setActivacionMenuAvisoPrivacidad(
			String activacionMenuAvisoPrivacidad) {
		this.activacionMenuAvisoPrivacidad = activacionMenuAvisoPrivacidad;
	}

	public String getActivacionLocucionCartaDeducibilidad() {
		return activacionLocucionCartaDeducibilidad;
	}

	public void setActivacionLocucionCartaDeducibilidad(
			String activacionLocucionCartaDeducibilidad) {
		this.activacionLocucionCartaDeducibilidad = activacionLocucionCartaDeducibilidad;
	}

	
	/**
	 * @return the intentosIdSession
	 */
	public String getIntentosIdSession() {
		return intentosIdSession;
	}

	/**
	 * @param intentosIdSession the intentosIdSession to set
	 */
	public void setIntentosIdSession(String intentosIdSession) {
		this.intentosIdSession = intentosIdSession;
	}

	@Override
	public String toString() {
		return "BeanControlador [codServivr=" + codServivr
				+ ", nombreControlador=" + nombreControlador + ", bienvenidas="
				+ Arrays.toString(bienvenidas) + ", despedidas="
				+ Arrays.toString(despedidas) + ", identificacion="
				+ identificacion + ", autenticacion=" + autenticacion
				+ ", activacionMenuAvisoPrivacidad="
				+ activacionMenuAvisoPrivacidad
				+ ", activacionLocucionCartaDeducibilidad="
				+ activacionLocucionCartaDeducibilidad +"intentosIdSession=" +intentosIdSession+"]";
	}

	// ******************************************//

	/**
	 * Devuelve el saludo (bienvenida o despedida) asociado al vdn y tipo de cliente indicado
	 * 
	 * @param {@link BeanSaludosControlador[] } saludos
	 * @param vdn
	 * @param tipoCliente
	 * @return {@link BeanSaludoControlador}
	 */
	public BeanSaludoControlador getSaludo(BeanSaludoControlador[] saludos, String vdn, String tipoCliente) {

		BeanSaludoControlador saludoEncontrado = null;
		for (BeanSaludoControlador saludo : saludos) {
			if (vdn != null) {
				if (saludo.getVdn() != null && saludo.getVdn().equalsIgnoreCase(vdn)) {
					// casa con el vdn
					if (tipoCliente == null) {
						// no debe casar por tipo cliente, asi que ya lo he
						// encontrado
						saludoEncontrado = saludo;
						break;
					} else {
						// tamben debe coincidir el tipo de cliente
						if (saludo.getTipoCliente() != null && saludo.getTipoCliente().equalsIgnoreCase(tipoCliente)) {
							saludoEncontrado = saludo;
							break;
						}
					}
				}
			} else if (tipoCliente != null) {
				// solo buscamos por tipo de cliente
				if (saludo.getTipoCliente() != null && saludo.getTipoCliente().equalsIgnoreCase(tipoCliente)) {
					saludoEncontrado = saludo;
					break;
				}
			}
		}
		return saludoEncontrado;

	}

	/**
	 * Devuelve el tipo de identificacion/autenticacion activo de todos los instrumentos de identificacon/autenticacion definidos en el controlador
	 * 
	 * @param {@link BeanInstrumento[]} instrumentos
	 * @return {@link String} tipoIdentificacion
	 */
	// public String getInstrumentoActivo(BeanInstrumento[] instrumentos) {
	//
	// String tipoIdentificacion = "";
	// for (BeanInstrumento ins : instrumentos) {
	// if (ins.getValue() != null && ins.getValue().equalsIgnoreCase("ON")) {
	// tipoIdentificacion = ins.getNombre();
	// break;
	// }
	// }
	// return tipoIdentificacion;
	//
	// }

}
