package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "variable")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "segmentoCliente", "numeracionEntrada", "nombre"})
public class BeanVariableTransfer {

	@XmlAttribute(name = "segmentoCliente")
	private String segmentoCliente;
	
	@XmlAttribute(name = "numeracionEntrada")
	private String numeracionEntrada;
	
	@XmlValue
	private String nombre;

	public String getSegmentoCliente() {
		return segmentoCliente;
	}

	public void setSegmentoCliente(String segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}

	public String getNumeracionEntrada() {
		return numeracionEntrada;
	}

	public void setNumeracionEntrada(String numeracionEntrada) {
		this.numeracionEntrada = numeracionEntrada;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "BeanVariableTransfer [segmentoCliente=" + segmentoCliente
				+ ", numeracionEntrada=" + numeracionEntrada + ", nombre="
				+ nombre + "]";
	}
	

}
