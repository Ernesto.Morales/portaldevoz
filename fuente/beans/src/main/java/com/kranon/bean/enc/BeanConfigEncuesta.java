package com.kranon.bean.enc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de una Encuesta. Se corresponde con la tabla
 * TGCE006_ENCUESTA
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "encActiva", "descripcionEnc", "modoReproduccion", "wavNoActivo", "modoInteraccion", "controlHorario", "statCmActivo", "tranRegActivo" })
public class BeanConfigEncuesta {

	/* Activacion de la Encuesta */
	@XmlElement(name = "enc_activa")
	private String encActiva;

	/* Descripcion de la Encuesta */
	@XmlElement(name = "desc_enc")
	private String descripcionEnc;

	/* Modo de reproduccion */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Locucion si no activo */
	@XmlElement(name = "wav_no_activo")
	private String wavNoActivo;

	/* Modo de Interaccion */
	@XmlElement(name = "modo_interaccion")
	private String modoInteraccion;

	/* Control Horario */
	@XmlElement(name = "control_horario")
	private String controlHorario;

	/* Activacion estadisticas cuadro de mando */
	@XmlElement(name = "stat_cm_activo")
	private String statCmActivo;

	/* Activacion del registro de respuestas por transaccion */
	@XmlElement(name = "tran_reg_activo")
	private String tranRegActivo;

	public String getEncActiva() {
		return encActiva;
	}

	public void setEncActiva(String encActiva) {
		this.encActiva = encActiva;
	}

	public String getDescripcionEnc() {
		return descripcionEnc;
	}

	public void setDescripcionEnc(String descripcionEnc) {
		this.descripcionEnc = descripcionEnc;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavNoActivo() {
		return wavNoActivo;
	}

	public void setWavNoActivo(String wavNoActivo) {
		this.wavNoActivo = wavNoActivo;
	}

	public String getModoInteraccion() {
		return modoInteraccion;
	}

	public void setModoInteraccion(String modoInteraccion) {
		this.modoInteraccion = modoInteraccion;
	}

	public String getControlHorario() {
		return controlHorario;
	}

	public void setControlHorario(String controlHorario) {
		this.controlHorario = controlHorario;
	}

	public String getStatCmActivo() {
		return statCmActivo;
	}

	public void setStatCmActivo(String statCmActivo) {
		this.statCmActivo = statCmActivo;
	}

	public String getTranRegActivo() {
		return tranRegActivo;
	}

	public void setTranRegActivo(String tranRegActivo) {
		this.tranRegActivo = tranRegActivo;
	}

	@Override
	public String toString() {
		return "BeanConfigEncuesta [encActiva=" + encActiva + ", descripcionEnc=" + descripcionEnc + ", modoReproduccion=" + modoReproduccion
				+ ", wavNoActivo=" + wavNoActivo + ", modoInteraccion=" + modoInteraccion + ", controlHorario=" + controlHorario + ", statCmActivo="
				+ statCmActivo + ", tranRegActivo=" + tranRegActivo + "]";
	}

}
