package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nombre", "value" })
public class BeanAccionesProcesoOperativa {

	/* Nombre de la accion */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Valor de la accion */
	@XmlValue
	private String value;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanAccionesProcesoModulo [nombre=" + nombre + ", value=" + value + "]";
	}

}
