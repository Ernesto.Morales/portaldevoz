package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

@XmlRootElement(name = "opcion")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanOpcionServ {

	/* Id de la opcion */
	@XmlAttribute(name = "id")
	private String idOpcion;

	/* Descripcion de la opcion */
	@XmlElement(name = "desc_opc")
	private String descOpcion;

	/* Activacion de la contingencia */
	@XmlElement(name = "cont_activo")
	private String contActivo;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getIdOpcion() {
		return idOpcion;
	}

	public void setIdOpcion(String idOpcion) {
		this.idOpcion = idOpcion;
	}

	public String getDescOpcion() {
		return descOpcion;
	}

	public void setDescOpcion(String descOpcion) {
		this.descOpcion = descOpcion;
	}

	public String getContActivo() {
		return contActivo;
	}

	public void setContActivo(String contActivo) {
		this.contActivo = contActivo;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanOpcionServ [idOpcion=" + idOpcion + ", descOpcion="
				+ descOpcion + ", contActivo=" + contActivo + ", modif="
				+ modif + "]";
	}

}
