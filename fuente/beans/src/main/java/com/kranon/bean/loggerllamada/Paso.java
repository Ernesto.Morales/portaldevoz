package com.kranon.bean.loggerllamada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Paso {
	@XmlAttribute(name = "jps")
	private String nombrejps = "";
	@XmlAttribute(name = "idjsp")
	private String nombrefrom = "";
	@XmlAttribute(name = "cod")
	private String codrespuesta ="";
	@XmlAttribute(name = "resop")
	private String resultadoOperacion= "";
	@XmlAttribute(name = "inf")
	private String variables= "";
	
	
	public Paso() {
		super();
	}
	
	public Paso(String nombrejps, String nombrefrom, String codrespuesta,
			String resultadoOperacion, String variables) {
		super();
		this.nombrejps = nombrejps;
		this.nombrefrom = nombrefrom;
		this.codrespuesta = codrespuesta;
		this.resultadoOperacion = resultadoOperacion;
		this.variables = variables;
	}
	public String getNombrejps() {
		return nombrejps;
	}
	public void setNombrejps(String nombrejps) {
		this.nombrejps = nombrejps;
	}
	public String getNombrefrom() {
		return nombrefrom;
	}
	public void setNombrefrom(String nombrefrom) {
		this.nombrefrom = nombrefrom;
	}
	public String getCodrespuesta() {
		return codrespuesta;
	}
	public void setCodrespuesta(String codrespuesta) {
		this.codrespuesta = codrespuesta;
	}
	public String getResultadoOperacion() {
		return resultadoOperacion;
	}
	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}
	public String getVariables() {
		return variables;
	}
	public void setVariables(String variables) {
		this.variables = variables;
	}

	@Override
	public String toString() {
		return "Paso [nombrejps=" + nombrejps + ", nombrefrom=" + nombrefrom
				+ ", codrespuesta=" + codrespuesta + ", resultadoOperacion="
				+ resultadoOperacion + ", variables=" + variables + "]";
	}
	
}
