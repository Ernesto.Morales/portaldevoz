package com.kranon.bean.plantillanotifications;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "tamanio", "charInicio", "charFin", "espaciado" })
public class BeanCampoPlantilla {

	@XmlAttribute(name = "id")
	private String id;

	@XmlAttribute(name = "tamanio")
	private int tamanio;

	@XmlAttribute(name = "charInicio")
	private String charInicio;

	@XmlAttribute(name = "charFin")
	private String charFin;

	@XmlAttribute(name = "espaciado")
	private int espaciado;

	@XmlValue
	private String nombreCampo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}

	public String getCharInicio() {
		return charInicio;
	}

	public void setCharInicio(String charInicio) {
		this.charInicio = charInicio;
	}

	public String getCharFin() {
		return charFin;
	}

	public void setCharFin(String charFin) {
		this.charFin = charFin;
	}

	public int getEspaciado() {
		return espaciado;
	}

	public void setEspaciado(int espaciado) {
		this.espaciado = espaciado;
	}

	public String getNombreCampo() {
		return nombreCampo;
	}

	public void setNombreCampo(String nombreCampo) {
		this.nombreCampo = nombreCampo;
	}

	@Override
	public String toString() {
		return String.format("BeanCampoPlantilla [id=%s, tamanio=%s, charInicio=%s, charFin=%s, espaciado=%s, nombreCampo=%s]", id, tamanio,
				charInicio, charFin, espaciado, nombreCampo);
	}

}
