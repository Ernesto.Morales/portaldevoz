package com.kranon.bean.transfer_schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanProducto {

	/* Codigo del producto */
	@XmlAttribute(name = "cd_producto")
	private String codProducto;
	
	/* Codigo transferencia label */
	@XmlElement(name = "cod_transfer_label")
	private String codTransferLabel;
	
	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	/* Array de sectores dentro del producto */
//	@XmlElement(name = "sector")
//	private BeanSector[] sectores;

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public String getCodTransferLabel() {
		return codTransferLabel;
	}

	public void setCodTransferLabel(String codTransferLabel) {
		this.codTransferLabel = codTransferLabel;
	}
	

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanProducto [codProducto=" + codProducto + ", codTransferLabel=" + codTransferLabel + ", modif=" + modif + "]";
	}

}
