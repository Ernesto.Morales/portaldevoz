package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de un Servicio. Se corresponde con la tabla
 * IVR_CFG_PARAM_SVC
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "servActivo", "cti", "controlHorario", "modoInteraccion", "controlTransferActivo", "controlContActivo", "lbabActivo",
		"tipifActivo", "statCmActivo", "encuestaActivo", "encuestaDefecto", "pregAbiertaActivo", "numIntPregAbierta", "servNumReintAsr", "servNumReintAsrToDtmf",
		"servNumReintDtmf", "vdnTransferDef", "bargeinAsrActivo", "bargeinDtmfActivo", "propertyTimeoutAsr", "propertyMaxspeechTimeoutAsr",
		"propertyIncompleteTimeoutAsr", "propertyCompleteTimeoutAsr", "propertyConfidenceLevelAsr", "propertySensitivityAsr", "timeoutDtmf",
		"interdigitTimeoutDtmf" })
public class BeanConfigServicio {

	/* Activacion del servicio */
	@XmlElement(name = "serv_activo")
	private String servActivo;

	/* Interfaz cti */
	@XmlElement(name = "cti")
	private String cti;

	/* Control horario */
	@XmlElement(name = "control_horario")
	private String controlHorario;

	/* Modo de interaccion ASR/DTMF */
	@XmlElement(name = "modo_interaccion")
	private String modoInteraccion;

	/* Activacion de transferencias */
	@XmlElement(name = "control_transfer_activo")
	private String controlTransferActivo;

	/* Activacion contingencia */
	@XmlElement(name = "control_cont_activo")
	private String controlContActivo;

	/* Activacion transaccion LBAB */
	@XmlElement(name = "lbab_activo")
	private String lbabActivo;

	/* Activacion de Tipificacion */
	@XmlElement(name = "tipif_activo")
	private String tipifActivo;

	/* Activacion estadisticas cuadro de mando */
	@XmlElement(name = "stat_cm_activo")
	private String statCmActivo;

	/* Activacion de encuesta */
	@XmlElement(name = "encuesta_activo")
	private String encuestaActivo;

	/* Codigo Encuesta por defecto */
	@XmlElement(name = "encuesta_defecto")
	private String encuestaDefecto;

	/* Activacion pregunta abierta */
	@XmlElement(name = "preg_abierta_activo")
	private String pregAbiertaActivo;

	/* Num. intentos preg abierta */
	@XmlElement(name = "num_int_preg_abierta")
	private String numIntPregAbierta;

	/* Num. intentos ASR */
	@XmlElement(name = "serv_num_reint_asr")
	private String servNumReintAsr;

	/* NO SE USA */
	@XmlElement(name = "serv_num_reint_asr_to_dtmf")
	private String servNumReintAsrToDtmf;

	/* Num. intentos DTMF */
	@XmlElement(name = "serv_num_reint_dtmf")
	private String servNumReintDtmf;

	/* VDN de transferencia por defecto */
	@XmlElement(name = "vdn_transfer_def")
	private String vdnTransferDef;

	/* Bargein ASR */
	@XmlElement(name = "bargein_asr_activo")
	private String bargeinAsrActivo;

	/* Bargein DTMF */
	@XmlElement(name = "bargein_dtmf_activo")
	private String bargeinDtmfActivo;

	/* Timeout ASR */
	@XmlElement(name = "property_timeout_asr")
	private String propertyTimeoutAsr;

	/* Maxspeech Timeout ASR */
	@XmlElement(name = "property_maxspeech_timeout_asr")
	private String propertyMaxspeechTimeoutAsr;

	/* Incomplete Timeout ASR */
	@XmlElement(name = "property_incomplete_timeout_asr")
	private String propertyIncompleteTimeoutAsr;

	/* Complete Timeout ASR */
	@XmlElement(name = "property_complete_timeout_asr")
	private String propertyCompleteTimeoutAsr;

	/* Nivel de Confianza ASR */
	@XmlElement(name = "property_confidence_level_asr")
	private String propertyConfidenceLevelAsr;

	/* Sensibilidad ASR */
	@XmlElement(name = "property_sensitivity_asr")
	private String propertySensitivityAsr;

	/* Timeout DTMF */
	@XmlElement(name = "property_timeout_dtmf")
	private String timeoutDtmf;

	/* Interdigit Timeout DTMF */
	@XmlElement(name = "property_interdigit_timeout_dtmf")
	private String interdigitTimeoutDtmf;

	public String getServActivo() {
		return servActivo;
	}

	public void setServActivo(String servActivo) {
		this.servActivo = servActivo;
	}

	public String getCti() {
		return cti;
	}

	public void setCti(String cti) {
		this.cti = cti;
	}

	public String getControlHorario() {
		return controlHorario;
	}

	public void setControlHorario(String controlHorario) {
		this.controlHorario = controlHorario;
	}

	public String getModoInteraccion() {
		return modoInteraccion;
	}

	public void setModoInteraccion(String modoInteraccion) {
		this.modoInteraccion = modoInteraccion;
	}

	public String getControlTransferActivo() {
		return controlTransferActivo;
	}

	public void setControlTransferActivo(String controlTransferActivo) {
		this.controlTransferActivo = controlTransferActivo;
	}

	public String getControlContActivo() {
		return controlContActivo;
	}

	public void setControlContActivo(String controlContActivo) {
		this.controlContActivo = controlContActivo;
	}

	public String getLbabActivo() {
		return lbabActivo;
	}

	public void setLbabActivo(String lbabActivo) {
		this.lbabActivo = lbabActivo;
	}

	public String getTipifActivo() {
		return tipifActivo;
	}

	public void setTipifActivo(String tipifActivo) {
		this.tipifActivo = tipifActivo;
	}

	public String getStatCmActivo() {
		return statCmActivo;
	}

	public void setStatCmActivo(String statCmActivo) {
		this.statCmActivo = statCmActivo;
	}

	public String getEncuestaActivo() {
		return encuestaActivo;
	}

	public void setEncuestaActivo(String encuestaActivo) {
		this.encuestaActivo = encuestaActivo;
	}

	public String getEncuestaDefecto() {
		return encuestaDefecto;
	}

	public void setEncuestaDefecto(String encuestaDefecto) {
		this.encuestaDefecto = encuestaDefecto;
	}

	public String getPregAbiertaActivo() {
		return pregAbiertaActivo;
	}

	public void setPregAbiertaActivo(String pregAbiertaActivo) {
		this.pregAbiertaActivo = pregAbiertaActivo;
	}

	public String getNumIntPregAbierta() {
		return numIntPregAbierta;
	}

	public void setNumIntPregAbierta(String numIntPregAbierta) {
		this.numIntPregAbierta = numIntPregAbierta;
	}

	public String getServNumReintAsr() {
		return servNumReintAsr;
	}

	public void setServNumReintAsr(String servNumReintAsr) {
		this.servNumReintAsr = servNumReintAsr;
	}

	public String getServNumReintAsrToDtmf() {
		return servNumReintAsrToDtmf;
	}

	public void setServNumReintAsrToDtmf(String servNumReintAsrToDtmf) {
		this.servNumReintAsrToDtmf = servNumReintAsrToDtmf;
	}

	public String getServNumReintDtmf() {
		return servNumReintDtmf;
	}

	public void setServNumReintDtmf(String servNumReintDtmf) {
		this.servNumReintDtmf = servNumReintDtmf;
	}

	public String getVdnTransferDef() {
		return vdnTransferDef;
	}

	public void setVdnTransferDef(String vdnTransferDef) {
		this.vdnTransferDef = vdnTransferDef;
	}

	public String getBargeinAsrActivo() {
		return bargeinAsrActivo;
	}

	public void setBargeinAsrActivo(String bargeinAsrActivo) {
		this.bargeinAsrActivo = bargeinAsrActivo;
	}

	public String getBargeinDtmfActivo() {
		return bargeinDtmfActivo;
	}

	public void setBargeinDtmfActivo(String bargeinDtmfActivo) {
		this.bargeinDtmfActivo = bargeinDtmfActivo;
	}

	public String getPropertyTimeoutAsr() {
		return propertyTimeoutAsr;
	}

	public void setPropertyTimeoutAsr(String propertyTimeoutAsr) {
		this.propertyTimeoutAsr = propertyTimeoutAsr;
	}

	public String getPropertyMaxspeechTimeoutAsr() {
		return propertyMaxspeechTimeoutAsr;
	}

	public void setPropertyMaxspeechTimeoutAsr(String propertyMaxspeechTimeoutAsr) {
		this.propertyMaxspeechTimeoutAsr = propertyMaxspeechTimeoutAsr;
	}

	public String getPropertyIncompleteTimeoutAsr() {
		return propertyIncompleteTimeoutAsr;
	}

	public void setPropertyIncompleteTimeoutAsr(String propertyIncompleteTimeoutAsr) {
		this.propertyIncompleteTimeoutAsr = propertyIncompleteTimeoutAsr;
	}

	public String getPropertyCompleteTimeoutAsr() {
		return propertyCompleteTimeoutAsr;
	}

	public void setPropertyCompleteTimeoutAsr(String propertyCompleteTimeoutAsr) {
		this.propertyCompleteTimeoutAsr = propertyCompleteTimeoutAsr;
	}

	public String getPropertyConfidenceLevelAsr() {
		return propertyConfidenceLevelAsr;
	}

	public void setPropertyConfidenceLevelAsr(String propertyConfidenceLevelAsr) {
		this.propertyConfidenceLevelAsr = propertyConfidenceLevelAsr;
	}

	public String getPropertySensitivityAsr() {
		return propertySensitivityAsr;
	}

	public void setPropertySensitivityAsr(String propertySensitivityAsr) {
		this.propertySensitivityAsr = propertySensitivityAsr;
	}

	public String getTimeoutDtmf() {
		return timeoutDtmf;
	}

	public void setTimeoutDtmf(String timeoutDtmf) {
		this.timeoutDtmf = timeoutDtmf;
	}

	public String getInterdigitTimeoutDtmf() {
		return interdigitTimeoutDtmf;
	}

	public void setInterdigitTimeoutDtmf(String interdigitTimeoutDtmf) {
		this.interdigitTimeoutDtmf = interdigitTimeoutDtmf;
	}

	@Override
	public String toString() {
		return "BeanConfigServicio [servActivo=" + servActivo + ", cti=" + cti + ", controlHorario=" + controlHorario + ", modoInteraccion="
				+ modoInteraccion + ", controlTransferActivo=" + controlTransferActivo + ", controlContActivo=" + controlContActivo + ", lbabActivo="
				+ lbabActivo + ", tipifActivo=" + tipifActivo + ", statCmActivo=" + statCmActivo + ", encuestaActivo=" + encuestaActivo
				+ ", encuestaDefecto=" + encuestaDefecto + ", pregAbiertaActivo=" + pregAbiertaActivo + ", numIntPregAbierta=" + numIntPregAbierta
				+ ", servNumReintAsr=" + servNumReintAsr + ", servNumReintAsrToDtmf=" + servNumReintAsrToDtmf + ", servNumReintDtmf="
				+ servNumReintDtmf + ", vdnTransferDef=" + vdnTransferDef + ", bargeinAsrActivo=" + bargeinAsrActivo + ", bargeinDtmfActivo="
				+ bargeinDtmfActivo + ", propertyTimeoutAsr=" + propertyTimeoutAsr + ", propertyMaxspeechTimeoutAsr=" + propertyMaxspeechTimeoutAsr
				+ ", propertyIncompleteTimeoutAsr=" + propertyIncompleteTimeoutAsr + ", propertyCompleteTimeoutAsr=" + propertyCompleteTimeoutAsr
				+ ", propertyConfidenceLevelAsr=" + propertyConfidenceLevelAsr + ", propertySensitivityAsr=" + propertySensitivityAsr
				+ ", timeoutDtmf=" + timeoutDtmf + ", interdigitTimeoutDtmf=" + interdigitTimeoutDtmf + "]";
	}

}
