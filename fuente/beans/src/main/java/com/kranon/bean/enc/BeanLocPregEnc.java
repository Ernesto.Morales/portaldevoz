package com.kranon.bean.enc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para las locuciones de las Preguntas de una Encuesta
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanLocPregEnc {

	/* Tipo de locucion: ASR o TTS */
	@XmlAttribute(name = "tipo")
	private String tipo;

	/* Locucion 1 */
	@XmlElement(name = "prompt1")
	private String prompt1;

	/* Locucion 2 */
	@XmlElement(name = "prompt2")
	private String prompt2;

	/* Locucion 3 */
	@XmlElement(name = "prompt3")
	private String prompt3;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPrompt1() {
		return prompt1;
	}

	public void setPrompt1(String prompt1) {
		this.prompt1 = prompt1;
	}

	public String getPrompt2() {
		return prompt2;
	}

	public void setPrompt2(String prompt2) {
		this.prompt2 = prompt2;
	}

	public String getPrompt3() {
		return prompt3;
	}

	public void setPrompt3(String prompt3) {
		this.prompt3 = prompt3;
	}

	@Override
	public String toString() {
		return "BeanLocPregEnc [tipo=" + tipo + ", prompt1=" + prompt1
				+ ", prompt2=" + prompt2 + ", prompt3=" + prompt3 + "]";
	}

}
