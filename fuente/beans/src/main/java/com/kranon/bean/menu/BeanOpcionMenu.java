package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de las opciones Se corresponde con la tabla
 * TGCE009_OPCIONDINAM
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "configuracion", "configAsr", "configDtmf" })
public class BeanOpcionMenu {

	/* Codigo de la opcion */
	@XmlAttribute(name = "cd_opc")
	private String codOpcion;

	/* Parametros de configuracion general de la opcion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion_opc")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion_opc")
	private BeanConfigOpcMenu configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "asr_opc")
	private BeanConfigOpcAsrMenu configAsr;

	/* Parametros de configuracion */
	@XmlElement(name = "dtmf_opc")
	private BeanConfigOpcDtmfMenu configDtmf;

	/* Parametros de configuracion de ASR de la opcion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "asr_opc")
	// private BeanParam[] configAsr;

	/* Parametros de configuracion de DTMF de la opcion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "dtmf_opc")
	// private BeanParam[] configDtmf;


	public BeanConfigOpcMenu getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigOpcMenu configuracion) {
		this.configuracion = configuracion;
	}

	public BeanConfigOpcAsrMenu getConfigAsr() {
		return configAsr;
	}

	public void setConfigAsr(BeanConfigOpcAsrMenu configAsr) {
		this.configAsr = configAsr;
	}

	public BeanConfigOpcDtmfMenu getConfigDtmf() {
		return configDtmf;
	}

	public void setConfigDtmf(BeanConfigOpcDtmfMenu configDtmf) {
		this.configDtmf = configDtmf;
	}

	/**
	 * @return the codOpcion
	 */
	public String getCodOpcion() {
		return codOpcion;
	}

	/**
	 * @param codOpcion the codOpcion to set
	 */
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}

	@Override
	public String toString() {
		return "BeanOpcionMenu [codOpcion=" + codOpcion + ", configuracion=" + configuracion + ", configAsr=" + configAsr + ", configDtmf="
				+ configDtmf + "]";
	}

}
