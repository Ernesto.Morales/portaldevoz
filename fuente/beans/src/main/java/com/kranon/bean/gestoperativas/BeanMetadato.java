package com.kranon.bean.gestoperativas;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


public class BeanMetadato {

	private String action;
	
	private String type;
	
	private String balance;
	
	private String folio;
	
	private String insurance;
	
	private String data;
	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	
	@Override
	public String toString() {
		return "BeanMetadato [action=" + action + ", type=" + type
				+ ", balance=" + balance + ", folio=" + folio + ", insurance="
				+ insurance + ", data=" + data + "]";
	}
	
	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public boolean equals(Object obj) {
		if (obj instanceof BeanMetadato) {
			BeanMetadato other = (BeanMetadato) obj;
			EqualsBuilder builder = new EqualsBuilder();
			builder.append(this.action, other.action);
			builder.append(this.type, other.type);
			builder.append(this.balance, other.balance);
			builder.append(this.folio, other.folio);
			builder.append(this.insurance, other.insurance);
			builder.append(this.data, other.data);
			return builder.isEquals();
		}
		return false;
	}

	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(action);
		builder.append(type);
		builder.append(balance);
		builder.append(folio);
		builder.append(insurance);
		builder.append(data);
		return builder.toHashCode();
	}

}
