package com.kranon.bean.gestioncm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion del Cuadro de mando
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "invocacionWS", "borradoFicheroLlamadas", "borradoFicheroContadores" })
public class BeanConfigCuadroMando {

	/* Activacion de la invocacion al WS */
	@XmlElement(name = "invocacionWS")
	private String invocacionWS;

	/* Activacion del borrado del fichero llamada */
	@XmlElement(name = "borradoFicheroLlamadas")
	private String borradoFicheroLlamadas;

	/* Activacion del borrado del fichero contador */
	@XmlElement(name = "borradoFicheroContadores")
	private String borradoFicheroContadores;

	public String getInvocacionWS() {
		return invocacionWS;
	}

	public void setInvocacionWS(String invocacionWS) {
		this.invocacionWS = invocacionWS;
	}

	public String getBorradoFicheroLlamadas() {
		return borradoFicheroLlamadas;
	}

	public void setBorradoFicheroLlamadas(String borradoFicheroLlamadas) {
		this.borradoFicheroLlamadas = borradoFicheroLlamadas;
	}

	public String getBorradoFicheroContadores() {
		return borradoFicheroContadores;
	}

	public void setBorradoFicheroContadores(String borradoFicheroContadores) {
		this.borradoFicheroContadores = borradoFicheroContadores;
	}

	@Override
	public String toString() {
		return String.format("BeanConfigCuadroMando [invocacionWS=%s, borradoFicheroLlamadas=%s, borradoFicheroContadores=%s]", invocacionWS,
				borradoFicheroLlamadas, borradoFicheroContadores);
	}

}
