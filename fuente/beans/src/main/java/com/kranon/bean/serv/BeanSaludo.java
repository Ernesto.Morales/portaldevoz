package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

/**
 * Bean para los Saludos de un Servicio. Se corresponde con
 * la tabla IVR_CFG_SALUDO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSaludo {

	/* Fecha de inicio del saludo */
	@XmlAttribute(name = "fecha_inicio")
	private String fechaInicio;

	/* Fecha de fin del saludo */
	@XmlAttribute(name = "fecha_fin")
	private String fechaFin;

	/* Modo reproduccion del saludo */
	@XmlElement(name = "modo_reproduccion")
	private String modoReproduccion;

	/* Wav del saludo */
	@XmlElement(name = "wav_saludo")
	private String wavSaludo;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getWavSaludo() {
		return wavSaludo;
	}

	public void setWavSaludo(String wavSaludo) {
		this.wavSaludo = wavSaludo;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanSaludo [fechaInicio=" + fechaInicio + ", fechaFin="
				+ fechaFin + ", modoReproduccion=" + modoReproduccion
				+ ", wavSaludo=" + wavSaludo + ", modif=" + modif + "]";
	}

}
