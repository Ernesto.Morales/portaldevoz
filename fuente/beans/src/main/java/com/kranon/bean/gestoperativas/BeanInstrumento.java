package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "activo", "numIntentos", "nombre" })
public class BeanInstrumento {

	/* Id para indicar el orden del instrumento */
	@XmlAttribute(name = "id")
	private int id;

	@XmlAttribute(name = "activo")
	private String activo;
	
	/* Numero de intentos que se va a solicitar el instrumento si la validacion falla */
	@XmlAttribute(name = "numIntentos")
	private int numIntentos;

	/* Valor del instrumento */
	@XmlValue
	private String nombre;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public int getNumIntentos() {
		return numIntentos;
	}

	public void setNumIntentos(int numIntentos) {
		this.numIntentos = numIntentos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "BeanInstrumento [id=" + id + ", activo=" + activo
				+ ", numIntentos=" + numIntentos + ", nombre=" + nombre + "]";
	}

}
