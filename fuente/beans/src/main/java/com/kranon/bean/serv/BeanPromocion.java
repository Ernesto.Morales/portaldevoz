package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para las Promociones de un Servicio. Se corresponde con la tabla
 * IVR_CFG_PROMO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPromocion {

	/* Fecha de inicio de la promocion */
	@XmlAttribute(name = "fecha_inicio")
	private String fechaInicio;

	/* Fecha de fin de la promocion */
	@XmlAttribute(name = "fecha_fin")
	private String fechaFin;

	/* Array de productos de la promocion en estas fechas */
	@XmlElement(name = "producto")
	private BeanProductoPromocion[] productos;

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public BeanProductoPromocion[] getProductos() {
		return productos;
	}

	public void setProductos(BeanProductoPromocion[] productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		return "BeanPromocion [fechaInicio=" + fechaInicio + ", fechaFin="
				+ fechaFin + ", productos=" + Arrays.toString(productos) + "]";
	}

}
