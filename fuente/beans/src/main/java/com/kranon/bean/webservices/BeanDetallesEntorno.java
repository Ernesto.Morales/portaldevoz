package com.kranon.bean.webservices;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanDetallesEntorno {

	/* Nombre WS */
	@XmlAttribute(name = "nombre")
	private String nombre;
	
	/* Version WS */
	@XmlAttribute(name = "version")
	private String version;
	
	/* Url para el servicio */
	@XmlElement(name = "url")
	private String url;

	/* Parametros fijos para el servicio */
	@XmlElementWrapper(name = "params")
	@XmlElement(name = "param")
	private BeanParamWS[] parametros;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BeanParamWS[] getParametros() {
		return parametros;
	}

	public void setParametros(BeanParamWS[] parametros) {
		this.parametros = parametros;
	}

	@Override
	public String toString() {
		return String
				.format("BeanDetallesEntorno [nombre=%s, version=%s, url=%s, parametros=%s]",
						nombre, version, url, Arrays.toString(parametros));
	}
	
	/**************************************************/
	
	/**
	 * Devuelve el valor del parametro enviado como clave <key> <product>
	 * @param String key
	 * @param String product
	 * @return String
	 */
	public String getValueParametro(String key, String product){
		if(this.parametros == null || this.parametros.length == 0){
			// no hay parametros para buscar
			return null;
		}
		
		for(BeanParamWS p: this.parametros){
			if(p.getNombre().equalsIgnoreCase(key)){
				if(product.length() == 0){
					// es el parametro que estamos buscando
					return p.getValue();
				}
				else{
					if(p.getProducto().equalsIgnoreCase(product))
						// es el parametro que estamos buscando
						return p.getValue();
				}
			}
		}
		// no he encontrado ese parametro
		return null;
	}

	
	/**
	 * Devuelve el valor del parametro enviado como clave <key>
	 * @param String key
	 * @return String
	 */
//	public String getValueParametro(String key, String prod){
	public String getValueParametro(String key){
		if(this.parametros == null || this.parametros.length == 0){
			// no hay parametros para buscar
			return null;
		}
		
		for(BeanParamWS p: this.parametros){
			if(p.getNombre().equalsIgnoreCase(key)){
				// es el parametro que estamos buscando
				return p.getValue();
			}
		}
		// no he encontrado ese parametro
		return null;
	}
}
