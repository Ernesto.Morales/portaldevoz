package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "postProceso")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "transferencia"})
public class BeanInfoPostProceso {

	@XmlElement(name = "transferencia")
	private BeanInfoTransferencia transferencia;

	public BeanInfoTransferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(BeanInfoTransferencia transferencia) {
		this.transferencia = transferencia;
	}

	@Override
	public String toString() {
		return "BeanInfoPostProceso [transferencia=" + transferencia + "]";
	}

}
