package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.kranon.bean.BeanControlHorario;

/**
 * Bean para la configuracion de un Servicio. Se corresponde con la tabla TGCE001_PARAM_SVC
 * 
 */
@XmlRootElement(name = "servicio")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanServicio {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServIVR;

	/* Parametros de configuracion */
	// @XmlJavaTypeAdapter(ArrayParamAdapter.class)
	// @XmlElement(name = "configuracion")
	// private BeanParam[] configuracion;

	/* Parametros de configuracion */
	@XmlElement(name = "configuracion")
	private BeanConfigServicio configuracion;

	/* Horarios del servicio */
	// @XmlElementWrapper(name = "control_horario")
	// @XmlElement(name = "horario")
	// private BeanHorario[] horario;

	/* Horarios del servicio */
	// @XmlElementWrapper(name = "control_horario")
	@XmlElement(name = "control_horario")
	private BeanControlHorario horario;

	/* Menus y opciones del servicio */
	@XmlElementWrapper(name = "opc_menus")
	@XmlElement(name = "menu")
	private BeanMenuServ[] menus;

	/* Transferencias asociadas al servicio */
	@XmlElementWrapper(name = "transferencia")
	@XmlElement(name = "schema")
	private BeanTransferServ[] schemas;

	/* Saludos asociados al servicio */
	@XmlElementWrapper(name = "saludos")
	@XmlElement(name = "saludo")
	private BeanSaludo[] saludos;

	/* Promociones asociadas al servicio */
	@XmlElementWrapper(name = "promociones")
	@XmlElement(name = "promocion")
	private BeanPromocion[] promociones;

	public String getCodServIVR() {
		return codServIVR;
	}

	public void setCodServIVR(String codServIVR) {
		this.codServIVR = codServIVR;
	}

	public BeanConfigServicio getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(BeanConfigServicio configuracion) {
		this.configuracion = configuracion;
	}

	public BeanMenuServ[] getMenus() {
		return menus;
	}

	public BeanControlHorario getHorario() {
		return horario;
	}

	public void setHorario(BeanControlHorario horario) {
		this.horario = horario;
	}

	public void setMenus(BeanMenuServ[] menus) {
		this.menus = menus;
	}

	public BeanTransferServ[] getSchemas() {
		return schemas;
	}

	public void setSchemas(BeanTransferServ[] schemas) {
		this.schemas = schemas;
	}

	public BeanSaludo[] getSaludos() {
		return saludos;
	}

	public void setSaludos(BeanSaludo[] saludos) {
		this.saludos = saludos;
	}

	public BeanPromocion[] getPromociones() {
		return promociones;
	}

	public void setPromociones(BeanPromocion[] promociones) {
		this.promociones = promociones;
	}


	@Override
	public String toString() {
		return String.format("BeanServicio [codServIVR=%s, configuracion=%s, horario=%s, menus=%s, schemas=%s, saludos=%s, promociones=%s]",
				codServIVR, configuracion, horario, Arrays.toString(menus), Arrays.toString(schemas), Arrays.toString(saludos),
				Arrays.toString(promociones));
	}
	// *********************************** //

	/**
	 * Devuelve el valor del parametro <key> indicado, dentro del array de configuracion
	 * 
	 * @param {@link String} key
	 * @return {@link String} value
	 */
	// public String getKeyValueConfig(String key){
	//
	// if(this.configuracion == null || this.configuracion.length == 0){
	// return null;
	// }
	// String value="";
	// // recorro los parametros
	// for(BeanParam param: this.configuracion){
	// if(param.getKey().equals(key)){
	// // he encontrado el parametro que busco, con la key indicada
	// value = param.getValue();
	// break;
	// }
	// }
	// return value;
	//
	// }

	public BeanTransferServ getSchema(String codEsquema) {
		BeanTransferServ[] schemas = this.getSchemas();
		for (BeanTransferServ t : schemas) {
			if (t.getCodEsquema().equalsIgnoreCase(codEsquema)) {
				return t;
			}
		}
		return null;
	}

	

	public BeanSaludo getSaludo(String diaHoy) {

		BeanSaludo saludoResul = null;
		int diaHoyInt = Integer.parseInt(diaHoy);
		for (BeanSaludo saludo : this.getSaludos()) {
			String fechaInicio = saludo.getFechaInicio();
			String fechaFin = saludo.getFechaFin();

			int fechaInicioInt = Integer.parseInt(fechaInicio);
			int fechaFinInt = Integer.parseInt(fechaFin);

			if (fechaInicioInt < diaHoyInt && diaHoyInt <= fechaFinInt) {
				// hay un saludo temporal activo para el dia de hoy
				saludoResul = saludo;
				break;
			}

		}
		return saludoResul;
	}

}
