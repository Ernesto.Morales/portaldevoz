package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion propia del Reconocimiento DTMF de un Menu.
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "bargeinDtmfActivo", "interdigitTimeoutDtmf", "timeoutDtmf" })
public class BeanConfigRecDtmf {

	/* Bargein */
	@XmlElement(name = "bargein_dtmf_activo")
	private String bargeinDtmfActivo;

	/* Interdigit timeout */
	@XmlElement(name = "interdigit_timeout_dtmf")
	private String interdigitTimeoutDtmf;

	/* Timeout */
	@XmlElement(name = "timeout_dtmf")
	private String timeoutDtmf;

	public String getBargeinDtmfActivo() {
		return bargeinDtmfActivo;
	}

	public void setBargeinDtmfActivo(String bargeinDtmfActivo) {
		this.bargeinDtmfActivo = bargeinDtmfActivo;
	}

	public String getInterdigitTimeoutDtmf() {
		return interdigitTimeoutDtmf;
	}

	public void setInterdigitTimeoutDtmf(String interdigitTimeoutDtmf) {
		this.interdigitTimeoutDtmf = interdigitTimeoutDtmf;
	}

	public String getTimeoutDtmf() {
		return timeoutDtmf;
	}

	public void setTimeoutDtmf(String timeoutDtmf) {
		this.timeoutDtmf = timeoutDtmf;
	}

	@Override
	public String toString() {
		return "BeanConfigRecDtmf [bargeinDtmfActivo=" + bargeinDtmfActivo + ", interdigitTimeoutDtmf=" + interdigitTimeoutDtmf + ", timeoutDtmf="
				+ timeoutDtmf + "]";
	}

}
