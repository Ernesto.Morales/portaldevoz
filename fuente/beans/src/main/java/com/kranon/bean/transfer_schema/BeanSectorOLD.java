package com.kranon.bean.transfer_schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanSectorOLD {

	/* Codigo del sector */
	@XmlAttribute(name = "cd_sector")
	private String codSector;

	/* Codigo transferencia label */
	@XmlElement(name = "cod_transfer_label")
	private String codTransferLabel;
	
	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;



	public String getCodSector() {
		return codSector;
	}

	public void setCodSector(String codSector) {
		this.codSector = codSector;
	}

	public String getCodTransferLabel() {
		return codTransferLabel;
	}

	public void setCodTransferLabel(String codTransferLabel) {
		this.codTransferLabel = codTransferLabel;
	}

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanSector [codSector=" + codSector + ", codTransferLabel="
				+ codTransferLabel + ", modif=" + modif + "]";
	}

}
