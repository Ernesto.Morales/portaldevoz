package com.kranon.bean.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para los distintos entornos: desarrollo, integrado, produccion que almacena sus url y parametros fijos para cada servicio web
 * 
 * @author abalfaro
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanEntorno {

	/**
	 * ************************************************************************* PReSTAMOS
	 * *************************************************************************
	 */
	/* Nombre Entorno */
	@XmlAttribute(name = "nombre")
	private String nombreEntorno;

	/* Servicio de Granting Ticket */
	@XmlElement(name = "grantingTicket")
	private BeanDetallesEntorno grantingTicket;

	/* Servicio de validateOTP*/
	@XmlElement(name = "validateOTP")
	private BeanDetallesEntorno validateOTP;
	
	/* Servicio de validateOTP*/
	@XmlElement(name = "saveOTP")
	private BeanDetallesEntorno saveOTP;
	
	/* Servicio de apiOpenPay */
	@XmlElement(name = "apiOpenPay")
	private BeanDetallesEntorno apiOpenPay;
	
	/* Servicio de getCotizacion */
	@XmlElement(name = "getCotizacion")
	private BeanDetallesEntorno getCotizacion;
	
	/* Servicio de getCotizacion */
	@XmlElement(name = "getCampanas")
	private BeanDetallesEntorno getCampanas;
	
	/* Servicio para conexion CTI */
	@XmlElement(name = "integracionCTI")
	private BeanDetallesEntorno integracionCTI;
	
	@XmlElement(name = "URI_TRANSFERENCIA")
	private BeanDetallesEntorno uriTransferencia;

	/* Servicio de deleteTicket */
	@XmlElement(name = "deleteTicket")
	private BeanDetallesEntorno deleteTicket;
	
	/* Servicio de deleteTsec */
	@XmlElement(name = "deleteTsec")
	private BeanDetallesEntorno deleteTsec;
	
	/* Servicio de proxyMGCET001010101MX */
	@XmlElement(name = "proxyMGCET001010101MX")
	private BeanDetallesEntorno proxyMGCET001010101MX;
	
	public String getNombreEntorno() {
		return nombreEntorno;
	}

	public void setNombreEntorno(String nombreEntorno) {
		this.nombreEntorno = nombreEntorno;
	}

	public BeanDetallesEntorno getGrantingTicket() {
		return grantingTicket;
	}

	public void setGrantingTicket(BeanDetallesEntorno grantingTicket) {
		this.grantingTicket = grantingTicket;
	}
	
	public BeanDetallesEntorno getValidateOTP() {
		return validateOTP;
	}

	public void setValidateOTP(BeanDetallesEntorno validateOTP) {
		this.validateOTP = validateOTP;
	}
	
	/**
	 * @return the apiOpenPay
	 */
	public BeanDetallesEntorno getApiOpenPay() {
		return apiOpenPay;
	}

	/**
	 * @param apiOpenPay the apiOpenPay to set
	 */
	public void setApiOpenPay(BeanDetallesEntorno apiOpenPay) {
		this.apiOpenPay = apiOpenPay;
	}

	
	/**
	 * @return the deleteTicket
	 */
	public BeanDetallesEntorno getDeleteTicket() {
		return deleteTicket;
	}

	/**
	 * @param deleteTicket the deleteTicket to set
	 */
	public void setDeleteTicket(BeanDetallesEntorno deleteTicket) {
		this.deleteTicket = deleteTicket;
	}

	/**
	 * @return the deleteTsec
	 */
	public BeanDetallesEntorno getDeleteTsec() {
		return deleteTsec;
	}

	/**
	 * @param deleteTsec the deleteTsec to set
	 */
	public void setDeleteTsec(BeanDetallesEntorno deleteTsec) {
		this.deleteTsec = deleteTsec;
	}

	
	
	/**
	 * @return the integracionCTI
	 */
	public BeanDetallesEntorno getIntegracionCTI() {
		return integracionCTI;
	}

	/**
	 * @param integracionCTI the integracionCTI to set
	 */
	public void setIntegracionCTI(BeanDetallesEntorno integracionCTI) {
		this.integracionCTI = integracionCTI;
	}

	/**
	 * @return the uriTransferencia
	 */
	public BeanDetallesEntorno getUriTransferencia() {
		return uriTransferencia;
	}

	/**
	 * @param uriTransferencia the uriTransferencia to set
	 */
	public void setUriTransferencia(BeanDetallesEntorno uriTransferencia) {
		this.uriTransferencia = uriTransferencia;
	}

	
	
	/**
	 * @return the proxyMGCET001010101MX
	 */
	public BeanDetallesEntorno getProxyMGCET001010101MX() {
		return proxyMGCET001010101MX;
	}

	/**
	 * @param proxyMGCET001010101MX the proxyMGCET001010101MX to set
	 */
	public void setProxyMGCET001010101MX(BeanDetallesEntorno proxyMGCET001010101MX) {
		this.proxyMGCET001010101MX = proxyMGCET001010101MX;
	}

	
	
	/**
	 * @return the getCotizacion
	 */
	public BeanDetallesEntorno getGetCotizacion() {
		return getCotizacion;
	}

	/**
	 * @param getCotizacion the getCotizacion to set
	 */
	public void setGetCotizacion(BeanDetallesEntorno getCotizacion) {
		this.getCotizacion = getCotizacion;
	}
	
	/**
	 * @return the getCampanas
	 */
	public BeanDetallesEntorno getGetCampanas() {
		return getCampanas;
	}

	/**
	 * @param getCampanas the getCampanas to set
	 */
	public void setGetCampanas(BeanDetallesEntorno getCampanas) {
		this.getCampanas = getCampanas;
	}

	
	/**
	 * @return the saveOTP
	 */
	public BeanDetallesEntorno getSaveOTP() {
		return saveOTP;
	}

	/**
	 * @param saveOTP the saveOTP to set
	 */
	public void setSaveOTP(BeanDetallesEntorno saveOTP) {
		this.saveOTP = saveOTP;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanEntorno [nombreEntorno=" + nombreEntorno
				+ ", grantingTicket=" + grantingTicket + ", validateOTP="
				+ validateOTP + "openpay="+apiOpenPay+"getCampanas="+getCampanas+"saveOTP="+saveOTP+"]";
	}

	
}
