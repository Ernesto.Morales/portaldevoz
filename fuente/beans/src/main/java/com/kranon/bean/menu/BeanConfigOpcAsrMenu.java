package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.kranon.bean.BeanGramatica;

/**
 * Bean para la configuracion ASR de una opcion de un Menu
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "gramaticaAsr", "promptIntroAsr", "promptAsr", "promptOutroAsr" })
public class BeanConfigOpcAsrMenu {

	/* Gramatica asr */
	@XmlElement(name = "grammar_asr")
	private BeanGramatica gramaticaAsr;

	/* Prompt en primer lugar */
	@XmlElement(name = "prompt_intro_asr")
	private String promptIntroAsr;

	/* Prompt en segundo lugar */
	@XmlElement(name = "prompt_asr")
	private String promptAsr;

	/* Prompt en tercer lugar */
	@XmlElement(name = "prompt_outro_asr")
	private String promptOutroAsr;

	public BeanGramatica getGramaticaAsr() {
		return gramaticaAsr;
	}

	public void setGramaticaAsr(BeanGramatica gramaticaAsr) {
		this.gramaticaAsr = gramaticaAsr;
	}

	public String getPromptIntroAsr() {
		return promptIntroAsr;
	}

	public void setPromptIntroAsr(String promptIntroAsr) {
		this.promptIntroAsr = promptIntroAsr;
	}

	public String getPromptAsr() {
		return promptAsr;
	}

	public void setPromptAsr(String promptAsr) {
		this.promptAsr = promptAsr;
	}

	public String getPromptOutroAsr() {
		return promptOutroAsr;
	}

	public void setPromptOutroAsr(String promptOutroAsr) {
		this.promptOutroAsr = promptOutroAsr;
	}

	@Override
	public String toString() {
		return "BeanConfigOpcAsrMenu [gramaticaAsr=" + gramaticaAsr + ", promptIntroAsr=" + promptIntroAsr + ", promptAsr=" + promptAsr
				+ ", promptOutroAsr=" + promptOutroAsr + "]";
	}

}
