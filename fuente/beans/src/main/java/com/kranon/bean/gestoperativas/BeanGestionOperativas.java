package com.kranon.bean.gestoperativas;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kranon.beanadapter.MapAdapterOperativa;

/**
 * Bean para contener la informacion del XML <codServ>-GESTION-OPERATIVAS.xml
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "gestion_operativa")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanGestionOperativas {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_servivr")
	private String codServivr;

	@XmlJavaTypeAdapter(MapAdapterOperativa.class)
	@XmlElement(name = "operativas")
	//private BeanInfoOperativa[] operativa;
	private HashMap <String, BeanOperativa> tablaOperativas;
	

	public String getCodServivr() {
		return codServivr;
	}

	public void setCodServivr(String codServivr) {
		this.codServivr = codServivr;
	}


	public HashMap<String, BeanOperativa> getTablaOperativas() {
		return tablaOperativas;
	}

	public void setTablaOperativas(
			HashMap<String, BeanOperativa> tablaOperativas) {
		this.tablaOperativas = tablaOperativas;
	}

	@Override
	public String toString() {
		return "BeanGestionOperativas [codServivr=" + codServivr
				+ ", tablaOperativas=" + tablaOperativas + "]";
	}

}
