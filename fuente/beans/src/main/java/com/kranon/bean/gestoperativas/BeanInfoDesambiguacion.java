package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "desambiguacion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "menu"})
public class BeanInfoDesambiguacion {

	
	@XmlElement(name = "menu")
	private BeanInfoMenu[] menu;
	
	
	public BeanInfoMenu[] getMenu() {
		return menu;
	}

	public void setMenu(BeanInfoMenu[] menu) {
		this.menu = menu;
	}

	@Override
	public String toString() {
		return "BeanInfoDesambiguacion [menu=" + Arrays.toString(menu) + "]";
	}
	
	// ****************************************************//
	
	/**
	 * Devuelve el menu de desambiguacion para el nombre de controlador actual
	 * 
	 * @param nombre menu
	 * @return
	 */
	public String buscaMenu(String nombreControlador) {
		String nombreMenu = null;
		for (BeanInfoMenu infoMenu : this.menu) {
			if (infoMenu.getControlador().equals(nombreControlador)) {
				// hemos encontrado el controlador que buscamos
				nombreMenu = infoMenu.getNombre();
				
			}
		}
		if(nombreMenu != null && !nombreMenu.equals("")){
			// he encontrado un menu
			// elimino espacios por si acaso
			nombreMenu = nombreMenu.trim();
			nombreMenu = nombreMenu.replaceAll("\\s+","");
			return nombreMenu;
		} else {
			// no hemos encontrado menu por controlador, miro si hay uno por defecto
			for (BeanInfoMenu infoMenu : this.menu) {
				if (infoMenu.getControlador().equals("")) {
					// hemos encontrado el menu por defecto
					nombreMenu = infoMenu.getNombre();
					// elimino espacios por si acaso
					nombreMenu = nombreMenu.trim();
					nombreMenu = nombreMenu.replaceAll("\\s+","");
					
					return nombreMenu;
				}
			}
		}
		return "";
	}

}
