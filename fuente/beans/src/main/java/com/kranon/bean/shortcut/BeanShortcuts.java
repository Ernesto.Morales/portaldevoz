package com.kranon.bean.shortcut;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para contener la informacion del XML <codServ>-SEGUROSOPV1GESTION-SHORTCUT.xml
 * 
 * @author abalfaro
 *
 */
@XmlRootElement(name = "gestion_shorcut")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanShortcuts {
		/* Codigo del servicio */
		@XmlAttribute(name = "cd_servivr")
		private String codServivr;
		
		/* Configuracion de los shorcut appLineaBancomer */
		@XmlElementWrapper(name = "shortcuts")
		@XmlElement(name = "shorcut")
		private List<BeanShortCut> shortcuts;		
			
		public BeanShortcuts() {
			super();
		}

		public BeanShortcuts(String codServivr, List<BeanShortCut> shortcuts) {
			super();
			this.codServivr = codServivr;
			this.shortcuts = shortcuts;
		}

		public String getCodServivr() {
			return codServivr;
		}

		public void setCodServivr(String codServivr) {
			this.codServivr = codServivr;
		}

		public List<BeanShortCut> getShortcuts() {
			return shortcuts;
		}

		public void setShortcuts(List<BeanShortCut> shortcuts) {
			this.shortcuts = shortcuts;
		}
		public void limpiar(){
			for (BeanShortCut shortcut : shortcuts) {
				shortcut.setLigaRetornoL();
			}
		}
		@Override
		public String toString() {
			return "BeanShortcuts [codServivr=" + codServivr + ", shortcuts="
					+ shortcuts + "]";
		}
		
		
}
