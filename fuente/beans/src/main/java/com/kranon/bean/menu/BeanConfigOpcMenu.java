package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion de una opcion de un Menu
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "posicion", "opcionActiva", "opcionVisible", "opcionMenuDesc", "modoReproduccion", "accion" })
public class BeanConfigOpcMenu {

	/* Posicion de la opcion */
	@XmlElement(name = "posicion")
	private String posicion;

	/* Activacion de la opcion */
	@XmlElement(name = "opcion_activa")
	private String opcionActiva;

	/* Visibilidad de la opcion */
	@XmlElement(name = "opcion_visible")
	private String opcionVisible;

	/* Descripcion de la opcion */
	@XmlElement(name = "opcion_menu_desc")
	private String opcionMenuDesc;

	/* Modo Reproduccion */
	@XmlElement(name = "modo_reproduccion_opc")
	private String modoReproduccion;

	/* Accion de la opcion */
	@XmlElement(name = "accion")
	private String accion;

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public String getOpcionActiva() {
		return opcionActiva;
	}

	public void setOpcionActiva(String opcionActiva) {
		this.opcionActiva = opcionActiva;
	}

	public String getOpcionVisible() {
		return opcionVisible;
	}

	public void setOpcionVisible(String opcionVisible) {
		this.opcionVisible = opcionVisible;
	}

	public String getOpcionMenuDesc() {
		return opcionMenuDesc;
	}

	public void setOpcionMenuDesc(String opcionMenuDesc) {
		this.opcionMenuDesc = opcionMenuDesc;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Override
	public String toString() {
		return "BeanConfigOpcMenu [posicion=" + posicion + ", opcionActiva=" + opcionActiva + ", opcionVisible=" + opcionVisible
				+ ", opcionMenuDesc=" + opcionMenuDesc + ", modoReproduccion=" + modoReproduccion + ", accion=" + accion + "]";
	}

}
