package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Bean para la configuracion propia del Reconocimiento ASR de un Menu.
 * 
 */
@XmlRootElement(name = "configuracion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "bargeinAsrActivo", "propertyCompleteTimeoutAsr", "propertyConfidenceLevelAsr", "propertyIncompleteTimeoutAsr",
		"propertyMaxspeechTimeoutAsr", "propertySensitivityAsr", "propertyTimeoutAsr" })
public class BeanConfigRecAsr {

	/* Bargein */
	@XmlElement(name = "bargein_asr_activo")
	private String bargeinAsrActivo;

	/* Complete timeout */
	@XmlElement(name = "property_complete_timeout_asr")
	private String propertyCompleteTimeoutAsr;

	/* Nivel de confianza */
	@XmlElement(name = "property_confidence_level_asr")
	private String propertyConfidenceLevelAsr;

	/* Incomplete timeout */
	@XmlElement(name = "property_incomplete_timeout_asr")
	private String propertyIncompleteTimeoutAsr;

	/* Maxspeech timeout */
	@XmlElement(name = "property_maxspeech_timeout_asr")
	private String propertyMaxspeechTimeoutAsr;

	/* Sensibilidad */
	@XmlElement(name = "property_sensitivity_asr")
	private String propertySensitivityAsr;

	/* Timeout */
	@XmlElement(name = "property_timeout_asr")
	private String propertyTimeoutAsr;

	public String getBargeinAsrActivo() {
		return bargeinAsrActivo;
	}

	public void setBargeinAsrActivo(String bargeinAsrActivo) {
		this.bargeinAsrActivo = bargeinAsrActivo;
	}

	public String getPropertyCompleteTimeoutAsr() {
		return propertyCompleteTimeoutAsr;
	}

	public void setPropertyCompleteTimeoutAsr(String propertyCompleteTimeoutAsr) {
		this.propertyCompleteTimeoutAsr = propertyCompleteTimeoutAsr;
	}

	public String getPropertyConfidenceLevelAsr() {
		return propertyConfidenceLevelAsr;
	}

	public void setPropertyConfidenceLevelAsr(String propertyConfidenceLevelAsr) {
		this.propertyConfidenceLevelAsr = propertyConfidenceLevelAsr;
	}

	public String getPropertyIncompleteTimeoutAsr() {
		return propertyIncompleteTimeoutAsr;
	}

	public void setPropertyIncompleteTimeoutAsr(String propertyIncompleteTimeoutAsr) {
		this.propertyIncompleteTimeoutAsr = propertyIncompleteTimeoutAsr;
	}

	public String getPropertyMaxspeechTimeoutAsr() {
		return propertyMaxspeechTimeoutAsr;
	}

	public void setPropertyMaxspeechTimeoutAsr(String propertyMaxspeechTimeoutAsr) {
		this.propertyMaxspeechTimeoutAsr = propertyMaxspeechTimeoutAsr;
	}

	public String getPropertySensitivityAsr() {
		return propertySensitivityAsr;
	}

	public void setPropertySensitivityAsr(String propertySensitivityAsr) {
		this.propertySensitivityAsr = propertySensitivityAsr;
	}

	public String getPropertyTimeoutAsr() {
		return propertyTimeoutAsr;
	}

	public void setPropertyTimeoutAsr(String propertyTimeoutAsr) {
		this.propertyTimeoutAsr = propertyTimeoutAsr;
	}

	@Override
	public String toString() {
		return "BeanConfigRecAsr [bargeinAsrActivo=" + bargeinAsrActivo + ", propertyCompleteTimeoutAsr=" + propertyCompleteTimeoutAsr
				+ ", propertyConfidenceLevelAsr=" + propertyConfidenceLevelAsr + ", propertyIncompleteTimeoutAsr=" + propertyIncompleteTimeoutAsr
				+ ", propertyMaxspeechTimeoutAsr=" + propertyMaxspeechTimeoutAsr + ", propertySensitivityAsr=" + propertySensitivityAsr
				+ ", propertyTimeoutAsr=" + propertyTimeoutAsr + "]";
	}

}
