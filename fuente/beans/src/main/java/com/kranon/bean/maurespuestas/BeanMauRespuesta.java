package com.kranon.bean.maurespuestas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanMauRespuesta {
	/* VDN de entrada para decidir el saludo */
	@XmlAttribute(name = "opcion")
	private String opcion;

	/* Modo de reproduccion del wav */
	@XmlAttribute(name = "modo")
	private String modoReproduccion;

	
	public BeanMauRespuesta() {
		super();
	}

	public BeanMauRespuesta(String opcion,
			String modoReproduccion, String value) {
		super();
		this.opcion = opcion;
		this.modoReproduccion = modoReproduccion;
		this.value = value;
	}

	/* Locucion a reproducir */
	@XmlValue
	private String value;
	
	

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	public String getModoReproduccion() {
		return modoReproduccion;
	}

	public void setModoReproduccion(String modoReproduccion) {
		this.modoReproduccion = modoReproduccion;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanSaludoControlador [opcion=" + opcion + ", modoReproduccion=" + modoReproduccion + ", value=" + value
				+ "]";
	}

}
