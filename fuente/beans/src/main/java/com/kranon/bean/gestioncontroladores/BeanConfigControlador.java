package com.kranon.bean.gestioncontroladores;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanConfigControlador {

	/* VDN Origen del menu */
	@XmlAttribute(name = "vdnOrigen")
	private String vdnOrigen;

	/* xferId del menu */
	@XmlAttribute(name = "xferId")
	private String xferId;

	/* Segmento del Cliente */
	@XmlAttribute(name = "segmentoCliente")
	private String segmentoCliente;
	
	@XmlValue
	private String nombreControlador;

	public String getVdnOrigen() {
		return vdnOrigen;
	}

	public void setVdnOrigen(String vdnOrigen) {
		this.vdnOrigen = vdnOrigen;
	}

	public String getXferId() {
		return xferId;
	}

	public void setXferId(String xferId) {
		this.xferId = xferId;
	}

	public String getSegmentoCliente() {
		return segmentoCliente;
	}

	public void setSegmentoCliente(String segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}
	
	public String getNombreControlador() {
		return nombreControlador;
	}

	public void setNombreControlador(String nombreControlador) {
		this.nombreControlador = nombreControlador;
	}

	@Override
	public String toString() {
		return "BeanConfigControlador [vdnOrigen=" + vdnOrigen + ", xferId=" + xferId + ", segmentoCliente=" + segmentoCliente + ", nombreControlador=" + nombreControlador + "]";
	}

}
