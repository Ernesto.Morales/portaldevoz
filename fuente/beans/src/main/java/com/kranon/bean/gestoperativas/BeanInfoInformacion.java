package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "informacion")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "locuciones", "accion", "excepcion" })
public class BeanInfoInformacion {

	@XmlElementWrapper(name = "locuciones")
	@XmlElement(name = "locucion")
	private BeanLocGestionOperativa[] locuciones;

	@XmlElement(name = "accion")
	private String accion;

	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanInfoExcepcion[] excepcion;

	public BeanLocGestionOperativa[] getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocGestionOperativa[] locuciones) {
		this.locuciones = locuciones;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public BeanInfoExcepcion[] getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(BeanInfoExcepcion[] excepcion) {
		this.excepcion = excepcion;
	}

	@Override
	public String toString() {
		return "BeanInfoInformacion [locuciones=" + Arrays.toString(locuciones) + ", accion=" + accion + ", excepcion=" + Arrays.toString(excepcion)
				+ "]";
	}

	// ********************************************************//

	/**
	 * Ordenar las locuciones por id de menor a mayor y devuelve una cadena con los nombres de todas las locuciones separadas por |
	 * 
	 * @param {@link BeanLocGestionOperativa[]} locuciones
	 * @return {@link String}
	 * @throws Exception
	 */
	public String getNombresDeLocuciones() {

		String cadenasLocuciones = "";

		BeanLocGestionOperativa[] promptsOrdenados;

		try {
			promptsOrdenados = this.getLocuciones();

			for (int i = 0; i < (promptsOrdenados.length - 1); i++) {
				for (int j = i + 1; j < promptsOrdenados.length; j++) {
					int posicionI = Integer.parseInt(promptsOrdenados[i].getId());
					int posicionJ = Integer.parseInt(promptsOrdenados[j].getId());
					if (posicionI > posicionJ) {
						BeanLocGestionOperativa variableauxiliar = promptsOrdenados[i];
						promptsOrdenados[i] = promptsOrdenados[j];
						promptsOrdenados[j] = variableauxiliar;
					}
				}
			}
			for (int i = 0; i < promptsOrdenados.length; i++) {
				cadenasLocuciones = cadenasLocuciones + promptsOrdenados[i].getNombre();
				if (i != (promptsOrdenados.length - 1)) {
					// hay mas elementos, concateno el pipe
					cadenasLocuciones = cadenasLocuciones + "|";
				}
			}

		} catch (Exception e) {
			return null;
		}
		return cadenasLocuciones;
	}

}
