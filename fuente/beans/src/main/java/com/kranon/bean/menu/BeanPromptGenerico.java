package com.kranon.bean.menu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"id", "modo" , "uso" , "emphasis" , "silence" })
public class BeanPromptGenerico {

	/* Orden del prompt */
	@XmlAttribute(name = "id")
	private String id;

	/* Modo del prompt TTS, WAV */
	@XmlAttribute(name = "modo")
	private String modo;

	/* Uso del prompt NORMAL, DIGITS, DATE */
	@XmlAttribute(name = "uso")
	private String uso;

	/* Uso del prompt  STRONG MODERATE REDUCED NONE*/
	@XmlAttribute(name = "emphasis")
	private String emphasis;
	
	/* Uso del prompt  Tiempo en ms */
	@XmlAttribute(name = "silence")
	private String silence;
	
	/* Value del prompt */
	@XmlValue
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getUso() {
		return uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}
	
	public String getEmphasis() {
		return emphasis;
	}

	public void setEmphasis(String emphasis) {
		this.emphasis = emphasis;
	}

	public String getSilence() {
		return silence;
	}

	public void setSilence(String silence) {
		this.silence = silence;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BeanPromptGenerico [id=" + id + ", modo=" + modo + ", uso="
				+ uso + ", emphasis=" + emphasis + ", silence=" + silence
				+ ", value=" + value + "]";
	}
	
	/******************************************/
	
	/**
	 * Elimino saltos de lnea en locuciones
	 * @return
	 */
	public String getValueFormateado() {
		if(this.value == null){
			return null;
		}
		return value.replaceAll("\n", ""); 
	}
	



}
