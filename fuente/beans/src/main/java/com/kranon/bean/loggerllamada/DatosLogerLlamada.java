package com.kranon.bean.loggerllamada;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loggerLlamada")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosLogerLlamada {
	@XmlElement(name = "idllam")
	private String idLlamada;
	@XmlElement(name = "ini")
	private String  inicio;
	@XmlElement(name = "idSer")
	private String idServicio;
	@XmlElement(name = "idEle")
	private String idElemento;
	@XmlElement(name = "numOr")
	private String numOrigen;
	@XmlElement(name = "numDes")
	private String numDestino;
	@XmlElement(name = "idioma")
	private String idioma;
	@XmlElement(name = "tipo")
	private String tipo;
	@XmlElement(name = "tipoOpc")
	private String tipoOpc;
	@XmlElementWrapper(name = "Etapas")
	@XmlElement(name = "Etapa")
	private List<Paso> pasos= new ArrayList<Paso>();

	public DatosLogerLlamada() {
		super();
	}


	public DatosLogerLlamada(String idLlamada, String inicio,
			String idServicio, String idElemento, String numOrigen,
			String numDestino, String idioma, String tipo, String tipoOpc,
			List<Paso> pasos) {
		super();
		this.idLlamada = idLlamada;
		this.inicio = inicio;
		this.idServicio = idServicio;
		this.idElemento = idElemento;
		this.numOrigen = numOrigen;
		this.numDestino = numDestino;
		this.idioma = idioma;
		this.tipo = tipo;
		this.tipoOpc = tipoOpc;
		this.pasos = pasos;
	}


	public String getIdLlamada() {
		return idLlamada;
	}

	public void setIdLlamada(String idLlamada) {
		this.idLlamada = idLlamada;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getIdElemento() {
		return idElemento;
	}

	public void setIdElemento(String idElemento) {
		this.idElemento = idElemento;
	}

	public String getNumOrigen() {
		return numOrigen;
	}

	public void setNumOrigen(String numOrigen) {
		this.numOrigen = numOrigen;
	}

	public String getNumDestino() {
		return numDestino;
	}

	public void setNumDestino(String numDestino) {
		this.numDestino = numDestino;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Paso> getPasos() {
		return pasos;
	}

	public void setPasos(List<Paso> pasos) {
		this.pasos = pasos;
	}

	public String getTipoOpc() {
		return tipoOpc;
	}

	public void setTipoOpc(String tipoOpc) {
		this.tipoOpc = tipoOpc;
	}

	@Override
	public String toString() {
		return "DatosLogerLlamada [idLlamada=" + idLlamada + ", inicio="
				+ inicio + ", idServicio=" + idServicio + ", idElemento="
				+ idElemento + ", numOrigen=" + numOrigen + ", numDestino="
				+ numDestino + ", idioma=" + idioma + ", tipo=" + tipo
				+ ", tipoOpc=" + tipoOpc + ", pasos=" + pasos + "]";
	}
	
	
}
