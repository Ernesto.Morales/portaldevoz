package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "activo", "estadistica", "ruta", "instrumentos", "excepcion", "accion" })
public class BeanProcesoOperativa {

	/* Identificador del proceso */
	@XmlAttribute(name = "id")
	private String id;

	/* Activacion del proceso */
	@XmlAttribute(name = "activo")
	private String activo;

	/* Estadisticas del proceso */
	@XmlAttribute(name = "estadistica")
	private String estadistica;

	@XmlElement(name = "ruta")
	private String ruta;

	/* Instrumentos del proceso */
	@XmlElementWrapper(name = "instrumentos")
	@XmlElement(name = "instrumento")
	private BeanInstrumento[] instrumentos;

	/* Acciones del proceso */
	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanInfoExcepcion[] excepcion;

	@XmlElement(name = "accion")
	private String accion;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public BeanInstrumento[] getInstrumentos() {
		return instrumentos;
	}

	public void setInstrumentos(BeanInstrumento[] instrumentos) {
		this.instrumentos = instrumentos;
	}

	public BeanInfoExcepcion[] getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(BeanInfoExcepcion[] excepcion) {
		this.excepcion = excepcion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Override
	public String toString() {
		return String.format("BeanProcesoOperativa [id=%s, activo=%s, estadistica=%s, ruta=%s, instrumentos=%s, excepcion=%s, accion=%s]", id,
				activo, estadistica, ruta, Arrays.toString(instrumentos), Arrays.toString(excepcion), accion);
	}

	// ******************************************//

	/**
	 * Devuelve la accion a realizar por el nombre recogido como parametro
	 * 
	 * @param {@link String} nombreAccion
	 * @return {@link String} accion a realizar
	 */
	// public String getResultadoAccion(String nombreAccion) {
	//
	// if (acciones == null) {
	// // no hay acciones
	// return null;
	// }
	//
	// for (BeanAccionesProcesoOperativa acc : acciones) {
	// if (acc.getNombre() != null && acc.getNombre().equalsIgnoreCase(nombreAccion)) {
	// return acc.getValue();
	// }
	// }
	// // si no encuentro la accion
	// return null;
	//
	// }

	/**
	 * Devuelve todos los intentos de los instrumentos de activos ordenados por id en formato XXX|XXX|XXX Devuelvo null si algo ha fallado. Devuelvo
	 * vacio si no hay instrumentos activos.
	 * 
	 * @return {@link String} intentos instrumentos activos separados por '|'
	 */
	public String getNumIntentosInstrumentosActivos() {

		if (instrumentos == null) {
			// no hay instrumentos
			return "";
		}

		// primero ordeno todos los instrumentos por id
		BeanInstrumento[] insOrdenados = this.ordenarInstrumentossPorId(instrumentos);
		if (insOrdenados == null) {
			// algo ha fallado al ordenar
			return null;
		}
		String insActivos = "";
		for (BeanInstrumento ins : instrumentos) {
			// recorro todos los intrumentos y me guardo los activos
			if (ins.getActivo() != null && ins.getActivo().equalsIgnoreCase("ON")) {
				if (!insActivos.equals("")) {
					insActivos = insActivos + "|";
				}
				insActivos = insActivos + ins.getNumIntentos();
			}
		}
		return insActivos;

	}

	/**
	 * Devuelve todos los instrumentos de activos ordenados por id en formato XXX|XXX|XXX Devuelvo null si algo ha fallado. Devuelvo vacio si no hay
	 * instrumentos activos.
	 * 
	 * @return {@link String} instrumentos activos separados por '|'
	 */
	public String getInstrumentosActivos() {

		if (instrumentos == null) {
			// no hay instrumentos
			return "";
		}

		// primero ordeno todos los instrumentos por id
		BeanInstrumento[] insOrdenados = this.ordenarInstrumentossPorId(instrumentos);
		if (insOrdenados == null) {
			// algo ha fallado al ordenar
			return null;
		}
		String insActivos = "";
		for (BeanInstrumento ins : instrumentos) {
			// recorro todos los intrumentos y me guardo los activos
			if (ins.getActivo() != null && ins.getActivo().equalsIgnoreCase("ON")) {
				if (!insActivos.equals("")) {
					insActivos = insActivos + "|";
				}
				insActivos = insActivos + ins.getNombre();
			}
		}
		return insActivos;

	}

	/**
	 * Ordenar los instrumentos por id de menor a mayor
	 * 
	 * @param {@link BeanInstrumento[]} instrumentos
	 * @return {@link BeanInstrumento[]}
	 * @throws Exception
	 */
	private BeanInstrumento[] ordenarInstrumentossPorId(BeanInstrumento[] ins) {

		BeanInstrumento[] insOrdenados;

		try {
			insOrdenados = ins;

			for (int i = 0; i < (insOrdenados.length - 1); i++) {
				for (int j = i + 1; j < insOrdenados.length; j++) {
					int posicionI = insOrdenados[i].getId();
					int posicionJ = insOrdenados[j].getId();
					if (posicionI > posicionJ) {
						BeanInstrumento variableauxiliar = insOrdenados[i];
						insOrdenados[i] = insOrdenados[j];
						insOrdenados[j] = variableauxiliar;

					}
				}
			}
		} catch (Exception e) {
			return null;
		}

		return insOrdenados;
	}

}
