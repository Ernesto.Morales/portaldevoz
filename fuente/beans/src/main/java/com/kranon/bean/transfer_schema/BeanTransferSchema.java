package com.kranon.bean.transfer_schema;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transfer_schema")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTransferSchema {

	/* Array de esquemas */
	@XmlElement(name = "esquema")
	private BeanSchema[] esquemas;

	public BeanSchema[] getEsquemas() {
		return esquemas;
	}

	public void setEsquemas(BeanSchema[] esquemas) {
		this.esquemas = esquemas;
	}

	@Override
	public String toString() {
		return "BeanTransferSchema [esquemas=" + Arrays.toString(esquemas)
				+ "]";
	}

	// ******************************************************** //
	
	/**
	 * Obtener el codigo de la transferencia dados un esquema, segmento y producto
	 * 
	 * @param codEsquema
	 * @param codSegmento
	 * @param codProducto
	 * @return
	 */
	public String getCodTransferLabel(String codEsquema, String codSegmento, String codProducto){
		for (BeanSchema sch : this.getEsquemas()) {
			if (sch.getCodEsquema().equalsIgnoreCase(codEsquema)) {
				// tengo el esquema en cuestion
				for (BeanSegmento segm : sch.getSegmentos()) {
					if (segm.getCodSegmento().equalsIgnoreCase(codSegmento)) {
						// tengo el segmento en cuestion
						for (BeanProducto prod : segm.getProductos()) {
							if (prod.getCodProducto().equalsIgnoreCase(codProducto)) {
								// tengo el producto en cuestion
								return prod.getCodTransferLabel();
							}
						}
					}
				}

			}
		}
		return null;
	}
}
