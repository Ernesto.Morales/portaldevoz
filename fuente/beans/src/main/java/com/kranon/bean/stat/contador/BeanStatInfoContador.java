package com.kranon.bean.stat.contador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "contador")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanStatInfoContador {

	@XmlElement(name = "cod_servivr")
	private String codServIVR;

	@XmlElement(name = "cod_accivr")
	private String codAccIVR;

	// Informacion que no se exportara a XML
	@XmlTransient
	private String nombre;
	@XmlTransient
	private String tipo;
	@XmlTransient
	private String codigoRetorno;
	@XmlTransient
	private String param1;
	@XmlTransient
	private String param2;
	@XmlTransient
	private String param3;
	@XmlTransient
	private String param4;
	@XmlTransient
	private String param5;

	public String getCodServIVR() {
		return codServIVR;
	}

	public void setCodServIVR(String codServIVR) {
		this.codServIVR = codServIVR;
	}

	public String getCodAccIVR() {
		return codAccIVR;
	}

	public void setCodAccIVR(String codAccIVR) {
		this.codAccIVR = codAccIVR;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam4() {
		return param4;
	}

	public void setParam4(String param4) {
		this.param4 = param4;
	}

	public String getParam5() {
		return param5;
	}

	public void setParam5(String param5) {
		this.param5 = param5;
	}

	@Override
	public String toString() {
		return "BeanStatInfoContador [codServIVR=" + codServIVR
				+ ", codAccIVR=" + codAccIVR + ", nombre=" + nombre + ", tipo="
				+ tipo + ", codigoRetorno=" + codigoRetorno + ", param1="
				+ param1 + ", param2=" + param2 + ", param3=" + param3
				+ ", param4=" + param4 + ", param5=" + param5 + "]";
	}

//	@Override
//	public String toString() {
//		return String.format("BeanEstContador [codServIVR=%s, codAccIVR=%s]", codServIVR, codAccIVR);
//	}

}
