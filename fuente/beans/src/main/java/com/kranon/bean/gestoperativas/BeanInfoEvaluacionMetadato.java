package com.kranon.bean.gestoperativas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "metadato")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "action", "type", "balance", "folio", "insurance", "data", "estadistica"})
public class BeanInfoEvaluacionMetadato {

	@XmlAttribute(name = "action")
	private String action;
	
	@XmlAttribute(name = "type")
	private String type;
	
	@XmlAttribute(name = "balance")
	private String balance;
	
	@XmlAttribute(name = "folio")
	private String folio;
	
	@XmlAttribute(name = "insurance")
	private String insurance;
	
	@XmlAttribute(name = "data")
	private String data;
	
	@XmlAttribute(name = "estadistica")
	private String estadistica;
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	
	@Override
	public String toString() {
		return "BeanInfoEvaluacionMetadato [action=" + action + ", type="
				+ type + ", balance=" + balance + ", folio=" + folio
				+ ", insurance=" + insurance + ", data=" + data
				+ ", estadistica=" + estadistica + "]";
	}

}
