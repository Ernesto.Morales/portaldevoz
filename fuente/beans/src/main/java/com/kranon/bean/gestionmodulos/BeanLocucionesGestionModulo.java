package com.kranon.bean.gestionmodulos;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = { "locucion", "accion"})
public class BeanLocucionesGestionModulo {

	/* Locuciones de Gestion Modulos */
	@XmlElement(name = "locucion")
	private BeanLocGestionModulo[] locuciones;

	/* Accion en caso de tipoSalida = LOCUCION */
	@XmlElement(name = "accion")
	private String accion;

	public BeanLocGestionModulo[] getLocuciones() {
		return locuciones;
	}

	public void setLocuciones(BeanLocGestionModulo[] locuciones) {
		this.locuciones = locuciones;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Override
	public String toString() {
		return String.format("BeanLocucionesGestionModulo [locuciones=%s, accion=%s]", Arrays.toString(locuciones), accion);
	}
	
	
	
	//********************************************************//
	
	/**
	 * Ordenar las locuciones por id de menor a mayor y devuelve una cadena con los nombres de todas las locuciones separadas por |
	 * 
	 * @param {@link BeanLocGestionModulo[]} prompts
	 * @return {@link String}
	 * @throws Exception
	 */
	public String getNombresDeLocucionesTransfer() {

		String cadenasLocuciones="";
		
		BeanLocGestionModulo[] promptsOrdenados; 
		
		try {
			promptsOrdenados = this.getLocuciones();

			for (int i = 0; i < (promptsOrdenados.length - 1); i++) {
				for (int j = i + 1; j < promptsOrdenados.length; j++) {
					int posicionI = Integer.parseInt(promptsOrdenados[i].getId());
					int posicionJ = Integer.parseInt(promptsOrdenados[j].getId());
					if (posicionI > posicionJ) { 
						BeanLocGestionModulo variableauxiliar = promptsOrdenados[i];
						promptsOrdenados[i] = promptsOrdenados[j];
						promptsOrdenados[j] = variableauxiliar;
					}
				}
			}
			for (int i = 0; i < promptsOrdenados.length; i++) {
				cadenasLocuciones = cadenasLocuciones + promptsOrdenados[i].getNombre();
				if(i != (promptsOrdenados.length - 1)){
					// hay mas elementos, concateno el pipe
					cadenasLocuciones = cadenasLocuciones + "|";
				}
			}
			
		} catch (Exception e) {
			return null;
		}
		return cadenasLocuciones;
	}

}
