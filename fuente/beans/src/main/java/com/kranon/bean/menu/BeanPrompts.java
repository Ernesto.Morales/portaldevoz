package com.kranon.bean.menu;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPrompts {

	/* Prompts del tipo de reconomiento */
	@XmlElement(name = "intento")
	private BeanIntento[] intentos;

	/* Prompt del paso de voz a tonos */
	@XmlElement(name = "prompt_asr_to_dtmf")
	private BeanPrompt promptAsr2Dtmf;

	/* Prompt de maxint */
	@XmlElement(name = "prompt_maxint")
	private BeanPrompt promptMaxint;

	public BeanIntento[] getIntentos() {
		return intentos;
	}

	public void setIntentos(BeanIntento[] intentos) {
		this.intentos = intentos;
	}

	public BeanPrompt getPromptAsr2Dtmf() {
		return promptAsr2Dtmf;
	}

	public void setPromptAsr2Dtmf(BeanPrompt promptAsr2Dtmf) {
		this.promptAsr2Dtmf = promptAsr2Dtmf;
	}

	public BeanPrompt getPromptMaxint() {
		return promptMaxint;
	}

	public void setPromptMaxint(BeanPrompt promptMaxint) {
		this.promptMaxint = promptMaxint;
	}

	@Override
	public String toString() {
		return String
				.format("BeanPrompts [intentos=%s, promptAsr2Dtmf=%s, promptMaxint=%s]", Arrays.toString(intentos), promptAsr2Dtmf, promptMaxint);
	}

}
