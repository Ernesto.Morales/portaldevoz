package com.kranon.bean.promp;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "proms")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanPromps {
	@XmlElementWrapper(name = "respuestas")
	@XmlElement(name = "respuesta")
	private List<BeanProm> respuesta;

	public BeanPromps() {
		super();
	}

	public BeanPromps(List<BeanProm> respuesta) {
		super();
		this.respuesta = respuesta;
	}

	public List<BeanProm> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(List<BeanProm> respuesta) {
		this.respuesta = respuesta;
	}

	@Override
	public String toString() {
		return "BeanPromps [respuesta=" + respuesta + "]";
	}

	
}
