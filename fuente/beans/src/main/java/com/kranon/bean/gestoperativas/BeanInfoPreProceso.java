package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "preProceso")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "controlador", "excepcion" })
public class BeanInfoPreProceso {

	@XmlElementWrapper(name = "controladores")
	@XmlElement(name = "controlador")
	private BeanInfoControlador[] controlador;

	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanInfoExcepcion[] excepcion;

	public BeanInfoControlador[] getControlador() {
		return controlador;
	}

	public void setControlador(BeanInfoControlador[] controlador) {
		this.controlador = controlador;
	}

	public BeanInfoExcepcion[] getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(BeanInfoExcepcion[] excepcion) {
		this.excepcion = excepcion;
	}

	@Override
	public String toString() {
		return "BeanInfoPreProceso [controlador=" + Arrays.toString(controlador) + ", excepcion=" + Arrays.toString(excepcion) + "]";
	}

	// ****************************************************//

	/**
	 * Devuelve si est activa o no la operativa para el nombre de controlador actual
	 * 
	 * @param nombreControlador
	 * @return
	 */
	public String buscaEstado(String nombreControlador) {

		for (BeanInfoControlador infoEstado : this.controlador) {
			if (infoEstado.getValue().equals(nombreControlador)) {
				// hemos encontrado el controlador que buscamos
				return infoEstado.getActivo();
			}
		}
		return null;
	}
}
