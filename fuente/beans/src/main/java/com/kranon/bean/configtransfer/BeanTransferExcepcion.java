package com.kranon.bean.configtransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTransferExcepcion {

	/* Nombre de la excepcion */
	@XmlAttribute(name = "nombre")
	private String nombre;

	/* Valor nuevo del segmento en caso de que se produzca esa excepcion */
	@XmlValue
	private String value;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("BeanTransferExcepcion [nombre=%s, value=%s]", nombre, value);
	}

}
