package com.kranon.bean;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "horarios")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTipoHorario {

	/* */
	@XmlAttribute(name = "param")
	private String param;

	 @XmlElement(name = "horario")
	private BeanHorario[] horario;

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public BeanHorario[] getHorario() {
		return horario;
	}

	public void setHorario(BeanHorario[] horario) {
		this.horario = horario;
	}

	@Override
	public String toString() {
		return String.format("BeanTipoHorario [param=%s, horario=%s]", param, Arrays.toString(horario));
	}

}
