package com.kranon.bean.blacklist;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para cada tipo de objeto de la lista negra. Se corresponde con la tabla
 * IVR_CFG_BLACKLIST
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTipoBlackObj {

	/* Tipo de objeto */
	@XmlAttribute(name = "tipo")
	private String tipo;

	/* Lista de objetos */
	@XmlElement(name = "obj")
	private BeanBlackObj[] objetos;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public BeanBlackObj[] getObjetos() {
		return objetos;
	}

	public void setObjetos(BeanBlackObj[] objetos) {
		this.objetos = objetos;
	}

	@Override
	public String toString() {
		return "BeanTipoBlackObj [tipo=" + tipo + ", objetos="
				+ Arrays.toString(objetos) + "]";
	}

}
