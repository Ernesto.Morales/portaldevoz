package com.kranon.bean.loggerllamada;

import java.util.ArrayList;
import java.util.List;

public class LoggerLlamadaTemp {
	private String inicio;
	private int numeroPaso;
	private List<Paso> pasos= new ArrayList<Paso>();
	
	public LoggerLlamadaTemp() {
		super();
	}
	public LoggerLlamadaTemp(String inicio, int numeroPaso, List<Paso> pasos) {
		super();
		this.inicio = inicio;
		this.numeroPaso = numeroPaso;
		this.pasos = pasos;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public int getNumeroPaso() {
		return numeroPaso;
	}
	public void setNumeroPaso(int numeroPaso) {
		this.numeroPaso = numeroPaso;
	}
	public List<Paso> getPasos() {
		return pasos;
	}
	public void setPasos(List<Paso> pasos) {
		this.pasos = pasos;
	}
	@Override
	public String toString() {
		return "LoggerLlamadaTemp [inicio=" + inicio + ", numeroPaso="
				+ numeroPaso + ", pasos=" + pasos + "]";
	}
}
