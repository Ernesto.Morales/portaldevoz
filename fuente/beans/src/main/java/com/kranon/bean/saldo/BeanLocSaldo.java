package com.kranon.bean.saldo;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para las Locuciones de Saldos. Se corresponde con la tabla
 * IVR_CFG_LOC_SALDO
 * 
 */
@XmlRootElement(name = "locuciones_saldos")
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanLocSaldo {

	/* Lista de locuciones */
	@XmlElement(name = "locucion")
	private BeanTipoLocucion[] tipoLocuciones;

	public BeanTipoLocucion[] getTipoLocuciones() {
		return tipoLocuciones;
	}

	public void setTipoLocuciones(BeanTipoLocucion[] tipoLocuciones) {
		this.tipoLocuciones = tipoLocuciones;
	}

	@Override
	public String toString() {
		return "BeanLocSaldo [tipoLocuciones="
				+ Arrays.toString(tipoLocuciones) + "]";
	}

}
