package com.kranon.bean.gestoperativas;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "transferencia")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "directa", "variables", "defecto", "excepcion" })
public class BeanInfoTransferencia {

	@XmlElement(name = "directa")
	private String directa;

	@XmlElementWrapper(name = "variables")
	@XmlElement(name = "variable")
	private BeanVariableTransfer[] variables;

	@XmlElement(name = "default")
	private String defecto;

	@XmlElementWrapper(name = "excepciones")
	@XmlElement(name = "excepcion")
	private BeanInfoExcepcion[] excepcion;

	public String getDirecta() {
		return directa;
	}

	public void setDirecta(String directa) {
		this.directa = directa;
	}

	public BeanVariableTransfer[] getVariables() {
		return variables;
	}

	public void setVariables(BeanVariableTransfer[] variables) {
		this.variables = variables;
	}

	public String getDefecto() {
		return defecto;
	}

	public void setDefecto(String defecto) {
		this.defecto = defecto;
	}

	public BeanInfoExcepcion[] getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(BeanInfoExcepcion[] excepcion) {
		this.excepcion = excepcion;
	}

	@Override
	public String toString() {
		return String.format("BeanInfoTransferencia [directa=%s, variables=%s, defecto=%s, excepcion=%s]", directa, Arrays.toString(variables),
				defecto, Arrays.toString(excepcion));
	}

	// *****************************************************************//

	/**
	 * Devuelve segmento de transferencia configurado para el segmentoCliente  (y con segmento entrada a vacio)
	 * 
	 * @param segmentoCliente
	 * @return
	 */
	public String getTransferPorSegmentoCliente(String segmentoCliente) {
		if (this.variables == null || this.variables.length == 0) {
			return null;
		}
		for (BeanVariableTransfer var : this.variables) {
			// debe coincidir el segmento del cliente y que el segmento de entrada est vacio
			if (var.getSegmentoCliente().equals(segmentoCliente) && (var.getNumeracionEntrada() == null || var.getNumeracionEntrada().equals(""))) {
				return var.getNombre();
			}
		}
		// no lo he encontrado
		return null;
	}

	/**
	 * Devuelve segmento de transferencia configurado para el segmentoEntrada (y con segmento cliente a vacio)
	 * 
	 * @param numeracionEntrada
	 * @return
	 */
	public String getTransferPorSegmentoEntrada(String numeracionEntrada) {
		if (this.variables == null || this.variables.length == 0) {
			return null;
		}
		for (BeanVariableTransfer var : this.variables) {
			// debe coincidir el segmento de entrada y que el segmento cliente est vacio
			if (var.getNumeracionEntrada().equals(numeracionEntrada) && (var.getSegmentoCliente() == null || var.getSegmentoCliente().equals(""))) {

				return var.getNombre();
			}
		}
		// no lo he encontrado
		return null;
	}

}
