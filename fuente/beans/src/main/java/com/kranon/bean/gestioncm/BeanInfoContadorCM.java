package com.kranon.bean.gestioncm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Bean para los datos de un Contador en el cuadro de mando
 * 
 */
@XmlRootElement(name = "contador")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "type", "codigoRetorno", "param1", "param2", "param3", "param4", "param5", "valor" })
public class BeanInfoContadorCM {

	/* Tipo del Contador */
	@XmlAttribute(name = "type")
	private String type;

	/* Codigo de Retorno del contador */
	@XmlAttribute(name = "codigoRetorno")
	private String codigoRetorno;

	/* Codigo del servicio */
	@XmlAttribute(name = "param1")
	private String param1;

	/* Codigo del servicio */
	@XmlAttribute(name = "param2")
	private String param2;

	/* Codigo del servicio */
	@XmlAttribute(name = "param3")
	private String param3;

	/* Codigo del servicio */
	@XmlAttribute(name = "param4")
	private String param4;

	/* Codigo del servicio */
	@XmlAttribute(name = "param5")
	private String param5;

	/* Valor del contador */
	@XmlValue
	private String valor;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam4() {
		return param4;
	}

	public void setParam4(String param4) {
		this.param4 = param4;
	}

	public String getParam5() {
		return param5;
	}

	public void setParam5(String param5) {
		this.param5 = param5;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return String.format("BeanInfoContador [type=%s, codigoRetorno=%s, param1=%s, param2=%s, param3=%s, param4=%s, param5=%s, valor=%s]", type,
				codigoRetorno, param1, param2, param3, param4, param5, valor);
	}

	//*********************************************************************************//
	
	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public boolean equals(Object obj) {
		// dos contadores son iguales si tienen la misma info en sus atributos
		
		if (obj instanceof BeanInfoContadorCM) {
			BeanInfoContadorCM other = (BeanInfoContadorCM) obj;
			EqualsBuilder builder = new EqualsBuilder();
			builder.append(this.type, other.type);
			builder.append(this.codigoRetorno, other.codigoRetorno);
			builder.append(this.param1, other.param1);
			builder.append(this.param2, other.param2);
			builder.append(this.param3, other.param3);
			builder.append(this.param4, other.param4);
			builder.append(this.param5, other.param5);
			return builder.isEquals();
		
		}
		return false;
	}

	/**
	 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
	 */
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(type);
		builder.append(codigoRetorno);
		builder.append(param1);
		builder.append(param2);
		builder.append(param3);
		builder.append(param4);
		builder.append(param5);
		return builder.toHashCode();
	}

}
