package com.kranon.bean.gestoperativas;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kranon.beanadapter.MapAdapterMetadatos;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

@XmlRootElement(name = "operativa")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nombre", "tablaMetadatos" })
public class BeanOperativa implements Comparable<BeanOperativa> {

	/* Nombre del modulo */
	@XmlAttribute(name = "nombre")
	private String nombre;

	@XmlJavaTypeAdapter(MapAdapterMetadatos.class)
	@XmlElement(name = "tipos_metadato")
	// private BeanInfoMetadato[] metadato;
	private HashMap<BeanMetadato, BeanInfoOperativa> tablaMetadatos;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public HashMap<BeanMetadato, BeanInfoOperativa> getTablaMetadatos() {
		return tablaMetadatos;
	}

	public void setTablaMetadatos(HashMap<BeanMetadato, BeanInfoOperativa> tablaMetadatos) {
		this.tablaMetadatos = tablaMetadatos;
	}

	@Override
	public String toString() {
		return "BeanInfoOperativa [nombre=" + nombre + ", tablaMetadatos=" + tablaMetadatos + "]";
	}

	// **********************************************************************//

	/**
	 * Busca en la tabla el valor, para la clave asociada a todos los elementos indicados
	 * 
	 * @param action
	 * @param type
	 * @param balance
	 * @param folio
	 * @param insurance
	 * @param data
	 * @return {@link BeanInfoOperativa}
	 */
	public BeanInfoOperativa buscaInfoMetadato(String action, String type, String balance, String folio, String insurance, String data) {

		BeanMetadato beanMetadato = new BeanMetadato();
		beanMetadato.setAction(action);
		beanMetadato.setType(type);
		beanMetadato.setBalance(balance);
		beanMetadato.setFolio(folio);
		beanMetadato.setInsurance(insurance);
		beanMetadato.setData(data);

		// recupero la operativa con metadatos
		BeanInfoOperativa beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);
		if (beanInfoMetadato == null && data != null && !data.equals("")) {
			// no he encontrado el elemento en la tabla y tengo un metadato data relleno

			// miro si existe en la tabla con data=*
			beanMetadato.setData("*");
			beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);

			if (beanInfoMetadato == null) {
				// sigo sin encontrar el elemento en la tabla

				// miro si existe operativa por defecto con data vacio
				beanMetadato.setData("");
				beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);
			}

		}
		if (beanInfoMetadato == null) {
			// no he encontrado el elemento en la tabla, pero no me ha venido el data relleno o he fallado en la busqueda
			// intento buscar solo la operativa con metadatos vacios (operativa por defecto)

			beanMetadato.setAction("");
			beanMetadato.setType("");
			beanMetadato.setBalance("");
			beanMetadato.setFolio("");
			beanMetadato.setInsurance("");
			beanMetadato.setData("");

			beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);

			// si no lo encuentro entonces s devolver null, porque no existe

		}

		return beanInfoMetadato;
	}

	@Override
	public int compareTo(BeanOperativa o) {
		if (nombre.compareTo(o.nombre) < 0) {
			return -1;
		}
		if (nombre.compareTo(o.nombre) > 0) {
			return 1;
		}
		return 0;
	}

	public BeanInfoOperativa buscaInfoMetadato(String action, String type, String balance, String folio, String insurance, String data,
			CommonLoggerService log) {

		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("nombreOperativa", this.getNombre()));
		parametrosEntrada.add(new ParamEvent("action", action));
		parametrosEntrada.add(new ParamEvent("type", type));
		parametrosEntrada.add(new ParamEvent("balance", balance));
		parametrosEntrada.add(new ParamEvent("folio", folio));
		parametrosEntrada.add(new ParamEvent("insurance", insurance));
		parametrosEntrada.add(new ParamEvent("data", data));
		log.initModuleService("BUSCA_INFO_METADATO", parametrosEntrada);

		BeanMetadato beanMetadato = new BeanMetadato();
		beanMetadato.setAction(action);
		beanMetadato.setType(type);
		beanMetadato.setBalance(balance);
		beanMetadato.setFolio(folio);
		beanMetadato.setInsurance(insurance);
		beanMetadato.setData(data);

		// recupero la operativa con metadatos
		BeanInfoOperativa beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);
		if (beanInfoMetadato == null && data != null && !data.equals("")) {
			// no he encontrado el elemento en la tabla y tengo un metadato data relleno

			log.actionEvent("BUSCA_INFO_METADATO", "NO_ENCONTRADO", null);
			log.actionEvent("BUSCA_INFO_METADATO", "BUSCO_CON_DATA_*", null);

			// miro si existe en la tabla con data=*
			beanMetadato.setData("*");
			beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);

			if (beanInfoMetadato == null) {
				// sigo sin encontrar el elemento en la tabla

				log.actionEvent("BUSCA_INFO_METADATO", "NO_ENCONTRADO", null);
				log.actionEvent("BUSCA_INFO_METADATO", "BUSCO_CON_DATA_VACIO", null);

				// miro si existe operativa por defecto con data vacio
				beanMetadato.setData("");
				beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);
			}

		}
		if (beanInfoMetadato == null) {
			// no he encontrado el elemento en la tabla, pero no me ha venido el data relleno o he fallado en la busqueda
			// intento buscar solo la operativa con metadatos vacios (operativa por defecto)

			log.actionEvent("BUSCA_INFO_METADATO", "NO_ENCONTRADO", null);
			log.actionEvent("BUSCA_INFO_METADATO", "BUSCO_CON_METADATOS_VACIOS", null);

			beanMetadato.setAction("");
			beanMetadato.setType("");
			beanMetadato.setBalance("");
			beanMetadato.setFolio("");
			beanMetadato.setInsurance("");
			beanMetadato.setData("");

			beanInfoMetadato = this.getTablaMetadatos().get(beanMetadato);

			// si no lo encuentro entonces s devolver null, porque no existe

		}
		if (beanInfoMetadato != null) {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("nombreOperativa", this.getNombre()));
			parametrosSalida.add(new ParamEvent("action", beanInfoMetadato.getAction()));
			parametrosSalida.add(new ParamEvent("type", beanInfoMetadato.getType()));
			parametrosSalida.add(new ParamEvent("balance", beanInfoMetadato.getBalance()));
			parametrosSalida.add(new ParamEvent("folio", beanInfoMetadato.getFolio()));
			parametrosSalida.add(new ParamEvent("insurance", beanInfoMetadato.getInsurance()));
			parametrosSalida.add(new ParamEvent("data", beanInfoMetadato.getData()));
			log.endModuleService("BUSCA_INFO_METADATO", "OK", parametrosSalida, null);
		} else {
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("nombreOperativa", "NULL"));
			log.endModuleService("BUSCA_INFO_METADATO", "KO", parametrosSalida, null);
		}

		return beanInfoMetadato;
	}

}
