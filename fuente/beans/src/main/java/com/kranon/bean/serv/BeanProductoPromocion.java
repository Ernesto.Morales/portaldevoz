package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para los Productos de las Promociones de un Servicio. Se corresponde con
 * la tabla IVR_CFG_PROMO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanProductoPromocion {

	/* Codigo del producto de la promocion */
	@XmlAttribute(name = "cd_producto")
	private String codProducto;

	/* Array de servicios de la promocion */
	@XmlElement(name = "servicio")
	private BeanServicioPromocion[] servicios;

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public BeanServicioPromocion[] getServicios() {
		return servicios;
	}

	public void setServicios(BeanServicioPromocion[] servicios) {
		this.servicios = servicios;
	}

	@Override
	public String toString() {
		return "BeanProductoPromocion [codProducto=" + codProducto
				+ ", servicios=" + Arrays.toString(servicios) + "]";
	}

}
