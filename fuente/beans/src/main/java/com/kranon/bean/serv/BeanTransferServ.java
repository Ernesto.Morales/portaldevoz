package com.kranon.bean.serv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.kranon.bean.BeanModificacion;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTransferServ {

	/* Codigo del servicio */
	@XmlAttribute(name = "cd_esquema")
	private String codEsquema;

	/* Flag para el Esquema-Directo-Directo-Directo */
	@XmlElement(name = "flag_esq_directo")
	private String flagEsqDirecto;

	/* Ultima modificacion */
	@XmlTransient
	private BeanModificacion modif;


	public String getCodEsquema() {
		return codEsquema;
	}

	public void setCodEsquema(String codEsquema) {
		this.codEsquema = codEsquema;
	}

	public String getFlagEsqDirecto() {
		return flagEsqDirecto;
	}

	public void setFlagEsqDirecto(String flagEsqDirecto) {
		this.flagEsqDirecto = flagEsqDirecto;
	}
	

	public BeanModificacion getModif() {
		return modif;
	}

	public void setModif(BeanModificacion modif) {
		this.modif = modif;
	}

	@Override
	public String toString() {
		return "BeanTransferServ [codEsquema=" + codEsquema
				+ ", flagEsqDirecto=" + flagEsqDirecto + ", modif=" + modif
				+ "]";
	}

}
