package com.kranon.bean.serv;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean para las Tarifas de las Promociones de un Servicio. Se corresponde con
 * la tabla IVR_CFG_PROMO_SVC
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BeanTarifaPromocion {

	/* Codigo de la tarifa de la promocion */
	@XmlAttribute(name = "cd_tarifa")
	private String codTarifa;

	/* Array de segmentos de la promocion */
	@XmlElement(name = "segmento")
	private BeanSegmentoPromocion[] segmentos;

	public String getCodTarifa() {
		return codTarifa;
	}

	public void setCodTarifa(String codTarifa) {
		this.codTarifa = codTarifa;
	}

	public BeanSegmentoPromocion[] getSegmentos() {
		return segmentos;
	}

	public void setSegmentos(BeanSegmentoPromocion[] segmentos) {
		this.segmentos = segmentos;
	}

	@Override
	public String toString() {
		return "BeanTarifaPromocion [codTarifa=" + codTarifa + ", segmentos="
				+ Arrays.toString(segmentos) + "]";
	}

}
