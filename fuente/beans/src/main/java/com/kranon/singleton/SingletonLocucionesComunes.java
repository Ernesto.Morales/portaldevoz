package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.loccomunes.BeanLocucionesComunes;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanLocucionesComunes CACHEADA a
 * 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonLocucionesComunes {

	private static SingletonLocucionesComunes singletonLocucionesComunes;

	private HashMap<String, BeanLocucionesComunes> mapLocucionesComunes;

	private LoadingCache<String, HashMap<String, BeanLocucionesComunes>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonLocucionesComunes() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonLocucionesComunes getInstance(CommonLoggerService log) throws Exception {

		if (singletonLocucionesComunes == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_LOCUCIONES_COMUNES|NUEVA_INSTANCIA");
			singletonLocucionesComunes = new SingletonLocucionesComunes();
			singletonLocucionesComunes.log = log;
			singletonLocucionesComunes.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonLocucionesComunes.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanLocucionesComunes>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanLocucionesComunes y se vuelve a cargar
						 * <key> es el codigo del servicio a recuperar
						 */
						public HashMap<String, BeanLocucionesComunes> load(String key) throws Exception {

							singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|LOAD_CACHE|codServIvr=" + key);
							if (singletonLocucionesComunes.getMapLocucionesComunes() == null) {
								// si la tabla es null, la creo
								singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|NEW_INSTANCE_MAP_LOCUCIONES_COMUNES");
								singletonLocucionesComunes.setMapLocucionesComunes(new HashMap<String, BeanLocucionesComunes>());
							}
							// si el mapa contiene las locuciones, lo elimino
							if (singletonLocucionesComunes.getMapLocucionesComunes().containsKey(key)) {
								singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|DELETE_BEAN_LOCUCIONES_COMUNES|codServIvr=: "
										+ key);
								singletonLocucionesComunes.getMapLocucionesComunes().remove(key);
							} else {
								singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|GET_BEAN_LOCUCIONES_COMUNES|codServIvr=" + key);

							try {
								// Recupero el BeanLocucionesComunes con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonLocucionesComunes.log.getIdLlamada(),
										singletonLocucionesComunes.log.getIdElemento());
								BeanLocucionesComunes beanLocucionesComunes = unmarshallProcess.unmarshallLocucionesComunes(key);

								singletonLocucionesComunes.getMapLocucionesComunes().put(key, beanLocucionesComunes);

								// singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|GET_LOCUCIONES_COMUNES="
								// +
								// singletonLocucionesComunes.getMapLocucionesComunes().get(key).toString()
								// + "|OK");
								singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|GET_LOCUCIONES_COMUNES|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonLocucionesComunes.log.error(e.toString(), "SINGLETON_LOCUCIONES_COMUNES", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonLocucionesComunes.getMapLocucionesComunes();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_LOCUCIONES_COMUNES|EXISTE_INSTANCIA");
			singletonLocucionesComunes.log = log;
		}

		return singletonLocucionesComunes;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanLocucionesComunes asociado de
	 * la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanLocucionesComunes}
	 */
	public BeanLocucionesComunes getLocucionesComunes(String cod) {
		BeanLocucionesComunes serv = null;
		try {
			singletonLocucionesComunes.log.comment("SINGLETON_LOCUCIONES_COMUNES|LOAD_CACHE|codServIvr=" + cod);

			HashMap<String, BeanLocucionesComunes> map = singletonLocucionesComunes.cache.get(cod);
			if (map == null) {
				return null;
			} else {
				serv = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonLocucionesComunes.log.error(e.toString(), "GET_LOCUCIONES_COMUNES_CACHE", parametrosAdicionales, e);
		}

		return serv;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanLocucionesComunes> getMapLocucionesComunes() {

		return mapLocucionesComunes;
	}

	private void setMapLocucionesComunes(HashMap<String, BeanLocucionesComunes> mapLocucionesComunes) {
		this.mapLocucionesComunes = mapLocucionesComunes;
	}

}
