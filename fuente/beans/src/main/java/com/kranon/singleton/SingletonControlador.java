package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.controlador.BeanControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanControlador CACHEADA a 120
 * segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonControlador {

	private static SingletonControlador singletonControlador;

	private HashMap<KeysControlador, BeanControlador> mapControlador;

	private LoadingCache<KeysControlador, HashMap<KeysControlador, BeanControlador>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	// Clase para recibir la <clave> de busqueda
	public class KeysControlador {
		public String codServ;
		public String nombreControlador;

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public boolean equals(Object obj) {
			if (obj instanceof KeysControlador) {
				KeysControlador other = (KeysControlador) obj;
				EqualsBuilder builder = new EqualsBuilder();
				builder.append(this.codServ, other.codServ);
				builder.append(this.nombreControlador, other.nombreControlador);
				return builder.isEquals();
			}
			return false;
		}

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public int hashCode() {
			HashCodeBuilder builder = new HashCodeBuilder();
			builder.append(codServ);
			builder.append(nombreControlador);
			return builder.toHashCode();
		}
	}

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonControlador() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonControlador getInstance(CommonLoggerService log) throws Exception {

		if (singletonControlador == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_CONTROLADOR|NUEVA_INSTANCIA");
			singletonControlador = new SingletonControlador();
			singletonControlador.log = log;
			singletonControlador.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonControlador.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<KeysControlador, HashMap<KeysControlador, BeanControlador>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanControlador y se vuelve a cargar
						 * key es un objeto de tipo KeysControlador con el codigo del servicio y nombre de controlador a recuperar
						 */
						public HashMap<KeysControlador, BeanControlador> load(KeysControlador key) throws Exception {

							singletonControlador.log.comment("SINGLETON_CONTROLADOR|LOAD_CACHE|codServIvr=" + key.codServ + "|nombreControlador="
									+ key.nombreControlador);
							if (singletonControlador.getMapControlador() == null) {
								// si la tabla es null, la creo
								singletonControlador.log.comment("SINGLETON_CONTROLADOR|NEW_INSTANCE_MAP_CONTROLADOR");
								singletonControlador.setMapControlador(new HashMap<KeysControlador, BeanControlador>());
							}
							// si el mapa contiene el controlador, lo elimino
							if (singletonControlador.getMapControlador().containsKey(key)) {
								singletonControlador.log.comment("SINGLETON_CONTROLADOR|DELETE_BEAN_CONTROLADOR|codServIvr=" + key.codServ
										+ "|nombreControlador=" + key.nombreControlador);
								singletonControlador.getMapControlador().remove(key);
							} else {
								singletonControlador.log.comment("SINGLETON_CONTROLADOR|KEY_NOT_FOUND|codServIvr=" + key.codServ
										+ "|nombreControlador=" + key.nombreControlador);
							}

							singletonControlador.log.comment("SINGLETON_CONTROLADOR|GET_BEAN_CONTROLADOR|codServIvr=" + key.codServ
									+ "|nombreControlador=" + key.nombreControlador);

							try {
								// Recupero el BeanControlador con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonControlador.log.getIdLlamada(), singletonControlador.log
										.getIdElemento());
								BeanControlador beanControlador = unmarshallProcess.unmarshallControlador(key.codServ, key.nombreControlador);

								singletonControlador.getMapControlador().put(key, beanControlador);

								singletonControlador.log.comment("SINGLETON_CONTROLADOR|GET_CONTROLADOR|OK");
//								singletonControlador.log.comment("SINGLETON_CONTROLADOR|GET_CONTROLADOR="
//										+ singletonControlador.getMapControlador().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key.codServ));
								parametrosAdicionales.add(new ParamEvent("nombreControlador", key.nombreControlador));
								singletonControlador.log.error(e.toString(), "SINGLETON_CONTROLADOR", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonControlador.getMapControlador();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_CONTROLADOR|EXISTE_INSTANCIA");
			singletonControlador.log = log;
		}

		return singletonControlador;
	}

	/**
	 * Dado un codigo de servicio y un nombre de controlador, recupera el
	 * BeanControlador asociado de la cache o vuelve a leer del XML para
	 * actualizar la instancia
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanControlador}
	 */
	public BeanControlador getControlador(String codServ, String nombreControlador) {
		BeanControlador controlador = null;
		try {
			singletonControlador.log.comment("SINGLETON_CONTROLADOR|LOAD_CACHE|codServIvr=" + codServ + "|nombreControlador=" + nombreControlador);
			SingletonControlador.KeysControlador key = new KeysControlador();
			key.codServ = codServ;
			key.nombreControlador = nombreControlador;

			// controlador = singletonControlador.cache.get(key).get(key);
			HashMap<KeysControlador, BeanControlador> map = singletonControlador.cache.get(key);
			if (map == null) {
				return null;
			} else {
				controlador = map.get(key);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", codServ));
			parametrosAdicionales.add(new ParamEvent("nombreControlador", nombreControlador));
			singletonControlador.log.error(e.toString(), "GET_CONTROLADOR_CACHE", parametrosAdicionales, e);
		}

		return controlador;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<KeysControlador, BeanControlador> getMapControlador() {
		return mapControlador;
	}

	private void setMapControlador(HashMap<KeysControlador, BeanControlador> mapControlador) {
		this.mapControlador = mapControlador;
	}

}
