package com.kranon.singleton;

import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.modulo.BeanModulo;

public class TestSingleton {

	public static void main(String[] args) {

		try {

//			System.out.println("INICIO");

			CommonLoggerService log = new CommonLoggerService("PRUEBA");
			log.inicializar("idLlamada", "idServicio", "idElemento");

			// Recuperamos el Bean del Modulo en cuestion
			BeanModulo modulo = null;
			SingletonModulo instanceModulo = SingletonModulo.getInstance(log);
			if (instanceModulo != null) {
				modulo = instanceModulo.getModulo(new String("PRESTAMOS"), new String("SALDO-CRED-AUTO"));
			}

			if (modulo == null) {
//				System.out.println("MODULO NULL");

			} else {
//				System.out.println("MODULO: " + modulo.getCodModulo());
			}

			// Recuperamos el Bean del Modulo en cuestion
			BeanModulo modulo2 = null;
			SingletonModulo instanceModulo2 = SingletonModulo.getInstance(log);
			if (instanceModulo2 != null) {
				modulo2 = instanceModulo2.getModulo(new String("PRESTAMOS"), new String("SALDO-CRED-AUTO"));
			}
			if (modulo2 == null) {
//				System.out.println("MODULO 2 NULL");

			} else {
//				System.out.println("MODULO 2: " + modulo2.getCodModulo());
			}
			
//			System.out.println("FIN");

		} catch (Exception e) {
//			System.out.println("ERROR: " + e.toString());
		}
	}
}
