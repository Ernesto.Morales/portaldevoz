package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.mensajeserror.*;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanRespuestasMau CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonMensajesError {

	private static SingletonMensajesError SingletonMensajes;

	private HashMap<String, BeanMensajesError> mapServicios;
	
	private LoadingCache<String, HashMap<String, BeanMensajesError>> cache;
	
	private CommonLoggerService log;
	
	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonMensajesError() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonMensajesError getInstance(CommonLoggerService log) throws Exception {

		if (SingletonMensajes == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_PROMS|NUEVA_INSTANCIA");
			SingletonMensajes = new SingletonMensajesError();
			SingletonMensajes.log = log;
			SingletonMensajes.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas veces se acceda
			        .expireAfterWrite(SingletonMensajes.tiempoSegCacheo, TimeUnit.SECONDS)
			        .build(new CacheLoader<String, HashMap<String, BeanMensajesError>>() {
			            @Override
			            /**
			             * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanMensajesError y se vuelve a cargar
			             * <key> es el codigo del servicio a recuperar
			             */
			            public HashMap<String, BeanMensajesError> load(String key) throws Exception {
			            	
			            	SingletonMensajes.log.comment("SINGLETON_PROMS|LOAD_CACHE|codServIvr=" + key);
							if (SingletonMensajes.getMapServicios() == null) {
								// si la tabla es null, la creo
								SingletonMensajes.log.comment("SINGLETON_PROMS|NEW_INSTANCE_MAP_SERVICIO");
								SingletonMensajes.setMapServicios(new HashMap<String, BeanMensajesError>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (SingletonMensajes.getMapServicios().containsKey(key)) {
								SingletonMensajes.log.comment("SINGLETON_PROMS|DELETE_BEAN_SERVICIO|codServIvr=" + key);
								SingletonMensajes.getMapServicios().remove(key);
							} else {
								SingletonMensajes.log.comment("SINGLETON_PROMS|KEY_NOT_FOUND|codServIvr=" + key);
							}
						
							SingletonMensajes.log.comment("SINGLETON_PROMS|GET_BEAN_SERVICIO|codServIvr=" + key);
	
							try {
								// Recupero el BeanMensajesError con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(SingletonMensajes.log.getIdLlamada(), SingletonMensajes.log.getIdElemento());
								BeanMensajesError beanServicio = unmarshallProcess.unmarshallMensajesError(key);
										
								SingletonMensajes.getMapServicios().put(key, beanServicio);
								
								SingletonMensajes.log.comment("SINGLETON_PROMS|GET_SERVICIO|OK");
//								SingletonMensajes.log.comment("SINGLETON_PROMS|GET_SERVICIO=" + SingletonMensajes.getMapServicios().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								SingletonMensajes.log.error(e.toString(), "SINGLETON_PROMS", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}
							

			                return SingletonMensajes.getMapServicios();
			            }
			        });		
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_PROMS|EXISTE_INSTANCIA");
			SingletonMensajes.log = log;
		}
				
		return SingletonMensajes;
	}
	
	/**
	 * Dado un codigo de servicio, recupera el BeanMensajesError asociado de la cache 
	 * o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanMensajesError}
	 */
	public BeanMensajesError getServicio(String cod) {
		BeanMensajesError serv = null;
		try {
			SingletonMensajes.log.comment("SINGLETON_PROMS|LOAD_CACHE|codServIvr=" + cod);
			
//			serv = SingletonMensajes.cache.get(cod).get(cod);
			HashMap<String, BeanMensajesError> map = SingletonMensajes.cache.get(cod);
			if(map == null){
				return null;
			} else {
				serv = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			SingletonMensajes.log.error(e.toString(), "SINGLETON_PROMS", parametrosAdicionales, e);
		}
		
		return serv;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanMensajesError> getMapServicios() {
	
		return mapServicios;
	}

	private void setMapServicios(HashMap<String, BeanMensajesError> mapServicios) {
		this.mapServicios = mapServicios;
	}

}
