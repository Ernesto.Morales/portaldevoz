package com.kranon.singleton;

import java.util.HashMap;

import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanServicio SIN CACHE
 * 
 * @author abalfaro
 *
 */
public class SingletonServicioSinCache {

	private static SingletonServicioSinCache singletonServicio;

	private HashMap<String, BeanServicio> mapServicios;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonServicioSinCache() {

	}

	/**
	 * Obtiene una instancia del recurso.
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonServicioSinCache getInstance(CommonLoggerService log, String codServIvr) throws Exception {
		if (singletonServicio == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_SERVICIO_SIN_CACHE|NUEVA_INSTANCIA");
			singletonServicio = new SingletonServicioSinCache();
		}
		if (singletonServicio.getMapServicios() == null) {
			// si la tabla es null, la creo
			log.comment("SINGLETON_SERVICIO_SIN_CACHE|NEW_INSTANCE_MAP_SERVICIO");
			singletonServicio.setMapServicios(new HashMap<String, BeanServicio>());
		}
		if (!singletonServicio.getMapServicios().containsKey(codServIvr)) {
			// la tabla NO contiene el bean servicio con este codigo

			log.comment("SINGLETON_SERVICIO_SIN_CACHE|GET_BEAN_SERVICIO|codServIvr=" + codServIvr);

			// Recupero el BeanServicio con ese codigo
			Unmarshall unmarshallProcess = new Unmarshall(log.getIdLlamada(), log.getIdElemento());
			BeanServicio beanServicio = unmarshallProcess.unmarshallServicio(codServIvr);

			singletonServicio.getMapServicios().put(codServIvr, beanServicio);
		}
		
		return singletonServicio;
	}

	public HashMap<String, BeanServicio> getMapServicios() {
	
		return mapServicios;
	}

	public void setMapServicios(HashMap<String, BeanServicio> mapServicios) {
		this.mapServicios = mapServicios;
	}

}
