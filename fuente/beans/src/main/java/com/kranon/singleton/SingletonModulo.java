package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.modulo.BeanModulo;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia del BeanModulo CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonModulo {

	private static SingletonModulo singletonModulo;

	private HashMap<KeysModulo, BeanModulo> mapModulo;

	private LoadingCache<KeysModulo, HashMap<KeysModulo, BeanModulo>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	// Clase para recibir la <clave> de busqueda
	public class KeysModulo {
		public String codServ;
		public String nombreModulo;

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public boolean equals(Object obj) {
			if (obj instanceof KeysModulo) {
				KeysModulo other = (KeysModulo) obj;
				EqualsBuilder builder = new EqualsBuilder();
				builder.append(this.codServ, other.codServ);
				builder.append(this.nombreModulo, other.nombreModulo);
				return builder.isEquals();
			}
			return false;
		}

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public int hashCode() {
			HashCodeBuilder builder = new HashCodeBuilder();
			builder.append(codServ);
			builder.append(nombreModulo);
			return builder.toHashCode();
		}
	}

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonModulo() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonModulo getInstance(CommonLoggerService log) throws Exception {

		if (singletonModulo == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_MODULO|NUEVA_INSTANCIA");
			singletonModulo = new SingletonModulo();
			singletonModulo.log = log;
			singletonModulo.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonModulo.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<KeysModulo, HashMap<KeysModulo, BeanModulo>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanModulo y se vuelve a cargar
						 * key es un objeto de tipo KeysModulo con el codigo del servicio y nombre de modulo a recuperar
						 */
						public HashMap<KeysModulo, BeanModulo> load(KeysModulo key) throws Exception {

							singletonModulo.log
									.comment("SINGLETON_MODULO|LOAD_CACHE|codServIvr=" + key.codServ + "|nombreModulo=" + key.nombreModulo);
							if (singletonModulo.getMapModulo() == null) {
								// si la tabla es null, la creo
								singletonModulo.log.comment("SINGLETON_MODULO|NEW_INSTANCE_MAP_MODULO");
								singletonModulo.setMapModulo(new HashMap<KeysModulo, BeanModulo>());
							}
							// si el mapa contiene el modulo, lo elimino
							if (singletonModulo.getMapModulo().containsKey(key)) {
								singletonModulo.log.comment("SINGLETON_MODULO|DELETE_BEAN_MODULO|codServIvr=" + key.codServ + "|nombreModulo="
										+ key.nombreModulo);
								singletonModulo.getMapModulo().remove(key);
							} else {
								singletonModulo.log.comment("SINGLETON_MODULO|KEY_NOT_FOUND|codServIvr=" + key.codServ + "|nombreModulo="
										+ key.nombreModulo);
							}

							singletonModulo.log.comment("SINGLETON_MODULO|GET_BEAN_MODULO|codServIvr=" + key.codServ + "|nombreModulo="
									+ key.nombreModulo);

							try {

								// Recupero el BeanModulo con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonModulo.log.getIdLlamada(), singletonModulo.log.getIdElemento());
								// BeanModulo beanModulo =
								// unmarshallProcess.unmarshallModulo(key.codServ,
								// key.nombreModulo);

								// [20160822] cambio barras bajas en el nombre
								// modulo por guiones
								String nombreModuloAux = key.nombreModulo.replaceAll("_", "-");
								BeanModulo beanModulo = unmarshallProcess.unmarshallModulo(key.codServ, nombreModuloAux);

								singletonModulo.getMapModulo().put(key, beanModulo);

								singletonModulo.log.comment("SINGLETON_MODULO|GET_MODULO|OK");
//								singletonModulo.log.comment("SINGLETON_MODULO|GET_MODULO=" + singletonModulo.getMapModulo().get(key).toString()
//										+ "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key.codServ));
								parametrosAdicionales.add(new ParamEvent("nombreModulo", key.nombreModulo));
								singletonModulo.log.error(e.toString(), "SINGLETON_MODULO", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonModulo.getMapModulo();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_MODULO|EXISTE_INSTANCIA");
			singletonModulo.log = log;
		}

		return singletonModulo;
	}

	/**
	 * Dado un codigo de servicio y un nombre de modulo, recupera el BeanModulo
	 * asociado de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param codServ
	 * @param nombreModulo
	 * @return {@link BeanModulo}
	 */
	public BeanModulo getModulo(String codServ, String nombreModulo) {
		BeanModulo modulo = null;
		try {
			// [20160822] cambio barras bajas en el nombre modulo por guiones
			String nombreModuloAux = nombreModulo.replaceAll("_", "-");

			singletonModulo.log.comment("SINGLETON_MODULO|LOAD_CACHE|codServIvr=" + codServ + "|nombreModulo=" + nombreModuloAux);
			SingletonModulo.KeysModulo key = new KeysModulo();
			key.codServ = codServ;
			key.nombreModulo = nombreModuloAux;

			HashMap<KeysModulo, BeanModulo> map = singletonModulo.cache.get(key);
			if (map == null) {
				return null;
			} else {
				modulo = map.get(key);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", codServ));
			parametrosAdicionales.add(new ParamEvent("nombreModulo", nombreModulo.replaceAll("_", "-")));
			singletonModulo.log.error(e.toString(), "GET_MODULO_CACHE", parametrosAdicionales, e);
		}

		return modulo;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<KeysModulo, BeanModulo> getMapModulo() {
		return mapModulo;
	}

	private void setMapModulo(HashMap<KeysModulo, BeanModulo> mapModulo) {
		this.mapModulo = mapModulo;
	}

}
