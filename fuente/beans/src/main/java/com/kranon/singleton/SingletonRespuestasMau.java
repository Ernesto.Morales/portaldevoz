package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.maurespuestas.BeanRespuestasMau;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanRespuestasMau CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonRespuestasMau {

	private static SingletonRespuestasMau singletonRespuestasMau;

	private HashMap<String, BeanRespuestasMau> mapServicios;
	
	private LoadingCache<String, HashMap<String, BeanRespuestasMau>> cache;
	
	private CommonLoggerService log;
	
	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonRespuestasMau() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonRespuestasMau getInstance(CommonLoggerService log) throws Exception {

		if (singletonRespuestasMau == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_RESPUESTA_MAU|NUEVA_INSTANCIA");
			singletonRespuestasMau = new SingletonRespuestasMau();
			singletonRespuestasMau.log = log;
			singletonRespuestasMau.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas veces se acceda
			        .expireAfterWrite(singletonRespuestasMau.tiempoSegCacheo, TimeUnit.SECONDS)
			        .build(new CacheLoader<String, HashMap<String, BeanRespuestasMau>>() {
			            @Override
			            /**
			             * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanRespuestasMau y se vuelve a cargar
			             * <key> es el codigo del servicio a recuperar
			             */
			            public HashMap<String, BeanRespuestasMau> load(String key) throws Exception {
			            	
			            	singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|LOAD_CACHE|codServIvr=" + key);
							if (singletonRespuestasMau.getMapServicios() == null) {
								// si la tabla es null, la creo
								singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|NEW_INSTANCE_MAP_SERVICIO");
								singletonRespuestasMau.setMapServicios(new HashMap<String, BeanRespuestasMau>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonRespuestasMau.getMapServicios().containsKey(key)) {
								singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|DELETE_BEAN_SERVICIO|codServIvr=" + key);
								singletonRespuestasMau.getMapServicios().remove(key);
							} else {
								singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|KEY_NOT_FOUND|codServIvr=" + key);
							}
						
							singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|GET_BEAN_SERVICIO|codServIvr=" + key);
	
							try {
								// Recupero el BeanRespuestasMau con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonRespuestasMau.log.getIdLlamada(), singletonRespuestasMau.log.getIdElemento());
								BeanRespuestasMau beanServicio = unmarshallProcess.unmarshallMauRespuestas(key);
										
								singletonRespuestasMau.getMapServicios().put(key, beanServicio);
								
								singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|GET_SERVICIO|OK");
//								singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|GET_SERVICIO=" + singletonRespuestasMau.getMapServicios().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonRespuestasMau.log.error(e.toString(), "SINGLETON_RESPUESTA_MAU", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}
							

			                return singletonRespuestasMau.getMapServicios();
			            }
			        });		
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_RESPUESTA_MAU|EXISTE_INSTANCIA");
			singletonRespuestasMau.log = log;
		}
				
		return singletonRespuestasMau;
	}
	
	/**
	 * Dado un codigo de servicio, recupera el BeanRespuestasMau asociado de la cache 
	 * o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanRespuestasMau}
	 */
	public BeanRespuestasMau getServicio(String cod) {
		BeanRespuestasMau serv = null;
		try {
			singletonRespuestasMau.log.comment("SINGLETON_RESPUESTA_MAU|LOAD_CACHE|codServIvr=" + cod);
			
//			serv = singletonRespuestasMau.cache.get(cod).get(cod);
			HashMap<String, BeanRespuestasMau> map = singletonRespuestasMau.cache.get(cod);
			if(map == null){
				return null;
			} else {
				serv = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonRespuestasMau.log.error(e.toString(), "SINGLETON_RESPUESTA_MAU", parametrosAdicionales, e);
		}
		
		return serv;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanRespuestasMau> getMapServicios() {
	
		return mapServicios;
	}

	private void setMapServicios(HashMap<String, BeanRespuestasMau> mapServicios) {
		this.mapServicios = mapServicios;
	}

}
