package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.event.ParamEvent;
import com.kranon.unmarshall.Unmarshall;
import com.kranon.bean.shortcut.BeanShortcuts;

/**
 * Clase para que solo haya una instancia de el BeanShortcuts
 * CACHEADA a 120 segundos
 * 
 * 
 *
 */
public class SingletonShortCuts {
	
	
	private static SingletonShortCuts singletonShortCuts;

	private HashMap<String, BeanShortcuts> mapShortCuts;

	private LoadingCache<String, HashMap<String, BeanShortcuts>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonShortCuts() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonShortCuts getInstance(CommonLoggerService log) throws Exception {

		if (singletonShortCuts == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_CONTROLADORES|NUEVA_INSTANCIA");
			singletonShortCuts = new SingletonShortCuts();
			singletonShortCuts.log = log;
			singletonShortCuts.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonShortCuts.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanShortcuts>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanShortcuts y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar sus controladores
						 */
						public HashMap<String, BeanShortcuts> load(String key) throws Exception {

							singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|LOAD_CACHE|codServIvr=" + key);
							if (singletonShortCuts.getMapGestionControladores() == null) {
								// si la tabla es null, la creo
								singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|NEW_INSTANCE_MAP_GESTION_CONTROLADORES");
								singletonShortCuts.setMapGestionControladores(new HashMap<String, BeanShortcuts>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonShortCuts.getMapGestionControladores().containsKey(key)) {
								singletonShortCuts.log
										.comment("SINGLETON_GESTION_CONTROLADORES|DELETE_BEAN_GESTION_CONTROLADORES|codServIvr=" + key);
								singletonShortCuts.getMapGestionControladores().remove(key);
							} else {
								singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_BEAN_GESTION_CONTROLADORES|codServIvr="
									+ key);

							try {
								// Recupero el BeanShortcuts con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonShortCuts.log.getIdLlamada(),
										singletonShortCuts.log.getIdElemento());
								BeanShortcuts beanGestionControladores = unmarshallProcess.unmarshallShortCuts(key);
//								beanGestionControladores.limpiar();
								singletonShortCuts.getMapGestionControladores().put(key, beanGestionControladores);

								singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_GESTION_CONTROLADORES|OK");
//								singletonShortCuts.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_GESTION_CONTROLADORES="
//										+ singletonShortCuts.getMapGestionControladores().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonShortCuts.log.error(e.toString(), "SINGLETON_GESTION_CONTROLADORES", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonShortCuts.getMapGestionControladores();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_CONTROLADORES|EXISTE_INSTANCIA");
			singletonShortCuts.log = log;
		}

		return singletonShortCuts;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanShortcuts asociado
	 * de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanShortcuts}
	 */
	public BeanShortcuts getShortcuts(String cod) {
		BeanShortcuts gestionShortcuts = null;
		try {
			singletonShortCuts.log.comment("SINGLETON_SHORTCUTS|LOAD_CACHE|codServIvr=" + cod);

			// gestionControladores =
			// singletonShortCuts.cache.get(cod).get(cod);
			HashMap<String, BeanShortcuts> map = singletonShortCuts.cache.get(cod);
			if (map == null) {
				return null;
			} else {
				gestionShortcuts = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonShortCuts.log.error(e.toString(), "GET_GESTION_CONTROLADORES_CACHE", parametrosAdicionales, e);
		}

		return gestionShortcuts;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */

	private HashMap<String, BeanShortcuts> getMapGestionControladores() {
		return mapShortCuts;
	}

	private void setMapGestionControladores(HashMap<String, BeanShortcuts> mapShortCuts) {
		this.mapShortCuts = mapShortCuts;
	}
}
