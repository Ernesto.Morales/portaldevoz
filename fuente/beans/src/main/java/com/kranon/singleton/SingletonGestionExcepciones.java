package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.excepserv.BeanGestionExcepciones;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia de el BeanGestionExcepciones CACHEADA
 * a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonGestionExcepciones {

	private static SingletonGestionExcepciones singletonGestionExcepciones;

	private HashMap<String, BeanGestionExcepciones> mapGestionExcepciones;

	private LoadingCache<String, HashMap<String, BeanGestionExcepciones>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonGestionExcepciones() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 * 
	 * @param log
	 * @param codServIvr
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionExcepciones getInstance(CommonLoggerService log) throws Exception {

		if (singletonGestionExcepciones == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_EXCEPCIONES|NUEVA_INSTANCIA");
			singletonGestionExcepciones = new SingletonGestionExcepciones();
			singletonGestionExcepciones.log = log;
			singletonGestionExcepciones.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionExcepciones.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionExcepciones>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionExcepciones y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar sus excepciones
						 */
						public HashMap<String, BeanGestionExcepciones> load(String key) throws Exception {

							singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionExcepciones.getMapGestionExcepciones() == null) {
								// si la tabla es null, la creo
								singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|NEW_INSTANCE_MAP_GESTION_EXCEPCIONES");
								singletonGestionExcepciones.setMapGestionExcepciones(new HashMap<String, BeanGestionExcepciones>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionExcepciones.getMapGestionExcepciones().containsKey(key)) {
								singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|DELETE_BEAN_GESTION_EXCEPCIONES|codServIvr="
										+ key);
								singletonGestionExcepciones.getMapGestionExcepciones().remove(key);
							} else {
								singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|GET_BEAN_GESTION_EXCEPCIONES|codServIvr=" + key);

							try {
								// Recupero el BeanGestionExcepciones con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonGestionExcepciones.log.getIdLlamada(),
										singletonGestionExcepciones.log.getIdElemento());
								BeanGestionExcepciones BeanGestionExcepciones = unmarshallProcess.unmarshallGestionExcepciones(key);

								singletonGestionExcepciones.getMapGestionExcepciones().put(key, BeanGestionExcepciones);

								singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|GET_GESTION_EXCEPCIONES|OK");
//								singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|GET_GESTION_EXCEPCIONES="
//										+ singletonGestionExcepciones.getMapGestionExcepciones().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionExcepciones.log.error(e.toString(), "SINGLETON_GESTION_EXCEPCIONES", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonGestionExcepciones.getMapGestionExcepciones();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_EXCEPCIONES|EXISTE_INSTANCIA");
			singletonGestionExcepciones.log = log;
		}

		return singletonGestionExcepciones;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanGestionExcepciones asociado
	 * de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanGestionExcepciones}
	 */
	public BeanGestionExcepciones getGestionExcepciones(String cod) {
		BeanGestionExcepciones gestionExcepciones = null;
		try {
			singletonGestionExcepciones.log.comment("SINGLETON_GESTION_EXCEPCIONES|LOAD_CACHE|codServIvr=" + cod);

			HashMap<String, BeanGestionExcepciones> map = singletonGestionExcepciones.cache.get(cod);
			if (map == null) {
				return null;
			} else {
				gestionExcepciones = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonGestionExcepciones.log.error(e.toString(), "GET-GESTION-EXCEPCIONES-CACHE", parametrosAdicionales, e);
		}

		return gestionExcepciones;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanGestionExcepciones> getMapGestionExcepciones() {
		return mapGestionExcepciones;
	}

	private void setMapGestionExcepciones(HashMap<String, BeanGestionExcepciones> mapGestionExcepciones) {
		this.mapGestionExcepciones = mapGestionExcepciones;
	}

}
