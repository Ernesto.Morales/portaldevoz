package com.kranon.singleton;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanWebServices CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonWebServices {

	private static SingletonWebServices singletonWebServices;

	private BeanWebServices beanWebServices;

	private LoadingCache<String, BeanWebServices> cache;

	private CommonLoggerProcess log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonWebServices() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonWebServices getInstance(CommonLoggerProcess log) throws Exception {

		if (singletonWebServices == null) {
			// si el patron es null, lo creo
			/** INICIO EVENTO - COMENTARIO **/
			log.comment("SINGLETON_WEB_SERVICES|NUEVA_INSTANCIA");
			/** FIN EVENTO - COMENTARIO **/

			singletonWebServices = new SingletonWebServices();
			singletonWebServices.log = log;
			singletonWebServices.cache = CacheBuilder.newBuilder()
			// expira pasados XX segundos, independientemente de cuantas
			// veces se acceda
					.expireAfterWrite(singletonWebServices.tiempoSegCacheo, TimeUnit.SECONDS).build(new CacheLoader<String, BeanWebServices>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanWebServices y se vuelve a cargar
						 * key es vacio ya que solo vamos a tener un elemento
						 */
						public BeanWebServices load(String key) throws Exception {
							/** INICIO EVENTO - COMENTARIO **/
							singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|LOAD_CACHE|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							/** INICIO EVENTO - COMENTARIO **/
							singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|NEW_INSTANCE_MAP_WEB_SERVICES");
							/** FIN EVENTO - COMENTARIO **/

							singletonWebServices.setBeanWebServices(new BeanWebServices());

							/** INICIO EVENTO - COMENTARIO **/
							singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|GET_BEAN_WEB_SERVICES|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							try {
								// Recupero el BeanWebServices con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonWebServices.log.getIdInvocacion(), singletonWebServices.log
										.getIdElemento());
								BeanWebServices beanWebServices = unmarshallProcess.unmarshallWebServices(key);

								singletonWebServices.setBeanWebServices(beanWebServices);

								if (beanWebServices == null) {
									throw new Exception("SINGLETON_WEB_SERVICES|KEY_NOT_FOUND|codServIvr=" + key);
								}

								singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|GET_WEB_SERVICES|OK");
								/** INICIO EVENTO - COMENTARIO **/
								// singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|GET_WEB_SERVICES="
								// + singletonWebServices.getBeanWebServices().toString() + "|OK");
								/** FIN EVENTO - COMENTARIO **/
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								singletonWebServices.log.error(e.toString(), "SINGLETON_WEB_SERVICES", null, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonWebServices.getBeanWebServices();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_WEB_SERVICES|EXISTE_INSTANCIA");
			singletonWebServices.log = log;
		}

		return singletonWebServices;
	}

	/**
	 * Dado un codigo de servicio y un nombre de controlador, recupera el BeanWebServices asociado de la cache o vuelve a leer del XML para actualizar
	 * la instancia
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanWebServices}
	 */
	public BeanWebServices getWebServices() {
		return this.getWebServices("");
	}

	/**
	 * Dado un codigo de servicio y un nombre de controlador, recupera el BeanWebServices asociado de la cache o vuelve a leer del XML para actualizar
	 * la instancia
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanWebServices}
	 */
	public BeanWebServices getWebServices(String codServicio) {
		BeanWebServices bean = null;
		try {
			/** INICIO EVENTO - COMENTARIO **/
			singletonWebServices.log.comment("SINGLETON_WEB_SERVICES|LOAD_CACHE|codServIvr=" + codServicio);
			/** FIN EVENTO - COMENTARIO **/

			bean = singletonWebServices.cache.get(codServicio);
			
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			singletonWebServices.log.error(e.toString(), "GET_WEB_SERVICES_CACHE", null, e);
			/** FIN EVENTO - ERROR **/
		}

		return bean;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private BeanWebServices getBeanWebServices() {
		return beanWebServices;
	}

	private void setBeanWebServices(BeanWebServices beanWebServices) {
		this.beanWebServices = beanWebServices;
	}

}
