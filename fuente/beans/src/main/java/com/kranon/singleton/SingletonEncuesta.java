package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.enc.BeanEncuesta;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia del BeanEncuesta CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonEncuesta {

	private static SingletonEncuesta singletonEncuesta;

	private HashMap<KeysEncuesta, BeanEncuesta> mapEncuesta;

	private LoadingCache<KeysEncuesta, HashMap<KeysEncuesta, BeanEncuesta>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	// Clase para recibir la <clave> de busqueda
	public class KeysEncuesta {
		public String codServ;
		public String nombreEncuesta;

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public boolean equals(Object obj) {
			if (obj instanceof KeysEncuesta) {
				KeysEncuesta other = (KeysEncuesta) obj;
				EqualsBuilder builder = new EqualsBuilder();
				builder.append(this.codServ, other.codServ);
				builder.append(this.nombreEncuesta, other.nombreEncuesta);
				return builder.isEquals();
			}
			return false;
		}

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public int hashCode() {
			HashCodeBuilder builder = new HashCodeBuilder();
			builder.append(codServ);
			builder.append(nombreEncuesta);
			return builder.toHashCode();
		}
	}

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonEncuesta() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonEncuesta getInstance(CommonLoggerService log) throws Exception {

		if (singletonEncuesta == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_ENCUESTA|NUEVA_INSTANCIA");
			singletonEncuesta = new SingletonEncuesta();
			singletonEncuesta.log = log;
			singletonEncuesta.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonEncuesta.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<KeysEncuesta, HashMap<KeysEncuesta, BeanEncuesta>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanEncuesta y se vuelve a cargar
						 * key es un objeto de tipo KeysEncuesta con el codigo del servicio y nombre de la encuesta a recuperar
						 */
						public HashMap<KeysEncuesta, BeanEncuesta> load(KeysEncuesta key) throws Exception {

							singletonEncuesta.log
									.comment("SINGLETON_ENCUESTA|LOAD_CACHE|codServIvr=" + key.codServ + "|nombreEncuesta=" + key.nombreEncuesta);
							if (singletonEncuesta.getMapEncuesta() == null) {
								// si la tabla es null, la creo
								singletonEncuesta.log.comment("SINGLETON_ENCUESTA|NEW_INSTANCE_MAP_ENCUESTA");
								singletonEncuesta.setMapEncuesta(new HashMap<KeysEncuesta, BeanEncuesta>());
							}
							// si el mapa contiene la encuesta, lo elimino
							if (singletonEncuesta.getMapEncuesta().containsKey(key)) {
								singletonEncuesta.log.comment("SINGLETON_ENCUESTA|DELETE_BEAN_ENCUESTA|codServIvr=" + key.codServ + "|nombreEncuesta="
										+ key.nombreEncuesta);
								singletonEncuesta.getMapEncuesta().remove(key);
							} else {
								singletonEncuesta.log.comment("SINGLETON_ENCUESTA|KEY_NOT_FOUND|codServIvr=" + key.codServ + "|nombreEncuesta="
										+ key.nombreEncuesta);
							}

							singletonEncuesta.log.comment("SINGLETON_ENCUESTA|GET_BEAN_ENCUESTA|codServIvr=" + key.codServ + "|nombreEncuesta="
									+ key.nombreEncuesta);

							try {

								// Recupero el BeanEncuesta con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonEncuesta.log.getIdLlamada(), singletonEncuesta.log.getIdElemento());
								
								// [20160822] cambio barras bajas en el nombre
								// encuesta por guiones
								String nombreEncuestaAux = key.nombreEncuesta.replaceAll("_", "-");
								BeanEncuesta beanEncuesta = unmarshallProcess.unmarshallEncuesta(key.codServ, nombreEncuestaAux);

								singletonEncuesta.getMapEncuesta().put(key, beanEncuesta);


							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key.codServ));
								parametrosAdicionales.add(new ParamEvent("nombreEncuesta", key.nombreEncuesta));
								singletonEncuesta.log.error(e.toString(), "SINGLETON_ENCUESTA", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonEncuesta.getMapEncuesta();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_ENCUESTA|EXISTE_INSTANCIA");
			singletonEncuesta.log = log;
		}

		return singletonEncuesta;
	}

	/**
	 * Dado un codigo de servicio y un nombre de modulo, recupera el BeanEncuesta
	 * asociado de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param codServ
	 * @param nombreEncuesta
	 * @return {@link BeanEncuesta}
	 */
	public BeanEncuesta getEncuesta(String codServ, String nombreEncuesta) {
		BeanEncuesta encuesta = null;
		try {
			// [20160822] cambio barras bajas en el nombre modulo por guiones
			String nombreEncuestaAux = nombreEncuesta.replaceAll("_", "-");

			singletonEncuesta.log.comment("SINGLETON_ENCUESTA|LOAD_CACHE|codServIvr=" + codServ + "|nombreEncuesta=" + nombreEncuestaAux);
			SingletonEncuesta.KeysEncuesta key = new KeysEncuesta();
			key.codServ = codServ;
			key.nombreEncuesta = nombreEncuestaAux;

			HashMap<KeysEncuesta, BeanEncuesta> map = singletonEncuesta.cache.get(key);
			if (map == null) {
				return null;
			} else {
				encuesta = map.get(key);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", codServ));
			parametrosAdicionales.add(new ParamEvent("nombreEncuesta", nombreEncuesta.replaceAll("_", "-")));
			singletonEncuesta.log.error(e.toString(), "GET_ENCUESTA_CACHE", parametrosAdicionales, e);
		}

		return encuesta;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<KeysEncuesta, BeanEncuesta> getMapEncuesta() {
		return mapEncuesta;
	}

	private void setMapEncuesta(HashMap<KeysEncuesta, BeanEncuesta> mapEncuesta) {
		this.mapEncuesta = mapEncuesta;
	}

}
