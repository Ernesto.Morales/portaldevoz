package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.gestioncontroladores.BeanGestionControladores;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia de el BeanGestionControladores
 * CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonGestionControladores {

	private static SingletonGestionControladores singletonGestionControladores;

	private HashMap<String, BeanGestionControladores> mapGestionControladores;

	private LoadingCache<String, HashMap<String, BeanGestionControladores>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonGestionControladores() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionControladores getInstance(CommonLoggerService log) throws Exception {

		if (singletonGestionControladores == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_CONTROLADORES|NUEVA_INSTANCIA");
			singletonGestionControladores = new SingletonGestionControladores();
			singletonGestionControladores.log = log;
			singletonGestionControladores.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionControladores.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionControladores>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionControladores y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar sus controladores
						 */
						public HashMap<String, BeanGestionControladores> load(String key) throws Exception {

							singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionControladores.getMapGestionControladores() == null) {
								// si la tabla es null, la creo
								singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|NEW_INSTANCE_MAP_GESTION_CONTROLADORES");
								singletonGestionControladores.setMapGestionControladores(new HashMap<String, BeanGestionControladores>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionControladores.getMapGestionControladores().containsKey(key)) {
								singletonGestionControladores.log
										.comment("SINGLETON_GESTION_CONTROLADORES|DELETE_BEAN_GESTION_CONTROLADORES|codServIvr=" + key);
								singletonGestionControladores.getMapGestionControladores().remove(key);
							} else {
								singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_BEAN_GESTION_CONTROLADORES|codServIvr="
									+ key);

							try {
								// Recupero el BeanGestionControladores con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonGestionControladores.log.getIdLlamada(),
										singletonGestionControladores.log.getIdElemento());
								BeanGestionControladores beanGestionControladores = unmarshallProcess.unmarshallGestionControladores(key);

								singletonGestionControladores.getMapGestionControladores().put(key, beanGestionControladores);

								singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_GESTION_CONTROLADORES|OK");
//								singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|GET_GESTION_CONTROLADORES="
//										+ singletonGestionControladores.getMapGestionControladores().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionControladores.log.error(e.toString(), "SINGLETON_GESTION_CONTROLADORES", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonGestionControladores.getMapGestionControladores();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_CONTROLADORES|EXISTE_INSTANCIA");
			singletonGestionControladores.log = log;
		}

		return singletonGestionControladores;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanGestionControladores asociado
	 * de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanGestionControladores}
	 */
	public BeanGestionControladores getGestionControladores(String cod) {
		BeanGestionControladores gestionControladores = null;
		try {
			singletonGestionControladores.log.comment("SINGLETON_GESTION_CONTROLADORES|LOAD_CACHE|codServIvr=" + cod);

			// gestionControladores =
			// singletonGestionControladores.cache.get(cod).get(cod);
			HashMap<String, BeanGestionControladores> map = singletonGestionControladores.cache.get(cod);
			if (map == null) {
				return null;
			} else {
				gestionControladores = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonGestionControladores.log.error(e.toString(), "GET_GESTION_CONTROLADORES_CACHE", parametrosAdicionales, e);
		}

		return gestionControladores;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */

	private HashMap<String, BeanGestionControladores> getMapGestionControladores() {
		return mapGestionControladores;
	}

	private void setMapGestionControladores(HashMap<String, BeanGestionControladores> mapGestionControladores) {
		this.mapGestionControladores = mapGestionControladores;
	}

}
