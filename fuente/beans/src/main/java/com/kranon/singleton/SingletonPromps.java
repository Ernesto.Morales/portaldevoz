package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.promp.BeanPromps;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanRespuestasMau CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonPromps {

	private static SingletonPromps SingletonPromps;

	private HashMap<String, BeanPromps> mapServicios;
	
	private LoadingCache<String, HashMap<String, BeanPromps>> cache;
	
	private CommonLoggerService log;
	
	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonPromps() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonPromps getInstance(CommonLoggerService log) throws Exception {

		if (SingletonPromps == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_PROMS|NUEVA_INSTANCIA");
			SingletonPromps = new SingletonPromps();
			SingletonPromps.log = log;
			SingletonPromps.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas veces se acceda
			        .expireAfterWrite(SingletonPromps.tiempoSegCacheo, TimeUnit.SECONDS)
			        .build(new CacheLoader<String, HashMap<String, BeanPromps>>() {
			            @Override
			            /**
			             * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanPromps y se vuelve a cargar
			             * <key> es el codigo del servicio a recuperar
			             */
			            public HashMap<String, BeanPromps> load(String key) throws Exception {
			            	
			            	SingletonPromps.log.comment("SINGLETON_PROMS|LOAD_CACHE|codServIvr=" + key);
							if (SingletonPromps.getMapServicios() == null) {
								// si la tabla es null, la creo
								SingletonPromps.log.comment("SINGLETON_PROMS|NEW_INSTANCE_MAP_SERVICIO");
								SingletonPromps.setMapServicios(new HashMap<String, BeanPromps>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (SingletonPromps.getMapServicios().containsKey(key)) {
								SingletonPromps.log.comment("SINGLETON_PROMS|DELETE_BEAN_SERVICIO|codServIvr=" + key);
								SingletonPromps.getMapServicios().remove(key);
							} else {
								SingletonPromps.log.comment("SINGLETON_PROMS|KEY_NOT_FOUND|codServIvr=" + key);
							}
						
							SingletonPromps.log.comment("SINGLETON_PROMS|GET_BEAN_SERVICIO|codServIvr=" + key);
	
							try {
								// Recupero el BeanPromps con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(SingletonPromps.log.getIdLlamada(), SingletonPromps.log.getIdElemento());
								BeanPromps beanServicio = unmarshallProcess.unmarshallPromps(key);
										
								SingletonPromps.getMapServicios().put(key, beanServicio);
								
								SingletonPromps.log.comment("SINGLETON_PROMS|GET_SERVICIO|OK");
//								SingletonPromps.log.comment("SINGLETON_PROMS|GET_SERVICIO=" + SingletonPromps.getMapServicios().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								SingletonPromps.log.error(e.toString(), "SINGLETON_PROMS", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}
							

			                return SingletonPromps.getMapServicios();
			            }
			        });		
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_PROMS|EXISTE_INSTANCIA");
			SingletonPromps.log = log;
		}
				
		return SingletonPromps;
	}
	
	/**
	 * Dado un codigo de servicio, recupera el BeanPromps asociado de la cache 
	 * o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanPromps}
	 */
	public BeanPromps getServicio(String cod) {
		BeanPromps serv = null;
		try {
			SingletonPromps.log.comment("SINGLETON_PROMS|LOAD_CACHE|codServIvr=" + cod);
			
//			serv = SingletonPromps.cache.get(cod).get(cod);
			HashMap<String, BeanPromps> map = SingletonPromps.cache.get(cod);
			if(map == null){
				return null;
			} else {
				serv = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			SingletonPromps.log.error(e.toString(), "SINGLETON_PROMS", parametrosAdicionales, e);
		}
		
		return serv;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanPromps> getMapServicios() {
	
		return mapServicios;
	}

	private void setMapServicios(HashMap<String, BeanPromps> mapServicios) {
		this.mapServicios = mapServicios;
	}

}
