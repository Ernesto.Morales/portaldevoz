package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.gestioncm.BeanGestionCuadroMando;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia de el BeanGestionCuadroMando CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonGestionCuadroMando {

	private static SingletonGestionCuadroMando singletonGestionCuadroMando;

	private HashMap<String, BeanGestionCuadroMando> mapGestionCuadroMando;

	private LoadingCache<String, HashMap<String, BeanGestionCuadroMando>> cache;

	private CommonLoggerProcess log;
	private CommonLoggerService logService;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonGestionCuadroMando() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionCuadroMando getInstance(CommonLoggerProcess log) throws Exception {

		if (singletonGestionCuadroMando == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_CUADRO_MANDO|NUEVA_INSTANCIA");
			singletonGestionCuadroMando = new SingletonGestionCuadroMando();
			singletonGestionCuadroMando.log = log;
			singletonGestionCuadroMando.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionCuadroMando.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionCuadroMando>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionCuadroMando y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar su gestion de cuadro mando
						 */
						public HashMap<String, BeanGestionCuadroMando> load(String key) throws Exception {

							singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionCuadroMando.getMapGestionCuadroMando() == null) {
								// si la tabla es null, la creo
								singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|NEW_INSTANCE_MAP_GESTION_CUADRO_MANDO");
								singletonGestionCuadroMando.setMapGestionCuadroMando(new HashMap<String, BeanGestionCuadroMando>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionCuadroMando.getMapGestionCuadroMando().containsKey(key)) {
								singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|DELETE_BEAN_GESTION_CUADRO_MANDO|codServIvr="
										+ key);
								singletonGestionCuadroMando.getMapGestionCuadroMando().remove(key);
							} else {
								singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_BEAN_GESTION_CUADRO_MANDO|codServIvr=" + key);

							try {
								// Recupero el BeanGestionCuadroMando con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonGestionCuadroMando.log.getIdInvocacion(),
										singletonGestionCuadroMando.log.getIdElemento());
								BeanGestionCuadroMando beanGestionCuadroMando = unmarshallProcess.unmarshallGestionCuadroMando(key);

								singletonGestionCuadroMando.getMapGestionCuadroMando().put(key, beanGestionCuadroMando);

								singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_GESTION_CUADRO_MANDO|OK");
								// singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_GESTION_CUADRO_MANDO="
								// + singletonGestionCuadroMando.getMapGestionCuadroMando().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionCuadroMando.log.error(e.toString(), "SINGLETON_GESTION_CUADRO_MANDO", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonGestionCuadroMando.getMapGestionCuadroMando();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_CUADRO_MANDO|EXISTE_INSTANCIA");
			singletonGestionCuadroMando.log = log;
		}

		return singletonGestionCuadroMando;
	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionCuadroMando getInstance(CommonLoggerService log) throws Exception {

		if (singletonGestionCuadroMando == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_CUADRO_MANDO|NUEVA_INSTANCIA");
			singletonGestionCuadroMando = new SingletonGestionCuadroMando();
			singletonGestionCuadroMando.logService = log;
			singletonGestionCuadroMando.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionCuadroMando.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionCuadroMando>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionCuadroMando y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar su gestion de cuadro mando
						 */
						public HashMap<String, BeanGestionCuadroMando> load(String key) throws Exception {

							singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionCuadroMando.getMapGestionCuadroMando() == null) {
								// si la tabla es null, la creo
								singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|NEW_INSTANCE_MAP_GESTION_CUADRO_MANDO");
								singletonGestionCuadroMando.setMapGestionCuadroMando(new HashMap<String, BeanGestionCuadroMando>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionCuadroMando.getMapGestionCuadroMando().containsKey(key)) {
								singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|DELETE_BEAN_GESTION_CUADRO_MANDO|codServIvr="
										+ key);
								singletonGestionCuadroMando.getMapGestionCuadroMando().remove(key);
							} else {
								singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_BEAN_GESTION_CUADRO_MANDO|codServIvr=" + key);

							try {
								// Recupero el BeanGestionCuadroMando con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonGestionCuadroMando.logService.getIdLlamada(),
										singletonGestionCuadroMando.logService.getIdElemento());
								BeanGestionCuadroMando beanGestionCuadroMando = unmarshallProcess.unmarshallGestionCuadroMando(key);

								singletonGestionCuadroMando.getMapGestionCuadroMando().put(key, beanGestionCuadroMando);

								singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_GESTION_CUADRO_MANDO|OK");
								// singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|GET_GESTION_CUADRO_MANDO="
								// + singletonGestionCuadroMando.getMapGestionCuadroMando().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionCuadroMando.logService.error(e.toString(), "SINGLETON_GESTION_CUADRO_MANDO", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/

								return null;
							}

							return singletonGestionCuadroMando.getMapGestionCuadroMando();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_CUADRO_MANDO|EXISTE_INSTANCIA");
			singletonGestionCuadroMando.logService = log;
		}

		return singletonGestionCuadroMando;
	}
	
	/**
	 * Dado un codigo de servicio, recupera el BeanGestionCuadroMando asociado de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanGestionCuadroMando}
	 */
	public BeanGestionCuadroMando getGestionCuadroMando(String cod) {
		BeanGestionCuadroMando gestionCuadroMando = null;
		try {
			if (log != null)
				singletonGestionCuadroMando.log.comment("SINGLETON_GESTION_CUADRO_MANDO|LOAD_CACHE|codServIvr=" + cod);
			else
				singletonGestionCuadroMando.logService.comment("SINGLETON_GESTION_CUADRO_MANDO|LOAD_CACHE|codServIvr=" + cod);
			
			// gestionCuadroMando = singletonGestionCuadroMando.cache.get(cod).get(cod);
			HashMap<String, BeanGestionCuadroMando> map = singletonGestionCuadroMando.cache.get(cod);
			if (map == null) {
				return null;
			} else {
				gestionCuadroMando = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			if (log != null)
				singletonGestionCuadroMando.log.error(e.toString(), "GET_GESTION_CUADRO_MANDO_CACHE", parametrosAdicionales, e);
			else
				singletonGestionCuadroMando.logService.error(e.toString(), "GET_GESTION_CUADRO_MANDO_CACHE", parametrosAdicionales, e);
		}

		return gestionCuadroMando;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanGestionCuadroMando> getMapGestionCuadroMando() {
		return mapGestionCuadroMando;
	}

	private void setMapGestionCuadroMando(HashMap<String, BeanGestionCuadroMando> mapGestionCuadroMando) {
		this.mapGestionCuadroMando = mapGestionCuadroMando;
	}

}
