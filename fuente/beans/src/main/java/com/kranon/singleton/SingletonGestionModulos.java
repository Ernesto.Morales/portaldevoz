package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.gestionmodulos.BeanGestionModulos;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia de el BeanGestionModulos
 * CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonGestionModulos {

	private static SingletonGestionModulos singletonGestionModulos;

	private HashMap<String, BeanGestionModulos> mapGestionModulos;

	private LoadingCache<String, HashMap<String, BeanGestionModulos>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonGestionModulos() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionModulos getInstance(CommonLoggerService log) throws Exception {

		if (singletonGestionModulos == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_MODULOS|NUEVA_INSTANCIA");
			singletonGestionModulos = new SingletonGestionModulos();
			singletonGestionModulos.log = log;
			singletonGestionModulos.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionModulos.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionModulos>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionModulos y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar sus modulos
						 */
						public HashMap<String, BeanGestionModulos> load(String key) throws Exception {

							singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionModulos.getMapGestionModulos() == null) {
								// si la tabla es null, la creo
								singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|NEW_INSTANCE_MAP_GESTION_MODULOS");
								singletonGestionModulos.setMapGestionModulos(new HashMap<String, BeanGestionModulos>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionModulos.getMapGestionModulos().containsKey(key)) {
								singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|DELETE_BEAN_GESTION_MODULOS|codServIvr=" + key);
								singletonGestionModulos.getMapGestionModulos().remove(key);
							} else {
								singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|GET_BEAN_GESTION_MODULOS|codServIvr=" + key);

							try {
								// Recupero el BeanGestionModulos con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(
										singletonGestionModulos.log.getIdLlamada(), singletonGestionModulos.log.getIdElemento());
								BeanGestionModulos beanGestionModulos = unmarshallProcess.unmarshallGestionModulos(key);

								singletonGestionModulos.getMapGestionModulos().put(key, beanGestionModulos);

								singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|GET_GESTION_MODULOS|OK");
//								singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|GET_GESTION_MODULOS="
//										+ singletonGestionModulos.getMapGestionModulos().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionModulos.log.error(e.toString(), "SINGLETON_GESTION_MODULOS", parametrosAdicionales,e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}

							return singletonGestionModulos.getMapGestionModulos();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_MODULOS|EXISTE_INSTANCIA");
			singletonGestionModulos.log = log;
		}

		return singletonGestionModulos;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanGestionModulos asociado
	 * de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanGestionModulos}
	 */
	public BeanGestionModulos getGestionModulos(String cod) {
		BeanGestionModulos gestionModulos = null;
		try {
			singletonGestionModulos.log.comment("SINGLETON_GESTION_MODULOS|LOAD_CACHE|codServIvr=" + cod);
			
//			gestionModulos = singletonGestionModulos.cache.get(cod).get(cod);
			HashMap<String, BeanGestionModulos> map = singletonGestionModulos.cache.get(cod);
			if(map == null){
				return null;
			} else {
				gestionModulos = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonGestionModulos.log.error(e.toString(), "GET_GESTION_MODULOS_CACHE", parametrosAdicionales,e);
		}

		return gestionModulos;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */

	private HashMap<String, BeanGestionModulos> getMapGestionModulos() {
		return mapGestionModulos;
	}

	private void setMapGestionModulos(HashMap<String, BeanGestionModulos> mapGestionModulos) {
		this.mapGestionModulos = mapGestionModulos;
	}

}
