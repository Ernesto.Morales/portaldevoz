package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.menu.BeanMenu;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia del BeanMenu CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonMenuEncuesta {

	private static SingletonMenuEncuesta singletonMenu;

	private HashMap<KeysMenu, BeanMenu> mapMenu;
	
	private LoadingCache<KeysMenu, HashMap<KeysMenu, BeanMenu>> cache;
	
	private CommonLoggerProcess log;
	
	private final int tiempoSegCacheo = 120;
	
	// Clase para recibir la <clave> de busqueda
	public class KeysMenu {
		public String codServ;
		public String nombreMenu;	
		
		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public boolean equals(Object obj){
	        if (obj instanceof KeysMenu) {
	        	KeysMenu other = (KeysMenu) obj;
	            EqualsBuilder builder = new EqualsBuilder();
	            builder.append(this.codServ, other.codServ);
	            builder.append(this.nombreMenu, other.nombreMenu);
	            return builder.isEquals();
	        }
	        return false;
	    }

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
	    public int hashCode(){
	        HashCodeBuilder builder = new HashCodeBuilder();
	        builder.append(codServ);
	        builder.append(nombreMenu);
	        return builder.toHashCode();
	    }
	}

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonMenuEncuesta() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonMenuEncuesta getInstance(CommonLoggerProcess log) throws Exception {

		if (singletonMenu == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_MENU_ENC|NUEVA_INSTANCIA");
			singletonMenu = new SingletonMenuEncuesta();
			singletonMenu.log = log;
			singletonMenu.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas veces se acceda
			        .expireAfterWrite(singletonMenu.tiempoSegCacheo, TimeUnit.SECONDS)
			        .build(new CacheLoader<KeysMenu, HashMap<KeysMenu, BeanMenu>>() {
			            @Override
			            /**
			             * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanMenu y se vuelve a cargar
			             * key es un objeto de tipo KeysMenu con el codigo del servicio y nombre de menu a recuperar
			             */
			            public HashMap<KeysMenu, BeanMenu> load(KeysMenu key) throws Exception {
			            	
			            	singletonMenu.log.comment("SINGLETON_MENU_ENC|LOAD_CACHE|codServIvr=" + key.codServ + "|nombreMenu=" + key.nombreMenu);
							if (singletonMenu.getMapMenu() == null) {
								// si la tabla es null, la creo
								singletonMenu.log.comment("SINGLETON_MENU_ENC|NEW_INSTANCE_MAP_MENU");
								singletonMenu.setMapMenu(new HashMap<KeysMenu, BeanMenu>());
							}
							// si el mapa contiene el menu, lo elimino
							if (singletonMenu.getMapMenu().containsKey(key)) {
								singletonMenu.log.comment("SINGLETON_MENU_ENC|DELETE_BEAN_MENU|codServIvr=" + key.codServ + "|nombreMenu=" + key.nombreMenu);
								singletonMenu.getMapMenu().remove(key);
							} else {
								singletonMenu.log.comment("SINGLETON_MENU_ENC|KEY_NOT_FOUND|codServIvr=" + key.codServ + "|nombreMenu=" + key.nombreMenu);
							}
						
							singletonMenu.log.comment("SINGLETON_MENU_ENC|GET_BEAN_MENU|codServIvr=" + key.codServ + "|nombreMenu=" + key.nombreMenu);
	
							try {
								// Recupero el BeanMenu con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(
										singletonMenu.log.getIdInvocacion(), singletonMenu.log.getIdElemento());
								BeanMenu beanMenu = unmarshallProcess.unmarshallMenuEncuesta(key.codServ, key.nombreMenu);
										
								singletonMenu.getMapMenu().put(key, beanMenu);
								
								singletonMenu.log.comment("SINGLETON_MENU_ENC|GET_MENU=" + singletonMenu.getMapMenu().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key.codServ));
								parametrosAdicionales.add(new ParamEvent("nombreMenu", key.nombreMenu));
								singletonMenu.log.error(e.toString(), "SINGLETON_MENU_ENC", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}
							

			                return singletonMenu.getMapMenu();
			            }
			        });		
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_MENU|EXISTE_INSTANCIA");
			singletonMenu.log = log;
		}
				
		return singletonMenu;
	}
	
	
	/**
	 * Dado un codigo de servicio y un nombre de menu, recupera el BeanMenu asociado de la cache 
	 * o vuelve a leer del XML para actualizar la instancia
	 * @param codServ
	 * @param nombreMenu
	 * @return {@link BeanMenu}
	 */
	public BeanMenu getMenuEnc(String codServ, String nombreMenu) {
		BeanMenu menu = null;
		try {
			singletonMenu.log.comment("SINGLETON_MENU_ENC|LOAD_CACHE|codServIvr=" + codServ + "|nombreMenu=" + nombreMenu);
			SingletonMenuEncuesta.KeysMenu key = new KeysMenu();
			key.codServ=codServ;
			key.nombreMenu = nombreMenu;
			
			HashMap<KeysMenu, BeanMenu> map = singletonMenu.cache.get(key);
			if(map == null){
				return null;
			} else {
				menu = map.get(key);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", codServ));
			parametrosAdicionales.add(new ParamEvent("nombreMenu", nombreMenu));
			singletonMenu.log.error(e.toString(), "SINGLETON_MENU_ENC", parametrosAdicionales, e);
		}
		
		return menu;
	}

	

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<KeysMenu, BeanMenu> getMapMenu() {
		return mapMenu;
	}

	private void setMapMenu(HashMap<KeysMenu, BeanMenu> mapMenu) {
		this.mapMenu = mapMenu;
	}
	
}
