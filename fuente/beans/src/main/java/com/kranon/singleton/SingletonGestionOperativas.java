package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.gestoperativas.BeanGestionOperativas;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia de el BeanGestionOperativas
 * CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonGestionOperativas {

	private static SingletonGestionOperativas singletonGestionOperativas;

	private HashMap<String, BeanGestionOperativas> mapGestionOperativas;

	private LoadingCache<String, HashMap<String, BeanGestionOperativas>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonGestionOperativas() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonGestionOperativas getInstance(CommonLoggerService log) throws Exception {

		if (singletonGestionOperativas == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_GESTION_OPERATIVAS|NUEVA_INSTANCIA");
			singletonGestionOperativas = new SingletonGestionOperativas();
			singletonGestionOperativas.log = log;
			singletonGestionOperativas.cache = CacheBuilder
					.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas
					// veces se acceda
					.expireAfterWrite(singletonGestionOperativas.tiempoSegCacheo, TimeUnit.SECONDS)
					.build(new CacheLoader<String, HashMap<String, BeanGestionOperativas>>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanGestionOperativas y se vuelve a cargar
						 * <key> es el codigo del servicio del que se tienen que recuperar sus operativas
						 */
						public HashMap<String, BeanGestionOperativas> load(String key) throws Exception {

							singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|LOAD_CACHE|codServIvr=" + key);
							if (singletonGestionOperativas.getMapGestionOperativas() == null) {
								// si la tabla es null, la creo
								singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|NEW_INSTANCE_MAP_GESTION_OPERATIVAS");
								singletonGestionOperativas.setMapGestionOperativas(new HashMap<String, BeanGestionOperativas>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonGestionOperativas.getMapGestionOperativas().containsKey(key)) {
								singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|DELETE_BEAN_GESTION_OPERATIVAS|codServIvr=" + key);
								singletonGestionOperativas.getMapGestionOperativas().remove(key);
							} else {
								singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|KEY_NOT_FOUND|codServIvr=" + key);
							}

							singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|GET_BEAN_GESTION_OPERATIVAS|codServIvr=" + key);

							try {
								// Recupero el BeanGestionOperativas con ese
								// codigo
								Unmarshall unmarshallProcess = new Unmarshall(
										singletonGestionOperativas.log.getIdLlamada(), singletonGestionOperativas.log.getIdElemento());
								BeanGestionOperativas beanGestionOperativas = unmarshallProcess.unmarshallGestionOperativas(key);

								singletonGestionOperativas.getMapGestionOperativas().put(key, beanGestionOperativas);

								singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|GET_GESTION_OPERATIVAS|OK");
//								singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|GET_GESTION_OPERATIVAS="
//										+ singletonGestionOperativas.getMapGestionOperativas().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonGestionOperativas.log.error(e.toString(), "SINGLETON_GESTION_OPERATIVAS", parametrosAdicionales,e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}

							return singletonGestionOperativas.getMapGestionOperativas();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_GESTION_OPERATIVAS|EXISTE_INSTANCIA");
			singletonGestionOperativas.log = log;
		}

		return singletonGestionOperativas;
	}

	/**
	 * Dado un codigo de servicio, recupera el BeanGestionOperativas asociado
	 * de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanGestionOperativas}
	 */
	public BeanGestionOperativas getGestionOperativas(String cod) {
		BeanGestionOperativas gestionOperativas = null;
		try {
			singletonGestionOperativas.log.comment("SINGLETON_GESTION_OPERATIVAS|LOAD_CACHE|codServIvr=" + cod);
			
//			gestionOperativas = singletonGestionOperativas.cache.get(cod).get(cod);
			HashMap<String, BeanGestionOperativas> map = singletonGestionOperativas.cache.get(cod);
			if(map == null){
				return null;
			} else {
				gestionOperativas = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonGestionOperativas.log.error(e.toString(), "GET_GESTION_OPERATIVAS_CACHE", parametrosAdicionales,e);
		}

		return gestionOperativas;
	}

	/**
	 * Se privatizan para evitar llamadas externas
	 * 
	 */

	private HashMap<String, BeanGestionOperativas> getMapGestionOperativas() {
		return mapGestionOperativas;
	}

	private void setMapGestionOperativas(HashMap<String, BeanGestionOperativas> mapGestionOperativas) {
		this.mapGestionOperativas = mapGestionOperativas;
	}

}
