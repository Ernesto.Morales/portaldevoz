package com.kranon.singleton;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.transfer_schema.BeanTransferSchema;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanTransferSchema CACHEADA a 120
 * segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonTransferSchema {

	private static SingletonTransferSchema singletonTransferSchema;

	private BeanTransferSchema beanTransferSchema;

	private LoadingCache<String, BeanTransferSchema> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonTransferSchema() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonTransferSchema getInstance(CommonLoggerService log) throws Exception {

		if (singletonTransferSchema == null) {
			// si el patron es null, lo creo
			/** INICIO EVENTO - COMENTARIO **/
			log.comment("SINGLETON_TRANSFER_SCHEMA|NUEVA_INSTANCIA");
			/** FIN EVENTO - COMENTARIO **/

			singletonTransferSchema = new SingletonTransferSchema();
			singletonTransferSchema.log = log;
			singletonTransferSchema.cache = CacheBuilder.newBuilder()
			// expira pasados XX segundos, independientemente de cuantas
			// veces se acceda
					.expireAfterWrite(singletonTransferSchema.tiempoSegCacheo, TimeUnit.SECONDS).build(new CacheLoader<String, BeanTransferSchema>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanTransferSchema y se vuelve a cargar
						 * key es vacio ya que solo vamos a tener un elemento
						 */
						public BeanTransferSchema load(String key) throws Exception {
							/** INICIO EVENTO - COMENTARIO **/
							singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|LOAD_CACHE|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							/** INICIO EVENTO - COMENTARIO **/
							singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|NEW_INSTANCE_MAP_TRANSFER_SCHEMA");
							/** FIN EVENTO - COMENTARIO **/

							singletonTransferSchema.setBeanTransferSchema(new BeanTransferSchema());

							/** INICIO EVENTO - COMENTARIO **/
							singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|GET_BEAN_TRANSFER_SCHEMA|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							try {
								// Recupero el BeanTransferSchema con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonTransferSchema.log.getIdLlamada(), singletonTransferSchema.log
										.getIdElemento());
								BeanTransferSchema beanTransferSchema = unmarshallProcess.unmarshallTransferSchema();
								
								singletonTransferSchema.setBeanTransferSchema(beanTransferSchema);
								
								if(beanTransferSchema == null){
									throw new Exception("SINGLETON_TRANSFER_SCHEMA|KEY_NOT_FOUND|codServIvr=" + key);
								}

								singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|GET_TRANSFER_SCHEMA|OK");
								/** INICIO EVENTO - COMENTARIO **/
//								singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|GET_TRANSFER_SCHEMA="
//										+ singletonTransferSchema.getBeanTransferSchema().toString() + "|OK");
								/** FIN EVENTO - COMENTARIO **/
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								singletonTransferSchema.log.error(e.toString(), "SINGLETON_TRANSFER_SCHEMA", null, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}

							return singletonTransferSchema.getBeanTransferSchema();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_TRANSFER_SCHEMA|EXISTE_INSTANCIA");
			singletonTransferSchema.log = log;
		}

		return singletonTransferSchema;
	}

	/**
	 * Dado un codigo de servicio y un nombre de controlador, recupera el
	 * BeanTransferSchema asociado de la cache o vuelve a leer del XML para
	 * actualizar la instancia
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanTransferSchema}
	 */
	public BeanTransferSchema getTransferSchema() {
		BeanTransferSchema bean = null;
		try {
			/** INICIO EVENTO - COMENTARIO **/
			singletonTransferSchema.log.comment("SINGLETON_TRANSFER_SCHEMA|LOAD_CACHE");
			/** FIN EVENTO - COMENTARIO **/

			bean = singletonTransferSchema.cache.get("");

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			singletonTransferSchema.log.error(e.toString(), "GET_TRANSFER_SCHEMA_CACHE", null, e);
			/** FIN EVENTO - ERROR **/
		}

		return bean;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private BeanTransferSchema getBeanTransferSchema() {
		return beanTransferSchema;
	}

	private void setBeanTransferSchema(BeanTransferSchema beanTransferSchema) {
		this.beanTransferSchema = beanTransferSchema;
	}

}
