package com.kranon.singleton;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.plantillanotifications.BeanConfiguracionNotifications;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanConfiguracionNotifications CACHEADA a 120
 * segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonConfiguracionNotifications {

	private static SingletonConfiguracionNotifications singletonConfiguracionNotifications;

	private BeanConfiguracionNotifications beanConfiguracionNotifications;

	private LoadingCache<String, BeanConfiguracionNotifications> cache;

	private CommonLoggerProcess log;

	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonConfiguracionNotifications() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonConfiguracionNotifications getInstance(CommonLoggerProcess log) throws Exception {

		if (singletonConfiguracionNotifications == null) {
			// si el patron es null, lo creo
			/** INICIO EVENTO - COMENTARIO **/
			log.comment("SINGLETON_CONFIG_NOTIF|NUEVA_INSTANCIA");
			/** FIN EVENTO - COMENTARIO **/

			singletonConfiguracionNotifications = new SingletonConfiguracionNotifications();
			singletonConfiguracionNotifications.log = log;
			singletonConfiguracionNotifications.cache = CacheBuilder.newBuilder()
			// expira pasados XX segundos, independientemente de cuantas
			// veces se acceda
					.expireAfterWrite(singletonConfiguracionNotifications.tiempoSegCacheo, TimeUnit.SECONDS).build(new CacheLoader<String, BeanConfiguracionNotifications>() {
						@Override
						/**
						 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanConfiguracionNotifications y se vuelve a cargar
						 * key es vacio ya que solo vamos a tener un elemento
						 */
						public BeanConfiguracionNotifications load(String key) throws Exception {
							/** INICIO EVENTO - COMENTARIO **/
							singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|LOAD_CACHE|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							/** INICIO EVENTO - COMENTARIO **/
							singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|NEW_INSTANCE_MAP_WEB_SERVICES");
							/** FIN EVENTO - COMENTARIO **/

							singletonConfiguracionNotifications.setBeanConfiguracionNotifications(new BeanConfiguracionNotifications());

							/** INICIO EVENTO - COMENTARIO **/
							singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|GET_BEAN_WEB_SERVICES|codServIvr=" + key);
							/** FIN EVENTO - COMENTARIO **/

							try {
								// Recupero el BeanConfiguracionNotifications con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonConfiguracionNotifications.log.getIdInvocacion(), singletonConfiguracionNotifications.log
										.getIdElemento());
								BeanConfiguracionNotifications beanConfiguracionNotifications = unmarshallProcess.unmarshallConfiguracionNotifications(key);
								
								singletonConfiguracionNotifications.setBeanConfiguracionNotifications(beanConfiguracionNotifications);
								
								if(beanConfiguracionNotifications == null){
									throw new Exception("SINGLETON_CONFIG_NOTIF|KEY_NOT_FOUND|codServIvr=" + key);
								}

								singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|GET_WEB_SERVICES|OK");
								/** INICIO EVENTO - COMENTARIO **/
//								singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|GET_WEB_SERVICES="
//										+ singletonConfiguracionNotifications.getBeanConfiguracionNotifications().toString() + "|OK");
								/** FIN EVENTO - COMENTARIO **/
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								singletonConfiguracionNotifications.log.error(e.toString(), "SINGLETON_CONFIG_NOTIF", null, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}

							return singletonConfiguracionNotifications.getBeanConfiguracionNotifications();
						}
					});
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_CONFIG_NOTIF|EXISTE_INSTANCIA");
			singletonConfiguracionNotifications.log = log;
		}

		return singletonConfiguracionNotifications;
	}

	/**
	 * Dado un codigo de servicio y un nombre de controlador, recupera el
	 * BeanConfiguracionNotifications asociado de la cache o vuelve a leer del XML para
	 * actualizar la instancia
	 * 
	 * @param codServ
	 * @param nombreControlador
	 * @return {@link BeanConfiguracionNotifications}
	 */
	public BeanConfiguracionNotifications getWebServices() {
		BeanConfiguracionNotifications bean = null;
		try {
			/** INICIO EVENTO - COMENTARIO **/
			singletonConfiguracionNotifications.log.comment("SINGLETON_CONFIG_NOTIF|LOAD_CACHE");
			/** FIN EVENTO - COMENTARIO **/

			bean = singletonConfiguracionNotifications.cache.get("");

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			singletonConfiguracionNotifications.log.error(e.toString(), "GET_WEB_SERVICES_CACHE", null, e);
			/** FIN EVENTO - ERROR **/
		}

		return bean;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private BeanConfiguracionNotifications getBeanConfiguracionNotifications() {
		return beanConfiguracionNotifications;
	}

	private void setBeanConfiguracionNotifications(BeanConfiguracionNotifications beanConfiguracionNotifications) {
		this.beanConfiguracionNotifications = beanConfiguracionNotifications;
	}

}
