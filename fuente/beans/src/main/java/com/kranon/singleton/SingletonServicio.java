package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanServicio CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonServicio {

	private static SingletonServicio singletonServicio;

	private HashMap<String, BeanServicio> mapServicios;
	
	private LoadingCache<String, HashMap<String, BeanServicio>> cache;
	
	private CommonLoggerService log;
	
	private final int tiempoSegCacheo = 120;

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonServicio() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonServicio getInstance(CommonLoggerService log) throws Exception {

		if (singletonServicio == null) {
			// si el patron es null, lo creo
			log.comment("SINGLETON_SERVICIO|NUEVA_INSTANCIA");
			singletonServicio = new SingletonServicio();
			singletonServicio.log = log;
			singletonServicio.cache = CacheBuilder.newBuilder()
					// expira pasados XX segundos, independientemente de cuantas veces se acceda
			        .expireAfterWrite(singletonServicio.tiempoSegCacheo, TimeUnit.SECONDS)
			        .build(new CacheLoader<String, HashMap<String, BeanServicio>>() {
			            @Override
			            /**
			             * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanServicio y se vuelve a cargar
			             * <key> es el codigo del servicio a recuperar
			             */
			            public HashMap<String, BeanServicio> load(String key) throws Exception {
			            	
			            	singletonServicio.log.comment("SINGLETON_SERVICIO|LOAD_CACHE|codServIvr=" + key);
							if (singletonServicio.getMapServicios() == null) {
								// si la tabla es null, la creo
								singletonServicio.log.comment("SINGLETON_SERVICIO|NEW_INSTANCE_MAP_SERVICIO");
								singletonServicio.setMapServicios(new HashMap<String, BeanServicio>());
							}
							// si el mapa contiene el servicio, lo elimino
							if (singletonServicio.getMapServicios().containsKey(key)) {
								singletonServicio.log.comment("SINGLETON_SERVICIO|DELETE_BEAN_SERVICIO|codServIvr=" + key);
								singletonServicio.getMapServicios().remove(key);
							} else {
								singletonServicio.log.comment("SINGLETON_SERVICIO|KEY_NOT_FOUND|codServIvr=" + key);
							}
						
							singletonServicio.log.comment("SINGLETON_SERVICIO|GET_BEAN_SERVICIO|codServIvr=" + key);
	
							try {
								// Recupero el BeanServicio con ese codigo
								Unmarshall unmarshallProcess = new Unmarshall(singletonServicio.log.getIdLlamada(), singletonServicio.log.getIdElemento());
								BeanServicio beanServicio = unmarshallProcess.unmarshallServicio(key);
										
								singletonServicio.getMapServicios().put(key, beanServicio);
								
								singletonServicio.log.comment("SINGLETON_SERVICIO|GET_SERVICIO|OK");
//								singletonServicio.log.comment("SINGLETON_SERVICIO|GET_SERVICIO=" + singletonServicio.getMapServicios().get(key).toString() + "|OK");
							} catch (Exception e) {
								/** INICIO EVENTO - ERROR **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("codServIvr", key));
								singletonServicio.log.error(e.toString(), "SINGLETON_SERVICIO", parametrosAdicionales, e);
								/** FIN EVENTO - ERROR **/
								
								return null;
							}
							

			                return singletonServicio.getMapServicios();
			            }
			        });		
		} else {
			// ya hay una instancia del singleton
			log.comment("SINGLETON_SERVICIO|EXISTE_INSTANCIA");
			singletonServicio.log = log;
		}
				
		return singletonServicio;
	}
	
	/**
	 * Dado un codigo de servicio, recupera el BeanServicio asociado de la cache 
	 * o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param {@link String} codServicio
	 * @return {@link BeanServicio}
	 */
	public BeanServicio getServicio(String cod) {
		BeanServicio serv = null;
		try {
			singletonServicio.log.comment("SINGLETON_SERVICIO|LOAD_CACHE|codServIvr=" + cod);
			
//			serv = singletonServicio.cache.get(cod).get(cod);
			HashMap<String, BeanServicio> map = singletonServicio.cache.get(cod);
			if(map == null){
				return null;
			} else {
				serv = map.get(cod);
			}
		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codServicio", cod));
			singletonServicio.log.error(e.toString(), "SINGLETON_SERVICIO", parametrosAdicionales, e);
		}
		
		return serv;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<String, BeanServicio> getMapServicios() {
	
		return mapServicios;
	}

	private void setMapServicios(HashMap<String, BeanServicio> mapServicios) {
		this.mapServicios = mapServicios;
	}

}
