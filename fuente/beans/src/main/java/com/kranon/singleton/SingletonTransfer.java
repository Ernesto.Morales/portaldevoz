package com.kranon.singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.kranon.bean.transfer.BeanTransfer;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.unmarshall.Unmarshall;

/**
 * Clase para que solo haya una instancia el BeanTransfer CACHEADA a 120 segundos
 * 
 * @author abalfaro
 *
 */
public class SingletonTransfer {

	private static SingletonTransfer singletonTransfer;

	private HashMap<KeysTransfer, BeanTransfer> mapTransfer;

	private LoadingCache<KeysTransfer, HashMap<KeysTransfer, BeanTransfer>> cache;

	private CommonLoggerService log;

	private final int tiempoSegCacheo = 120;

	// Clase para recibir la <clave> de busqueda
	public class KeysTransfer {
		public String idServicio;
		public String segmento;

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public boolean equals(Object obj) {
			if (obj instanceof KeysTransfer) {
				KeysTransfer other = (KeysTransfer) obj;
				EqualsBuilder builder = new EqualsBuilder();
				builder.append(this.idServicio, other.idServicio);
				builder.append(this.segmento, other.segmento);
				return builder.isEquals();
			}
			return false;
		}

		/**
		 * ES OBLIGATORIO REDEFINIR, uso de libreria apache-commons-lang
		 */
		public int hashCode() {
			HashCodeBuilder builder = new HashCodeBuilder();
			builder.append(idServicio);
			builder.append(segmento);
			return builder.toHashCode();
		}
	}

	/**
	 * Se privatiza el metodo para evitar instancias no deseadas.
	 */
	private SingletonTransfer() {

	}

	/**
	 * Obtiene una instancia del recurso y se define la cache
	 *
	 * @return
	 * @throws Exception
	 */
	public static synchronized SingletonTransfer getInstance(CommonLoggerService log) throws Exception {

		try {
			if (singletonTransfer == null) {
				// si el patron es null, lo creo
				log.comment("SINGLETON_TRANSFER|NUEVA_INSTANCIA");
				singletonTransfer = new SingletonTransfer();
				singletonTransfer.log = log;
				singletonTransfer.cache = CacheBuilder
						.newBuilder()
						// expira pasados XX segundos, independientemente de cuantas
						// veces se acceda
						.expireAfterWrite(singletonTransfer.tiempoSegCacheo, TimeUnit.SECONDS)
						.build(new CacheLoader<KeysTransfer, HashMap<KeysTransfer, BeanTransfer>>() {

							@Override
							/**
							 * Si han pasado <tiempoSegCacheo> segundos se elimina el BeanTransfer y se vuelve a cargar
							 * key es un objeto de tipo KeysTransfer con el codigo del servicio y nombre de segmento a recuperar
							 */
							public HashMap<KeysTransfer, BeanTransfer> load(KeysTransfer key) throws Exception {

								singletonTransfer.log.comment("SINGLETON_TRANSFER|LOAD_CACHE|idServicio=" + key.idServicio + "|segmento="
										+ key.segmento);
								if (singletonTransfer.getMapTransfer() == null) {
									// si la tabla es null, la creo
									singletonTransfer.log.comment("SINGLETON_TRANSFER|NEW_INSTANCE_MAP_TRANSFER");
									singletonTransfer.setMapTransfer(new HashMap<KeysTransfer, BeanTransfer>());
								}
								// si el mapa contiene el controlador, lo elimino
								if (singletonTransfer.getMapTransfer().containsKey(key)) {
									singletonTransfer.log.comment("SINGLETON_TRANSFER|DELETE_BEAN_TRANSFER|idServicio=" + key.idServicio
											+ "|segmento=" + key.segmento);
									singletonTransfer.getMapTransfer().remove(key);
								} else {
									singletonTransfer.log.comment("SINGLETON_TRANSFER|KEY_NOT_FOUND|idServicio=" + key.idServicio + "|segmento="
											+ key.segmento);
								}

								singletonTransfer.log.comment("SINGLETON_TRANSFER|GET_BEAN_TRANSFER|idServicio=" + key.idServicio + "|segmento="
										+ key.segmento);

								try {
									// Recupero el BeanControlador con ese codigo
									Unmarshall unmarshallProcess = new Unmarshall(singletonTransfer.log.getIdLlamada(), singletonTransfer.log
											.getIdElemento());
									BeanTransfer beanTransfer = unmarshallProcess.unmarshallTransfer(key.idServicio + "_" + key.segmento);

									singletonTransfer.getMapTransfer().put(key, beanTransfer);

									singletonTransfer.log.comment("SINGLETON_TRANSFER|GET_TRANSFER|OK");
//									singletonTransfer.log.comment("SINGLETON_TRANSFER|GET_TRANSFER="
//											+ singletonTransfer.getMapTransfer().get(key).toString() + "|OK");
								} catch (Exception e) {
									/** INICIO EVENTO - ERROR **/
									ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
									parametrosAdicionales.add(new ParamEvent("idServicio", key.idServicio));
									parametrosAdicionales.add(new ParamEvent("segmento", key.segmento));
									singletonTransfer.log.error(e.toString(), "SINGLETON_TRANSFER", parametrosAdicionales, e);
									/** FIN EVENTO - ERROR **/

									return null;
								}

								return singletonTransfer.getMapTransfer();
							}

						});
			} else {
				// ya hay una instancia del singleton
				log.comment("SINGLETON_TRANSFER|EXISTE_INSTANCIA");
				singletonTransfer.log = log;
			}

			return singletonTransfer;
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			singletonTransfer.log.error(e.toString(), "SINGLETON_TRANSFER", null, e);
			/** FIN EVENTO - ERROR **/

			return null;
		}
	}

	/**
	 * Dado un idServicio y un segmento, recupera el BeanTransfer asociado de la cache o vuelve a leer del XML para actualizar la instancia
	 * 
	 * @param idServicio
	 * @param segmento
	 * @return {@link BeanTransfer}
	 */
	public BeanTransfer getTransfer(String idServicio, String segmento) {
		BeanTransfer transfer = null;
		try {
			singletonTransfer.log.comment("SINGLETON_TRANSFER|LOAD_CACHE|idServicio=" + idServicio + "|segmento= " + segmento);
			SingletonTransfer.KeysTransfer key = new KeysTransfer();
			key.idServicio = idServicio;
			key.segmento = segmento;

			HashMap<KeysTransfer, BeanTransfer> map = singletonTransfer.cache.get(key);
			if (map == null) {
				return null;
			} else {
				transfer = map.get(key);
			}

		} catch (Exception e) {
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("idServicio", idServicio));
			parametrosAdicionales.add(new ParamEvent("segmento", segmento));
			singletonTransfer.log.error(e.toString(), "SINGLETON_TRANSFER", parametrosAdicionales, e);
		}

		return transfer;
	}

	/**
	 * Se privatiza para evitar llamadas externas
	 * 
	 */
	private HashMap<KeysTransfer, BeanTransfer> getMapTransfer() {
		return mapTransfer;
	}

	private void setMapTransfer(HashMap<KeysTransfer, BeanTransfer> mapTransfer) {
		this.mapTransfer = mapTransfer;
	}

}
