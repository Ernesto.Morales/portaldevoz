package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define la alarma de CFG
 * @author abalfaro
 *
 */
public class CfgAlarm extends MonitoringAlarm implements AlarmInterface {

	private static final String nombreAlarma = "CFG";

	private String idServicio;
	private String idModulo;
	private String idArchivo;
	private String rutaCompleta;
	private String descripcionError;
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	public CfgAlarm(String idServicio, String idModulo, String idArchivo, String rutaCompleta, String descripcionError,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(CfgAlarm.nombreAlarma);

		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.idArchivo = idArchivo;
		this.rutaCompleta = rutaCompleta;
		this.descripcionError = descripcionError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.idArchivo == null) ? "" : ("|" + this.idArchivo))
						  + ((this.rutaCompleta == null) ? "" : ("|" + this.rutaCompleta))
						  + ((this.descripcionError == null) ? "" : ("|" + this.descripcionError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
