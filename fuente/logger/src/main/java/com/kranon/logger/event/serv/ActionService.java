package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;


/**
 * Clase que define el evento de ACTION para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class ActionService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "ACTION";

	private String idAccion;
	private String resultado;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public ActionService(String idLlamada, String idServicio, String idElemento, 
			String idAccion, String resultado, 
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idLlamada, idServicio, idElemento, ActionService.nombreEvento);

		this.idAccion = idAccion;
		this.resultado = resultado;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idAccion 
				+ ((this.resultado == null) ? "" : ("|" + this.resultado))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
