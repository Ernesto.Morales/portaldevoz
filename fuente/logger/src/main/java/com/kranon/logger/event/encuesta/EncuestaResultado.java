package com.kranon.logger.event.encuesta;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;



/**
 * Clase que define el evento de RESULTADO ENCUESTA
 * @author aarce
 *
 */
public class EncuestaResultado extends EncuestaEvent implements EncuestaInterface {

	private static final String nombreEvento = "RESULTADO_ENCUESTA";

	private String descripcionEncuesta;
	private String numTarjeta;
	private String idAsesor;
	private String encuestaCompleta;
	private ArrayList<ParamEvent> resultados;

	public EncuestaResultado(String idLlamada, String idServicio, String idElemento, String descripcionEncuesta, String numTarjeta, String idAsesor, String encuestaCompleta, ArrayList<ParamEvent> resultados) {
		super(idLlamada, idServicio, idElemento, EncuestaResultado.nombreEvento);

		this.descripcionEncuesta = descripcionEncuesta;
		this.numTarjeta = numTarjeta;
		this.idAsesor = idAsesor;
		this.encuestaCompleta = encuestaCompleta;
		this.resultados = resultados;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message 
				+ ((this.descripcionEncuesta == null) ? "|" : ("|" + this.descripcionEncuesta))
				+ ((this.numTarjeta == null) ? "|" : ("|" + this.numTarjeta))
				+ ((this.idAsesor == null) ? "|" : ("|" + this.idAsesor))
				+ ((this.encuestaCompleta == null) ? "|" : ("|" + this.encuestaCompleta))
				+ ((this.resultados == null) ? "" : ("|" + this.resultados.toString()));
	
		return message;
	}

}
