package com.kranon.logger.monit;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

public interface LoggerMonitoringInterface {

	/** ASR **/
	public void alarmASR(String idServicio, String idModulo, String idError, String idMenu, ArrayList<ParamEvent> parametrosAdicionales);

	/** TTS **/
	public void alarmTTS(String idServicio, String idModulo, String idError, ArrayList<ParamEvent> parametrosAdicionales);

	/** WS **/
	public void alarmWS(String idServicio, String idModulo, String idWebService, String url, String tipoError, String codigoError,
			String descripcionError, ArrayList<ParamEvent> parametrosAdicionales);

	/** DEFAULT **/
	public void alarmDefault(String idAlarma, String idServicio, String idModulo, String idError, ArrayList<ParamEvent> parametrosAdicionales);
}
