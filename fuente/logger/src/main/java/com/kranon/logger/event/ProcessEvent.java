package com.kranon.logger.event;

/**
 * Clase PADRE comun a todos los EVENTOS de los PROCESOS
 * 
 * @author abalfaro
 *
 */
public abstract class ProcessEvent {

	// id de invocacion asociado al proceso con formato: yyyyMMddHHmmssSSS
	private String idInvocacion;
	// id del proceso
	private String idElemento;
	// tipo de evento: INIT_CALL, END_CALL, INIT_MODULE, END_MODULE, SPEECH,
	// DIALOGUE, ACTION, SIST_EXTERN, STATISTICS, ERROR, COMMENT
	private String tipoEvento;

	public ProcessEvent(String idInvocacion, String idElemento, String tipoEvento) {
		this.idInvocacion = idInvocacion;
		this.idElemento = idElemento;
		this.tipoEvento = tipoEvento;
	}

	public String getHeader() {
		String message = "";

		message = message + this.idInvocacion + "|" + this.idElemento + "|" + this.tipoEvento + "|";

		return message;
	}

}
