package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define la alarma de ASR
 * @author abalfaro
 *
 */
public class AsrAlarm extends MonitoringAlarm implements AlarmInterface {

	private static final String nombreAlarma = "ASR";
	private String idInvocacion;
	private String idServicio;
	private String idModulo;
	private String idError;
	private String idMenu;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public AsrAlarm(String idInvocacion, String idServicio, String idModulo, String idError, String idMenu,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(/*servidor, componente, */AsrAlarm.nombreAlarma);
		this.idInvocacion = idInvocacion;
		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.idError = idError;
		this.idMenu = idMenu;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero anado la cabecera
		String message = this.getHeader();

		// anado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.idError == null) ? "" : ("|" + this.idError))
						  + ((this.idMenu == null) ? "" : ("|" + this.idMenu))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()))
						  + ((this.idInvocacion == null) ? "" : ("|" + this.idInvocacion));

		return message;
	}
}
