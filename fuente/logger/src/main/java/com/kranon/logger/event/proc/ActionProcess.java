package com.kranon.logger.event.proc;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ProcessEvent;

/**
 * Clase que define el evento de ACTION para un PROCESO
 * @author abalfaro
 *
 */
public class ActionProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "ACTION";

	private String idAccion;
	private String resultado;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public ActionProcess(String idInvocacion, String idElemento, String idAccion, String resultado, 
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idInvocacion, idElemento, ActionProcess.nombreEvento);

		this.idAccion = idAccion;
		this.resultado = resultado;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idAccion 
				+ ((this.resultado == null) ? "" : ("|" + this.resultado))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}
}
