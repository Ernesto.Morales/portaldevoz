package com.kranon.logger.serv;

import java.util.ArrayList;

import com.kranon.logger.CommonLogger;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.serv.ActionService;
import com.kranon.logger.event.serv.CommentService;
import com.kranon.logger.event.serv.DialogueService;
import com.kranon.logger.event.serv.EndCall;
import com.kranon.logger.event.serv.EndModuleService;
import com.kranon.logger.event.serv.ErrorService;
import com.kranon.logger.event.serv.InitCall;
import com.kranon.logger.event.serv.InitModuleService;
import com.kranon.logger.event.serv.SistExternService;
import com.kranon.logger.event.serv.SpeechService;
import com.kranon.logger.event.serv.StatisticsService;

public class CommonLoggerService extends CommonLogger implements LoggerServiceInterface {

	// propio del log de servicios IVR
	private String idServicio;

	public CommonLoggerService(String logFile) {
		super(logFile);
	}

	/**
	 * Inicializa la informacion del LOGGER
	 * 
	 * @param String
	 *            idLlamada
	 * @param String
	 *            idServicio
	 * @param String
	 *            idElemento
	 */
	public void inicializar(String idLlamada, String idServicio, String idElemento) {
		super.inicializar(idLlamada, idElemento);
		this.idServicio = idServicio;
	}

	/************ GETTERS **************/
	public String getIdLlamada() {
		// return idLlamada;
		return idInvocacion;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public String getIdElemento() {
		return idElemento;
	}

	/**************************/
	/**************************/
	/**************************/
	/**
	 * Crea la cabecera de la traza con la siguiente info del punto donde se ha mandado escribir la traza, con formato: [clase.metodo:linea]
	 * 
	 * @return {@link String}
	 */
	private String getInfoLogger() {
		int nivel = 4;
		String className = Thread.currentThread().getStackTrace()[nivel].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[nivel].getMethodName();
		int lineNumber = Thread.currentThread().getStackTrace()[nivel].getLineNumber();

		return "[" + className + "." + methodName + ":" + lineNumber + "] - ";
	}
	

	/**
	 * Escribe una traza de Inicio de Llamada con severidad INFO
	 * 
	 * @param {@link String} numOrigen
	 * @param {@link String} numDestino
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void initCall(String numOrigen, String numDestino, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			InitCall initCallEvent = new InitCall(idInvocacion, idServicio, idElemento, numOrigen, numDestino, parametrosAdicionales);
			String traza = this.getInfoLogger() + initCallEvent.getMessage();

			this.log.info(traza);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Fin de Llamada con severidad INFO
	 * 
	 * @param {@link String} numOrigen
	 * @param {@link String} numDestino
	 * @param {@link String} codigoRetorno
	 * @param {@link String} duracion
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void endCall(String numOrigen, String numDestino, String codigoRetorno, String duracion, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			EndCall endCallEvent = new EndCall(idInvocacion, idServicio, idElemento, numOrigen, numDestino, codigoRetorno, duracion,
					parametrosAdicionales);
			String traza = this.getInfoLogger() + endCallEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Inicio de Modulo con severidad INFO
	 * 
	 * @param {@link String} idModulo
	 * @param {@link ArrayList<ParamEvent>} parametrosEntrada
	 */
	public void initModuleService(String idModulo, ArrayList<ParamEvent> parametrosEntrada) {
		try {
			InitModuleService initModuleEvent = new InitModuleService(idInvocacion, idServicio, idElemento, idModulo, parametrosEntrada);
			String traza = this.getInfoLogger() + initModuleEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Fin de Modulo con severidad INFO
	 * 
	 * @param {@link String} idModulo
	 * @param {@link String} codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosSalida
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void endModuleService(String idModulo, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			EndModuleService endModuleEvent = new EndModuleService(idInvocacion, idServicio, idElemento, idModulo, codigoRetorno, parametrosSalida,
					parametrosAdicionales);
			String traza = this.getInfoLogger() + endModuleEvent.getMessage();

			this.log.info(traza);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Locucion con severidad INFO
	 * 
	 * @param {@link String} tipoLocucion
	 * @param {@link String} locucion
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void speechEvent(String tipoLocucion, String locucion, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			SpeechService speechEvent = new SpeechService(idInvocacion, idServicio, idElemento, tipoLocucion, locucion, parametrosAdicionales);
			String traza = this.getInfoLogger() + speechEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Menu con severidad INFO
	 * 
	 * @param {@link String} idMenu
	 * @param {@link String} recDisponible
	 * @param {@link String} recUtilizado
	 * @param int numIntento
	 * @param {@link String} respuesta
	 * @param {@link String} codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void dialogueEvent(String idMenu, String recDisponible, String recUtilizado, int numIntento, String respuesta, String codigoRetorno,
			ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			DialogueService dialogueEvent = new DialogueService(idInvocacion, idServicio, idElemento, idMenu, recDisponible, recUtilizado,
					numIntento, respuesta, codigoRetorno, parametrosAdicionales);
			String traza = this.getInfoLogger() + dialogueEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Accion con severidad INFO
	 * 
	 * @param {@link String} idAccion
	 * @param {@link String} resultado
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void actionEvent(String idAccion, String resultado, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			ActionService actionEvent = new ActionService(idInvocacion, idServicio, idElemento, idAccion, resultado, parametrosAdicionales);
			String traza = this.getInfoLogger() + actionEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Sistema Externo con severidad INFO
	 * 
	 * @param {@link String} idSistemaExterno
	 * @param {@link String} rutaSistema
	 * @param {@link ArrayList<ParamEvent>} parametrosEntrada
	 * @param {@link ArrayList<ParamEvent>} parametrosSalida
	 * @param {@link String} codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void sistExternEvent(String idSistemaExterno, String rutaSistema, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosSalida, String codigoRetorno, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			SistExternService sistExternEvent = new SistExternService(idInvocacion, idServicio, idElemento, idSistemaExterno, rutaSistema,
					parametrosEntrada, parametrosSalida, codigoRetorno, parametrosAdicionales);
			String traza = this.getInfoLogger() + sistExternEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Estadistica con severidad INFO
	 * 
	 * @param {@link String} idEstadistica
	 * @param {@link String} resultado
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void statisticEvent(String idEstadistica, String resultado, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			StatisticsService statisticEvent = new StatisticsService(idInvocacion, idServicio, idElemento, idEstadistica, resultado,
					parametrosAdicionales);
			String traza = this.getInfoLogger() + statisticEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Error con severidad ERROR
	 * 
	 * @param {@link String} idError
	 * @param {@link String} localizacion
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 * @param {@link Exception} exception
	 */
	public void error(String idError, String localizacion, ArrayList<ParamEvent> parametrosAdicionales, Exception exception) {
		try {
			ErrorService errorEvent = new ErrorService(idInvocacion, idServicio, idElemento, idError, localizacion, parametrosAdicionales, exception);
			String traza = this.getInfoLogger() + errorEvent.getMessage();

			this.log.error(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Comentario con severidad DEBUG
	 * 
	 * @param {@link String} texto
	 */
	public void comment(String texto) {
		try {
			CommentService commentEvent = new CommentService(idInvocacion, idServicio, idElemento, texto);
			String traza = this.getInfoLogger() + commentEvent.getMessage();

			this.log.debug(traza);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
