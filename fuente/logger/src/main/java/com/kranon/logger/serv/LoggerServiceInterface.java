package com.kranon.logger.serv;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

public interface LoggerServiceInterface {

	/** CALL **/
	public void initCall(String numOrigen, String numDestino, ArrayList<ParamEvent> parametrosAdicionales);

	public void endCall(String numOrigen, String numDestino, String codigoRetorno, String duracion, ArrayList<ParamEvent> parametrosAdicionales);

	/** MODULE **/
	public void initModuleService(String idModulo, ArrayList<ParamEvent> parametrosEntrada);

	public void endModuleService(String idModulo, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales);

	/** SPEECH **/
	public void speechEvent(String tipoLocucion, String locucion, ArrayList<ParamEvent> parametrosAdicionales);

	/** DIALOGUE **/
	public void dialogueEvent(String idMenu, String recDisponible, String recUtilizado, int numIntento, String respuesta, String codigoRetorno,
			ArrayList<ParamEvent> parametrosAdicionales);

	/** ACTION **/
	public void actionEvent(String idAccion, String resultado, ArrayList<ParamEvent> parametrosAdicionales);

	/** SIST_EXTERN **/
	public void sistExternEvent(String idSistemaExterno, String rutaSistema, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosSalida, String codigoRetorno, ArrayList<ParamEvent> parametrosAdicionales);

	/** STATISTICS **/
	public void statisticEvent(String idEstadistica, String resultado, ArrayList<ParamEvent> parametrosAdicionales);

	/** ERROR **/
	public void error(String idError, String localizacion, ArrayList<ParamEvent> parametrosAdicionales, Exception e);

	/** COMMENT **/
	public void comment(String texto);
}
