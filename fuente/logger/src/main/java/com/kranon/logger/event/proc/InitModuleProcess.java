package com.kranon.logger.event.proc;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ProcessEvent;

/**
 * Clase que define el evento de INICIO de MoDULO para un PROCESO
 * @author abalfaro
 *
 */
public class InitModuleProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "INIT_MODULE";

	private String idModulo;

	private ArrayList<ParamEvent> parametrosEntrada;

	public InitModuleProcess(String idInvocacion, String idElemento, String idModulo, ArrayList<ParamEvent> parametrosEntrada) {
		super(idInvocacion, idElemento, InitModuleProcess.nombreEvento);

		this.idModulo = idModulo;
		this.parametrosEntrada = parametrosEntrada;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idModulo 
				+ ((this.parametrosEntrada == null) ? "" : ("|IN=" + this.parametrosEntrada.toString()));

		return message;
	}

}
