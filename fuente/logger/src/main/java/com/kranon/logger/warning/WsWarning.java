package com.kranon.logger.warning;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.logger.utilidades.UtilidadesLogger;

/**
 * Clase que define el WARNING WS (v2)
 * @author abalfaro
 *
 */
public class WsWarning extends MonitoringWarning implements WarningInterface {

	private static final String nombreAlarma = "ASO";

	private String idInvocacion;
	private String idServicio;
	private String versionWS;
	private String nombreWS;
	private String codError;
	private String tipoError;
	private String descError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public WsWarning(String idInvocacion, String idServicio, String versionWS, String nombreWS, String codError,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(WsWarning.nombreAlarma);
		
		this.idInvocacion = idInvocacion;

		this.idServicio = UtilidadesLogger.cambiarNombreAplicacion(idServicio);				
		
		this.versionWS = (versionWS.length() > Constantes.LONG_MAX_VERSION_WS) ? 
				(versionWS.substring(0, Constantes.LONG_MAX_VERSION_WS)) : (versionWS);
				
		this.nombreWS = (nombreWS.length() > Constantes.LONG_MAX_NOMBRE_WS) ? 
				(nombreWS.substring(0, Constantes.LONG_MAX_NOMBRE_WS)) : (nombreWS);
				
		this.codError = (codError.length() > Constantes.LONG_MAX_COD_ERROR) ? 
				(codError.substring(0, Constantes.LONG_MAX_COD_ERROR)) : (codError);
				
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public WsWarning(String idInvocacion, String idServicio, String versionWS, String nombreWS, String codError, String tipoError, String descError,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(WsWarning.nombreAlarma);
		
		this.idInvocacion = idInvocacion;

		this.idServicio = UtilidadesLogger.cambiarNombreAplicacion(idServicio);				
		
		this.versionWS = (versionWS.length() > Constantes.LONG_MAX_VERSION_WS) ? 
				(versionWS.substring(0, Constantes.LONG_MAX_VERSION_WS)) : (versionWS);
				
		this.nombreWS = (nombreWS.length() > Constantes.LONG_MAX_NOMBRE_WS) ? 
				(nombreWS.substring(0, Constantes.LONG_MAX_NOMBRE_WS)) : (nombreWS);
				
		this.codError = (codError.length() > Constantes.LONG_MAX_COD_ERROR) ? 
				(codError.substring(0, Constantes.LONG_MAX_COD_ERROR)) : (codError);
		
		this.tipoError = tipoError;
		
		this.descError = descError;
		
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
				+ ((this.versionWS == null) ? ("|" + "") : ("|" + this.versionWS))
				+ ((this.nombreWS == null) ? ("|" + "") : ("|" + this.nombreWS))
				+ ((this.codError == null) ? ("|" + "") : ("|" + this.codError))
				+ ((this.tipoError == null) ? ("|" + "") : ("|" + this.tipoError))
				+ ((this.descError == null) ? ("|" + "") : ("|" + this.descError))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()))
				+ ((this.idInvocacion == null) ? ("|" + "") : ("|" + this.idInvocacion));

		return message;
	}
}
