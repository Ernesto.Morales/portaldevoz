package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de SISTEMA EXTERNO para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class SistExternService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "SIST_EXTERN";

	private String idSistemaExterno;
	private String rutaSistema;
	private ArrayList<ParamEvent> parametrosEntrada;
	private ArrayList<ParamEvent> parametrosSalida;
	private String codigoRetorno;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public SistExternService(String idLlamada, String idServicio, String idElemento, 
			String idSistemaExterno, String rutaSistema,
			ArrayList<ParamEvent> parametrosEntrada, ArrayList<ParamEvent> parametrosSalida, String codigoRetorno,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idLlamada, idServicio, idElemento, SistExternService.nombreEvento);

		this.idSistemaExterno = idSistemaExterno;
		this.rutaSistema = rutaSistema;
		this.parametrosEntrada = parametrosEntrada;
		this.parametrosSalida = parametrosSalida;
		this.codigoRetorno = codigoRetorno;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idSistemaExterno 
				+ ((this.rutaSistema == null) ? "" : ("|" + this.rutaSistema))
				+ ((this.parametrosEntrada == null) ? "" : ("|IN=" + this.parametrosEntrada.toString()))
				+ ((this.parametrosSalida == null) ? "" : ("|OUT=" + this.parametrosSalida.toString()))
				+ ((this.codigoRetorno == null) ? "" : ("|" + this.codigoRetorno))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
