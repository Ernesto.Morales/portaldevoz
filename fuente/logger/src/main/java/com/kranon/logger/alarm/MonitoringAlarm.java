package com.kranon.logger.alarm;

/**
 * Clase PADRE comun a todas las ALARMAS
 * 
 * @author abalfaro
 *
 */
public class MonitoringAlarm {
	// IP del servidor donde se ha generado la alarma
//	private String servidor;
	// Identificador del componente
//	private String componente;
	// tipo de alarma: ASR, TTS, WS, CTI, CFG, WAS
	private String tipoAlarma;

	public MonitoringAlarm(/*String servidor, String componente, */String tipoAlarma) {
//		this.servidor = servidor;
//		this.componente = componente;
		this.tipoAlarma = tipoAlarma;
	}

	public String getHeader() {
		String message = "";

//		message = message + this.servidor + "|" + this.componente + "|" + this.tipoAlarma + "|";
		message = message +  this.tipoAlarma + "|";

		return message;
	}
}
