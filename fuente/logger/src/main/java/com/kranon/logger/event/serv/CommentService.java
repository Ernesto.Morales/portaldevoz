package com.kranon.logger.event.serv;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ServiceEvent;


/**
 * Clase que define el evento de COMENTARIO para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class CommentService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "COMMENT";

	private String texto;

	public CommentService(String idLlamada, String idServicio, String idElemento, String texto) {
		super(idLlamada, idServicio, idElemento, CommentService.nombreEvento);

		this.texto = texto;
	}

	public String getMessage() {
		// primero añado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.texto;
	
		return message;
	}

}
