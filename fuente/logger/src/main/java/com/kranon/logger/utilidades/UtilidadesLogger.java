package com.kranon.logger.utilidades;


public class UtilidadesLogger {

	private static final String PRESTAMOS_IN = "PRESTAMOS";
	private static final String PRESTAMOS_OUT = "PR";
	private static final String LINEABANCOMER_IN = "LINEABANCOMER";
	private static final String LINEABANCOMER_OUT = "LB";
	

	/**
	 * De la cadena mandada por parametro devuelve el codigo de error (40X, 500, etc.) si forma parte de la cadena de entrada
	 * 
	 * @param cadena
	 * @return
	 */
	public static String cambiarNombreAplicacion(String codServ){
		if(codServ == null){
			return null;
		}
		String codServModificado = "";
		
		// Si el servicio IVR es Linea Bancomer: LINEABANCOMERvX --> LBVX
		if(codServ.startsWith(LINEABANCOMER_IN))
			codServModificado = codServ.replaceFirst(LINEABANCOMER_IN, LINEABANCOMER_OUT);
		else {
			// Si el servicio IVR es Prestamos: PRESTAMOSvX --> PRVX
			if(codServ.startsWith(PRESTAMOS_IN))
				codServModificado = codServ.replaceFirst(PRESTAMOS_IN, PRESTAMOS_OUT);
			else
				codServModificado = codServ;
		}
		codServModificado = codServModificado.toUpperCase();
		return codServModificado;
	}
	
	/**
	 * De la cadena mandada por parametro devuelve el codigo de error (40X, 500, etc.) si forma parte de la cadena de entrada
	 * 
	 * @param cadena
	 * @return
	 */
	public static String getCodigoError(String cadena){
		if(cadena == null){
			return null;
		}
		String codigoError = cadena;
		codigoError = "500";
		return codigoError;
	}

	/**
	 * De la cadena mandada por parametro devuelve la descripcion del error, es decir, sin ERROR_EXT ni ERROR_IVR
	 * 
	 * @param cadena
	 * @return
	 */
	public static String getDescripcionError(String cadena){
		if(cadena == null){
			return null;
		}
		String tipoError = cadena;

		// Si la cadena contiene "ERROR_EXT" o "ERROR_IVR", se obtiene solo la descripcion que va entre parentesis 
		if (tipoError.startsWith("ERROR_EXT") || tipoError.startsWith("ERROR_IVR") || tipoError.startsWith("ERR_EXT") || tipoError.startsWith("ERR_IVR")) {
			if ((tipoError.indexOf("(") > 0) && (tipoError.indexOf(")") > 0))
				tipoError = tipoError.substring(tipoError.indexOf("(") + 1, tipoError.indexOf(")"));
		}
		
		return tipoError;
	}
	

}