package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define cualquier tipo de alarma no controlada. Se recoger el tipo como parametro: nombreAlarma
 * @author abalfaro
 *
 */
public class DefaultAlarm extends MonitoringAlarm implements AlarmInterface {

	private String idServicio;
	private String idModulo;
	private String idError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public DefaultAlarm(String nombreAlarma, String idServicio, String idModulo, String idError,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(nombreAlarma);

		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.idError = idError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.idError == null) ? "" : ("|" + this.idError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}
}
