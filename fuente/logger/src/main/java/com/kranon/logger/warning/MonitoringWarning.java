package com.kranon.logger.warning;

/**
 * Clase PADRE comun a todas los WARNING
 * 
 * @author abalfaro
 *
 */
public class MonitoringWarning {

	// tipo de alarma: ASR, TTS, ASO, CTI, CFG, WAS
	private String tipoAlarma;

	public MonitoringWarning(String tipoAlarma) {

		this.tipoAlarma = tipoAlarma;
	}

	public String getHeader() {
		String message = "";
		message = message +  this.tipoAlarma + "|";
		return message;
	}
}
