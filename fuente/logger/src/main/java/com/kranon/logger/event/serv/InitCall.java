package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de INICIO de LLAMADA para un SERVICIO IVR
 * 
 * @author abalfaro
 *
 */
public class InitCall extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "INIT_CALL";

	private String numOrigen;
	private String numDestino;

	private ArrayList<ParamEvent> parametrosAdicionales;

	// public InitCall(String idLlamada, String idServicio, String idElemento,
	// String numOrigen, String numDestino, ArrayList<ParamEvent>
	// parametrosAdicionales) {
	public InitCall(String idLlamada, String idServicio, String idElemento, String numOrigen, String numDestino,
			ArrayList<ParamEvent> parametrosAdicionales) {

		super(idLlamada, idServicio, idElemento, InitCall.nombreEvento);

		this.numOrigen = numOrigen;
		this.numDestino = numDestino;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message  
				+ ((this.numOrigen == null) ? "" : ("|" + this.numOrigen))
				+ ((this.numDestino == null) ? "" : ("|" + this.numDestino))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
