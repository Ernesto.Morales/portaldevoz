package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de FIN de LLAMADA para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class EndCall extends ServiceEvent implements EventInterface {
	
	private static final String nombreEvento = "END_CALL";
	
	private String numOrigen;
	private String numDestino;
	// Valores codigoRetorno = FIN, TRANSFER o CUELGUE
	private String codigoRetorno;
	// duracion de la llamada en segundos
	private String duracion;
	
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	public EndCall(String idLlamada, String idServicio, String idElemento,
			String numOrigen, String numDestino, String codigoRetorno, String duracion,
			ArrayList<ParamEvent> parametrosAdicionales) {
			
		super(idLlamada, idServicio, idElemento, EndCall.nombreEvento);
			
		this.numOrigen = numOrigen;
		this.numDestino = numDestino;
		this.codigoRetorno = codigoRetorno;
		this.duracion = duracion;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.numOrigen
				+ ((this.numDestino==null)?"": ("|" + this.numDestino))
				+ ((this.codigoRetorno==null)?"": ("|" + this.codigoRetorno))
				+ ((this.duracion==null)?"": ("|" + this.duracion))
				+ ((this.parametrosAdicionales==null)?"":("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
