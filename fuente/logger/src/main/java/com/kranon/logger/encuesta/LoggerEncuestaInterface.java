package com.kranon.logger.encuesta;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

public interface LoggerEncuestaInterface {

	/** ENCUESTA **/
	public void resultadoEncuesta(String descripcionEncuesta, String numTarjeta, String idAsesor, String encuestaCompleta, ArrayList<ParamEvent> resultados);
	
}
