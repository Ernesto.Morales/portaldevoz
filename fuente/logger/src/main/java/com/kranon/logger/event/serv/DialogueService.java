package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de MENU para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class DialogueService extends ServiceEvent implements EventInterface {
	
	private static final String nombreEvento = "DIALOGUE";
	
	private String idMenu;
	// reconocimiento disponible: ASR, DTMF o ASRDTMF
	private String recDisponible;
	// reconocimiento utilizado: ASR o DTMF
	private String recUtilizado;
	// numero de intento actual
	private int numIntento;
	// respuesta: <respuesta>, NOMATCH, NOINPUT
	private String respuesta;
	// codigo de retorno: OK, KO o MAXINT
	private String codigoRetorno;
	
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	
	public DialogueService(String idLlamada, String idServicio, String idElemento,
			String idMenu, String recDisponible, String recUtilizado, int numIntento, 
			String respuesta, String codigoRetorno,
			ArrayList<ParamEvent> parametrosAdicionales) {
		
		super(idLlamada, idServicio, idElemento, DialogueService.nombreEvento);
			
		this.idMenu = idMenu;
		this.recDisponible = recDisponible;
		this.recUtilizado = recUtilizado;
		this.numIntento = numIntento;
		this.respuesta = respuesta;
		this.codigoRetorno = codigoRetorno;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idMenu
				+ ((this.recDisponible==null)?"": ("|" + this.recDisponible))
				+ ((this.recUtilizado==null)?"": ("|" + this.recUtilizado))
				+ "|" + this.numIntento
				+ ((this.respuesta==null)?"": ("|" + this.respuesta))
				+ ((this.codigoRetorno==null)?"": ("|" + this.codigoRetorno))
				+ ((this.parametrosAdicionales==null)?"":("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
