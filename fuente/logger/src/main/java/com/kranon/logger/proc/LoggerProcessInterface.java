package com.kranon.logger.proc;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

public interface LoggerProcessInterface {

	/** PROCESS **/
	public void initProcess(String idProceso, String rutaEjecucion, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosAdicionales);

	public void endProcess(String idProceso, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida, ArrayList<ParamEvent> parametrosAdicionales);

	/** MODULE **/
	public void initModuleProcess(String idModulo, ArrayList<ParamEvent> parametrosEntrada);

	public void endModuleProcess(String idModulo, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales);


	/** ACTION **/
	public void actionEvent(String idAccion, String resultado, ArrayList<ParamEvent> parametrosAdicionales);


	/** SIST_EXTERN **/
	public void sistExternEvent(String idSistemaExterno, String rutaSistema, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosSalida, String codigoRetorno, ArrayList<ParamEvent> parametrosAdicionales);


	/** ERROR **/
	public void error(String idError, String localizacion, ArrayList<ParamEvent> parametrosAdicionales, Exception e);


	/** COMMENT **/
	public void comment(String texto);

}
