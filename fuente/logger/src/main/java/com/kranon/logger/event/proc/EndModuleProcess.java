package com.kranon.logger.event.proc;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ProcessEvent;

/**
 * Clase que define el evento de FIN de MoDULO para un PROCESO
 * @author abalfaro
 *
 */
public class EndModuleProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "END_MODULE";

	private String idModulo;
	private String codigoRetorno;
	private ArrayList<ParamEvent> parametrosSalida;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public EndModuleProcess(String idInvocacion, String idElemento, String idModulo, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idInvocacion, idElemento, EndModuleProcess.nombreEvento);

		this.idModulo = idModulo;
		this.codigoRetorno = codigoRetorno;
		this.parametrosSalida = parametrosSalida;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idModulo 
				+ ((this.codigoRetorno == null) ? "" : ("|" + this.codigoRetorno))
				+ ((this.parametrosSalida == null) ? "" : ("|OUT=" + this.parametrosSalida.toString()))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
