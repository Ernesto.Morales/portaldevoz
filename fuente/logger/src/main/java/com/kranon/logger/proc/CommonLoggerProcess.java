package com.kranon.logger.proc;

import java.util.ArrayList;

import com.kranon.logger.CommonLogger;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.proc.ActionProcess;
import com.kranon.logger.event.proc.CommentProcess;
import com.kranon.logger.event.proc.EndModuleProcess;
import com.kranon.logger.event.proc.EndProcess;
import com.kranon.logger.event.proc.ErrorProcess;
import com.kranon.logger.event.proc.InitModuleProcess;
import com.kranon.logger.event.proc.InitProcess;
import com.kranon.logger.event.proc.SistExternProcess;


public class CommonLoggerProcess extends CommonLogger implements LoggerProcessInterface {


	public CommonLoggerProcess(String logFile) {
		super(logFile);
	}

	public String inicializar(String idInvocacion, String idElemento) {
		// si el idInvocacion viene a null, se genera uno con la fecha/dia
		// actual y se devuelve
		// en otro caso se devuelve null
		String idInvocacionGenerado = super.inicializar(idInvocacion, idElemento);
		if (idInvocacionGenerado != null) {
			// he generado un id de invocacion
			/** INICIO EVENTO - COMENTARIO **/
			this.comment("idInvocacionGenerado=" + idInvocacionGenerado);
			/** FIN EVENTO - COMENTARIO **/
		}
		return idInvocacionGenerado;
	}

	/************ GETTERS **************/
	public String getIdInvocacion() {
		return idInvocacion;
	}

	public String getIdElemento() {
		return idElemento;
	}

	/**************************/
	/**************************/
	/**************************/

	/**
	 * Crea la cabecera de la traza con la siguiente info del punto donde se ha
	 * mandado escribir la traza, con formato: [clase.metodo:linea]
	 * 
	 * @return {@link String}
	 */
	private String getInfoLogger() {
		int nivel = 4;
		String className = Thread.currentThread().getStackTrace()[nivel].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[nivel].getMethodName();
		int lineNumber = Thread.currentThread().getStackTrace()[nivel].getLineNumber();

		return "[" + className + "." + methodName + ":" + lineNumber + "] - ";
	}


	/**
	 * Escribe una traza de Inicio de Proceso con severidad INFO
	 * 
	 * @param String
	 *            idProceso
	 * @param String
	 *            rutaEjecucion
	 * @param {@link ArrayList<ParamEvent>} parametrosEntrada
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void initProcess(String idProceso, String rutaEjecucion, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			InitProcess initProcessEvent = new InitProcess(idInvocacion, idElemento, idProceso, rutaEjecucion, parametrosEntrada,
					parametrosAdicionales);
			String traza = this.getInfoLogger() + initProcessEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Fin de Proceso con severidad INFO
	 * 
	 * @param String
	 *            idProceso
	 * @param String
	 *            codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosSalida
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void endProcess(String idProceso, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			EndProcess endProcessEvent = new EndProcess(idInvocacion, idElemento, idProceso, codigoRetorno, parametrosSalida, parametrosAdicionales);
			String traza = this.getInfoLogger() + endProcessEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Inicio de Modulo con severidad INFO
	 * 
	 * @param String
	 *            idModulo
	 * @param {@link ArrayList<ParamEvent>} parametrosEntrada
	 */
	public void initModuleProcess(String idModulo, ArrayList<ParamEvent> parametrosEntrada) {
		try {
			InitModuleProcess initModuleEvent = new InitModuleProcess(idInvocacion, idElemento, idModulo, parametrosEntrada);
			String traza = this.getInfoLogger() + initModuleEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Fin de Modulo con severidad INFO
	 * 
	 * @param String
	 *            idModulo
	 * @param String
	 *            codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosSalida
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void endModuleProcess(String idModulo, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			EndModuleProcess endModuleEvent = new EndModuleProcess(idInvocacion, idElemento, idModulo, codigoRetorno, parametrosSalida,
					parametrosAdicionales);
			String traza = this.getInfoLogger() + endModuleEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Action con severidad INFO
	 * 
	 * @param String
	 *            idAccion
	 * @param String
	 *            resultado
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void actionEvent(String idAccion, String resultado, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			ActionProcess actionEvent = new ActionProcess(idInvocacion, idElemento, idAccion, resultado, parametrosAdicionales);
			String traza = this.getInfoLogger() + actionEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Escribe una traza de Sistema Externo con severidad INFO
	 * 
	 * @param String
	 *            idSistemaExterno
	 * @param String
	 *            rutaSistema
	 * @param {@link ArrayList<ParamEvent>} parametrosEntrada
	 * @param {@link ArrayList<ParamEvent>} parametrosSalida
	 * @param codigoRetorno
	 * @param {@link ArrayList<ParamEvent>} parametrosAdicionales
	 */
	public void sistExternEvent(String idSistemaExterno, String rutaSistema, ArrayList<ParamEvent> parametrosEntrada,
			ArrayList<ParamEvent> parametrosSalida, String codigoRetorno, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			SistExternProcess sistExternEvent = new SistExternProcess(idInvocacion, idElemento, idSistemaExterno, rutaSistema, parametrosEntrada,
					parametrosSalida, codigoRetorno, parametrosAdicionales);
			String traza = this.getInfoLogger() + sistExternEvent.getMessage();

			this.log.info(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Error con severidad ERROR
	 * 
	 * @param {@link String} idError
	 * @param {@link String} localizacion
	 * @param {@link ArrayList}<{@link ParamEvent}> parametrosAdicionales
	 * @param {@link Exception} exception
	 */
	public void error(String idError, String localizacion, ArrayList<ParamEvent> parametrosAdicionales, Exception exception) {
		try {

			ErrorProcess errorEvent = new ErrorProcess(idInvocacion, idElemento, idError, localizacion, parametrosAdicionales, exception);
			String traza = this.getInfoLogger() + errorEvent.getMessage();
			
			this.log.error(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe una traza de Comentario con severidad DEBUG
	 * 
	 * @param String
	 *            texto
	 */
	public void comment(String texto) {
		try {
			CommentProcess commentEvent = new CommentProcess(idInvocacion, idElemento, texto);
			String traza = this.getInfoLogger() + commentEvent.getMessage();
			
			this.log.debug(traza);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
