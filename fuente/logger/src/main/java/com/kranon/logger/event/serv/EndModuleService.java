package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de FIN de MoDULO para un SERVICIO IVR
 * 
 * @author abalfaro
 *
 */
public class EndModuleService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "END_MODULE";

	private String idModulo;
	// Valores codigoRetorno = FIN, TRANSFER, CUELGUE, PREGUNTA_ABIERTA
	private String codigoRetorno;

	private ArrayList<ParamEvent> parametrosSalida;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public EndModuleService(String idLlamada, String idServicio, String idElemento, String idModulo, String codigoRetorno,
			ArrayList<ParamEvent> parametrosSalida, ArrayList<ParamEvent> parametrosAdicionales) {

		super(idLlamada, idServicio, idElemento, EndModuleService.nombreEvento);

		this.idModulo = idModulo;
		this.codigoRetorno = codigoRetorno;
		this.parametrosSalida = parametrosSalida;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message  + this.idModulo  
				+ ((this.codigoRetorno == null) ? "" : ("|" + this.codigoRetorno))
				+ ((this.parametrosSalida == null) ? "" : ("|OUT=" + this.parametrosSalida.toString()))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
