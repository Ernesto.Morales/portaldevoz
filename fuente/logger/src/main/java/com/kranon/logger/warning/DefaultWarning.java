package com.kranon.logger.warning;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.utilidades.UtilidadesLogger;

/**
 * Clase que define cualquier tipo de alarma no controlada. Se recoger el tipo como parametro: nombreAlarma
 * @author abalfaro
 *
 */
public class DefaultWarning extends MonitoringWarning implements WarningInterface {
	private String idInvocacion;
	private String idServicio;
	private String idModulo;
	private String idError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public DefaultWarning(String idInvocacion, String nombreAlarma, String idServicio, String idModulo, String idError,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(nombreAlarma);
		this.idInvocacion = idInvocacion;
		this.idServicio = UtilidadesLogger.cambiarNombreAplicacion(idServicio);	
		this.idModulo = idModulo;
		this.idError = idError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.idError == null) ? "" : ("|" + this.idError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()))
						  + ((this.idInvocacion == null) ? "" : ("|" + this.idInvocacion));

		return message;
	}
}
