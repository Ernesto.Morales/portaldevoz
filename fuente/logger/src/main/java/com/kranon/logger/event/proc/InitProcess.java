package com.kranon.logger.event.proc;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ProcessEvent;

/**
 * Clase que define el evento de INICIO de PROCESO
 * @author abalfaro
 *
 */
public class InitProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "INIT_PROCESS";
	
	private String idProceso;
	private String rutaEjecucion;

	private ArrayList<ParamEvent> parametrosEntrada;
	private ArrayList<ParamEvent> parametrosAdicionales;


	public InitProcess(String idInvocacion, String idElemento, String idProceso, String rutaEjecucion,
			ArrayList<ParamEvent> parametrosEntrada, ArrayList<ParamEvent> parametrosAdicionales) {
		super(idInvocacion, idElemento, InitProcess.nombreEvento);
			
		this.idProceso = idProceso;
		this.rutaEjecucion = rutaEjecucion;
		this.parametrosEntrada = parametrosEntrada;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idProceso
				+ ((this.rutaEjecucion==null)?"": ("|" + this.rutaEjecucion))
				+ ((this.parametrosEntrada==null)?"":("|IN=" + this.parametrosEntrada.toString()))
				+ ((this.parametrosAdicionales==null)?"":("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
