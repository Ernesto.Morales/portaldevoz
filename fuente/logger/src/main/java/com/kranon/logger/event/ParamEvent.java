package com.kranon.logger.event;

import org.apache.commons.lang.StringUtils;

public class ParamEvent {
	private String clave;
	private String valor;

	public ParamEvent(String clave, String valor) {
		this.clave = clave;
		this.valor = valor;
//		this.valor = this.cifrar(valor, 5);
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String toString() {
		return clave + "=" + valor;
	}
	
	public String cifrar(String cadena, int salvarUltimos) {
		if (cadena == null) {
			return null;
		}
		String cadenaCifrada = cadena;

		if (salvarUltimos > cadena.length()) {
			// nos piden salvar mayor cantidad que la que la cadena contiene
			// no asteriscamos nada
			return cadena;
		}
		cadenaCifrada = StringUtils.repeat("*", cadena.length() - salvarUltimos);
		cadenaCifrada = cadenaCifrada + cadena.subSequence(cadena.length() - salvarUltimos, cadena.length());

		return cadenaCifrada;
	}
}
