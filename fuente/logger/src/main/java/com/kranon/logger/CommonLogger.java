package com.kranon.logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class CommonLogger {

	// objeto logger para la escritura de trazas
	 protected Logger log;

	protected String idInvocacion;
	protected String idElemento;

	protected CommonLogger(String logFile) {
		try {
			
			log = Logger.getLogger(logFile.toUpperCase());


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected String inicializar(String idInvocacion, String idElemento) {
		this.idElemento = idElemento;
		if (idInvocacion == null) {
			// genero un nuevo id de invocacion para este proceso
			this.idInvocacion = (new SimpleDateFormat("yyyyMMddHHmmssSSS"))
					.format(new Date());
			return this.idInvocacion;
		} else {
			this.idInvocacion = idInvocacion;
			return null;
		}
	}

}
