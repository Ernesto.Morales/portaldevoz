package com.kranon.logger.enmasc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author planderos
 *
 */
public class Enmascaramiento {

	/**
	 * Construye y devuelve un {@code String} cuyo contenido es la representaci�n textual de la lista
	 * {@code List} que se le env�e como par�metro y cuya informaci�n sensible ha sido enmascarada.
	 * <br>
	 * La idea general de esta funci�n es generar un {@code String} como el que se obtendr�a invocando
	 * al m�todo {@code toString()} sobre el objeto lista proporcionado, lo que, a su vez, es una
	 * invocaci�n recursiva a este m�todo en la jerarqu�a de objetos que definen los elementos de la
	 * lista y que sobreescriben este m�todo. La parada de dicha ejecuci�n recursiva se dar�a hasta
	 * encontrarse con atributos de tipo {@code String} o primitivos.
	 * <br>
	 * Bajo este misma idea {@code toMaskedString()} debe invocar recursivamente al m�todo {@code toMaskedString()}
	 * en aquellos atributos que deban ser enmascarados, a trav�s de la jerarqu�a de clases que definen
	 * los objetos de la lista.
	 * <br>
	 * Esta llamada a {@code toMaskedString()} que se encuentra en la base de la pila de llamadas s�lo
	 * impone la implementaci�n de �ste m�todo en las clases de los elementos de la lista. Es responsabilidad
	 * del desarrollador implementar m�todos de enmascaramiento similares a trav�s de toda la jerarqu�a de
	 * clases que definan los elementos de la lista porque de otro modo s�lo se enmascarar�an los atributos
	 * {@code String} m�s superficiales.
	 * 
	 * @param lista El objeto tipo {@code List} cuyos elementos <strong>deben implementar la interfaz
	 * {@link Enmascarable}</strong>, y son los que se desean representar en un {@code String}.
	 * @return {@code String} con la informaci�n sensible enmascarada, definido por la concatenaci�n de
	 * cada uno de los elementos que componen al {@code List} separados por una coma y un espacio y
	 * envueltos por corchetes, e.g.: {@code [elem0, elem1, elem2]}
	 * @throws ClassCastException Si los elementos que componen la lista que se reciba como par�metro
	 * no implementan la interfaz {@link Enmascarable}, se genera esta excepci�n.
	 */
	public static String list2MaskedStr(List lista){
		StringBuilder maskedStr = new StringBuilder();
		
		maskedStr.append("[");
		
		if(lista != null && !lista.isEmpty()){
			for(int i = 0; i < lista.size(); i++){
				if(lista.get(i) != null){
					maskedStr.append(((Enmascarable) lista.get(i)).toMaskedString());
				}else{
					maskedStr.append("null");
				}
				if(i < (lista.size() - 1)){
					maskedStr.append(", ");					
				}
			}
		}
		maskedStr.append("]");
		
		return maskedStr.toString();
	}
	
	
	/**
	 * Construye y devuelve un {@code String} cuyo contenido es la representaci�n textual de la lista
	 * {@code List} que se le env�e como par�metro y cuya informaci�n ha sido enmascarada TOTALMENTE.
	 * <br>
	 * Este m�todo genera una subcadena por cada elemento de la lista de la siguiente forma:
	 * <ul>
	 * <li>Caso 1: Cualquier elemento que sea {@code null} genera la subcadena: {@code "null"}.</li>
	 * <li>Caso 2: Cualquier elemento que sea la cadena vac�a {@code ""} genera la subcadena vac�a: {@code ""}.</li>
	 * <li>Caso 3: Cualquier otro elemento genera una subcadena con un asterisco: {@code "*"}.</li>
	 * </ul>
	 * @param strLst El objeto tipo {@code List<String>} cuyos elementos son los que se desean representar
	 * en un {@code String}.
	 * @return {@code String} con la informaci�n enmascarada, definido por la concatenaci�n de cada una de
	 * las subcadenas, obtenidas seg�n la descripci�n general de esta funci�n, separados por una coma y un
	 * espacio, y finalmente envuelta por corchetes, e.g.: {@code [null, , *]}
	 */
	public static String strList2MaskedStr(List<String> strLst){
		StringBuilder maskedStr = new StringBuilder();
		
		maskedStr.append("[");
		
		if(strLst != null && !strLst.isEmpty()){
			for(int i = 0; i < strLst.size(); i++){
				if(strLst.get(i) != null){
					if(strLst.get(i).equals("")){						
						maskedStr.append("");
					}else{
						maskedStr.append("*");
					}
				}else{
					maskedStr.append("null");
				}
				if(i < (strLst.size() - 1)){
					maskedStr.append(", ");					
				}
			}
		}
		maskedStr.append("]");
		
		return maskedStr.toString();
	}
	
	/**
	 * Construye y devuelve una representaci&oacute;n en {@code String} de todo
	 * el contenido del <strong>header</strong> que fue <strong>enviado</strong>
	 * al servicio. Si el header contiene un elemento cuya clave sea {@code "tsec"},
	 * el valor de &eacute;ste es enmascarado, siendo reemplazado por un asterisco,
	 * a menos que su valor sea {@code ""}, en cuyo caso se mantiene dicha cadena.
	 * @return La cadena resultante o {@code null} si el header enviado es 
	 * {@code null}. Si se genera alguna excepci&oacute;n, devuelve una cadena vac&iacute;a.
	 */
	public static String headerReq2MaskedStr(HashMap<String, String> headerEnvio) {
		if (headerEnvio == null) {
			return null;
		} else {
			try{
				if (headerEnvio.containsKey("tsec")) {
					HashMap<String, String> headerEnvioMasked = new HashMap<String, String>();
					// Recorrer el header
					Iterator<Entry<String, String>> it = headerEnvio.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, String> e = (Map.Entry<String, String>) it.next();
						String clave = e.getKey();
						String valor = e.getValue();
						if (clave != null && clave.equalsIgnoreCase("tsec")) {
							valor = !valor.equals("") ? "*" : "";
						}
						headerEnvioMasked.put(clave, valor);
					}
					return headerEnvioMasked.toString();
				}
			}catch(Exception ex){
				return "";
			}
		}
		return headerEnvio.toString();
	}
	
	
	/**
	 * Construye y devuelve una representaci&oacute;n en {@code String} de todo
	 * el contenido del <strong>header</strong> que fue <strong>recibido</strong>
	 * del servicio. Si el header contiene un elemento cuya clave sea {@code "tsec"},
	 * el valor de &eacute;ste elemento es enmascarado, siendo reemplazado por un asterisco,
	 * a menos que su valor sea {@code ""} en cuyo caso se mantiene dicha cadena.
	 * @return La cadena resultante o {@code null} si el header recibido es
	 * {@code null}. Si se genera alguna excepci&oacute;n, devuelve una cadena vac&iacute;a.
	 */
	public static String headerResp2MaskedStr(HashMap<String, List<String>> headerRespuesta) {
		try {
			if (headerRespuesta == null) {
				return null;
			} else {
				if (headerRespuesta.containsKey("tsec")) {
					HashMap<String, List<String>> headerRespMasked = new HashMap<String, List<String>>();
					// Recorrer el header
					Iterator<Entry<String, List<String>>> it = headerRespuesta.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, List<String>> e = (Map.Entry<String, List<String>>) it.next();
						String clave = e.getKey();
						List<String> valor = e.getValue();
						if (clave != null && clave.equalsIgnoreCase("tsec")) {
							String tsecMasked = !valor.get(0).equals("") ? "*" : ""; 
							valor = new ArrayList<String>();
							valor.add(tsecMasked);
						}
						headerRespMasked.put(clave, valor);
					}
					return headerRespMasked.toString();
				}
			}
			return headerRespuesta.toString();
		} catch (Exception e) {
			return "";
		}
	}
	
}