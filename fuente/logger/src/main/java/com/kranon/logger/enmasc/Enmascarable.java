package com.kranon.logger.enmasc;

/**
 * Interfaz que establece un m�todo para ser usado en lugar de {@link Object#toString()}
 * cuando se despliega el contenido del objeto que la implementa. Usada para imprimir
 * colecciones {@link java.util.List} de objetos cuya informaci�n sensible deba
 * ser enmascarada.
 * 
 * Se debe implementar esta interfaz en los objetos que sean parte de la colecci�n
 * pero no es necesario que la implementen los objetos referenciados jerarquicamente
 * por aquellos en la colecci�n.  
 * @author planderos
 */
public interface Enmascarable {
	
	/**
	 * 
	 * @return
	 */
	public String toMaskedString();
}