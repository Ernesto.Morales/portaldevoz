package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define la alarma de WS
 * @author abalfaro
 *
 */
public class WsAlarm extends MonitoringAlarm implements AlarmInterface {

	private static final String nombreAlarma = "WS";

	private String idServicio;
	private String idModulo;
	private String idWebService;
	private String url;
	private String tipoError;
	private String codigoError;
	private String descripcionError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public WsAlarm(String idServicio, String idModulo, String idWebService, String url, String tipoError, String codigoError,
			String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		super(WsAlarm.nombreAlarma);

		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.idWebService = idWebService;
		this.url = url;
		this.tipoError = tipoError;
		this.codigoError = codigoError;
		this.descripcionError = descripcionError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero anado la cabecera
		String message = this.getHeader();

		// anado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.idWebService == null) ? "" : ("|" + this.idWebService))
						  + ((this.url == null) ? "" : ("|" + this.url))
						  + ((this.tipoError == null) ? "" : ("|" + this.tipoError))
						  + ((this.codigoError == null) ? "" : ("|" + this.codigoError))
						  + ((this.descripcionError == null) ? "" : ("|" + this.descripcionError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}
}
