package com.kranon.logger.utilidades;

public class Constantes {

	// Nombre fichero de monitorizacion de cada servicio 
	// El nombre final sera: "MONITORIZACION_" + <servicioIVR>
	public static final String FICHERO_SERVICIO_IVR = "MONITORIZACION_";
	
	// Nombre fichero de monitorizacion del enrutador 
	// El nombre final sera: "MONITORIZACION_ENRUTADOR_" + <servicioIVR>
	public static final String FICHERO_ENRUTADOR = "MONITORIZACION_ENRUTADOR_";
		
	// Variables para almacenar el tamanio maximo de los campos que deben escribirse en la traza de monitorizacion
	// FORMATO: PVO|<TipoAlarma>|Campo_1|Campo_2|Campo_3|Campo_4|<FechaHora>
	// Campo_1
	public static final int LONG_MAX_ID_SERVICIO = 4;
	// Campo_2
	public static final int LONG_MAX_VERSION_WS = 3;
	public static final int LONG_MAX_ID_MODULO = 3;
	public static final int LONG_MAX_IP_CTI = 3;
	// Campo_3
	public static final int LONG_MAX_NOMBRE_WS = 15;
	public static final int LONG_MAX_DESC_ERROR = 15;
	// Campo_4
	public static final int LONG_MAX_COD_ERROR = 3;
	
}
