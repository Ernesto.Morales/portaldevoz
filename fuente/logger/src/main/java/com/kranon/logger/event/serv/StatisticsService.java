package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de ESTADISTICA para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class StatisticsService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "STATISTICS";

	// identificador de la estadistica
	private String idEstadistica;
	private String resultado;
	
	private ArrayList<ParamEvent> parametrosAdicionales;

	public StatisticsService(String idLlamada, String idServicio, String idElemento, 
			String idEstadistica, String resultado,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idLlamada, idServicio, idElemento, StatisticsService.nombreEvento);

		this.idEstadistica = idEstadistica;
		this.resultado = resultado;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idEstadistica 
				+ ((this.resultado == null) ? "" : ("|" + this.resultado))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
