package com.kranon.logger.event;

/**
 * Clase PADRE comun a todos los EVENTOS de los SERVICIOS IVR
 * @author abalfaro
 *
 */
public abstract class ServiceEvent {

	// identificador callID de la llamada
	private String idLlamada;
	// identificador del servicio
	private String idServicio;
	// identificador del elemento: Enrutador, Controlador, Modulo
	private String idElemento;
	// tipo de evento: INIT_CALL, END_CALL, INIT_MODULE, END_MODULE, SPEECH,
	// DIALOGUE, ACTION, SIST_EXTERN, STATISTICS, ERROR, COMMENT
	private String tipoEvento;

	public ServiceEvent(String idLlamada, String idServicio, String idElemento, String tipoEvento) {
		this.idLlamada = idLlamada;
		this.idServicio = idServicio;
		this.idElemento = idElemento;
		this.tipoEvento = tipoEvento;
	}

	public String getHeader() {
		String message = "";

		message = message 
				+ this.idLlamada + "|" 
				+ this.idServicio + "|" 
				+ this.idElemento + "|" 
				+ this.tipoEvento + "|";

		return message;
	}
}
