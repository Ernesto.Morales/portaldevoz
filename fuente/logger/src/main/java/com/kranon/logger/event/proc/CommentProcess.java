package com.kranon.logger.event.proc;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ProcessEvent;


/**
 * Clase que define el evento de COMENTARIO para un PROCESO
 * @author abalfaro
 *
 */
public class CommentProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "COMMENT";

	private String texto;

	public CommentProcess(String idInvocacion, String idElemento, String texto) {
		super(idInvocacion, idElemento, CommentProcess.nombreEvento);

		this.texto = texto;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.texto;
	
		return message;
	}

}
