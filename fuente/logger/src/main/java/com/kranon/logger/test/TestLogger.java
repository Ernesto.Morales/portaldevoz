package com.kranon.logger.test;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;

public class TestLogger {

	public static void main(String[] args) {

		/*********************************************/
		/*********** LOGGER PROCESO JAVA *************/
		/*********************************************/

		// este logFile coincide con la declaracion en el log4j.properties
		String logFileJava = "PROCESO_JAVA";

		String idProcesoJava = "PROCESO_PRUEBAS";
		String idElementoJava = "PRUEBA_LOGGER";
		String idModuloJava = "EJ_TRAZA";

		String rutaEjecucionaJava = "/pruebas_ruta/";

		// el parametro es nombre del fichero de log donde queremos que escriba
		// (se corresponde con los declarados en el log4j.properties)
		CommonLoggerProcess log = new CommonLoggerProcess(logFileJava);

		try {

			/** INICIALIZO LOGGER **/
			// al inicializar, si pasamos el primer argumento a null se genera
			// solo
			// el idInvocacion (con fecha/dia...)
			log.inicializar(null, idElementoJava);

			// podemos guardarnos el idInvocacion creado al inicializar para
			// poder crear objetos mas adelante con el mismo identificador
			// String idInvocacion = log.getIdInvocacion();

			/** INICIO EVENTO - INICIO DE PROCESO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("clave", "valor"));
			log.initProcess(idProcesoJava, rutaEjecucionaJava, parametrosEntrada, parametrosAdicionales);
			/** FIN EVENTO - INICIO DE PROCESO **/

			/** INICIO EVENTO - INICIO DE MoDULO **/
			ArrayList<ParamEvent> parametrosEntrada2 = new ArrayList<ParamEvent>();
			parametrosEntrada2.add(new ParamEvent("clave", "valor"));
			log.initModuleProcess(idModuloJava, parametrosEntrada2);
			/** FIN EVENTO - INICIO DE MoDULO **/

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales2 = new ArrayList<ParamEvent>();
			parametrosAdicionales2.add(new ParamEvent("clave", "valor"));
			log.actionEvent("idAccion", "OK", parametrosAdicionales2);
			/** FIN EVENTO - ACCION **/

			/** INICIO EVENTO - SISTEMA EXTERNO **/
			ArrayList<ParamEvent> parametrosEntrada3 = new ArrayList<ParamEvent>();
			parametrosEntrada3.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosSalida3 = new ArrayList<ParamEvent>();
			parametrosSalida3.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales3 = new ArrayList<ParamEvent>();
			parametrosAdicionales3.add(new ParamEvent("clave", "valor"));
			log.sistExternEvent("idSistemaExterno", "rutaSistema", parametrosEntrada3, parametrosSalida3, "OK", parametrosAdicionales3);
			/** FIN EVENTO - SISTEMA EXTERNO **/

			/** INICIO EVENTO - COMENTARIO **/
			log.comment("variable=" + "<valor_variable>");
			/** FIN EVENTO - COMENTARIO **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosEntrada4 = new ArrayList<ParamEvent>();
			parametrosEntrada4.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales4 = new ArrayList<ParamEvent>();
			parametrosAdicionales4.add(new ParamEvent("clave", "valor"));
			log.endModuleProcess(idModuloJava, "OK", parametrosEntrada4, parametrosAdicionales4);
			/** FIN EVENTO - FIN DE MoDULO **/

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida4 = new ArrayList<ParamEvent>();
			parametrosSalida4.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales5 = new ArrayList<ParamEvent>();
			parametrosAdicionales5.add(new ParamEvent("clave", "valor"));
			log.endProcess(idProcesoJava, "OK", parametrosSalida4, parametrosAdicionales5);
			/** FIN EVENTO - FIN DE PROCESO **/

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), idProcesoJava, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MoDULO **/
			ArrayList<ParamEvent> parametrosEntrada4 = new ArrayList<ParamEvent>();
			parametrosEntrada4.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales4 = new ArrayList<ParamEvent>();
			parametrosAdicionales4.add(new ParamEvent("clave", "valor"));
			log.endModuleProcess(idModuloJava, "KO", parametrosEntrada4, parametrosAdicionales4);
			/** FIN EVENTO - FIN DE MoDULO **/

			/** INICIO EVENTO - FIN DE PROCESO **/
			ArrayList<ParamEvent> parametrosSalida4 = new ArrayList<ParamEvent>();
			parametrosSalida4.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales5 = new ArrayList<ParamEvent>();
			parametrosAdicionales5.add(new ParamEvent("clave", "valor"));
			log.endProcess(idProcesoJava, "KO", parametrosSalida4, parametrosAdicionales5);
			/** FIN EVENTO - FIN DE PROCESO **/

		}

		/*********************************************/
		/*********** LOGGER SERVICIO IVR *************/
		/*********************************************/

		// este logFile coincide con la declaracion en el log4j.properties
		String logFileIVR = "SERVICIO_IVR";

		String idElementoIVR = "PRUEBA_LOGGER";
		String idModuloIVR = "EJ_TRAZA";

		String idLlamada = "call_id_de_la_llamada";
		String idServicio = "SERVICIO_PRUEBA_LOGGER";

		// el parametro es nombre del fichero de log donde queremos que escriba
		// (se corresponde con los declarados en el log4j.properties)
		CommonLoggerService logService = new CommonLoggerService(logFileIVR);
		try {
			/** INICIALIZO LOGGER **/
			// al inicializar, pasamos el id de la llamada que es el CallID
			logService.inicializar(idLlamada, idServicio, idElementoIVR);

			/** INICIO EVENTO - INICIO DE LLAMADA **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("clave", "valor"));
			logService.initCall("numOrigen", "numDestino", parametrosEntrada);
			/** FIN EVENTO - INICIO DE LLAMADA **/

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada2 = new ArrayList<ParamEvent>();
			parametrosEntrada2.add(new ParamEvent("clave", "valor"));
			logService.initModuleService(idModuloIVR, parametrosEntrada2);
			/** FIN EVENTO - INICIO DE MODULO **/

			/** INICIO EVENTO - COMENTARIO **/
			logService.comment("variable=" + "<valor_variable>");
			/** FIN EVENTO - COMENTARIO **/

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("clave", "valor"));
			logService.actionEvent("idAccion", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			/** INICIO EVENTO - DIALOGO **/
			ArrayList<ParamEvent> parametrosAdicionales2 = new ArrayList<ParamEvent>();
			parametrosAdicionales2.add(new ParamEvent("clave", "valor"));
			logService.dialogueEvent("idMenu", "ASRDTMF", "DTMF", 1, "2", "OK", parametrosAdicionales2);
			/** FIN EVENTO - DIALOGO **/

			/** INICIO EVENTO - LOCUCION **/
			ArrayList<ParamEvent> parametrosAdicionales4 = new ArrayList<ParamEvent>();
			parametrosAdicionales4.add(new ParamEvent("clave", "valor"));
			logService.speechEvent("TTS", "Por favor introduzca...", parametrosAdicionales4);
			/** FIN EVENTO - LOCUCION **/

			/** INICIO EVENTO - SISTEMA EXTERNO **/
			ArrayList<ParamEvent> parametrosEntrada3 = new ArrayList<ParamEvent>();
			parametrosEntrada3.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales3 = new ArrayList<ParamEvent>();
			parametrosAdicionales3.add(new ParamEvent("clave", "valor"));
			logService.sistExternEvent("idSistemaExterno", "rutaSistema", parametrosEntrada3, parametrosSalida, "OK", parametrosAdicionales3);
			/** FIN EVENTO - SISTEMA EXTERNO **/

			/** INICIO EVENTO - ESTADISTICA **/
			ArrayList<ParamEvent> parametrosAdicionales5 = new ArrayList<ParamEvent>();
			parametrosAdicionales5.add(new ParamEvent("clave", "valor"));
			logService.statisticEvent("idEstadistica", "OK", parametrosAdicionales5);
			/** FIN EVENTO - ESTADISTICA **/

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida2 = new ArrayList<ParamEvent>();
			parametrosSalida2.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales6 = new ArrayList<ParamEvent>();
			parametrosAdicionales6.add(new ParamEvent("clave", "valor"));
			logService.endModuleService(idModuloIVR, "OK", parametrosSalida2, parametrosAdicionales6);
			/** FIN EVENTO - FIN DE MODULO **/

			/** INICIO EVENTO - FIN LLAMADA **/
			ArrayList<ParamEvent> parametrosAdicionales7 = new ArrayList<ParamEvent>();
			parametrosAdicionales7.add(new ParamEvent("clave", "valor"));
			logService.endCall("numOrigen", "numDestino", "OK", "15", parametrosAdicionales7);
			/** FIN EVENTO - FIN LLAMADA **/

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			logService.error(e.getMessage(), idModuloIVR, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("clave", "valor"));
			ArrayList<ParamEvent> parametrosAdicionales2 = new ArrayList<ParamEvent>();
			parametrosAdicionales2.add(new ParamEvent("clave", "valor"));
			logService.endModuleService(idModuloIVR, "KO", parametrosSalida, parametrosAdicionales2);
			/** FIN EVENTO - FIN DE MODULO **/

			/** INICIO EVENTO - FIN LLAMADA **/
			ArrayList<ParamEvent> parametrosAdicionales3 = new ArrayList<ParamEvent>();
			parametrosAdicionales3.add(new ParamEvent("clave", "valor"));
			logService.endCall("numOrigen", "numDestino", "KO", "15", parametrosAdicionales3);
			/** FIN EVENTO - FIN LLAMADA **/
		}

	}

}
