package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define la alarma de WAS
 * @author abalfaro
 *
 */
public class WasAlarm extends MonitoringAlarm implements AlarmInterface {

	private static final String nombreAlarma = "WAS";

	private String idServicio;
	private String idModulo;
	private String error;
	private String descripcionError;
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	public WasAlarm(String idServicio, String idModulo, String error, String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		super(WasAlarm.nombreAlarma);

		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.error = error;
		this.descripcionError = descripcionError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.error == null) ? "" : ("|" + this.error))
	
						  + ((this.descripcionError == null) ? "" : ("|" + this.descripcionError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
