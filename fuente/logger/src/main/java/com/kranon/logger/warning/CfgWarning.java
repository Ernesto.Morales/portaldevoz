package com.kranon.logger.warning;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.logger.utilidades.UtilidadesLogger;

/**
 * Clase que define el WARNING CFG (v2)
 * @author abalfaro
 *
 */
public class CfgWarning extends MonitoringWarning implements WarningInterface {

	private static final String nombreAlarma = "CFG";
	private String idInvocacion;
	private String idServicio;
	private String idModulo;
	private String descripcionError;
	private String codError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public CfgWarning(String idInvocacion, String idServicio, String idModulo, String descripcionError, String codError, 
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(CfgWarning.nombreAlarma);
		
		this.idInvocacion = idInvocacion;
		
		this.idServicio = UtilidadesLogger.cambiarNombreAplicacion(idServicio);	
				
		this.idModulo = (idModulo.length() > Constantes.LONG_MAX_ID_MODULO) ? 
				(idModulo.substring(0, Constantes.LONG_MAX_ID_MODULO)) : (idModulo);
	
		this.codError = (codError.length() > Constantes.LONG_MAX_COD_ERROR) ? 
				(codError.substring(0, Constantes.LONG_MAX_COD_ERROR)) : (codError);
				
		// Si el codigo de error es "key", entonces se almacenan los primeros N caracteres de la descripcion
		if (this.codError.equalsIgnoreCase("key")) {
			this.descripcionError = (descripcionError.length() > Constantes.LONG_MAX_DESC_ERROR) ? 
					(descripcionError.substring(0, Constantes.LONG_MAX_DESC_ERROR)) : (descripcionError);
		}
		// En cualquier otro caso (normalmente se informara "fil"), se almacenan los ultimos N caracteres de la descripcion
		else {
			int resta = descripcionError.length() - Constantes.LONG_MAX_DESC_ERROR;
			this.descripcionError = (descripcionError.length() > Constantes.LONG_MAX_DESC_ERROR) ? 
					(descripcionError.substring(resta)) : (descripcionError);
		}
										
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idServicio 
				+ ((this.idModulo == null) ? ("|" + "") : ("|" + this.idModulo))
				+ ((this.descripcionError == null) ? ("|" + "") : ("|" + this.descripcionError))
				+ ((this.codError == null) ? ("|" + "") : ("|" + this.codError))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()))
				+ ((this.idInvocacion == null) ? ("|" + "") : ("|" + this.idInvocacion));

		return message;
	}

}
