package com.kranon.logger.event.proc;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ProcessEvent;


/**
 * Clase que define el evento de FIN de PROCESO
 * @author abalfaro
 *
 */
public class EndProcess extends ProcessEvent implements EventInterface {

	private static final String nombreEvento = "END_PROCESS";

	private String idProceso;
	private String codigoRetorno;

	private ArrayList<ParamEvent> parametrosSalida;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public EndProcess(String idInvocacion, String idElemento, String idProceso, String codigoRetorno, ArrayList<ParamEvent> parametrosSalida,
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(idInvocacion, idElemento, EndProcess.nombreEvento);

		this.idProceso = idProceso;
		this.codigoRetorno = codigoRetorno;
		this.parametrosSalida = parametrosSalida;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idProceso 
				+ ((this.codigoRetorno == null) ? "" : ("|" + this.codigoRetorno))
				+ ((this.parametrosSalida == null) ? "" : ("|OUT=" + this.parametrosSalida.toString()))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
