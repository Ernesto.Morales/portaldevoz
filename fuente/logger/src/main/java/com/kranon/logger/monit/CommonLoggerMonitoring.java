package com.kranon.logger.monit;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.kranon.logger.alarm.AsrAlarm;
import com.kranon.logger.alarm.CfgAlarm;
import com.kranon.logger.alarm.CtiAlarm;
import com.kranon.logger.alarm.DefaultAlarm;
import com.kranon.logger.alarm.TtsAlarm;
import com.kranon.logger.alarm.WasAlarm;
import com.kranon.logger.alarm.WsAlarm;
import com.kranon.logger.warning.AsrWarning;
import com.kranon.logger.warning.CfgWarning;
import com.kranon.logger.warning.CtiWarning;
import com.kranon.logger.warning.DefaultWarning;
import com.kranon.logger.warning.TtsWarning;
import com.kranon.logger.warning.WasWarning;
import com.kranon.logger.warning.WsWarning;
import com.kranon.logger.event.ParamEvent;

public class CommonLoggerMonitoring implements LoggerMonitoringInterface {

	// Objeto logger para la escritura de trazas
	private String idInvocacion;
	private Logger log;

	public CommonLoggerMonitoring(String idInvocacion, String logFile) {
		try {
			this.idInvocacion = idInvocacion;
			log = Logger.getLogger(logFile.toUpperCase());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * -----------------------------------------------------------------------------------------
	 * ------------------------ INICIO VERSION 1 LOGS DE MONITORIZACION ------------------------
	 * -----------------------------------------------------------------------------------------
	 */
	
	/** ASR **/
	public void alarmASR(String idServicio, String idModulo, String idError, String idMenu, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			AsrAlarm asrAlarm = new AsrAlarm(this.idInvocacion, idServicio, idModulo, idError, idMenu, parametrosAdicionales);
			this.log.info(asrAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** TTS **/
	public void alarmTTS(String idServicio, String idModulo, String idError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			TtsAlarm ttsAlarm = new TtsAlarm(idServicio, idModulo, idError, parametrosAdicionales);
			this.log.info(ttsAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** WS **/
	public void alarmWS(String idServicio, String idModulo, String idWebService, String url, String tipoError, String codigoError,
			String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			WsAlarm wsAlarm = new WsAlarm(idServicio, idModulo, idWebService, url, tipoError, codigoError, descripcionError, parametrosAdicionales);
			this.log.info(wsAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** CTI **/
	public void alarmCTI(String idServicio, String idModulo, String codigoError, String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			CtiAlarm ctiAlarm = new CtiAlarm(idServicio, idModulo, codigoError, descripcionError, parametrosAdicionales);
			this.log.info(ctiAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** CFG **/
	public void alarmCFG(String idServicio, String idModulo, String idArchivo, String rutaCompleta, String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			CfgAlarm cfgAlarm = new CfgAlarm(idServicio, idModulo, idArchivo, rutaCompleta, descripcionError, parametrosAdicionales);
			this.log.info(cfgAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** WAS **/
	public void alarmWAS(String idServicio, String idModulo, String error, String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			WasAlarm wasAlarm = new WasAlarm(idServicio, idModulo, error, descripcionError, parametrosAdicionales);
			this.log.info(wasAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** DEFAULT **/
	public void alarmDefault(String idAlarma, String idServicio, String idModulo, String idError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			DefaultAlarm defaultAlarm = new DefaultAlarm(idAlarma, idServicio, idModulo, idError, parametrosAdicionales);
			this.log.info(defaultAlarm.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * -----------------------------------------------------------------------------------------
	 * ------------------------ FIN VERSION 1 LOGS DE MONITORIZACION ------------------------
	 * -----------------------------------------------------------------------------------------
	 */
	
	
	/**
	 * -----------------------------------------------------------------------------------------
	 * ------------------------ INICIO VERSION 2 LOGS DE MONITORIZACION ------------------------
	 * -----------------------------------------------------------------------------------------
	 */

	/** ASR **/
	public void warningASR(String idServicio, String idModulo, String descripcionError, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			AsrWarning asrWarning = new AsrWarning(this.idInvocacion, idServicio, idModulo, descripcionError, codError, parametrosAdicionales);
			this.log.info(asrWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** TTS **/
	public void warningTTS(String idServicio, String idModulo, String descripcionError, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			TtsWarning ttsWarning = new TtsWarning(this.idInvocacion, idServicio, idModulo, descripcionError, codError, parametrosAdicionales);
			this.log.info(ttsWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** WS **/
	public void warningWS(String idServicio, String versionWS, String nombreWS, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			WsWarning wsWarning = new WsWarning(this.idInvocacion, idServicio, versionWS, nombreWS, codError, parametrosAdicionales);
			this.log.info(wsWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** WS **/
	public void warningWS(String idServicio, String versionWS, String nombreWS, String codError, String tipoError, String descError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			WsWarning wsWarning = new WsWarning(this.idInvocacion, idServicio, versionWS, nombreWS, codError, tipoError, descError, parametrosAdicionales);
			this.log.info(wsWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** CTI **/
	public void warningCTI(String idServicio, String idModulo, String descripcionError, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {
			CtiWarning ctiWarning = new CtiWarning(this.idInvocacion, idServicio, idModulo, descripcionError, codError, parametrosAdicionales);
			this.log.info(ctiWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** CFG **/
	public void warningCFG(String idServicio, String idModulo, String descripcionError, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			CfgWarning cfgWarning = new CfgWarning(this.idInvocacion, idServicio, idModulo, descripcionError, codError, parametrosAdicionales);
			this.log.info(cfgWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** WAS **/
	public void warningWAS(String idServicio, String idModulo, String error, String codError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			WasWarning wasWarning = new WasWarning(this.idInvocacion, idServicio, idModulo, error, codError, parametrosAdicionales);
			this.log.info(wasWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** DEFAULT **/
	public void warningDefault(String idAlarma, String idServicio, String idModulo, String idError, ArrayList<ParamEvent> parametrosAdicionales) {
		try {	
			DefaultWarning defaultWarning = new DefaultWarning(this.idInvocacion, idAlarma, idServicio, idModulo, idError, parametrosAdicionales);
			this.log.info(defaultWarning.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * -----------------------------------------------------------------------------------------
	 * ------------------------ FIN VERSION 2 LOGS DE MONITORIZACION ------------------------
	 * -----------------------------------------------------------------------------------------
	 */

}
