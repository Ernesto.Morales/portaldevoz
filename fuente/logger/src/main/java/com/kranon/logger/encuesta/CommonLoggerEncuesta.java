package com.kranon.logger.encuesta;

import java.util.ArrayList;

import com.kranon.logger.CommonLogger;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.encuesta.EncuestaResultado;

public class CommonLoggerEncuesta extends CommonLogger implements LoggerEncuestaInterface {

	// propio del log de servicios IVR
	private String idServicio;

	public CommonLoggerEncuesta(String logFile) {
		super(logFile);
	}

	/**
	 * Inicializa la informacion del LOGGER
	 * 
	 * @param String
	 *            idLlamada
	 * @param String
	 *            idServicio
	 * @param String
	 *            idElemento
	 */
	public void inicializar(String idLlamada, String idServicio, String idElemento) {
		super.inicializar(idLlamada, idElemento);
		this.idServicio = idServicio;
	}

	/************ GETTERS **************/
	public String getIdLlamada() {
		// return idLlamada;
		return idInvocacion;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public String getIdElemento() {
		return idElemento;
	}
	
	
	/**
	 * Escribe una traza de Comentario con severidad DEBUG
	 * 
	 * @param {@link String} texto
	 */
	public void resultadoEncuesta(String descripcionEncuesta, String numTarjeta, String idAsesor, String encuestaCompleta, ArrayList<ParamEvent> resultados) {
		try {
			EncuestaResultado encuestaResultado = new EncuestaResultado(idInvocacion, idServicio, idElemento, descripcionEncuesta, numTarjeta, idAsesor, encuestaCompleta, resultados);
			//String traza = this.getInfoLogger() + encuestaResultado.getMessage();
			String traza = encuestaResultado.getMessage();
			
			this.log.info(traza);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
