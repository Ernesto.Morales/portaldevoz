package com.kranon.logger.warning;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.logger.utilidades.UtilidadesLogger;

/**
 * Clase que define el WARNING TTS (v2)
 * @author abalfaro
 *
 */
public class TtsWarning extends MonitoringWarning implements WarningInterface {

	private static final String nombreAlarma = "TTS";
	private String idInvocacion;
	private String idServicio;
	private String idModulo;
	private String descripcionError;
	private String codError;
	private ArrayList<ParamEvent> parametrosAdicionales;

	public TtsWarning(String idInvocacion, String idServicio, String idModulo, String descripcionError, String codError, 
			ArrayList<ParamEvent> parametrosAdicionales) {
		super(TtsWarning.nombreAlarma);
		
		this.idInvocacion = idInvocacion;

		this.idServicio = UtilidadesLogger.cambiarNombreAplicacion(idServicio);	
		
		this.idModulo = (idModulo.length() > Constantes.LONG_MAX_ID_MODULO) ? 
				(idModulo.substring(0, Constantes.LONG_MAX_ID_MODULO)) : (idModulo);

		this.descripcionError = (UtilidadesLogger.getDescripcionError(descripcionError).length() > Constantes.LONG_MAX_DESC_ERROR) ? 
				(UtilidadesLogger.getDescripcionError(descripcionError).substring(0, Constantes.LONG_MAX_DESC_ERROR)) : 
				(UtilidadesLogger.getDescripcionError(descripcionError));
				
		this.codError = (codError.length() > Constantes.LONG_MAX_COD_ERROR) ? 
				(codError.substring(0, Constantes.LONG_MAX_COD_ERROR)) : (codError);
				
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero anado la cabecera
		String message = this.getHeader();

		// anado el resto de la traza
		message = message + this.idServicio 
				+ ((this.idModulo == null) ? ("|" + "") : ("|" + this.idModulo))
				+ ((this.descripcionError == null) ? ("|" + "") : ("|" + this.descripcionError))
				+ ((this.codError == null) ? ("|" + "") : ("|" + this.codError))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()))
				+ ((this.idInvocacion == null) ? ("|" + "") : ("|" + this.idInvocacion));

		return message;
	}
}
