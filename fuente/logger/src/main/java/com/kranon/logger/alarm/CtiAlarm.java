package com.kranon.logger.alarm;

import java.util.ArrayList;

import com.kranon.logger.event.ParamEvent;

/**
 * Clase que define la alarma de CTI
 * @author abalfaro
 *
 */
public class CtiAlarm extends MonitoringAlarm implements AlarmInterface {

	private static final String nombreAlarma = "CTI";

	private String idServicio;
	private String idModulo;
	private String codigoError;
	private String descripcionError;
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	public CtiAlarm(String idServicio, String idModulo, String codigoError, String descripcionError, ArrayList<ParamEvent> parametrosAdicionales) {
		super(CtiAlarm.nombreAlarma);

		this.idServicio = idServicio;
		this.idModulo = idModulo;
		this.codigoError = codigoError;
		this.descripcionError = descripcionError;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero anado la cabecera
		String message = this.getHeader();

		// anado el resto de la traza
		message = message + this.idServicio 
						  + ((this.idModulo == null) ? "" : ("|" + this.idModulo))
						  + ((this.codigoError == null) ? "" : ("|" + this.codigoError))
						  + ((this.descripcionError == null) ? "" : ("|" + this.descripcionError))
						  + ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
