package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de ERROR para un SERVICIO IVR
 * 
 * @author abalfaro
 *
 */
public class ErrorService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "ERROR";

	private String idError;
	private String localizacion;
	private ArrayList<ParamEvent> parametrosAdicionales;
	private Exception exception;

	public ErrorService(String idLlamada, String idServicio, String idElemento, String idError, String localizacion,
			ArrayList<ParamEvent> parametrosAdicionales, Exception exception) {
		super(idLlamada, idServicio, idElemento, ErrorService.nombreEvento);

		this.idError = idError;
		this.localizacion = localizacion;
		this.parametrosAdicionales = parametrosAdicionales;
		this.exception = exception;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message + this.idError + ((this.localizacion == null) ? "" : ("|" + this.localizacion))
				+ ((this.parametrosAdicionales == null) ? "" : ("|" + this.parametrosAdicionales.toString()));

		if (this.exception != null) {
			StackTraceElement[] stackTrace = this.exception.getStackTrace();
			if (stackTrace != null) {
				int i = 0;
				message = message + "|" + "[stackTrace=";
				for (StackTraceElement elem : stackTrace) {
					if (elem.toString().contains("com.kranon.")) {
						if (i > 0) {
							message = message + ", ";
						}
						message = message + elem.toString();
						i = i + 1;
					}

				}
				message = message + "]";
			}
		}

		return message;
	}

}
