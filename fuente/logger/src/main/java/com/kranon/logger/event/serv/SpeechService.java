package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de LOCUCIoN para un SERVICIO IVR
 * @author abalfaro
 *
 */
public class SpeechService extends ServiceEvent implements EventInterface {
	
	private static final String nombreEvento = "SPEECH";
	
	// WAV o TTS
	private String tipoLocucion;
	// identificador de la locucion o texto a reproducir
	private String locucion;
	
	private ArrayList<ParamEvent> parametrosAdicionales;
	
	public SpeechService(String idLlamada, String idServicio, String idElemento,
			String tipoLocucion, String locucion, 
			ArrayList<ParamEvent> parametrosAdicionales) {
		
		super(idLlamada, idServicio, idElemento, SpeechService.nombreEvento);
			
		this.tipoLocucion = tipoLocucion;
		this.locucion = locucion;
		this.parametrosAdicionales = parametrosAdicionales;
	}

	public String getMessage() {
		// primero anado la cabecera
		String message = this.getHeader();

		// anado el resto de la traza
		message = message + this.tipoLocucion
				+ ((this.locucion==null)?"": ("|" + this.locucion))
				+ ((this.parametrosAdicionales==null)?"":("|" + this.parametrosAdicionales.toString()));

		return message;
	}

}
