package com.kranon.logger.logger.all;

import com.kranon.logger.CommonLogger;

public class CommonLoggerKranon extends CommonLogger {

	private String idServicio;

	public CommonLoggerKranon(String logFile,String idLlamada) {
		super(logFile);
		idServicio=idLlamada;
	}
	
	public void imprimeLogKranon(String log) {
		try {
			this.log.info(idServicio+"|"+this.getInfoLogger()+log);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**************************/
	/**************************/
	/**************************/
	/**
	 * Crea la cabecera de la traza con la siguiente info del punto donde se ha mandado escribir la traza, con formato: [clase.metodo:linea]
	 * 
	 * @return {@link String}
	 */
	private String getInfoLogger() {
		int nivel = 4;
		String className = Thread.currentThread().getStackTrace()[nivel].getClassName();
//		String methodName = Thread.currentThread().getStackTrace()[nivel].getMethodName();
//		int lineNumber = Thread.currentThread().getStackTrace()[nivel].getLineNumber();
		String claseArray[] = className.split("\\.");
		int val = claseArray.length - 1;
		return " [" + claseArray[val] + "] - ";
	}
	
//	public String obtenerformatoCadena(String []){
//		
//	}
	
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}
	public String getIdServicio() {
		return idServicio;
	}

}
