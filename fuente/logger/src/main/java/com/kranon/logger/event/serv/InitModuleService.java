package com.kranon.logger.event.serv;

import java.util.ArrayList;

import com.kranon.logger.event.EventInterface;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.event.ServiceEvent;

/**
 * Clase que define el evento de INICIO de MoDULO para un SERVICIO IVR
 * 
 * @author abalfaro
 *
 */
public class InitModuleService extends ServiceEvent implements EventInterface {

	private static final String nombreEvento = "INIT_MODULE";

	private String idModulo;
	private ArrayList<ParamEvent> parametrosEntrada;

	public InitModuleService(String idLlamada, String idServicio, String idElemento, String idModulo, ArrayList<ParamEvent> parametrosEntrada) {

		super(idLlamada, idServicio, idElemento, InitModuleService.nombreEvento);

		this.idModulo = idModulo;
		this.parametrosEntrada = parametrosEntrada;
	}

	public String getMessage() {
		// primero aado la cabecera
		String message = this.getHeader();

		// aado el resto de la traza
		message = message 
				+ this.idModulo 
				+ ((this.parametrosEntrada == null) ? "" : ("|IN=" + this.parametrosEntrada.toString()));

		return message;
	}

}
