
// ************************************************************************ //
// ********** RECUPERA EL TIPO DE BIN  *************** //
// ************************************************************************ //

function devuelveTipoBin(bin){
	bin=bin+"";
	var resultado = "";
	switch (bin) {
				case '001701' : resultado = "debito"; break;
				case '001702' : resultado = "debito"; break;
				case '001780' : resultado = "credito empresarial"; break;
				case '001700' : resultado = "credito empresarial"; break;
				
				case '408341' : resultado = "movil"; break;
				case '408343' : resultado = "movil"; break;
				case '409851' : resultado = "express"; break;
				
				case '410177' : resultado = "debito"; break;
				case '410180' : resultado = "credito"; break;
				case '410181' : resultado = "credito"; break;
				case '415201' : resultado = "debito"; break;
				case '415231' : resultado = "debito"; break;
				case '415327' : resultado = "credito"; break;
				case '418073' : resultado = "credito"; break;
				case '418075' : resultado = "credito"; break;
				case '418076' : resultado = "credito empresarial"; break;
				case '418077' : resultado = "credito"; break;
				case '418078' : resultado = "credito"; break;
				case '418080' : resultado = "credito"; break;
				case '418090' : resultado = "credito"; break;
				case '418091' : resultado = "credito"; break;
				case '418093' : resultado = "credito"; break;
				case '418094' : resultado = "credito"; break;
				
				case '441310' : resultado = "credito"; break;
				case '441311' : resultado = "credito"; break;
				case '441312' : resultado = "prepago"; break;
				case '441313' : resultado = "prepago"; break;
				case '441314' : resultado = "prepago"; break;
				case '441531' : resultado = "prepago"; break;
				case '444085' : resultado = "credito"; break;
				case '444086' : resultado = "credito"; break;
				case '444087' : resultado = "credito"; break;
				case '446115' : resultado = "prepago"; break;
				case '446116' : resultado = "prepago"; break;
				case '446117' : resultado = "prepago"; break;
				case '446118' : resultado = "prepago"; break;
				
				case '451712' : resultado = "debito"; break;
				case '455500' : resultado = "credito"; break;
				case '455501' : resultado = "credito"; break;
				case '455502' : resultado = "credito"; break;
				case '455503' : resultado = "credito"; break;
				case '455504' : resultado = "credito"; break;
				case '455505' : resultado = "credito"; break;
				case '455506' : resultado = "credito empresarial"; break;
				case '455507' : resultado = "credito empresarial"; break;
				case '455508' : resultado = "credito"; break;
				case '455509' : resultado = "debito"; break;
				case '455510' : resultado = "debito"; break;
				case '455511' : resultado = "credito empresarial"; break;
				case '455513' : resultado = "credito empresarial"; break;
				case '455514' : resultado = "credito empresarial"; break;
				case '455515' : resultado = "credito empresarial"; break;
				case '455517' : resultado = "credito empresarial"; break;
				case '455518' : resultado = "credito empresarial"; break;
				case '455519' : resultado = "credito empresarial"; break;
				case '455525' : resultado = "credito"; break;
				case '455526' : resultado = "credito"; break;
				case '455527' : resultado = "credito"; break;
				case '455529' : resultado = "credito"; break;
				case '455533' : resultado = "debito"; break;
				case '455537' : resultado = "debito"; break;
				case '455540' : resultado = "credito"; break;
				case '455545' : resultado = "credito"; break;
				case '455549' : resultado = "debito"; break;
				
				case '477210' : resultado = "credito"; break;
				case '477211' : resultado = "credito"; break;
				case '477212' : resultado = "credito"; break;
				case '477213' : resultado = "credito"; break;
				case '477214' : resultado = "credito"; break;
				case '477291' : resultado = "credito"; break;
				case '477292' : resultado = "credito"; break;
				
				case '481514' : resultado = "credito"; break;
				case '481515' : resultado = "credito"; break;
				case '481516' : resultado = "debito"; break;
				case '481517' : resultado = "debito"; break;
				case '485982' : resultado = "credito"; break;
				
				case '493160' : resultado = "credito"; break;
				case '493161' : resultado = "credito"; break;
				case '493162' : resultado = "credito"; break;
				case '494398' : resultado = "credito"; break;
				case '494399' : resultado = "debito"; break;
				case '498584' : resultado = "credito"; break;
				case '498585' : resultado = "credito"; break;
				case '498586' : resultado = "debito"; break;
				case '498587' : resultado = "debito"; break;
				
				case '517439' : resultado = "debito"; break;
				case '517440' : resultado = "movil"; break;
				
				case '522498' : resultado = "credito"; break;
				
				case '535875' : resultado = "credito"; break;
				
				case '542010' : resultado = "credito"; break;
				case '542015' : resultado = "credito"; break;
				case '542073' : resultado = "credito"; break;
				case '542977' : resultado = "credito"; break;
				case '544053' : resultado = "credito empresarial"; break;
				case '544551' : resultado = "credito"; break;
				case '546688' : resultado = "credito"; break;
				case '547077' : resultado = "credito"; break;
				case '547086' : resultado = "credito"; break;
				case '547095' : resultado = "credito"; break;
				case '547155' : resultado = "credito"; break;
				case '547156' : resultado = "credito"; break;
				case '547492' : resultado = "credito empresarial"; break;
				case '549613' : resultado = "debito"; break;
				
				case '554629' : resultado = "credito"; break;
				
				
				default: resultado = "DESCONOCIDO";
			}
			
	return resultado;
}



function esBinConDesborde(bin){
	bin=bin+"";
	var resultado = "";
	switch (bin) {
	
				case '410180' : resultado = "SI"; break;
				case '410181' : resultado = "SI"; break;
				
				case '441310' : resultado = "SI"; break;
				case '441311' : resultado = "SI"; break;
				case '441314' : resultado = "SI"; break;
				
				case '455500' : resultado = "SI"; break;
				case '455503' : resultado = "SI"; break;
				case '455504' : resultado = "SI"; break;
				case '455505' : resultado = "SI"; break;
				case '455508' : resultado = "SI"; break;
				case '455529' : resultado = "SI"; break;
				case '455540' : resultado = "SI"; break;
				case '455545' : resultado = "SI"; break;
				
				case '477210' : resultado = "SI"; break;
				case '477211' : resultado = "SI"; break;
				case '477212' : resultado = "SI"; break;
				case '477213' : resultado = "SI"; break;
				case '477214' : resultado = "SI"; break;
				case '477291' : resultado = "SI"; break;
				case '477292' : resultado = "SI"; break;
				
				case '493160' : resultado = "SI"; break;
				case '493161' : resultado = "SI"; break;
				case '493162' : resultado = "SI"; break;
				case '494398' : resultado = "SI"; break;
				
				case '542010' : resultado = "SI"; break;
				case '542977' : resultado = "SI"; break;
				case '544053' : resultado = "SI"; break;
				case '544551' : resultado = "SI"; break;
				case '547077' : resultado = "SI"; break;
				case '547086' : resultado = "SI"; break;
				case '547095' : resultado = "SI"; break;
				case '547155' : resultado = "SI"; break;
				case '547156' : resultado = "SI"; break;
				
				case '554629' : resultado = "SI"; break;
				
				default: resultado = "NO";
			}
			
	return resultado;
}	

function esBinPlatinum(bin){
	bin=bin+"";
	var resultado = "";
	switch (bin) {
	
				case '410181' : resultado = "platinum"; break;
				default: resultado = "NO";
			}
			
	return resultado;
}
				
				

			