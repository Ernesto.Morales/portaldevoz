
// ***************************************************** //
// ************ DATOS BEAN-INFO-OPERATIVA ************** //
// ***************************************************** //

function BeanInfoOperativa() {	
	
	this.nombre = "";
	
	// si esta activa la operativa en el controlador
	this.isActivo = "";
	
	this.tipoProceso = "";
	
	// tipoProceso = Desambiguacion
//	this.infoProceso = new GestionDesambiguacion();
	this.infoProceso = "";
	
	
	
}

//** EXCEPCIONES **/
//function GestionExcepciones() {	
//	this.excep = "";
//}

//** DESAMBIGUACION **/
function GestionDesambiguacion() {	
	this.menu = "";
}

//** INFORMACION **/
function GestionInformacion() {	
	// nombre de las locuciones comunes a reproducir separadas por pipes
	this.locuciones = "";
	
//	 excepciones[i] = new GestionExcepciones();
//	this.excepciones = new Array();
	
	// accion a realizar si no se cumple ninguna excepcion
	this.accion="";
}

//** TRANSFERENCIA **/
function GestionTransferencia() {	
	/////// ?????????????????????????????????????????????????
}
// ** EVALUACION **/
//function GestionEvaluacion() {	
//	this.nombreOperativa = "";
//	this.tipoMetadato = "";
//	this.nombreMetadato = "";
//}

//** AUTOMATIZACION **/
function GestionAutomatizacion() {	
	
	this.proactivo="";
	// accion a realizar si no se cumple ninguna excepcion
	this.accion="";
	this.nombre="";
	this.ruta="";
	this.activo="";
	
	// identificacion
	this.isReqIdentificacion = "";
	this.rutaIdentificacion = "";
	this.tipoIdentificacion = ""; // cadena de los candados requeridos separados por |
	this.intentosIdentificacion = "";
//	this.accionMaxintMenuIdentificacion = "";
//	this.accionMaxintValidIdentificacion = "";
	
	// autenticacion
	this.isReqAutenticacion = ""; 
	this.rutaAutenticacion = "";
	this.tipoAutenticacion = ""; // cadena de los candados requeridos separados por |
	this.intentosAutenticacion = "";	
	
	// ABALFARO_20170619
	this.accionAutenticacion = "";
	
	// acciones para autenticacion LB
//	this.accionClienteSinElementosAuth = "";
//	this.accionClienteBloqueadoCandadoAuth = "";
//	this.accionMaxintMenuAuth = "";
//	this.accionMaxintValidAuth = "";
//	this.accionClienteBloqueadoValidAuth = "";
}