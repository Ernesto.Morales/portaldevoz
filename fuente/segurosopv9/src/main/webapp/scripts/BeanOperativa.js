
// ******************************************* //
// ********** DATOS DE LA OPERATIVA ********** //
// ******************************************* //
function BeanOperativa() {	
	
	// etiqueta final en bruto con formato --> operativa#tipoMetadato#valorMetadato
	this.etiquetaRaw = "";
	//this.historicoEtiqueta = "";
	
	this.nombreOperativa = "";
		
	this.action = "";
	this.type = "";
	this.balance = "";
	this.folio = "";
	this.insurance = "";
	this.data = "";
}

// funcion para guardar una nueva operativa
function almacenaOperativa(operativas, nuevaOperativa) {
	
	if(operativas != ''){
		operativas = operativas + "|";
	}
	operativas = operativas + nuevaOperativa;
	
	return operativas;
}