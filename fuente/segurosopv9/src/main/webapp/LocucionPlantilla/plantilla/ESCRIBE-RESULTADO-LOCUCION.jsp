<?xml version="1.0" encoding="ISO-8859-1"?>

<%@page import="java.util.ArrayList"%>
<%@page import="com.kranon.logger.event.ParamEvent"%>
<%@page import="com.kranon.logger.serv.CommonLoggerService"%>
<%@page import="com.kranon.logger.monit.CommonLoggerMonitoring"%>

<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Collections"%>

<%@page import="org.apache.commons.lang.StringUtils"%>


<%
	
// ** PARAMETROS DE ENTRADA		
	
String idLlamada = request.getParameter("PRM_IN_LOC_idLlamada");
String idServicio = request.getParameter("PRM_IN_LOC_idServicio");
String idElemento = request.getParameter("PRM_IN_LOC_idElemento");

String resultadoOperacion = request.getParameter("PRM_OUT_LOC_resultadoOperacion");
String error = request.getParameter("PRM_OUT_LOC_error");
 
	
// ** PARAMETROS DE SALIDA
String resultadoOperacionEscribe = "";

%>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ESCRIBE-RESULTADO-LOCUCION
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<var name="SUB_resultadoOperacion" expr="''"/>
<var name="SUB_error" expr="''"/>

<!--
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
<!-- KRANON: Se captura cualquier error que pueda producirse, y se devuelve a la aplicacion un codigo de retorno ERROR.-->
<catch event="error">

	<log label="LOCUCION-RESULTADO"><value expr="'ERROR: ' + _event + ':'+_message"/></log>
	
	
	<assign name="SUB_resultadoOperacion" expr="'ERROR'"/>
	<assign name="SUB_error" expr="'ERROR_IVR(' +  _event + ':'+_message + ')'"/>

	
	<goto next="#FIN"/>

</catch>


<!--
******************************************
********** CAPTURA DEL CUELGUE ***********
******************************************
-->
<!-- KRANON: Se captura el cuelgue, y se devuelve a la aplicacion un codigo de retorno CUELGUE.-->
<catch event="connection.disconnect.hangup">

	<log label="LOCUCION-RESULTADO"><value expr="'HANGUP'"/></log>
	
	<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
	<assign name="SUB_error" expr="''"/>
	
	<goto next="#FIN"/>
	
</catch>




<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="INICIO">
	
	
		<block>				
			<log label="LOCUCION-RESULTADO"><value expr="'INICIO - LOCUCION-RESULTADO'"/></log>
			
			<log label="LOCUCION-RESULTADO"><value expr="'PRM_idInvocacion: <%=idLlamada%>'"/></log>
			<log label="LOCUCION-RESULTADO"><value expr="'PRM_idServicio: <%=idServicio%>'"/></log>
			<log label="LOCUCION-RESULTADO"><value expr="'PRM_idElemento: <%=idElemento%>'"/></log>
<%-- 			<log label="LOCUCION-RESULTADO"><value expr="'PRM_idMenu: <%=idMenu%>'"/></log> --%>
<%-- 			<log label="LOCUCION-RESULTADO"><value expr="'PRM_respuestaSensible: <%=respuestaSensible%>'"/></log> --%>
			<log label="LOCUCION-RESULTADO"><value expr="'PRM_OUT_LOC_resultadoOperacion: <%=resultadoOperacion%>'"/></log>
			<log label="LOCUCION-RESULTADO"><value expr="'PRM_OUT_LOC_error: <%=error%>'"/></log>
		<%
		
		
			CommonLoggerService log = null;
			try {	
				/** INICIALIZAR LOG **/
				log = new CommonLoggerService(idServicio);
				log.inicializar(idLlamada, idServicio, idElemento);
	
				/** INICIO EVENTO - INICIO DE MODULO **/
				//log.initModuleService(idMenu, null);
				/** FIN EVENTO - INICIO DE MODULO **/
				
				/** INICIO EVENTO - INICIO DE ACCION **/
				//log.actionEvent(idMenu, "INIT", null);
				/** FIN EVENTO - INICIO DE MODULO **/
				
				// variables para trza resumen
				String resultadoFinalMenu = "";
				String recDisponibleFinalMenu = "";
				String recUtilizadoFinalMenu = "";
				String respuestaFinalMenu = "";
				String respuestaRawFinalMenu = "";
				
				String tipo = request.getParameter("PRM_OUT_LOC_trazas.tipo");
				
				ArrayList<String> listaTrazas = new ArrayList<String>();
				
				Enumeration<?> enumeration = request.getParameterNames();
				while (enumeration.hasMoreElements()) {
					String parameterName = (String) enumeration.nextElement();
					if (parameterName.contains("PRM_OUT_LOC_trazas.locuciones.")) {
						listaTrazas.add(parameterName);
					}
				}
				
				// ordeno la lista de trazas
				Collections.sort(listaTrazas);
				
				int numLocucionActual = 0;
// 				int numElementoActual = 0;
				for(String parameterName: listaTrazas){
					
// 					log.comment("parameterName=" + parameterName);
			
					if (parameterName.contains("PRM_OUT_LOC_trazas.locuciones.")
							&& parameterName.contains(".valor")) {
					
						String cabeceraTraza = "PRM_OUT_LOC_trazas.locuciones." + numLocucionActual ;
											
						String modo = request.getParameter(cabeceraTraza + ".modo");
						String valor = request.getParameter(cabeceraTraza + ".valor");
						String silence = request.getParameter(cabeceraTraza + ".silence");
						String emphasis = request.getParameter(cabeceraTraza + ".emphasis");
						
// 						log.comment(numLocucionActual + " -> modo=" + modo);
// 						log.comment(numLocucionActual + " -> valor=" + valor);
// 						log.comment(numLocucionActual + " -> silence=" + silence);
// 						log.comment(numLocucionActual + " -> emphasis=" + emphasis);
						
						/** FIN EVENTO - LOCUCION **/
						ArrayList<ParamEvent> parametrosAdicionales = null;
						if(silence != null && !silence.equals("")){
							parametrosAdicionales = (parametrosAdicionales == null)? (new ArrayList<ParamEvent>()) : parametrosAdicionales;
							parametrosAdicionales.add(new ParamEvent("silence", silence));
						}
						if(emphasis != null && !emphasis.equals("")){
							parametrosAdicionales = (parametrosAdicionales == null)? (new ArrayList<ParamEvent>()) : parametrosAdicionales;
							parametrosAdicionales.add(new ParamEvent("emphasis", emphasis));
						}
						log.speechEvent(modo, valor, parametrosAdicionales);
						/** INICIO EVENTO - LOCUCION **/
						
						numLocucionActual = numLocucionActual + 1;
					}
				
				}
				
				
				resultadoOperacionEscribe = "OK";
				
			} catch (final Exception e) {
	
				if (log == null) {
					/** INICIALIZAR LOG **/
					log = new CommonLoggerService(idServicio);
					log.inicializar(idLlamada, idServicio, idElemento);
				}
				
				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				log.error(e.getMessage(),  "", parametrosAdicionales, e);
				/** FIN EVENTO - ERROR **/
	
				resultadoOperacionEscribe = "KO";

				
			}
		
		
		%>	
		
			<assign name="SUB_resultadoOperacion" expr="'<%=resultadoOperacionEscribe%>'"/>
			<assign name="SUB_error" expr="''"/>
			
			<goto next="#FIN"/>	
		
		</block>
	
	</form>

<!-- 
***********************************
********** FORM FIN ***************
***********************************
-->
	<form id="FIN">
	
		<block>							
			<log label="LOCUCION-RESULTADO"><value expr="'FIN - LOCUCION-RESULTADO'"/></log>
			
			<log label="LOCUCION-RESULTADO"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="LOCUCION-RESULTADO"><value expr="'SUB_error: ' + SUB_error"/></log>
			
			<return namelist="SUB_resultadoOperacion SUB_error"/>
			
		</block>
	
	</form>


</vxml>

