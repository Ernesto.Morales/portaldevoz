<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="com.kranon.logger.serv.CommonLoggerService"%>
<%@page import="com.kranon.logger.event.ParamEvent"%>
<%@page import="com.kranon.logger.monit.CommonLoggerMonitoring"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.kranon.modulo.BeanModulo"%>
<%@page import="com.kranon.singleton.SingletonModulo"%>
<%@page import="com.kranon.loccomunes.BeanLocucionesComunes"%>
<%@page import="com.kranon.singleton.SingletonLocucionesComunes"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Collections"%>
<%@page import="com.kranon.utilidades.UtilidadesBeans"%>
<%@page import="com.kranon.logger.logger.all.CommonLoggerKranon"%>
<%@page import="com.kranon.util.UtilidadesLoggerKranon"%>
<%@page import="java.util.List"%>


<%
	CommonLoggerKranon logKranon;
	List<String> listlogger = new ArrayList<String>();
	logKranon= new CommonLoggerKranon("KRANON-LOG","123");
	listlogger.add(UtilidadesLoggerKranon.mensaje);
	
	String idioma = (String) request.getParameter("PRM_IN_LOC_idioma");
	String idInvocacionLog = (String) request.getParameter("PRM_IN_LOC_idLlamada");
	logKranon.setIdServicio(idInvocacionLog);
	String idServicioLog = (String) request.getParameter("PRM_IN_LOC_idServicio");
	String idElementoLog = (String) request.getParameter("PRM_IN_LOC_idElemento");
	
	// tipo locucion a reproducir: {WAV, TTS, COMUN, MODULO}
	String tipoLocucion = (String) request.getParameter("PRM_IN_LOC_tipoLocucion");
	// nombre de las locuciones a reproducir desde el Modulo o Locuciones comunes
	String nombreLocuciones = (String) request.getParameter("PRM_IN_LOC_locuciones");
	listlogger.add(nombreLocuciones);
	// para locuciones de MODULO indico el identificador
	String idModulo = (String) request.getParameter("PRM_IN_LOC_idModulo");
	
	
	/** INICIALIZAR LOG **/
	CommonLoggerService log = new CommonLoggerService(idServicioLog);
	log.inicializar(idInvocacionLog, idServicioLog, idElementoLog);
	
	
	// array de variables a informar
	HashMap<String, String> tablaDatos = new HashMap<String, String>();
	
	Enumeration enumeration = request.getParameterNames();
	while (enumeration.hasMoreElements()) {
		String parameterName = (String) enumeration.nextElement();
		if (!parameterName.contains("PRM_IN_LOC_")) {
			// es un dato a informar
// 			listaTrazas.add(parameterName);
			String parameterValue = (String) request.getParameter(parameterName);
			// tablaDatos.put(parameterName, parameterValue);
			String parameterNameAux = parameterName;
			String parameterValueAux = parameterValue;
			
			
			// ** FORMATEA JSON
			String[] formatea = UtilidadesBeans.formateaJson(parameterNameAux);
			if (formatea[0].equals("true")) {
				// se ha formateado el json
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
				paramOld.add(new ParamEvent("parameterName", parameterName));
				log.actionEvent("FORMAT_JSON", "OLD", paramOld);
				/** FIN EVENTO - ACCION **/
				
				parameterNameAux = formatea[1];

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
				paramNew.add(new ParamEvent("parameterNameAux", parameterNameAux));
				log.actionEvent("FORMAT_JSON", "NEW", paramNew);
				/** FIN EVENTO - ACCION **/
			}
			// ** FORMATEA JSON
			formatea = UtilidadesBeans.formateaJson(parameterValueAux);
			if (formatea[0].equals("true")) {
				// se ha formateado el json
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
				paramOld.add(new ParamEvent("parameterValue", parameterValue));
				log.actionEvent("FORMAT_JSON", "OLD", paramOld);
				/** FIN EVENTO - ACCION **/
				
				parameterValueAux = formatea[1];

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
				paramNew.add(new ParamEvent("parameterValueAux", parameterValueAux));
				log.actionEvent("FORMAT_JSON", "NEW", paramNew);
				/** FIN EVENTO - ACCION **/
			}
			
			tablaDatos.put(parameterNameAux, parameterValueAux);
			
			
		}
	}
	request.setAttribute("tablaDatos", tablaDatos);
		
	
	
	
	log.comment("idioma=" + idioma);
	log.comment("nombreLocuciones=" + nombreLocuciones);
	log.comment("tipoLocucion=" + tipoLocucion);
	
	if(tipoLocucion.equalsIgnoreCase("MODULO")){
		//**** Recuperamos el Bean del Modulo en cuesti�n
		BeanModulo modulo = null;
		SingletonModulo instanceModulo = SingletonModulo.getInstance(log);
		if (instanceModulo != null) {
			modulo = instanceModulo.getModulo(idServicioLog, idModulo);
		}
	
// 		log.comment("M�dulo recuperado: " + modulo.toString());
		
		// A�ado par�metros de salida a la request
		request.setAttribute("modulo", modulo);
	}
	
	if(tipoLocucion.equalsIgnoreCase("COMUN")){
		// Recuperamos las locuciones comunes al servicio
		BeanLocucionesComunes locComunes = null;
		SingletonLocucionesComunes instanceLocComunes = SingletonLocucionesComunes.getInstance(log);
		if (instanceLocComunes != null) {
			locComunes = instanceLocComunes.getLocucionesComunes(idServicioLog);
		}
	
// 		log.comment("LocucionesComunes recuperadas: "+ locComunes.toString());
		
		// A�ado par�metros de salida a la request
		request.setAttribute("locComunes", locComunes);
	
	}
	
	request.setAttribute("idioma", idioma);
	request.setAttribute("idInvocacionLog", idInvocacionLog);
	request.setAttribute("idServicioLog", idServicioLog);
	request.setAttribute("idElementoLog", idElementoLog);
	request.setAttribute("nombreLocuciones", nombreLocuciones);
	request.setAttribute("tipoLocucion", tipoLocucion);
	listlogger.add(tipoLocucion);
// 	listlogger.add("idioma: "+idioma);
// 	listlogger.add("idInvocacionLog: "+idInvocacionLog);
// 	listlogger.add("nombreLocuciones: "+ nombreLocuciones);
// 	listlogger.add("tipoLocucion: "+tipoLocucion);
	logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));

%>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${idioma}" >


<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	LOCUCION-PLANTILLA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
<meta http-equiv="Expires" content="0"/>

<script src="../scripts/LoggerLocucion.js"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="PRM_IN_LOC_idioma" expr="'${idioma}'"/>
<var name="PRM_IN_LOC_idLlamada" expr="'${idInvocacionLog}'"/>
<var name="PRM_IN_LOC_idServicio" expr="'${idServicioLog}'"/>
<var name="PRM_IN_LOC_idElemento" expr="'${idElementoLog}'"/>
<var name="PRM_IN_LOC_idModulo" expr="'${idModulo}'"/>
<var name="PRM_IN_LOC_locuciones" expr="'${nombreLocuciones}'"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="'${tipoLocucion}'"/>

<!-- Parametro de entrada al subdialogo -->
<var name="SUB_textoTTS" />


<!-- KRANON: Definicion de parametros de retorno del subdialogo -->
<var name="PRM_OUT_LOC_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_LOC_error" expr="''"/>

<var name="PRM_OUT_LOC_trazas" expr="''"/>

<!-- KRANON: Definicion de variables a leer con uso -->
<c:forEach var="entry" items="${tablaDatos}">  
  	<var name="${entry.key}" expr="'${entry.value}'"/>
</c:forEach>


<!--
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
<!-- KRANON: Se captura cualquier error que pueda producirse, y se devuelve a la aplicacion un codigo de retorno ERROR.-->
<catch event="error">
	<log label="LOCUCION-PLANTILLA"><value expr="'-- CATCH ERROR LOCUCION-PLANTILLA --'"/></log>
	<assign name="PRM_OUT_LOC_resultadoOperacion" expr="'ERROR'"/>
	<assign name="PRM_OUT_LOC_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
	<!-- <goto next="#ESCRIBE_RESULTADO_LOCUCION"/> -->
	<goto next="#RETORNO" />
</catch>


<!--
******************************************
********** CAPTURA DEL CUELGUE ***********
******************************************
-->
<!-- KRANON: Se captura el cuelgue, y se devuelve a la aplicacion un codigo de retorno CUELGUE.-->
<catch event="connection.disconnect.hangup">
	<log label="LOCUCION-PLANTILLA"><value expr="'-- CATCH HANGUP LOCUCION-PLANTILLA --'"/></log>
	<assign name="PRM_OUT_LOC_resultadoOperacion" expr="'HANGUP'"/>
	<assign name="PRM_OUT_LOC_error" expr="''"/>
<!-- 	<goto next="#ESCRIBE_RESULTADO_LOCUCION"/> -->
	<goto next="#RETORNO" />
</catch>

<!--
********************************************
********** FORMULARIO INICIAL **************
********************************************
-->
<form id="LOCUCION_PLANTILLA">

	<var name="PRM_IN_LOC_textoTTS" /> 
	
	<block>	
	
		<log label="LOCUCION-PLANTILLA"><value expr="'-- INICIO LOCUCION-PLANTILLA --'"/></log>
		
			<assign name="SUB_textoTTS" expr="PRM_IN_LOC_textoTTS"/>
			
			<log label="LOCUCION-PLANTILLA"><value expr="'SUB_textoTTS=' + SUB_textoTTS"/></log>
			
		
			<!-- TRAZAS DE LA LOCUCION -->
			<script>
				if(PRM_OUT_LOC_trazas == null || PRM_OUT_LOC_trazas == ''){
					PRM_OUT_LOC_trazas = new LoggerLocucion();		
				}
				
				PRM_OUT_LOC_trazas.tipo = '${tipoLocucion}';
				
				if(PRM_OUT_LOC_trazas.locuciones == null){
					PRM_OUT_LOC_trazas.locuciones = new Array();
				}
			</script>
			<!-- TRAZAS DE LA LOCUCION -->

		<log label="LOCUCION-PLANTILLA"><value expr="'-- FIN CREAR SCRIPT trazas --'"/></log>

		<!--
		********************************************
		********** LOCUCIONES COMUNES **************
		********************************************
		-->
		<c:if test="${tipoLocucion == 'COMUN' || tipoLocucion == 'comun'}">						
			<c:forTokens items="${nombreLocuciones}" delims="|" var="nombreLoc">	
				<log label="LOCUCION-PLANTILLA"><value expr="'locucion a reproducir: ${nombreLoc}'"/></log>
				
				<prompt bargein="false">
					<c:forEach items="${locComunes.getLocucionesByNombre(nombreLoc)}" var="loc">		
						<c:if test="${loc.uso == null || loc.uso == ''}">				
							<c:choose>
							    <c:when test="${loc.modo != null && loc.modo == 'WAV'}">
							    	<!-- es WAV -->
							       	<audio expr="'${loc.value}'"/>
							    </c:when>
							    <c:otherwise>
							    	<!-- es TTS -->
							       	<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <value expr="'${loc.value}'"/> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<value expr="'${loc.value}'"/>
							    		</c:otherwise>
							       	</c:choose>						       	
							    </c:otherwise>
							</c:choose>		
						</c:if>
						<c:if test="${loc.uso != null && loc.uso != ''}">
							<c:choose>
							    <c:when test="${loc.uso == 'DIGITOS' || loc.uso == 'DIGITS'}">
									<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <say-as interpret-as="vxml:digits"><value expr="${loc.value}"/></say-as> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<say-as interpret-as="vxml:digits"><value expr="${loc.value}"/></say-as>
							    		</c:otherwise>
							       	</c:choose>		
							    </c:when>
							    <c:when test="${loc.uso == 'NORMAL'}">
									<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <value expr="${loc.value}"/> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<value expr="${loc.value}"/>
							    		</c:otherwise>
							       	</c:choose>								     
							    </c:when>
							    <c:otherwise>
							     	<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <say-as interpret-as="vxml:<c:out value="${loc.value}"/>"><value expr="${loc.value}"/></say-as> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<say-as interpret-as="vxml:<c:out value="${loc.value}"/>"><value expr="${loc.value}"/></say-as>
							    		</c:otherwise>
							       	</c:choose>		
							    </c:otherwise>
							</c:choose>										
						</c:if>
						<c:if test="${loc.silence != null && loc.silence != ''}">
							<break time="<c:out value="${loc.silence}"/>" />
						</c:if>							
					</c:forEach>
				</prompt>
				
				<!-- TRAZAS LOCUCION -->
				<c:forEach items="${locComunes.getLocucionesByNombre(nombreLoc)}" var="loc">						
					<c:if test="${loc.uso == null || loc.uso == ''}">	
							<!-- TRAZAS DE LA LOCUCION SIN USO -->
							<script>
								var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
								PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${loc.modo}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = '${loc.valueFormateado}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '${loc.silence}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '${loc.emphasis}';
							</script>
							<!-- TRAZAS DE LA LOCUCION SIN USO-->
						</c:if>
						
						<c:if test="${loc.uso != null && loc.uso != ''}">
							<!-- TRAZAS DE LA LOCUCION CON USO -->
							<script>
								var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
								PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${loc.modo}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = ${loc.valueFormateado};
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '${loc.silence}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '${loc.emphasis}';
							</script>
							<!-- TRAZAS DE LA LOCUCION CON USO-->
						</c:if>
				</c:forEach>
				<!-- TRAZAS LOCUCION -->
				
			</c:forTokens>
		
		</c:if>
		
		<!--
		********************************************
		********** LOCUCIONES MODULO ***************
		********************************************
		-->
		<c:if test="${tipoLocucion == 'MODULO' || tipoLocucion == 'modulo'}">	
			<c:forTokens items="${nombreLocuciones}" delims="|" var="nombreLoc">	
				<log label="LOCUCION-PLANTILLA"><value expr="'locucion a reproducir: ${nombreLoc}'"/></log>
				
				<prompt bargein="false">
					<c:forEach items="${modulo.getLocucionesByNombre(nombreLoc)}" var="loc">		
						<c:if test="${loc.uso == null || loc.uso == ''}">				
							<c:choose>
							    <c:when test="${loc.modo != null && loc.modo == 'WAV'}">
							    	<!-- es WAV -->
							       	<audio expr="'${loc.value}'"/>
							    </c:when>
							    <c:otherwise>
							    	<!-- es TTS -->
							       	<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <value expr="'${loc.value}'"/> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<value expr="'${loc.value}'"/>
							    		</c:otherwise>
							       	</c:choose>						       	
							    </c:otherwise>
							</c:choose>		
						</c:if>
						<c:if test="${loc.uso != null && loc.uso != ''}">
							<c:choose>
							    <c:when test="${loc.uso == 'DIGITOS' || loc.uso == 'DIGITS'}">
									<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <say-as interpret-as="vxml:digits"><value expr="${loc.value}"/></say-as> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<say-as interpret-as="vxml:digits"><value expr="${loc.value}"/></say-as>
							    		</c:otherwise>
							       	</c:choose>		
							    </c:when>
							    <c:when test="${loc.uso == 'NORMAL'}">
									<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <value expr="${loc.value}"/> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<value expr="${loc.value}"/>
							    		</c:otherwise>
							       	</c:choose>								     
							    </c:when>
							    <c:otherwise>
							     	<c:choose>
							       		<c:when test="${loc.emphasis != null && (loc.emphasis == 'strong' || loc.emphasis == 'moderate' || loc.emphasis == 'reduced')}">
							       			<emphasis level="<c:out value="${loc.emphasis}"/>"> <say-as interpret-as="vxml:<c:out value="${loc.value}"/>"><value expr="${loc.value}"/></say-as> </emphasis>
							       		</c:when>
							    		<c:otherwise>
							    			<say-as interpret-as="vxml:<c:out value="${loc.value}"/>"><value expr="${loc.value}"/></say-as>
							    		</c:otherwise>
							       	</c:choose>		
							    </c:otherwise>
							</c:choose>										
						</c:if>
						<c:if test="${loc.silence != null && loc.silence != ''}">
							<break time="<c:out value="${loc.silence}"/>" />
						</c:if>							
					</c:forEach>
				</prompt>
				
				<!-- TRAZAS LOCUCION -->
				<c:forEach items="${modulo.getLocucionesByNombre(nombreLoc)}" var="loc">						
					<c:if test="${loc.uso == null || loc.uso == ''}">	
							<!-- TRAZAS DE LA LOCUCION SIN USO -->
							<script>
								var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
								PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${loc.modo}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = '${loc.valueFormateado}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '${loc.silence}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '${loc.emphasis}';
							</script>
							<!-- TRAZAS DE LA LOCUCION SIN USO-->
						</c:if>
						
						<c:if test="${loc.uso != null && loc.uso != ''}">
							<!-- TRAZAS DE LA LOCUCION CON USO -->
							<script>
								var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
								PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${loc.modo}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = ${loc.valueFormateado};
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '${loc.silence}';
								PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '${loc.emphasis}';
							</script>
							<!-- TRAZAS DE LA LOCUCION CON USO-->
						</c:if>
				</c:forEach>
				<!-- TRAZAS LOCUCION -->
				
			</c:forTokens>
		
		</c:if>


		<!--
		**********************************************
		********** LOCUCIONES NORMALES TTS ***********
		**********************************************
		-->
		<c:if test="${tipoLocucion == 'TTS' || tipoLocucion == 'tts'}">	
			<prompt bargein="false">
				<value expr="SUB_textoTTS"/> 
			</prompt>
			
			<!-- TRAZAS DE LA LOCUCION TTS -->
			<script>
				var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
				PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${tipoLocucion}';
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = SUB_textoTTS;
				
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '';
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '';
			</script>
<%-- 			<%listlogger.add("SUB_textoTTS"+SUB_textoTTS); --%>
<!-- 			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger)); -->
<!-- 			%> -->
			
			<!-- TRAZAS DE LA LOCUCION TTS -->
		</c:if>
		
		
		<!--
		**********************************************
		********** LOCUCIONES NORMALES WAV ***********
		**********************************************
		-->		
		<c:if test="${tipoLocucion == 'WAV' || tipoLocucion == 'wav'}">	
			<prompt bargein="false">
				<audio expr="'${nombreLocuciones}'"/>
			</prompt>
			
			<!-- TRAZAS DE LA LOCUCION WAV -->
			<script>
				var longitudLoc = PRM_OUT_LOC_trazas.locuciones.length;
				PRM_OUT_LOC_trazas.locuciones[longitudLoc] = new Locucion();
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].modo = '${tipoLocucion}';
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].valor = '${nombreLocuciones}';
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].silence = '';
				PRM_OUT_LOC_trazas.locuciones[longitudLoc].emphasis = '';
			</script>
			<!-- TRAZAS DE LA LOCUCION WAV -->
		</c:if>



		<assign name="PRM_OUT_LOC_resultadoOperacion" expr="'OK'"/>
		<assign name="PRM_OUT_LOC_error" expr="''"/>

<!-- 		<goto next="#RETORNO" /> -->
		<goto next="#ESCRIBE_RESULTADO_LOCUCION" />

	</block>
</form>	



<!-- 
*****************************************************
******* ESCRIBE TRAZAS DE LA LOCUCI�N ***************
*****************************************************
-->
<form id="ESCRIBE_RESULTADO_LOCUCION">

	<block>
		<log label="LOCUCION-PLANTILLA"><value expr="'VOY A ESCRIBIR EL RESULTADO'"/></log>
				
	</block>
	
	<subdialog name="escribeResultadoLocucion" fetchhint="safe" method="post" 
		src="../plantilla/ESCRIBE-RESULTADO-LOCUCION.jsp"
		namelist="PRM_OUT_LOC_trazas 
			PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento 
			PRM_OUT_LOC_resultadoOperacion PRM_OUT_LOC_error">

		<filled>
			
			<log label="LOCUCION-PLANTILLA"><value expr="'VUELVO A LA PLANTILLA'"/></log>
			
			<if cond="escribeResultadoLocucion.SUB_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="PRM_OUT_LOC_resultadoOperacion" expr="'HANGUP'"/>
				<assign name="PRM_OUT_LOC_error" expr="''"/>
			</if>
			
			
			
			<!-- ABALFARO_BORRAR -->
			<if cond="escribeResultadoLocucion.SUB_resultadoOperacion != 'OK'">
				<prompt bargein="false">
					<value expr="'Tras escribir el resultado operacion se ha producido un ' + escribeResultadoLocucion.SUB_resultadoOperacion"/>
				</prompt>
			</if>
			<!-- ABALFARO_BORRAR -->
			
			<goto next="#RETORNO" />					
		</filled>
	</subdialog>
</form>

<!-- 
**************************************
************** RETORNO  **************
**************************************
-->
<form id="RETORNO">
	<block>
		<log label="LOCUCION-PLANTILLA"><value expr="'PRM_OUT_LOC_resultadoOperacion: ' + PRM_OUT_LOC_resultadoOperacion"/></log>
		<log label="LOCUCION-PLANTILLA"><value expr="'PRM_OUT_LOC_error: ' + PRM_OUT_LOC_error"/></log>		
		<log label="LOCUCION-PLANTILLA"><value expr="'-- FIN LOCUCION-PLANTILLA --'"/></log>
		
		<!-- ABALFARO_BORRAR -->
<!-- 		<if cond="PRM_OUT_LOC_resultadoOperacion != 'OK'"> -->
<!-- 			<prompt bargein="false"> -->
<!-- 				<value expr="'Fin de plantilla locucion con resultado ' + PRM_OUT_LOC_resultadoOperacion"/> -->
<!-- 			</prompt> -->
<!-- 		</if> -->
		<!-- ABALFARO_BORRAR -->
		
		<return namelist="PRM_OUT_LOC_resultadoOperacion PRM_OUT_LOC_error"/>
	</block>
</form>


</vxml>