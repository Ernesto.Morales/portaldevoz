<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ZonaPagoDatos/staticvxml/MODULO-ZPDATOS-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-ZP-DATOS
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="PRM_indiceIntentosMENU" expr="1"/>
<var name="PRM_loggerServicio" expr="''"/>

<!-- Para recorrer el string con la informacion de los candados -->
<var name="PRM_indiceCandado" expr="''"/>
<var name="PRM_indiceIntentos" expr="''"/>

<!-- Candado actual: identificador + intentos -->
<var name="PRM_candadoActual" expr="''"/>
<var name="PRM_candado" expr="''"/>
<var name="PRM_intentosCandado" expr="''"/>

<!-- Para el saldo automatico -->
<var name="PRM_campanas" expr="''"/>
<var name="PRM_AUXTAR" expr="''"/>
<var name="PRM_errorloc" expr="''"/>
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_info" expr="''"/>

<var name="PRM_estaEnListaNegra" expr="''"/>
<var name="PRM_aniConfiable" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<script>
  	VG_candados = new Array();
  	VG_intentosCandados = new Array();
</script>



<form id="MODULO_MODULO-ZP-DATOS_EJECUTAR">	
	<block>
		<log label="INICIO-EJECUTAR"><value expr="'VG_cliente.nombreCliente: ' + VG_cliente.nombreCliente"/></log>
		
		<log label="MODULO-ZDDATOS"><value expr="'tipoIdentificacion: ${tipoIdentificacion}'"/></log>	
		<log label="MODULO-ZDDATOS"><value expr="'intentosIdentificacion: ${intentosIdentificacion}'"/></log>
		
		<assign name="PRM_indiceCandado" expr="0"/>
		<assign name="PRM_indiceIntentos" expr="0"/>
		<assign name="PRM_candadoActual" expr="0"/>
		<assign name="PRM_info" expr="'TIKO'"/>
<!-- 		<goto next="#OBTENER_CAMPANA"/>	 -->
		<goto next="#CONFIRMAR-MONTO"/>
	</block>
</form>

<form id="CONFIRMAR-MONTO">	


	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPConfirmaMonto" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado VG_DatosCotizacion">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>		
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.tarjeta" expr="subIdentifTarjeta.PRM_numTarjetaIntroducido"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
			
<!-- 		<prompt> -->
<!-- 		datosososos menu -->
<!-- 			<value expr="PRM_info"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">	
				<assign name="PRM_info" expr="'MIOK'"/>			
				<!-- Ha ido bien la identificacion -->
				<goto next="#IDENTIF_TARJETA"/>		
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'MI01'"/>	
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'MI01'"/>		
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
						<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#FIN"/>
				</if>
			<assign name="PRM_info" expr="'MI00'"/>	
				<goto next="#FIN"/>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="IDENTIF_TARJETA">	


	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.tarjeta" expr="subIdentifTarjeta.PRM_numTarjetaIntroducido"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
<!-- 		<prompt> -->
<!-- 		datosososos TARJETA -->
<!-- 			<value expr="VG_DatosCotizacion.tarjeta"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">			
				<!-- Ha ido bien la identificacion -->
				<script>
						var tarjeta = '';
						var opcion;
						tarjeta=subIdentifTarjeta.PRM_numTarjetaIntroducido;
						opcion=tarjeta.length;
						PRM_AUXTAR=opcion;
				</script>
						
				<if cond="PRM_AUXTAR == '15'">
					<log label="MODULO-ZDDATOS"><value expr="'AMEX'"/></log>
					<assign name="PRM_AUXTAR" expr="'ID-CLI-SOLICITUD-CVVTARJETA-AMEX'"/>
					<goto next="#OBTENER_CAMPANA"/>
				<else/>
					<log label="MODULO-ZDDATOS"><value expr="'NO AMEX'"/></log>
					<assign name="PRM_AUXTAR" expr="'ID-CLI-SOLICITUD-CVVTARJETA'"/>
					<goto next="#OBTENER_CAMPANA"/>
				</if>
					
				<if cond="PRM_codigoRetorno == 'OK'">
					<!-- el cliente se ha identificado -->
						
						<!-- guardo el instrumento de identificacion para la cadena cti/uui -->
<!-- 						<assign name="VG_llaveAccesoFront" expr="VG_cliente.tarjetas[VG_cliente.tarjetas.length - 1].numeroTarjeta"/> -->
				
					<!-- Continuo en el bucle por si hay mas candados a solicitar -->
<!-- 					<goto next="#TIPO_CANDADO"/> -->
					<goto next="#RESULTADO_OK"/>
					
				<else/>
					<!-- el cliente no se identifico OK -->
					<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#RESULTADO_KO"/>
				</if>
			
			
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'TI02'"/>	
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'TI01'"/>		
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#FIN"/>
				</if>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="OBTENER_CAMPANA">	


	<subdialog name="subObeterCA" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosCObtenerCampana" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado VG_DatosCotizacion">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_indiceIntentosMENU"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subObeterCA.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subObeterCA.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subObeterCA.PRM_error"/>
			<assign name="PRM_campanas" expr="subObeterCA.PRM_AUX_camapanas"/>
			<assign name="PRM_errorloc" expr="subObeterCA.PRM_errorloc"/>
			<assign name="PRM_indiceIntentosMENU" expr="PRM_indiceIntentosMENU + 1"/>
<!-- 		<prompt> -->
<!-- 		datO -->
<!-- 			<value expr="PRM_resultadoOperacion"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">	
				<assign name="VG_DatosCotizacion.afiliacionId" expr="subObeterCA.PRM_AUX_IdAfiliacion"/>		
				<!-- Ha ido bien la identificacion -->
				<assign name="PRM_info" expr="'CAOK'"/>	
				<goto next="#IDENTIF_TARJETA_MES"/>		
			<elseif cond="PRM_error == 'ERRORC'" />
				<assign name="PRM_error" expr="'ERROR'"/>	
				<log label="MODULO-ZDDATOS"><value expr="'ok'"/></log>
				<goto next="#IDENTIF_TARJETA"/>
			<elseif cond="PRM_resultadoOperacion == 'KO'" />
					<goto next="#RESULTADO_KO"/>
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
				<assign name="PRM_info" expr="'TIKO'"/>	
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<goto next="#FIN"/>
				</if>
				<goto next="#FIN"/>
			</if>	
		</filled>
	</subdialog>

</form>




<form id="IDENTIF_TARJETA_MES">	

	<subdialog name="subIdentifMESTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosMesTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifMESTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifMESTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifMESTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.mestarjeta" expr="subIdentifMESTarjeta.PRM_MESTarjetaIntroducido"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifMESTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifMESTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifMESTarjeta.PRM_errorloc"/>
<!-- 		<prompt> -->
<!-- 		datosososos MES -->
<!-- 			<value expr="VG_DatosCotizacion.mestarjeta"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">			
				<!-- Ha ido bien la identificacion -->		
				<goto next="#IDENTIF_TARJETA_ANO"/>
<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, VG_cliente.segmento)"/> -->
				
				<log label="MODULO-ZDDATOS"><value expr="'ok'"/></log>
					
				<if cond="PRM_codigoRetorno == 'OK'">
					<!-- el cliente se ha identificado -->
						
						<!-- guardo el instrumento de identificacion para la cadena cti/uui -->
<!-- 						<assign name="VG_llaveAccesoFront" expr="VG_cliente.tarjetas[VG_cliente.tarjetas.length - 1].numeroTarjeta"/> -->
				
					<!-- Continuo en el bucle por si hay mas candados a solicitar -->
<!-- 					<goto next="#TIPO_CANDADO"/> -->
					<goto next="#RESULTADO_OK"/>
					
				<else/>
					<!-- el cliente no se identifico OK -->
					<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#RESULTADO_KO"/>
				</if>
			
			
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />
			<assign name="PRM_info" expr="'TI02'"/>		
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
				<assign name="PRM_info" expr="'TI01'"/>	
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#FIN"/>
				</if>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="IDENTIF_TARJETA_ANO">	
	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosAnoTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.anotarjeta" expr="subIdentifTarjeta.PRM_AnoTarjetaIntroducido"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
<!-- 		<prompt> -->
<!-- 		datosososos TARJETA -->
<!-- 			<value expr="VG_DatosCotizacion.anotarjeta"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">			
				<!-- Ha ido bien la identificacion -->
				<goto next="#IDENTIF_TARJETA_CCV"/>
<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, VG_cliente.segmento)"/> -->
							
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
				<assign name="PRM_info" expr="'TI02'"/>	
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
				<assign name="PRM_info" expr="'TI01'"/>	
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<assign name="PRM_info" expr="'TIKO'"/>	
					<goto next="#FIN"/>
				</if>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="IDENTIF_TARJETA_CCV">	

	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosCCVTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		<param name="PRM_IN_menu" expr="PRM_AUXTAR"/>
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.ccv" expr="subIdentifTarjeta.PRM_CCVTarjetaIntroducido"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
<!-- 		<prompt> -->
<!-- 			<value expr="VG_DatosCotizacion.ccv"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">	
				<assign name="PRM_info" expr="'TIOK'"/>	
				<log label="MODULO-ZDDATOS"><value expr="'ok CCVTARJETA'"/></log>	
				<!-- Ha ido bien la identificacion -->
				<goto next="#SELECCIONAR_CAMPANA"/>
<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, VG_cliente.segmento)"/> -->
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
				<assign name="PRM_info" expr="'TI02'"/>	
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
				<assign name="PRM_info" expr="'TI01'"/>	
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
				<assign name="PRM_info" expr="'TIKO'"/>	
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<goto next="#FIN"/>
				</if>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="RESULTADO_OK">	
	<block>	
			
		<log label="MODULO-ZDDATOS"><value expr="'El cliente SI ha sido Identificado'"/></log>
<!-- 		<assign name="VG_cliente.isIdentificado" expr="'true'"/> -->
		<assign name="PRM_codigoRetorno" expr="'OK'"/>
		<assign name="PRM_resultadoOperacion" expr="'OK'"/>
		<assign name="PRM_error" expr="''"/>
		<assign name="PRM_OUT_ODPA_isIdentificado" expr="'true'"/>
		
		<goto next="#FIN"/>
	</block>
</form>

<form id="RESULTADO_KO">	
	<block>	
	<if cond="PRM_resultadoOperacion == 'MAXINT_MENU'" >	
			<assign name="PRM_codigoRetorno" expr="'KO'"/>	
	<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
			<assign name="PRM_codigoRetorno" expr="'KO'"/>				
			<else/>	
			<log label="MODULO-ZDDATOS"><value expr="'El cliente NO ha sido Identificado'"/></log>
	</if>	
<!-- 			<assign name="PRM_resultadoOperacion" expr="'KO'"/> -->
		<log label="MODULO-ZDDATOS"><value expr="'El cliente NO ha sido Identificado'"/></log>
<!-- 		<assign name="VG_cliente.isIdentificado" expr="'false'"/> -->
		<assign name="PRM_OUT_ODPA_isIdentificado" expr="'false'"/>
		
		<goto next="#FIN"/>
		
	</block>
	
</form>



<form id="SELECCIONAR_CAMPANA">	


	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/subZPDatosSelecCampana" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado VG_DatosCotizacion PRM_campanas">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-ZDDATOS"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion.campana" expr="subIdentifTarjeta.PRM_Campana"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
<!-- 			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/> -->
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
<!-- 		<prompt> -->
<!-- 		datO -->
<!-- 			<value expr="PRM_resultadoOperacion"></value> -->
<!-- 		</prompt> -->
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">			
				<!-- Ha ido bien la identificacion -->
				<assign name="PRM_info" expr="'CAOK'"/>	
<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, VG_cliente.segmento)"/> -->
				<log label="MODULO-ZDDATOS"><value expr="'ok'"/></log>
				<goto next="#RESULTADO_OK"/>
							
			<elseif cond="PRM_resultadoOperacion == 'KO'" />
				<assign name="PRM_info" expr="'CI00'"/>	
				<goto next="#RESULTADO_KO"/>
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'CI00'"/>		
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
			
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-ZDDATOS"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
				<assign name="PRM_info" expr="'CI00'"/>		
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
				<assign name="PRM_info" expr="'TIKO'"/>	
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<goto next="#FIN"/>
				</if>
				<goto next="#FIN"/>
			</if>	
		</filled>
	</subdialog>

</form>








<form id="FIN">	
	<block>	
<!-- 		<prompt> -->
<!-- 		FIn -->
<!-- 			<value expr="PRM_info"></value> -->
<!-- 		</prompt> -->
		<assign name="PRM_OUT_ODPA_codigoRetorno" expr="PRM_codigoRetorno"/>
		<assign name="PRM_OUT_ODPA_resultadoOperacion" expr="PRM_resultadoOperacion"/>
		<assign name="PRM_OUT_ODPA_error" expr="PRM_error"/>	
		<assign name="PRM_OUT_ODPA_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<assign name="PRM_OUT_ODPA_info" expr="PRM_info"/>
		
<!-- 		<assign name="PRM_OUT_ODPA_gestionExcepciones" expr="VG_gestionExcepciones"/> -->
		
		<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/finModuloZPDatos" method="post"  
				namelist="VG_loggerServicio PRM_OUT_ODPA_codigoRetorno PRM_OUT_ODPA_resultadoOperacion PRM_OUT_ODPA_error
						PRM_OUT_ODPA_DatosCotizacion VG_stat"/>
										
	</block>
	
</form>


</vxml>
