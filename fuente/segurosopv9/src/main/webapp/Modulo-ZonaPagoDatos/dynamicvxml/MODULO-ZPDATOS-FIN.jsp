<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ZonaPagoDatos/staticvxml/MODULO-ZPDATOS-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-ZONA-PAGO-FIN
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="verificaDigitos" expr="''"/>
<form id="MODULO_IDENTIFICACION_FIN">

	
	<block>		
<!-- 	<exit/>	 -->
		<log label="MODULO-IDENTIFICACION"><value expr="'INICIO - MODULO-ZONA-PAGO-FIN'"/></log>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_ODPA_resultadoOperacion: ' + PRM_OUT_ODPA_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_ODPA_codigoRetorno: ' + PRM_OUT_ODPA_codigoRetorno"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_ODPA_error: ' + PRM_OUT_ODPA_error"/></log>

		<log label="MODULO-IDENTIFICACION"><value expr="'FIN - MODULO-ZONA-PAGO-FIN'"/></log>
					
		<!-- ABALFARO_20170217 devuelvo variable stat-->
		<assign name="PRM_OUT_ODPA_stat" expr="VG_stat"/>
		<return namelist="PRM_OUT_ODPA_codigoRetorno PRM_OUT_ODPA_resultadoOperacion PRM_OUT_ODPA_error
						PRM_OUT_ODPA_DatosCotizacion PRM_OUT_ODPA_stat PRM_OUT_ODPA_info" />
		
	</block>

</form>

</vxml>
