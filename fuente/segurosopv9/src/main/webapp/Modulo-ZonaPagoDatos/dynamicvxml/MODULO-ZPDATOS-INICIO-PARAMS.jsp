<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ZonaPagoDatos/staticvxml/MODULO-ZPDATOS-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  ZPAGODATOSICADOR:	MODULO-ZONAPAGODATOS-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>



<form id="MODULO_ZPAGODATOS_INICIO_PARAMS">

	<var name="PRM_IN_ZPDATOS_loggerServicio"/>
<!-- 	<var name="PRM_IN_ZPDATOS_cliente"/> -->
	
	<var name="PRM_IN_ZPDATOS_DatosCotizacion"/>
<!-- 	<var name="PRM_IN_ZPDATOS_accionMaxintValid"/> -->
	
	<!-- ABALFARO_20170217 -->
	<var name="PRM_IN_ZPDATOS_stat"/>
	
	<block>			
		<log label="MODULO-ZPAGODATOS"><value expr="'INICIO - MODULO-ZPAGODATOS-INICIO-PARAMS'"/></log>

		<assign name="VG_loggerServicio" expr="PRM_IN_ZPDATOS_loggerServicio"/>
		<assign name="VG_DatosCotizacion" expr="PRM_IN_ZPDATOS_DatosCotizacion"/>
		
		<assign name="VG_loggerServicio.idElemento" expr="'${idElemento}'"/>
		
<!-- 		<assign name="VG_accionMaxintMenu" expr="PRM_IN_ZPDATOS_accionMaxintMenu"/>	 -->
<!-- 		<assign name="VG_accionMaxintValid" expr="PRM_IN_ZPDATOS_accionMaxintValid"/> -->
		
<!-- 		<assign name="VG_gestionExcepciones" expr="PRM_IN_ZPDATOS_gestionExcepciones"/> -->
		
		<!-- ABALFARO_20170217 -->
		<assign name="VG_stat" expr="PRM_IN_ZPDATOS_stat"/>	
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
<!-- 		<assign name="VG_controlador" expr="PRM_IN_ZPDATOS_controlador"/>	 -->
		
			
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		
		<log label="MODULO-ZPAGODATOS"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-ZPAGODATOS"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="MODULO-ZPAGODATOS"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
			
			
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_ZPAGODATOSICACION_' + VG_loggerServicio.op, 'INIT')"/> 
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- ***** STAT: inicio operacion ***** -->
		<assign name="VG_stat" expr="inicioOperacion(VG_stat, '${milisecInicioOperacion}', 'MODULO_ZPAGODATOSICACION_' + VG_loggerServicio.op)"/> 
		<!-- ***** STAT: inicio operacion ***** -->			
		
		<if cond="PRM_resultadoOperacion == 'OK'">
		
			<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/ejecutarModuloZPDatos" method="post"  
				namelist="VG_loggerServicio"/>
		
		<else/>
				
			<log label="MODULO-ZPAGODATOS"><value expr="'FIN - MODULO-ZPAGODATOSICACION-INICIO-PARAMS'"/></log>
		
			<assign name="PRM_OUT_ODPA_codigoRetorno" expr="VG_codigoRetorno"/>
			<assign name="PRM_OUT_ODPA_resultadoOperacion" expr="PRM_resultadoOperacion"/>
			<assign name="PRM_OUT_ODPA_error" expr="PRM_error"/>
			<assign name="PRM_OUT_ODPA_isIdentificado" expr="'false'"/>
			<assign name="PRM_OUT_ODPA_ramaIdentificacion" expr="''"/>
<!-- 			<assign name="PRM_OUT_ODPA_cliente" expr="VG_cliente"/> -->
<!-- 			<assign name="PRM_OUT_ODPA_llaveAccesoFront" expr="VG_llaveAccesoFront"/>	 -->
				
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_ZPAGODATOSICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_ZPAGODATOSICADO';
				</script>
			<assign name="PRM_OUT_ODPA_gestionExcepciones" expr="VG_gestionExcepciones"/>	
							
			<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/finModuloZPDatos" method="post"  
				namelist="VG_loggerServicio PRM_OUT_ODPA_codigoRetorno PRM_OUT_ODPA_resultadoOperacion PRM_OUT_ODPA_error
						PRM_OUT_ODPA_isIdentificado  PRM_OUT_ODPA_ramaIdentificacion
						PRM_OUT_ODPA_info PRM_OUT_ODPA_gestionExcepciones VG_stat"/>
						
		</if>
	
		
	</block>

</form>



</vxml>
