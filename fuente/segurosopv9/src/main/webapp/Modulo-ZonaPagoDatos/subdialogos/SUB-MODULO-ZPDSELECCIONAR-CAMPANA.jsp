<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Properties"%>
<%@page import="java.util.Vector"%>
<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-OBTENER-CAMPANA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>

<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>
<!-- COTIZACION -->
<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>

<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_cliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!-- Numero de intentos para introducir la tarjeta -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>

<var name="idLocucionDeAcuerdo"/>

<!-- guardo el numero de tarjeta si lo introduce el cliente para identificarse -->
<var name="PRM_Campana" expr="''"/>

<!-- Nos indica que tipo de identificacion tenemos que lanzar primero (AUTO: LoanDocuments, HIPOTECA: LoanBalance) -->
<var name="PRM_candado" expr="''"/>

<!-- Nos dice si el cliente se ha identificado como AUTO o como HIPOTECA -->
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_tipoProducto" expr="''"/>

<!-- para indicar el la salida lo que ha ocurrido {MAXINT_MENU, MAXINT_VALID} -->
<var name="PRM_info" expr="''"/>

<var name="PRM_bloqueado" expr="''"/>
<var name="PRM_nombreCliente" expr="''"/>
<var name="PRM_momentoDia" expr="''"/>
<var name="PRM_segmento" expr="''"/>
<var name="PRM_tipoBin" expr="''"/>
<var name="PRM_errorloc" expr="''"/>

<var name="bin" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>
<var name="PRM_IN_MENU_opciones"/>
<var name="PRM_IN_MENU_opciones_AUX"/>
<var name="PRM_Confirmación"/>



<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_OUT_Campana" expr="''"/>
<var name="PRM_AUX_PROM" expr="''"/>
<var name="PRM_AUX_OPCION" expr="''"/>
<var name="PRM_indiceIntentosMENU" expr="0"/>
<var name="VG_menu" expr="''"/>

<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Cotizacion****
	*****************************************************
	-->
	<var name="VG_DatosCotizacion"/>
<script>
	VG_menu = new Menu();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-ZDDATOS"><value expr="'Catch de cuelgue en SUB-SELECCIONAR-CAMPANA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>		
		<assign name="PRM_info" expr="''"/>

		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-ZDDATOS"><value expr="'Catch de error en SUB-SELECCIONAR-CAMPANA'"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<assign name="PRM_info" expr="''"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_SELECCIONAR_CAMPANA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_intentosCandado"/>
	<var name="PRM_IN_tipoCandado"/>
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
	<var name="PRM_IN_controlador"/>
	
	<block>
		<log label="MODULO-ZDDATOS"><value expr="'INICIO - SUB-MODULO-OBTENER-CAMPANAS'"/></log>
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_maxNumIntentos" expr="PRM_IN_intentosCandado"/>
		<assign name="PRM_candado" expr="PRM_IN_tipoCandado"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
		<assign name="VG_controlador" expr="PRM_IN_controlador"/>
		
		<log label="MODULO-ZDDATOS"><value expr="'CANDADO TARJETA - PRM_maxNumIntentos:' + PRM_maxNumIntentos"/></log>
<!-- 		<log label="MODULO-ZDDATOS"><value expr="'identificacion por tarjeta tsec: ' + PRM_cliente.tsec"/></log> -->
		
		<assign name="PRM_numIntentos" expr="0"/>
		
		<if cond="PRM_resultadoOperacion == 'OK'">
			<assign name="PRM_IN_MENU_opciones" expr="'${opcionesjson}'"/>
			<!-- <prompt>Cadena recibida en el MODULO_SELECCIONAR_CAMPANA<value expr="PRM_IN_MENU_opciones"/></prompt> -->
			<goto next="#MENU_SELECCIONAR_CAMPANA"/>
		<else/>	
			<assign name="PRM_Campana" expr="'${opcion}'"/>
			<assign name="PRM_AUX_PROM" expr="'${texto}'"/>
				<goto next="#MENU-CONFIRMAR-CAMPANA"/>
		</if>
		
		
<!-- 		<prompt> -->
<!-- 		opciones -->
<!-- 			<value expr="PRM_IN_MENU_opciones"></value> -->
<!-- 		</prompt> -->
		
		
				
	
	</block>
	
</form>

<form id="MENU_SELECCIONAR_CAMPANA">
	<block>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-MENU-BASE-CAMPANA'"/>
							
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>		
	</block>

	<subdialog name="MenuSolicitarTarjeta" fetchhint="safe" method="post" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA-XBEAN.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional PRM_IN_MENU_opciones">
		
		<filled>
			
			<assign name="VG_menu.codigoRetorno" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_respuestaUsuario"/> 
			<assign name="VG_menu.intento" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.respuestaUsuarioRaw" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_respuestaUsuarioRaw"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="MODULO-ZDDATOS"><value expr="'VUELTA menu solicitar tarjeta identificacion cliente'"/></log>		
			<if cond="VG_menu.codigoRetorno == 'OK'">
					
				<!-- ha introducido un numero de tarjeta -->
					
				<log label="MODULO-ZDDATOS"><value expr="'ANTES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
					<script>
						var respuestaProm = '';
						var opcion='';
						var accion='';
						respuestaProm=VG_menu.respuestaUsuarioRaw;
						respuestaProm=respuestaProm.replace("marca", "");
						respuestaProm=respuestaProm.replace("Pago", "");
						VG_menu.respuestaUsuarioRaw=respuestaProm;
					</script>
				<assign name="PRM_Campana" expr="VG_menu.respuestaUsuario"/>
				<assign name="PRM_AUX_PROM" expr="VG_menu.respuestaUsuarioRaw"/>
<!-- 				<prompt> -->
<!-- 					Campana -->
<!-- 					<value expr="PRM_Campana"></value> -->
<!-- 				</prompt> -->
<!-- 				<prompt> -->
<!-- 					aux prom -->
<!-- 					<value expr="PRM_AUX_PROM"></value> -->
<!-- 				</prompt> -->
				<!-- Se comprueba si el Cliente es Tipo EXPRESS por el BIN, antes de la identificacion por el WS ProxyServiceWHY5 -->
<!-- 				<assign name="PRM_Campana" expr="PRM_Campana + '00000000'"/> -->
<!-- 				<assign name="PRM_cliente.tarjetas[0].numeroTarjeta" expr="PRM_Campana"/> -->
<!-- 				<assign name="PRM_cliente.tarjetaSeleccionada" expr="0"/>		 -->
					
				<log label="MODULO-ZDDATOS"><value expr="'Numero de referencia BNC: '+ PRM_Campana"/></log>
				<log label="MODULO-ZDDATOS"><value expr="'Tarjeta seleccionada: ' + PRM_cliente.tarjetaSeleccionada"/></log>
				
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>	
				<assign name="PRM_codigoRetorno" expr="'OK'"/>	
				<assign name="PRM_error" expr="''"/>

<!-- 				<goto next="#RECUPERA_CLIENTE"/> -->
				<goto next="#MENU-CONFIRMAR-CAMPANA"/>
			
			<elseif cond="VG_menu.codigoRetorno == 'MAXINT'" />	
			
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
				<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
				<assign name="PRM_error" expr="''"/>
				<assign name="PRM_info" expr="'MAXINT_MENU'"/>
				
				<goto next="#FIN"/>
			
			<else/>
				<!-- codigoRetorno = ERROR o HANGUP-->	
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>	
				<assign name="PRM_codigoRetorno" expr="VG_menu.codigoRetorno"/>	
				<assign name="PRM_error" expr="VG_menu.error"/>
				
				<goto next="#FIN"/>
			
			</if>
		</filled>
	</subdialog>
</form>

<form id="MENU-CONFIRMAR-CAMPANA">

	<block>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-CONFIRMAR-CAMPANA'"/>					
		<assign name="PRM_IN_MENU_variable" expr="PRM_AUX_PROM"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
	    <log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>	
	</block>

	<subdialog name="MenuConfirmarTarjeta" fetchhint="safe" method="post"
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp" 
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
		
		<filled>
			
			<assign name="VG_menu.codigoRetorno" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_error"/>
			
			<assign name="VG_menu.respuestaUsuario" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_respuestaUsuario"/> 
			
			<assign name="VG_menu.intento" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VUELTA menu solicitar tarjeta identificacion cliente'"/></log>
			
<!-- 		<prompt> -->
<!-- 			regrese -->
<!-- 			<value expr="VG_menu.respuestaUsuario"></value> -->
<!-- 		</prompt> -->
<!-- 		<prompt> -->
<!-- 			intento -->
<!-- 			<value expr="VG_menu.intento"></value> -->
<!-- 		</prompt>	 -->
			
			<if cond="VG_menu.codigoRetorno == 'OK'">
			<assign name="PRM_Confirmación" expr="VG_menu.respuestaUsuario"/>
				<if cond="PRM_Confirmación == '1'">
					<assign name="PRM_resultadoOperacion" expr="'OK'"/>	
					<assign name="PRM_codigoRetorno" expr="'OK'"/>
					<assign name="PRM_error" expr="''"/>
					 <goto next="#FIN"/>
				 <elseif cond="PRM_Confirmación == '2'"/>
				 	<if cond="PRM_indiceIntentosMENU &lt; 3">
						<assign name="PRM_indiceIntentosMENU" expr="PRM_indiceIntentosMENU + 1"/>
						<goto next="#MENU_SELECCIONAR_CAMPANA"/>
					<else/>
						<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
						<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
						<assign name="PRM_error" expr="''"/>
						<assign name="PRM_info" expr="'MAXINT_MENU'"/>
						<goto next="#FIN"/>
					</if>
				<else/>
					<if cond="PRM_indiceIntentosMENU &lt; 3">
						<assign name="PRM_indiceIntentosMENU" expr="PRM_indiceIntentosMENU + 1"/>
						<goto next="#MENU-CONFIRMAR-MONTO"/>
					<else/>
						<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
						<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
						<assign name="PRM_error" expr="''"/>
						<assign name="PRM_info" expr="'MAXINT_MENU'"/>
						<goto next="#FIN"/>
						
					</if>
				</if>
				<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>
				<else/>
					<assign name="PRM_resultadoOperacion" expr="VG_menu.codigoRetorno"/>	
					<assign name="PRM_codigoRetorno" expr="VG_menu.codigoRetorno"/>
					<assign name="PRM_error" expr="VG_menu.error"/>
					<goto next="#FIN"/>
			</if>
			</filled>
			</subdialog>
</form>

<form id="FIN">
	<block>
		<log label="MODULO-ZDDATOS"><value expr="'FIN - identificacion por tarjeta'"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_error: ' + PRM_error"/></log>
		
<!-- 		<return namelist="PRM_errorloc PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado"/> -->
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_Campana"/>
	</block>
</form>

</vxml>
