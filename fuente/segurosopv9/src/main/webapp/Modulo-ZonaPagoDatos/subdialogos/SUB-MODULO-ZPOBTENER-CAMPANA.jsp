<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Properties"%>
<%@page import="java.util.Vector"%>
<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-OBTENER-CAMPANA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>

<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>
<!-- COTIZACION -->
<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>

<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_cliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!-- Numero de intentos para introducir la tarjeta -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>

<var name="idLocucionDeAcuerdo"/>

<!-- guardo el numero de tarjeta si lo introduce el cliente para identificarse -->
<var name="PRM_Campana" expr="''"/>

<!-- Nos indica que tipo de identificacion tenemos que lanzar primero (AUTO: LoanDocuments, HIPOTECA: LoanBalance) -->
<var name="PRM_candado" expr="''"/>

<!-- Nos dice si el cliente se ha identificado como AUTO o como HIPOTECA -->
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_tipoProducto" expr="''"/>

<!-- para indicar el la salida lo que ha ocurrido {MAXINT_MENU, MAXINT_VALID} -->
<var name="PRM_info" expr="''"/>

<var name="PRM_bloqueado" expr="''"/>
<var name="PRM_nombreCliente" expr="''"/>
<var name="PRM_momentoDia" expr="''"/>
<var name="PRM_segmento" expr="''"/>
<var name="PRM_tipoBin" expr="''"/>
<var name="PRM_errorloc" expr="''"/>

<var name="bin" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>
<var name="PRM_IN_MENU_opciones"/>
<var name="PRM_AUX_camapanas"/>
<var name="PRM_AUX_IdAfiliacion"/>
<var name="PRM_Confirmacion"/>



<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_OUT_Campana" expr="''"/>
<var name="PRM_AUX_PROM" expr="''"/>
<var name="PRM_AUX_OPCION" expr="''"/>
<var name="PRM_indiceIntentosMENU" expr="0"/>
<var name="VG_menu" expr="''"/>

<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Cotizacion****
	*****************************************************
	-->
	<var name="VG_DatosCotizacion"/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-ZDDATOS"><value expr="'Catch de cuelgue en SUB-OBTENER-CAMPANAS'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>		
		<assign name="PRM_info" expr="''"/>

		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-ZDDATOS"><value expr="'Catch de error en SUB-OBTENER-CAMPANAS'"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<assign name="PRM_info" expr="''"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_SELECCIONAR_CAMPANA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_intentosCandado"/>
	<var name="PRM_IN_tipoCandado"/>
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
	<var name="PRM_IN_controlador"/>
	
	<block>
		<log label="MODULO-ZDDATOS"><value expr="'INICIO - SUB-MODULO-OBTENER-CAMPANAS'"/></log>
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_numIntentos" expr="PRM_IN_intentosCandado"/>
		<assign name="PRM_candado" expr="PRM_IN_tipoCandado"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="PRM_maxNumIntentos" expr="'${maxint}'"/>
		<assign name="PRM_AUX_PROM" expr="'${infoerror}'"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
		<assign name="VG_controlador" expr="PRM_IN_controlador"/>
		
		<log label="MODULO-ZDDATOS"><value expr="'CANDADO TARJETA - PRM_maxNumIntentos:' + PRM_maxNumIntentos"/></log>
<!-- 		<log label="MODULO-ZDDATOS"><value expr="'identificacion por tarjeta tsec: ' + PRM_cliente.tsec"/></log> -->
		
<!-- 		<assign name="PRM_numIntentos" expr="0"/> -->

		<if cond="PRM_resultadoOperacion == 'OK'">
			<assign name="PRM_IN_MENU_opciones" expr="'${opcionesjson}'"/>
			<assign name="PRM_AUX_IdAfiliacion" expr="'${afiliacionId}'"/>
			<goto next="#FIN"/>
		<else/>	
			<if cond="PRM_numIntentos &lt; PRM_maxNumIntentos">
					<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/>
							<goto next="#RESPUESTA_MENSAJE"/>
						<else/>	
						<assign name="PRM_AUX_PROM" expr="'AUT-CLI-INFO-PAGO-MAXIMOINT'"/>
						<assign name="PRM_error" expr="'MAXINT'"/>
							<goto next="#RESPUESTA_MENSAJE"/>
			</if>

		</if>
	</block>
	
</form>
<form id="RESPUESTA_MENSAJE">
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar mensajde error'"/></log>
		
		<!-- ABALFARO_20170717 cambio para dar locucion directamente en vez de menu -->
		<%-- <c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-AVISO-PRIVACIDAD"/> --%>
<%-- 		<c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-INICIO-AVISO-PRIVACIDAD"/> --%>
		
<%-- 		<log label="CONTROLADOR"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log> --%>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="PRM_AUX_PROM"/>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		
		<filled>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="PRM_resultadoOperacion" expr="'HANGUP'"/>
				<assign name="SUB_error" expr="''"/>
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>
	</subdialog>
</form>
<form id="FIN">
	<block>
	
		<assign name="PRM_AUX_camapanas" expr="PRM_IN_MENU_opciones"/>
		<log label="MODULO-ZDDATOS"><value expr="'FIN - identificacion por tarjeta'"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-ZDDATOS"><value expr="'PRM_error: ' + PRM_error"/></log>
		
<!-- 		<return namelist="PRM_errorloc PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado"/> -->
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_Campana PRM_AUX_camapanas PRM_AUX_IdAfiliacion"/>
	</block>
</form>

</vxml>
