<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-IDENTIF-TARJETA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>

<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>
<!-- COTIZACION -->
<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>

<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_cliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!-- Numero de intentos para introducir la tarjeta -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>

<var name="idLocucionDeAcuerdo"/>

<!-- guardo el numero de tarjeta si lo introduce el cliente para identificarse -->
<var name="PRM_CCVTarjetaIntroducido" expr="''"/>

<!-- Nos indica que tipo de identificacion tenemos que lanzar primero (AUTO: LoanDocuments, HIPOTECA: LoanBalance) -->
<var name="PRM_candado" expr="''"/>

<!-- Nos dice si el cliente se ha identificado como AUTO o como HIPOTECA -->
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_tipoProducto" expr="''"/>

<!-- para indicar el la salida lo que ha ocurrido {MAXINT_MENU, MAXINT_VALID} -->
<var name="PRM_info" expr="''"/>

<var name="PRM_bloqueado" expr="''"/>
<var name="PRM_nombreCliente" expr="''"/>
<var name="PRM_momentoDia" expr="''"/>
<var name="PRM_segmento" expr="''"/>
<var name="PRM_tipoBin" expr="''"/>
<var name="PRM_errorloc" expr="''"/>

<var name="bin" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>


<var name="VG_menu" expr="''"/>

<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Cotizacion****
	*****************************************************
	-->
	<var name="VG_DatosCotizacion"/>
<script>
	VG_menu = new Menu();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-IDENTIF"><value expr="'Catch de cuelgue en SUB-MODULO-TARJETA-CVV'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>		
		<assign name="PRM_info" expr="''"/>

		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-IDENTIF"><value expr="'Catch de error en SUB-MODULO-IDENTIF-TARJETA'"/></log>
		<log label="MODULO-IDENTIF"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-IDENTIF"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<assign name="PRM_info" expr="''"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_IDENTIF_TARJETA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_intentosCandado"/>
	<var name="PRM_IN_tipoCandado"/>
	<var name="PRM_IN_menu"/>
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
	<var name="PRM_IN_controlador"/>
	
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'INICIO - identificacion por referencia'"/></log>
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_maxNumIntentos" expr="PRM_IN_intentosCandado"/>
		<assign name="PRM_candado" expr="PRM_IN_tipoCandado"/>
		<assign name="PRM_IN_MENU_idMenu" expr="PRM_IN_menu"/>
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
		<assign name="VG_controlador" expr="PRM_IN_controlador"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'CANDADO TARJETA - PRM_maxNumIntentos:' + PRM_maxNumIntentos"/></log>
<!-- 		<log label="MODULO-IDENTIF"><value expr="'identificacion por tarjeta tsec: ' + PRM_cliente.tsec"/></log> -->
		
		<assign name="PRM_numIntentos" expr="0"/>
		
		<goto next="#MENU_SOLICITAR_TARJETA"/>		
	
	</block>
	
</form>

<!-- FORMULARIO para solicitar el numero de serie del token -->
<form id="MENU_SOLICITAR_TARJETA">
	<block>
		
<!-- 		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-SOLICITUD-CVVTARJETA'"/> -->
							
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>		
	</block>

	<subdialog name="MenuSolicitarTarjeta" fetchhint="safe" method="post" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
		
		<filled>
			
			<assign name="VG_menu.codigoRetorno" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_respuestaUsuario"/> 
			<assign name="VG_menu.intento" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VUELTA menu solicitar tarjeta identificacion cliente'"/></log>
					
			<if cond="VG_menu.codigoRetorno == 'OK'">
					
				<!-- ha introducido un numero de tarjeta -->
					
				<log label="MODULO-IDENTIFICACION"><value expr="'ANTES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
				
				<if cond="VG_menu.modoInteraccion == 'voice' || VG_menu.modoInteraccion == 'VOICE'">
					<!-- si el dato se ha introducido por voz, tengo que eliminar los espacios entre los digitos -->
					<script>
						var respuesta = '';
						var arrayResp = new Array();
						arrayResp = VG_menu.respuestaUsuario.split('');
	
						for (var x in arrayResp) {
							if (arrayResp[x] != ' '){
								// si no es espacio en blanco lo concateno
								respuesta = respuesta + arrayResp[x];
							}
						}
						VG_menu.respuestaUsuario = respuesta;
					</script>
						
						<log label="MODULO-IDENTIFICACION"><value expr="'DESPUES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>				
				</if>
					
				<assign name="PRM_CCVTarjetaIntroducido" expr="VG_menu.respuestaUsuario"/>
					
				<!-- Se comprueba si el Cliente es Tipo EXPRESS por el BIN, antes de la identificacion por el WS ProxyServiceWHY5 -->
<!-- 				<assign name="PRM_CCVTarjetaIntroducido" expr="PRM_CCVTarjetaIntroducido + '00000000'"/> -->
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>	
				<assign name="PRM_codigoRetorno" expr="'OK'"/>	
				<assign name="PRM_error" expr="''"/>

<!-- 				<goto next="#RECUPERA_CLIENTE"/> -->
				<goto next="#FIN"/>
			
			<elseif cond="VG_menu.codigoRetorno == 'MAXINT'" />	
			
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
				<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
				<assign name="PRM_error" expr="''"/>
				<assign name="PRM_info" expr="'MAXINT_MENU'"/>
				
				<goto next="#FIN"/>
			
			<else/>
				<!-- codigoRetorno = ERROR o HANGUP-->	
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>	
				<assign name="PRM_codigoRetorno" expr="VG_menu.codigoRetorno"/>	
				<assign name="PRM_error" expr="VG_menu.error"/>
				
				<goto next="#FIN"/>
			
			</if>
		</filled>
	</subdialog>
</form>

<form id="FIN">
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'FIN - identificacion por tarjeta'"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		
<!-- 		<return namelist="PRM_errorloc PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado"/> -->
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_CCVTarjetaIntroducido"/>
	</block>
</form>

</vxml>
