<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">

<!--
 *************************************************************************************************
 *  ODATOSPAGOICADOR:	MODULO-ZONAPAGODATOSDePAGO-INICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
	

	<meta http-equiv="Expires" content="0"/>
	<property name="fetchaudio" value="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/audio/SPA-silence.wav"/>

	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
	
	<!-- ABALFARO_20170217 -->
	<!-- Cuadro de mando - STAT -->
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Stat.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/FuncionesStat.js"/>
	<!-- COTIZACION -->
	<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>
	
	<!-- 
	****************************************************
	********* DEFINICION DE VARIABLES GLOBALES *********
	****************************************************
	-->

	
	<var name="VG_codigoRetorno" expr="''"/>
	<var name="VG_cliente" expr="''"/>
	<var name="VG_tipoIdentificacion" expr="''"/>
	<var name="VG_intentosIdentificacion" expr="''"/>
	<var name="VG_menu" expr="''"/>
	
	<var name="VG_candados" expr="''"/>
	<var name="VG_intentosCandados" expr="''"/>
	
	<var name="VG_gestionExcepciones" expr="''"/>
	
	<!-- elemento de identif del cliente -->
	<var name="VG_llaveAccesoFront" expr="''"/>

	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
	<var name="VG_controlador" expr="''"/>
	
	<!--
	****************************************************
	********* DEFINICION DE VARIABLES LOGGER ***********
	****************************************************
	-->
	<var name="VG_loggerServicio" expr="''"/>
	
	<!-- 
	****************************************************
	*** DEFINICION DE VARIABLES STAT-CUADRO MANDO ******
	****************************************************
	-->
	<!-- ABALFARO_20170217 -->
	<var name="VG_stat" expr="''"/>


	<!-- PARAMETROS DE SALIDA DEL MODULO-ODATOSPAGO -->
	<var name="PRM_OUT_ODPA_resultadoOperacion" expr="''"/>
	<var name="PRM_OUT_ODPA_codigoRetorno" expr="''"/>
	<var name="PRM_OUT_ODPA_error" expr="''"/>
	<var name="PRM_OUT_ODPA_info" expr="''"/>
	<var name="PRM_OUT_ODPA_gestionExcepciones" expr="''"/>
	<var name="PRM_OUT_ODPA_DatosCotizacion" expr="''"/>
	<var name="PRM_OUT_ODPA_isIdentificado" expr="''"/>
	
	<!-- ABALFARO_20170217 -->
	<var name="PRM_OUT_ODPA_stat" expr="''"/>
	
	<!-- 
	****************************************************
	** DEFINICIoN DE PROPIEDADES GLOBALES DEL SERVICIO *
	****************************************************
	-->

	<property name="inputmodes" value="dtmf voice"/>
	<property name="termchar" value=""/>

	<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Cotizacion****
	*****************************************************
	-->
	<var name="VG_DatosCotizacion"/>
	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-ODATOSPAGO"><value expr="'Catch de cuelgue en MODULO-ZP-DATOS'"/></log>
		
		<assign name="PRM_OUT_ODPA_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_ODPA_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_OUT_ODPA_error" expr="''"/>
		
		<assign name="PRM_OUT_ODPA_isIdentificado" expr="'false'"/>
<!-- 		<assign name="PRM_OUT_ODPA_tipoIdentificacion" expr="''"/> -->
<!-- 		<assign name="PRM_OUT_ODPA_cliente" expr="VG_cliente"/> -->
<!-- 		<assign name="PRM_OUT_ODPA_llaveAccesoFront" expr="VG_llaveAccesoFront"/> -->
		<assign name="PRM_OUT_ODPA_gestionExcepciones" expr="VG_gestionExcepciones"/>
		<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/finModuloZPDatos" method="post"  
				namelist="VG_loggerServicio PRM_OUT_ODPA_codigoRetorno PRM_OUT_ODPA_resultadoOperacion PRM_OUT_ODPA_error
						PRM_OUT_ODPA_isIdentificado PRM_OUT_ODPA_tipoIdentificacion PRM_OUT_ODPA_ramaIdentificacion
						PRM_OUT_ODPA_info  PRM_OUT_ODPA_gestionExcepciones VG_stat"/>
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-ODATOSPAGO"><value expr="'Catch de error en MODULO-ZP-DATOS'"/></log>
		
		<log label="MODULO-ODATOSPAGO"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-ODATOSPAGO"><value expr="'MESSAGE: ' + _message"/></log>
			
		<assign name="PRM_OUT_ODPA_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_ODPA_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_OUT_ODPA_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		
		<assign name="PRM_OUT_ODPA_isIdentificado" expr="'false'"/>
<!-- 		<assign name="" expr="''"/> -->
<!-- 		<assign name="PRM_OUT_ODPA_cliente" expr="VG_cliente"/> -->
<!-- 		<assign name="PRM_OUT_ODPA_llaveAccesoFront" expr="VG_llaveAccesoFront"/> -->
<!-- 			<if cond="PRM_OUT_ODPA_isIdentificado == 'true'" >   -->
<!-- 				ABALFARO_20170223 relleno excepcion ID_CLIENTE_ODATOSPAGOICADO -->
<!-- 				<script> -->
<!-- 					var numExcepTotal = VG_gestionExcepciones.postInformacion.length; -->
<!-- 					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_ODATOSPAGOICADO'; -->
<!-- 				</script> -->
<!-- 			<else/> -->
<!-- 				ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_ODATOSPAGOICADO -->
<!-- 				<script> -->
<!-- 					var numExcepTotal = VG_gestionExcepciones.postInformacion.length; -->
<!-- 					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_ODATOSPAGOICADO'; -->
<!-- 				</script> -->
<!-- 			</if>	 -->
		<assign name="PRM_OUT_ODPA_gestionExcepciones" expr="VG_gestionExcepciones"/>
							
		<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/finModuloZPDatos" method="post"  
				namelist="VG_loggerServicio PRM_OUT_ODPA_codigoRetorno PRM_OUT_ODPA_resultadoOperacion PRM_OUT_ODPA_error
						PRM_OUT_ODPA_isIdentificado PRM_OUT_ODPA_tipoIdentificacion PRM_OUT_ODPA_ramaIdentificacion
						PRM_OUT_ODPA_info  PRM_OUT_ODPA_gestionExcepciones VG_stat"/>

	</catch>
	
		

	<form id="MODULO_ODATOSPAGO_INICIO">
		
		<block>
			<!-- esto NO se ejecuta, siempre se empezara en la pagina INICIO-PARAMS -->
			<submit next="${pageContext.request.contextPath}/MODULO-ODATOSPAGO-LB/inicioParams" method="post" namelist=""/>
			 
		</block>
		
	</form>

</vxml>





