<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-IdentificacionLB/staticvxml/MODULO-IDENTIF-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-IDENTIFICACION-EJECUTAR
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>

<var name="PRM_loggerServicio" expr="''"/>

<!-- Para recorrer el string con la informacion de los candados -->
<var name="PRM_indiceCandado" expr="''"/>
<var name="PRM_indiceIntentos" expr="''"/>

<!-- Candado actual: identificador + intentos -->
<var name="PRM_candadoActual" expr="''"/>
<var name="PRM_candado" expr="''"/>
<var name="PRM_intentosCandado" expr="''"/>

<!-- Para el saldo automatico -->
<var name="PRM_saldo" expr="''"/>
<var name="PRM_saldoVencido" expr="''"/>
<var name="PRM_importeUltimaMensualidad" expr="''"/>
<var name="PRM_fechaUltimaMensualidad" expr="''"/>
<var name="PRM_moneda" expr="''"/>
<var name="PRM_errorloc" expr="''"/>
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_info" expr="''"/>

<var name="PRM_estaEnListaNegra" expr="''"/>
<var name="PRM_aniConfiable" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<script>
  	VG_candados = new Array();
  	VG_intentosCandados = new Array();
</script>



<form id="MODULO_IDENTIFICACION_EJECUTAR">	
	<block>
		<log label="INICIO-EJECUTAR"><value expr="'VG_cliente.nombreCliente: ' + VG_cliente.nombreCliente"/></log>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'tipoIdentificacion: ${tipoIdentificacion}'"/></log>	
		<log label="MODULO-IDENTIFICACION"><value expr="'intentosIdentificacion: ${intentosIdentificacion}'"/></log>
		
		<assign name="PRM_indiceCandado" expr="0"/>
		<assign name="PRM_indiceIntentos" expr="0"/>
		<assign name="PRM_candadoActual" expr="0"/>
		
		<!-- Recuperamos los candados que debe pasar el cliente DESDE EL SERVLET ServletEjecutar -->
		<c:forTokens items="${tipoIdentificacion}" delims="|" var="candado">			
			<script>
				VG_candados[PRM_indiceCandado] = '${candado}';	
			</script>			
			<assign name="PRM_indiceCandado" expr="PRM_indiceCandado + 1"/>		
		</c:forTokens>
		
		<!-- Recuperamos el numero de intentos que tiene para cada candado DESDE EL SERVLET ServletEjecutar -->
		<c:forTokens items="${intentosIdentificacion}" delims="|" var="intento">			
			<script>
			VG_intentosCandados[PRM_indiceIntentos] = '${intento}';	
			</script>			
			<assign name="PRM_indiceIntentos" expr="PRM_indiceIntentos + 1"/>		
		</c:forTokens>
		
		<!-- Tiene que coincidir el numero de candados y el de intentos y sino dar un error -->
		<if cond="PRM_indiceCandado == PRM_indiceIntentos">			
			<goto next="#TIPO_CANDADO"/>			
			
		<else/>	
			<log label="MODULO-IDENTIFICACION"><value expr="'No coinicide el numero de candados con los intentos'"/></log>
			<goto next="#RESULTADO_KO"/>
		</if>			
		
	</block>
</form>


<form id="TIPO_CANDADO">	
	<block>
	
		<if cond="PRM_candadoActual &lt; PRM_indiceCandado">			
			<!-- Recuperamos el candado que tenemos que identificar -->
			<assign name="PRM_candado" expr="VG_candados[PRM_candadoActual]"/>
			<assign name="PRM_intentosCandado" expr="VG_intentosCandados[PRM_candadoActual]"/>
			
			<log label="MODULO-IDENTIFICACION"><value expr="'CANDADO: ' + PRM_candado + ' - INTENTOS:' + PRM_intentosCandado "/></log>
						
		<else/>	
			<!-- Si he llegado a este punto es que he pasado todos los candados -->
			<log label="MODULO-IDENTIFICACION"><value expr="'Se han pasado todos los candados'"/></log>
			
			<goto next="#RESULTADO_OK"/>
		</if>	
		
		<log label="MODULO-IDENTIFICACION"><value expr="'CANDADO: ' + PRM_candado"/></log>
	
		<if cond="PRM_candado == 'TARJETA'">			
			<goto next="#IDENTIF_TARJETA"/>	
		<else/>				
			<goto next="#RESULTADO_KO"/>			
		</if>			
	</block>

</form>



<form id="IDENTIF_TARJETA">	

	<block>	
		
	</block>

	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/subIdentifTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-IDENTIFICACION"><value expr="'Vuelvo a modulo edentif-ejecutar'"/></log>
					
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_cliente" expr="subIdentifTarjeta.PRM_cliente"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subIdentifTarjeta.PRM_tipoIdentifRecuperada"/>
			<assign name="PRM_info" expr="subIdentifTarjeta.PRM_info"/>
			<assign name="PRM_errorloc" expr="subIdentifTarjeta.PRM_errorloc"/>
											
			<!-- Asigno la locucion de resultado -->
			<if cond="PRM_resultadoOperacion == 'OK'">			
				<!-- Ha ido bien la identificacion -->

<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, VG_cliente.segmento)"/> -->
				
				<log label="MODULO-IDENTIFICACION"><value expr="'ok'"/></log>
					
				<if cond="PRM_codigoRetorno == 'OK'">
					<!-- el cliente se ha identificado -->
						
						<!-- guardo el instrumento de identificacion para la cadena cti/uui -->
<!-- 						<assign name="VG_llaveAccesoFront" expr="VG_cliente.tarjetas[VG_cliente.tarjetas.length - 1].numeroTarjeta"/> -->
				
					<!-- Continuo en el bucle por si hay mas candados a solicitar -->
<!-- 					<goto next="#TIPO_CANDADO"/> -->
					<goto next="#RESULTADO_OK"/>
					
				<else/>
					<!-- el cliente no se identifico OK -->
					<goto next="#RESULTADO_KO"/>
				</if>
			
			
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />	
			
						<!-- el cliente ha superado el numero maximo de intentos en el menu -->
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-IDENTIFICACION"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-IDENTIFICACION"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
					
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
			
						<!-- el cliente ha superado el numero maximo de intentos en la validacion -->
						
<!-- 					<var name="PRM_longitudClienteIdentificacion" expr="VG_cliente.tiposIdentificacion.length - 1"/> -->
				
<!-- 					<log label="MODULO-IDENTIFICACION"><value expr="'tipo: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].tipo"/></log> -->
<!-- 					<log label="MODULO-IDENTIFICACION"><value expr="'result: ' + VG_cliente.tiposIdentificacion[PRM_longitudClienteIdentificacion].resultado"/></log> -->
					
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<goto next="#FIN"/>
				</if>
			</if>	
		</filled>
	</subdialog>

</form>

<form id="RESULTADO_OK">	
	<block>	
			
		<log label="MODULO-IDENTIFICACION"><value expr="'El cliente SI ha sido Identificado'"/></log>
<!-- 		<assign name="VG_cliente.isIdentificado" expr="'true'"/> -->
		<assign name="PRM_codigoRetorno" expr="'OK'"/>
		<assign name="PRM_resultadoOperacion" expr="'OK'"/>
		<assign name="PRM_error" expr="''"/>
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'true'"/>
		
		<goto next="#FIN"/>
	</block>
</form>

<form id="RESULTADO_KO">	
	<block>	
			
		<log label="MODULO-IDENTIFICACION"><value expr="'El cliente NO ha sido Identificado'"/></log>
<!-- 		<assign name="VG_cliente.isIdentificado" expr="'false'"/> -->
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'false'"/>
		
		<goto next="#RESULTADO_KO2"/>
		
	</block>
	
</form>





<!-- formulario para informar al cliente que no esta identificado -->
<form id="LOC_TARJETA_NO_IDENTIF">
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'El cliente NO esta identificado'"/></log>
		
		<c:set var="locucionesInformacion" scope="request" value="ID-CLI-INFO-TARJETA-NO-IDENTIF"/>								
		<log label="MODULO-IDENTIFICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>	
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>	
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>	
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>	
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	

		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>	
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>

		<log label="MODULO-IDENTIFICACION"><value expr="'SALTO A PLANTILLA'"/></log>
			
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>

			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>	
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>	
					
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#RESULTADO_KO2"/>
									
		</filled>	
		
	</subdialog>
</form>	


<form id="RESULTADO_KO2">	
	<block>	
			
		<if cond="PRM_resultadoOperacion == 'MAXINT_VALID'" >      
		
			<assign name="PRM_resultadoOperacion" expr="'KO'"/>
<!-- 			<assign name="PRM_codigoRetorno" expr="VG_accionMaxintValid"/> -->

			<!-- ABALFARO_20170131 fuerzo codigoRetorno = OK, ya se tratara la excepcion -->
			<assign name="PRM_codigoRetorno" expr="'OK'"/> 				
		
		<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" /> 
			
			<assign name="PRM_resultadoOperacion" expr="'KO'"/>
<!-- 			<assign name="PRM_codigoRetorno" expr="VG_accionMaxintMenu"/> -->

			<!-- ABALFARO_20170131 fuerzo codigoRetorno = OK, ya se tratara la excepcion -->
			<assign name="PRM_codigoRetorno" expr="'OK'"/> 
			
		<else/>
			<!-- en otro caso devuelvo el codigoRetorno que ya tenga: ERROR, HANGUP -->
			<!-- <assign name="PRM_codigoRetorno" expr="'ERROR'"/> -->
			
			<!-- ABALFARO_20170124 resultadoOperacion OK si NO_CLIENTE o ROBO_EXTRAVIO -->
			<!-- a no ser que fuese NO_CLIENTE o ROBO_EXTRAVIO, que obligo a devolver OK -->
			<if cond="PRM_info == 'NO_CLIENTE' || PRM_info == 'ROBO_EXTRAVIO'">  
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>
				
				<!-- ABALFARO_20170131 fuerzo codigoRetorno = OK, ya se tratara la excepcion -->
				<assign name="PRM_codigoRetorno" expr="'OK'"/> 
				
				
				<!-- ABALFARO_20170131 relleno excepcion ID_LB_NO_CLIENTE -->
				<if cond="PRM_info == 'NO_CLIENTE'" >  
					<script>
						var numExcepTotal = VG_gestionExcepciones.postIdentificacion.length;
						VG_gestionExcepciones.postIdentificacion[numExcepTotal] = 'ID_LB_NO_CLIENTE';
					</script>
				</if>
				
				<!-- ABALFARO_20170131 relleno excepcion ID_LB_ROBO_EXTRAVIO -->
				<if cond="PRM_info == 'ROBO_EXTRAVIO'" >  
					<script>
						var numExcepTotal = VG_gestionExcepciones.postIdentificacion.length;
						VG_gestionExcepciones.postIdentificacion[numExcepTotal] = 'ID_LB_ROBO_EXTRAVIO';
					</script>
				</if>
				
				
				
			</if>
		</if>   

		
		<goto next="#FIN"/>
	</block>
	
</form>






<form id="CLIENTE_BLOQUEADO">	
	<block>	
		<assign name="PRM_info" expr="'CLIENTE_BLOQUEADO'"/>
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
<!-- 		<assign name="PRM_codigoRetorno" expr="'FIN'"/> -->

		<!-- ABALFARO_20170131 fuerzo codigoRetorno = OK, ya se tratara la excepcion -->
		<assign name="PRM_codigoRetorno" expr="'OK'"/>
		 
		<goto next="#FIN"/>
	</block>	
</form>	


<form id="FIN">	
	<block>	
	
		<assign name="PRM_OUT_IDENT_codigoRetorno" expr="PRM_codigoRetorno"/>
		<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
		<assign name="PRM_OUT_IDENT_error" expr="PRM_error"/>
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'true'"/>
		<assign name="PRM_OUT_IDENT_tipoIdentificacion" expr="VG_tipoIdentificacion"/>		
		<assign name="PRM_OUT_IDENT_cliente" expr="VG_cliente"/>
		<assign name="PRM_OUT_IDENT_info" expr="PRM_info"/>
		<assign name="PRM_OUT_IDENT_llaveAccesoFront" expr="VG_llaveAccesoFront"/>
		
		
			<if cond="PRM_OUT_IDENT_isIdentificado == 'true'" >  
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_IDENTIFICADO';
				</script>
			<else/>
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_IDENTIFICADO';
				</script>
			</if>	
			
		<assign name="PRM_OUT_IDENT_gestionExcepciones" expr="VG_gestionExcepciones"/>
		
		<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/finModuloIdentificacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_tipoIdentificacion PRM_OUT_IDENT_ramaIdentificacion
						PRM_OUT_IDENT_info PRM_OUT_IDENT_llaveAccesoFront PRM_OUT_IDENT_gestionExcepciones VG_stat"/>
										
	</block>
	
</form>


</vxml>
