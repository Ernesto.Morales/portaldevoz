<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-IdentificacionLB/staticvxml/MODULO-IDENTIF-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-IDENTIF-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>



<form id="MODULO_IDENTIF_INICIO_PARAMS">

	<var name="PRM_IN_IDENT_loggerServicio"/>
	<var name="PRM_IN_IDENT_tipoIdentificacion"/>
	<var name="PRM_IN_IDENT_intentosIdentificacion"/>
	<var name="PRM_IN_IDENT_cliente"/>
	
<!-- 	<var name="PRM_IN_IDENT_accionMaxintMenu"/> -->
<!-- 	<var name="PRM_IN_IDENT_accionMaxintValid"/> -->
	
	<var name="PRM_IN_IDENT_gestionExcepciones"/>
	
	<!-- ABALFARO_20170217 -->
	<var name="PRM_IN_IDENT_stat"/>
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
	<var name="PRM_IN_IDENT_controlador"/>

	<block>				
		<log label="MODULO-IDENTIF"><value expr="'INICIO - MODULO-IDENTIF-INICIO-PARAMS'"/></log>

		<assign name="VG_loggerServicio" expr="PRM_IN_IDENT_loggerServicio"/>
		<assign name="VG_loggerServicio.idElemento" expr="'${idElemento}'"/>
		
		<assign name="VG_cliente" expr="PRM_IN_IDENT_cliente"/>
		<assign name="VG_tipoIdentificacion" expr="PRM_IN_IDENT_tipoIdentificacion"/>
		<assign name="VG_intentosIdentificacion" expr="PRM_IN_IDENT_intentosIdentificacion"/>	
		
<!-- 		<assign name="VG_accionMaxintMenu" expr="PRM_IN_IDENT_accionMaxintMenu"/>	 -->
<!-- 		<assign name="VG_accionMaxintValid" expr="PRM_IN_IDENT_accionMaxintValid"/> -->
		
		<assign name="VG_gestionExcepciones" expr="PRM_IN_IDENT_gestionExcepciones"/>
		
		<!-- ABALFARO_20170217 -->
		<assign name="VG_stat" expr="PRM_IN_IDENT_stat"/>	
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<assign name="VG_controlador" expr="PRM_IN_IDENT_controlador"/>	
		
		<log label="MODULO-IDENTIF"><value expr="'VG_tipoIdentificacion: ' + VG_tipoIdentificacion"/></log>
		<log label="MODULO-IDENTIF"><value expr="'VG_intentosIdentificacion: ' + VG_intentosIdentificacion"/></log>
<!-- 		<log label="MODULO-IDENTIF"><value expr="'tsec: ' + VG_cliente.tsec"/></log> -->
		
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		
		<log label="MODULO-IDENTIF"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIF"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="MODULO-IDENTIF"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
			
		<!-- inicializamos la identificacion a false -->
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'false'"/>
		<assign name="VG_cliente.isIdentificado" expr="'false'"/>
			
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION_' + VG_loggerServicio.op, 'INIT')"/> 
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- ***** STAT: inicio operacion ***** -->
		<assign name="VG_stat" expr="inicioOperacion(VG_stat, '${milisecInicioOperacion}', 'MODULO_IDENTIFICACION_' + VG_loggerServicio.op)"/> 
		<!-- ***** STAT: inicio operacion ***** -->			
		
		<if cond="PRM_resultadoOperacion == 'OK'">
		
			<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/ejecutarModuloIdentificacion" method="post"  
				namelist="VG_loggerServicio VG_tipoIdentificacion VG_intentosIdentificacion"/>
		
		<else/>
				
			<log label="MODULO-IDENTIF"><value expr="'FIN - MODULO-IDENTIFICACION-INICIO-PARAMS'"/></log>
		
			<assign name="PRM_OUT_IDENT_codigoRetorno" expr="VG_codigoRetorno"/>
			<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
			<assign name="PRM_OUT_IDENT_error" expr="PRM_error"/>
			<assign name="PRM_OUT_IDENT_isIdentificado" expr="'false'"/>
			<assign name="PRM_OUT_IDENT_tipoIdentificacion" expr="VG_tipoIdentificacion"/>
			<assign name="PRM_OUT_IDENT_ramaIdentificacion" expr="''"/>
			<assign name="PRM_OUT_IDENT_cliente" expr="VG_cliente"/>
			<assign name="PRM_OUT_IDENT_llaveAccesoFront" expr="VG_llaveAccesoFront"/>	
				
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_IDENTIFICADO';
				</script>
			<assign name="PRM_OUT_IDENT_gestionExcepciones" expr="VG_gestionExcepciones"/>	
							
			<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/finModuloIdentificacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_tipoIdentificacion PRM_OUT_IDENT_ramaIdentificacion
						PRM_OUT_IDENT_info PRM_OUT_IDENT_llaveAccesoFront PRM_OUT_IDENT_gestionExcepciones VG_stat"/>
						
		</if>
	
		
	</block>

</form>



</vxml>
