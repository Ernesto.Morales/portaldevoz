<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-IdentificacionLB/staticvxml/MODULO-IDENTIF-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-IDENTIFICACION-FIN
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="verificaDigitos" expr="''"/>
<form id="MODULO_IDENTIFICACION_FIN">

	
	<block>				
		<log label="MODULO-IDENTIFICACION"><value expr="'INICIO - MODULO-IDENTIFICACION-FIN'"/></log>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_resultadoOperacion: ' + PRM_OUT_IDENT_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_codigoRetorno: ' + PRM_OUT_IDENT_codigoRetorno"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_error: ' + PRM_OUT_IDENT_error"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_isIdentificado: ' + PRM_OUT_IDENT_isIdentificado"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_tipoIdentificacion: ' + PRM_OUT_IDENT_tipoIdentificacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_ramaIdentificacion: ' + PRM_OUT_IDENT_ramaIdentificacion"/></log>
	
		<log label="MODULO-IDENTIFICACION"><value expr="'FIN - MODULO-IDENTIFICACION-FIN'"/></log>
					
		<!-- ABALFARO_20170315 -->
		<!-- ***** STAT: aniade contador ***** -->
			<!-- solo aniadiremos el contador si esta identificado o si no lo esta pero la info viene rellena -->
			<if cond="PRM_OUT_IDENT_isIdentificado == 'true' || (PRM_OUT_IDENT_isIdentificado == 'false' &amp;&amp; PRM_OUT_IDENT_info != '')">
				<assign name="verificaDigitos" expr="PRM_OUT_IDENT_cliente.tarjetaIdentificacion.substring(0,6)"/>
				<if	cond="verificaDigitos == '409851'">
					<log label="CONTROLADOR"><value expr="'ES EXPRES-IDEN' + verificaDigitos"/></log>
				<else/>
				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION_' + VG_loggerServicio.op, 'INFO', '',
									PRM_OUT_IDENT_isIdentificado, PRM_OUT_IDENT_tipoIdentificacion, PRM_OUT_IDENT_info)"/>
				</if>
			</if> 
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- ABALFARO_20170315 -->
		<if cond="PRM_OUT_IDENT_isIdentificado == 'true'">
				<!-- se ha identificado al cliente, almaceno su numeroCliente -->
				
				<log label="MODULO-IDENTIFICACION"><value expr="'PRM_OUT_IDENT_cliente.numCliente: ' + PRM_OUT_IDENT_cliente.numCliente"/></log>
		
				<!-- ***** STAT: guarda id cliente ***** -->
				<assign name="VG_stat" expr="guardaIdCliente(VG_stat, PRM_OUT_IDENT_cliente.numCliente)"/> 
				<!-- ***** STAT: guarda id cliente ***** -->		
		</if>
		
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION_' + VG_loggerServicio.op, 'END', PRM_OUT_IDENT_codigoRetorno)"/> 
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- ***** STAT: fin operacion ***** -->
		<assign name="VG_stat" expr="finOperacion(VG_stat, '${milisecFinOperacion}', 'MODULO_IDENTIFICACION_' + VG_loggerServicio.op, PRM_OUT_IDENT_resultadoOperacion, PRM_OUT_IDENT_codigoRetorno)"/> 
		<!-- ***** STAT: fin operacion ***** -->
		
		<!-- ABALFARO_20170217 devuelvo variable stat-->
		<assign name="PRM_OUT_IDENT_stat" expr="VG_stat"/>
		
		
		<return namelist="PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_error 
					PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_tipoIdentificacion PRM_OUT_IDENT_ramaIdentificacion PRM_OUT_IDENT_cliente 
					PRM_OUT_IDENT_llaveAccesoFront PRM_OUT_IDENT_info PRM_OUT_IDENT_gestionExcepciones PRM_OUT_IDENT_stat" />
		
	</block>

</form>

</vxml>
