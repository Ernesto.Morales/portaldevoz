<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-IDENTIF-RECUPERA-CLIENTE-TARJETA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>

<var name="VG_loggerServicio" expr="''"/>

<var name="PRM_cliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<var name="VG_resultadoOperacions" expr="''"/>
<var name="VG_errors" expr="''"/>

<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_MENERR" expr="''"/>
<var name="PRM_candado" expr="''"/>
<var name="PRM_numTarjetaIntroducido" expr="''"/>
<var name="PRM_bloqueado" expr="''"/>
<var name="bin" expr="''"/>



	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-IDENTIF"><value expr="123456789Catch de cuelgue en SUB-MODULO-IDENTIF-RECUPERA-CLIENTE-TARJETA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_bloqueado" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="MODULO-IDENTIF"><value expr="'Catch de error en SUB-MODULO-IDENTIF-RECUPERA-CLIENTE-TARJETA'"/></log>
		
		<log label="MODULO-IDENTIF"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-IDENTIF"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_bloqueado" />		
		
	</catch>
	
		

<!-- FORMULARIO para obtener el resultado de la operacion WS ProxyWYH5 -->
<form id="MODULO_IDENTIF_RECUPERA_CLIENTE_TARJETA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_candado"/>
		
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'INICIO - peticion ProxyWYH5'"/></log>
		
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_candado" expr="PRM_IN_candado"/>
		
		
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="PRM_numTarjetaIntroducido" expr="'${numTarjetaIntroducido}'"/>
		<assign name="PRM_bloqueado" expr="'${bloqueado}'"/>
	    <assign name="PRM_MENERR" expr="'${idError}'"/>
		<if cond="PRM_resultadoOperacion == 'OK'">
		
				
				<assign name="PRM_cliente.numCliente" expr="'${datosCliente.kndbish5[0].numecte}'"/>
				<assign name="PRM_cliente.nombreCliente" expr="'${datosCliente.kndbish5[0].nombcte}'"/>
				<assign name="PRM_cliente.calle" expr="'${datosCliente.kndbish5[0].calldom}'"/>
				<assign name="PRM_cliente.numeroCalle" expr="'${datosCliente.kndbish5[0].numeint}'"/>
				<assign name="PRM_cliente.colonia" expr="'${datosCliente.kndbish5[0].poblaci}'"/>
				<assign name="PRM_cliente.codigoPostal" expr="'${datosCliente.kndbish5[0].codpost}'"/>
				<assign name="PRM_cliente.segmento" expr="'${datosCliente.kndbish5[0].segment}'"/>
				
				<script>
					PRM_cliente.tarjetas = new Array();		
					PRM_cliente.tarjetas[0] = new Tarjeta();
					PRM_cliente.tickets = new Array();
					PRM_cliente.cuentas = new Array();
					PRM_cliente.creditos = new Array();
					PRM_cliente.prestamos = new Array(); 
				</script>
				<assign name="PRM_cliente.tarjetaSeleccionada" expr="0"/>				
				<assign name="PRM_cliente.tarjetas[0].numeroTarjeta" expr="PRM_numTarjetaIntroducido"/>
				
				<!-- ABALFARO_20170608 almacenamos la tarjeta con la que se identifica -->
				<assign name="PRM_cliente.tarjetaIdentificacion" expr="PRM_numTarjetaIntroducido"/>
				
				<assign name="bin" expr="PRM_numTarjetaIntroducido.substring(0,6)"/>
				
				<assign name="PRM_cliente.tarjetas[0].tipoTarjeta" expr="devuelveTipoBin(bin)"/>
				
				<assign name="PRM_cliente.cuentaSeleccionada" expr="-1"/>
				<assign name="PRM_cliente.inversionSeleccionada" expr="-1"/>
			
				<assign name="PRM_tipoIdentifRecuperada" expr="'TARJETA'"/>
				<goto next="#FIN"/>		
				
		<else/>
			<log label="CONTROLADOR"><value expr="'REGRESO'+PRM_MENERR"/></log>
			<goto next="#BIENVENIDA"/>	
		</if>
		<assign name="PRM_MENERR" expr="'ERROR'"/>
		<goto next="#FIN"/>
	</block>
	
</form>



<form id="FIN">
	<block>
				
		<log label="MODULO-IDENTIFICACION"><value expr="'FIN - peticion ProxyWYH5'"/></log>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		
		
		<return namelist="PRM_MENERR PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_bloqueado"/>
		
	</block>
</form>

<form id="BIENVENIDA">
		<log label="MODULO-IDENTIFICACION"><value expr="'MENSAJES'"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'MENSAJESlOGGeR'+VG_loggerServicio.idioma"/></log>
	<subdialog name="subBienvenida" method="post" 
	src="${pageContext.request.contextPath}/CONTROLADOR/subMensajeError"
		namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short PRM_MENERR">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio" />
		<filled>
			
			<assign name="VG_resultadoOperacions" expr="subBienvenida.SUB_resultadoOperacion"/>	
			<assign name="VG_errors" expr="subBienvenida.SUB_error"/>	
			
			<log label="CONTROLADOR"><value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacions"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + VG_errors"/></log>	
			
			<!-- [AAO-20161215]  -->
			<if	cond="VG_resultadoOperacions == 'HANGUP' ">
				<!-- si es HANGUP debo finalizar llamada -->
				
				<assign name="VG_resultadoOperacions" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_errors" expr="''"/>
									  
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>		
				
			<else/>
				<!-- si ha ido OK la bienvenida o ERROR, continuo a identificar -->
<!-- 				<assign name="VG_esMAU" expr="'subBienvenida.SUB_esMAU'"/> -->
				<goto next="#FIN" />
				
			</if>			
									
		</filled>	
		
	</subdialog>	
	

</form> 


</vxml>
