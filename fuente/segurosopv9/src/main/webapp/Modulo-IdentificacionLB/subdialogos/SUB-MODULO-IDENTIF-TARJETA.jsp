<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-IDENTIF-TARJETA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>

<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>


<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_cliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!-- Numero de intentos para introducir la tarjeta -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>

<var name="idLocucionDeAcuerdo"/>

<!-- guardo el numero de tarjeta si lo introduce el cliente para identificarse -->
<var name="PRM_numTarjetaIntroducido" expr="''"/>

<!-- Nos indica que tipo de identificacion tenemos que lanzar primero (AUTO: LoanDocuments, HIPOTECA: LoanBalance) -->
<var name="PRM_candado" expr="''"/>

<!-- Nos dice si el cliente se ha identificado como AUTO o como HIPOTECA -->
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_tipoProducto" expr="''"/>

<!-- para indicar el la salida lo que ha ocurrido {MAXINT_MENU, MAXINT_VALID} -->
<var name="PRM_info" expr="''"/>

<var name="PRM_bloqueado" expr="''"/>
<var name="PRM_nombreCliente" expr="''"/>
<var name="PRM_momentoDia" expr="''"/>
<var name="PRM_segmento" expr="''"/>
<var name="PRM_tipoBin" expr="''"/>
<var name="PRM_errorloc" expr="''"/>

<var name="bin" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>


<var name="VG_menu" expr="''"/>
<script>
	VG_menu = new Menu();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-IDENTIF"><value expr="'8888888Catch de cuelgue en SUB-MODULO-IDENTIF-TARJETA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>		
		<assign name="PRM_info" expr="''"/>

		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-IDENTIF"><value expr="'Catch de error en SUB-MODULO-IDENTIF-TARJETA'"/></log>
		<log label="MODULO-IDENTIF"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-IDENTIF"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<assign name="PRM_info" expr="''"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_IDENTIF_TARJETA">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_intentosCandado"/>
	<var name="PRM_IN_tipoCandado"/>
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
	<var name="PRM_IN_controlador"/>
	
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'INICIO - identificacion por referencia'"/></log>
		
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_maxNumIntentos" expr="PRM_IN_intentosCandado"/>
		<assign name="PRM_candado" expr="PRM_IN_tipoCandado"/>
		
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
		<assign name="VG_controlador" expr="PRM_IN_controlador"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'CANDADO TARJETA - PRM_maxNumIntentos:' + PRM_maxNumIntentos"/></log>
<!-- 		<log label="MODULO-IDENTIF"><value expr="'identificacion por tarjeta tsec: ' + PRM_cliente.tsec"/></log> -->
		
		<assign name="PRM_numIntentos" expr="0"/>
		
		<goto next="#MENU_SOLICITAR_REFERENCIA"/>		
	
	</block>
	
</form>

<!-- FORMULARIO para solicitar la referencia BNC -->
<form id="MENU_SOLICITAR_REFERENCIA">
	<block>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-SOLICITUD-CONTRATO-BNC'"/>
							
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>		
	</block>

	<subdialog name="MenuSolicitarReferencia" fetchhint="safe" method="post" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
		
		<filled>
			<assign name="VG_menu.codigoRetorno" expr="MenuSolicitarReferencia.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuSolicitarReferencia.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="MenuSolicitarReferencia.PRM_OUT_MENU_respuestaUsuario"/> 
			<assign name="VG_menu.intento" expr="MenuSolicitarReferencia.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuSolicitarReferencia.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuSolicitarReferencia.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="MODULO-IDENTIFICACION-REFERENCIA"><value expr="'VUELTA menu solicitar numero de referencia BNC'"/></log>
					
			<if cond="VG_menu.codigoRetorno == 'OK'">
				
				<!-- ha introducido un numero de tarjeta -->
					
				<log label="MODULO-IDENTIFICACION"><value expr="'ANTES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
				
				<if cond="VG_menu.modoInteraccion == 'voice' || VG_menu.modoInteraccion == 'VOICE'">
					<!-- si el dato se ha introducido por voz, tengo que eliminar los espacios entre los digitos -->
					<script>
						var respuesta = '';
						var arrayResp = new Array();
						arrayResp = VG_menu.respuestaUsuario.split('');
	
						for (var x in arrayResp) {
							if (arrayResp[x] != ' '){
								// si no es espacio en blanco lo concateno
								respuesta = respuesta + arrayResp[x];
							}
						}
						VG_menu.respuestaUsuario = respuesta;
					</script>
						
					<log label="MODULO-IDENTIFICACION"><value expr="'DESPUES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>				
				</if>
					
				<assign name="PRM_numTarjetaIntroducido" expr="VG_menu.respuestaUsuario"/>
				
				<script>
					PRM_cliente.numCliente = "";		
				</script>
					
				<!-- Se comprueba si el Cliente es Tipo EXPRESS por el BIN, antes de la identificacion por el WS ProxyServiceWHY5 -->
				<assign name="PRM_cliente.numCliente" expr="PRM_numTarjetaIntroducido"/>
					
				<log label="MODULO-IDENTIFICACION"><value expr="'Numero de serie del Token: '+ PRM_numTarjetaIntroducido"/></log>
				<log label="MODULO-IDENTIFICACION"><value expr="'Numero de Cliente: ' + PRM_cliente.numCliente"/></log>
					
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>	
				<assign name="PRM_codigoRetorno" expr="'OK'"/>	
				<assign name="PRM_error" expr="''"/>

				<goto next="#MENU_SOLICITAR_TARJETA"/>
			
			<elseif cond="VG_menu.codigoRetorno == 'MAXINT'" />	
			
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
				<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
				<assign name="PRM_error" expr="''"/>
				<assign name="PRM_info" expr="'MAXINT_MENU'"/>
				
				<goto next="#FIN"/>
			
			<else/>
				<!-- codigoRetorno = ERROR o HANGUP-->	
				
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>	
				<assign name="PRM_codigoRetorno" expr="VG_menu.codigoRetorno"/>	
				<assign name="PRM_error" expr="VG_menu.error"/>
				
				<goto next="#FIN"/>
			
			</if>
		</filled>
	</subdialog>
</form>

<!-- FORMULARIO para solicitar el numero de serie del token -->
<form id="MENU_SOLICITAR_TARJETA">
	<block>
		
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-SOLICITUD-NUMERO-SERIE'"/>
							
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>		
	</block>

	<subdialog name="MenuSolicitarTarjeta" fetchhint="safe" method="post" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
		
		<filled>
			
			<assign name="VG_menu.codigoRetorno" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_respuestaUsuario"/> 
			<assign name="VG_menu.intento" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuSolicitarTarjeta.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'VUELTA menu solicitar tarjeta identificacion cliente'"/></log>
					
			<if cond="VG_menu.codigoRetorno == 'OK'">
					
				<!-- ha introducido un numero de tarjeta -->
					
				<log label="MODULO-IDENTIFICACION"><value expr="'ANTES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
				
				<if cond="VG_menu.modoInteraccion == 'voice' || VG_menu.modoInteraccion == 'VOICE'">
					<!-- si el dato se ha introducido por voz, tengo que eliminar los espacios entre los digitos -->
					<script>
						var respuesta = '';
						var arrayResp = new Array();
						arrayResp = VG_menu.respuestaUsuario.split('');
	
						for (var x in arrayResp) {
							if (arrayResp[x] != ' '){
								// si no es espacio en blanco lo concateno
								respuesta = respuesta + arrayResp[x];
							}
						}
						VG_menu.respuestaUsuario = respuesta;
					</script>
						
						<log label="MODULO-IDENTIFICACION"><value expr="'DESPUES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>				
				</if>
					
				<assign name="PRM_numTarjetaIntroducido" expr="VG_menu.respuestaUsuario"/>
				
				<script>
					PRM_cliente.tarjetas = new Array();		
					PRM_cliente.tarjetas[0] = new Tarjeta();
				</script>
					
				<!-- Se comprueba si el Cliente es Tipo EXPRESS por el BIN, antes de la identificacion por el WS ProxyServiceWHY5 -->
<!-- 				<assign name="PRM_numTarjetaIntroducido" expr="PRM_numTarjetaIntroducido + '00000000'"/> -->
				<assign name="PRM_cliente.tarjetas[0].numeroTarjeta" expr="PRM_numTarjetaIntroducido"/>
				<assign name="PRM_cliente.tarjetaSeleccionada" expr="0"/>		
					
				<log label="MODULO-IDENTIFICACION"><value expr="'Numero de referencia BNC: '+ PRM_numTarjetaIntroducido"/></log>
				<log label="MODULO-IDENTIFICACION"><value expr="'Tarjeta seleccionada: ' + PRM_cliente.tarjetaSeleccionada"/></log>
				
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>	
				<assign name="PRM_codigoRetorno" expr="'OK'"/>	
				<assign name="PRM_error" expr="''"/>

<!-- 				<goto next="#RECUPERA_CLIENTE"/> -->
				<goto next="#FIN"/>
			
			<elseif cond="VG_menu.codigoRetorno == 'MAXINT'" />	
			
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
				<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
				<assign name="PRM_error" expr="''"/>
				<assign name="PRM_info" expr="'MAXINT_MENU'"/>
				
				<goto next="#FIN"/>
			
			<else/>
				<!-- codigoRetorno = ERROR o HANGUP-->	
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>	
				<assign name="PRM_codigoRetorno" expr="VG_menu.codigoRetorno"/>	
				<assign name="PRM_error" expr="VG_menu.error"/>
				
				<goto next="#FIN"/>
			
			</if>
		</filled>
	</subdialog>
</form>


<!-- FORMULARIO ejecuta web service ProxyServiceWHY5 para obtener toda la info del cliente en base al numTarjeta -->
<form id="RECUPERA_CLIENTE">	

	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'TARJETA - VOY A RECUPERAR DATOS CLIENTE'"/></log>
	</block>

	<subdialog name="subRecuperaClienteTarjeta" src="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/subIdentifClienteTarjeta" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_numTarjetaIntroducido PRM_cliente.tsec PRM_numIntentos">
			
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="PRM_cliente"/>
<!-- 		<param name="PRM_IN_candado" expr="PRM_tipoIdentifRecuperada"/> -->
		<param name="PRM_IN_candado" expr="PRM_candado"/>
		
			
		<filled>
			<log label="MODULO-IDENTIFICACION"><value expr="'Vuelvo de RecuperarClienteTarjeta'"/></log>
			
			<assign name="PRM_resultadoOperacion" expr="subRecuperaClienteTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subRecuperaClienteTarjeta.PRM_codigoRetorno"/>
			<assign name="PRM_error" expr="subRecuperaClienteTarjeta.PRM_error"/>
			<assign name="PRM_errorloc" expr="subRecuperaClienteTarjeta.PRM_MENERR"/>
			<assign name="PRM_cliente" expr="subRecuperaClienteTarjeta.PRM_cliente"/>
			<assign name="PRM_tipoIdentifRecuperada" expr="subRecuperaClienteTarjeta.PRM_tipoIdentifRecuperada"/>
			<assign name="PRM_bloqueado" expr="subRecuperaClienteTarjeta.PRM_bloqueado"/>
			
			<if cond="PRM_resultadoOperacion == 'OK'">
				<!-- ha ido OK  -->
				<log label="MODULO-IDENTIFICACION"><value expr="'okkkkkkkkkk'+PRM_errorloc"/></log>
						
				<goto next="#CLIENTE_IDENTIF"/>
			<elseif cond="PRM_resultadoOperacion == 'KO'"/>
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<goto next="#FIN"/>
				<else/>
					<!-- ha ido KO -->
					<if cond="PRM_bloqueado == 'SI'">
						<goto next="#CLIENTE_BLOQUEADO"/>
					<else/>
						<if cond="PRM_errorloc == 'ERROR_IDENT'">
							<log label="MODULO-IDENTIFICACION"><value expr="'PRM_errorrrrrrrrrrrr'+PRM_errorloc"/></log>
							<goto next="#FIN"/>
						<else/>
						<log label="MODULO-IDENTIFICACION"><value expr="'okkkk'+PRM_errorloc"/></log>						
							<goto next="#CLIENTE_NO_IDENTIF"/>
						</if>	
					</if>	
				</if>	
			<else/>
				<goto next="#FIN"/>
			</if>	
			
		</filled>
	</subdialog>
</form>



<form id="CLIENTE_IDENTIF">
	<block>
	
		<!-- BIENVENIDA DEL CLIENTE -->
		<assign name="PRM_nombreCliente" expr="PRM_cliente.nombreCliente + ' ' + PRM_cliente.apellido1"/>
		<assign name="PRM_segmento" expr="PRM_cliente.segmento"/>
		<assign name="PRM_tipoBin" expr="PRM_cliente.tarjetas[0].tipoTarjeta"/>
		<assign name="PRM_momentoDia" expr="'${momentoDia}'"/>
		
		<if cond="PRM_segmento == 'R1' || PRM_segmento == 'R2' || PRM_segmento == 'R3' || PRM_segmento == 'R4' " >
			<assign name="PRM_codigoRetorno" expr="'OK'"/>
			<goto next="#FIN"/>
		</if>

		<c:set var="locucionesInformacion" scope="request" value="ID-CLI-INFO-SALUDO-CLIENTE"/>								
		<log label="MODULO-IDENTIFICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>	
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>	
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>	
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>	
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'SALTO A PLANTILLA'"/></log>
			
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion
				PRM_momentoDia PRM_nombreCliente">
		<filled>

			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<!-- Cliente Express despues de identificacion -->
			<!--  <if cond="PRM_tipoBin == 'express'"> -->
				<!--  <log label="MODULO-IDENTIFICACION"><value expr="'Valor del BIN Recuperado: ' + PRM_tipoBin"/></log> -->
				<!--  <assign name="PRM_tipoIdentifRecuperada" expr="'EXPRESS'"/> -->
				<!--  <goto next="#FIN"/> -->
			<!--  </if> -->
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>	
					
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>	
		
	</subdialog>
</form>


<form id="CLIENTE_NO_IDENTIF">
	<block>
		<!-- NO se ha identificado el cliente, le vuelvo a solicitar la tarjeta -->
		
		<c:set var="locucionesInformacion" scope="request" value="ID-CLI-INFO-TARJETA-KO-NUM-INVALIDO"/>								
		<log label="MODULO-IDENTIFICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>	
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>	
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>	
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>	
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>	
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>

		<log label="MODULO-IDENTIFICACION"><value expr="'SALTO A PLANTILLA'"/></log>
			
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>

			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>	
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>	
					
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/>
		
			<!-- compruebo que me queden intentos -->
			<if cond="PRM_numIntentos &lt; PRM_maxNumIntentos">
			
				<!-- quedan intentos -->
				<goto next="#MENU_SOLICITAR_TARJETA"/>	
			<else/>
				<!-- no identifico, resultado MAXINT_VALID -->
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_VALID'"/>
				
				<assign name="PRM_info" expr="'MAXINT_VALID'"/>
				
				<goto next="#FIN"/>	
			</if>	
									
		</filled>	
		
	</subdialog>
</form>	



<form id="CLIENTE_BLOQUEADO">
	<block>
	
		<c:set var="locucionesInformacion" scope="request" value="ID-CLI-INFO-TARJETA-CLIENTE-BLOQUEADO"/>								
		<log label="MODULO-IDENTIFICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>	
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>	
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>	
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>	
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>	
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'SALTO A PLANTILLA'"/></log>
			
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>

			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-IDENTIFICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>	
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>	
					
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>	
		
	</subdialog>
</form>	

<form id="FIN">
	<block>
		<log label="MODULO-IDENTIFICACION"><value expr="'FIN - identificacion por tarjeta'"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-IDENTIFICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		
<!-- 		<return namelist="PRM_errorloc PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado"/> -->
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente"/>
	</block>
</form>

</vxml>
