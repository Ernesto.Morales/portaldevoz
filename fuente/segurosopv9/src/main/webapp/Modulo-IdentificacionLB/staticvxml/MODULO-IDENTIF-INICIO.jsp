<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-IDENTIF-INICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
	

	<meta http-equiv="Expires" content="0"/>
	<property name="fetchaudio" value="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/audio/SPA-silence.wav"/>

	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
	
	<!-- ABALFARO_20170217 -->
	<!-- Cuadro de mando - STAT -->
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Stat.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/FuncionesStat.js"/>
	
	
	<!-- 
	****************************************************
	********* DEFINICION DE VARIABLES GLOBALES *********
	****************************************************
	-->

	
	<var name="VG_codigoRetorno" expr="''"/>
	<var name="VG_cliente" expr="''"/>
	<var name="VG_tipoIdentificacion" expr="''"/>
	<var name="VG_intentosIdentificacion" expr="''"/>
	<var name="VG_menu" expr="''"/>
	
	<var name="VG_candados" expr="''"/>
	<var name="VG_intentosCandados" expr="''"/>
	
<!-- 	<var name="VG_accionMaxintMenu" expr="''"/> -->
<!-- 	<var name="VG_accionMaxintValid" expr="''"/> -->
	
	<var name="VG_gestionExcepciones" expr="''"/>
	
	<!-- elemento de identif del cliente -->
	<var name="VG_llaveAccesoFront" expr="''"/>

	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
	<var name="VG_controlador" expr="''"/>
	
	<!--
	****************************************************
	********* DEFINICION DE VARIABLES LOGGER ***********
	****************************************************
	-->
	<var name="VG_loggerServicio" expr="''"/>
	
	<!-- 
	****************************************************
	*** DEFINICION DE VARIABLES STAT-CUADRO MANDO ******
	****************************************************
	-->
	<!-- ABALFARO_20170217 -->
	<var name="VG_stat" expr="''"/>


	<!-- PARAMETROS DE SALIDA DEL MODULO-IDENTIF -->
	<var name="PRM_OUT_IDENT_resultadoOperacion" expr="''"/>
	<var name="PRM_OUT_IDENT_codigoRetorno" expr="''"/>
	<var name="PRM_OUT_IDENT_error" expr="''"/>
	<var name="PRM_OUT_IDENT_isIdentificado" expr="''"/>
	<var name="PRM_OUT_IDENT_tipoIdentificacion" expr="''"/>
	<var name="PRM_OUT_IDENT_ramaIdentificacion" expr="''"/>
	<var name="PRM_OUT_IDENT_cliente" expr="''"/>
	<var name="PRM_OUT_IDENT_info" expr="''"/>
	<var name="PRM_OUT_IDENT_llaveAccesoFront" expr="''"/>
	<var name="PRM_OUT_IDENT_gestionExcepciones" expr="''"/>
	<!-- ABALFARO_20170217 -->
	<var name="PRM_OUT_IDENT_stat" expr="''"/>
	
	<!-- 
	****************************************************
	** DEFINICIoN DE PROPIEDADES GLOBALES DEL SERVICIO *
	****************************************************
	-->

	<property name="inputmodes" value="dtmf voice"/>
	<property name="termchar" value=""/>

	
	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-IDENTIF"><value expr="'Catch de cuelgue en INICIO'"/></log>
		
		<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_IDENT_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_OUT_IDENT_error" expr="''"/>
		
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'false'"/>
		<assign name="PRM_OUT_IDENT_tipoIdentificacion" expr="''"/>
		<assign name="PRM_OUT_IDENT_cliente" expr="VG_cliente"/>
		<assign name="PRM_OUT_IDENT_llaveAccesoFront" expr="VG_llaveAccesoFront"/>
		
		
			<if cond="PRM_OUT_IDENT_isIdentificado == 'true'" >  
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_IDENTIFICADO';
				</script>
			<else/>
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_IDENTIFICADO';
				</script>
			</if>	
		<assign name="PRM_OUT_IDENT_gestionExcepciones" expr="VG_gestionExcepciones"/>
		
		
		<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/finModuloIdentificacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_tipoIdentificacion PRM_OUT_IDENT_ramaIdentificacion
						PRM_OUT_IDENT_info PRM_OUT_IDENT_llaveAccesoFront PRM_OUT_IDENT_gestionExcepciones VG_stat"/>
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-IDENTIF"><value expr="'Catch de error en INICIO'"/></log>
		
		<log label="MODULO-IDENTIF"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-IDENTIF"><value expr="'MESSAGE: ' + _message"/></log>
			
		<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_IDENT_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_OUT_IDENT_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		
		<assign name="PRM_OUT_IDENT_isIdentificado" expr="'false'"/>
		<assign name="PRM_OUT_IDENT_tipoIdentificacion" expr="''"/>
		<assign name="PRM_OUT_IDENT_cliente" expr="VG_cliente"/>
		<assign name="PRM_OUT_IDENT_llaveAccesoFront" expr="VG_llaveAccesoFront"/>
		
			<if cond="PRM_OUT_IDENT_isIdentificado == 'true'" >  
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_IDENTIFICADO';
				</script>
			<else/>
				<!-- ABALFARO_20170223 relleno excepcion ID_CLIENTE_NO_IDENTIFICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'ID_CLIENTE_NO_IDENTIFICADO';
				</script>
			</if>	
		<assign name="PRM_OUT_IDENT_gestionExcepciones" expr="VG_gestionExcepciones"/>
							
		<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/finModuloIdentificacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_tipoIdentificacion PRM_OUT_IDENT_ramaIdentificacion
						PRM_OUT_IDENT_info PRM_OUT_IDENT_llaveAccesoFront PRM_OUT_IDENT_gestionExcepciones VG_stat"/>

	</catch>
	
		

	<form id="MODULO_IDENTIF_INICIO">
		
		<block>
			<!-- esto NO se ejecuta, siempre se empezara en la pagina INICIO-PARAMS -->
			<submit next="${pageContext.request.contextPath}/MODULO-IDENTIF-LB/inicioParams" method="post" namelist=""/>
			 
		</block>
		
	</form>

</vxml>





