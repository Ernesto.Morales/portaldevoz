<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ZonaPago-OP/staticvxml/MODULO-ZONA-PAGO-OPEN-PAY-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-ZONA-PAGO-OPEN-PAY-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>



<form id="MODULO_IDENTIF_INICIO_PARAMS">

	<var name="PRM_IN_COT_loggerServicio"/>
	<var name="PRM_IN_COT_cliente"/>
	<var name="PRM_IN_COT_intentosIdentificacion"/>
	<var name="PRM_IN_COT_DatosCotizacion"/>
	<var name="PRM_IN_COT_PREDatosCotizacion"/>
	<var name="PRM_IN_COT_RETORNOCTI"/>
	
	<!-- ABALFARO_20170217 -->
	<var name="PRM_IN_COT_stat"/>
	

	<block>
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'INICIO - MODULO-ZONA-PAGO-OPEN-PAY-INICIO-PARAMS'"/></log>

		<assign name="VG_loggerServicio" expr="PRM_IN_COT_loggerServicio"/>
		<assign name="VG_DatosPrecotizacion" expr="PRM_IN_COT_PREDatosCotizacion"/>
		<assign name="VG_DatosCotizacion" expr="PRM_IN_COT_DatosCotizacion"/>
		<assign name="VG_cliente" expr="PRM_IN_COT_cliente"/>
		<assign name="VG_RetornoCTI" expr="PRM_IN_COT_RETORNOCTI"/>
		<assign name="VG_loggerServicio.idElemento" expr="'${idElemento}'"/>
		<assign name="VG_intentosIdentificacion" expr="PRM_IN_COT_intentosIdentificacion"/>
		<!-- ABALFARO_20170217 -->
		<assign name="VG_stat" expr="PRM_IN_COT_stat"/>	
		
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_maxNumIntentos" expr="'${maxIntentosIdsssesion}'"/>
		<assign name="VG_intentosIdentificacion" expr="VG_RetornoCTI.idIntentos"/>	
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
			
			
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION', 'INIT')"/> 
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- ***** STAT: inicio operacion ***** -->
		<assign name="VG_stat" expr="inicioOperacion(VG_stat, '${milisecInicioOperacion}', 'MODULO_IDENTIFICACION')"/> 
		<if cond="PRM_resultadoOperacion == 'OK'">
			<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO/ejecutarModuloZonaPagoOP" method="post"  
				namelist="VG_loggerServicio VG_intentosIdentificacion"/>
		
		<else/>
				
			<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'FIN - MODULO-ZONA-PAGO-OPEN-PAY-INICIO-PARAMS'"/></log>
		
			<assign name="PRM_OUT_IDENT_codigoRetorno" expr="VG_codigoRetorno"/>
			<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
			<assign name="PRM_OUT_IDENT_error" expr="PRM_error"/>
			<assign name="PRM_OUT_IDENT_cliente" expr="VG_cliente"/>
				

						
			<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO/finModuloZonaPagoOP" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado VG_stat"/>
						
		</if>
	
		
	</block>

</form>



</vxml>
