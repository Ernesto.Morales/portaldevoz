<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ZonaPago-OP/staticvxml/MODULO-ZONA-PAGO-OPEN-PAY-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-ZONA-PAGO-OPEN-PAY-EJECUTAR
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="PRM_errorInfo" expr="''"/>
<var name="PRM_loggerServicio" expr="''"/>
<var name="PRM_indiceIntentos" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_intentosCandado" expr="''"/>
<var name="PRM_numIntentos" expr="0"/>
<script>
  	VG_intentosCandados = new Array();
</script>
<form id="INICIO_EJECUTAR">
	<block>
<!-- 		<prompt>  -->
<!-- 			cti  -->
<!-- 			<value expr="VG_RetornoCTI.idIntentos"></value> -->
<!-- 		</prompt> -->
		<assign name="PRM_numIntentos" expr="${intentosIdentificacion}"/>
		<goto next="#COMPROBAR_INTENTOS"/>
	</block>
</form>
<!-- controla el numero de veces que se han solicitado los candados -->
<form id="COMPROBAR_INTENTOS">
	<block>
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'Entramos a comprobar intentos Datos_incorrectos'"/></log>
<!-- 		Se comprueba el numero de intentos -->
		<if cond="PRM_numIntentos &lt; PRM_maxNumIntentos">
				<if cond="PRM_errorInfo == 'PAGO'" >
<!-- 				<assign name="PRM_resultadoOperacion" expr="''"/> -->
<!-- 					<assign name="PRM_numIntentos" expr="PRM_numIntentos - 1"/> -->
					<goto next="#RESULTADO_KO"/>
				<else/>
					<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/>
					<assign name="VG_RetornoCTI.idIntentos" expr="PRM_numIntentos"/>
<!-- 				Ha superado el maximo numero de intentos. NO esta autenticado -->
					<goto next="#MODULO_ZPOPENPAY_EJECUTAR"/>
				</if>
		<else/>
			<assign name="PRM_errorInfo" expr="''"/>
			<assign name="PRM_resultadoOperacion" expr="'KO'"/>
<!-- 			Ha superado el maximo numero de intentos. NO esta autenticado -->
			<goto next="#LOC_MAXIMO_INT"/>
		</if>
	</block>
</form>

<form id="MODULO_ZPOPENPAY_EJECUTAR">	

	
	
	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO/realizarPago" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio  VG_DatosPrecotizacion VG_cliente VG_DatosCotizacion">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_DatosPREcotizacion" expr="VG_DatosPrecotizacion"/>
		<param name="PRM_IN_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<param name="PRM_IN_COT_RETORNOCTI" expr="VG_RetornoCTI"/>
		
		<filled>
			<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'Vuelvo de Identificacion por Tarjeta'"/></log>	
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>				
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion" expr="subIdentifTarjeta.VG_DatosCotizacion"/>
			<assign name="PRM_errorInfo" expr="subIdentifTarjeta.PRM_info"/>
			<assign name="VG_RetornoCTI" expr="subIdentifTarjeta.PRM_OUT_IDENT_DatosRetornoCTI"/>
			<if cond="PRM_resultadoOperacion == 'OK'">	
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,PRM_resultadoOperacion,'PE00')"/>		
			<goto next="#RESULTADO_OK"/>
			<elseif cond="PRM_errorInfo == 'PAGO'" />
<!-- 				<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/> -->
<!-- 				<assign name="PRM_resultadoOperacion" expr="''"/> -->
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,'KO','PI00')"/>	
<!-- 				<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/> -->
				<goto next="#COMPROBAR_INTENTOS"/>						
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,'KO','PI00')"/>
				<assign name="PRM_resultadoOperacion" expr="'FIN'"/>
				<goto next="#RESULTADO_KO"/>				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,'KO','PI00')"/>
				<assign name="PRM_resultadoOperacion" expr="'FIN'"/>
				<goto next="#RESULTADO_KO"/>							
			<else/>	
			
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,PRM_codigoRetorno,'PI00')"/>
					<goto next="#FIN"/>
				<else/>	
					<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,PRM_resultadoOperacion,'PI00')"/>
					<goto next="#FIN"/>
				</if>
				
			</if>	
		</filled>
	</subdialog>

</form>


<form id="RESULTADO_OK">	
	<block>	
		
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'El cliente SI ha sido identificado por n�mero de cliente'"/></log>
		<assign name="PRM_codigoRetorno" expr="'OK'"/>
		<assign name="PRM_resultadoOperacion" expr="'OK'"/>
		<assign name="PRM_error" expr="''"/>
		
		<goto next="#FIN"/>
	</block>
</form>



<form id="RESULTADO_KO">	
	<block>	
		<log label="MODULO-ZONA-PAGO-OPEN-PAY"><value expr="'El cliente NO ha sido identificado por n�mero de cliente'"/></log>
		<goto next="#FIN"/>
		
	</block>
	
</form>


<form id="LOC_MAXIMO_INT">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'La respuesta no es valida'"/></log>
		<c:set var="locucionesInformacion" scope="request" value="AUT-CLI-INFO-PAGO-MAXIMOINT"/>
<%-- 		<log label="MODULO-AUTENTICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log> --%>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>
			</if>
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
		</filled>
	</subdialog>
</form>



<form id="FIN">	
	<block>
		<assign name="PRM_OUT_IDENT_codigoRetorno" expr="PRM_codigoRetorno"/>
		<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
		<assign name="PRM_OUT_IDENT_error" expr="PRM_error"/>
		<assign name="PRM_OUT_IDENT_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<assign name="PRM_OUT_IDENT_infoError" expr="PRM_errorInfo"/>
		<assign name="PRM_OUT_IDENT_DatosRetornoCTI" expr="VG_RetornoCTI"/>					
		<submit next="${pageContext.request.contextPath}/MODULO-ZONAPAGO/finModuloZonaPagoOP" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						VG_stat PRM_OUT_IDENT_DatosCotizacion PRM_OUT_IDENT_infoError PRM_OUT_IDENT_DatosRetornoCTI"/>
										
	</block>
	
</form>


</vxml>
