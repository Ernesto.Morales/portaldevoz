<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-OBTIENE-COTIZACION
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/TransactionCTI.js"/>
<script src="${pageContext.request.contextPath}/scripts/TipoBIN.js"/>
<!-- COTIZACION -->
	<script src="${pageContext.request.contextPath}/scripts/PreCotizacion.js"/>
	<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>
	
<var name="VG_DatosPrecotizacion" expr="''"/>
<var name="VG_DatosCotizacion" expr="''"/>
<var name="VG_loggerServicio" expr="''"/>
<var name="PRM_indiceIntentosMENU" expr="0"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_monto"/>
<var name="PRM_numCliente"/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!-- Numero de intentos para introducir la tarjeta -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>

<var name="idLocucionDeAcuerdo"/>

<!-- guardo el numero de tarjeta si lo introduce el cliente para identificarse -->
<var name="PRM_numTarjetaIntroducido" expr="''"/>
<!-- Respuesta menu confirmar Tarjeta -->
<var name="PRM_Confirmación" expr="''"/>

<!-- Nos indica que tipo de identificacion tenemos que lanzar primero (AUTO: LoanDocuments, HIPOTECA: LoanBalance) -->
<var name="PRM_candado" expr="''"/>

<!-- Nos dice si el cliente se ha identificado como AUTO o como HIPOTECA -->
<var name="PRM_tipoIdentifRecuperada" expr="''"/>
<var name="PRM_tipoProducto" expr="''"/>

<!-- para indicar el la salida lo que ha ocurrido {NO_CLIENTE, MAXINT_MENU, MAXINT_VALID, ROBO_EXTRAVIO} -->
<var name="PRM_info" expr="''"/>

<var name="PRM_bloqueado" expr="''"/>
<var name="PRM_nombreCliente" expr="''"/>
<var name="PRM_momentoDia" expr="''"/>
<var name="PRM_segmento" expr="''"/>
<var name="PRM_tipoBin" expr="''"/>
<var name="PRM_errorloc" expr="''"/>
<var name="VG_RetornoCTI" expr="''"/>
<var name="bin" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_OUT_IDENT_DatosRetornoCTI" expr="''"/>

<var name="VG_menu" expr="''"/>
<script>
	VG_menu = new Menu();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-ZPOPENPAY"><value expr="'Catch de cuelgue en MODULO-ZONA-DATOS-OPEN-PAY'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
		
		<assign name="PRM_info" expr="''"/>
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="MODULO-ZPOPENPAY"><value expr="'Catch de error en SUB-MODULO-ZPOPENPAY'"/></log>
		
		<log label="MODULO-ZPOPENPAY"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-ZPOPENPAY"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVRPAGOOP('+_event + ':'+_message+')'"/>
		
		<assign name="PRM_info" expr="''"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_tipoIdentifRecuperada PRM_info PRM_bloqueado" />		
		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_ZONA_PAGO_OPENPAY">

	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_DatosPREcotizacion"/>
	<var name="PRM_IN_DatosCotizacion"/>
	<var name="PRM_IN_COT_RETORNOCTI"/>
<!-- 	<var name="PRM_IN_COT_stat"/> -->
	<block>
	
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'INICIO - identificacion por tarjeta'"/></log>
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="VG_DatosPrecotizacion" expr="PRM_IN_DatosPREcotizacion"/>
		<assign name="VG_DatosCotizacion" expr="PRM_IN_DatosCotizacion"/>
		<assign name="VG_RetornoCTI" expr="PRM_IN_COT_RETORNOCTI"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>	
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		
		<if	cond="PRM_resultadoOperacion == 'OK'">
			<assign name="VG_RetornoCTI.tipocamp" expr="'${auxCampana}'"/>
			<assign name="VG_RetornoCTI.valorcamp" expr="'${pused}'"/>
			<assign name="VG_RetornoCTI.montocamp" expr="'${premain}'"/>
			<assign name="VG_RetornoCTI.mensaje" expr="'${pamount}'"/>
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,PRM_resultadoOperacion,'PE00')"/>
			<assign name="VG_RetornoCTI" expr="setOperacion(VG_RetornoCTI,'${pcurren}')"/>
			<goto next="#LOC_RESPUESTA"/>	
		<elseif	cond="PRM_resultadoOperacion == 'KO'"/>
			<goto next="#MENU-ERROR-PAGO"/>			
		<else/>
			<goto next="#MENU-ERROR-PAGO"/>	
		</if>
				
	
	
	</block>
</form>

<form id="LOC_RESPUESTA">
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar LA RESPUESTA DE OPEN PAY'"/></log>
		<%-- 		<c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-INICIO-AVISO-PRIVACIDAD"/> --%>
		
		<log label="CONTROLADOR"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${mensaje}'"/>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		
		<filled>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="PRM_error" expr="''"/>
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>
	</subdialog>
</form>

<form id="MENU-ERROR-PAGO">
	<block>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-ACCION-PAGO-KO'"/>					
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="''"/>
		
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
	    <log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>	
	</block>

	<subdialog name="MenuConfirmarTarjeta" fetchhint="safe" method="post"
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp" 
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
		
		<filled>
			
			<assign name="VG_menu.codigoRetorno" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_error"/>
			
			<assign name="VG_menu.respuestaUsuario" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_respuestaUsuario"/> 
			
			<assign name="VG_menu.intento" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuConfirmarTarjeta.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			
			<log label="MODULO-OBTENERCOTIZACION"><value expr="'VUELTA menu solicitar tarjeta identificacion cliente'"/></log>
			
<!-- 		<prompt> -->
<!-- 			regrese -->
<!-- 			<value expr="VG_menu.respuestaUsuario"></value> -->
<!-- 		</prompt> -->
<!-- 		<prompt> -->
<!-- 			intento -->
<!-- 			<value expr="VG_menu.intento"></value> -->
<!-- 		</prompt>	 -->
			
			<if cond="VG_menu.codigoRetorno == 'OK'">
			<assign name="PRM_Confirmación" expr="VG_menu.respuestaUsuario"/>
				<if cond="PRM_Confirmación == '1'">
					<assign name="PRM_info" expr="'PAGO'"/>
<!-- 			<prompt>  -->
<!-- 				regrese -->
<!-- 				<value expr="PRM_info"></value> -->
<!-- 			</prompt> -->
					 <goto next="#FIN"/>
				 <elseif cond="PRM_Confirmación == '2'"/>
					<assign name="PRM_info" expr="'ASESOR'"/>
					<!-- ha introducido que no es cliente -->			
					<goto next="#FIN"/>
				<else/>
					<if cond="PRM_indiceIntentosMENU &lt; 3">
						<assign name="PRM_indiceIntentosMENU" expr="PRM_indiceIntentosMENU + 1"/>
						<goto next="#MENU-ERROR-PAGO"/>
					<else/>
						<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>	
						<assign name="PRM_codigoRetorno" expr="'MAXINT_MENU'"/>
						<assign name="PRM_info" expr="'MAXINT_MENU'"/>
						<goto next="#FIN"/>
						
					</if>
				</if>
				<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>
				<else/>
					<goto next="#FIN"/>
			</if>
			</filled>
			</subdialog>
</form>

<form id="FIN">
	<block>

		<log label="MODULO-OBTENERCOTIZACION"><value expr="'FIN - SUB OBTENER COTIZACION'"/></log>
		
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_error: ' + PRM_error"/></log>		
		<assign name="PRM_OUT_IDENT_DatosRetornoCTI" expr="VG_RetornoCTI"/>					
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_tipoIdentifRecuperada: ' + PRM_tipoIdentifRecuperada"/></log>
		
		<return namelist="PRM_errorloc PRM_resultadoOperacion PRM_codigoRetorno PRM_error VG_DatosCotizacion PRM_info PRM_OUT_IDENT_DatosRetornoCTI"/>
		
	</block>
</form>


</vxml>
