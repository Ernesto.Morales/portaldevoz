<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-OBTIENE-COTIZACION
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<!-- COTIZACION -->
	<script src="${pageContext.request.contextPath}/scripts/PreCotizacion.js"/>
	<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>
	
<var name="VG_DatosPrecotizacion" expr="''"/>
<var name="VG_DatosCotizacion" expr="''"/>
<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<var name="VG_controlador" expr=""/>

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>


	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-IDENTIF"><value expr="'Catch de cuelgue en SUB-MODULO-IDENTIF-TARJETA'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
		
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="MODULO-IDENTIF"><value expr="'Catch de error en SUB-MODULO-OBTENER-COTIZACION'"/></log>
		
		<log label="MODULO-IDENTIF"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-IDENTIF"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVRIDENTARJETA('+_event + ':'+_message+')'"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />		
		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="MODULO_IDENTIF_TARJETA">

	<var name="PRM_IN_COT_loggerServicio"/>
	<var name="PRM_IN_COT_DatosCotizacion"/>
	<var name="PRM_IN_COT_PREDatosCotizacion"/>
<!-- 	<var name="PRM_IN_COT_stat"/> -->
	
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
	<var name="PRM_IN_controlador"/>
	<block>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'INICIO - identificacion por tarjeta'"/></log>
		<assign name="VG_loggerServicio" expr="PRM_IN_COT_loggerServicio"/>
		<assign name="VG_DatosPrecotizacion" expr="PRM_IN_COT_PREDatosCotizacion"/>
		<assign name="VG_DatosCotizacion" expr="PRM_IN_COT_DatosCotizacion"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>	
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<if	cond="PRM_resultadoOperacion == 'OK'">
				<!-- Guardo la info obtenida -->
				<assign name="VG_DatosCotizacion.idCotiza" expr="'${Cotizacion.idCotiza}'"/>
				<assign name="VG_DatosCotizacion.idSession " expr="'${Cotizacion.idSession}'"/>
				<assign name="VG_DatosCotizacion.montoCobro" expr="'${Cotizacion.montoCobro}'"/>
				<assign name="VG_DatosCotizacion.monedaCobro" expr="'${Cotizacion.monedaCobro}'"/>
				<assign name="VG_DatosCotizacion.nombreCliente" expr="'${Cotizacion.nombreCliente}'"/>
				<assign name="VG_DatosCotizacion.apellidosCliente" expr="'${Cotizacion.apellidosCliente}'"/>
				<assign name="VG_DatosCotizacion.telefono" expr="'${Cotizacion.telefono}'"/>
				<assign name="VG_DatosCotizacion.correo" expr="'${Cotizacion.correo}'"/> 
<!-- 				<assign name="VG_DatosCotizacion.afiliacionId" expr="'${Cotizacion.afiliacionId}'"/> -->
				<assign name="VG_DatosCotizacion.descripcionp" expr="'${Cotizacion.descripcion}'"/>
				
			<!-- 		***************************** -->
<!-- 			<prompt> -->
<!-- 			Cotizacion -->
<!-- 			<value expr="VG_DatosCotizacion.idSession"></value> -->
<!-- 			</prompt> -->
			<else/>
				<goto next="#LOC_RESPUESTA" />
				
			</if>
		
		<goto next="#FIN"/>		
	
	</block>
	
</form>

<form id="LOC_RESPUESTA">
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar Respuesta de Obtener Cotizacion'"/></log>
		
		<!-- ABALFARO_20170717 cambio para dar locucion directamente en vez de menu -->
<%-- 		<c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-AVISO-PRIVACIDAD"/> --%>
<%-- 		<c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-INICIO-AVISO-PRIVACIDAD"/> --%>
		
<%-- 		<log label="CONTROLADOR"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log> --%>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${mensaje}'"/>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		
		<filled>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="PRM_error" expr="''"/>
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>
	</subdialog>
</form>

<form id="FIN">
	<block>

		<log label="MODULO-OBTENERCOTIZACION"><value expr="'FIN - SUB OBTENER COTIZACION'"/></log>
		
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_error: ' + PRM_error"/></log>		
			
<!-- 			<prompt bargein="false"> -->
<!-- 			Clienteantes -->
<!-- 				<value expr="VG_DatosCotizacion.telefono"/>  -->
<!-- 			</prompt> -->
			
<!-- 		<log label="MODULO-OBTENERCOTIZACION"><value expr="'PRM_tipoIdentifRecuperada: ' + PRM_tipoIdentifRecuperada"/></log> -->
		
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error VG_DatosCotizacion"/>
		
	</block>
</form>


</vxml>
