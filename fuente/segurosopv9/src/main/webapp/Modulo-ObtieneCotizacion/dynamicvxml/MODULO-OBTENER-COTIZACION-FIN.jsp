<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ObtieneCotizacion/staticvxml/MODULO-OBTENER-COTIZACION-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-OBTENER-COTIZACION-FIN
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<form id="MODULO_IDENTIFICACION_FIN">

	
	<block>
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'INICIO - MODULO-OBTENER-COTIZACION-FIN'"/></log>
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'PRM_OUT_IDENT_resultadoOperacion: ' + PRM_OUT_IDENT_resultadoOperacion"/></log>
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'PRM_OUT_IDENT_codigoRetorno: ' + PRM_OUT_IDENT_codigoRetorno"/></log>
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'PRM_OUT_IDENT_error: ' + PRM_OUT_IDENT_error"/></log>
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'PRM_OUT_IDENT_isIdentificado: ' + PRM_OUT_IDENT_isIdentificado"/></log>
		
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'FIN - MODULO-OBTENER-COTIZACION-FIN'"/></log>
		
<%-- 		<assign name="PRM_OUT_IDENT_cliente" expr="'${numCliente}'"/> --%>

<!-- 		<if cond="PRM_OUT_IDENT_isIdentificado == 'true'"> -->
<!-- 				se ha identificado al cliente, almaceno su numeroCliente -->
<!-- 			<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION', 'INFO','','true','CLIENTE')"/>  -->
<!-- 			***** STAT: guarda id cliente ***** -->
<!-- 				<assign name="VG_stat" expr="guardaIdCliente(VG_stat, PRM_OUT_IDENT_cliente.numCliente)"/>  -->
<!-- 				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, PRM_OUT_IDENT_cliente.segmento)"/>  -->
<!-- 			***** STAT: guarda id cliente *****	 -->
<!-- 			<log label="MODULO-OBTENER-COTIZACION"><value expr="'PRM_OUT_IDENT_cliente.numCliente: ' + PRM_OUT_IDENT_cliente.numCliente"/></log> -->
<!-- 		<else/> -->
<!-- 				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION', 'INFO','','true','CLIENTE')"/>  -->
<!-- 				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_IDENTIFICACION', 'INFO','','false','CLIENTE',PRM_OUT_IDENT_codigoRetorno)"/>  -->
<!-- 				***** STAT: guarda id cliente ***** -->
				<assign name="VG_stat" expr="guardaIdCliente(VG_stat, PRM_OUT_IDENT_cliente.numCliente)"/> 
				<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, PRM_OUT_IDENT_cliente.segmento)"/> 
<!-- 				***** STAT: guarda id cliente *****		 -->
<!-- 		</if> -->
		<assign name="PRM_OUT_IDENT_stat" expr="VG_stat"/>
		
		
		<return namelist="PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_error 
					PRM_OUT_IDENT_isIdentificado PRM_OUT_IDENT_cliente PRM_OUT_IDENT_stat PRM_OUT_IDENT_DatosCotizacion" />		
		
		
	</block>

</form>



</vxml>
