<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-ObtieneCotizacion/staticvxml/MODULO-OBTENER-COTIZACION-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-OBTENER-COTIZACION-EJECUTAR
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>

<var name="PRM_loggerServicio" expr="''"/>
<var name="PRM_indiceIntentos" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_intentosCandado" expr="''"/>
<script>
  	VG_intentosCandados = new Array();
</script>
<form id="MODULO_IDENTIFICACION_NUM_CLIENTE_EJECUTAR">	

	<block>	
	<assign name="PRM_indiceIntentos" expr="0"/>
	<assign name="PRM_intentosCandado" expr="3"/>
	
	</block>
	<subdialog name="subIdentifTarjeta" src="${pageContext.request.contextPath}/MODULO-OBTENER-COTIZACION/subObtenerCotizacion" fetchhint="safe" method="post" 
			namelist="VG_loggerServicio PRM_intentosCandado VG_DatosPrecotizacion">
				
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_intentosCandado" expr="PRM_intentosCandado"/>
		<param name="PRM_IN_tipoCandado" expr="PRM_candado"/>
		<param name="PRM_IN_DatosPREcotizacion" expr="VG_DatosPrecotizacion"/>
		<param name="PRM_IN_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-OBTENER-COTIZACION"><value expr="'Vuelvo de Identificacion por Tarjeta'"/></log>	
			<assign name="PRM_resultadoOperacion" expr="subIdentifTarjeta.PRM_resultadoOperacion"/>
			<assign name="PRM_codigoRetorno" expr="subIdentifTarjeta.PRM_codigoRetorno"/>				
			<assign name="PRM_error" expr="subIdentifTarjeta.PRM_error"/>
			<assign name="VG_DatosCotizacion" expr="subIdentifTarjeta.VG_DatosCotizacion"/>
			<if cond="PRM_resultadoOperacion == 'OK'">				
			<goto next="#RESULTADO_OK"/>
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />
				<assign name="PRM_resultadoOperacion" expr="'FIN'"/>
				<goto next="#RESULTADO_KO"/>
				
			<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />	
				<assign name="PRM_resultadoOperacion" expr="'FIN'"/>
				<goto next="#RESULTADO_KO"/>
										
			<else/>	
			
				<if cond="PRM_codigoRetorno == 'HANGUP'">
					<!-- ha colgado durante la identificacion de la tarjeta -->
					<goto next="#FIN"/>
				<else/>	
					<goto next="#FIN"/>
				</if>
				
			</if>	
		</filled>
	</subdialog>

</form>


<form id="RESULTADO_OK">	
	<block>	
		
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'El cliente SI ha sido identificado por n�mero de cliente'"/></log>
		<assign name="PRM_codigoRetorno" expr="'OK'"/>
		<assign name="PRM_resultadoOperacion" expr="'OK'"/>
		<assign name="PRM_error" expr="''"/>
		
		<goto next="#FIN"/>
	</block>
</form>


<form id="RESULTADO_KO">	
	<block>	
		<log label="MODULO-OBTENER-COTIZACION"><value expr="'El cliente NO ha sido identificado por n�mero de cliente'"/></log>
		<goto next="#FIN"/>
		
	</block>
	
</form>





<form id="FIN">	
	<block>
		<assign name="PRM_OUT_IDENT_codigoRetorno" expr="PRM_codigoRetorno"/>
		<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
		<assign name="PRM_OUT_IDENT_error" expr="PRM_error"/>
		<assign name="PRM_OUT_IDENT_DatosCotizacion" expr="VG_DatosCotizacion"/>
		
					
		
		<submit next="${pageContext.request.contextPath}/MODULO-OBTENER-COTIZACION/finModuloObtenerCotizacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_IDENT_codigoRetorno PRM_OUT_IDENT_resultadoOperacion PRM_OUT_IDENT_error
						PRM_OUT_IDENT_isIdentificado VG_stat PRM_OUT_IDENT_DatosCotizacion"/>
										
	</block>
	
</form>


</vxml>
