<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-AUTENTICACION-VALIDA-CANDADO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>

<var name="VG_loggerServicio" expr="''"/>
<var name="PRM_cliente"/>
<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>
<var name="PRM_tipoError" expr="''"/>
<var name="VG_resultadoOperacions" expr="''"/>
<var name="VG_errors" expr="''"/>
<var name="PRM_MENERR" expr="''"/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de cuelgue en SUB-MODULO-AUTENTICACION-VALIDA-CANDADO'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoError" />
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de error en SUB-MODULO-AUTENTICACION-VALIDA-CANDADO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MESSAGE: ' + _message"/></log>
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoError" />
	</catch>
	

<!-- FORMULARIO para obtener el resultado de la operacion WS Granting Ticket Privado -->
<form id="MODULO_AUTENTICACION_VALIDA_CANDADO">
	
	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'INICIO - peticion Granting Ticket Privado'"/></log>
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="PRM_tipoError" expr="'${tipoError}'"/>
		<if cond="PRM_resultadoOperacion == 'OK'">
<%-- 			<assign name="PRM_cliente.tsecPrivado" expr="'${tsecPrivado}'"/>	 --%>
			<goto next="#FIN"/>				
		<else/>
			<if cond="PRM_codigoRetorno == 'KO'">
				<goto next="#FIN"/>
			</if>	
			<goto next="#FIN"/>
		</if>
		<goto next="#FIN"/>
	</block>
</form>


<form id="FIN">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'FIN - peticion Granting Ticket Privado'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_tipoError"/>
	</block>
</form>

<form id="MENSAJE">
		<log label="MODULO-AUTENTICACION"><value expr="'MENSAJESAUTENTICACION'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MENSAJESlOGGeR'+VG_loggerServicio.idioma"/></log>
	<subdialog name="subBienvenida" method="post" 
	src="${pageContext.request.contextPath}/CONTROLADOR/subMensajeError"
		namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short PRM_MENERR">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio" />
		<filled>
			<assign name="VG_resultadoOperacions" expr="subBienvenida.SUB_resultadoOperacion"/>	
			<assign name="VG_errors" expr="subBienvenida.SUB_error"/>	
			<assign name="PRM_error" expr="'ERROR_HV'"/>		
			
			<log label="CONTROLADOR"><value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacions"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + VG_errors"/></log>	
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + PRM_error"/></log>
			<!-- [AAO-20161215]  -->
			<if	cond="VG_resultadoOperacions == 'HANGUP' ">
				<!-- si es HANGUP debo finalizar llamada -->
				<assign name="VG_resultadoOperacions" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_errors" expr="''"/>
				<goto next="#FIN" />					  
<%-- 				<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post"  --%>
<!-- 							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error  -->
<!-- 							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/> -->
			<else/>
				<goto next="#FIN" />
				
			</if>					
		</filled>
	</subdialog>
</form>

</vxml>
