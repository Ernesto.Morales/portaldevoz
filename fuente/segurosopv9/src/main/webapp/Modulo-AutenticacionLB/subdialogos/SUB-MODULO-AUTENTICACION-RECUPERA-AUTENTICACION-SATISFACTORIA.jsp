<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-MODULO-AUTENTICACION-RECUPERA-TIPO-CANDADO
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
<!-- [NMB 20170227] Cuadro de Mando -->
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Stat.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/FuncionesStat.js"/>

<var name="VG_loggerServicio" expr="''"/>
<var name="PRM_cliente"/>
<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>
<var name="PRM_MENERR" expr="''"/>
<var name="PRM_candado" expr="''"/>
<var name="PRM_codigoCandado" expr="''"/>
<var name="PRM_tipoError" expr="''"/>
<var name="PRM_tipo" expr="''"/>
<var name="VG_resultadoOperacions" expr="''"/>
<var name="VG_errors" expr="''"/>
<var name="PRM_stat" expr="''"/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de cuelgue en SUB-MODULO-AUTENTICACION-RECUPERA-TIPO-CANDADO'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_candado" />
	</catch>
	
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de error en SUB-MODULO-AUTENTICACION-RECUPERA-TIPO-CANDADO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MESSAGE: ' + _message"/></log>		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_candado" />
	</catch>
		

<!-- FORMULARIO para obtener el resultado de la operacion WS getShortReferenceNumber -->
<form id="MODULO_AUTENTICACION_RECUPERA_AUTENTICACION_SATISFACTORIA">
	<var name="PRM_IN_loggerServicio"/>
	<var name="PRM_IN_cliente"/>
	<var name="PRM_IN_vgStat"/>
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'INICIO - peticion ShortReferenceNumber'"/></log>
		<assign name="PRM_stat" expr="PRM_IN_vgStat"/>
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_cliente" expr="PRM_IN_cliente"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		
		<if cond="PRM_resultadoOperacion == 'OK'">
			<assign name="PRM_codigoCandado" expr="'${datosCliente.securityElement}'"/>
			<log label="MODULO-AUTENTICACION"><value expr="'CANDADOOOO ' + ${datosCliente.securityElement}"/></log>
			<goto next="#FIN"/>
			
		<else/>
			<assign name="PRM_tipoError" expr="'${tipoError}'"/>
			<if cond="PRM_codigoRetorno == 'KO'">
				<!-- Tratamos 2 casos especiales de candados. El bloqueado y el de sin elementos -->
				<log label="MODULO-AUTENTICACION"><value expr="'TIPO ERROR'+PRM_tipoError"/></log>
		
				<if cond="PRM_tipoError == 'FOLIO_YA_GENERADO'">
<!-- 					<assign name="PRM_stat" expr="aniadeContador(PRM_stat, 'MODULO_AUTENTICACION_' + VG_loggerServicio.op, 'INFO', 'false','CLIENTE_SIN_ELEMENTOS','CLIENTE_SIN_ELEMENTOS')"/> -->
					<assign name="PRM_MENERR" expr="'AUT_CLIENTE_BLOC'"/>
					<goto next="#MENSAJE"/>
					<!-- Por el momento su servicio se encuentra bloqueado.-->
				<else/>
					<assign name="PRM_MENERR" expr="'ERROR_IDENT'"/>
<!-- 					Lo sentimos, por problemas t�cnicos no podemos seguir atendi�ndole. -->
				</if>
				<goto next="#MENSAJE"/>
			<else/>
				<log label="MODULO-AUTENTICACION"><value expr="'ACA'+PRM_tipoError"/></log>
<!-- 				<assign name="PRM_tipoError" expr="'CLIENTE_BLOQUEADO'"/> -->
				<if cond="PRM_tipoError == 'FOLIO_YA_GENERADO'">
					<assign name="PRM_MENERR" expr="'AUT_CLIENTE_BLOC'"/>
<!-- 					Por el momento su servicio se encuentra bloqueado, para realizar sus consultas y operaciones le invitamos a utilizar Bancomer M�vil o acudir a cualquiera de nuestros cajeros Autom�ticos. -->
				<else/>
					<assign name="PRM_MENERR" expr="'ERROR_IDENT'"/>
<!-- 					Lo sentimos, por problemas t�cnicos no podemos seguir atendi�ndole. -->
				</if>
			</if>
				<!-- Comprobamos el tipo de error -->
			<goto next="#MENSAJE"/>
			
		</if>
		
		<goto next="#FIN"/>
	</block>
	
</form>


<form id="FIN">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'FIN - peticion AuthenticationFactor'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		<return namelist="PRM_stat PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_cliente PRM_candado PRM_tipo"/>
	</block>
</form>

<form id="MENSAJE">
		<log label="MODULO-AUTENTICACION"><value expr="'MENSAJESTIPOCANDADO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MENSAJESlOGGeR'+VG_loggerServicio.idioma"/></log>
	<subdialog name="subBienvenida" method="post" 
	src="${pageContext.request.contextPath}/CONTROLADOR/subMensajeError"
		namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short PRM_MENERR">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio" />
		<filled>
			<assign name="VG_resultadoOperacions" expr="subBienvenida.SUB_resultadoOperacion"/>
			<assign name="VG_errors" expr="subBienvenida.SUB_error"/>
			<assign name="PRM_error" expr="'ERROR_HV'"/>
			<log label="CONTROLADOR"><value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacions"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + VG_errors"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + PRM_error"/></log>
			<!-- [AAO-20161215]  -->
			<if	cond="VG_resultadoOperacions == 'HANGUP' ">
				<!-- si es HANGUP debo finalizar llamada -->
				<assign name="VG_resultadoOperacions" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_errors" expr="''"/>
				<goto next="#FIN" />
			<else/>
				<goto next="#FIN" />
			</if>
		</filled>
	</subdialog>
</form>

</vxml>
