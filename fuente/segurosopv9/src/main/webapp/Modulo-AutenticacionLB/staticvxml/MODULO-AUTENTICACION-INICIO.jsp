<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-AUTENTICACION-INICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

	<meta http-equiv="Expires" content="0"/>
	<property name="fetchaudio" value="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/audio/SPA-silence.wav"/>
	
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>

	<!-- [NMB 20170227] Cuadro de Mando -->
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Stat.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/FuncionesStat.js"/>
	
	<!-- 
	****************************************************
	********* DEFINICION DE VARIABLES GLOBALES *********
	****************************************************
	-->

	<var name="VG_codigoRetorno" expr="''"/>
	<var name="VG_cliente" expr="''"/>
	<var name="VG_tipoAutenticacion" expr="''"/>
	<var name="VG_intentosAutenticacion" expr="''"/>
	<var name="VG_menu" expr="''"/>
	<var name="VG_gestionExcepciones" expr="''"/>
	<var name="VG_infoOperativa" expr="''"/>
	<var name="VG_candados" expr="''"/>
	<var name="VG_intentosCandados" expr="''"/>

	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
	<var name="VG_controlador" expr="''"/>
	
	<!--
	****************************************************
	********* DEFINICION DE VARIABLES LOGGER ***********
	****************************************************
	-->
	<var name="VG_loggerServicio" expr="''"/>
	
	<!-- 
	****************************************************
	*** DEFINICION DE VARIABLES STAT-CUADRO MANDO ******
	****************************************************
	-->
	<!-- [NMB 20170227] Cuadro de Mando -->
	<var name="VG_stat" expr="''"/>
		
	<!-- PARAMETROS DE SALIDA DEL MODULO-AUTENTICACION -->
	<var name="PRM_OUT_AUT_resultadoOperacion" expr="''"/>
	<var name="PRM_OUT_AUT_codigoRetorno" expr="''"/>
	<var name="PRM_OUT_AUT_error" expr="''"/>
	<var name="PRM_OUT_AUT_isAutenticado" expr="''"/>
	<var name="PRM_OUT_AUT_tipoAutenticacion" expr="''"/>
	<var name="PRM_OUT_AUT_cliente" expr="''"/>
	<var name="PRM_OUT_AUT_info" expr="''"/>
	<var name="PRM_OUT_AUT_candado" expr="''"/>
	<var name="PRM_OUT_AUT_gestionExcepciones" expr="''"/>
	<!-- [NMB 20170227] Cuadro de Mando -->
	<var name="PRM_OUT_AUT_stat" expr="''"/>

	<!-- 
	****************************************************
	** DEFINICIoN DE PROPIEDADES GLOBALES DEL SERVICIO *
	****************************************************
	-->

	<property name="inputmodes" value="dtmf voice"/>
	<property name="termchar" value=""/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de cuelgue en INICIO'"/></log>
		
		<assign name="PRM_OUT_AUT_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_AUT_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_OUT_AUT_error" expr="''"/>
		<assign name="PRM_OUT_AUT_isAutenticado" expr="'false'"/>
		<assign name="PRM_OUT_AUT_tipoAutenticacion" expr="''"/>
		<assign name="PRM_OUT_AUT_cliente" expr="VG_cliente"/>
		
			<if cond="PRM_OUT_AUT_isAutenticado == 'true'" >  
				<!-- ABALFARO_20170223 relleno excepcion AUT_CLIENTE_AUTENTICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'AUT_CLIENTE_AUTENTICADO';
				</script>
			<else/>
				<!-- ABALFARO_20170223 relleno excepcion AUT_CLIENTE_NO_AUTENTICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'AUT_CLIENTE_NO_AUTENTICADO';
				</script>
			</if>	
			
		<assign name="PRM_OUT_AUT_gestionExcepciones" expr="VG_gestionExcepciones"/>
			
		<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/finModuloAutenticacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_AUT_codigoRetorno PRM_OUT_AUT_resultadoOperacion PRM_OUT_AUT_error
						PRM_OUT_AUT_isAutenticado PRM_OUT_AUT_tipoAutenticacion PRM_OUT_AUT_info PRM_OUT_AUT_candado 
						PRM_OUT_AUT_gestionExcepciones VG_stat"/>		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de error en INICIO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MESSAGE: ' + _message"/></log>
			
		<assign name="PRM_OUT_AUT_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_OUT_AUT_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_OUT_AUT_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<assign name="PRM_OUT_AUT_isAutenticado" expr="'false'"/>
		<assign name="PRM_OUT_AUT_tipoAutenticacion" expr="''"/>
		<assign name="PRM_OUT_AUT_cliente" expr="VG_cliente"/>			
		<assign name="PRM_OUT_AUT_gestionExcepciones" expr="VG_gestionExcepciones"/>
					
		<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/finModuloAutenticacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_AUT_codigoRetorno PRM_OUT_AUT_resultadoOperacion PRM_OUT_AUT_error
						PRM_OUT_AUT_isAutenticado PRM_OUT_AUT_tipoAutenticacion PRM_OUT_AUT_info PRM_OUT_AUT_candado 
						PRM_OUT_AUT_gestionExcepciones VG_stat"/>		

	</catch>
		

	<form id="MODULO_AUTENTICACION_INICIO">
		<block>
			<!-- esto NO se ejecuta, siempre se empezara en la pagina INICIO-PARAMS -->
			<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/inicioParams" method="post" namelist=""/>
		</block>		
	</form>

</vxml>
