<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-AutenticacionLB/staticvxml/MODULO-AUTENTICACION-INICIO.jsp">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.kranon.logger.logger.all.CommonLoggerKranon"%>
<%@page import="com.kranon.util.UtilidadesLoggerKranon"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-AUTENTICACION-EJECUTAR
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="huellaVocal" expr="''"/>
<var name="tipoTelefono" expr="''"/>
<var name="PRM_loggerServicio" expr="''"/>
<!-- El candado se recupera del WS -->
<var name="PRM_candado" expr="''"/>
<!-- tipo de nip a solicitar -->
<var name="PRM_tipoNip" expr="''"/>
<var name="PRM_tipoError" expr="''"/>
<var name="PRM_numTarjeta" expr="''"/>
<var name="PRM_numTarjetaTerminacion" expr="''"/>
<var name="PRM_numDigitosTerminacion" expr="'5'"/> <!-- TODO! De que Xml puedo leer el numero de digitos q se leen? -->
<var name="PRM_numCliente" expr="''"/>
<!-- <var name="PRM_identifier" expr="''"/> -->
<var name="PRM_idSessionGenerado" expr="''"/>
<!-- controlCTI -->
<var name="PRM_bandera" expr="''"/>
<var name="PRM_tar" expr="''"/>
<var name="PRM_client" expr="''"/>
<var name="PRM_idServShort" expr="''"/>
<var name="PRM_MENERR" expr="''"/>
<var name="VG_resultadoOperacions" expr="''"/>
<var name="VG_errors" expr="''"/>
<var name="PRM_info" expr="''"/>
<!-- Datos Introducidos por el cliente -->
<var name="PRM_datoIntroducido" expr="''"/>
<!-- Control de intentos -->
<var name="PRM_numIntentos" expr="''"/>
<var name="PRM_maxNumIntentos" expr="''"/>
<var name="PRM_indiceIntentos" expr="''"/>
<var name="PRM_indiceCandado" expr="''"/>
<var name="PRM_nombreCandado" expr="''"/>
<var name="PRM_servicio" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<script>
	VG_menu = new Menu();
	VG_candados = new Array();
	VG_intentosCandados = new Array();
</script>

<form id="MODULO_AUTENTICACION_EJECUTAR">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'INICIO - MODULO_AUTENTICACION_EJECUTAR'"/></log>
		<assign name="PRM_numIntentos" expr="0"/>
		<assign name="PRM_indiceIntentos" expr="0"/>
		<!-- Recuperamos el numero de intentos que tiene para cada candado -->
		<c:forTokens items="${intentosAutenticacion}" delims="|" var="intento">
			<script>
				VG_intentosCandados[PRM_indiceIntentos] = '${intento}';
			</script>
			<assign name="PRM_indiceIntentos" expr="PRM_indiceIntentos + 1"/>
		</c:forTokens>
		<assign name="PRM_indiceCandado" expr="0"/>
		
		<!-- Recuperamos los candados que debe pasar el cliente -->
		<c:forTokens items="${tipoAutenticacion}" delims="|" var="candado">
			<script>
				VG_candados[PRM_indiceCandado] = '${candado}';
			</script>
			<assign name="PRM_indiceCandado" expr="PRM_indiceCandado + 1"/>
		</c:forTokens>
		
		<if cond="VG_intentosCandados.length &gt; 0 ">
			<assign name="PRM_maxNumIntentos" expr="VG_intentosCandados[0]"/>
			<assign name="PRM_nombreCandado" expr="VG_candados[0]"/>
		<else/>
			<!-- por defecto ponemos  -->
			<assign name="PRM_maxNumIntentos" expr="3"/>
		</if>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_indiceIntentos ' + PRM_indiceIntentos"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_indiceCandado ' + PRM_indiceCandado"/></log>
		<goto next="#OTP"/>
	</block>
</form>


<!-- Solicita los datos Numero de referencia y numero de serie -->

<!-- Autenticacion por OTP -->
<form id="OTP">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'AUTENTICACION POR OTP'"/></log>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-CLI-MENU-AUTENTICACION-OTP'"/>
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>
	</block>

	<subdialog name="MenuSolicitarToken" fetchhint="safe" method="post" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
			
		<filled>
			<assign name="VG_menu.codigoRetorno" expr="MenuSolicitarToken.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="MenuSolicitarToken.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="MenuSolicitarToken.PRM_OUT_MENU_respuestaUsuario"/>
			<assign name="VG_menu.intento" expr="MenuSolicitarToken.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="MenuSolicitarToken.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="MenuSolicitarToken.PRM_OUT_MENU_nivelConfianza"/>
					
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VUELTA menu solicitar token autenticacion cliente'"/></log>
			
			<if cond="VG_menu.codigoRetorno == 'OK'">
				<log label="MODULO-AUTENTICACION"><value expr="'ANTES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
				
				<if cond="VG_menu.modoInteraccion == 'voice' || VG_menu.modoInteraccion == 'VOICE'">
					<!-- si el dato se ha introducido por voz, tengo que eliminar los espacios entre los digitos -->
					<script>
						var respuesta = '';
						var arrayResp = new Array();
						arrayResp = VG_menu.respuestaUsuario.split('');
						for (var x in arrayResp) {
							if (arrayResp[x] != ' '){
								// si no es espacio en blanco lo concateno
								respuesta = respuesta + arrayResp[x];
							}
						}
						VG_menu.respuestaUsuario = respuesta;
					</script>
					<log label="MODULO-AUTENTICACION"><value expr="'DESPUES VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
				</if>
				<assign name="PRM_datoIntroducido" expr="VG_menu.respuestaUsuario"/>
				<goto next="#VALIDA_CANDADO"/>

			<elseif cond="VG_menu.codigoRetorno == 'MAXINT'" />
				<assign name="PRM_resultadoOperacion" expr="'MAXINT_MENU'"/>
				<assign name="PRM_error" expr="''"/>
				<assign name="PRM_servicio" expr="'GT'"/>
				<goto next="#RESULTADO_KO"/>

			<elseif cond="VG_menu.codigoRetorno == 'HANGUP'" />
				<assign name="PRM_resultadoOperacion" expr="'HANGUP'"/>
				<assign name="PRM_error" expr="VG_menu.error"/>
				<goto next="#RESULTADO_KO"/>
				
			<else/>
				<!-- codigoRetorno = ERROR -->
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_error" expr="VG_menu.error"/>
				<goto next="#RESULTADO_KO"/>
			</if>
		</filled>
	</subdialog>
</form>


<!-- Lanza el validate OTP  -->
<form id="VALIDA_CANDADO">
	<block>
		<assign name="PRM_numCliente" expr="VG_cliente.numCliente"/>
	</block>
	<subdialog name="subValidaCandado" method="post" src="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/subValidaCandado" 
			namelist="VG_loggerServicio PRM_datoIntroducido VG_cliente">
					
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_cliente" expr="VG_cliente"/>
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'Vuelvo de ValidaCandado'"/></log>
			<assign name="VG_cliente" expr="subValidaCandado.PRM_cliente"/>
			<if cond="subValidaCandado.PRM_resultadoOperacion == 'OK'">
				<assign name="VG_cliente.isAutenticado" expr="'true'"/>
				<assign name="VG_cliente.tsecPrivado" expr="'12345'"/>
				
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>
				<goto next="#GUARDA_OTP"/>
				
			<else/>
				<log label="MODULO-AUTENTICACION"><value expr="'Vuelvo de ValidaCandado otp - Datos KO'"/></log>
				<if cond="subValidaCandado.PRM_codigoRetorno == 'KO'">
					<assign name="VG_cliente.isAutenticado" expr="'false'"/>
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					
					<if cond="subValidaCandado.PRM_tipoError == 'DATOS_INCORRECTOS'">
						<assign name="PRM_servicio" expr="'GT'"/>
						<goto next="#COMPROBAR_INTENTOS"/>
					
					<else/>
						<log label="MODULO-AUTENTICACION"><value expr="'Vuelvo de ValidaCandado - Datos Error'"/></log>
						<assign name="PRM_error" expr="subValidaCandado.PRM_error"/>
						<goto next="#FIN"/>
					</if>
				
				<else/>
					<!-- ha habido un error o un cuelgue -->
					<assign name="PRM_resultadoOperacion" expr="'ERROR'"/>
					<assign name="PRM_codigoRetorno" expr="subValidaCandado.PRM_codigoRetorno"/>
					<assign name="PRM_error" expr="subValidaCandado.PRM_error"/>
					<assign name="PRM_MENERR" expr="'ERROR_IDENT'"/>
					<goto next="#MENSAJE"/>
<!-- 					<goto next="#RESULTADO_KO"/> -->
				
				</if>
			</if>

		</filled>
	</subdialog>
</form>


<!-- controla el numero de veces que se han solicitado los candados -->
<form id="COMPROBAR_INTENTOS">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'Entramos a comprobar intentos Datos_incorrectos'"/></log>
		<!-- Se comprueba el numero de intentos -->
		<assign name="PRM_numIntentos" expr="PRM_numIntentos + 1"/>
		<if cond="PRM_numIntentos &lt; PRM_maxNumIntentos">
				<goto next="#LOC_RESPUESTA_INVALIDA"/>
		<else/>
			<!-- Ha superado el maximo numero de intentos. NO esta autenticado -->
			<goto next="#LOC_AUTENTICADO_KO"/>
		</if>
	</block>
</form>


<form id="LOC_RESPUESTA_INVALIDA">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'La respuesta no es valida'"/></log>
		<c:set var="locucionesInformacion" scope="request" value="AUT-CLI-INFO-RESPUESTA-INVALIDA"/>
		<log label="MODULO-AUTENTICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>
			</if>
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#OTP"/>
		</filled>
	</subdialog>
</form>


<form id="LOC_AUTENTICADO_KO">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'El cliente no ha sido autenticado'"/></log>
		
		<c:set var="locucionesInformacion" scope="request" value="AUT-CLI-MAU-INFO-AUTENTICACION-KO"/>
		<log label="MODULO-AUTENTICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="PRM_error" expr="''"/>
				<goto next="#FIN"/>
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<assign name="PRM_resultadoOperacion" expr="'MAXINT_VALID'"/>
			<goto next="#FIN"/>
		</filled>
	</subdialog>
</form>


<form id="RESULTADO_KO">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'El cliente NO ha sido Autenticado'"/></log>
		<assign name="VG_cliente.isAutenticado" expr="'false'"/>
		<goto next="#FIN"/>
	</block>
</form>


<form id="FIN">
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'Entro en INICIO- FIN'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		
		<if cond="PRM_resultadoOperacion == 'OK'">
				<assign name="PRM_codigoRetorno" expr="'OK'"/>
<!-- 				<assign name="PRM_codigoRetorno" expr="'TRANSFER'"/> -->
				<assign name="PRM_info" expr="'AUTENTICACION_OK'"/>
		<elseif cond="PRM_resultadoOperacion == 'MAXINT_VALID'" />
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'OK'"/>
				<assign name="PRM_info" expr="'MAXINT_VALID'"/>
				<!-- ***** STAT: aniade contador Excede intentos Validando AF ***** -->
				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'false', 'AF', PRM_info)"/>
				<assign name="PRM_info" expr="'AU01'"/>
				<!-- ABALFARO_20170131 relleno excepcion AUT_LB_MAXINT_VALID -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
					VG_gestionExcepciones.postAutenticacion[numExcepTotal] = 'AUT_LB_MAXINT_VALID';
				</script>
		<elseif cond="PRM_resultadoOperacion == 'MAXINT_MENU'" />
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'OK'"/>
				<assign name="PRM_info" expr="'MAXINT_MENU'"/>
				<if cond="PRM_servicio == 'AF'">
					<!-- ***** STAT: aniade contador Excede intentos Menu AF ***** -->
					<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'false', 'AF', PRM_info)"/>
				<elseif cond="PRM_servicio == 'GT'"/>
					<!-- ***** STAT: aniade contador Excede intentos Menu GT ***** -->
					<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'false', 'TOKEN', PRM_info)"/>
				</if>
				<assign name="PRM_info" expr="'AU02'"/>
				<!-- ABALFARO_20170131 relleno excepcion AUT_LB_MAXINT_MENU -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
					VG_gestionExcepciones.postAutenticacion[numExcepTotal] = 'AUT_LB_MAXINT_MENU';
				</script>
		<elseif cond="PRM_resultadoOperacion == 'CLIENTE_SIN_ELEMENTOS'" />
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
				<assign name="PRM_info" expr="'CLIENTE_SIN_ELEMENTOS'"/>
				<!-- ***** STAT: aniade contador Cliente sin Elementos ***** -->
				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'false', 'AF', PRM_info)"/>
				<!-- ABALFARO_20170131 relleno excepcion AUT_LB_CLIENTE_SIN_ELEMENTOS -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
					VG_gestionExcepciones.postAutenticacion[numExcepTotal] = 'AUT_LB_CLIENTE_SIN_ELEMENTOS';
				</script>
		<elseif cond="PRM_resultadoOperacion == 'CLIENTE_BLOQUEADO_CANDADO'" />
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
				<assign name="PRM_info" expr="'CLIENTE_BLOQUEADO_CANDADO'"/>
				<!-- ***** STAT: aniade contador Bloqueado AF ***** -->
				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'false', 'AF', PRM_info)"/>
				<!-- ABALFARO_20170131 relleno excepcion AUT_LB_CLIENTE_BLOQUEADO_CANDADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
					VG_gestionExcepciones.postAutenticacion[numExcepTotal] = 'AUT_LB_CLIENTE_BLOQUEADO_CANDADO';
				</script>
		<elseif cond="PRM_resultadoOperacion == 'HANGUP'" />
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<elseif cond="PRM_resultadoOperacion == 'KO'" />
			<if cond="PRM_codigoRetorno == 'HANGUP'">
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
			<else/>
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
			</if>
		<else/>
				<assign name="PRM_resultadoOperacion" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
<!-- 				creo que aqui es  -->
				<assign name="PRM_error" expr="ERROR"/>
		</if>
		
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_info: ' + PRM_info"/></log>
		<assign name="PRM_OUT_AUT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
		<assign name="PRM_OUT_AUT_codigoRetorno" expr="PRM_codigoRetorno"/>
		<assign name="PRM_OUT_AUT_error" expr="PRM_error"/>
		<assign name="PRM_OUT_AUT_isAutenticado" expr="VG_cliente.isAutenticado"/>
		<assign name="PRM_OUT_AUT_tipoAutenticacion" expr="VG_tipoAutenticacion"/>
		<assign name="PRM_OUT_AUT_cliente" expr="VG_cliente"/>
		<assign name="PRM_OUT_AUT_info" expr="PRM_info"/>
		<assign name="PRM_OUT_AUT_candado" expr="PRM_candado"/>
		
			<if cond="PRM_OUT_AUT_isAutenticado == 'true'" >
				<!-- ABALFARO_20170223 relleno excepcion AUT_CLIENTE_AUTENTICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'AUT_CLIENTE_AUTENTICADO';
				</script>
			<else/>
				<!-- ABALFARO_20170223 relleno excepcion AUT_CLIENTE_NO_AUTENTICADO -->
				<script>
					var numExcepTotal = VG_gestionExcepciones.postInformacion.length;
					VG_gestionExcepciones.postInformacion[numExcepTotal] = 'AUT_CLIENTE_NO_AUTENTICADO';
				</script>
			</if>
			
		<assign name="PRM_OUT_AUT_gestionExcepciones" expr="VG_gestionExcepciones"/>
		
	<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/finModuloAutenticacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_AUT_codigoRetorno PRM_OUT_AUT_resultadoOperacion PRM_OUT_AUT_error
						PRM_OUT_AUT_isAutenticado PRM_OUT_AUT_tipoAutenticacion PRM_OUT_AUT_info PRM_OUT_AUT_candado 
						PRM_OUT_AUT_gestionExcepciones VG_stat"/>
	</block>
</form>


<form id="MENSAJE">
		<block>
		<log label="MODULO-AUTENTICACION"><value expr="'La respuesta no es valida'"/></log>
<%-- 		<c:set var="locucionesInformacion" scope="request" value="AUT-CLI-INFO-RESPUESTA-INVALIDA"/> --%>
<%-- 		<log label="MODULO-AUTENTICACION"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log> --%>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP'"/>
		
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					<assign name="PRM_resultadoOperacion" expr="'KO'"/>
					<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
					<assign name="PRM_error" expr="''"/>
					<goto next="#FIN"/>
			</if>
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
		</filled>
	</subdialog>
</form>

<!-- Lanza el validate OTP  -->
<form id="GUARDA_OTP">
	<block>
<!-- 		<prompt> -->
<!-- 			GUARDA OTP -->
<!-- 			<value expr="PRM_datoIntroducido"></value> -->
<!-- 		</prompt>	 -->
	</block>
	<subdialog name="subGuardaOTP" method="post" src="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/subGuardaOTP" 
			namelist="VG_loggerServicio PRM_datoIntroducido PRM_numTarjeta VG_cliente">		
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		<filled>
			<log label="MODULO-AUTENTICACION"><value expr="'Vuelvo de ValidaCandado'"/></log>
			<if	cond="subGuardaOTP.PRM_resultadoOperacion == 'HANGUP' ">
				<!-- si es HANGUP debo finalizar llamada -->
				<assign name="VG_resultadoOperacions" expr="'KO'"/>
				<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_errors" expr="''"/>
				<goto next="#FIN"/>			
			<elseif cond="subGuardaOTP.PRM_resultadoOperacion == 'OK'"/>
				<assign name="PRM_resultadoOperacion" expr="'OK'"/>
				<goto next="#FIN"/>				
			<else/>
				<log label="MODULO-AUTENTICACION"><value expr="'Vuelvo de GUARDAR OTP - Datos KO'"/></log>
				<if cond="subGuardaOTP.PRM_codigoRetorno == 'KO'">
					<assign name="PRM_resultadoOperacion" expr="'OK'"/>
					<goto next="#FIN"/>
				<else/>
					<assign name="PRM_resultadoOperacion" expr="'OK'"/>
					<goto next="#FIN"/>
				</if>
			</if>

		</filled>
	</subdialog>
</form>
</vxml>
