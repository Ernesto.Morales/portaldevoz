<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-AutenticacionLB/staticvxml/MODULO-AUTENTICACION-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-AUTENTICACION-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>

<form id="MODULO_AUTENTICACION_INICIO_PARAMS">
	<var name="PRM_IN_AUT_loggerServicio"/>
	<var name="PRM_IN_AUT_tipoAutenticacion"/>
	<var name="PRM_IN_AUT_intentosAutenticacion"/>
	<var name="PRM_IN_AUT_cliente"/>
	<var name="PRM_IN_AUT_operativa"/>
	<var name="PRM_IN_AUT_ramaIdentificacion"/>
	<var name="PRM_IN_AUT_gestionExcepciones"/>
	<!-- [NMB 20170227] Cuadro de Mando -->
	<var name="PRM_IN_AUT_stat"/>
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
	<var name="PRM_IN_AUT_controlador"/>
	
	<block>
		<log label="MODULO-AUTENTICACION"><value expr="'INICIO - MODULO-AUTENTICACION-INICIO-PARAMS'"/></log>

		<assign name="VG_loggerServicio" expr="PRM_IN_AUT_loggerServicio"/>
		<assign name="VG_cliente" expr="PRM_IN_AUT_cliente"/>
		<assign name="VG_tipoAutenticacion" expr="PRM_IN_AUT_tipoAutenticacion"/>
		<assign name="VG_intentosAutenticacion" expr="PRM_IN_AUT_intentosAutenticacion"/>
		<assign name="VG_gestionExcepciones" expr="PRM_IN_AUT_gestionExcepciones"/>
		<assign name="VG_infoOperativa" expr="PRM_IN_AUT_operativa"/>
		<!-- [NMB 20170227] Cuadro de Mando -->
		<assign name="VG_stat" expr="PRM_IN_AUT_stat"/>
		<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
		<assign name="VG_controlador" expr="PRM_IN_AUT_controlador"/>
		<assign name="VG_loggerServicio.idElemento" expr="'${idElemento}'"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_error: ' + PRM_error"/></log>
		<!-- inicializamos la autenticacion a false -->
		<assign name="PRM_OUT_AUT_isAutenticado" expr="'false'"/>
		<assign name="VG_cliente.isAutenticado" expr="'false'"/>

		<!-- [NMB 20170227] Cuadro de Mando -->
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INIT')"/>
		<!-- ***** STAT: aniade contador ***** -->
<!-- 		<assign name="VG_stat" expr="guardaSegmentoCliente(VG_stat, 'YAD')"/> -->
		<!-- ***** STAT: inicio operacion ***** -->
		<assign name="VG_stat" expr="inicioOperacion(VG_stat, '${milisecInicioOperacion}', 'MODULO_AUTENTICACION')"/>
		<!-- ***** STAT: inicio operacion ***** -->
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'INFO', '', 'true', 'TOKEN')"/>
		<!-- ***** STAT: aniade contador ***** -->
			
		<if cond="PRM_resultadoOperacion == 'OK'">
			<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/ejecutarModuloAutenticacion" method="post"  
				namelist="VG_loggerServicio VG_tipoAutenticacion VG_intentosAutenticacion VG_stat"/>
		<else/>
			<log label="MODULO-AUTENTICACION"><value expr="'FIN - MODULO-AUTENTICACION-INICIO-PARAMS '"/></log>
		
			<assign name="PRM_OUT_AUT_codigoRetorno" expr="VG_codigoRetorno"/>
			<assign name="PRM_OUT_AUT_resultadoOperacion" expr="PRM_resultadoOperacion"/>
			<assign name="PRM_OUT_AUT_error" expr="PRM_error"/>
			<assign name="PRM_OUT_AUT_isAutenticado" expr="'false'"/>
			<assign name="PRM_OUT_AUT_tipoAutenticacion" expr="VG_tipoAutenticacion"/>
			<assign name="PRM_OUT_AUT_cliente" expr="VG_cliente"/>
			
			<!-- ABALFARO_20170223 relleno excepcion AUT_CLIENTE_NO_AUTENTICADO -->
			<script>
				var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
				VG_gestionExcepciones.postAutenticacion[numExcepTotal] = 'AUT_CLIENTE_NO_AUTENTICADO';
			</script>
			
			<assign name="PRM_OUT_AUT_gestionExcepciones" expr="VG_gestionExcepciones"/>
			
		<submit next="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/finModuloAutenticacion" method="post"  
				namelist="VG_loggerServicio PRM_OUT_AUT_codigoRetorno PRM_OUT_AUT_resultadoOperacion PRM_OUT_AUT_error
						PRM_OUT_AUT_isAutenticado PRM_OUT_AUT_tipoAutenticacion PRM_OUT_AUT_info PRM_OUT_AUT_candado 
						PRM_OUT_AUT_gestionExcepciones VG_stat"/>
		</if>
	</block>
</form>

</vxml>
