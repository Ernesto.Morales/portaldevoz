<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Modulo-AutenticacionLB/staticvxml/MODULO-AUTENTICACION-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MODULO-AUTENTICACION-FIN
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<form id="MODULO_AUTENTICACION_FIN">
	<block>				
		<log label="MODULO-AUTENTICACION"><value expr="'INICIO - MODULO-AUTENTICACION-FIN'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_OUT_AUT_resultadoOperacion: ' + PRM_OUT_AUT_resultadoOperacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_OUT_AUT_codigoRetorno: ' + PRM_OUT_AUT_codigoRetorno"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_OUT_AUT_error: ' + PRM_OUT_AUT_error"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_OUT_AUT_isAutenticado: ' + PRM_OUT_AUT_isAutenticado"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'PRM_OUT_AUT_tipoAutenticacion: ' + PRM_OUT_AUT_tipoAutenticacion"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'FIN - MODULO-AUTENTICACION-FIN'"/></log>
		
		<!-- ABALFARO_20170315 -->
		<!-- ***** STAT: aniade contador ***** -->
			<!-- solo aniadiremos el contador si esta autenticado o si no lo esta pero la info viene rellena -->
<!-- 			<if cond="PRM_OUT_AUT_isAutenticado == 'true' || (PRM_OUT_AUT_isAutenticado == 'false' &amp;&amp; PRM_OUT_AUT_info != '')"> -->
<!-- 				<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION_' + VG_loggerServicio.op, 'INFO', '', -->
<!-- 									PRM_OUT_AUT_isAutenticado, PRM_OUT_AUT_candado, PRM_OUT_AUT_info)"/> -->
<!-- 			</if> -->
		<!-- ***** STAT: aniade contador ***** -->
		
		<!-- [NMB 20170227] Cuadro de Mando -->
		<!-- ***** STAT: aniade contador ***** -->
		<assign name="VG_stat" expr="aniadeContador(VG_stat, 'MODULO_AUTENTICACION', 'END', PRM_OUT_AUT_codigoRetorno)"/>
		<!-- ***** STAT: aniade contador ***** -->

		<!-- ***** STAT: fin operacion ***** -->
		<assign name="VG_stat" expr="finOperacion(VG_stat, '${milisecFinOperacion}', 'MODULO_AUTENTICACION', PRM_OUT_AUT_resultadoOperacion, PRM_OUT_AUT_codigoRetorno)"/> 
		<!-- ***** STAT: fin operacion ***** -->
		<!-- [NMB 20170227] Cuadro de Mando -->
		<assign name="PRM_OUT_AUT_stat" expr="VG_stat"/>		
		<return namelist="PRM_OUT_AUT_resultadoOperacion PRM_OUT_AUT_codigoRetorno PRM_OUT_AUT_error 
					PRM_OUT_AUT_isAutenticado PRM_OUT_AUT_tipoAutenticacion PRM_OUT_AUT_cliente PRM_OUT_AUT_info
					PRM_OUT_AUT_gestionExcepciones PRM_OUT_AUT_stat PRM_OUT_AUT_isAutenticado" />		
		
	</block>
</form>

</vxml>
