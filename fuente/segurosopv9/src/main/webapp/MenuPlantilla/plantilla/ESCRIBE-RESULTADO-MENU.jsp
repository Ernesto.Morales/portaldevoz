<?xml version="1.0" encoding="ISO-8859-1"?>

<%@page import="com.kranon.menuplantilla.UtilidadesPlantillaMenu"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.kranon.logger.event.ParamEvent"%>
<%@page import="com.kranon.logger.serv.CommonLoggerService"%>
<%@page import="com.kranon.logger.monit.CommonLoggerMonitoring"%>
<%@page import="com.kranon.logger.utilidades.Constantes"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Collections"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.kranon.utilidades.UtilidadesBeans"%>

<%
	
// ** PARAMETROS DE ENTRADA
String idLlamada = request.getParameter("PRM_idInvocacion");
String idServicio = request.getParameter("PRM_idServicio");
String idElemento = request.getParameter("PRM_idElemento");
String idMenu = request.getParameter("PRM_idMenu");
String respuestaSensible = request.getParameter("PRM_respuestaSensible");
String paramAdicional = request.getParameter("PRM_paramAdicional");
String codigoRetorno = request.getParameter("PRM_OUT_MENU_codigoRetorno");
String error = request.getParameter("PRM_OUT_MENU_error");
// ** PARAMETROS DE SALIDA
String resultadoOperacion = "";

%>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	ESCRIBE-RESULTADO-MENU
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="SUB_resultadoOperacion" expr="''"/>
<var name="SUB_error" expr="''"/>
<var name="CATCH_resultadoOperacion" expr="''"/>
<var name="CATCH_error" expr="''"/>

<!--
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
<!-- KRANON: Se captura cualquier error que pueda producirse, y se devuelve a la aplicacion un codigo de retorno ERROR.-->
<catch event="error">
	<log label="MENU-RESULTADO"><value expr="'ERROR: ' + _event + ':'+_message"/></log>
	<assign name="CATCH_resultadoOperacion" expr="'ERROR'"/>
	<assign name="CATCH_error" expr="'ERROR_IVR(' +  _event + ':'+_message + ')'"/>
	<goto next="#FIN"/>
</catch>


<!--
******************************************
********** CAPTURA DEL CUELGUE ***********
******************************************
-->
<!-- KRANON: Se captura el cuelgue, y se devuelve a la aplicacion un codigo de retorno CUELGUE.-->
<catch event="connection.disconnect.hangup">
	<log label="MENU-RESULTADO"><value expr="'HANGUP'"/></log>
	<assign name="CATCH_resultadoOperacion" expr="'HANGUP'"/>
	<assign name="CATCH_error" expr="''"/>
	<goto next="#FIN"/>
</catch>


<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="INICIO">
		<block>				
			<log label="MENU-RESULTADO"><value expr="'INICIO - MENU-RESULTADO'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_idInvocacion: <%=idLlamada%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_idServicio: <%=idServicio%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_idElemento: <%=idElemento%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_idMenu: <%=idMenu%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_respuestaSensible: <%=respuestaSensible%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_paramAdicional: <%=paramAdicional%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_OUT_MENU_codigoRetorno: <%=codigoRetorno%>'"/></log>
			<log label="MENU-RESULTADO"><value expr="'PRM_OUT_MENU_error: <%=error%>'"/></log>
		<%
			CommonLoggerService log = null;
			try {	
				/** INICIALIZAR LOG **/
				log = new CommonLoggerService(idServicio);
				log.inicializar(idLlamada, idServicio, idElemento);
	
				/** INICIO EVENTO - INICIO DE MODULO **/
				//log.initModuleService(idMenu, null);
				/** FIN EVENTO - INICIO DE MODULO **/
				
				/** INICIO EVENTO - INICIO DE ACCION **/
				//log.actionEvent(idMenu, "INIT", null);
				/** FIN EVENTO - INICIO DE MODULO **/
				
				// variables para trza resumen
				String resultadoFinalMenu = "";
				String recDisponibleFinalMenu = "";
				String recUtilizadoFinalMenu = "";
				String respuestaFinalMenu = "";
				String respuestaRawFinalMenu = "";
				
				ArrayList<String> listaTrazas = new ArrayList<String>();
				
				Enumeration enumeration = request.getParameterNames();
				while (enumeration.hasMoreElements()) {
					String parameterName = (String) enumeration.nextElement();
					if (parameterName.contains("PRM_OUT_MENU_trazas.intentos.")) {
						listaTrazas.add(parameterName);
					}
				}
				
				log.actionEvent("PRM_OUT_MENU_trazas", "tamanio=" + listaTrazas.size(), null);
				
				
				// ordeno la lista de trazas
				// Collections.sort(listaTrazas);
				ArrayList<String> listaTrazasAux = UtilidadesPlantillaMenu.ordenaArrayTrazas(listaTrazas);
				if(listaTrazasAux != null){
					// si ha ido bien la ordenacion
					listaTrazas = listaTrazasAux;
				}
				
				
				
				int numIntentoActual = 0;
				int numElementoActual = 0;
				for(String parameterName: listaTrazas){
										
	
					if (parameterName.contains("PRM_OUT_MENU_trazas.intentos.")) {
						// es una traza
						if (!parameterName.contains("PRM_OUT_MENU_trazas.intentos." + numIntentoActual)) {
							// hemos pasado al siguiente intento
							numIntentoActual = numIntentoActual + 1;
							numElementoActual = 0;
							
						}
						
						String cabeceraTraza = "PRM_OUT_MENU_trazas.intentos." + numIntentoActual + ".elementos." + numElementoActual;
						if (parameterName.contains(cabeceraTraza)) {
	
							if (parameterName.contains(cabeceraTraza + ".idLocucion")) {
								// es una locucion
								String locucion = request.getParameter(cabeceraTraza + ".idLocucion");
								String tipoLocucion = request.getParameter(cabeceraTraza + ".tipoLocucion");
	
								/** INICIO EVENTO - LOCUCION **/
								log.speechEvent(tipoLocucion, locucion, null);
								/** FIN EVENTO - LOCUCION **/
	
								// paso al siguiente elemento
								numElementoActual = numElementoActual + 1;
	
							} else if (parameterName.contains(cabeceraTraza + ".idMenu")) {
								// es un intento de menu
	
								int numIntento = Integer.parseInt((request.getParameter(cabeceraTraza + ".numIntento") == null) ? "0" : request
										.getParameter(cabeceraTraza + ".numIntento"));
								String recDisponible = request.getParameter(cabeceraTraza + ".recDisponible");
								String recUtilizado = request.getParameter(cabeceraTraza + ".recUtilizado");
								String respuesta = request.getParameter(cabeceraTraza + ".respuesta");
								String respuestaRaw = request.getParameter(cabeceraTraza + ".respuestaRaw");
								String codRetorno = (respuesta.equalsIgnoreCase("NOMATCH") || respuesta.equalsIgnoreCase("NOINPUT")
														|| respuesta.equalsIgnoreCase("ERROR_ASR_NO_RESOURCE") 
														|| respuesta.equalsIgnoreCase("ERROR_BAD_GRAMMAR")
														|| respuesta.equalsIgnoreCase("ERROR_RECOGNITION") 
														|| respuesta.equalsIgnoreCase("ERROR_MAXSPEECHTIMEOUT")
														|| respuesta.equalsIgnoreCase("ERROR_TTS_NO_RESOURCE")
														|| respuesta.equalsIgnoreCase("ERROR")
														|| respuesta.equalsIgnoreCase("HANGUP")
														) ? "KO" : "OK";
								String respuestaCodificada = "";
								if(respuesta.equals("NOMATCH") || respuesta.equals("NOINPUT") || respuesta.equals("HANGUP")){
									respuestaCodificada = respuesta;
								} else {
									respuestaCodificada = (UtilidadesBeans.cifrar(respuesta,4));
								}
								log.comment("respuesta="+ respuestaCodificada);
								log.comment("codRetorno=" + codRetorno);
								
								/** INICIO EVENTO - STATISTIC respuesta **/
								// ABALFARO_20170224 sacamos traza INFO de la respuesta debido a que el informe debe obtener el contrato/tarjeta
								ArrayList<ParamEvent> parametrosAdicStatResp = new ArrayList<ParamEvent>();
								// ABALFARO_20170404
								if(respuesta.equalsIgnoreCase("NOMATCH") && !respuestaRaw.equals("")){
									// si la respuesta es nomatch pero el raw viene relleno lo traceo, procede de un no match forzado por excepcion
									parametrosAdicStatResp.add(new ParamEvent("respuesta", respuestaRaw));
								} else {
// 									parametrosAdicStatResp.add(new ParamEvent("respuesta", respuesta));
									parametrosAdicStatResp.add(new ParamEvent("respuesta", respuestaCodificada));
								}
								log.statisticEvent(idMenu, "RESPUESTA", parametrosAdicStatResp);
								/** FIN EVENTO - STATISTIC respuesta **/
								
								if(codRetorno.equals("OK")){
									// es un filled en el menu
									if(respuestaSensible != null && respuestaSensible.equalsIgnoreCase("ON")){
										// si viene informado a ON o ON, cambio la respuesta por asteriscos
										respuesta = StringUtils.repeat("*", respuesta.length());
										respuestaRaw = StringUtils.repeat("*", respuestaRaw.length());
									}
								}
								/** INICIO EVENTO - DIALOGO **/
								ArrayList<ParamEvent> parametrosAdicionales = null;
								if(respuestaRaw != null && !respuestaRaw.equals("")){
									parametrosAdicionales = new ArrayList<ParamEvent>();
									parametrosAdicionales.add(new ParamEvent("raw", respuestaRaw));
								}
								log.dialogueEvent(idMenu, recDisponible, recUtilizado, numIntento, respuesta, codRetorno, parametrosAdicionales);
								/** FIN EVENTO - DIALOGO **/
	
								// ************************************************************************************************************
								// [20161219-NMB] INICIO - SE INCLUYEN TRAZA DE STATISTICS POR INTENTO QUE HAY QUE CONTABILIZAR EN LOS INFORMES
								// ************************************************************************************************************																
								ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
								parametrosAdicionalesStat.add(new ParamEvent("recDisponible", recDisponible));
								parametrosAdicionalesStat.add(new ParamEvent("recUtilizado", recUtilizado));
								parametrosAdicionalesStat.add(new ParamEvent("numIntento", String.valueOf(numIntento)));
								parametrosAdicionalesStat.add(new ParamEvent("respuesta", respuesta));
								if (respuestaRaw != null && !respuestaRaw.equals("")) {
									parametrosAdicionalesStat.add(new ParamEvent("raw", respuestaRaw));
								}
								parametrosAdicionalesStat.add(new ParamEvent("paramAdicional", paramAdicional));
								log.statisticEvent(idMenu, codRetorno, parametrosAdicionalesStat);
								// *********************************************************************************************************
								// [20161219-NMB] FIN - SE INCLUYEN TRAZA DE STATISTICS POR INTENTO QUE HAY QUE CONTABILIZAR EN LOS INFORMES
								// *********************************************************************************************************		
								
								// paso al siguiente elemento
								numElementoActual = numElementoActual + 1;
								
								// guardo este resultado Final
								if(codRetorno.equals("KO")){
									// el resultado es la respuesta con lo que ha ocurrido
									resultadoFinalMenu = respuesta;
								} else {
									resultadoFinalMenu = "OK";
								}
								recDisponibleFinalMenu = recDisponible;
								recUtilizadoFinalMenu = recUtilizado;
								respuestaFinalMenu = respuesta;
								respuestaRawFinalMenu = respuestaRaw;
	
								// ****************************************************************
								// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
								// ****************************************************************
 								if(respuestaFinalMenu.equalsIgnoreCase("ERROR_ASR_NO_RESOURCE") ||
										respuestaFinalMenu.equalsIgnoreCase("ERROR_BAD_GRAMMAR") || 
										respuestaFinalMenu.equalsIgnoreCase("ERROR_RECOGNITION") ||
										respuestaFinalMenu.equalsIgnoreCase("ERROR_MAXSPEECHTIMEOUT")){
									// MONITORIZACION - ASR 
									CommonLoggerMonitoring logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
									logWarning.warningASR(idServicio, idElemento, respuestaFinalMenu.replaceFirst("ERROR_", ""), "", null);
									// MONITORIZACION - ASR  
								}								
								if(respuestaFinalMenu.equalsIgnoreCase("ERROR_TTS_NO_RESOURCE")){
									// MONITORIZACION - TTS  
									CommonLoggerMonitoring logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
									logWarning.warningTTS(idServicio, idElemento, respuestaFinalMenu.replaceFirst("ERROR_", ""), "", null);
									// MONITORIZACION - TTS 
								} 
								// ****************************************************************
								// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
								// ****************************************************************
							}
							// else, estoy recorriendo los atributos que ya he
							// escrito del elemento actual
	
						} 
					}
				}
				
				// [20161214] contingencia para informes
				if(resultadoFinalMenu.equals("") && recDisponibleFinalMenu.equals("") && recUtilizadoFinalMenu.equals("") && respuestaFinalMenu.equals("")
						&& respuestaRawFinalMenu.equals("")){
					// algo fallo en la plantilla que no se di� ningun intento de menu 
					resultadoFinalMenu = "ERROR";	
				}
				
				/** INICIO EVENTO - STATISTIC resumen menu **/
				ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
				parametrosAdicionalesStat.add(new ParamEvent("recDisponible", recDisponibleFinalMenu));
				parametrosAdicionalesStat.add(new ParamEvent("recUtilizado", recUtilizadoFinalMenu));
				parametrosAdicionalesStat.add(new ParamEvent("respuesta", respuestaFinalMenu));
				parametrosAdicionalesStat.add(new ParamEvent("respuestaRaw", respuestaRawFinalMenu));
				parametrosAdicionalesStat.add(new ParamEvent("codigoRetorno", codigoRetorno));
				if (error != null && !error.equals("")) {
					parametrosAdicionalesStat.add(new ParamEvent("error", error));
				}
				parametrosAdicionalesStat.add(new ParamEvent("paramAdicional", paramAdicional));
				log.statisticEvent(idMenu, resultadoFinalMenu, parametrosAdicionalesStat);
				/** FIN EVENTO - STATISTIC resumen menu **/
				
				resultadoOperacion = "OK";
				
			} catch (final Exception e) {
	
				if (log == null) {
					/** INICIALIZAR LOG **/
					log = new CommonLoggerService(idServicio);
					log.inicializar(idLlamada, idServicio, idElemento);
				}
				
				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				log.error(e.getMessage(),  idMenu, parametrosAdicionales, e);
				/** FIN EVENTO - ERROR **/
	
				resultadoOperacion = "KO";
	//			error = "ERROR_EXT(" + e.getMessage() + ")";
	
				/** INICIO EVENTO - FIN DE MODULO **/
				//log.endModuleService(idMenu, resultadoOperacion, null, null);
				/** FIN EVENTO - FIN DE MODULO **/
				
				/** INICIO EVENTO - INICIO DE ACCION **/
				//log.actionEvent(idMenu, "END", null);
				/** FIN EVENTO - INICIO DE MODULO **/
				
			}
		
		
		%>	
		
			<assign name="SUB_resultadoOperacion" expr="'<%=resultadoOperacion%>'"/>
			<assign name="SUB_error" expr="''"/>			
			<goto next="#FIN"/>	
		
		</block>
	</form>

<!-- 
***********************************
********** FORM FIN ***************
***********************************
-->
	<form id="FIN">
		<block>							
			<log label="MENU-RESULTADO"><value expr="'FIN - MENU-RESULTADO'"/></log>
			<log label="MENU-RESULTADO"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="MENU-RESULTADO"><value expr="'SUB_error: ' + SUB_error"/></log>
			<log label="MENU-RESULTADO"><value expr="'CATCH_resultadoOperacion: ' + CATCH_resultadoOperacion"/></log>
			<log label="MENU-RESULTADO"><value expr="'CATCH_error: ' + CATCH_error"/></log>
			
			<if cond="CATCH_resultadoOperacion != ''">
				<!-- se ha producido un error o un cuelgue y debo salir con ese resultado -->
				<assign name="SUB_resultadoOperacion" expr="CATCH_resultadoOperacion"/>
				<assign name="SUB_error" expr="CATCH_error"/>
			
			<else/>
				<if cond="SUB_resultadoOperacion == ''">
					<assign name="SUB_resultadoOperacion" expr="'OK'"/>
					<assign name="SUB_error" expr="''"/>
				</if>
			</if>
			
			<log label="MENU-RESULTADO"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="MENU-RESULTADO"><value expr="'SUB_error: ' + SUB_error"/></log>
			<return namelist="SUB_resultadoOperacion SUB_error"/>
			
		</block>
	
	</form>


</vxml>

