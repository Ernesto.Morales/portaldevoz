<?xml version="1.0" encoding="ISO-8859-1"?>

<%@page import="java.util.Properties"%>
<%@page import="java.util.Vector"%>
<%@page import="com.kranon.menuplantilla.*"%>
<%@page import="com.kranon.logger.event.ParamEvent"%>

<!-- imports de BEANS -->
<%@page import="com.kranon.bean.BeanGramatica"%>
<%@page import="com.kranon.bean.menu.BeanMenu"%>
<%@page import="com.kranon.bean.menu.BeanIntento"%>
<%@page import="com.kranon.bean.menu.BeanPromptGenerico"%>
<%@page import="com.kranon.bean.menu.BeanExcepcionMenu"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<%
// separador de variables en PRM_IN_MENU_variable
String varSplitBy = "\\|";
// separador de wavs dentro de una misma variable de PRM_IN_MENU_variable
String wavSplitBy = "\\#";
// EJEMPLO:
// 	WAV: /prompts/nombre.wav|/prompts/20y.wav#/prompts/3.wav#/prompts/marzo.wav


// Captura de parametros enviados mediante submit PRM_IN_MENU_idAplicacion PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable
// String idAplicacion = (String) request.getParameter("PRM_IN_MENU_idAplicacion");	
String pathXML = (String) request.getParameter("PRM_IN_MENU_pathXML");	
String idMenu = (String) request.getParameter("PRM_IN_MENU_idMenu");	
String variablesMenu = (String) request.getParameter("PRM_IN_MENU_variable");
// 26042016 nuevo parametro de entrada para el idInvocacion del LOGGER
String idInvocacionLog = (String) request.getParameter("PRM_IN_MENU_idInvocacion");
//15062016 nuevo parametro de entrada para el idServicio del LOGGER, para invocar a escritura de trazas al final
String idServicioLog = (String) request.getParameter("PRM_IN_MENU_idServicio");
//15062016 nuevo parametro de entrada para el idElemento del LOGGER, para invocar a escritura de trazas al final
String idElementoLog = (String) request.getParameter("PRM_IN_MENU_idElemento");
//NMB_201705 Nuevo parametro de entrada para el invocar al menu con parametros adicionales (por ejemplo, rama en PR, tipo de Cliente en LB)
String paramAdicional = (String) request.getParameter("PRM_IN_MENU_paramAdicional");

String idModuloLog = "MENU_PLANTILLA";

//separo las variables que se van a reproducir en el menu
String[] variablesMenuArray;
if(variablesMenu != null && !variablesMenu.equals("")){
	variablesMenuArray = variablesMenu.split(varSplitBy);
} else {
	variablesMenuArray = new String[0];
}

//String PATH_XML = pageContext.getServletContext().getRealPath("") + pathXML;  

//Se crean los objetos para el menu de fichero de configuracion
// ****************************************************************
// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
// ****************************************************************
MenuConfigurableXML menuConfigurableXML = new MenuConfigurableXML(idInvocacionLog, idMenu);
//MenuConfigurableXML menuConfigurableXML = new MenuConfigurableXML(idInvocacionLog, idElementoLog);
// ****************************************************************
// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
// ****************************************************************

/** INICIO EVENTO - INICIO DE PROCESO **/
ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
// parametrosEntrada.add(new ParamEvent("idAplicacion", idAplicacion));
parametrosEntrada.add(new ParamEvent("idInvocacion", idInvocacionLog));
parametrosEntrada.add(new ParamEvent("idMenu", idMenu));
parametrosEntrada.add(new ParamEvent("pathXML", pathXML));
parametrosEntrada.add(new ParamEvent("variable", variablesMenu));
menuConfigurableXML.getLog().initProcess(idModuloLog, "", parametrosEntrada, null);
/** FIN EVENTO - INICIO DE PROCESO **/
	
// obtengo el menu del XML
BeanMenu menu = menuConfigurableXML.obtenerMenu(idServicioLog, idMenu);

// variable para parar la ejecucion java en caso de error
boolean continuar = true;


String lenguajeMenu = "";
String modoInteraccionMenu = "";
String modoReproduccionMenu = "";
String numIntentosVoz = "";
String numIntentosTonos = "";
int numIntentosVozInt;
int numIntentosTonosInt;
String respuestaSensible = "";

// control de errores java
String codigoRetorno = "";
String error = "";

// locucion de maxint
String modoMaxint="";
String locMaxint="";

if(menu == null){
	// no se ha recuperado correctamente el Menu
	// lanzare un error mas adelante
	lenguajeMenu = "es-MX";
	modoInteraccionMenu = "DTMF";
	modoReproduccionMenu = "TTS";
	numIntentosVoz = "0";
	numIntentosTonos = "0";
	numIntentosVozInt = 0;
	numIntentosTonosInt = 0;
	respuestaSensible = "OFF";
	
} else {
	//Me guardo la configuracion general
	lenguajeMenu = menu.getConfiguracion().getIdioma();
	//MODO INTERACCION: ASR / DTMF
	modoInteraccionMenu = menu.getConfiguracion().getModoInteraccion();
	//MODO REPRODUCCION: WAV / VOC (builtin:) / TTS
	modoReproduccionMenu = menu.getConfiguracion().getModoReproduccion();

	numIntentosVoz = menu.getConfiguracion().getNumReintAsr();
	numIntentosTonos = menu.getConfiguracion().getNumReintDtmf();
	numIntentosVoz = (numIntentosVoz==null)?"0":numIntentosVoz;
	numIntentosTonos = (numIntentosTonos==null)?"0":numIntentosTonos;

	numIntentosVozInt = Integer.parseInt(numIntentosVoz);
	numIntentosTonosInt = Integer.parseInt(numIntentosTonos);
	
	respuestaSensible = menu.getConfiguracion().getRespuestaSensible();
	if(respuestaSensible == null || respuestaSensible.equals("")){
		respuestaSensible = "OFF";
	}

	/** INICIO EVENTO - COMENTARIO **/
	menuConfigurableXML.getLog().comment("idioma=" + lenguajeMenu);
	menuConfigurableXML.getLog().comment("modo_interaccion=" + modoInteraccionMenu);
	menuConfigurableXML.getLog().comment("modo_reproduccion=" + modoReproduccionMenu);
	menuConfigurableXML.getLog().comment("num_reint_asr=" + numIntentosVoz);
	menuConfigurableXML.getLog().comment("num_reint_dtmf=" + numIntentosTonos);
	/** FIN EVENTO - COMENTARIO **/
	
	if(!numIntentosVoz.equals("0") && numIntentosTonos.equals("0")){
		// hay tonos de voz, pero no de tonos
		// dare el maxint de voz
		if(menu.getConfigAsr().getPrompts().getPromptMaxint()!=null){
			
			modoMaxint = menu.getConfigAsr().getPrompts().getPromptMaxint().getModo();
			locMaxint = menu.getConfigAsr().getPrompts().getPromptMaxint().getValue();
		}
	} else {
		// en otro caso dare el maxint de tonos
		if(menu.getConfigDtmf().getPrompts().getPromptMaxint()!=null){
			
			modoMaxint = menu.getConfigDtmf().getPrompts().getPromptMaxint().getModo();
			locMaxint = menu.getConfigDtmf().getPrompts().getPromptMaxint().getValue();
		}
	}
	
}




%>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="<%=lenguajeMenu%>" >
<%-- application="<%=idAplicacion%>" --%>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	MENU-PLANTILLA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
<meta http-equiv="Expires" content="0"/>

<script src="../scripts/LoggerMenu.js"/>

<!-- KRANON: Definicion de variables locales -->
<!-- <var name="datosTrazaDialogo" expr="''"/> -->
<var name="numIntentosTotales" expr="0"/>
<var name="numIntentosNoMatch" expr="0"/>
<var name="numIntentosNoInput" expr="0"/>
<var name="tipoReconocimientoEmpleado" expr="'-'"/>

<var name="tipoPrompts_NM_Voz" expr="''"/>
<var name="prompts_NM_Voz" expr="''"/>
<var name="tipoPrompts_NI_Voz" expr="''"/>
<var name="prompts_NI_Voz" expr="''"/>

<!-- ABALFARO_20170404 nomatch excepcion -->
<var name="numExcepcionLanzada" expr="0"/>
<var name="respuestaNMExcepcion" expr="''"/>
<var name="prompts_NM_Excepcion_Voz" expr="''"/>
<var name="prompts_NM_Excepcion_Tonos" expr="''"/>

<var name="tipoPrompts_NM_Tonos" expr="''"/>
<var name="prompts_NM_Tonos" expr="''"/>
<var name="tipoPrompts_NI_Tonos" expr="''"/>
<var name="prompts_NI_Tonos" expr="''"/>

<!-- 07062016 control de tonos activos para gramaticas builtin -->
<var name="tonosActivosBuiltin" expr="''"/>
<!-- 07062016 control para comparar el tono introducido con los activos -->
<var name="encontradoTono" expr="''"/>

<!-- KRANON: Definicion de parametros de retorno del subdialogo -->
<var name="PRM_OUT_MENU_codigoRetorno" expr="''"/>
<var name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
<var name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
<!-- 07062016 nuevo parametro de salida para enviar el tipo de error _event:_message -->
<var name="PRM_OUT_MENU_error" expr="''"/>
<var name="PRM_OUT_MENU_intento" expr="''"/>
<var name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
<var name="PRM_OUT_MENU_nivelConfianza" expr="''"/>


<!-- 09062016 nuevo parametro de salida para enviar las trazas de todos los intentos del menu (dialogos y prompts) -->
<var name="PRM_OUT_MENU_trazas" expr="''"/>


<var name="PRM_idInvocacion" expr="''"/>
<var name="PRM_idServicio" expr="''"/>
<var name="PRM_idElemento" expr="''"/>
<var name="PRM_idMenu" expr="''"/>
<var name="PRM_respuestaSensible" expr="''"/>
<!-- [NMB_201705] Nuevo parametro de entrada para el invocar al menu con parametros adicionales -->
<var name="PRM_paramAdicional" expr="''"/>

<var name="PRM_promptsIntentosVoz" expr="''"/>
<var name="PRM_promptsIntentosTonos" expr="''"/>

<var name="PRM_errorPasoATonos" expr="''"/>
<var name="PRM_menuAEjecutar" expr="''"/>

<!-- ABALFARO_20170628 control si ya vamos a escribir el resultado y hemos saltado previamente -->
<var name="PRM_saltoEscribeResultadoMenu" expr="'NO'"/>


<script>
	tipoPrompts_NM_Voz = new Array();
	prompts_NM_Voz = new Array();
	tipoPrompts_NI_Voz = new Array();
	prompts_NI_Voz = new Array();
	
	// ABALFARO_20170404
	prompts_NM_Excepcion_Voz = new Array();
	prompts_NM_Excepcion_Tonos = new Array();
	
	tipoPrompts_NM_Tonos = new Array();
	prompts_NM_Tonos = new Array();
	tipoPrompts_NI_Tonos = new Array();
	prompts_NI_Tonos = new Array();
	
	// para los tonos activos de las opciones relleno la accion con la etiqueta accion
	// y gramaticas generales BUILTIN, relleno la accion con el propio tono
	function InfoTonosActivos() {	
		this.tono = "";
		this.accion = "";
	}
	// cada elemento: tonosActivosBuiltin[i] = new InfoTonosActivos();
	tonosActivosBuiltin = new Array();

</script>

<!--
******************************************
********** ERROR RECOGNITION *************
******************************************
-->
<!-- error.recognition:unknown recognition error -->
<catch event="error.recognition">

	<log label="MENU-PLANTILLA"><value expr="'CATCH ERR_IVR('+_event + ':'+_message+')'"/></log>
	
	<log label="MENU-PLANTILLA"><value expr="'PASO A TONOS POR ERROR_RECOGNITION'"/></log>
	
	<assign name="PRM_errorPasoATonos" expr="'RECOGNITION'"/>
	<log label="MENU-PLANTILLA"><value expr="'PRM_errorPasoATonos: ' + PRM_errorPasoATonos"/></log>
	
	
		<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR_RECOGNITION";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>
	
	<goto next="#MENU_PLANTILLA"/>
</catch>

<!--
***********************************************
********** ERROR ASR NO RESOURCE **************
***********************************************
-->
<catch event="error.noresource.asr">
	
	<log label="MENU-PLANTILLA"><value expr="'CATCH ERR_IVR('+_event + ':'+_message+')'"/></log>
	
	<log label="MENU-PLANTILLA"><value expr="'PASO A MENU DTMF POR ERROR NO RESOURCE'"/></log>
	
		
	<assign name="PRM_errorPasoATonos" expr="'NO_RESOURCE'"/>
	<log label="MENU-PLANTILLA"><value expr="'PRM_errorPasoATonos: ' + PRM_errorPasoATonos"/></log>
	
					
			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR_ASR_NO_RESOURCE";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			</script>

	<goto next="#MENU_PLANTILLA"/>

</catch>

<!--
***********************************************
********** ERROR TTS NO RESOURCE **************
***********************************************
-->
<catch event="error.noresource.tts">

			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = "";
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR_TTS_NO_RESOURCE";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>

	<assign name="PRM_OUT_MENU_codigoRetorno" expr="'ERROR'"/>
	<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
	<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
	<assign name="PRM_OUT_MENU_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
	<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
	<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
	<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
	
	<goto next="#ESCRIBE_RESULTADO_MENU"/>

</catch>

<!--
***********************************************
*************** ERROR GRAMMAR *****************
***********************************************
-->
<catch event="error.grammar">

			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = "";
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR_BAD_GRAMMAR";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>

	<assign name="PRM_OUT_MENU_codigoRetorno" expr="'ERROR'"/>
	<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
	<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
	<assign name="PRM_OUT_MENU_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
	<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
	<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
	<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
	
	<goto next="#ESCRIBE_RESULTADO_MENU"/>

</catch>


<!--
******************************************
********** MAX SPEECH TIMEOUT ************
******************************************
-->
<catch event="maxspeechtimeout">
	<assign name="PRM_OUT_MENU_codigoRetorno" expr="'ERROR'"/>
	<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
	<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
	<assign name="PRM_OUT_MENU_error" expr="'ERR_IVR(maxspeechtimeout)'"/>
	<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
	<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
	<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
	
	<log label="MENU-PLANTILLA"><value expr="'PASO A TODOS POR MAXSPEECHTIMEOUT'"/></log>
	
<!-- 	<goto next="#ESCRIBE_RESULTADO_MENU"/> -->
	<goto next="#DE_VOZ_A_TONO"/>
	
	
	<log label="MENU-PLANTILLA"><value expr="'CATCH ERR_IVR('+_event + ':'+_message+')'"/></log>
	
	<log label="MENU-PLANTILLA"><value expr="'PASO A MENU DTMF POR ERROR MAXSPEECHTIMEOUT'"/></log>
	
		
	<assign name="PRM_errorPasoATonos" expr="'MAXSPEECHTIMEOUT'"/>
	<log label="MENU-PLANTILLA"><value expr="'PRM_errorPasoATonos: ' + PRM_errorPasoATonos"/></log>
	
					
			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR_MAXSPEECHTIMEOUT";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>
			
	<goto next="#MENU_PLANTILLA"/>
</catch>


<!--
****************************************
********** ERROR GENERICO **************
****************************************
-->
<!-- KRANON: Se captura cualquier error que pueda producirse, y se devuelve a la aplicacion un codigo de retorno ERROR.-->
<catch event="error">

	<!-- ABALFARO_20170628 -->
	<if cond="PRM_saltoEscribeResultadoMenu == 'SI'">
		<!-- ya he saltado previamente a escribe resultado menu y algo fallo, voy directamente a retorno -->
		<goto next="#RETORNO"/>
	</if>
	
			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = "";
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "ERROR";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>

	<assign name="PRM_OUT_MENU_codigoRetorno" expr="'ERROR'"/>
	<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
	<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
	<assign name="PRM_OUT_MENU_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
	<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
	<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
	<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>	
	
	<goto next="#ESCRIBE_RESULTADO_MENU"/>

</catch>

<!--
******************************************
********** CAPTURA DEL CUELGUE ***********
******************************************
-->
<!-- KRANON: Se captura el cuelgue, y se devuelve a la aplicacion un codigo de retorno CUELGUE.-->
<catch event="connection.disconnect.hangup">
			
			<!-- TRAZAS DEL INTENTO -->
			<script>
				if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
					PRM_OUT_MENU_trazas = new LoggerMenu();		
				}
				if(PRM_OUT_MENU_trazas.intentos == null){
					PRM_OUT_MENU_trazas.intentos = new Array();
				}
				
				var intentoMenu = new IntentoMenu();
				intentoMenu.elementos = new Array();
					
				var numElems = 0;
			
				var dialogoMenu = new DialogoMenu();
					dialogoMenu.idMenu = PRM_idMenu;
					dialogoMenu.recDisponible = "ASRDTMF";
					dialogoMenu.recUtilizado = "";
					dialogoMenu.numIntento = numIntentosTotales + 1;
					dialogoMenu.respuesta = "HANGUP";
					dialogoMenu.respuestaRaw = "";
				intentoMenu.elementos[numElems] = dialogoMenu;	
					
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
				
			</script>


	<assign name="PRM_OUT_MENU_codigoRetorno" expr="'HANGUP'"/>
	<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
	<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
	<assign name="PRM_OUT_MENU_error" expr="''"/>
	<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
	<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
	<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
	
	<goto next="#ESCRIBE_RESULTADO_MENU"/>
<!-- 	<goto next="#RETORNO"/> -->
	
</catch>



<!--
******************************************************************************************************
************** INICIO PLANTILLA MENU PARA COMPROBAR DISPONIBILIDAD DE RECURSO DE VOZ *****************
******************************************************************************************************
-->
<form id="INICIO_PLANTILLA">
	
	<block>				
			<log label="MENU-PLANTILLA"><value expr="'-- INICIO PLANTILLA --'"/></log>
			
			<assign name="PRM_respuestaSensible" expr="'<%=respuestaSensible%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_respuestaSensible: ' + PRM_respuestaSensible"/></log>
			
			<assign name="PRM_idInvocacion" expr="'<%=idInvocacionLog%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_idInvocacion: ' + PRM_idInvocacion"/></log>
			
			<assign name="PRM_idServicio" expr="'<%=idServicioLog%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_idServicio: ' + PRM_idServicio"/></log>
			
			<assign name="PRM_idElemento" expr="'<%=idElementoLog%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_idElemento: ' + PRM_idElemento"/></log>

			<!-- [NMB_201705] Nuevo parametro de entrada para el invocar al menu con parametros adicionales -->
			<assign name="PRM_paramAdicional" expr="'<%=paramAdicional%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_paramAdicional: ' + PRM_paramAdicional"/></log>
						
			<assign name="PRM_idMenu" expr="'<%=idMenu%>'"/>
			<log label="MENU-PLANTILLA"><value expr="'PRM_idMenu: ' + PRM_idMenu"/></log>
			<goto next="#MENU_PLANTILLA"/>
		
<!-- 			<goto next="#CAPTURAR_NORESOURCE"/> -->
	</block>
</form>

<!--
********************************************
********** FORMULARIO INICIAL **************
********************************************
-->

<form id="MENU_PLANTILLA">
	
	<block>	
	
		<log label="MENU-PLANTILLA"><value expr="'-- MENU_PLANTILLA --'"/></log>

		
<%
		PromptMenu[][] promptsTonos = null;
		PromptMenu[][] promptsVoz = null;
		if(menu == null){
			// NO se ha recuperado correctamente el menu, devuelvo un error
			
			// relleno variables salida Java
			codigoRetorno = "KO";
			error = "ERROR_EXT(MENU_NULL)";
			
			// paro la ejecucion java
			continuar = false;
						
			%>
			<log label="MENU-PLANTILLA"><value expr="'ERROR: el menu se ha recuperado a null'"/></log>
			<goto next="#RETORNO_ERROR_JAVA"/>
			
			<%
			
		} else {
			
			%>
					
					<!--
					*****************************************
					** INICIALIZACION VARIABLE TRAZAS MENU **
					*****************************************
					-->	
					
<!-- 					<script> -->
<!-- 						PRM_OUT_MENU_trazas = new LoggerMenu();		 -->
<!-- 						PRM_OUT_MENU_trazas.intentos = new Array(); -->
<!-- 					</script> -->
					<script>
					if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
						PRM_OUT_MENU_trazas = new LoggerMenu();		
					}
					if(PRM_OUT_MENU_trazas.intentos == null){
						PRM_OUT_MENU_trazas.intentos = new Array();
					}
					</script>
						
						
			<%
			if(continuar){
				try {
					%>
					<!--
					****************************************************
					** COMPROBACION VARIABLES DINAMICAS POR PARAMETRO **
					****************************************************
					-->		
					<%
					/** INICIO EVENTO - INICIO MODULO **/
					ArrayList<ParamEvent> parametrosEntrada2 = new ArrayList<ParamEvent>();
					parametrosEntrada2.add(new ParamEvent("variablesMenu", variablesMenu));
					menuConfigurableXML.getLog().initModuleProcess("COMPROBACION_VARIABLES_DINAMICAS", parametrosEntrada2);
					/** FIN EVENTO - INICIO MODULO **/
					
					if (modoInteraccionMenu.equalsIgnoreCase("DTMF")) {
						// miro que el numero de variables enviadas como parametro coincidan con las declaradas en cada intento
						if(variablesMenuArray.length > 0){
							if(!menuConfigurableXML.comprobarVariablesDinamicas(menu.getConfigDtmf().getPrompts().getIntentos(), variablesMenuArray.length)){
								// no conciden el numero las variables a cantar
								
								/** INICIO EVENTO - FIN MODULO **/
								menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_VARIABLES_DINAMICAS", "KO", null, null);
								/** FIN EVENTO - FIN MODULO **/
					
								// relleno variables salida Java
								codigoRetorno = "ERROR";
								error = "ERROR_EXT(VARIABLES_DINAMICAS_DTMF)";
								
								// paro la ejecucion java
								continuar = false;
			%>				
								<log label="MENU-PLANTILLA"><value expr="'ERROR: COMPROBACION_VARIABLES_DINAMICAS en DTMF'"/></log>
								<goto next="#RETORNO_ERROR_JAVA"/>
			<%
							}
						}
					} else if (modoInteraccionMenu.equalsIgnoreCase("ASR")) {
						// miro que el numero de variables enviadas como parametro coincidan con las declaradas en cada intento
						if(variablesMenuArray.length > 0){
							if(!menuConfigurableXML.comprobarVariablesDinamicas(menu.getConfigAsr().getPrompts().getIntentos(), variablesMenuArray.length)){
								// no conciden el numero las variables a cantar
								
								/** INICIO EVENTO - FIN MODULO **/
								menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_VARIABLES_DINAMICAS", "KO", null, null);
								/** FIN EVENTO - FIN MODULO **/
					
								// relleno variables salida Java
								codigoRetorno = "ERROR";
								error = "ERROR_EXT(VARIABLES_DINAMICAS_ASR)";
								
								// paro la ejecucion java
								continuar = false;
			%>
								<log label="MENU-PLANTILLA"><value expr="'ERROR: COMPROBACION_VARIABLES_DINAMICAS en ASR'"/></log>
								<goto next="#RETORNO_ERROR_JAVA"/>
			<%
							}
						}		
					}
					/** INICIO EVENTO - FIN MODULO **/
					menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_VARIABLES_DINAMICAS", "OK", null, null);
					/** FIN EVENTO - FIN MODULO **/
					
				} catch(Exception e) {
					/** INICIO EVENTO - ERROR **/
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					parametrosAdicionales.add(new ParamEvent("error", e.toString()));
					menuConfigurableXML.getLog().error(e.getMessage(), "COMPROBACION_VARIABLES_DINAMICAS", parametrosAdicionales, e);
					/** FIN EVENTO - ERROR **/
					
					/** INICIO EVENTO - FIN MODULO **/
					menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_VARIABLES_DINAMICAS", "KO", null, null);
					/** FIN EVENTO - FIN MODULO **/
					
					// relleno variables salida Java
					codigoRetorno = "ERROR";
					error = "ERROR_EXT("+e.getMessage()+")";
					
					// paro la ejecucion java
					continuar = false;
					
					%>
					<log label="MENU-PLANTILLA"><value expr="'ERROR: COMPROBACION_VARIABLES_DINAMICAS'"/></log>
					<goto next="#RETORNO_ERROR_JAVA"/>
					<%
				}
			} // continuar
			%>
					
					<!--
					*****************************************************************************************
					** LOCUCIONES DE VOZ Y TONOS DEL MENU (DE JAVA A VXML) Y EL TIPO DE PROMPT DE CADA UNO **
					*****************************************************************************************
					-->	
			<%			
			
// 			PromptMenu[][] promptsTonos = null;
// 			PromptMenu[][] promptsVoz = null;
			
			if(continuar){
					/** INICIO EVENTO - INICIO MODULO **/
					menuConfigurableXML.getLog().initModuleProcess("CARGA_LOCUCIONES_VOZ_Y_TONOS", null);
					/** FIN EVENTO - INICIO MODULO **/	
					
					try {	
						// creo las matrices con los prompt
						if (menu.getOpciones() != null && menu.getOpciones().length > 0){
							if (modoInteraccionMenu.equalsIgnoreCase("DTMF")) {
								// menu solo por tonos
								promptsTonos = menuConfigurableXML.creaMatrizLocucionesOpciones("DTMF", menu.getConfigDtmf().getPrompts(), 
										menu.getOpciones(), numIntentosTonosInt, variablesMenuArray);
								
								
								
							} else {
								// menu por asr y tonos
								promptsTonos = menuConfigurableXML.creaMatrizLocucionesOpciones("DTMF", menu.getConfigDtmf().getPrompts(), 
												menu.getOpciones(), numIntentosTonosInt, variablesMenuArray);
								promptsVoz =  menuConfigurableXML.creaMatrizLocucionesOpciones("ASR", menu.getConfigAsr().getPrompts(), 
												menu.getOpciones(), numIntentosVozInt, variablesMenuArray);
							}
						} else{
							// creo las matrices con los prompt
							if (modoInteraccionMenu.equalsIgnoreCase("DTMF")) {
								// menu solo por tonos
								promptsTonos = menuConfigurableXML.creaMatrizLocuciones(menu.getConfigDtmf().getPrompts(), numIntentosTonosInt, variablesMenuArray);
							} else {
								// menu por asr y tonos
								promptsTonos = menuConfigurableXML.creaMatrizLocuciones(menu.getConfigDtmf().getPrompts(), numIntentosTonosInt, variablesMenuArray);
								promptsVoz =  menuConfigurableXML.creaMatrizLocuciones(menu.getConfigAsr().getPrompts(), numIntentosVozInt, variablesMenuArray);
							}
						}
						if (modoInteraccionMenu.equalsIgnoreCase("ASR")) {
							%>
							<!-- Guardo los PROMPT de VOZ en VXML para las estadisticas -->
							<script>
							PRM_promptsIntentosVoz = new Array();
							<%
								for(int i=0; i<promptsVoz.length; i++){
									%>
									PRM_promptsIntentosVoz[<![CDATA[(<%=i%>)]]>] = new Array();
									<%
									int numPromptValido = 0;
									for(int j=0; j<promptsVoz[i].length; j++){
										
										if(promptsVoz[i][j].prompt!= null && !promptsVoz[i][j].prompt.equals("")){
											
											if(promptsVoz[i][j].uso != null && !promptsVoz[i][j].uso.equals("")){
												// hay un uso en esta locucion
												if(promptsVoz[i][j].uso.equals("DIGITS") || promptsVoz[i][j].uso.equals("DIGITOS")){
													String promptConUso = "";
													for(int c=0; c<promptsVoz[i][j].prompt.length(); c++){
														promptConUso = promptConUso + promptsVoz[i][j].prompt.charAt(c) + " ";
													}
													promptsVoz[i][j].prompt = promptConUso;
												}
											}
											
											%>
											PRM_promptsIntentosVoz[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>] = new LocucionMenu();
											PRM_promptsIntentosVoz[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>].tipoLocucion = '<%=promptsVoz[i][j].modo%>'; 
											PRM_promptsIntentosVoz[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>].idLocucion = '<%=promptsVoz[i][j].prompt%>'; 
											<%
											numPromptValido = numPromptValido + 1;
										}
									}
								}
							%>
							</script>
													
						
							<%			
						}
						if(promptsTonos != null){
						%>
						<!-- Guardo los PROMPT de TONOS en VXML para las estadisticas -->
						<script>
							PRM_promptsIntentosTonos = new Array();
						<%
							for(int i=0; i<promptsTonos.length; i++){
								%>
								PRM_promptsIntentosTonos[<![CDATA[(<%=i%>)]]>] = new Array();
								<%
								int numPromptValido = 0;
								for(int j=0; j<promptsTonos[i].length; j++){
									
									if(promptsTonos[i][j].prompt!= null && !promptsTonos[i][j].prompt.equals("")){
										
										if(promptsTonos[i][j].uso != null && !promptsTonos[i][j].uso.equals("")){
											// hay un uso en esta locucion
											if(promptsTonos[i][j].uso.equals("DIGITS") || promptsTonos[i][j].uso.equals("DIGITOS")){
												String promptConUso = "";
												for(int c=0; c<promptsTonos[i][j].prompt.length(); c++){
													promptConUso = promptConUso + promptsTonos[i][j].prompt.charAt(c) + " ";
												}
												promptsTonos[i][j].prompt = promptConUso;
											}
										}
										
										%>
										PRM_promptsIntentosTonos[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>] = new LocucionMenu();
										PRM_promptsIntentosTonos[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>].tipoLocucion = '<%=promptsTonos[i][j].modo%>'; 
										PRM_promptsIntentosTonos[<![CDATA[(<%=i%>)]]>][<![CDATA[(<%=numPromptValido%>)]]>].idLocucion = '<%=promptsTonos[i][j].prompt%>'; 
										<%
										numPromptValido = numPromptValido + 1;
									}
								}
							}
						
						%>
						</script>
							
						<%	
						}
						
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_VOZ_Y_TONOS", "OK", null, null);
						/** FIN EVENTO - FIN MODULO **/
					} catch(Exception e) {
						/** INICIO EVENTO - ERROR **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("error", e.toString()));
						menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_LOCUCIONES_VOZ_Y_TONOS", parametrosAdicionales, e);
						/** FIN EVENTO - ERROR **/
						
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_VOZ_Y_TONOS", "KO", null, null);
						/** FIN EVENTO - FIN MODULO **/
						
						// relleno variables salida Java
						codigoRetorno = "ERROR";
						error = "ERROR_EXT("+e.getMessage()+")";
						
						// paro la ejecucion java
						continuar = false;
						
						%>
						<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_LOCUCIONES_VOZ_Y_TONOS'"/></log>
						<goto next="#RETORNO_ERROR_JAVA"/>
						<%
					}
			} // continuar
			
			if(continuar){
				try {
				%>	
				<!--
				******************************************************
				** LOCUCIONES DE NOINPUT Y NOMATCH (DE JAVA A VXML) **
				******************************************************
				-->		
				
				<%
						/** INICIO EVENTO - INICIO MODULO **/
						menuConfigurableXML.getLog().initModuleProcess("CARGA_LOCUCIONES_NI_NM_VOZ_Y_TONOS", null);
						/** FIN EVENTO - INICIO MODULO **/
					
						// PROMPTS NO MATCH Y NO INPUT DE VOZ
						if(menu.getConfigAsr() != null && menu.getConfigAsr().getPrompts() != null){
							BeanIntento[] intentosVoz = menu.getConfigAsr().getPrompts().getIntentos();
							// por cada intento de voz, recupero prompts
							// i: se refiere a cada intento

							for(int i= 0; i< numIntentosVozInt; i++) {
								BeanIntento intentoVoz;
								if(i>= intentosVoz.length){
									intentoVoz = intentosVoz[intentosVoz.length - 1];
								} else {
									intentoVoz = intentosVoz[i];
								}
								%>
								<script>
									prompts_NM_Voz[<![CDATA[(<%=i%>)]]>] = '<%=intentoVoz.getPromptNoMatch().getValue()%>';
									tipoPrompts_NM_Voz[<![CDATA[(<%=i%>)]]>] = '<%=intentoVoz.getPromptNoMatch().getModo()%>';
									
									prompts_NI_Voz[<![CDATA[(<%=i%>)]]>] = '<%=intentoVoz.getPromptNoInput().getValue()%>';
									tipoPrompts_NI_Voz[<![CDATA[(<%=i%>)]]>] = '<%=intentoVoz.getPromptNoInput().getModo()%>';
								</script>
								<%
						menuConfigurableXML.getLog().comment("ASR - Relleno prompt NI y NM INTENTO "  + i);
								menuConfigurableXML.getLog().comment("Prompt NM value: "  + intentoVoz.getPromptNoMatch().getValue());
								menuConfigurableXML.getLog().comment("Prompt NM modo: "  + intentoVoz.getPromptNoMatch().getModo());
								menuConfigurableXML.getLog().comment("Prompt NI value: "  + intentoVoz.getPromptNoInput().getValue());
								menuConfigurableXML.getLog().comment("Prompt NI modo: "  + intentoVoz.getPromptNoInput().getModo());
								menuConfigurableXML.getLog().comment("***********");
							}
						}
			
						//PROMPTS NO MATCH Y NO INPUT DE TONOS
						if(menu.getConfigDtmf() != null && menu.getConfigDtmf().getPrompts() != null){
							BeanIntento[] intentosTonos = menu.getConfigDtmf().getPrompts().getIntentos();
							// por cada intento de tonos, recupero prompts
							// i: se refiere a cada intento
							for(int i= 0; i< numIntentosTonosInt; i++) {
								BeanIntento intentoTonos;
								if(i>= intentosTonos.length){
									intentoTonos = intentosTonos[intentosTonos.length - 1];
								} else {
									intentoTonos = intentosTonos[i];
								}
							
								%>
								<script>
									prompts_NM_Tonos[<![CDATA[(<%=i%>)]]>] = '<%=intentoTonos.getPromptNoMatch().getValue()%>';
									tipoPrompts_NM_Tonos[<![CDATA[(<%=i%>)]]>] = '<%=intentoTonos.getPromptNoMatch().getModo()%>';
										
									prompts_NI_Tonos[<![CDATA[(<%=i%>)]]>] = '<%=intentoTonos.getPromptNoInput().getValue()%>';
									tipoPrompts_NI_Tonos[<![CDATA[(<%=i%>)]]>] = '<%=intentoTonos.getPromptNoInput().getModo()%>';
								</script>
								<%
								menuConfigurableXML.getLog().comment("DTMF - Relleno prompt NI y NM INTENTO "  + i);
								menuConfigurableXML.getLog().comment("Prompt NM value: "  + intentoTonos.getPromptNoMatch().getValue());
								menuConfigurableXML.getLog().comment("Prompt NM modo: "  + intentoTonos.getPromptNoMatch().getModo());
								menuConfigurableXML.getLog().comment("Prompt NI value: "  + intentoTonos.getPromptNoInput().getValue());
								menuConfigurableXML.getLog().comment("Prompt NI modo: "  + intentoTonos.getPromptNoInput().getModo());
								menuConfigurableXML.getLog().comment("***********");
							}
						}
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_NI_NM_VOZ_Y_TONOS", "OK", null, null);
						/** FIN EVENTO - FIN MODULO **/
					} catch(Exception e) {		
						/** INICIO EVENTO - ERROR **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("error", e.toString()));
						menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_LOCUCIONES_NI_NM_VOZ_Y_TONOS", parametrosAdicionales,e);
						/** FIN EVENTO - ERROR **/
						
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_NI_NM_VOZ_Y_TONOS", "KO", null, null);
						/** FIN EVENTO - FIN MODULO **/
						
						// relleno variables salida Java
						codigoRetorno = "ERROR";
						error = "ERROR_EXT("+e.getMessage()+")";
						
						// paro la ejecucion java
						continuar = false;
						
						%>
						
						<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_LOCUCIONES_NI_NM_VOZ_Y_TONOS'"/></log>
						<goto next="#RETORNO_ERROR_JAVA"/>
						<%
				
					}
				
				
					try {
						/** INICIO EVENTO - INICIO MODULO **/
						menuConfigurableXML.getLog().initModuleProcess("CARGA_LOCUCIONES_EXCEPCION", null);
						/** FIN EVENTO - INICIO MODULO **/
						
						//PROMPTS EXCEPCION
						if(menu.getExcepciones() != null){
							
							BeanExcepcionMenu[] excepciones = menu.getExcepciones();
							// por cada excepcion
							for(int i= 0; i< excepciones.length; i++) {
								BeanExcepcionMenu excepActual = excepciones[i];
								if(excepActual.getActive().equalsIgnoreCase("ON")){
									// la excepcion esta activa
									//// TODO ana
	
									
									if(excepActual.getMode().equalsIgnoreCase("ASR")){
										// excepcion para respuestas asr
										
										%>
										<script>
											// creo el array de locuciones de la excepcion i, pero solo en asr
											var longExcepVoz = prompts_NM_Excepcion_Voz.length;
											prompts_NM_Excepcion_Voz[longExcepVoz] = new Array();
											<%
											// por cada intento, recupero los prompts
											for(int j= 0; j < numIntentosVozInt; j++) {
												BeanIntento intentoActual = null;										
												if(j>= excepActual.getPrompts().getIntentos().length){
													intentoActual = excepActual.getPrompts().getIntentos()[excepActual.getPrompts().getIntentos().length - 1];
												} else {
													intentoActual = excepActual.getPrompts().getIntentos()[j];
												}
												
												String modoLocIntentoExcep = "";
												String locucionIntentoExcep = "";
												for(int k=0; k<intentoActual.getPrompts().length; k++){
													BeanPromptGenerico pgActual = intentoActual.getPrompts()[k];
													// guardo el primero modo
													if(modoLocIntentoExcep.equals("")){
														modoLocIntentoExcep = pgActual.getModo();
													}
													// concateno todos los posibles prompts
													locucionIntentoExcep = locucionIntentoExcep + " " + pgActual.getValue();
												}
												
												%>
												prompts_NM_Excepcion_Voz[longExcepVoz][<![CDATA[(<%=j%>)]]>] = new LocucionMenu();
												prompts_NM_Excepcion_Voz[longExcepVoz][<![CDATA[(<%=j%>)]]>].tipoLocucion = '<%=modoLocIntentoExcep%>'; 
												prompts_NM_Excepcion_Voz[longExcepVoz][<![CDATA[(<%=j%>)]]>].idLocucion = '<%=locucionIntentoExcep%>'; 
												<% 
												
											}
											%>
										
										</script>
										<%
										
									} else {
										// excepcion para respuestas dtmf
										
										%>
										<script>
											// creo el array de locuciones de la excepcion i, pero solo en asr
											var longExcepTonos = prompts_NM_Excepcion_Tonos.length;
											prompts_NM_Excepcion_Tonos[longExcepTonos] = new Array();
											<%
											// por cada intento, recupero los prompts
											for(int j= 0; j < numIntentosVozInt; j++) {
												BeanIntento intentoActual = null;										
												if(j>= excepActual.getPrompts().getIntentos().length){
													intentoActual = excepActual.getPrompts().getIntentos()[excepActual.getPrompts().getIntentos().length - 1];
												} else {
													intentoActual = excepActual.getPrompts().getIntentos()[j];
												}
												
												String modoLocIntentoExcep = "";
												String locucionIntentoExcep = "";
												for(int k=0; k<intentoActual.getPrompts().length; k++){
													BeanPromptGenerico pgActual = intentoActual.getPrompts()[k];
													// guardo el primero modo
													if(modoLocIntentoExcep.equals("")){
														modoLocIntentoExcep = pgActual.getModo();
													}
													// concateno todos los posibles prompts
													locucionIntentoExcep = locucionIntentoExcep + " " + pgActual.getValue();
												}
												
												%>
												prompts_NM_Excepcion_Tonos[longExcepTonos][<![CDATA[(<%=j%>)]]>] = new LocucionMenu();
												prompts_NM_Excepcion_Tonos[longExcepTonos][<![CDATA[(<%=j%>)]]>].tipoLocucion = '<%=modoLocIntentoExcep%>'; 
												prompts_NM_Excepcion_Tonos[longExcepTonos][<![CDATA[(<%=j%>)]]>].idLocucion = '<%=locucionIntentoExcep%>'; 
												<% 
												
											}
											%>
										
										</script>
										<%
										
									}
									
								}
							}
							
							
						}
						
						
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_EXCEPCION", "OK", null, null);
						/** FIN EVENTO - FIN MODULO **/
						
					} catch(Exception e) {		
						/** INICIO EVENTO - ERROR **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("error", e.toString()));
						menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_LOCUCIONES_EXCEPCION", parametrosAdicionales,e);
						/** FIN EVENTO - ERROR **/
						
						/** INICIO EVENTO - FIN MODULO **/
						menuConfigurableXML.getLog().endModuleProcess("CARGA_LOCUCIONES_EXCEPCION", "KO", null, null);
						/** FIN EVENTO - FIN MODULO **/
						
						// relleno variables salida Java
						codigoRetorno = "ERROR";
						error = "ERROR_EXT("+e.getMessage()+")";
						
						// paro la ejecucion java
						continuar = false;
						
						%>
						
						<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_LOCUCIONES_EXCEPCION'"/></log>
						<goto next="#RETORNO_ERROR_JAVA"/>
						<%
				
					}
				
				
				
			} // continuar
			%>			
					
					<!--
					**************************************************************************************
					** COMPROBACION NUM INTENTOS Y NUM GRAMATICAS EN FUNCION DEL MODO DE RECONOCIMIENTO **
					**************************************************************************************
					-->			
			<%
			if(continuar) {
					
					/** INICIO EVENTO - INICIO MODULO **/
					menuConfigurableXML.getLog().initModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", null);
					/** FIN EVENTO - INICIO MODULO **/
	
					if (modoInteraccionMenu.equalsIgnoreCase("ASR")) {		
						int numGramaticasVoz = menu.getConfigAsr().getGrammars().length;
						if (numIntentosVoz.compareTo("0") == 0 && numIntentosTonos.compareTo("0") == 0)	{
							
							/** INICIO EVENTO - FIN MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("modoInteraccionMenu", modoInteraccionMenu));
							parametrosSalida.add(new ParamEvent("numIntentosVoz", numIntentosVoz));
							parametrosSalida.add(new ParamEvent("numIntentosTonos", numIntentosTonos));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "KO", parametrosSalida, null);
							/** FIN EVENTO - FIN MODULO **/
						
							
							// relleno variables salida Java
							codigoRetorno = "ERROR";
							error = "ERROR_EXT(NUMINT_CERO_ASR)";
						
							// paro la ejecucion java
							continuar = false;
						
			%>
			
							<log label="MENU-PLANTILLA"><value expr="'ERROR: modo ASR y num intentos es 0'"/></log>
							<goto next="#RETORNO_ERROR_JAVA"/>
			<%
						} else if (numIntentosVoz.compareTo("0") == 0 && numIntentosTonos.compareTo("0") != 0)	{
							// el modo es ASR pero solo hay intentos por tonos
							// cambio a modo DTMF para evaluarlo
							modoInteraccionMenu = "DTMF";
						
						} else if (numGramaticasVoz == 0 && !menuConfigurableXML.hayGrammarEnOpciones(menu.getOpciones(), "ASR")) {
							// no hay gramaticas generales, y en las opciones tampoco hay gramaticas para el modo asr 
							
							/** INICIO EVENTO - FIN MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("modoInteraccionMenu", modoInteraccionMenu));
							parametrosSalida.add(new ParamEvent("numGramaticasVoz", numGramaticasVoz+""));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "KO", parametrosSalida, null);
							/** FIN EVENTO - FIN MODULO **/
							
							// relleno variables salida Java
							codigoRetorno = "ERROR";
							error = "ERROR_EXT(GRAMMAR_CERO_ASR)";
						
							// paro la ejecucion java
							continuar = false;
			%>
			
							<log label="MENU-PLANTILLA"><value expr="'ERROR: no hay gramaticas generales, y en las opciones tampoco hay gramaticas para el modo asr '"/></log>
							<goto next="#RETORNO_ERROR_JAVA"/>
			<%
						} else {
							
							/** INICIO EVENTO - INICIO MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("menuAEjecutar", "ASRDTMF"));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "OK", parametrosSalida, null);
							/** FIN EVENTO - INICIO MODULO **/
			%>
							<log label="MENU-PLANTILLA"><value expr="'Paso a Menu ASRDTMF'"/></log>
			
							<assign name="PRM_menuAEjecutar" expr="'ASRDTMF'"/>
			
							<goto next="#DECIDIR_FORM"/>
			
			<%
						}
					} // fin modo asr 
					
					if (modoInteraccionMenu.equalsIgnoreCase("DTMF")) {			
						int numGramaticasTonos = menu.getConfigDtmf().getGrammars().length;
						if (numIntentosTonos.compareTo("0") == 0) {
							// Error: el numero de intentos dtmf es 0 y estamos en modo DTMF
							
							/** INICIO EVENTO - FIN MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("modoInteraccionMenu", modoInteraccionMenu));
							parametrosSalida.add(new ParamEvent("numIntentosTonos", numIntentosTonos));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "KO", parametrosSalida, null);
							/** FIN EVENTO - FIN MODULO **/
					
							// relleno variables salida Java
							codigoRetorno = "ERROR";
							error = "ERROR_EXT(NUMINT_CERO_DTMF)";
						
							// paro la ejecucion java
							continuar = false;
			%>
			
							<log label="MENU-PLANTILLA"><value expr="'ERROR: el numero de intentos dtmf es 0 y estamos en modo DTMF'"/></log>
							<goto next="#RETORNO_ERROR_JAVA"/>
			<%
			
						} else if (numGramaticasTonos == 0 && !menuConfigurableXML.hayGrammarEnOpciones(menu.getOpciones(), "DTMF")) {
							//no hay gramaticas generales, y en las opciones tampoco hay gramaticas para el modo dtmf 
							
							/** INICIO EVENTO - FIN MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("modoInteraccionMenu", modoInteraccionMenu));
							parametrosSalida.add(new ParamEvent("numGramaticasTonos", numGramaticasTonos+""));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "KO", parametrosSalida, null);
							/** FIN EVENTO - FIN MODULO **/
							
							// relleno variables salida Java
							codigoRetorno = "ERROR";
							error = "ERROR_EXT(GRAMMAR_CERO_DTMF)";
						
							// paro la ejecucion java
							continuar = false;
			%>
			
							<log label="MENU-PLANTILLA"><value expr="'ERROR: no hay gramaticas generales, y en las opciones tampoco hay gramaticas para el modo dtmf '"/></log>
							<goto next="#RETORNO_ERROR_JAVA"/>
			<%
						} else {
							
							/** INICIO EVENTO - INICIO MODULO **/
							ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
							parametrosSalida.add(new ParamEvent("menuAEjecutar", "DTMF"));
							menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "OK", parametrosSalida, null);
							/** FIN EVENTO - INICIO MODULO **/
			%>
							<assign name="PRM_menuAEjecutar" expr="'DTMF'"/>
							
							<goto next="#DECIDIR_FORM"/>		
			<%
						}
					}  // fin modo dtmf
					
					
					if (!modoInteraccionMenu.equalsIgnoreCase("ASR") && !modoInteraccionMenu.equalsIgnoreCase("DTMF")) {
						
						// relleno variables salida Java
						codigoRetorno = "ERROR";
						error = "ERROR_EXT(MODO_INTERACCION)";
					
						// paro la ejecucion java
						continuar = false;	
						
						/** INICIO EVENTO - INICIO MODULO **/
						ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
						parametrosSalida.add(new ParamEvent("modoInteraccionMenu", modoInteraccionMenu));
						menuConfigurableXML.getLog().endModuleProcess("COMPROBACION_NUM_INTENTOS_Y_NUM_GRAMATICAS", "KO", null, null);
						/** FIN EVENTO - INICIO MODULO **/
			%>			
						<log label="MENU-PLANTILLA"><value expr="'ERROR: modo de interaccion distinto a ASR o DTMF'"/></log>
						<goto next="#RETORNO_ERROR_JAVA"/>
			<%			
					}
			} // continuar
} // fin else menu != null
%>
	</block>
</form>	




<form id="DECIDIR_FORM">
	<block>	
		<log label="MENU-PLANTILLA"><value expr="'-- DECIDIR_FORM --'"/></log>	
		
		<if cond="PRM_errorPasoATonos == 'NO_RESOURCE' || PRM_errorPasoATonos == 'RECOGNITION' || PRM_errorPasoATonos == 'MAXSPEECHTIMEOUT'">	
			<!-- se ha producido un error y debemos pasar a tonos directamente -->
			<log label="MENU-PLANTILLA"><value expr="'-- SALTO A MENU_DTMF --'"/></log>	
			
			<assign name="numIntentosTotales" expr="0"/>
			<assign name="numIntentosNoMatch" expr="0"/>
			<assign name="numIntentosNoInput" expr="0"/>
			<assign name="tipoReconocimientoEmpleado" expr="''"/>
			
			<script>
				// cada elemento: tonosActivosBuiltin[i] = new InfoTonosActivos();
				tonosActivosBuiltin = new Array();
			</script>
		
			<goto next="#MENU_DTMF"/>	
			
		<else/>
			<if cond="PRM_menuAEjecutar == 'ASRDTMF'">	
				<!-- salto a menu por asr y dtmf -->
				<log label="MENU-PLANTILLA"><value expr="'-- SALTO A MENU_ASRDTMF --'"/></log>	
				<goto next="#MENU_ASRDTMF"/>
					
			<else/>
			
				<!-- salto a menu por dtmf -->
				<log label="MENU-PLANTILLA"><value expr="'-- SALTO A MENU_DTMF --'"/></log>	
				
				<assign name="numIntentosTotales" expr="0"/>
				<assign name="numIntentosNoMatch" expr="0"/>
				<assign name="numIntentosNoInput" expr="0"/>
				<assign name="tipoReconocimientoEmpleado" expr="''"/>
				
				<script>
					// cada elemento: tonosActivosBuiltin[i] = new InfoTonosActivos();
					tonosActivosBuiltin = new Array();
				</script>
			
				<goto next="#MENU_DTMF"/>	
			</if>
		</if>
			
	</block>
	
</form>


<!--
********************************************************
********** RECONOCIMIENTO POR VOZ Y TONOS **************
********************************************************
-->
<form id="MENU_ASRDTMF">

<block>
	<%
	if(continuar){
		//*************************************
		// ****** TONOS ACTIVOS GENERALES *****
		// ************************************
		try {
			//Recorro las gramaticas de tonos
			if(menu.getConfigDtmf().getGrammars() != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES_DTMF", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < menu.getConfigDtmf().getGrammars().length;i++) {
					String grammarTonos = menu.getConfigDtmf().getGrammars()[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){					
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {
							// 07062016 como la gramatica es builtin, me guardo el numero de tonos activos 
							String tonosActivos = menu.getConfigDtmf().getGrammars()[i].getTonosActivos();
							if(tonosActivos != null){
								String[] tonosActivosArray = tonosActivos.split(",");
								for(int j = 0; j< tonosActivosArray.length; j++) {
									%>
									<script>
										var tonosAnteriores = tonosActivosBuiltin.length;
										tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
										tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonosActivosArray[j]%>';
										tonosActivosBuiltin[tonosAnteriores].accion = '<%=tonosActivosArray[j]%>';
									</script>
									
									<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
									<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
									<%
									/** INICIO EVENTO - COMENTARIO **/
									menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].tono=" + tonosActivosArray[j]);
									menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].accion=" + tonosActivosArray[j]);
									/** FIN EVENTO - COMENTARIO **/
								}		
							} else {
								// [20160825] si la gramatica no tiene tonos activos pero SI longitud la incluyo como L5 para length=5
								// y accion "=" para luego saber que tengo que devolver la respuesta tal cual
								// elimino los espacios en blanco
								grammarTonos = grammarTonos.trim();
								String tono = "";
								String accion = "=";
								if(grammarTonos.contains("length=") && !grammarTonos.contains("minlength=") && !grammarTonos.contains("maxlength=")){
									int index=grammarTonos.indexOf("length=");
									tono = "L" + grammarTonos.substring(index + ("length=".length()), grammarTonos.length());
									
								} else if(grammarTonos.contains("minlength=") && grammarTonos.contains("maxlength=")){
									String paramsGrammar = grammarTonos.substring(grammarTonos.indexOf("?") + 1, grammarTonos.length());
									int indexMin=paramsGrammar.indexOf("minlength=");
									int indexMax=paramsGrammar.indexOf("maxlength=");
									String minlength = "";
									String maxlength = "";
									if(indexMin < indexMax){
										// estan definidos minimo y maximo
										// -1 al final por el ; de separacion
										minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), indexMax - 1);
										maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), paramsGrammar.length());
									} else {
										// estan definidos maximo y minimo
										// -1 al final por el ; de separacion
										maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), indexMin - 1);
										minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), paramsGrammar.length());
									}
									
									tono = "Lmin" + minlength + "Lmax" + maxlength;
									
									
								} else if(grammarTonos.contains("minlength=")){
									int indexMin=grammarTonos.indexOf("minlength=");
									String minlength = grammarTonos.substring(indexMin + ("minlength=".length()), grammarTonos.length());
									tono = "Lmin" + minlength;
									
								} else if(grammarTonos.contains("maxlength=")){
									int indexMax=grammarTonos.indexOf("maxlength=");
									String maxlength = grammarTonos.substring(indexMax + ("maxlength=".length()), grammarTonos.length());
									tono = "Lmax" + maxlength;
									
								}
								%>
								<script>
									var tonosAnteriores = tonosActivosBuiltin.length;
									tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
									tonosActivosBuiltin[tonosAnteriores].tono = '<%=tono%>';
									tonosActivosBuiltin[tonosAnteriores].accion = '<%=accion%>';
								</script>
								
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
								<%
								/** INICIO EVENTO - COMENTARIO **/
								menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tono);
								menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
								/** FIN EVENTO - COMENTARIO **/
							}
						} 
					}
				}
				
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES_DTMF", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/

			}
		} catch(Exception e) {			
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_TONOS_ACTIVOS_GENERALES_DTMF", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
			
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES_DTMF", "KO", null, null);
			/** FIN EVENTO - FIN MODULO **/
			
			// relleno variables salida Java
			codigoRetorno = "ERROR";
			error = "ERROR_EXT("+e.getMessage()+")";
			
			// paro la ejecucion java
			continuar = false;
			
			%>
						
			<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_TONOS_ACTIVOS_GENERALES_DTMF'"/></log>
			<goto next="#RETORNO_ERROR_JAVA"/>
		
			<%
		}
	} // continuar
	
	BeanGramatica[] grammarTonosOpciones = null;
	if(continuar){
	
		//************************************
		// ****** TONOS ACTIVOS OPCIONES *****
		// ***********************************
		
		try {
		
			// Gramaticas de tonos de las opciones activas
			if(grammarTonosOpciones == null && menu.getOpciones() != null && menu.getOpciones().length > 0){
				// cargo las gramaticas
				grammarTonosOpciones = menuConfigurableXML.obtenerGrammarOpciones("DTMF", menu.getOpciones());
			}
			if(grammarTonosOpciones != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES_DTMF", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < grammarTonosOpciones.length;i++) {
					String grammarTonos = grammarTonosOpciones[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {					
							// 07062016 como la gramatica es builtin, lado los tonos activos por opcion al array de tonos activos en builtin
							String tonoActivo = menu.getOpciones()[i].getConfiguracion().getPosicion();
							String accion = menu.getOpciones()[i].getConfiguracion().getAccion();
							%>
								<script>
									var tonosAnteriores = tonosActivosBuiltin.length;
									tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
									tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonoActivo%>';
									tonosActivosBuiltin[tonosAnteriores].accion = '<%=accion%>';
								</script>
								
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
							<%	
							/** INICIO EVENTO - COMENTARIO **/
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tonoActivo);
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
							/** FIN EVENTO - COMENTARIO **/
							
						} 
					}
					// [20160825] Todas las gramaticas de tonos obligatoriamente llevan asociado un tono activo que es la posicion
					// else {}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES_DTMF", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/

			}
		
		} catch(Exception e) {		
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_TONOS_ACTIVOS_OPCIONES_DTMF", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
			
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES_DTMF", "KO", null, null);
			/** FIN EVENTO - FIN MODULO **/
			
			// relleno variables salida Java
			codigoRetorno = "ERROR";
			error = "ERROR_EXT("+e.getMessage()+")";
			
			// paro la ejecucion java
			continuar = false;
			
			%>
						
			<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_TONOS_ACTIVOS_OPCIONES'"/></log>
			<goto next="#RETORNO_ERROR_JAVA"/>
		
			<%
			
		}
	} // fin del continuar
%>
</block>

<%
if (continuar){
%>
	<property name="inputmodes" value="voice dtmf"/>
	
	<field name="menu_VozTonos"> 
	
		<%
		if (modoInteraccionMenu.equalsIgnoreCase("ASR")) {
			/** INICIO EVENTO - INICIO MODULO **/
			menuConfigurableXML.getLog().initModuleProcess("CARGA_PROPERTIES_VOZ", null);
			/** FIN EVENTO - INICIO MODULO **/
		
		%>	
			<property name="bargein" value="<%=menu.getConfigAsr().getConfiguracion().getBargeinAsrActivo().equalsIgnoreCase("ON")?true:false%>"/>
			<property name="timeout" value="<%=menu.getConfigAsr().getConfiguracion().getPropertyTimeoutAsr()%>s"/>
			<property name="confidencelevel" value="<%=menu.getConfigAsr().getConfiguracion().getPropertyConfidenceLevelAsr()%>"/>
			<property name="sensitivity" value="<%=menu.getConfigAsr().getConfiguracion().getPropertySensitivityAsr()%>"/>
			<property name="maxspeechtimeout" value="<%=menu.getConfigAsr().getConfiguracion().getPropertyMaxspeechTimeoutAsr()%>s"/>
			<property name="incompletetimeout" value="<%=menu.getConfigAsr().getConfiguracion().getPropertyIncompleteTimeoutAsr()%>s"/>
			<property name="completetimeout" value="<%=menu.getConfigAsr().getConfiguracion().getPropertyCompleteTimeoutAsr()%>s"/>
			
			<!-- ABALFARO: aqui si aplica -->
			<property name="interdigittimeout" value="<%=menu.getConfigDtmf().getConfiguracion().getInterdigitTimeoutDtmf()%>s"/>
		<%
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_PROPERTIES_VOZ", "OK", null, null);
			/** FIN EVENTO - FIN MODULO **/
		}
		
		%>
		<!--
		**********************************************
		** ACTIVACION DE GRAMATICAS POR VOZ Y TONOS **
		*********************************************
		-->			
<%
		// **************************
		// ****** VOZ GENERALES *****
		// **************************

		//Recorro las gramaticas de voz
		if(modoInteraccionMenu.equalsIgnoreCase("ASR") && menu.getConfigAsr() != null && menu.getConfigAsr().getGrammars() != null){		
			
			/** INICIO EVENTO - INICIO MODULO **/
			menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_GENERALES_VOZ", null);
			/** FIN EVENTO - INICIO MODULO **/
			
			for(int i = 0; i < menu.getConfigAsr().getGrammars().length;i++) {
				String grammarVoz = menu.getConfigAsr().getGrammars()[i].getValue();
				// si la gramatica acaba en .grxml o si empieza en builtin
				if(grammarVoz != null){
					
					/** INICIO EVENTO - COMENTARIO **/
					menuConfigurableXML.getLog().comment("grammarVoz[" + i + "]=" + grammarVoz);
					/** FIN EVENTO - COMENTARIO **/
					
					if (grammarVoz.startsWith("builtin") || grammarVoz.startsWith("BUILTIN")) {
						%>	
						<grammar src="<%=grammarVoz%>" />		
						<%
					} else if (grammarVoz.endsWith(".grxml") || grammarVoz.endsWith(".GRXML")) {
						%>
							<grammar mode="voice" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarVoz%>'"/>		
						<%
					} else {
						%>
							<grammar mode="voice" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarVoz%>'"/>				
							
						<%
					}	
				}
			}
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_GENERALES_VOZ", "OK", null, null);
			/** FIN EVENTO - FIN MODULO **/
		}

		//*************************
		// ****** VOZ OPCIONES *****
		// *************************
				
		// Gramaticas de voz de las opciones activas
		BeanGramatica[] grammarVozOpciones = null;
		if (modoInteraccionMenu.equalsIgnoreCase("ASR") && menu.getOpciones() != null && menu.getOpciones().length > 0){
			grammarVozOpciones = menuConfigurableXML.obtenerGrammarOpciones("ASR", menu.getOpciones());
			if(grammarVozOpciones != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_OPCIONES_VOZ", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < grammarVozOpciones.length;i++) {
					String grammarVoz = grammarVozOpciones[i].getValue();
					
					/** INICIO EVENTO - COMENTARIO **/
					menuConfigurableXML.getLog().comment("grammarVoz[" + i + "]=" + grammarVoz);
					/** FIN EVENTO - COMENTARIO **/
										
					// si la gramatica acaba en .grxml o si empieza en builtin 
					if(grammarVoz != null){
						if (grammarVoz.startsWith("builtin") || grammarVoz.startsWith("BUILTIN")) {
							%>
							<grammar src="<%=grammarVoz%>" />			
							<%
						} else if (grammarVoz.endsWith(".grxml") || grammarVoz.endsWith(".GRXML")) {
							%>
								<grammar mode="voice" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarVoz%>'"/>				
							<%
						} else {
							%>
								<grammar mode="voice" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarVoz%>'"/>				
							<%
						}	
					}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_OPCIONES_VOZ", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		}

		//*****************************
		// ****** TONOS GENERALES *****
		// *****************************

		//Recorro las gramaticas de tonos
		if(modoInteraccionMenu.equalsIgnoreCase("ASR") && menu.getConfigDtmf().getGrammars() != null){
			
			/** INICIO EVENTO - INICIO MODULO **/
			menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_GENERALES_TONOS", null);
			/** FIN EVENTO - INICIO MODULO **/
			
			for(int i = 0; i < menu.getConfigDtmf().getGrammars().length;i++) {
				String grammarTonos = menu.getConfigDtmf().getGrammars()[i].getValue();
				// si la gramatica acaba en .grxml o si empieza en builtin
				if(grammarTonos != null){
					
					/** INICIO EVENTO - COMENTARIO **/
					menuConfigurableXML.getLog().comment("grammarTonos[" + i + "]=" + grammarTonos);
					/** FIN EVENTO - COMENTARIO **/
				
					menuConfigurableXML.getLog().comment("grammarTonos[" + i + "]=" + grammarTonos);
					if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {
						
						if(menu.getConfigDtmf().getGrammars()[i].getMinLength() != 0){
							// hay definido un minimo
							if(!grammarTonos.contains("minlength")){
								// la gramatica no lo tiene definido, lo aado
								if(grammarTonos.endsWith("digits")){
									grammarTonos = grammarTonos + "?";
								} else {
									grammarTonos = grammarTonos + ";";
								}
								grammarTonos = grammarTonos + "minlength=" + menu.getConfigDtmf().getGrammars()[i].getMinLength();
								
								menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
							}
						}
						if(menu.getConfigDtmf().getGrammars()[i].getMaxLength() != 0){
							// hay definido un maximo
							if(!grammarTonos.contains("maxlength")){
								// la gramatica no lo tiene definido, lo aado
								if(grammarTonos.endsWith("digits")){
									grammarTonos = grammarTonos + "?";
								} else {
									grammarTonos = grammarTonos + ";";
								}
								grammarTonos = grammarTonos + "maxlength=" + menu.getConfigDtmf().getGrammars()[i].getMaxLength();
								
								menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
							}
						}
						%>
						<grammar src="<%=grammarTonos%>" />			
						<%
						// 07062016 como la gramatica es builtin, me guardo el numero de tonos activos 
						String tonosActivos = menu.getConfigDtmf().getGrammars()[i].getTonosActivos();
						if(tonosActivos != null){
							String[] tonosActivosArray = tonosActivos.split(",");
							for(int j= 0; j< tonosActivosArray.length; j++) {
								%>
								<script>
									var tonosAnteriores = tonosActivosBuiltin.length;
									tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
									tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonosActivosArray[j]%>';
									tonosActivosBuiltin[tonosAnteriores].accion = '<%=tonosActivosArray[j]%>';
								</script>
								
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
									
								<%
								/** INICIO EVENTO - COMENTARIO **/
								menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].tono=" + tonosActivosArray[j]);
								menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].accion=" + tonosActivosArray[j]);
								/** FIN EVENTO - COMENTARIO **/
							}					
						} else {
							// [20160825] si la gramatica no tiene tonos activos pero SI longitud la incluyo como L5 para length=5
							// y accion "=" para luego saber que tengo que devolver la respuesta tal cual
							// elimino los espacios en blanco
							// aqui ya tengo grammarTonos construida con los maxlength/minlength/length
							grammarTonos = grammarTonos.trim();
							String tono = "";
							String accion = "=";
							if(grammarTonos.contains("length=") && !grammarTonos.contains("minlength=") && !grammarTonos.contains("maxlength=")){
								int index=grammarTonos.indexOf("length=");
								tono = "L" + grammarTonos.substring(index + ("length=".length()), grammarTonos.length());
								
							} else if(grammarTonos.contains("minlength=") && grammarTonos.contains("maxlength=")){
								String paramsGrammar = grammarTonos.substring(grammarTonos.indexOf("?") + 1, grammarTonos.length());
								int indexMin=paramsGrammar.indexOf("minlength=");
								int indexMax=paramsGrammar.indexOf("maxlength=");
								String minlength = "";
								String maxlength = "";
								if(indexMin < indexMax){
									// estan definidos minimo y maximo
									// -1 al final por el ; de separacion
									minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), indexMax - 1);
									maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), paramsGrammar.length());
								} else {
									// estan definidos maximo y minimo
									// -1 al final por el ; de separacion
									maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), indexMin - 1);
									minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), paramsGrammar.length());
								}
								
								tono = "Lmin" + minlength + "Lmax" + maxlength;
								
								
							} else if(grammarTonos.contains("minlength=")){
								int indexMin=grammarTonos.indexOf("minlength=");
								String minlength = grammarTonos.substring(indexMin + ("minlength=".length()), grammarTonos.length());
								tono = "Lmin" + minlength;
								
							} else if(grammarTonos.contains("maxlength=")){
								int indexMax=grammarTonos.indexOf("maxlength=");
								String maxlength = grammarTonos.substring(indexMax + ("maxlength=".length()), grammarTonos.length());
								tono = "Lmax" + maxlength;
								
							}
							%>
							<script>
								var tonosAnteriores = tonosActivosBuiltin.length;
								tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
								tonosActivosBuiltin[tonosAnteriores].tono = '<%=tono%>';
								tonosActivosBuiltin[tonosAnteriores].accion = '<%=accion%>';
							</script>
							
							<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
							<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
							<%
							/** INICIO EVENTO - COMENTARIO **/
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tono);
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
							/** FIN EVENTO - COMENTARIO **/
						}
					} else if (grammarTonos.endsWith(".grxml") || grammarTonos.endsWith(".GRXML")) {
						%>
							<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarTonos%>'"/>								
						<%
					} else {
						%>
							<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarTonos%>'"/>			
						<%
					}	
				}
			}
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_GENERALES_TONOS", "OK", null, null);
			/** FIN EVENTO - FIN MODULO **/
		}

		//****************************
		// ****** TONOS OPCIONES *****
		// ***************************
		
		// Gramaticas de tonos de las opciones activas
		if(modoInteraccionMenu.equalsIgnoreCase("ASR") && menu.getOpciones() != null && menu.getOpciones().length > 0){
			grammarTonosOpciones = menuConfigurableXML.obtenerGrammarOpciones("DTMF", menu.getOpciones());
			if(grammarTonosOpciones != null){
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_OPCIONES_TONOS", null);
				/** FIN EVENTO - INICIO MODULO **/
			
				for(int i = 0; i < grammarTonosOpciones.length;i++) {
					String grammarTonos = grammarTonosOpciones[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){
						
						/** INICIO EVENTO - COMENTARIO **/
						menuConfigurableXML.getLog().comment("grammarTonos[" + i + "]=" + grammarTonos);
						/** FIN EVENTO - COMENTARIO **/
					
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")){
							if(grammarTonosOpciones[i].getMinLength() != 0){
								// hay definido un minimo
								if(!grammarTonos.contains("minlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "minlength=" + grammarTonosOpciones[i].getMinLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
							if(grammarTonosOpciones[i].getMaxLength() != 0){
								// hay definido un maximo
								if(!grammarTonos.contains("maxlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "maxlength=" + grammarTonosOpciones[i].getMaxLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
						
							%>
							<grammar src="<%=grammarTonos%>" />			
							<%
							// 07062016 como la gramatica es builtin, ado los tonos activos por opcion al array de tonos activos en builtin
							String tonoActivo = menu.getOpciones()[i].getConfiguracion().getPosicion();
							String accion = menu.getOpciones()[i].getConfiguracion().getAccion();
							%>
							<script>
								var tonosAnteriores = tonosActivosBuiltin.length;
								tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
								tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonoActivo%>';
								tonosActivosBuiltin[tonosAnteriores].accion = '<%=accion%>';
							</script>
							
							<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
							<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
							
							<%	
							/** INICIO EVENTO - COMENTARIO **/
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tonoActivo);
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
							/** FIN EVENTO - COMENTARIO **/
							
						} else if (grammarTonos.endsWith(".grxml") || grammarTonos.endsWith(".GRXML")) {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarTonos%>'"/>								
							<%
						} else {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarTonos%>'"/>			
							<%
						}	
					}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_OPCIONES_TONOS", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		}				

%>	
	
		<!--
		********************
		** INICIO NOMATCH **
		********************
		-->		
		<catch event="nomatch">	
		
			<log label="MENU-PLANTILLA"><value expr="'NOMATCH: en intento ASRDTMF'"/></log>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
		
			<!-- SE OFRECE LA LOCUCION DE NOMATCH DEL INTENTO CORRESPONDIENTE -->
			<!-- El usuario interactua de forma incorrecta por DTMF. -->
			<if cond="lastresult$.inputmode == 'dtmf'">	
			
				<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
								
			<!-- El usuario interactua de forma incorrecta por ASR. -->
			<else/>	
			
				<assign name="tipoReconocimientoEmpleado" expr="'ASR'"/>
			
			</if>			
			
			<!-- ABALFARO-20161005 -->
			<if cond="tipoPrompts_NM_Voz[numIntentosNoMatch] == 'WAV'">
				<prompt bargein="false">					
					<audio expr="prompts_NM_Voz[numIntentosNoMatch]"/>									
				</prompt>
			<elseif cond="tipoPrompts_NM_Voz[numIntentosNoMatch] == 'VOC'"/>
				<prompt bargein="false">					
					<audio expr="'builtin:'+prompts_NM_Voz[numIntentosNoMatch]"/>								
				</prompt>
			<else/>
				<prompt bargein="false">					
					<value expr="prompts_NM_Voz[numIntentosNoMatch]"/>				
				</prompt>
			</if>
				
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosVoz[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
						
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "ASRDTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOMATCH";
						dialogoMenu.respuestaRaw = "";
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNM = new LocucionMenu(); 	
					if(tipoReconocimientoEmpleado == 'ASR'){
						locucionNM.tipoLocucion = tipoPrompts_NM_Voz[numIntentosNoMatch];
						locucionNM.idLocucion = prompts_NM_Voz[numIntentosNoMatch];	
					} else {
						locucionNM.tipoLocucion = tipoPrompts_NM_Tonos[numIntentosNoMatch];
						locucionNM.idLocucion = prompts_NM_Tonos[numIntentosNoMatch];	
					}
					intentoMenu.elementos[numElems] = locucionNM;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoMatch" expr="numIntentosNoMatch + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales: ' + numIntentosTotales"/></log>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosVoz: <%=numIntentosVoz%>'"/></log>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			

			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoMatch &lt; <%=numIntentosVoz%>">	
		
				<if cond="numIntentosTotales == <%=numIntentosVoz%>">

					<!-- Control paso a tonos -->
					<if cond="<%=numIntentosTonos%> == 0">	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
						
						
						<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
						
						
						<goto next="#ESCRIBE_RESULTADO_MENU"/>
						
					<else/>	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
										
						<goto next="#DE_VOZ_A_TONO"/>
					
					</if>	
								
				</if>

				<log label="MENU-PLANTILLA"><value expr="'voy a hacer reprompt'"/></log>

				<clear namelist="menu_VozTonos" />
				<reprompt/>	
				
			<else/>
			
				<!-- Control paso a tonos -->
				<if cond="<%=numIntentosTonos%> == 0">
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
						<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. ado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>
										
				<else/>				
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
					<goto next="#DE_VOZ_A_TONO"/>
				
				</if>
				
			</if>			
		</catch> 
		
		<!--
		*****************
		** FIN NOMATCH **
		*****************
		-->
		
		<!--
		******************************
		** INICIO NOMATCH EXCEPCION **
		******************************
		-->		
					
		<!-- ABALFARO-20170404 -->
		<catch event="nomatch_excepcion">	
		
			<log label="MENU-PLANTILLA"><value expr="'NOMATCH EXCEPCION: en intento ASRDTMF'"/></log>
			
			<log label="MENU-PLANTILLA"><value expr="'numExcepcionLanzada: ' + numExcepcionLanzada"/></log>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
		
			<!-- SE OFRECE LA LOCUCION DE NOMATCH EXCEPCION DEL INTENTO CORRESPONDIENTE -->
			<!-- El usuario interactua de forma incorrecta por DTMF. -->
			<if cond="lastresult$.inputmode == 'dtmf'">				
				<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
				
				<!-- reproduzco la locucion de nomatch excepcion por tonos -->
				<if cond="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'WAV'">
					<prompt bargein="false">					
						<audio expr="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>									
					</prompt>
				<elseif cond="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'VOC'"/>
					<prompt bargein="false">					
						<audio expr="'builtin:'+ prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>								
					</prompt>
				<else/>
					<prompt bargein="false">					
						<value expr="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>				
					</prompt>
				</if>

								
			<!-- El usuario interactua de forma incorrecta por ASR. -->
			<else/>	
				<assign name="tipoReconocimientoEmpleado" expr="'ASR'"/>
				
				<!-- reproduzco la locucion de nomatch excepcion por voz -->
				<if cond="prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'WAV'">
					<prompt bargein="false">					
						<audio expr="prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>									
					</prompt>
				<elseif cond="prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'VOC'"/>
					<prompt bargein="false">					
						<audio expr="'builtin:'+ prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>								
					</prompt>
				<else/>
					<prompt bargein="false">					
						<value expr="prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>				
					</prompt>
				</if>
			
			</if>			
				
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosVoz[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
						
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "ASRDTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOMATCH";
						dialogoMenu.respuestaRaw = respuestaNMExcepcion;
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNM = new LocucionMenu(); 	
					if(tipoReconocimientoEmpleado == 'ASR'){
						locucionNM.tipoLocucion = prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion;
						locucionNM.idLocucion = prompts_NM_Excepcion_Voz[numExcepcionLanzada][numIntentosNoMatch].idLocucion;	
					} else {
						locucionNM.tipoLocucion = prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion;
						locucionNM.idLocucion = prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion;	
					}
					intentoMenu.elementos[numElems] = locucionNM;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoMatch" expr="numIntentosNoMatch + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales: ' + numIntentosTotales"/></log>
			
			<log label="MENU-PLANTILLA"><value expr="'numIntentosVoz: <%=numIntentosVoz%>'"/></log>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			

			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoMatch &lt; <%=numIntentosVoz%>">	
		
				<if cond="numIntentosTotales == <%=numIntentosVoz%>">

					<!-- Control paso a tonos -->
					<if cond="<%=numIntentosTonos%> == 0">	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
						
						
						<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
						
						
						<goto next="#ESCRIBE_RESULTADO_MENU"/>
						
					<else/>	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
										
						<goto next="#DE_VOZ_A_TONO"/>
					
					</if>	
								
				</if>

				<log label="MENU-PLANTILLA"><value expr="'voy a hacer reprompt'"/></log>

				<clear namelist="menu_VozTonos" />
				<reprompt/>	
				
			<else/>
			
				<!-- Control paso a tonos -->
				<if cond="<%=numIntentosTonos%> == 0">
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
						<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>
										
				<else/>				
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
					<goto next="#DE_VOZ_A_TONO"/>
				
				</if>
				
			</if>	
		</catch>
		<!--
		***************************
		** FIN NOMATCH EXCEPCION **
		***************************
		-->	
		
		<!--
		********************
		** INICIO NOINPUT **
		********************
		-->					
		<catch event="noinput">
		
			<log label="MENU-PLANTILLA"><value expr="'NOINPUT: en intento ASRDTMF'"/></log>
		
			<!-- SE OFRECE LA LOCUCION DE NOMATCH DEL INTENTO CORRESPONDIENTE -->
			<if cond="tipoPrompts_NI_Voz[numIntentosNoInput] == 'WAV'">
				<prompt bargein="false">					
					<audio expr="prompts_NI_Voz[numIntentosNoInput]"/>									
				</prompt>
			<elseif cond="tipoPrompts_NI_Voz[numIntentosNoInput] == 'VOC'"/>
				<prompt bargein="false">					
					<audio expr="'builtin:'+prompts_NI_Voz[numIntentosNoInput]"/>									
				</prompt>	
			<else/>
				<prompt bargein="false">					
					<value expr="prompts_NI_Voz[numIntentosNoInput]"/>				
				</prompt>
			</if>
			
			<assign name="tipoReconocimientoEmpleado" expr="'-'"/>
			
	
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[				
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
			
					for(var i=0; i<PRM_promptsIntentosVoz[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();
							locucionMenu.tipoLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
					}
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "ASRDTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOINPUT";
						dialogoMenu.respuestaRaw = "";
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNI = new LocucionMenu(); 	
						locucionNI.tipoLocucion = tipoPrompts_NI_Voz[numIntentosNoMatch];
						locucionNI.idLocucion = prompts_NI_Voz[numIntentosNoMatch];	
					intentoMenu.elementos[numElems] = locucionNI;		
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">			
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion: ' + elemento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo: ' + elemento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>

			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoInput" expr="numIntentosNoInput + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>
			
			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoInput &lt; <%=numIntentosVoz%>">	
		
				<if cond="numIntentosTotales == <%=numIntentosVoz%>">										

					<!-- Control paso a tonos -->
					<if cond="<%=numIntentosTonos%> == 0">	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
				
						<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
				
						<goto next="#ESCRIBE_RESULTADO_MENU"/>	
						
					<else/>	
						<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
						<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
						<assign name="PRM_OUT_MENU_error" expr="''"/>
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
									
						<goto next="#DE_VOZ_A_TONO"/>
					
					</if>	
				
				</if>
			
				<clear namelist="menu_VozTonos" />
				<reprompt/>	
				
			<else/>
			
				<!-- Control paso a tonos -->
				<if cond="<%=numIntentosTonos%> == 0">
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
					
					<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>
					
				<else/>				
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'PASO_A_TONOS'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
					
					<goto next="#DE_VOZ_A_TONO"/>
				
				</if>
				
			</if>	
			
		</catch> 
		<!--
		*****************
		** FIN NOINPUT **
		*****************
		-->
		
		<!--
		****************************************
		** LOCUCIONES DE LOS INTENTOS DE MENU **
		****************************************
		-->	
		
		<%		
		if(modoInteraccionMenu.equalsIgnoreCase("ASR")){
		
			if(numIntentosVozInt > 0){	
				if(modoReproduccionMenu.equalsIgnoreCase("WAV")){
					// es wav
%>
					<prompt>			
						<foreach item="locucion" array="PRM_promptsIntentosVoz[numIntentosTotales]">	
							<!-- no permite if ni logs -->
							<audio expr="locucion.idLocucion"/>					
						</foreach>	
					</prompt>
<%
				} else {
					// es tts
%>
					<prompt>			
						<foreach item="locucion" array="PRM_promptsIntentosVoz[numIntentosTotales]">	
							
							<!-- no permite if ni logs -->
							<value expr="locucion.idLocucion"/>					
						</foreach>	
					</prompt>
<%	
				} 
			} 
		} // fin locucion menu
		%>
	
			

		<filled>
			
			<log label="MENU-PLANTILLA"><value expr="'FILLED: en intento ASRDTMF'"/></log>
			
			
			
			
			
			<!-- [ABALFARO_20170330]  -->
			<%
				if(menu.getExcepciones() != null && menu.getExcepciones().length != 0){
					// hay excepciones a tratar
					int numExcepVozActual = -1;
					int numExcepTonosActual = -1;
					int numExcepActual = -1;
					for(int i=0; i<menu.getExcepciones().length;i ++){
						// por cada excepcion declarada
						String modeExcepcion = menu.getExcepciones()[i].getMode();
						String typeExcepcion = menu.getExcepciones()[i].getType();
						if(modeExcepcion.equals("ASR")){
							modeExcepcion = "voice";
							numExcepVozActual = numExcepVozActual + 1;
							numExcepActual = numExcepVozActual;
						} else {
							numExcepTonosActual = numExcepTonosActual + 1;
							numExcepActual = numExcepTonosActual;
						}
						modeExcepcion = modeExcepcion.toLowerCase();
						
						%>
						<log label="MENU-PLANTILLA"><value expr="'modeExcepcion: <%=modeExcepcion%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'typeExcepcion: <%=typeExcepcion%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'menu_VozTonos: ' + menu_VozTonos"/></log>
						
						<log label="MENU-PLANTILLA"><value expr="'numExcepVozActual: <%=numExcepVozActual%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'numExcepTonosActual: <%=numExcepTonosActual%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'numExcepActual: <%=numExcepActual%>'"/></log>
							
						<if cond="lastresult$.inputmode == '<%=modeExcepcion%>'">
							<!-- el modo de la excepcion coincide con el de la interaccion del usuario -->
						<%											
							String scriptExcepcion = menu.getExcepciones()[i].getScript();
							%>
							<script src="<%=scriptExcepcion%>"/>
							<%	
							String functionExcepcion = menu.getExcepciones()[i].getFunction();
							%>
							
							<!-- asigno el tipoReconocimientoEmpleado por si lanzo el NM excepcion -->
							<if cond="lastresult$.inputmode == 'dtmf'">								
								<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
							<else/>
								<assign name="tipoReconocimientoEmpleado" expr="'ASR'"/>
							</if>
							
							<var name="resultadoExcepcion" expr="''"/>
	
<%-- 							<log label="MENU-PLANTILLA"><value expr="'<%=functionExcepcion%>(' + menu_VozTonos + ')'"/></log> --%>
<%-- 							<assign name="resultadoExcepcion" expr="<%=functionExcepcion%>(menu_VozTonos)"/> --%>

							<!-- ABALFARO_20170615 -->
							<log label="MENU-PLANTILLA"><value expr="'<%=functionExcepcion%>(' + menu_VozTonos + ', <%=typeExcepcion%>, ' + PRM_paramAdicional + ')'"/></log>
							
							<assign name="resultadoExcepcion" expr="<%=functionExcepcion%>(menu_VozTonos, '<%=typeExcepcion%>', PRM_paramAdicional)"/>
													
							<log label="MENU-PLANTILLA"><value expr="'resultadoExcepcion: ' + resultadoExcepcion"/></log>
							
							
							<if cond="resultadoExcepcion == 'SI' || resultadoExcepcion == 'si'">	
							
								<!-- guardo el id de la excepcion que se ha activado -->
								<log label="MENU-PLANTILLA"><value expr="'numExcepcionLanzada: <%=numExcepActual%>'"/></log>
								<assign name="numExcepcionLanzada" expr="'<%=numExcepActual%>'"/>
								
								<!-- guardo la respuesta que ha dado el cliente -->
								<assign name="respuestaNMExcepcion" expr="menu_VozTonos"/>
								<log label="MENU-PLANTILLA"><value expr="'respuestaNMExcepcion: ' + respuestaNMExcepcion"/></log>
								
								
								<!-- lanzo el evento de la excepcion -->
								<throw event="nomatch_excepcion"/>
							<else/>
								<!-- en otro caso no lanzo la excepcion y continua la ejecucion del filled -->
							</if>
													
						</if>
						<%
					}
				}
			
			%>
			
			
			
			
			<!-- El usuario interactua correctamente por tonos. -->
			<if cond="lastresult$.inputmode == 'dtmf'">								
				
				<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
				
				<!-- 07062016 comprobar si la gramatica es builtin, la respuesta conincide con los tonos activados -->
				<assign name="encontradoTono" expr="'NO'"/>
				<!-- tengo que comprobar que la longitud del array no sea cero porque si no falla el foreach -->
				<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin.length: ' + tonosActivosBuiltin.length"/></log>
				<if cond="tonosActivosBuiltin.length != 0">
					<foreach item="tonoActivo" array="tonosActivosBuiltin">
						<if cond="tonoActivo.tono == menu_VozTonos">
							<assign name="encontradoTono" expr="'SI'"/>	
							
							<!-- cojo como respuesta de usuario la accion de la opcion -->
							<assign name="PRM_OUT_MENU_respuestaUsuario" expr="tonoActivo.accion"/>
							<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_VozTonos$.utterance"/>
							
						<else/>
							<script>
								var esLongitud = 'false';
								var tonoAct = tonoActivo.tono;
								if(tonoAct.substring(0,1) == 'L'){
									esLongitud = 'true';
								}
							</script>
							<if cond="esLongitud == 'true'">
								<!-- hay una longitud de tonos activa -->
								<log label="MENU-PLANTILLA"><value expr="'paso a comprobar por longitud'"/></log>
								<log label="MENU-PLANTILLA"><value expr="'menu_VozTonos: ' + menu_VozTonos"/></log>
								

								<script><![CDATA[
									tonoAct = tonoActivo.tono;
									if(tonoAct.substring(0,1) == 'L'){
										
										// ABALFARO_20170331
										if((tonoAct.indexOf('Lmin') != -1) && (tonoAct.indexOf('Lmax') != -1)){
											// el tono contiene un maximo y un minimo
											var longitudMin = tonoAct.substring(4,tonoAct.indexOf('Lmax'));
											var longitudMax = tonoAct.substring(tonoAct.indexOf('Lmax') + 4, tonoAct.length);
											if(menu_VozTonos.length >= longitudMin && menu_VozTonos.length <= longitudMax){
												// el dato es correcto porque su longitud esta entre el minimo y maximo de la del builtin
												encontradoTono = 'SI';
											}
										} else if(tonoAct.indexOf('Lmin') != -1){
											// el tono contiene un minimo
											var longitud = tonoAct.substring(4, tonoAct.length);
											if(menu_VozTonos.length >= longitud){
												// el dato es correcto porque su longitud es mayor o igual que la del builtin
												encontradoTono = 'SI';
											}
										} else if(tonoAct.indexOf('Lmax') != -1){
											// el tono contiene un maximo
											var longitud = tonoAct.substring(4, tonoAct.length);
											if(menu_VozTonos.length <= longitud){
												// el dato es correcto porque su longitud es menor o igual que la del builtin
												encontradoTono = 'SI';
											}
										
										} else {
											// el tono es una longitud concreta
											var longitud = tonoAct.substring(1, tonoAct.length);
											if(menu_VozTonos.length == longitud){
												// el dato es correcto porque su longitud coincide con la del builtin
												encontradoTono = 'SI';
											}
										}
										
										
									}
								]]></script>

								<log label="MENU-PLANTILLA"><value expr="'tonoAct: ' + tonoAct"/></log>
								<log label="MENU-PLANTILLA"><value expr="'longitud: ' + longitud"/></log>
								<if cond="encontradoTono == 'SI'">
									<!-- cojo como respuesta de usuario lo mismo que ha insertado -->
									<assign name="PRM_OUT_MENU_respuestaUsuario" expr="menu_VozTonos"/>
									<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_VozTonos$.utterance"/>
								</if>
							</if>
						</if>
					</foreach>
					<log label="MENU-PLANTILLA"><value expr="'encontradoTono: ' + encontradoTono"/></log>
					<if cond="encontradoTono != 'SI'">
						<!-- NO han respondido un tono activo, lanzo nomatch -->
						
<!-- 						<clear namelist="menu_VozTonos" />  -->
						
						<throw event="nomatch"/>
					</if>					
				<else/>
				
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="menu_VozTonos"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_VozTonos$.utterance"/>
				</if>
				<!-- fin - 07062016 -->
													
			<!-- El usuario interactua correctamente por voz. -->
			<else/>	
				<log label="MENU-PLANTILLA"><value expr="'RECONOCIMIENTO VOZ *******'"/></log>
				<log label="MENU-PLANTILLA"><value expr="'menu_VozTonos: ' + menu_VozTonos"/></log>
				<log label="MENU-PLANTILLA"><value expr="'menu_VozTonos$.interpretation: ' + menu_VozTonos$.interpretation"/></log>
				<log label="MENU-PLANTILLA"><value expr="'menu_VozTonos$.utterance: ' + menu_VozTonos$.utterance"/></log>
				
				

				
				<assign name="tipoReconocimientoEmpleado" expr="'ASR'"/>		

				<assign name="PRM_OUT_MENU_respuestaUsuario" expr="menu_VozTonos"/>
				<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_VozTonos$.utterance"/>
			
			</if>
			
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosVoz[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosVoz[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
						
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "ASRDTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = PRM_OUT_MENU_respuestaUsuario;
						dialogoMenu.respuestaRaw = PRM_OUT_MENU_respuestaUsuarioRaw;
					intentoMenu.elementos[numElems] = dialogoMenu;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			
			<assign name="numIntentosTotales" expr="numIntentosTotales + 1"/>
			
			
			<assign name="PRM_OUT_MENU_codigoRetorno" expr="'OK'"/>
			<assign name="PRM_OUT_MENU_error" expr="''"/>
			<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
			<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
			<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
				
		</filled>
		
	</field>
	
<%
} // fin continuar
%>	
	<block>
		<goto next="#ESCRIBE_RESULTADO_MENU"/>
	</block>
	
</form>	


<!--
*************************************************
********** CONTROL DE PASO A TONOS **************
*************************************************
-->
<form id="DE_VOZ_A_TONO">

	<block>	
	
		<log label="MENU-PLANTILLA"><value expr="'PASO A TONOS'"/></log>
		
		<!--
		****************************************************************************************************************
		** INICIALIZACION DE LOS CONTADORES DE INTENTOS Y DEL ARRAY QUE ALMACENA LAS LOCUCIONES QUE SE VAN OFRECIENDO **
		****************************************************************************************************************
		-->	
		<assign name="numIntentosTotales" expr="0"/>
		<assign name="numIntentosNoMatch" expr="0"/>
		<assign name="numIntentosNoInput" expr="0"/>
		<assign name="tipoReconocimientoEmpleado" expr="''"/>
		
		<script>
			// cada elemento: tonosActivosBuiltin[i] = new InfoTonosActivos();
			tonosActivosBuiltin = new Array();
		</script>
		
			<!--
			*****************************************
			** INICIALIZACION VARIABLE TRAZAS MENU **
			*****************************************
			-->	
			
<!-- 					<script> -->
<!-- 						PRM_OUT_MENU_trazas = new LoggerMenu();		 -->
<!-- 						PRM_OUT_MENU_trazas.intentos = new Array(); -->
<!-- 					</script> -->
			<script>
			if(PRM_OUT_MENU_trazas == null || PRM_OUT_MENU_trazas == ''){
				PRM_OUT_MENU_trazas = new LoggerMenu();		
			}
			if(PRM_OUT_MENU_trazas.intentos == null){
				PRM_OUT_MENU_trazas.intentos = new Array();
			}
			</script>
	

	<%
	if(modoInteraccionMenu.equalsIgnoreCase("ASR") && menu.getConfigAsr().getPrompts().getPromptAsr2Dtmf()!=null){
		
		String modoPasoATonos = menu.getConfigAsr().getPrompts().getPromptAsr2Dtmf().getModo();
		String locPasoATonos = menu.getConfigAsr().getPrompts().getPromptAsr2Dtmf().getValue();

		if (modoPasoATonos.equalsIgnoreCase("WAV")) {
	%>			
				<prompt bargein="false">					
					<audio expr="'<%=locPasoATonos%>'"/>								
				</prompt>
	<%
		} else if(modoPasoATonos.equalsIgnoreCase("VOC")){
	%>			
				<prompt bargein="false">					
					<audio expr="'builtin:<%=locPasoATonos%>'"/>								
				</prompt>
	<%			
		} else {
	%>			
				<prompt bargein="false">					
					<value expr="'<%=locPasoATonos%>'"/>					
				</prompt>
	<%
		}		
	%>
		<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
		<script>
			var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
			var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
			
			var locucionVoz2Tonos = new LocucionMenu(); 	
					locucionVoz2Tonos.tipoLocucion = '<%=modoPasoATonos%>';
					locucionVoz2Tonos.idLocucion = '<%=locPasoATonos%>';	
					
			PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionVoz2Tonos;
		</script>
				
		<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
		<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
		
		<log label="MENU-PLANTILLA"><value expr="'locucionVoz2Tonos.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
		<log label="MENU-PLANTILLA"><value expr="'locucionVoz2Tonos.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
		
	<%
	} // fin if hay loc de paso a tonos
	%>
		
		
		
		<goto next="#MENU_DTMF"/>
				
	</block>		

</form>	


<!--
**************************************************
********** RECONOCIMIENTO POR TONOS **************
**************************************************
-->
<form id="MENU_DTMF">

	<block>
	<%
	
	if(continuar){
		//*************************************
		// ****** TONOS ACTIVOS GENERALES *****
		// ************************************
		try {
			//Recorro las gramaticas de tonos
			if(menu.getConfigDtmf().getGrammars() != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < menu.getConfigDtmf().getGrammars().length;i++) {
					String grammarTonos = menu.getConfigDtmf().getGrammars()[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){					
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {
							// 07062016 como la gramatica es builtin, me guardo el numero de tonos activos 
							String tonosActivos = menu.getConfigDtmf().getGrammars()[i].getTonosActivos();
							if(tonosActivos != null){
								String[] tonosActivosArray = tonosActivos.split(",");
								for(int j= 0; j< tonosActivosArray.length; j++) {
									%>
									<script>
										var tonosAnteriores = tonosActivosBuiltin.length;
										tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
										tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonosActivosArray[j]%>';
										tonosActivosBuiltin[tonosAnteriores].accion = '<%=tonosActivosArray[j]%>';
									</script>
									
									<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
									<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
									<%
									/** INICIO EVENTO - COMENTARIO **/
									menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].tono=" + tonosActivosArray[j]);
									menuConfigurableXML.getLog().comment("tonoActivo[" + j + "].accion=" + tonosActivosArray[j]);
									/** FIN EVENTO - COMENTARIO **/
								}			
							} else {
								// [20160825] si la gramatica no tiene tonos activos pero SI longitud la incluyo como L5 para length=5
								// y accion "=" para luego saber que tengo que devolver la respuesta tal cual
								// elimino los espacios en blanco
								grammarTonos = grammarTonos.trim();
								String tono = "";
								String accion = "=";								
								if(grammarTonos.contains("length=") && !grammarTonos.contains("minlength=") && !grammarTonos.contains("maxlength=")){
									int index=grammarTonos.indexOf("length=");
									tono = "L" + grammarTonos.substring(index + ("length=".length()), grammarTonos.length());
									
								} else if(grammarTonos.contains("minlength=") && grammarTonos.contains("maxlength=")){
									String paramsGrammar = grammarTonos.substring(grammarTonos.indexOf("?") + 1, grammarTonos.length());
									int indexMin=paramsGrammar.indexOf("minlength=");
									int indexMax=paramsGrammar.indexOf("maxlength=");
									String minlength = "";
									String maxlength = "";
									if(indexMin < indexMax){
										// estan definidos minimo y maximo
										// -1 al final por el ; de separacion
										minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), indexMax - 1);
										maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), paramsGrammar.length());
									} else {
										// estan definidos maximo y minimo
										// -1 al final por el ; de separacion
										maxlength = paramsGrammar.substring(indexMax + ("maxlength=".length()), indexMin - 1);
										minlength = paramsGrammar.substring(indexMin + ("minlength=".length()), paramsGrammar.length());
									}
									
									tono = "Lmin" + minlength + "Lmax" + maxlength;
									
									
								} else if(grammarTonos.contains("minlength=")){
									int indexMin=grammarTonos.indexOf("minlength=");
									String minlength = grammarTonos.substring(indexMin + ("minlength=".length()), grammarTonos.length());
									tono = "Lmin" + minlength;
									
								} else if(grammarTonos.contains("maxlength=")){
									int indexMax=grammarTonos.indexOf("maxlength=");
									String maxlength = grammarTonos.substring(indexMax + ("maxlength=".length()), grammarTonos.length());
									tono = "Lmax" + maxlength;
									
								}
								%>
								<script>
									var tonosAnteriores = tonosActivosBuiltin.length;
									tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
									tonosActivosBuiltin[tonosAnteriores].tono = '<%=tono%>';
									tonosActivosBuiltin[tonosAnteriores].accion = '<%=accion%>';
								</script>
								
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
								<%
								/** INICIO EVENTO - COMENTARIO **/
								menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tono);
								menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
								/** FIN EVENTO - COMENTARIO **/
							}
						} 
					}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		} catch(Exception e) {		
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_TONOS_ACTIVOS_GENERALES", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
			
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_GENERALES", "KO", null, null);
			/** FIN EVENTO - FIN MODULO **/
			
			// relleno variables salida Java
			codigoRetorno = "ERROR";
			error = "ERROR_EXT("+e.getMessage()+")";
			
			// paro la ejecucion java
			continuar = false;
			
			%>
						
			<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_TONOS_ACTIVOS_GENERALES'"/></log>
			<goto next="#RETORNO_ERROR_JAVA"/>
		
			<%
		}
	
		//************************************
		// ****** TONOS ACTIVOS OPCIONES *****
		// ***********************************
		
		try {
		
			// Gramaticas de tonos de las opciones activas
			if(grammarTonosOpciones == null && menu.getOpciones() != null && menu.getOpciones().length > 0){
				// cargo las gramaticas de las opciones
				grammarTonosOpciones = menuConfigurableXML.obtenerGrammarOpciones("DTMF", menu.getOpciones());
			}
			if(grammarTonosOpciones != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < grammarTonosOpciones.length;i++) {
					String grammarTonos = grammarTonosOpciones[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {					
							// 07062016 como la gramatica es builtin, aado los tonos activos por opcion al array de tonos activos en builtin
							String tonoActivo = menu.getOpciones()[i].getConfiguracion().getPosicion();
							String accion = menu.getOpciones()[i].getConfiguracion().getAccion();
							%>
								<script>
									var tonosAnteriores = tonosActivosBuiltin.length;
									tonosActivosBuiltin[tonosAnteriores] = new InfoTonosActivos();
									tonosActivosBuiltin[tonosAnteriores].tono = '<%=tonoActivo%>';
									tonosActivosBuiltin[tonosAnteriores].accion= '<%=accion%>';
								</script>
								
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].tono: ' + tonosActivosBuiltin[tonosAnteriores].tono"/></log>
								<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin[tonosAnteriores].accion: ' + tonosActivosBuiltin[tonosAnteriores].accion"/></log>
							
							<%	
							/** INICIO EVENTO - COMENTARIO **/
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].tono=" + tonoActivo);
							menuConfigurableXML.getLog().comment("tonoActivo[" + i + "].accion=" + accion);
							/** FIN EVENTO - COMENTARIO **/
							
						} 
					}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		
		} catch(Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_TONOS_ACTIVOS_OPCIONES", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
			
			/** INICIO EVENTO - FIN MODULO **/
			menuConfigurableXML.getLog().endModuleProcess("CARGA_TONOS_ACTIVOS_OPCIONES", "KO", null, null);
			/** FIN EVENTO - FIN MODULO **/
			
			// relleno variables salida Java
			codigoRetorno = "ERROR";
			error = "ERROR_EXT("+e.getMessage()+")";
			
			// paro la ejecucion java
			continuar = false;
			
			%>
						
			<log label="MENU-PLANTILLA"><value expr="'ERROR: CARGA_TONOS_ACTIVOS_OPCIONES'"/></log>
			<goto next="#RETORNO_ERROR_JAVA"/>
		
			<%
		}
	} // fin continuar
	%>
	

	</block>

<%

if (continuar){
%>

	<property name="inputmodes" value="dtmf"/>	

	<field name="menu_Tonos"> 
		
		<property name="bargein" value="<%=menu.getConfigDtmf().getConfiguracion().getBargeinDtmfActivo().equalsIgnoreCase("ON")?true:false%>"/>
		<property name="timeout" value="<%=menu.getConfigDtmf().getConfiguracion().getTimeoutDtmf()%>s"/>
		<property name="interdigittimeout" value="<%=menu.getConfigDtmf().getConfiguracion().getInterdigitTimeoutDtmf()%>s"/>
	
		
		<!--
		****************************************
		** ACTIVACION DE GRAMATICAS POR TONOS **
		****************************************
		-->			
<%

		//*****************************
		// ****** TONOS GENERALES *****
		// ****************************
		try {
			//Recorro las gramaticas de tonos
			if(menu.getConfigDtmf().getGrammars() != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_GENERALES_TONOS", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < menu.getConfigDtmf().getGrammars().length;i++) {
					String grammarTonos = menu.getConfigDtmf().getGrammars()[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){
					
						/** INICIO EVENTO - COMENTARIO **/
						menuConfigurableXML.getLog().comment("grammarTonos[" + i + "]=" + grammarTonos);
						/** FIN EVENTO - COMENTARIO **/
					
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {
							if(menu.getConfigDtmf().getGrammars()[i].getMinLength() != 0){
								// hay definido un minimo
								if(!grammarTonos.contains("minlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "minlength=" + menu.getConfigDtmf().getGrammars()[i].getMinLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
							if(menu.getConfigDtmf().getGrammars()[i].getMaxLength() != 0){
								// hay definido un maximo
								if(!grammarTonos.contains("maxlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "maxlength=" + menu.getConfigDtmf().getGrammars()[i].getMaxLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
						
							%>
<%-- 							<grammar src="<%=grammarTonos%>" />			 --%>
							<grammar mode="dtmf" src="<%=grammarTonos%>" />	
							
							<%
						} else if (grammarTonos.endsWith(".grxml") || grammarTonos.endsWith(".GRXML")) {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarTonos%>'"/>								
							<%
						} else {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarTonos%>'"/>			
							<%
						}	
					}
				}
				
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_GENERALES_TONOS", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		} catch(Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_GRAMATICAS_GENERALES_TONOS", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
		}

		//****************************
		// ****** TONOS OPCIONES *****
		// ***************************
		
		try {
		
			// Gramaticas de tonos de las opciones activas
			if(grammarTonosOpciones == null && menu.getOpciones() != null && menu.getOpciones().length > 0){
				// cargo las gramaticas
				grammarTonosOpciones = menuConfigurableXML.obtenerGrammarOpciones("DTMF", menu.getOpciones());
			}
			if(grammarTonosOpciones != null){
				
				/** INICIO EVENTO - INICIO MODULO **/
				menuConfigurableXML.getLog().initModuleProcess("CARGA_GRAMATICAS_OPCIONES_TONOS", null);
				/** FIN EVENTO - INICIO MODULO **/
				
				for(int i = 0; i < grammarTonosOpciones.length;i++) {
					String grammarTonos = grammarTonosOpciones[i].getValue();
					// si la gramatica acaba en .grxml o si empieza en builtin
					if(grammarTonos != null){
						
						/** INICIO EVENTO - COMENTARIO **/
						menuConfigurableXML.getLog().comment("grammarTonos[" + i + "]=" + grammarTonos);
						/** FIN EVENTO - COMENTARIO **/
					
						if (grammarTonos.startsWith("builtin") || grammarTonos.startsWith("BUILTIN")) {
							
							if(grammarTonosOpciones[i].getMinLength() != 0){
								// hay definido un minimo
								if(!grammarTonos.contains("minlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "minlength=" + grammarTonosOpciones[i].getMinLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
							if(grammarTonosOpciones[i].getMaxLength() != 0){
								// hay definido un maximo
								if(!grammarTonos.contains("maxlength")){
									// la gramatica no lo tiene definido, lo aado
									if(grammarTonos.endsWith("digits")){
										grammarTonos = grammarTonos + "?";
									} else {
										grammarTonos = grammarTonos + ";";
									}
									grammarTonos = grammarTonos + "maxlength=" + grammarTonosOpciones[i].getMaxLength();
									
									menuConfigurableXML.getLog().comment("grammar=" + grammarTonos);					
								}
							}
							%>
<%-- 							<grammar src="<%=grammarTonos%>" />	 --%>
							<grammar  mode="dtmf" src="<%=grammarTonos%>" />
							
							<%
							
						} else if (grammarTonos.endsWith(".grxml") || grammarTonos.endsWith(".GRXML")) {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs+xml" srcexpr="'<%=grammarTonos%>'"/>								
							<%
						} else {
							%>
								<grammar mode="dtmf" xml:lang="<%=lenguajeMenu%>" maxage="0" weight="1.0" type="application/srgs" srcexpr="'<%=grammarTonos%>'"/>			
							<%
						}	
					}
				}
				/** INICIO EVENTO - FIN MODULO **/
				menuConfigurableXML.getLog().endModuleProcess("CARGA_GRAMATICAS_OPCIONES_TONOS", "OK", null, null);
				/** FIN EVENTO - FIN MODULO **/
			}
		
		} catch(Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			menuConfigurableXML.getLog().error(e.getMessage(), "CARGA_GRAMATICAS_OPCIONES_TONOS", parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
		}
		
%>	
		<!--
		********************
		** INICIO NOMATCH **
		********************
		-->		
		<catch event="nomatch">	
		
			<log label="MENU-PLANTILLA"><value expr="'NOMATCH: en intento tonos'"/></log>
		
<!-- 			SE OFRECE LA LOCUCION DE NOMATCH DEL INTENTO CORRESPONDIENTE -->
			<if cond="tipoPrompts_NM_Tonos[numIntentosNoMatch] == 'WAV'">
				<prompt bargein="false">					
					<audio expr="prompts_NM_Tonos[numIntentosNoMatch]"/>									
				</prompt>
			<elseif cond="tipoPrompts_NM_Tonos[numIntentosNoMatch] == 'VOC'"/>
				<prompt bargein="false">					
					<audio expr="'builtin:'+prompts_NM_Tonos[numIntentosNoMatch]"/>									
				</prompt>
			<else/>
				<prompt bargein="false">					
					<value expr="prompts_NM_Tonos[numIntentosNoMatch]"/>				
				</prompt>
			</if>
			
			<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>	
			
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosTonos[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "DTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOMATCH";
						dialogoMenu.respuestaRaw = "";
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNM = new LocucionMenu(); 	
						locucionNM.tipoLocucion = tipoPrompts_NM_Tonos[numIntentosNoMatch];
						locucionNM.idLocucion = prompts_NM_Tonos[numIntentosNoMatch];	
					intentoMenu.elementos[numElems] = locucionNM;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			
			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoMatch" expr="numIntentosNoMatch + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>
					

			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTonos: <%=numIntentosTonos%>'"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales: ' + numIntentosTotales"/></log>
			
			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoMatch &lt; <%=numIntentosTonos%>">	
			
				<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch &lt; numIntentosTonos'"/></log> 
		
				<if cond="numIntentosTotales == <%=numIntentosTonos%>">	
				
					<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales == numIntentosTonos'"/></log> 
														
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>	
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
					<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>			
				</if>
			
				<log label="MENU-PLANTILLA"><value expr="'HAGO REPROMPT'"/></log>
			
				<clear namelist="menu_Tonos" />
				<reprompt/>	
								
			<else/>
			
				<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
				<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="lastresult$.utterance"/>
				<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
				<assign name="PRM_OUT_MENU_error" expr="''"/>	
				<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
				<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
				<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
				
				<!-- doy locucion de maxint  -->
					<%
					if(!locMaxint.equals("") && !modoMaxint.equals("")){
							if (modoMaxint.equalsIgnoreCase("WAV")) {
						%>			
									<prompt bargein="false">					
										<audio expr="'<%=locMaxint%>'"/>								
									</prompt>
						<%
							} else if(modoMaxint.equalsIgnoreCase("VOC")){
						%>			
									<prompt bargein="false">					
										<audio expr="'builtin:<%=locMaxint%>'"/>								
									</prompt>
						<%			
							} else {
						%>			
									<prompt bargein="false">					
										<value expr="'<%=locMaxint%>'"/>					
									</prompt>
						<%
							}
						%>
							<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
							<script>
								var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
								var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
								
								var locucionMaxint = new LocucionMenu(); 	
									locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
									locucionMaxint.idLocucion = '<%=locMaxint%>';	
										
								PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
							</script>
									
							<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
							<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
							
							<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
							<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
						<%
					}
					%>
				
				<goto next="#ESCRIBE_RESULTADO_MENU"/>
				
			</if>			
		</catch> 
		<!--
		*****************
		** FIN NOMATCH **
		*****************
		-->
		
		<!--
		******************************
		** INICIO NOMATCH EXCEPCION **
		******************************
		-->		
		<catch event="nomatch_excepcion">	
		
			<log label="MENU-PLANTILLA"><value expr="'NOMATCH EXCEPCION: en intento tonos'"/></log>
			
			<!-- reproduzco la locucion de nomatch excepcion por tonos -->
			<if cond="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'WAV'">
				<prompt bargein="false">					
					<audio expr="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>									
				</prompt>
			<elseif cond="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion == 'VOC'"/>
				<prompt bargein="false">					
					<audio expr="'builtin:'+ prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>								
				</prompt>
			<else/>
				<prompt bargein="false">					
					<value expr="prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion"/>				
				</prompt>
			</if>
			
			<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>	
			
			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosTonos[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "DTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOMATCH";
						dialogoMenu.respuestaRaw = respuestaNMExcepcion;
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNM = new LocucionMenu(); 	
						locucionNM.tipoLocucion = prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].tipoLocucion;
						locucionNM.idLocucion = prompts_NM_Excepcion_Tonos[numExcepcionLanzada][numIntentosNoMatch].idLocucion;	
					intentoMenu.elementos[numElems] = locucionNM;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			
			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoMatch" expr="numIntentosNoMatch + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>
					

			<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch: ' + numIntentosNoMatch"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTonos: <%=numIntentosTonos%>'"/></log>
			<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales: ' + numIntentosTotales"/></log>
			
			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoMatch &lt; <%=numIntentosTonos%>">	
			
				<log label="MENU-PLANTILLA"><value expr="'numIntentosNoMatch &lt; numIntentosTonos'"/></log> 
		
				<if cond="numIntentosTotales == <%=numIntentosTonos%>">	
				
					<log label="MENU-PLANTILLA"><value expr="'numIntentosTotales == numIntentosTonos'"/></log> 
														
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>	
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
					
					<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>			
				</if>
			
				<log label="MENU-PLANTILLA"><value expr="'HAGO REPROMPT'"/></log>
			
				<clear namelist="menu_Tonos" />
				<reprompt/>	
								
			<else/>
			
				<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOMATCH'"/>
				<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="respuestaNMExcepcion"/>
				<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
				<assign name="PRM_OUT_MENU_error" expr="''"/>	
				<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
				<assign name="PRM_OUT_MENU_modoInteraccion" expr="lastresult$.inputmode"/>
				<assign name="PRM_OUT_MENU_nivelConfianza" expr="lastresult$.confidence"/>
				
				<!-- doy locucion de maxint  -->
					<%
					if(!locMaxint.equals("") && !modoMaxint.equals("")){
							if (modoMaxint.equalsIgnoreCase("WAV")) {
						%>			
									<prompt bargein="false">					
										<audio expr="'<%=locMaxint%>'"/>								
									</prompt>
						<%
							} else if(modoMaxint.equalsIgnoreCase("VOC")){
						%>			
									<prompt bargein="false">					
										<audio expr="'builtin:<%=locMaxint%>'"/>								
									</prompt>
						<%			
							} else {
						%>			
									<prompt bargein="false">					
										<value expr="'<%=locMaxint%>'"/>					
									</prompt>
						<%
							}
						%>
							<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
							<script>
								var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
								var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
								
								var locucionMaxint = new LocucionMenu(); 	
									locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
									locucionMaxint.idLocucion = '<%=locMaxint%>';	
										
								PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
							</script>
									
							<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
							<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
							
							<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
							<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
						<%
					}
					%>
				
				<goto next="#ESCRIBE_RESULTADO_MENU"/>
				
			</if>			
		</catch> 
		<!--
		***************************
		** FIN NOMATCH EXCEPCION **
		***************************
		-->
		
		<!--
		********************
		** INICIO NOINPUT **
		********************
		-->					
		<catch event="noinput">
		
			<log label="MENU-PLANTILLA"><value expr="'NOINPUT: en intento tonos'"/></log>
		
			<!-- SE OFRECE LA LOCUCION DE NOMATCH DEL INTENTO CORRESPONDIENTE -->
			<if cond="tipoPrompts_NI_Tonos[numIntentosNoInput] == 'WAV'">
				<prompt bargein="false">					
					<audio expr="prompts_NI_Tonos[numIntentosNoInput]"/>									
				</prompt>
			<elseif cond="tipoPrompts_NI_Tonos[numIntentosNoInput] == 'VOC'"/>
				<prompt bargein="false">					
					<audio expr="'builtin:'+prompts_NI_Tonos[numIntentosNoInput]"/>									
				</prompt>
			<else/>
				<prompt bargein="false">					
					<value expr="prompts_NI_Tonos[numIntentosNoInput]"/>				
				</prompt>
			</if>
			
			<assign name="tipoReconocimientoEmpleado" expr="'-'"/>
			

			<!-- TRAZAS DEL INTENTO -->
			<script><![CDATA[
			
				var intentoMenu = new IntentoMenu();
					intentoMenu.elementos = new Array();
					
					for(var i=0; i<PRM_promptsIntentosTonos[numIntentosTotales].length; i++){
						var locucionMenu = new LocucionMenu();	
							locucionMenu.tipoLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].tipoLocucion;
							locucionMenu.idLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].idLocucion;
						intentoMenu.elementos[i] = locucionMenu;
					}
						
					var numElems = intentoMenu.elementos.length;
				
					var dialogoMenu = new DialogoMenu();
						dialogoMenu.idMenu = PRM_idMenu;
						dialogoMenu.recDisponible = "DTMF";
						dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
						dialogoMenu.numIntento = numIntentosTotales + 1;
						dialogoMenu.respuesta = "NOINPUT";
						dialogoMenu.respuestaRaw = "";
					intentoMenu.elementos[numElems] = dialogoMenu;	
					
					numElems = numElems + 1;
					
					var locucionNI = new LocucionMenu(); 	
						locucionNI.tipoLocucion = tipoPrompts_NI_Tonos[numIntentosNoMatch];
						locucionNI.idLocucion = prompts_NI_Tonos[numIntentosNoMatch];	
					intentoMenu.elementos[numElems] = locucionNI;	
						
				var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
				PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
			]]></script>
			
			<!-- IMPRIMO TRAZAS DEL INTENTO -->
			<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
			
				<script>
					var tipoElemento;
					if(elemento instanceof LocucionMenu){
						tipoElemento = "LocucionMenu";
					} 
					if(elemento instanceof DialogoMenu){
						tipoElemento = "DialogoMenu";
					}
				</script>
				<if cond="tipoElemento == 'LocucionMenu'">
					<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
					<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
				</if>
				<if cond="tipoElemento == 'DialogoMenu'">
					<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
					<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
				</if>
			</foreach>
			
			<!-- SE ACTUALIZAN CONTADORES -->
			<assign name="numIntentosNoInput" expr="numIntentosNoInput + 1"/>
			<assign name="numIntentosTotales" expr="numIntentosNoMatch + numIntentosNoInput"/>			
			
			<!-- SE CONTROLA SI SE OFRECE UN NUEVO INTENTO O SE SALE DEL MENU -->
			<if cond="numIntentosNoInput &lt; <%=numIntentosTonos%>">	
					
				<if cond="numIntentosTotales == <%=numIntentosTonos%>">										
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
					<assign name="PRM_OUT_MENU_error" expr="''"/>	
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
					
					<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>		
				</if>
			
				<clear namelist="menu_Tonos" />
				<reprompt/>	
				
			<else/>
			
				<assign name="PRM_OUT_MENU_respuestaUsuario" expr="'NOINPUT'"/>
				<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
				<assign name="PRM_OUT_MENU_codigoRetorno" expr="'MAXINT'"/>	
				<assign name="PRM_OUT_MENU_error" expr="''"/>	
				<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
				<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
				<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>
				
				<!-- doy locucion de maxint  -->
						<%
						if(!locMaxint.equals("") && !modoMaxint.equals("")){
								if (modoMaxint.equalsIgnoreCase("WAV")) {
							%>			
										<prompt bargein="false">					
											<audio expr="'<%=locMaxint%>'"/>								
										</prompt>
							<%
								} else if(modoMaxint.equalsIgnoreCase("VOC")){
							%>			
										<prompt bargein="false">					
											<audio expr="'builtin:<%=locMaxint%>'"/>								
										</prompt>
							<%			
								} else {
							%>			
										<prompt bargein="false">					
											<value expr="'<%=locMaxint%>'"/>					
										</prompt>
							<%
								}
							%>
								<!-- TRAZAS DEL INTENTO. Aado un elemento al ultimo intento -->
								<script>
									var numIntentosUltimo = PRM_OUT_MENU_trazas.intentos.length - 1;
									var numElems = PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos.length;
									
									var locucionMaxint = new LocucionMenu(); 	
										locucionMaxint.tipoLocucion = '<%=modoMaxint%>';
										locucionMaxint.idLocucion = '<%=locMaxint%>';	
											
									PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems] = locucionMaxint;
								</script>
										
								<log label="MENU-PLANTILLA"><value expr="'numIntentosUltimo: ' + numIntentosUltimo"/></log>
								<log label="MENU-PLANTILLA"><value expr="'numElems: ' + numElems"/></log>
								
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.tipoLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].tipoLocucion"/></log>
								<log label="MENU-PLANTILLA"><value expr="'locucionMaxint.idLocucion: ' + PRM_OUT_MENU_trazas.intentos[numIntentosUltimo].elementos[numElems].idLocucion"/></log>
							<%
						}
						%>
				
				<goto next="#ESCRIBE_RESULTADO_MENU"/>
				
			</if>	
		</catch> 
		<!--
		*****************
		** FIN NOINPUT **
		*****************
		-->

		<!--
		****************************************
		** LOCUCIONES DE LOS INTENTOS DE MENU **
		****************************************
		-->	
<%
		if(modoReproduccionMenu.equalsIgnoreCase("WAV")){
			// es wav
		%>
			<prompt>			
				<foreach item="locucion" array="PRM_promptsIntentosTonos[numIntentosTotales]">	
					<!-- no permite if ni logs -->
					<audio expr="locucion.idLocucion"/>					
				</foreach>	
			</prompt>
		<%
		} else {
			// es tts
		%>
			<prompt>			
				<foreach item="locucion" array="PRM_promptsIntentosTonos[numIntentosTotales]">	
					<!-- no permite if ni logs -->
					<value expr="locucion.idLocucion"/>					
				</foreach>	
			</prompt>
		<%	
		} // fin locuciones menu
%>

				
		<filled>
	
	
	
			<!-- [ABALFARO_20170330]  -->
			<%
				if(menu.getExcepciones() != null && menu.getExcepciones().length != 0){
					// hay excepciones a tratar
					int numExcepTonosActual = -1;
					int numExcepActual = -1;
					for(int i=0; i<menu.getExcepciones().length;i ++){
						// por cada excepcion declarada
						
						
						String modeExcepcion = menu.getExcepciones()[i].getMode();
						String typeExcepcion = menu.getExcepciones()[i].getType();
						if(modeExcepcion.equals("ASR")){
							// como estamos en menu solo por tonos esto no aplica, voy a buscar la siguiente excepcion
							continue;
						} else {
							// if(modeExcepcion.equals("DTMF")){
							numExcepTonosActual = numExcepTonosActual + 1;
							numExcepActual = numExcepTonosActual;
						}
						modeExcepcion = modeExcepcion.toLowerCase();
						%>
						<log label="MENU-PLANTILLA"><value expr="'modeExcepcion: <%=modeExcepcion%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'typeExcepcion: <%=typeExcepcion%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'menu_Tonos: ' + menu_Tonos"/></log>

						<log label="MENU-PLANTILLA"><value expr="'numExcepTonosActual: <%=numExcepTonosActual%>'"/></log>
						<log label="MENU-PLANTILLA"><value expr="'numExcepActual: <%=numExcepActual%>'"/></log>
						<%
						String functionExcepcion = menu.getExcepciones()[i].getFunction();
						String scriptExcepcion = menu.getExcepciones()[i].getScript();
						%>
						<script src="<%=scriptExcepcion%>"/>

						<if cond="lastresult$.inputmode == '<%=modeExcepcion%>'">	
							<!-- el modo de la excepcion coincide con el de la interaccion del usuario -->
							
							<!-- asigno el tipoReconocimientoEmpleado por si lanzo el NM excepcion -->
							<if cond="lastresult$.inputmode == 'dtmf'">								
								<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
							<else/>
								<assign name="tipoReconocimientoEmpleado" expr="'ASR'"/>
							</if>
							
							<var name="resultadoExcepcion" expr="''"/>
	
<%-- 							<log label="MENU-PLANTILLA"><value expr="'<%=functionExcepcion%>(' + menu_Tonos + ')'"/></log> --%>
<%-- 							<assign name="resultadoExcepcion" expr="<%=functionExcepcion%>(menu_Tonos)"/> --%>

							<!-- ABALFARO_20170615 -->
							<log label="MENU-PLANTILLA"><value expr="'<%=functionExcepcion%>(' + menu_Tonos + ', <%=typeExcepcion%>, ' + PRM_paramAdicional + ')'"/></log>
							<assign name="resultadoExcepcion" expr="<%=functionExcepcion%>(menu_Tonos, '<%=typeExcepcion%>', PRM_paramAdicional)"/>
													
							<log label="MENU-PLANTILLA"><value expr="'resultadoExcepcion: ' + resultadoExcepcion"/></log>
							
							
							<if cond="resultadoExcepcion == 'SI' || resultadoExcepcion == 'si'">	
							
								<!-- guardo el id de la excepcion que se ha activado -->
								<log label="MENU-PLANTILLA"><value expr="'numExcepcionLanzada: <%=numExcepActual%>'"/></log>
								<assign name="numExcepcionLanzada" expr="'<%=numExcepActual%>'"/>
								
								<!-- guardo la respuesta que ha dado el cliente -->
								<assign name="respuestaNMExcepcion" expr="menu_Tonos"/>
								<log label="MENU-PLANTILLA"><value expr="'respuestaNMExcepcion: ' + respuestaNMExcepcion"/></log>
								
								<!-- lanzo el evento de la excepcion -->
								<throw event="nomatch_excepcion"/>
							<else/>
								<!-- en otro caso no lanzo la excepcion y continua la ejecucion del filled -->
							</if>
													
						</if>
						<%
					}
				}
			
			%>
	
	
	
	
	
	
	
	
				<assign name="tipoReconocimientoEmpleado" expr="'DTMF'"/>
			
			<!-- 07062016 comprobar si la gramatica es builtin, la respuesta conincide con los tonos activados --> 
				<assign name="encontradoTono" expr="'NO'"/>
				
				<!-- tengo que comprobar que la longitud del array no sea cero porque si no falla el foreach -->
				<log label="MENU-PLANTILLA"><value expr="'tonosActivosBuiltin.length: ' + tonosActivosBuiltin.length"/></log>
				<if cond="tonosActivosBuiltin.length != 0">
					<foreach item="tonoActivo" array="tonosActivosBuiltin">
						<if cond="tonoActivo.tono == menu_Tonos">
							<assign name="encontradoTono" expr="'SI'"/>	
							
							<!-- cojo como respuesta de usuario la accion de la opcion -->
							<assign name="PRM_OUT_MENU_respuestaUsuario" expr="tonoActivo.accion"/>
							<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_Tonos$.utterance"/>
							
						<else/>
							<script>
								var esLongitud = 'false';
								var tonoAct = tonoActivo.tono;
								if(tonoAct.substring(0,1) == 'L'){
									esLongitud = 'true';
								}
							</script>
							<if cond="esLongitud == 'true'">
								<!-- hay una longitud de tonos activa -->
								<log label="MENU-PLANTILLA"><value expr="'paso a comprobar por longitud'"/></log>
								<log label="MENU-PLANTILLA"><value expr="'menu_Tonos: ' + menu_Tonos"/></log>   
								
																
								<script><![CDATA[
									tonoAct = tonoActivo.tono;
									if(tonoAct.substring(0,1) == 'L'){
										
										// ABALFARO_20170331
										if((tonoAct.indexOf('Lmin') != -1) && (tonoAct.indexOf('Lmax') != -1)){
											// el tono contiene un maximo y un minimo
											var longitudMin = tonoAct.substring(4,tonoAct.indexOf('Lmax'));
											var longitudMax = tonoAct.substring(tonoAct.indexOf('Lmax') + 4, tonoAct.length);
											if(menu_Tonos.length >= longitudMin && menu_Tonos.length <= longitudMax){
												// el dato es correcto porque su longitud esta entre el minimo y maximo de la del builtin
												encontradoTono = 'SI';
											}
										} else if(tonoAct.indexOf('Lmin') != -1){
											// el tono contiene un minimo
											var longitud = tonoAct.substring(4, tonoAct.length);
											if(menu_Tonos.length >= longitud){
												// el dato es correcto porque su longitud es mayor o igual que la del builtin
												encontradoTono = 'SI';
											}
										} else if(tonoAct.indexOf('Lmax') != -1){
											// el tono contiene un maximo
											var longitud = tonoAct.substring(4, tonoAct.length);
											if(menu_Tonos.length <= longitud){
												// el dato es correcto porque su longitud es menor o igual que la del builtin
												encontradoTono = 'SI';
											}
										
										} else {
											// el tono es una longitud concreta
											var longitud = tonoAct.substring(1, tonoAct.length);
											if(menu_Tonos.length == longitud){
												// el dato es correcto porque su longitud coincide con la del builtin
												encontradoTono = 'SI';
											}
										}
										
										
									}
								]]></script>
								
								
								
								
								
								<log label="MENU-PLANTILLA"><value expr="'tonoAct: ' + tonoAct"/></log>
								<log label="MENU-PLANTILLA"><value expr="'longitud: ' + longitud"/></log>
								<if cond="encontradoTono == 'SI'">
									<!-- cojo como respuesta de usuario lo mismo que ha insertado -->
									<assign name="PRM_OUT_MENU_respuestaUsuario" expr="menu_Tonos"/>
									<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_Tonos$.utterance"/>
								</if>
							</if>
						</if>
					</foreach>
					<log label="MENU-PLANTILLA"><value expr="'encontradoTono: ' + encontradoTono"/></log>
					<if cond="encontradoTono != 'SI'">
						<!-- NO han respondido un tono activo, lanzo nomatch -->
						<log label="MENU-PLANTILLA"><value expr="'Lanzo NOMATCH - no coincide con los tonos activos'"/></log>
						
<!-- 						<clear namelist="menu_Tonos" />  -->
						
						<throw event="nomatch"/>
											
					<else/>
					
						<!-- SI han respondido un tono activo -->
						
						<log label="MENU-PLANTILLA"><value expr="'RESP ES UN TONO ACTIVO'"/></log> 							
							
							<!-- TRAZAS DEL INTENTO -->
							<script><![CDATA[
							
								var intentoMenu = new IntentoMenu();
									intentoMenu.elementos = new Array();
									
									for(var i=0; i<PRM_promptsIntentosTonos[numIntentosTotales].length; i++){
										var locucionMenu = new LocucionMenu();	
											locucionMenu.tipoLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].tipoLocucion;
											locucionMenu.idLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].idLocucion;
										intentoMenu.elementos[i] = locucionMenu;
									}
										
									var numElems = intentoMenu.elementos.length;
								
									var dialogoMenu = new DialogoMenu();
										dialogoMenu.idMenu = PRM_idMenu;
										dialogoMenu.recDisponible = "DTMF";
										dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
										dialogoMenu.numIntento = numIntentosTotales + 1;
										dialogoMenu.respuesta = PRM_OUT_MENU_respuestaUsuario;
										dialogoMenu.respuestaRaw = PRM_OUT_MENU_respuestaUsuarioRaw;
									intentoMenu.elementos[numElems] = dialogoMenu;	
										
								var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
								PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
							]]></script>
							
							<!-- IMPRIMO TRAZAS DEL INTENTO -->
							<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
							
								<script>
									var tipoElemento;
									if(elemento instanceof LocucionMenu){
										tipoElemento = "LocucionMenu";
									} 
									if(elemento instanceof DialogoMenu){
										tipoElemento = "DialogoMenu";
									}
								</script>
								<if cond="tipoElemento == 'LocucionMenu'">
									<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
									<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
								</if>
								<if cond="tipoElemento == 'DialogoMenu'">
									<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
								</if>
							</foreach>					
							
						<assign name="numIntentosTotales" expr="numIntentosTotales + 1"/>	
									
						<assign name="PRM_OUT_MENU_codigoRetorno" expr="'OK'"/>
						<assign name="PRM_OUT_MENU_error" expr="''"/>	
						<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
						<assign name="PRM_OUT_MENU_modoInteraccion" expr="menu_Tonos$.inputmode"/>
						<assign name="PRM_OUT_MENU_nivelConfianza" expr="menu_Tonos$.confidence"/>
						
						
						<goto next="#ESCRIBE_RESULTADO_MENU"/>
						
					</if>
				<else/>
				<!-- fin - 07062016 -->
					
					<!-- no tengo que comprobar tonos activos -->
					
					<log label="MENU-PLANTILLA"><value expr="'CODIGO FINAL'"/></log> 
							
							<!-- TRAZAS DEL INTENTO -->
							<script><![CDATA[
							
								var intentoMenu = new IntentoMenu();
									intentoMenu.elementos = new Array();
									
									for(var i=0; i<PRM_promptsIntentosTonos[numIntentosTotales].length; i++){
										var locucionMenu = new LocucionMenu();	
											locucionMenu.tipoLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].tipoLocucion;
											locucionMenu.idLocucion = PRM_promptsIntentosTonos[numIntentosTotales][i].idLocucion;
										intentoMenu.elementos[i] = locucionMenu;
									}
										
									var numElems = intentoMenu.elementos.length;
								
									var dialogoMenu = new DialogoMenu();
										dialogoMenu.idMenu = PRM_idMenu;
										dialogoMenu.recDisponible = "DTMF";
										dialogoMenu.recUtilizado = tipoReconocimientoEmpleado;
										dialogoMenu.numIntento = numIntentosTotales + 1;
										dialogoMenu.respuesta = menu_Tonos;
										dialogoMenu.respuestaRaw = menu_Tonos$.utterance;
									intentoMenu.elementos[numElems] = dialogoMenu;	
										
								var numIntentosTrazas = PRM_OUT_MENU_trazas.intentos.length;
								PRM_OUT_MENU_trazas.intentos[numIntentosTrazas] = intentoMenu;
							]]></script>
							
							<!-- IMPRIMO TRAZAS DEL INTENTO -->
							<foreach item="elemento" array="PRM_OUT_MENU_trazas.intentos[numIntentosTrazas].elementos">					
							
								<script>
									var tipoElemento;
									if(elemento instanceof LocucionMenu){
										tipoElemento = "LocucionMenu";
									} 
									if(elemento instanceof DialogoMenu){
										tipoElemento = "DialogoMenu";
									}
								</script>
								<if cond="tipoElemento == 'LocucionMenu'">
									<log label="MENU-PLANTILLA"><value expr="'locucion.tipoLocucion: ' + elemento.tipoLocucion"/></log>
									<log label="MENU-PLANTILLA"><value expr="'locucion.idLocucion: ' + elemento.idLocucion"/></log>
								</if>
								<if cond="tipoElemento == 'DialogoMenu'">
									<log label="MENU-PLANTILLA"><value expr="'dialogo.idMenu: ' + elemento.idMenu"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.recDisponible: ' + elemento.recDisponible"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.recUtilizado: ' + elemento.recUtilizado"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.numIntento: ' + elemento.numIntento"/></log>
									<log label="MENU-PLANTILLA"><value expr="'dialogo.respuesta: ' + elemento.respuesta"/></log>
								</if>
							</foreach>
						
					<assign name="numIntentosTotales" expr="numIntentosTotales + 1"/>	
								
					<assign name="PRM_OUT_MENU_respuestaUsuario" expr="menu_Tonos"/>
					<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="menu_Tonos$.utterance"/>
					<assign name="PRM_OUT_MENU_codigoRetorno" expr="'OK'"/>
					<assign name="PRM_OUT_MENU_error" expr="''"/>	
					<assign name="PRM_OUT_MENU_intento" expr="numIntentosTotales"/>
					<assign name="PRM_OUT_MENU_modoInteraccion" expr="menu_Tonos$.inputmode"/>
					<assign name="PRM_OUT_MENU_nivelConfianza" expr="menu_Tonos$.confidence"/>
					
					
					
					<goto next="#ESCRIBE_RESULTADO_MENU"/>					
			
				</if>
			
		</filled>
		
	</field>

<%
} // fin continuar
%>	
</form>	

<%
if(!continuar){
	// ha terminado toda la ejecucion Java con error	

	/** INICIO EVENTO - FIN DE PROCESO **/
	ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
	parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
	parametrosSalida.add(new ParamEvent("respuestaUsuario", ""));
	parametrosSalida.add(new ParamEvent("respuestaUsuarioRaw", ""));
	parametrosSalida.add(new ParamEvent("error", error));
	parametrosSalida.add(new ParamEvent("modoInteraccion", ""));
	parametrosSalida.add(new ParamEvent("nivelConfianza", ""));
	menuConfigurableXML.getLog().endProcess(idModuloLog, "KO", parametrosSalida, null);
	/** FIN EVENTO - FIN DE PROCESO **/
} else {
	// ha terminado toda la ejecucion Java OK
	
	/** INICIO EVENTO - FIN DE PROCESO **/
	ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
	parametrosSalida.add(new ParamEvent("codigoRetorno", "OK"));
	parametrosSalida.add(new ParamEvent("respuestaUsuario", ""));
	parametrosSalida.add(new ParamEvent("respuestaUsuarioRaw", ""));
	parametrosSalida.add(new ParamEvent("error", ""));
	parametrosSalida.add(new ParamEvent("modoInteraccion", ""));
	parametrosSalida.add(new ParamEvent("nivelConfianza", ""));
	menuConfigurableXML.getLog().endProcess(idModuloLog, "OK", parametrosSalida, null);
	/** FIN EVENTO - FIN DE PROCESO **/
}



%>


<!-- 
***********************************************
******* ESCRIBE TRAZAS DEL MENU ***************
***********************************************
-->
<form id="ESCRIBE_RESULTADO_MENU">

	<block>
		<log label="MENU-PLANTILLA"><value expr="'VOY A ESCRIBIR EL RESULTADO'"/></log>
		
		<!-- ABALFARO_20170628 -->
		<assign name="PRM_saltoEscribeResultadoMenu" expr="'SI'"/>
				
	</block>
	
	<subdialog name="escribeResultadoMenu" fetchhint="safe" method="post" 
		src="../plantilla/ESCRIBE-RESULTADO-MENU.jsp"
		namelist="PRM_OUT_MENU_trazas PRM_idInvocacion PRM_idServicio PRM_idElemento PRM_respuestaSensible PRM_idMenu 
					PRM_OUT_MENU_codigoRetorno PRM_OUT_MENU_error PRM_paramAdicional">

		<filled>
			
			<log label="MENU-PLANTILLA"><value expr="'VUELVO A LA PLANTILLA'"/></log>
			
						
			<if cond="escribeResultadoMenu.SUB_resultadoOperacion == 'HANGUP' || escribeResultadoMenu.SUB_resultadoOperacion == 'ERROR' ">
				<!-- al escribir el resultado se ha producido un error o un hangup -->
				<!-- devuelvo este resultado -->
				<assign name="PRM_OUT_MENU_codigoRetorno" expr="escribeResultadoMenu.SUB_resultadoOperacion"/>
				<assign name="PRM_OUT_MENU_error" expr="escribeResultadoMenu.SUB_error"/>
			</if>
						
			<!-- contingencia -->
			<if cond="PRM_OUT_MENU_codigoRetorno == ''">
				<assign name="PRM_OUT_MENU_codigoRetorno" expr="'ERROR'"/>
				
				<if cond="PRM_OUT_MENU_error == ''">
					<assign name="PRM_OUT_MENU_error" expr="'ERROR_EXT(MENU)'"/>
				</if>
			</if>
			
			<goto next="#RETORNO" />
									
		</filled>	
		
	</subdialog>	

</form>

<!-- 
**************************************
************** RETORNO  **************
**************************************
-->
<form id="RETORNO">	
	
	<block>

	
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_codigoRetorno: ' + PRM_OUT_MENU_codigoRetorno"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_error: ' + PRM_OUT_MENU_error"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_respuestaUsuario: ' + PRM_OUT_MENU_respuestaUsuario"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_respuestaUsuarioRaw: ' + PRM_OUT_MENU_respuestaUsuarioRaw"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_intento: ' + PRM_OUT_MENU_intento"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_modoInteraccion: ' + PRM_OUT_MENU_modoInteraccion"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_nivelConfianza: ' + PRM_OUT_MENU_nivelConfianza"/></log>
		
		
		<log label="MENU-PLANTILLA"><value expr="'-- FIN PLANTILLA --'"/></log>
		
	
		<return namelist="PRM_OUT_MENU_trazas PRM_OUT_MENU_codigoRetorno PRM_OUT_MENU_error PRM_OUT_MENU_respuestaUsuario PRM_OUT_MENU_respuestaUsuarioRaw 
						PRM_OUT_MENU_intento PRM_OUT_MENU_modoInteraccion PRM_OUT_MENU_nivelConfianza"/>
		
	</block>
</form>

<!-- 
**************************************
************** RETORNO  **************
**************************************
-->
<form id="RETORNO_ERROR_JAVA">	
	
	<block>
	
		<assign name="PRM_OUT_MENU_respuestaUsuario" expr="''"/>
		<assign name="PRM_OUT_MENU_respuestaUsuarioRaw" expr="''"/>
		<assign name="PRM_OUT_MENU_codigoRetorno" expr="'<%=codigoRetorno%>'"/>
		<assign name="PRM_OUT_MENU_error" expr="'<%=error%>'"/>
		<assign name="PRM_OUT_MENU_intento" expr="''"/>
		<assign name="PRM_OUT_MENU_modoInteraccion" expr="''"/>
		<assign name="PRM_OUT_MENU_nivelConfianza" expr="''"/>

	
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_codigoRetorno: ' + PRM_OUT_MENU_codigoRetorno"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_error: ' + PRM_OUT_MENU_error"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_respuestaUsuario: ' + PRM_OUT_MENU_respuestaUsuario"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_respuestaUsuarioRaw: ' + PRM_OUT_MENU_respuestaUsuarioRaw"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_intento: ' + PRM_OUT_MENU_intento"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_modoInteraccion: ' + PRM_OUT_MENU_modoInteraccion"/></log>
		<log label="MENU-PLANTILLA"><value expr="'PRM_OUT_MENU_nivelConfianza: ' + PRM_OUT_MENU_nivelConfianza"/></log>
		
		
		<log label="MENU-PLANTILLA"><value expr="'-- FIN PLANTILLA ERROR --'"/></log>
			
		<return namelist="PRM_OUT_MENU_trazas PRM_OUT_MENU_codigoRetorno PRM_OUT_MENU_error PRM_OUT_MENU_respuestaUsuario PRM_OUT_MENU_respuestaUsuarioRaw 
						  PRM_OUT_MENU_intento PRM_OUT_MENU_modoInteraccion PRM_OUT_MENU_nivelConfianza"/>
		
	</block>
</form>

</vxml>