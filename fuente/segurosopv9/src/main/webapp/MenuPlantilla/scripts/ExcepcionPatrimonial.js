// ************************************************* //
// ****** FUNCIONES para TODOS LOS MENÚS *********** //
// ************************************************* //

 
function lanzaNoMatchVoz(respuesta, segmentoValido, segmentoCliente) {
	
	var lanza = "NO";
  
    var resp = respuesta.toString();
    var segmValido = segmentoValido.toString();
    var segmCliente = segmentoCliente.toString();
    
    if(resp != 'ASESOR'){
    	// la respuesta no es ir a asesor
    	lanza = "NO";
    
    } else{
    	// paramAdicional 
    	if(segmValido == segmCliente){
    		// la respuesta de ir a asesor es valida para el segmento actual del cliente
        	lanza = "NO";
    	} else {
    		// NO es valida para el segmento actual del cliente
    		lanza = "SI";
    	}
     }
	
	return lanza;
}
function lanzaNoMatchTonos(respuesta, segmentoValido, segmentoCliente) {
	
	var lanza = "NO";
  
    var resp = respuesta.toString();
    var segmValido = segmentoValido.toString();
    var segmCliente = segmentoCliente.toString();
    
    if(resp != "0"){
    	// la respuesta no es ir a asesor
    	lanza = "NO";
    
    } else{
    	// paramAdicional 
    	if(segmValido == segmCliente){
    		// la respuesta de ir a asesor es valida para el segmento actual del cliente
        	lanza = "NO";
    	} else {
    		// NO es valida para el segmento actual del cliente
    		lanza = "SI";
    	}
     }
	
	return lanza;
}