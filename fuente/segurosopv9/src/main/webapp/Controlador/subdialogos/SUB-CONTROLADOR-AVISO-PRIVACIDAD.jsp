<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-CONTROLADOR-AVISO-PRIVACIDAD
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="VG_menu" expr="''"/>
<var name="VG_loggerServicio" expr="''"/>
<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
<var name="VG_controlador" expr="''"/>
<var name="SUB_activoMenu"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<var name="PRM_OUT_RESPUESTA_USUARIO" expr="''"/>
<var name="PRM_OUT_CODIGO_RETORNO" expr="''"/>
<var name="PRM_OUT_ERROR" expr="''"/>
<var name="PRM_OUT_INTENTO" expr="''"/>
<var name="PRM_OUT_MODO_INTERACCION" expr="''"/>
<var name="PRM_OUT_MODO_NIVEL_CONFIANZA" expr="''"/>


<!-- 
******************************************
********** CAPTURA DE CUELGUE ************
******************************************
-->
	<catch event="connection.disconnect.hangup">
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-AVISO-PRIVACIDAD'"/></log>
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</catch>
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-AVISO-PRIVACIDAD'"/></log>
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
<form id="SUB_CONTROLADOR_AVISO_PRIVACIDAD">
	<var name="PRM_IN_loggerServicio"/>
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
	<var name="PRM_IN_controlador"/>
		
		<block>
			<log label="CONTROLADOR"><value expr="'INICIO - SUB-CONTROLADOR-AVISO-PRIVACIDAD'"/></log>
			<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
			<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
			<assign name="VG_controlador" expr="PRM_IN_controlador"/>
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_activoMenu" expr="'${activo}'"/>
			<goto next="#ANALIZA_RESULTADO"/>
		</block>
	
</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
<form id="ANALIZA_RESULTADO">
	<block>
		<log label="CONTROLADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
		<log label="CONTROLADOR"><value expr="'error: ' + SUB_error"/></log>
		<log label="CONTROLADOR"><value expr="'activo menu privacidad: ' + SUB_activoMenu"/></log>
					
		<if cond="SUB_resultadoOperacion == 'OK'">
			<if cond="SUB_activoMenu == 'SI'">
					<goto next="#LOC_AVISO_PRIVACIDAD"/>
				<else/>
					<goto next="#FIN"/>
				</if>
		</if>
		<goto next="#FIN"/>	
	</block>
</form>

<!-- 
******************************************
***** FORM LOC_AVISO_PRIVACIDAD **********
******************************************
-->
<form id="LOC_AVISO_PRIVACIDAD">
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar el aviso de privacidad'"/></log>
		
		<!-- ABALFARO_20170717 cambio para dar locucion directamente en vez de menu -->
		<%-- <c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-AVISO-PRIVACIDAD"/> --%>
		<c:set var="locucionesInformacion" scope="request" value="COM-${idServicio}-INFO-INICIO-AVISO-PRIVACIDAD"/>
		
		<log label="CONTROLADOR"><value expr="'locucionesInformacion: ${locucionesInformacion}'"/></log>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>
		<assign name="PRM_IN_LOC_tipoLocucion" expr="'COMUN'"/>
		<assign name="PRM_IN_LOC_locuciones" expr="'${locucionesInformacion}'"/>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
		
		<filled>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
				<assign name="SUB_error" expr="''"/>
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#FIN"/>
									
		</filled>
	</subdialog>
</form>

<form id="FIN">
	<block>
		<log label="CONTROLADOR"><value expr="'FIN - SUB-CONTROLADOR-AVISO-PRIVACIDAD'"/></log>
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</block>
</form>

</vxml>

