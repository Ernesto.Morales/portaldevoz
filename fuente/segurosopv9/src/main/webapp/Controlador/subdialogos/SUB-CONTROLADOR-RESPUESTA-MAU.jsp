<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:  SUB_CONTROLADOR_RESPUESTA_MAU
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="SUB_tipoShortcut"/>
<var name="SUB_estaIdentificado"/>

<var name="SUB_esMAU"/>

<var name="SUB_DatosShortCut"/>
<var name="SUB_locucionSaludo"/>
<var name="SUB_modoSaludo"/>

<var name="VG_loggerServicio" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<!-- <var name="locucionesInformacion"/> -->
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-BIENVENIDA'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-BIENVENIDA'"/></log>
		
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_CONTROLADOR_RESPUESTA_MAU">
	
		<var name="PRM_IN_loggerServicio"/>
		
		<block>				
			<log label="CONTROLADOR"><value expr="'INICIO - SUB-CONTROLADOR-BIENVENIDA'"/></log>
			
			<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
			
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_tipoShortcut" expr="'${tipoShortcut}'"/>
		
			<assign name="SUB_estaIdentificado" expr="'${estaIdentificado}'"/>
			<assign name="SUB_esMAU" expr="'${esMAU}'"/>
			<assign name="SUB_DatosShortCut" expr="'${DatosShortCut}'"/>
			
			<assign name="SUB_locucionSaludo" expr="'${locucionSaludo}'"/>
			<assign name="SUB_modoSaludo" expr="'${modoSaludo}'"/>
			
			<log label="CONTROLADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'error: ' + SUB_error"/></log>
						
			<if cond="SUB_resultadoOperacion == 'OK'">
				
				<log label="CONTROLADOR"><value expr="'locucionSaludo: ' + SUB_locucionSaludo"/></log>
				<log label="CONTROLADOR"><value expr="'modoSaludo: ' + SUB_modoSaludo"/></log>
				
				<goto next="#DAR_RESPUESTA"/>
		
					
			</if>

			<goto next="#RETORNAR"/>
		
		</block>
	
	</form>
	
	
<!-- 
******************************************
******* FORM DAR RESPUESTA **************
******************************************
-->
	<form id="DAR_RESPUESTA">	
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar la bienvenida'"/></log>
		
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>	
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>	
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>	
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>	
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	

		<assign name="PRM_IN_LOC_tipoLocucion" expr="SUB_modoSaludo"/>	
		
		<!-- mando la locucion a vacio si es TTS, si es wav la relleno con el nombre del audio -->
		<assign name="PRM_IN_LOC_locuciones" expr="''"/>
		<if cond="SUB_modoSaludo == 'WAV'">
			<assign name="PRM_IN_LOC_locuciones" expr="SUB_locucionSaludo"/>
		</if>

		
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>

		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
			
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
				
		<param name="PRM_IN_LOC_textoTTS" expr="SUB_locucionSaludo"/>
		
		<filled>

			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>	
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
					<!-- actualizo la salida solo si es hangup -->
					
					<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
					<assign name="SUB_error" expr="''"/>	
					
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#RETORNAR"/>
									
		</filled>	
		
	</subdialog>
</form>	
	
	
<!-- 
******************************************
*********** FORM RETORNAR ****************
******************************************
-->	
	<form id="RETORNAR">
	
		<block>	
					
			<log label="CONTROLADOR"><value expr="'FIN - SUB_CONTROLADOR_RESPUESTA_MAU'"/></log>		
			
			<return namelist="SUB_resultadoOperacion SUB_error SUB_tipoShortcut SUB_DatosShortCut SUB_estaIdentificado SUB_esMAU"/>
			
		</block>
	</form>


</vxml>

