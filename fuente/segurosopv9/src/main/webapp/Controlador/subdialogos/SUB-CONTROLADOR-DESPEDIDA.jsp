<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-CONTROLADOR-DESPEDIDA
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="SUB_locucionDespedida"/>
<var name="SUB_modoDespedida"/>
<var name="VG_loggerServicio" expr="''"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE ************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-DESPEDIDA'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-DESPEDIDA'"/></log>
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_CONTROLADOR_DESPEDIDA">
		<var name="PRM_IN_loggerServicio"/>
		<block>
			<log label="CONTROLADOR"><value expr="'INICIO - SUB-CONTROLADOR-DESPEDIDA'"/></log>
			<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_locucionDespedida" expr="'${locucionDespedida}'"/>
			<assign name="SUB_modoDespedida" expr="'${modoDespedida}'"/>
			<log label="CONTROLADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'error: ' + SUB_error"/></log>
						
			<if cond="SUB_resultadoOperacion == 'OK'">
				<log label="CONTROLADOR"><value expr="'locucionDespedida: ' + SUB_locucionDespedida"/></log>
				<log label="CONTROLADOR"><value expr="'modoDespedida: ' + SUB_modoDespedida"/></log>
				<goto next="#DAR_DESPEDIDA"/>
			</if>
			
			<goto next="#RETORNAR"/>
		
		</block>
	</form>

<!-- 
******************************************
******** FORM DAR DESPEDIDA **************
******************************************
-->
<form id="DAR_DESPEDIDA">
	<block>
		<log label="CONTROLADOR"><value expr="'Voy a dar la despedida'"/></log>
		<assign name="PRM_IN_LOC_idioma" expr="VG_loggerServicio.idioma"/>
		<assign name="PRM_IN_LOC_idLlamada" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_LOC_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_LOC_idElemento" expr="VG_loggerServicio.idElemento"/>
		<assign name="PRM_IN_LOC_idModulo" expr="''"/>	
		<assign name="PRM_IN_LOC_tipoLocucion" expr="SUB_modoDespedida"/>
		<!-- mando la locucion a vacio si es TTS, si es wav la relleno con el nombre del audio -->
		<assign name="PRM_IN_LOC_locuciones" expr="''"/>
		
		<if cond="SUB_modoDespedida == 'WAV'">
			<assign name="PRM_IN_LOC_locuciones" expr="SUB_locucionDespedida"/>
		</if>

		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idioma: ' + PRM_IN_LOC_idioma"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idLlamada: ' + PRM_IN_LOC_idLlamada"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idServicio: ' + PRM_IN_LOC_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idElemento: ' + PRM_IN_LOC_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_idModulo: ' + PRM_IN_LOC_idModulo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_locuciones: ' + PRM_IN_LOC_locuciones"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_LOC_tipoLocucion: ' + PRM_IN_LOC_tipoLocucion"/></log>
		<log label="CONTROLADOR"><value expr="'SALTO A PLANTILLA'"/></log>
	</block>	
	<subdialog name="LocPlantilla" fetchhint="safe" 
		src="${pageContext.request.contextPath}/LocucionPlantilla/plantilla/LOCUCION-PLANTILLA.jsp"
		namelist="PRM_IN_LOC_idioma PRM_IN_LOC_idLlamada PRM_IN_LOC_idServicio PRM_IN_LOC_idElemento
				PRM_IN_LOC_idModulo PRM_IN_LOC_locuciones PRM_IN_LOC_tipoLocucion">
				
		<param name="PRM_IN_LOC_textoTTS" expr="SUB_locucionDespedida"/>
		
		<filled>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_resultadoOperacion: ' + LocPlantilla.PRM_OUT_LOC_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'LocPlantilla.PRM_OUT_LOC_error: ' + LocPlantilla.PRM_OUT_LOC_error"/></log>
			
			<if cond="LocPlantilla.PRM_OUT_LOC_resultadoOperacion == 'HANGUP'">
				<!-- actualizo la salida solo si es hangup -->
				<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
				<assign name="SUB_error" expr="''"/>	
			</if>
			
			<!-- **** FIN PLANTILLA-LOCUCION -->
			<goto next="#RETORNAR"/>				
		</filled>
		
	</subdialog>
</form>
	
	
<!-- 
******************************************
*********** FORM RETORNAR ****************
******************************************
-->	
<form id="RETORNAR">
	<block>
	<log label="CONTROLADOR"><value expr="'FIN - SUB-CONTROLADOR-BIENVENIDA'"/></log>
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</block>
</form>

</vxml>

