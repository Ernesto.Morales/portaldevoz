<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-CONTROLADOR-RECUPERA-CONT
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/BeanControlador.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_codigoRetorno"/>
<var name="SUB_error"/>
<var name="SUB_controlador"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-RECUPERA-CONT'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_codigoRetorno" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		<return namelist="SUB_resultadoOperacion SUB_codigoRetorno SUB_error SUB_controlador"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-RECUPERA-CONT'"/></log>
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_codigoRetorno" expr="'ERROR'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>	
		<return namelist="SUB_resultadoOperacion SUB_codigoRetorno SUB_error SUB_controlador"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_CONTROLADOR_RECUPERA_CONT">
		<block>				
			<log label="CONTROLADOR"><value expr="'INICIO - SUB-CONTROLADOR-RECUPERA-CONT'"/></log>
		
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_codigoRetorno" expr="'${codigoRetorno}'"/>
			<assign name="SUB_error" expr="'${error}'"/>	
			<goto next="#RECUPERA_CONT"/>	
		</block>
	
	</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
	<form id="RECUPERA_CONT">
		<block>				
			<log label="CONTROLADOR"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_codigoRetorno: ' + SUB_codigoRetorno"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_error: ' + SUB_error"/></log>
						
			<if cond="SUB_resultadoOperacion == 'OK'">
				<script>
					SUB_controlador = new BeanControlador();	
				</script>
				
				<assign name="SUB_controlador.codServIVR" expr="'${controlador.codServivr}'"/>
				<assign name="SUB_controlador.nombre" expr="'${controlador.nombreControlador}'"/>
				<assign name="SUB_controlador.isReqIdentificacion" expr="'${controlador.identificacion.activo}'"/>
				<assign name="SUB_controlador.tipoIdentificacion" expr="'${controlador.getIdentificacion().getInstrumentosActivos()}'"/>
				<assign name="SUB_controlador.intentosIdentificacion" expr="'${controlador.getIdentificacion().getNumIntentosInstrumentosActivos()}'"/>
           		<assign name="SUB_controlador.accionOkIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('OK')}'"/>
				<assign name="SUB_controlador.isReqAutenticacion" expr="'${controlador.autenticacion.activo}'"/>
				<assign name="SUB_controlador.tipoAutenticacion" expr="'${controlador.getAutenticacion().getInstrumentosActivos()}'"/>
				<assign name="SUB_controlador.intentosAutenticacion" expr="'${controlador.getAutenticacion().getNumIntentosInstrumentosActivos()}'"/>
				<assign name="SUB_controlador.accionClienteSinElementosAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_SIN_ELEMENTOS')}'"/>
	            <assign name="SUB_controlador.accionClienteBloqueadoCandadoAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_BLOQUEADO_CANDADO')}'"/>
	            <assign name="SUB_controlador.accionMaxintMenuAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('MAXINT_MENU')}'"/>
	            <assign name="SUB_controlador.accionMaxintValidAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('MAXINT_VALID')}'"/>
	            <assign name="SUB_controlador.accionClienteBloqueadoValidAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_BLOQUEADO_VALID')}'"/>
	 			
				<log label="CONTROLADOR"><value expr="'SUB_controlador.codServIVR: ' + SUB_controlador.codServIVR"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.nombre: ' + SUB_controlador.nombre"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.isReqIdentificacion: ' + SUB_controlador.isReqIdentificacion"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.tipoIdentificacion: ' + SUB_controlador.tipoIdentificacion"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.intentosIdentificacion: ' + SUB_controlador.intentosIdentificacion"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionOkIdentificacion: ' + SUB_controlador.accionOkIdentificacion"/></log>			
				<log label="CONTROLADOR"><value expr="'SUB_controlador.isReqAutenticacion: ' + SUB_controlador.isReqAutenticacion"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.tipoAutenticacion: ' + SUB_controlador.tipoAutenticacion"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.intentosAutenticacion: ' + SUB_controlador.intentosAutenticacion"/></log>			
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionClienteSinElementosAuth: ' + SUB_controlador.accionClienteSinElementosAuth"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionClienteBloqueadoCandadoAuth: ' + SUB_controlador.accionClienteBloqueadoCandadoAuth"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionMaxintMenuAuth: ' + SUB_controlador.accionMaxintMenuAuth"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionMaxintValidAuth: ' + SUB_controlador.accionMaxintValidAuth"/></log>
				<log label="CONTROLADOR"><value expr="'SUB_controlador.accionClienteBloqueadoValidAuth: ' + SUB_controlador.accionClienteBloqueadoValidAuth"/></log>
				<goto next="#RETORNAR"/>
				
			<else/>
				<goto next="#RETORNAR"/>	
				
			</if>
		</block>
	</form>
	
	
	<form id="RETORNAR">
		<block>		
			<log label="CONTROLADOR"><value expr="'FIN - SUB-CONTROLADOR-RECUPERA-CONT'"/></log>		
			<return namelist="SUB_resultadoOperacion SUB_codigoRetorno SUB_error SUB_controlador"/>
		</block>
	</form>


</vxml>

