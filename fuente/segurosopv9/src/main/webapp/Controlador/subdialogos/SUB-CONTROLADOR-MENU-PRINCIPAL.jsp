<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-CONTROLADOR-MENU-PRINCIPAL
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>

<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="VG_menu" expr="''"/>
<var name="VG_loggerServicio" expr="''"/>
<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
<var name="VG_controlador" expr="''"/>
<var name="SUB_activoMenu"/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>

<!--
************************************
** DEFINICION DE VARIABLES DE MENU *
************************************
-->
<var name="PRM_IN_MENU_pathXML" expr="''"/>
<var name="PRM_IN_MENU_idMenu" expr="''"/>
<var name="PRM_IN_MENU_variable" expr="''"/>
<var name="PRM_IN_MENU_idInvocacion" expr="''"/>
<var name="PRM_IN_MENU_idServicio" expr="''"/>
<var name="PRM_IN_MENU_idElemento" expr="''"/>
<var name="PRM_IN_MENU_paramAdicional" expr="''"/>
<var name="PRM_OUT_RESPUESTA_USUARIO" expr="''"/>
<var name="PRM_OUT_CODIGO_RETORNO" expr="''"/>
<var name="PRM_OUT_ERROR" expr="''"/>
<var name="PRM_OUT_INTENTO" expr="''"/>
<var name="PRM_OUT_MODO_INTERACCION" expr="''"/>
<var name="PRM_OUT_MODO_NIVEL_CONFIANZA" expr="''"/>

<script>
	VG_menu = new Menu();
</script>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-MENU-PRINCIPAL'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>	
		<return namelist="SUB_resultadoOperacion SUB_error"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-MENU-PRINCIPAL'"/></log>
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>	
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
<form id="SUB_CONTROLADOR_MENU_PRINCIPAL">
	
	<var name="PRM_IN_loggerServicio"/>
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
	<var name="PRM_IN_controlador"/>
		
		<block>
			<log label="CONTROLADOR"><value expr="'INICIO - SUB-CONTROLADOR-MENU-PRINCIPAL'"/></log>
			
			<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
			<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al menu -->
			<assign name="VG_controlador" expr="PRM_IN_controlador"/>
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_activoMenu" expr="'${activo}'"/>
			<goto next="#ANALIZA_RESULTADO"/>	
		
		</block>
	
	</form>

<!-- 
******************************************
********** FORM RESULTADOS ***************
******************************************
-->
<form id="ANALIZA_RESULTADO">
	<block>
		<log label="CONTROLADOR"><value expr="'resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
		<log label="CONTROLADOR"><value expr="'error: ' + SUB_error"/></log>
		<log label="CONTROLADOR"><value expr="'activo menu privacidad: ' + SUB_activoMenu"/></log>
					
		<if cond="SUB_resultadoOperacion == 'OK'">
			<if cond="SUB_activoMenu == 'SI'">
				<!-- ABALFARO_20170717 cambio para dar locucion directamente en vez de menu -->
				<goto next="#MENU_PRINCIPAL"/>
			</if>
		</if>
		<goto next="#FIN"/>	
		
	</block>
</form>


<form id="MENU_PRINCIPAL">
	<block>
		<log label="CONTROLADOR"><value expr="'Men� principal'"/></log>
		<assign name="PRM_IN_MENU_idMenu" expr="'ID-AU-MENU-PRINCIPAL'"/>	
		<assign name="PRM_IN_MENU_variable" expr="''"/>
		<assign name="PRM_IN_MENU_pathXML" expr="''"/>
		<assign name="PRM_IN_MENU_idInvocacion" expr="VG_loggerServicio.idLlamada"/>
		<assign name="PRM_IN_MENU_idServicio" expr="VG_loggerServicio.idServicio"/>
		<assign name="PRM_IN_MENU_idElemento" expr="VG_loggerServicio.idElemento"/>
<!-- 		<assign name="PRM_IN_MENU_paramAdicional" expr="VG_controlador.nombre"/>7 -->
		
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_idMenu: ' + PRM_IN_MENU_idMenu"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_variable: ' + PRM_IN_MENU_variable"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_pathXML: ' + PRM_IN_MENU_pathXML"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_idInvocacion: ' + PRM_IN_MENU_idInvocacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_idServicio: ' + PRM_IN_MENU_idServicio"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_idElemento: ' + PRM_IN_MENU_idElemento"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MENU_paramAdicional: ' + PRM_IN_MENU_paramAdicional"/></log>     
	</block>
<!-- method="post"  -->
	<subdialog name="menuPrincipal" fetchhint="safe" 
		src="${pageContext.request.contextPath}/MenuPlantilla/plantilla/MENU-DINAMICO-PLANTILLA.jsp"
		namelist="PRM_IN_MENU_pathXML PRM_IN_MENU_idMenu PRM_IN_MENU_variable PRM_IN_MENU_idInvocacion PRM_IN_MENU_idServicio PRM_IN_MENU_idElemento PRM_IN_MENU_paramAdicional">
			
		<filled>
			<assign name="VG_menu.codigoRetorno" expr="menuPrincipal.PRM_OUT_MENU_codigoRetorno"/>
			<assign name="VG_menu.error" expr="menuPrincipal.PRM_OUT_MENU_error"/>
			<assign name="VG_menu.respuestaUsuario" expr="menuPrincipal.PRM_OUT_MENU_respuestaUsuario"/>
			<assign name="VG_menu.intento" expr="menuPrincipal.PRM_OUT_MENU_intento"/>
			<assign name="VG_menu.modoInteraccion" expr="menuPrincipal.PRM_OUT_MENU_modoInteraccion"/>
			<assign name="VG_menu.nivelConfianza" expr="menuPrincipal.PRM_OUT_MENU_nivelConfianza"/>
			
			<log label="CONTROLADOR"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="CONTROLADOR"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="CONTROLADOR"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="CONTROLADOR"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="CONTROLADOR"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
			<log label="CONTROLADOR"><value expr="'VUELTA menu principal'"/></log>
			
<!-- 			<if cond="VG_menu.codigoRetorno == 'OK'">	 -->
				<assign name="PRM_OUT_CODIGO_RETORNO" expr="VG_menu.codigoRetorno"/>
				<assign name="PRM_OUT_ERROR" expr="VG_menu.error"/>
				<assign name="PRM_OUT_RESPUESTA_USUARIO" expr="VG_menu.respuestaUsuario"/>
				<assign name="PRM_OUT_INTENTO" expr="VG_menu.intento"/>
				<assign name="PRM_OUT_MODO_INTERACCION" expr="VG_menu.modoInteraccion"/>
				<assign name="PRM_OUT_MODO_NIVEL_CONFIANZA" expr="VG_menu.nivelConfianza"/>
				<goto next="#RETORNAR"/>
<!-- 			<else/> -->
<!-- 				<assign name="VG_resultadoOperacion" expr="'KO'"/> -->
<!-- 				<assign name="VG_codigoRetorno" expr="'MAXINT'"/> -->
<!-- 				<assign name="VG_error" expr="''"/> -->
<!-- 				<goto next="#FIN"/> -->
<!-- 			</if> -->
			
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.codigoRetorno: ' + VG_menu.codigoRetorno"/></log>
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.error: ' + VG_menu.error"/></log>
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.respuestaUsuario: ' + VG_menu.respuestaUsuario"/></log>
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.intento: ' + VG_menu.intento"/></log>
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.modoInteraccion: ' + VG_menu.modoInteraccion"/></log>
			<log label="MODULO-CONTROLADOR"><value expr="'VG_menu.nivelConfianza: ' + VG_menu.nivelConfianza"/></log>
		</filled>
	</subdialog>
</form>

<form id="FIN">
	<block>		
		<log label="CONTROLADOR"><value expr="'FIN - SUB-CONTROLADOR-MENU-PRINCIPAL'"/></log>		
		<return namelist="SUB_resultadoOperacion SUB_error"/>
	</block>
</form>

<form id="RETORNAR">
	<block>		
		<log label="CONTROLADOR"><value expr="'FIN - SUB-MENU-PRINCIPAL'"/></log>		
		<return namelist="PRM_OUT_CODIGO_RETORNO PRM_OUT_ERROR PRM_OUT_RESPUESTA_USUARIO PRM_OUT_INTENTO PRM_OUT_MODO_INTERACCION PRM_OUT_MODO_NIVEL_CONFIANZA"/>
	</block>
</form>

</vxml>

