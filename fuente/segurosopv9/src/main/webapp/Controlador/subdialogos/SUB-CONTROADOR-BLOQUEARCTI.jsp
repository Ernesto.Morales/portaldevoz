<?xml version="1.0" encoding="ISO-8859-1"?><%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" >
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-SUB-BLOQUEAR-CTI
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>

<var name="VG_loggerServicio" expr="''"/>

<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->

<var name="PRM_resultadoOperacion"/>
<var name="PRM_codigoRetorno"/>
<var name="PRM_error"/>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
	
		<log label="MODULO-BLOQUEARCTI"><value expr="'Catch de cuelgue en SUB-BLOQUEAR-CTI'"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'HANGUP'"/>
		<assign name="PRM_error" expr="''"/>
		
					
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error" />		
		
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
	
		<log label="MODULO-BLOQUEARCTI"><value expr="'Catch de error en SUB-BLOQUEAR-CTI'"/></log>
		
		<log label="MODULO-BLOQUEARCTI"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-BLOQUEARCTI"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="PRM_resultadoOperacion" expr="'KO'"/>
		<assign name="PRM_codigoRetorno" expr="'ERROR'"/>
		<assign name="PRM_error" expr="'ERR_IVRIDENTARJETA('+_event + ':'+_message+')'"/>
							
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error PRM_tipoIdentifRecuperada" />		
		
	</catch>
	
		

<!-- FORMULARIO para identificar al cliente por numero de tarjeta -->
<form id="SUB-BLOQUEAR-CTI">

	<var name="PRM_IN_loggerServicio"/>
<!-- 	<var name="PRM_IN_COT_stat"/> -->
	
	
	<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo, que a su vez invoca a la plantilla -->
<!-- 	<var name="PRM_IN_controlador"/> -->
	<block>
		<log label="SUB-BLOQUEAR-CTI"><value expr="'INICIO -BLOQUEAR CTI'"/></log>
		<assign name="VG_loggerServicio" expr="PRM_IN_loggerServicio"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>	
		<assign name="PRM_codigoRetorno" expr="'${codigoRetorno}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
<!-- 		<if	cond="PRM_resultadoOperacion == 'OK'"> -->
<!-- 					<prompt> -->
<!-- 			bloquear -->
<!-- 			<value expr="PRM_resultadoOperacion"></value> -->
<!-- 			</prompt> -->
<!-- 			<else/> -->
<!-- 				<goto next="#FIN" /> -->
				
<!-- 		</if> -->
		
		<goto next="#FIN"/>		
	
	</block>
	
</form>

<form id="FIN">
	<block>

		<log label="SUB-BLOQUEAR-CTI"><value expr="'FIN - SUB-BLOQUEAR-CTI'"/></log>
		
		<log label="SUB-BLOQUEAR-CTI"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="SUB-BLOQUEAR-CTI"><value expr="'PRM_codigoRetorno: ' + PRM_codigoRetorno"/></log>
		<log label="SUB-BLOQUEAR-CTI"><value expr="'PRM_error: ' + PRM_error"/></log>		
			
<!-- 			<prompt bargein="false"> -->
<!-- 			Clienteantes -->
<!-- 				<value expr="VG_DatosCotizacion.telefono"/>  -->
<!-- 			</prompt> -->
			
		
		<return namelist="PRM_resultadoOperacion PRM_codigoRetorno PRM_error"/>
		
	</block>
</form>


</vxml>
