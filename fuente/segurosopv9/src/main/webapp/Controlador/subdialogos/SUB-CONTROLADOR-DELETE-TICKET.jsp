<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	SUB-CONTROLADOR-DELETE-TICKET
 *  COPYRIGHT:		Copyright (c) 2017
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>


<!-- Definicion parametros salida del servlet -->
<var name="SUB_resultadoOperacion"/>
<var name="SUB_error"/>
<var name="SUB_tsecAnulado"/>

<!-- 
******************************************
********** CAPTURA DE CUELGUE **************
******************************************
-->
	<catch event="connection.disconnect.hangup">
	
		<log label="CONTROLADOR"><value expr="'CUELGUE - SUB-CONTROLADOR-DELETE-TICKET'"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'HANGUP'"/>
		<assign name="SUB_error" expr="''"/>		
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_tsecAnulado"/>
		
	</catch>	
	
<!-- 
******************************************
********** CAPTURA DE ERROR **************
******************************************
-->
	<catch event="error">
	
		<log label="CONTROLADOR"><value expr="'ERROR - SUB-CONTROLADOR-DELETE-TICKET'"/></log>
		
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="SUB_resultadoOperacion" expr="'KO'"/>
		<assign name="SUB_error" expr="'ERROR_IVR(' + _event + ')'"/>	
		
		<return namelist="SUB_resultadoOperacion SUB_error SUB_tsecAnulado"/>
	</catch>

<!-- 
******************************************
************ FORM INICIAL ****************
******************************************
-->
	<form id="SUB_CONTROLADOR_DELETE_TICKET">
	
		<var name="PRM_IN_loggerServicio"/>
		
		<block>				
			<log label="CONTROLADOR"><value expr="'INICIO - SUB_CONTROLADOR_DELETE_TICKET'"/></log>
		
			<assign name="SUB_resultadoOperacion" expr="'${resultadoOperacion}'"/>
			<assign name="SUB_error" expr="'${error}'"/>
			<assign name="SUB_tsecAnulado" expr="'${tsecAnulado}'"/>
			
			<log label="CONTROLADOR"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>	
			<log label="CONTROLADOR"><value expr="'SUB_error: ' + SUB_error"/></log>	
			<log label="CONTROLADOR"><value expr="'SUB_tsecAnulado: ' + SUB_tsecAnulado"/></log>	
					
			<goto next="#RETORNAR"/>	
		
		</block>
	
	</form>
	
	<form id="RETORNAR">
	
		<block>		
			<log label="CONTROLADOR"><value expr="'FIN - SUB_CONTROLADOR_DELETE_TICKET'"/></log>		
			
			<return namelist="SUB_resultadoOperacion SUB_error SUB_tsecAnulado"/>
			
		</block>
	
	</form>


</vxml>

