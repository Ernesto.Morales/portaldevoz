<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-INICIO
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

	<meta http-equiv="Expires" content="0"/>
	
	<property name="fetchaudio" value="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/audio/SPA-silence.wav"/>

	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/LoggerServicio.js"/>
    <script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/DatosCTI.js"/>
    <script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/DatosUUI.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Cliente.js"/>
	
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/BeanControlador.js"/>
<%-- 	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/BeanInfoModulo.js"/> --%>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Menu.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Transfer.js"/>
	
	<!-- [ABALFARO_20170126] Script propio del servicio para la Operativa -->
	<script src="${pageContext.request.contextPath}/scripts/BeanOperativa.js"/>
	<!-- [ABALFARO_20170126] Script propio del servicio para la Info de la Operativa -->
	<script src="${pageContext.request.contextPath}/scripts/BeanInfoOperativa.js"/>
	<!-- [ABALFARO_20170130] Script propio del servicio para la Gestion de Excepciones de Operativas -->
	<script src="${pageContext.request.contextPath}/scripts/BeanGestionExcepciones.js"/>
	<script src="${pageContext.request.contextPath}/scripts/PreCotizacion.js"/>
	<script src="${pageContext.request.contextPath}/scripts/Cotizacion.js"/>
	<!-- ABALFARO_20170217 -->
	<!-- Cuadro de mando - STAT -->
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/Stat.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/FuncionesStat.js"/>
	<script src="${pageContext.request.contextPath}/..${sessionScope.contextoEnrutador}/scripts/TransactionCTI.js"/>
	<!-- 
	****************************************************
	********* DEFINICION DE VARIABLES GLOBALES *********
	****************************************************
	-->
	
	<var name="VG_codigoRetorno" expr="''"/>
	<var name="VG_resultadoOperacion" expr="''"/>
	<var name="VG_error" expr="''"/>
	<var name="VG_cliente" expr="''"/>
	<var name="VG_operativasIVR" expr="''"/>
	<var name="VG_operativasIVRTotales" expr="''"/>
	
	<!-- Configuracion de transferencia a nivel controlador -->
	<var name="VG_activacionSegmentoControlador" expr="''"/>
	<var name="VG_segmentoTransferControlador" expr="''"/>
	<var name="VG_segmentoTransferDefaultControlador" expr="''"/>
	<var name="VG_transfer" expr="''"/>
	
	<!-- Elementos de transferencia -->
	<var name="VG_esquema" expr="''"/>
	<!-- segmento de entrada en la aplicacion -->
	<var name="VG_segmento" expr="''"/>
	<var name="VG_producto" expr="''"/>
	<var name="VG_servicioIVR" expr="''"/>
	<var name="VG_controlador" expr="''"/>
	<var name="VG_menu" expr="''"/>
	
	
	<!-- ABALFARO_20170619 -->
	<var name="VG_arrayOperativas" expr="''"/>
	<var name="VG_historicoEtiqueta" expr="''"/>
	
	<!-- Informacion de la operativa a ejecutar -->
	<var name="VG_operativa" expr="''"/>
	<!-- Informacion de la operativa en el XML de Gestion de Operativas -->
	<var name="VG_infoOperativa" expr="''"/>
	<!-- Informacion des posibles excepciones producidas en el flujo para luego tratarlas segun el XML de Gestion de Operativas -->
	<var name="VG_gestionExcepciones" expr="''"/>
	
	<!-- si esta activa la PA en el servicio: ON/OFF -->
	<var name="VG_pregAbiertaActivo" expr="''"/>
	
	<!-- modo interaccion en el servicio: ASR/DTMF -->
	<var name="VG_modoInteraccionServicio" expr="''"/>
	
	<!-- elemento de identif del cliente -->
	<var name="VG_llaveAccesoFront" expr="''"/>
	
	<!-- PARAMETROS DE SALIDA DEL CONTROLADOR -->
	<var name="PRM_OUT_CONTROLADOR_resultadoOperacion" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_codigoRetorno" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_error" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_cliente" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_operativasIVR" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_esquema" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_segmento" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_producto" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_transfer" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_llaveAccesoFront" expr="''"/>	
	<!-- ABALFARO_20170217 -->
	<var name="PRM_OUT_CONTROLADOR_stat" expr="''"/>
	<var name="PRM_OUT_CONTROLADOR_VG_RetornoCTI" expr="''"/>
	<!--
	****************************************************
	********* DEFINICION DE VARIABLES LOGGER ***********
	****************************************************
	-->
	<var name="VG_loggerServicio" expr="''"/>
	<!--
	****************************************************
	********* DEFINICIoN DE PROPIEDADES CTI ************
	****************************************************
	-->
	<var name="VG_datosCTI" expr="''"/>
	
	<var name="VG_RetornoCTI" expr="''"/>
	<!--
	****************************************************
	********* DEFINICIoN DE PROPIEDADES UUI ************
	****************************************************
	-->	
	<var name="VG_datosUUI" expr="''"/>
		
	<!-- 
	****************************************************
	*** DEFINICION DE VARIABLES STAT-CUADRO MANDO ******
	****************************************************
	-->
	<!-- ABALFARO_20170217 -->
	<var name="VG_stat" expr="''"/>
	
	<!-- 
	****************************************************
	** DEFINICIoN DE PROPIEDADES GLOBALES DEL SERVICIO *
	****************************************************
	-->

	<property name="inputmodes" value="dtmf voice"/>
	<property name="termchar" value=""/>

<!--
*************************************************
***** DEFINICION DE VARIABLES DE LOCUCION *******
*************************************************
-->
<var name="locucionesInformacion"/>
<var name="PRM_IN_LOC_idioma" expr="''"/>
<var name="PRM_IN_LOC_idLlamada" expr="''"/>
<var name="PRM_IN_LOC_idServicio" expr="''"/>
<var name="PRM_IN_LOC_idElemento" expr="''"/>
<var name="PRM_IN_LOC_idModulo" expr="''"/>
<var name="PRM_IN_LOC_locuciones" expr="''"/>
<var name="PRM_IN_LOC_tipoLocucion" expr="''"/>
<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Precotizacion****
	*****************************************************
-->
	<var name="VG_DatosPrecotizacion"/>
<!-- ****************************************************
	********* DEFINICION DE PROPIEDADES Precotizacion****
	*****************************************************
-->
	<var name="VG_DatosCotizacion"/>
	
	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->
	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->

	<catch event="connection.disconnect.hangup">
		<log label="CONTROLADOR"><value expr="'Catch de cuelgue en INICIO-CONTROLADOR'"/></log>
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
		<assign name="VG_error" expr="''"/>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat"/>
	</catch>	
		
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	<catch event="error">
		<log label="CONTROLADOR"><value expr="'Catch de error en INICIO-CONTROLADOR'"/></log>
		<log label="CONTROLADOR"><value expr="'EVENT: ' + _event"/></log>
		<log label="CONTROLADOR"><value expr="'MESSAGE: ' + _message"/></log>
		
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'ERROR'"/>
		<assign name="VG_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>				  
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat"/>								  
	</catch>
	

	<form id="CONTROLADOR_INICIO">
		<block>
			<!-- esto NO se ejecuta, siempre se empezara en la pagina INICIO-PARAMS -->
			<submit next="${pageContext.request.contextPath}/CONTROLADOR/inicioParams" namelist="" method="post"/>
		</block>
	</form>

</vxml>





