<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-GESTION_INICIAL
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="PRM_vdn" expr="''"/>
<var name="PRM_tipoCliente" expr="''"/>
<var name="PRM_IN_IDENT_tipoIdentificacion" expr="''"/>
<var name="PRM_IN_IDENT_intentosIdentificacion" expr="''"/>
<var name="PRM_IN_IDENT_controlador" expr=""/>
<var name="PRM_OUT_IDENT_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_IDENT_codigoRetorno" expr="''"/>
<var name="PRM_OUT_IDENT_error" expr="''"/>
<var name="PRM_OUT_IDENT_isIdentificado" expr="''"/>
<var name="PRM_OUT_IDENT_tipoIdentificacion" expr="''"/>
<var name="PRM_OUT_IDENT_ramaIdentificacion" expr="''"/>
<var name="PRM_OUT_IDENT_cliente" expr="''"/>
<var name="PRM_OUT_IDENT_llaveAccesoFront" expr="''"/>
<var name="PRM_OUT_IDENT_info" expr="''"/>
<var name="PRM_OUT_IDENT_gestionExcepciones" expr="''"/>
<var name="PRM_OUT_IDENT_stat" expr="''"/>
<var name="PRM_PROM" expr="''"/>
<var name="PRM_EXK" expr="''"/>
<var name="PRM_MENERR" expr="''"/>
<var name="PRM_Respuesta" expr="''"/>
<var name="verificaDigitos" expr="''"/>
<var name="PRM_IN_AUT_tipoAutenticacion" expr="''"/>
<var name="PRM_IN_AUT_intentosAutenticacion" expr="''"/>
<var name="VG_Datos_Shortcut" expr="''"/>
<var name="VG_tipoShortcut" expr="''"/>
<var name="VG_Short" expr="''"/>
<var name="VG_opc" expr="''"/>
<var name="VG_ligaRetorno" expr="''"/>
<var name="VG_serv" expr="''"/>
<var name="VG_segmentoId" expr="''"/>
<var name="PRM_listaTipoAutenticacion" expr="''"/>
<var name="PRM_listaIntentosAutenticacion" expr="''"/>
<script>
		VG_DatosCotizacion = new Cotizacion();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de cuelgue en CONTROLADOR_INICIO'"/></log>
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
		<assign name="VG_error" expr="''"/>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
	</catch>
	
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de error en CONTROLADOR_INICIO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MESSAGE: ' + _message"/></log>
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'ERROR'"/>
		<assign name="VG_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
	</catch>


<form id="CONTROLADOR_INICIO">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>
		<assign name="PRM_tipoCliente" expr="''"/>
		<assign name="VG_cliente.tsec" expr="'${tsec}'"/>
<!-- 		<goto next="#SUB_AUTENTICACION" /> -->
<!-- 			<goto next="#SUB_ZONADE_PAGO" /> -->
		<goto next="#BIENVENIDA"/>
	</block>
</form>

 <form id="BIENVENIDA">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>
		<assign name="PRM_tipoCliente" expr="''"/>
		prueba
	</block>
	
	<subdialog name="subBienvenida" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subBienvenida" namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subBienvenida.SUB_resultadoOperacion"/>
			<assign name="VG_error" expr="subBienvenida.SUB_error"/>
			
			<log label="CONTROLADOR"><value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + VG_error"/></log>
			
			<!-- [AAO-20161215]  -->
			<if	cond="VG_resultadoOperacion == 'HANGUP'">
				<!-- si es HANGUP debo finalizar llamada -->
				
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
				
			<else/>
				<goto next="#BLOCK" />
				
			</if>
		</filled>
	</subdialog>
</form>

<form id="BLOCK">
	<block>
	BLOCK
		<assign name="PRM_vdn" expr="VG_segmento"/>	
	</block>
	
	<subdialog name="subAvisoPrivacidad" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subBlock" 
		namelist="VG_loggerServicio VG_datosUUI">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subAvisoPrivacidad.SUB_resultadoOperacion"/>
			<assign name="VG_error" expr="subAvisoPrivacidad.SUB_error"/>
			23
			<prompt>
				<value expr="VG_resultadoOperacion"></value>
			</prompt>
			<if cond="VG_resultadoOperacion == 'OK'">
				<goto next="#AVISO_PRIVACIDAD" />
<!-- 				<goto next="#SUB_AUTENTICACION" /> -->
			<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				<goto next="#FIN"/>
			<else/>
				<assign name="VG_codigoRetorno" expr="'ERROR'"/>
				<goto next="#FIN"/>
			</if>		
		</filled>	
	</subdialog>	
</form>

<form id="AVISO_PRIVACIDAD">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>	
		Privacidad
	</block>
	
	<subdialog name="subAvisoPrivacidad" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subAvisoPrivacidad" 
		namelist="VG_loggerServicio">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subAvisoPrivacidad.SUB_resultadoOperacion"/>
			<assign name="VG_error" expr="subAvisoPrivacidad.SUB_error"/>
						
			<if cond="VG_resultadoOperacion == 'OK'">
			<goto next="#FIN"/>
<!-- 				<goto next="#SUB_OBTENER_COTIZACION" /> -->
<!-- 				<goto next="#SUB_AUTENTICACION" /> -->
			<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				<goto next="#FIN"/>
			<else/>
				<assign name="VG_codigoRetorno" expr="'ERROR'"/>
				<goto next="#FIN"/>
			</if>		
		</filled>	
	</subdialog>	
</form>


<form id="FIN">
	<block>
		<log label="CONTROLADOR-GESTION-SEGUROS"><value expr="'FIN - controlador SEGUROS'"/></log>
		<log label="CONTROLADOR-GESTION-SEGUROS"><value expr="'Vamos a CONTROLADOR-FIN'"/></log>
		
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
			namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
			VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat"/>
	</block>
</form>
	
<!--  -->
 <script><![CDATA[
      var viCont;
      var vsShortCut;
      var callStack = new Array();
      var callReturn;
      var INTV_DOC_NAME='Prueba';
      var INTV_ERROR_COUNT=0;
      var INTV_NOINPUT_COUNT=0;
      var INTV_NOMATCH_COUNT=0;
      var INTV_CONFIRM_COUNT=0;
      var INTV_RETURN_VALUE;
      var INTV_RETURN_LEG;
      var INTV_NULL;
   ]]></script>
</vxml>
