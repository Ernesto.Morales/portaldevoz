<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-GESTION_INICIAL
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->
<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_codigoRetorno" expr="''"/>
<var name="PRM_error" expr="''"/>
<var name="PRM_vdn" expr="''"/>
<var name="PRM_tipoCliente" expr="''"/>
<var name="PRM_IN_IDENT_tipoIdentificacion" expr="''"/>
<var name="PRM_IN_IDENT_intentosIdentificacion" expr="''"/>
<var name="PRM_IN_IDENT_controlador" expr=""/>
<var name="PRM_OUT_IDENT_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_IDENT_codigoRetorno" expr="''"/>
<var name="PRM_OUT_IDENT_error" expr="''"/>
<var name="PRM_OUT_IDENT_isIdentificado" expr="''"/>
<var name="PRM_OUT_IDENT_tipoIdentificacion" expr="''"/>
<var name="PRM_OUT_IDENT_ramaIdentificacion" expr="''"/>
<var name="PRM_OUT_IDENT_cliente" expr="''"/>
<var name="PRM_OUT_IDENT_llaveAccesoFront" expr="''"/>
<var name="PRM_OUT_IDENT_info" expr="''"/>
<var name="PRM_OUT_IDENT_gestionExcepciones" expr="''"/>
<var name="PRM_OUT_IDENT_stat" expr="''"/>
<var name="PRM_PROM" expr="''"/>
<var name="PRM_EXK" expr="''"/>
<var name="PRM_MENERR" expr="''"/>
<var name="PRM_Respuesta" expr="''"/>
<var name="verificaDigitos" expr="''"/>
<var name="PRM_IN_AUT_tipoAutenticacion" expr="''"/>
<var name="PRM_IN_AUT_intentosAutenticacion" expr="''"/>
<var name="VG_Datos_Shortcut" expr="''"/>
<var name="VG_tipoShortcut" expr="''"/>
<var name="VG_Short" expr="''"/>
<var name="VG_opc" expr="''"/>
<var name="VG_ligaRetorno" expr="''"/>
<var name="VG_serv" expr="''"/>
<var name="VG_segmentoId" expr="''"/>
<var name="PRM_listaTipoAutenticacion" expr="''"/>
<var name="PRM_listaIntentosAutenticacion" expr="''"/>
<script>
		VG_DatosCotizacion = new Cotizacion();
</script>

	<!-- 
	******************************************
	********** CAPTURA DEL CUELGUE ***********
	******************************************
	-->

	<!-- Para que reconozca en cualquier momento de la llamada si se cuelga la llamada-->
	<catch event="connection.disconnect.hangup">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de cuelgue en CONTROLADOR_INICIO'"/></log>
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
		<assign name="VG_error" expr="''"/>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
	</catch>
	
	<!-- 
	******************************************
	********** CAPTURA DE ERROR **************
	******************************************
	-->
	
	<catch event="error">
		<log label="MODULO-AUTENTICACION"><value expr="'Catch de error en CONTROLADOR_INICIO'"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'EVENT: ' + _event"/></log>
		<log label="MODULO-AUTENTICACION"><value expr="'MESSAGE: ' + _message"/></log>
		<assign name="VG_resultadoOperacion" expr="'KO'"/>
		<assign name="VG_codigoRetorno" expr="'ERROR'"/>
		<assign name="VG_error" expr="'ERR_IVR('+_event + ':'+_message+')'"/>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
	</catch>


<form id="CONTROLADOR_INICIO">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>
		<assign name="PRM_tipoCliente" expr="''"/>
		<assign name="VG_cliente.tsec" expr="'${tsec}'"/>
		<goto next="#BIENVENIDA"/>
	</block>
</form>

 <form id="BIENVENIDA">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>
		<assign name="PRM_tipoCliente" expr="''"/>
	</block>
	
	<subdialog name="subBienvenida" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subBienvenida" namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subBienvenida.SUB_resultadoOperacion"/>
			<assign name="VG_error" expr="subBienvenida.SUB_error"/>
			
			<log label="CONTROLADOR"><value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'subBienvenida error: ' + VG_error"/></log>
			
			<!-- [AAO-20161215]  -->
			<if	cond="VG_resultadoOperacion == 'HANGUP'">
				<!-- si es HANGUP debo finalizar llamada -->
				
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_transfer"/>
				
			<else/>
			
<!-- 			<goto next="#SUB_ZONA_PAGO_REALIZAR" /> -->
				<goto next="#BLOCK_CTI" /> 

				
			</if>
		</filled>
	</subdialog>
</form>


<form id="BLOCK_CTI">
	<block>
<!-- 	BLOCK -->
	</block>
	
	<subdialog name="subBloquearCTI" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subBloquearCTI" 
		namelist="VG_loggerServicio VG_datosUUI">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subBloquearCTI.PRM_resultadoOperacion"/>
			<assign name="VG_error" expr="subBloquearCTI.PRM_error"/>
			<assign name="VG_codigoRetorno" expr="subBloquearCTI.PRM_codigoRetorno"/>
<!-- 			23 -->
<!-- 			<prompt> -->
<!-- 				<value expr="VG_resultadoOperacion"></value> -->
<!-- 			</prompt> -->
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'BCKO')"/>
			<if cond="VG_resultadoOperacion == 'OK'">
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'BCOK')"/>
<!-- 			<goto next="#FIN"/> -->
<!-- 				<goto next="#SUB_OBTENER_COTIZACION" /> -->
				<goto next="#AVISO_PRIVACIDAD" />
			<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				<goto next="#FIN"/>
			<else/>
				<assign name="VG_codigoRetorno" expr="'ERROR'"/>
				<goto next="#FIN"/>
			</if>		
		</filled>	
	</subdialog>	
</form>

<form id="AVISO_PRIVACIDAD">
	<block>
		<assign name="PRM_vdn" expr="VG_segmento"/>	
	</block>
	
	<subdialog name="subAvisoPrivacidad" method="post" src="${pageContext.request.contextPath}/CONTROLADOR/subAvisoPrivacidad" 
		namelist="VG_loggerServicio">
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
		
		<filled>
			<assign name="VG_resultadoOperacion" expr="subAvisoPrivacidad.SUB_resultadoOperacion"/>
			<assign name="VG_error" expr="subAvisoPrivacidad.SUB_error"/>
						
			<if cond="VG_resultadoOperacion == 'OK'">
				<goto next="#SUB_OBTENER_COTIZACION" />
			<elseif cond="VG_menu.codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				 <goto next="#FIN"/>

			<else/>
				<assign name="VG_codigoRetorno" expr="'ERROR'"/>
				 <goto next="#FIN"/> 
			</if>		
		</filled>	
	</subdialog>	
</form>

<form id="SUB_AUTENTICACION">
	<block>
		<!--  esta activa la identificacion en el controlador -->
		<log label="MODULO-AUTENTICACION-OTP"><value expr="'AUTENTICACION ACTIVADA'"/></log>
		
<!-- 		<assign name="PRM_IN_AUT_tipoAutenticacion" expr="'tarjeta'"/> -->
<!-- 		<assign name="PRM_IN_AUT_intentosAutenticacion" expr="'3'"/> -->
		
		<assign name="PRM_IN_AUT_tipoAutenticacion" expr="PRM_listaTipoAutenticacion"/>
		<assign name="PRM_IN_AUT_intentosAutenticacion" expr="PRM_listaIntentosAutenticacion"/>
		<log label="CONTROLADOR"><value expr="'VGSERAUTENTICACION' + VG_serv"/></log>
		<assign name="VG_cliente.contratoIdentificacion" expr="VG_DatosCotizacion.idCotiza"/>
		<assign name="VG_cliente.idSession" expr="VG_DatosCotizacion.idSession"/>
<!-- 		<assign name="VG_cliente.telefonoContacto" expr="'5513810987'"/> -->
	</block>
	
	<subdialog name="subAutenticacion" method="post" src="${pageContext.request.contextPath}/MODULO-AUTENTICACION-LB/inicioParams" 
            namelist="VG_serv VG_opc VG_datosCTI VG_datosUUI VG_loggerServicio PRM_IN_AUT_tipoAutenticacion PRM_IN_AUT_intentosAutenticacion">

		<param name="PRM_IN_AUT_tipoAutenticacion" expr="PRM_IN_AUT_tipoAutenticacion"/>
		<param name="PRM_IN_AUT_intentosAutenticacion" expr="PRM_IN_AUT_intentosAutenticacion"/>
		<param name="PRM_IN_AUT_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_AUT_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_AUT_operativa" expr="VG_infoOperativa"/>
		<param name="PRM_IN_AUT_ramaIdentificacion" expr="VG_cliente.ramaIdentificacion"/>
		
		<!-- ABALFARO_20170131 -->
		<param name="PRM_IN_AUT_gestionExcepciones" expr="VG_gestionExcepciones"/>
		
		<!-- [NMB 20170227] Cuadro de Mando -->
		<param name="PRM_IN_AUT_stat" expr="VG_stat"/>
		
		<!-- [NMB_201705] Permite tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_AUT_controlador" expr="VG_controlador"/>
		
		<filled>
			<log label="MODULO-AUTENTICACION-"><value expr="'RETORNO A CONTROLADOR'"/></log>
			<assign name="VG_resultadoOperacion" expr="subAutenticacion.PRM_OUT_AUT_resultadoOperacion"/>
			<assign name="VG_codigoRetorno" expr="subAutenticacion.PRM_OUT_AUT_codigoRetorno"/>
			<assign name="VG_error" expr="subAutenticacion.PRM_OUT_AUT_error"/>
			<assign name="VG_stat" expr="subAutenticacion.PRM_OUT_AUT_stat"/>
			<param name="VG_Short" expr="subAutenticacion.PRM_OUT_AUT_info"/>
			<assign name="VG_ligaRetorno" expr="'_TRANSACCIONES'"/>
			<assign name="VG_transfer.ligaRetorno" expr="VG_ligaRetorno"/>
			
			<log label="MODULO-AUTENTICACION"><value expr="'codigooooooooooooooooooo' + PRM_error"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'AAAAAAAAAAAAARETORNO A CONTROLADOR' + VG_codigoRetorno"/></log>
			<log label="MODULO-AUTENTICACION"><value expr="'VG_cliente.isIdentificadoPPPPPP = ' + VG_cliente.isAutenticado"/></log>
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'AUKO')"/>
			<if	cond="VG_codigoRetorno == 'OK' ">
				<assign name="PRM_Respuesta" expr="subAutenticacion.PRM_OUT_AUT_isAutenticado"/>
				<assign name="VG_cliente" expr="subAutenticacion.PRM_OUT_AUT_cliente"/>
				<assign name="VG_DatosPrecotizacion.TipoOperacion" expr="'3'"/>
				<assign name="VG_RetornoCTI.tipoOperacion" expr="'3'"/>
				<log label="MODULO-ACTIVACION-TARJETA"><value expr="'VG_cliente.isIdentificadoPPPPPP = ' + PRM_Respuesta"/></log>
				<log label="MODULO-ACTIVACION-TARJETA"><value expr="'VG_cliente.TESSSSSSECCCC = ' + VG_cliente.tsecPrivado"/></log>
				<if	cond="VG_cliente.isAutenticado == 'true' ">
						<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'AUOK')"/>
						<goto next="#SUB_ZONADE_PAGO" />
				<else/>
					<goto next="#FIN" />
				</if>
			
			<elseif cond="VG_codigoRetorno == 'HANGUP'" />
					<goto next="#FIN" />
			<elseif cond="PRM_error == 'ERROR_HV'"/>
				<log label="CONTROLADOR"><value expr="'error'+PRM_error"/></log>
				<goto next="#FIN" />
			<else/>
				<!-- **** Descomentar si se quiere volver a reproducir el audio (autenticacion KO): -->
				<!-- **** Lo sentimos, los datos introducidos son invalidos. -->
<!-- 				<assign name="PRM_PROM" expr="'errorMAU'"/> -->
<!-- 				<goto next="#PROMP" /> -->
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'AUKO')"/>
				<goto next="#FIN" />
				
			</if>
		</filled>
	</subdialog>
</form>

<form id="SUB_OBTENER_COTIZACION">
	<block>
		<log label="MODULO-OBTENER COTIZACION"><value expr="'OBTENER COTIZACION'"/></log>	
<!-- 		HARDCORE -->
  <!--    		<assign name="VG_DatosPrecotizacion.idCotiza" expr="'654599'"/>  -->
 	<!--	<assign name="VG_DatosPrecotizacion.idSession" expr="'luCEufi2ZX83LRdSO0gIRG7zSUzGL70f'"/>	-->						 
	</block>
				
	<subdialog name="subObtenerCotizacion" method="post" src="${pageContext.request.contextPath}/MODULO-OBTENER-COTIZACION/subObtenerCotizacion" 
			namelist="VG_loggerServicio VG_DatosPrecotizacion">
		
		<param name="PRM_IN_COT_loggerServicio" expr="VG_loggerServicio"/>
<!-- 		<param name="PRM_IN_COT_stat" expr="VG_stat"/> -->
		<param name="PRM_IN_COT_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<param name="PRM_IN_COT_PREDatosCotizacion" expr="VG_DatosPrecotizacion"/>
		<filled>
		
			<log label="CONTROLADOR"><value expr="'RETORNO A CONTROLADOR DE MODULO-OBTENER COTIZACION'"/></log>
			 
			<assign name="VG_resultadoOperacion" expr="subObtenerCotizacion.PRM_resultadoOperacion"/>
			<assign name="VG_codigoRetorno" expr="subObtenerCotizacion.PRM_codigoRetorno"/>
			<assign name="VG_error" expr="subObtenerCotizacion.PRM_error"/>
			<assign name="VG_DatosCotizacion" expr="subObtenerCotizacion.VG_DatosCotizacion"/>
<!-- 			<assign name="PRM_OUT_IDENT_stat" expr="subObtenerCotizacion.PRM_OUT_IDENT_stat"/> -->
		
<!-- 		    <assign name="VG_stat" expr="PRM_OUT_IDENT_stat"/> -->
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_resultadoOperacion: ' + PRM_OUT_IDENT_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_codigoRetorno: ' + PRM_OUT_IDENT_codigoRetorno"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_error: ' + PRM_OUT_IDENT_error"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_cliente: ' + PRM_OUT_IDENT_cliente"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_isIdentificado: ' + PRM_OUT_IDENT_isIdentificado"/></log>
			<assign name="VG_codigoRetorno" expr="PRM_OUT_IDENT_codigoRetorno"/>
			<assign name="VG_cliente.numCliente" expr="PRM_OUT_IDENT_cliente"/>
			<assign name="VG_error" expr="PRM_OUT_IDENT_error"/>
			<if	cond="VG_resultadoOperacion == 'OK'">
				<assign name="VG_cliente.telefonoContacto" expr="VG_DatosCotizacion.telefono"/>
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'OCOK')"/>
			<if cond="VG_DatosPrecotizacion.tipoOperacion == '2'">
				<goto next="#SUB_AUTENTICACION" />
			<elseif cond="VG_DatosPrecotizacion.tipoOperacion == '3'" />
				<goto next="#SUB_ZONADE_PAGO" />
			<else/>
				<goto next="#SUB_AUTENTICACION" />
			</if>
			<elseif cond="VG_codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_codigoRetorno,'OCKO')"/>
				<goto next="#FIN"/>
			<else/>
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'OCKO')"/>
				<goto next="#FIN" />
			</if>
									
		</filled>	
		
	</subdialog>	

</form>

<form id="SUB_ZONADE_PAGO">
	<block>
		<log label="MODULO-GESTION-INICIAL"><value expr="'ZONA DE PAGO'"/></log>
		<assign name="VG_DatosPrecotizacion.idCotiza" expr="'654599'"/>							
	</block>
				
	<subdialog name="subZonaPagoDatos" method="post" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO-DATOS/inicioParams" 
			namelist="VG_loggerServicio PRM_IN_IDENT_intentosIdentificacion">
		
		<param name="PRM_IN_ZPDATOS_loggerServicio" expr="VG_loggerServicio"/>
		<param name="PRM_IN_ZPDATOS_stat" expr="VG_stat"/>
		<param name="PRM_IN_ZPDATOS_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<filled>
		
			<log label="CONTROLADOR"><value expr="'RETORNO A CONTROLADOR DE MODULO-IDENTIF'"/></log>
			 
			<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="subZonaPagoDatos.PRM_OUT_ODPA_resultadoOperacion"/>
			<assign name="PRM_OUT_IDENT_codigoRetorno" expr="subZonaPagoDatos.PRM_OUT_ODPA_codigoRetorno"/>
			<assign name="PRM_OUT_IDENT_error" expr="subZonaPagoDatos.PRM_OUT_ODPA_error"/>
			<assign name="VG_DatosCotizacion" expr="subZonaPagoDatos.PRM_OUT_ODPA_DatosCotizacion"/>
			<assign name="PRM_OUT_IDENT_stat" expr="subZonaPagoDatos.PRM_OUT_IDENT_stat"/>
			<assign name="VG_Short" expr="subZonaPagoDatos.PRM_OUT_ODPA_info"/>
			
<!-- 		    <assign name="VG_stat" expr="PRM_OUT_IDENT_stat"/> -->
			<assign name="VG_codigoRetorno" expr="PRM_OUT_IDENT_codigoRetorno"/>
			<assign name="PRM_resultadoOperacion" expr="PRM_OUT_IDENT_resultadoOperacion"/>
			<assign name="VG_error" expr="PRM_OUT_IDENT_error"/>
			
<!-- 			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'VALOR DE VARIABLE')"/> -->
<!-- 			BORRRAR AL SUBIR A DE  -->
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_TARJETAAAAAAAAA: ' + VG_DatosCotizacion.tarjeta"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_ANOOOOOTARJETAAAA: ' + VG_DatosCotizacion.mestarjeta"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_MESSSSSSSSSTARJETA: ' + VG_DatosCotizacion.anotarjeta"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_CVVVVVVVVVVVVVSTARJETA: ' + VG_DatosCotizacion.ccv"/></log>
<!-- 			regrese  -->
<!-- 			<prompt> -->
<!-- 				<value expr="PRM_OUT_IDENT_codigoRetorno"></value> -->
<!-- 			</prompt> -->
<!-- 			regrese sss -->
<!-- 			<prompt> -->
<!-- 				<value expr="PRM_resultadoOperacion"></value> -->
<!-- 			</prompt> -->
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_codigoRetorno,VG_Short)"/>
			<if	cond="PRM_resultadoOperacion == 'OK'">
						<goto next="#SUB_ZONA_PAGO_REALIZAR" />
			<elseif cond="VG_codigoRetorno == 'HANGUP'"/>
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_codigoRetorno,VG_Short)"/>
				<goto next="#FIN"/>
			<else/>
				<goto next="#FIN" />
			</if>
									
		</filled>	
		
	</subdialog>	

</form>

<form id="SUB_ZONA_PAGO_REALIZAR">
	<block>
		<log label="MODULO-GESTION-INICIAL"><value expr="'ZONA_PAGO_REALIZAR'"/></log>									
	</block>
				
	<subdialog name="subReazlizaPago" method="post" src="${pageContext.request.contextPath}/MODULO-ZONAPAGO/inicioParams" 
			namelist="VG_loggerServicio VG_DatosCotizacion VG_DatosPrecotizacion">
		
		<param name="PRM_IN_COT_loggerServicio" expr="VG_loggerServicio"/>
	 	<param name="PRM_IN_COT_stat" expr="VG_stat"/>
		<param name="PRM_IN_COT_DatosCotizacion" expr="VG_DatosCotizacion"/>
		<param name="PRM_IN_COT_PREDatosCotizacion" expr="VG_DatosPrecotizacion"/>
		<param name="PRM_IN_COT_cliente" expr="VG_cliente"/>
		<param name="PRM_IN_COT_RETORNOCTI" expr="VG_RetornoCTI"/>
		
		<filled>
		
			<log label="CONTROLADOR"><value expr="'RETORNO A CONTROLADOR DE MODULO realizar pago'"/></log>
			 
			<assign name="PRM_OUT_IDENT_resultadoOperacion" expr="subReazlizaPago.PRM_OUT_IDENT_resultadoOperacion"/>
			<assign name="PRM_OUT_IDENT_codigoRetorno" expr="subReazlizaPago.PRM_OUT_IDENT_codigoRetorno"/>
			<assign name="PRM_OUT_IDENT_error" expr="subReazlizaPago.PRM_OUT_IDENT_error"/>
			<assign name="PRM_OUT_IDENT_info" expr="subReazlizaPago.PRM_OUT_IDENT_infoError"/>
			<assign name="VG_RetornoCTI" expr="subReazlizaPago.PRM_OUT_IDENT_DatosRetornoCTI"/>
<!-- 			<prompt>  -->
<!-- 			regrese main -->
<!-- 			<value expr="PRM_OUT_IDENT_resultadoOperacion"></value> -->
<!-- 		</prompt> -->
<!-- 			<prompt>  -->
<!-- 			regreseejecutar -->
<!-- 			<value expr="PRM_OUT_IDENT_info"></value> -->
<!-- 		</prompt> -->
<!-- 			<assign name="PRM_OUT_IDENT_stat" expr="subObtenerCotizacion.PRM_OUT_IDENT_stat"/> -->
		
<!-- 		    <assign name="VG_stat" expr="PRM_OUT_IDENT_stat"/> -->
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_resultadoOperacion: ' + PRM_OUT_IDENT_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_codigoRetorno: ' + PRM_OUT_IDENT_codigoRetorno"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_error: ' + PRM_OUT_IDENT_error"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_cliente: ' + PRM_OUT_IDENT_cliente"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_COTIZACION_isIdentificado: ' + PRM_OUT_IDENT_isIdentificado"/></log>
			<assign name="VG_codigoRetorno" expr="PRM_OUT_IDENT_codigoRetorno"/>
<!-- 				<prompt>  -->
<!-- 			regrese main -->
<!-- 			<value expr="VG_codigoRetorno"></value> -->
<!-- 		</prompt> -->
			<assign name="PRM_resultadoOperacion" expr="PRM_OUT_IDENT_resultadoOperacion"/>
			<assign name="VG_error" expr="PRM_OUT_IDENT_error"/>
<!-- 			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_codigoRetorno,VG_Short)"/> -->
			<if	cond="PRM_resultadoOperacion == 'OK'">
				<goto next="#FIN" />
			<elseif cond="PRM_OUT_IDENT_info == 'PAGO'"/>
				<goto next="#SUB_ZONADE_PAGO" />
			<else/>
				<goto next="#FIN" />
			</if>
									
		</filled>	
		
	</subdialog>	

</form>

<form id="PROMP">
	<!-- **** Descomentar si se quiere volver a reproducir el audio (autenticacion KO): -->
	<!-- **** Lo sentimos, los datos introducidos son invalidos. -->

	<subdialog name="subProm" method="post"
		src="${pageContext.request.contextPath}/CONTROLADOR/subProms"
		namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente VG_Short PRM_PROM">

	<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio" />
	
	<filled>
		<assign name="VG_resultadoOperacion" expr="subProm.SUB_resultadoOperacion" />
		<assign name="VG_error" expr="subProm.SUB_error" /> <log label="CONTROLADOR">
		<value expr="'subBienvenida resultadoOperacion: ' + VG_resultadoOperacion" /></log>
		<log label="CONTROLADOR">
		<value expr="'subBienvenida error: ' + VG_error" /></log> <!-- [AAO-20161215]  -->
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
			namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
			VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_Cliente"/>
	 </filled>
	 <submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
								namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
								VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_Cliente"/>
	 </subdialog>
</form>


<form id="FIN">
	<block>
		<log label="CONTROLADOR-GESTION-SEGUROS"><value expr="'FIN - controlador SEGUROS'"/></log>
		<log label="CONTROLADOR-GESTION-SEGUROS"><value expr="'Vamos a CONTROLADOR-FIN'"/></log>
		<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
			namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
			VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_Cliente VG_RetornoCTI"/>
	</block>
</form>
	
<!--  -->
 <script><![CDATA[
      var viCont;
      var vsShortCut;
      var callStack = new Array();
      var callReturn;
      var INTV_DOC_NAME='Prueba';
      var INTV_ERROR_COUNT=0;
      var INTV_NOINPUT_COUNT=0;
      var INTV_NOMATCH_COUNT=0;
      var INTV_CONFIRM_COUNT=0;
      var INTV_RETURN_VALUE;
      var INTV_RETURN_LEG;
      var INTV_NULL;
   ]]></script>
</vxml>