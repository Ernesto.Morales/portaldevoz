<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-FIN
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="SUB_resultadoOperacion" expr="''"/>
<var name="SUB_error" expr="''"/>
<var name="SUB_tsecAnulado" expr="''"/>
<var name="PRM_vdn" expr="''"/>
<var name="PRM_tipoCliente" expr="''"/>
<var name="PRM_contador" expr=""/>
<var name="PRM_objetoIdentif" expr="''"/>
<var name="PRM_idCliente" expr="''"/>


<form id="CONTROLADOR_FIN">
	<block>
	
		<log label="CONTROLADOR"><value expr="'INICIO - CONTROLADOR-FIN'"/></log>
		<log label="CONTROLADOR"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
		
		<assign name="PRM_objetoIdentif" expr="VG_cliente.tarjetaIdentificacion"/>
		<assign name="PRM_idCliente" expr="VG_cliente.numCliente"/>
		
		<log label="CONTROLADOR"><value expr="'PRM_objetoIdentif: ' + PRM_objetoIdentif"/></log>	
		<log label="CONTROLADOR"><value expr="'PRM_idCliente: ' + PRM_idCliente"/></log>	
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		<!-- DELETE_TICKET: para fin de llamada o ok, y tambien si es hangup -->
		<!-- DELETE_TSEC: para transferencias o error, ya que si existe tsecPrivado acabaremos transfiriendo -->
		<if cond="VG_codigoRetorno == 'FIN' || VG_codigoRetorno == 'OK'">
			<log label="CONTROLADOR"><value expr="'YYYYYYYY' + VG_codigoRetorno"/></log>
			<!-- en caso de llegar con codigoRetorno OK, lo cambio a fin -->
			<assign name="VG_codigoRetorno" expr="'FIN'"/>
			<log label="JJJJ"><value expr="'JJJJ'+ VG_cliente.tsec "/></log>
<!-- 			<if cond="VG_cliente.tsecPrivado != null &amp;&amp; VG_cliente.tsecPrivado != ''"> -->
<!-- 				hemos generado un tsecPrivado en la llamada	 -->
<!-- 				<assign name="PRM_contador" expr="0"/> -->
<!-- 				<goto next="#CONTROLADOR_FIN_SUB_DELETE_TSEC"/>	 -->
							
<!-- 				<goto next="#CONTROLADOR_FIN_SUB_DELETE_TICKET"/>	 -->
<!-- 			<else/> -->
				<!-- NO hemos generado un tsecPrivado en la llamada -->
				<!-- vamos a despedir la llamada -->
				<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
<!-- 			</if> -->
		
		<elseif cond="VG_codigoRetorno == 'HANGUP'" />
			<assign name="VG_RetornoCTI.estatus" expr="VG_codigoRetorno"/>
			<if cond="VG_cliente.tsecPrivado != null &amp;&amp; VG_cliente.tsecPrivado != ''">
				<!-- hemos generado un tsecPrivado en la llamada -->
				<goto next="#RETORNO"/>	
<!-- 					<goto next="#CONTROLADOR_FIN_SUB_DELETE_TICKET"/> -->
			<else/>
				<!-- NO hemos generado un tsecPrivado en la llamada -->
				<!-- en este caso finalizamos -->
				<goto next="#RETORNO"/>
			</if>
		<elseif cond="VG_codigoRetorno == 'TRANSFER'" />
			<goto next="#RETORNO"/>
		<else/>
			<!-- codigoRetorno = TRANSFER o ERROR -->
			<log label="CONTROLADOR"><value expr="'TTTTTTTTT' + VG_codigoRetorno"/></log>
			
			<if cond="VG_cliente.tsecPrivado != null &amp;&amp; VG_cliente.tsecPrivado != ''">
				<!-- hemos generado un tsecPrivado en la llamada -->
				
				<assign name="PRM_contador" expr="0"/>					
				<goto next="#RETORNO"/>
			<else/>
				<!-- NO hemos generado un tsecPrivado en la llamada -->
				<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
			</if>
			<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
		</if>
	</block>
</form>


<!-- ASERNAG_20170505 ejecucion de WS deleteTicket cuando el retorno es un FIN -->
<form id="CONTROLADOR_FIN_SUB_DELETE_TICKET">
	<block>
		<log label="CONTROLADOR"><value expr="'INICIO - CONTROLADOR_FIN_SUB_DELETE_TICKET.'"/></log>

		<!-- ya se ha borrado el tsec, por ejemplo se ha borrado y despues ha habido hangup y volvemos a pasar por aqui -->
		<if	cond="VG_cliente.deleteTsec == 'OK' ">
			<!-- si se habia producido hangup no se despide la llamada -->
			<if cond="VG_codigoRetorno == 'HANGUP'">
				<goto next="#RETORNO"/>
			<else/>
<!-- 				<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>	 -->
			</if>	
		</if>	
	</block>

	<subdialog name="subDeleteTicket" src="${pageContext.request.contextPath}/CONTROLADOR/subDeleteTicket" method="post" 
				namelist="VG_loggerServicio VG_cliente.tsecPrivado VG_codigoRetorno PRM_objetoIdentif PRM_idCliente">
			
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
			
		<filled>
			<assign name="SUB_resultadoOperacion" expr="subDeleteTicket.SUB_resultadoOperacion"/>	
			<assign name="SUB_error" expr="subDeleteTicket.SUB_error"/>	
			<assign name="SUB_tsecAnulado" expr="subDeleteTicket.SUB_tsecAnulado"/>	
			
			<log label="CONTROLADOR"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_error: ' + SUB_error"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_tsecAnulado: ' + SUB_tsecAnulado"/></log>
			
			<!-- si salimos con anulacion a SI, lo marcamos en el cliente -->
			<if	cond="SUB_tsecAnulado == 'SI'">	
				<log label="CONTROLADOR"><value expr="'Marcamos el borrado del tsec a OK en el cliente'"/></log>
				<assign name="VG_cliente.deleteTsec" expr="'OK'"/>	
			</if>
			
			<!-- [AAO-20161215]  -->
			<if	cond="SUB_resultadoOperacion == 'HANGUP' ">
				<!-- si es HANGUP actualizo los datos de fin de llamada -->
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
				
				<goto next="#RETORNO"/>
			</if>	
			<!-- si se habia producido hangup no se despide la llamada -->
			<if cond="VG_codigoRetorno == 'HANGUP'">
				<goto next="#RETORNO"/>
			<else/>
				<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>	
			</if>					
			<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>	
		</filled>
	</subdialog>
</form>


<!-- ASERNAG_20170505 ejecucion de WS deleteTsec cuando el retorno es una Transferencia -->
<form id="CONTROLADOR_FIN_SUB_DELETE_TSEC">
	<block>
			<log label="CONTROLADOR"><value expr="'INICIO - CONTROLADOR_FIN_SUB_DELETE_TSEC'"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_contador: ' + PRM_contador"/></log>	
			
			<!-- ya se ha borrado el tsec, por ejemplo se ha borrado y despues ha habido hangup y volvemos a pasar por aqui -->
			<if	cond="VG_cliente.deleteTsec == 'OK' &amp;&amp; PRM_contador == 0">
				<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
			</if>
	</block>

	<subdialog name="subDeleteTsec" src="${pageContext.request.contextPath}/CONTROLADOR/subDeleteTsec" method="post" 
				namelist="VG_loggerServicio VG_cliente.tsecPrivado PRM_contador VG_codigoRetorno PRM_objetoIdentif PRM_idCliente">
			
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
			
		<filled>
			<assign name="SUB_resultadoOperacion" expr="subDeleteTsec.SUB_resultadoOperacion"/>	
			<assign name="SUB_error" expr="subDeleteTsec.SUB_error"/>	
			<assign name="SUB_tsecAnulado" expr="subDeleteTsec.SUB_tsecAnulado"/>	
			
			<log label="CONTROLADOR"><value expr="'SUB_resultadoOperacion: ' + SUB_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_error: ' + SUB_error"/></log>
			<log label="CONTROLADOR"><value expr="'SUB_tsecAnulado: ' + SUB_tsecAnulado"/></log>
			<!-- si salimos con anulacion a SI, lo marcamos en el cliente -->
			<if	cond="SUB_tsecAnulado == 'SI' &amp;&amp; PRM_contador == 0">	
				<log label="CONTROLADOR"><value expr="'Marcamos el borrado del tsec a OK en el cliente'"/></log>
				<assign name="VG_cliente.deleteTsec" expr="'OK'"/>	
			</if>
			<log label="CONTROLADOR"><value expr="'Estudiamos si es la primera o la segunda vez que se ejecuta.'"/></log>
			
			<if	cond="SUB_resultadoOperacion == 'OK'">	
				<assign name="PRM_contador" expr="PRM_contador + 1"/>	
				<if	cond="PRM_contador &lt;= 1">
					<!-- Estudiamos si es la primera o la segunda vez que se ejecuta. -->
						<log label="CONTROLADOR"><value expr="'Volvemos a ejecutar para comprobar si se ha anulado el tsec correctamente.'"/></log>
						<goto next="#CONTROLADOR_FIN_SUB_DELETE_TSEC"/>	
				<else/>
						<log label="CONTROLADOR"><value expr="'Ya es la segunda vez que se ejecuta OK: ' + PRM_contador"/></log>
						<assign name="PRM_contador" expr="0"/>
						<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
				</if>	
				
			<elseif cond="SUB_resultadoOperacion == 'HANGUP'" />
					<assign name="VG_resultadoOperacion" expr="'KO'"/>
					<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
					<assign name="VG_error" expr="''"/> 
					<goto next="#RETORNO"/>	
			<else/>
					<log label="CONTROLADOR"><value expr="'No se ha anulado el tsec correctamente' "/></log>
					<goto next="#CONTROLADOR_FIN_SUB_DESPEDIDA"/>
			</if>
		</filled>
	</subdialog>
</form>


<form id="CONTROLADOR_FIN_SUB_DESPEDIDA">
	<block>
		<!-- ABALFARO_20170505 solo para LB, si el segmento es distinto al nombre del controlador es porque ha habido un cambio -->
		<!-- por el tipo de cliente y prevalece este, luego siempre enviaremos como vdn el del controlador actual -->
		<!-- <assign name="PRM_vdn" expr="VG_segmento"/> -->
		<assign name="PRM_vdn" expr="VG_controlador.nombre"/>
		<assign name="PRM_tipoCliente" expr="''"/>	
	</block>

	<subdialog name="subDespedida" src="${pageContext.request.contextPath}/CONTROLADOR/subDespedida" method="post" 
				namelist="VG_loggerServicio PRM_vdn PRM_tipoCliente">
			
		<param name="PRM_IN_loggerServicio" expr="VG_loggerServicio"/>
			
		<filled>
			<assign name="SUB_resultadoOperacion" expr="subDespedida.SUB_resultadoOperacion"/>	
			<assign name="SUB_error" expr="subDespedida.SUB_error"/>
			<log label="CONTROLADOR"><value expr="'subDespedida resultadoOperacion: ' + VG_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'subDespedida error: ' + VG_error"/></log>	
			<!-- [AAO-20161215]  -->
			<if	cond="SUB_resultadoOperacion == 'HANGUP' ">
				<!-- si es HANGUP actualizo los datos de fin de llamada -->
				<assign name="VG_resultadoOperacion" expr="'KO'"/>
				<assign name="VG_codigoRetorno" expr="'HANGUP'"/>
				<assign name="VG_error" expr="''"/>
			</if>	
			<goto next="#RETORNO"/>
		</filled>
	</subdialog>
</form>


<form id="RETORNO">
	<block>	
		<log label="CONTROLADOR"><value expr="'FINCONTROLADOR'"/></log>
		<assign name="PRM_OUT_CONTROLADOR_resultadoOperacion" expr="VG_resultadoOperacion"/>
		<assign name="PRM_OUT_CONTROLADOR_codigoRetorno" expr="VG_codigoRetorno"/>
		<assign name="PRM_OUT_CONTROLADOR_error" expr="VG_error"/>
		<assign name="PRM_OUT_CONTROLADOR_cliente" expr="VG_cliente"/>
		<assign name="PRM_OUT_CONTROLADOR_operativasIVR" expr="VG_operativasIVR"/>
		<assign name="PRM_OUT_CONTROLADOR_esquema" expr="VG_esquema"/>
		<assign name="PRM_OUT_CONTROLADOR_segmento" expr="VG_segmento"/>
		<assign name="PRM_OUT_CONTROLADOR_producto" expr="VG_producto"/>
		<assign name="PRM_OUT_CONTROLADOR_transfer" expr="VG_transfer"/>
		<assign name="PRM_OUT_CONTROLADOR_llaveAccesoFront" expr="VG_llaveAccesoFront"/>		
		<!-- ABALFARO_20170217 devuelvo variable stat-->
		<assign name="PRM_OUT_CONTROLADOR_stat" expr="VG_stat"/>
		<assign name="PRM_OUT_CONTROLADOR_VG_RetornoCTI" expr="VG_RetornoCTI"/>
		<log label="CONTROLADOR"><value expr="'TSECCCCCC1' + PRM_OUT_CONTROLADOR_cliente.tsec"/></log>
		<return namelist="PRM_OUT_CONTROLADOR_resultadoOperacion PRM_OUT_CONTROLADOR_codigoRetorno PRM_OUT_CONTROLADOR_error 
						  PRM_OUT_CONTROLADOR_cliente PRM_OUT_CONTROLADOR_operativasIVR 
						  PRM_OUT_CONTROLADOR_esquema PRM_OUT_CONTssROLADOR_segmento PRM_OUT_CONTROLADOR_producto PRM_OUT_CONTROLADOR_transfer 
						  PRM_OUT_CONTROLADOR_llaveAccesoFront PRM_OUT_CONTROLADOR_stat PRM_OUT_CONTROLADOR_VG_RetornoCTI"/>
	
	</block>
</form>

</vxml>
