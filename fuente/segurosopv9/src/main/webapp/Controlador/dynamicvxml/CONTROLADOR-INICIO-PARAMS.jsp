<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-INICIO-PARAMS
 *  COPYRIGHT:		Copyright (c) 2016
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>
<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>


<form id="CONTROLADOR_INICIO_PARAMS">
	<var name="PRM_IN_CONTROLADOR_loggerServicio"/>
	<var name="PRM_IN_CONTROLADOR_CTIVAR"/>
	<var name="PRM_IN_CONTROLADOR_VG_datosUUI"/>
	<var name="PRM_IN_CONTROLADOR_nombre"/>
	<var name="PRM_IN_CONTROLADOR_cliente"/>
	<var name="PRM_IN_CONTROLADOR_esquema"/>
	<var name="PRM_IN_CONTROLADOR_segmento"/>
	<var name="PRM_IN_CONTROLADOR_producto"/>
	<var name="PRM_IN_CONTROLADOR_activacionSegmento"/>
	<var name="PRM_IN_CONTROLADOR_segmentoTransfer"/>
	<var name="PRM_IN_CONTROLADOR_segmentoTransferDefault"/>
	<!-- ABALFARO_20170217 -->
	<var name="PRM_IN_CONTROLADOR_stat"/>
	<var name="PRM_IN_CONTROLADOR_transfer"/>
	<var name="PRM_IN_CONTROLADOR_VG_RetornoCTI"/>
	<block>
	<!-- 	<exit/> -->
		<script>
			VG_DatosPrecotizacion = new PreCotizacion();
		</script>
		<log label="CONTROLADOR"><value expr="'INICIO - CONTROLADOR-INICIO-PARAMS'"/></log>
		<log label="CONTROLADOR"><value expr="'lang: ${sessionScope.lang}'"/></log>

		<assign name="VG_loggerServicio" expr="PRM_IN_CONTROLADOR_loggerServicio"/>
		<assign name="VG_datosCTI" expr="PRM_IN_CONTROLADOR_CTIVAR"/>
		<assign name="VG_datosUUI" expr="PRM_IN_CONTROLADOR_VG_datosUUI"/>
		<assign name="VG_loggerServicio.idElemento" expr="PRM_IN_CONTROLADOR_nombre"/>
		<assign name="VG_cliente" expr="PRM_IN_CONTROLADOR_cliente"/>
		<assign name="VG_esquema" expr="PRM_IN_CONTROLADOR_esquema"/>
		<assign name="VG_segmento" expr="PRM_IN_CONTROLADOR_segmento"/>
		<assign name="VG_producto" expr="PRM_IN_CONTROLADOR_producto"/>
		<assign name="VG_RetornoCTI" expr="PRM_IN_CONTROLADOR_VG_RetornoCTI"/>
		<!-- ABALFARO_20170203 hasta arreglo IDA asignamos el segmento de cliente al de la entrada -->
		<assign name="VG_cliente.segmento" expr="VG_segmento"/>
		<assign name="VG_activacionSegmentoControlador" expr="PRM_IN_CONTROLADOR_activacionSegmento"/>
		<assign name="VG_segmentoTransferControlador" expr="PRM_IN_CONTROLADOR_segmentoTransfer"/>
		<assign name="VG_segmentoTransferDefaultControlador" expr="PRM_IN_CONTROLADOR_segmentoTransferDefault"/>
		<assign name="VG_transfer" expr="PRM_IN_CONTROLADOR_transfer"/>
		<!-- ABALFARO_20170217 -->
		<assign name="VG_stat" expr="PRM_IN_CONTROLADOR_stat"/>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<assign name="VG_codigoRetorno" expr="'${codigoRetorno}'"/>
		<log label="CONTROLADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="CONTROLADOR"><value expr="'VG_codigoRetorno: ' + VG_codigoRetorno"/></log>
<!-- 		<prompt> -->
<!-- 				inicio Paramas -->
<!-- 				<value expr="VG_RetornoCTI.codigoRetorno"></value> -->
<!-- 		</prompt> -->
		<if cond="PRM_resultadoOperacion == 'OK'">
			<script>
				VG_controlador = new BeanControlador();
				VG_gestionExcepciones = new BeanGestionExcepciones();
				// ABALFARO_20170619
				VG_arrayOperativas = new Array();
				VG_historicoEtiqueta = "";
			</script>
			
			<assign name="VG_controlador.codServIVR" expr="'${controlador.codServivr}'"/>
			<assign name="VG_controlador.nombre" expr="'${controlador.nombreControlador}'"/>
			<assign name="VG_controlador.isReqIdentificacion" expr="'${controlador.identificacion.activo}'"/>
			<assign name="VG_controlador.tipoIdentificacion" expr="'${controlador.getIdentificacion().getInstrumentosActivos()}'"/>
			<assign name="VG_controlador.intentosIdentificacion" expr="'${controlador.getIdentificacion().getNumIntentosInstrumentosActivos()}'"/>
						
<%-- 			<assign name="VG_controlador.accionMaxintMenuIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('MAXINT_MENU')}'"/> --%>
<%--             <assign name="VG_controlador.accionMaxintValidIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('MAXINT_VALID')}'"/> --%>
<%--             <assign name="VG_controlador.accionOkIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('OK')}'"/> --%>
<%-- 			<assign name="VG_controlador.accionMaxintMenuIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('MAXINT_MENU')}'"/> --%>
<%--             <assign name="VG_controlador.accionMaxintValidIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('MAXINT_VALID')}'"/> --%>
            <assign name="VG_controlador.accionOkIdentificacion" expr="'${controlador.getIdentificacion().getResultadoAccion('OK')}'"/>
			<assign name="VG_controlador.isReqAutenticacion" expr="'${controlador.autenticacion.activo}'"/>
			<assign name="VG_controlador.tipoAutenticacion" expr="'${controlador.getAutenticacion().getInstrumentosActivos()}'"/>
			<assign name="VG_controlador.intentosAutenticacion" expr="'${controlador.getAutenticacion().getNumIntentosInstrumentosActivos()}'"/>
			<assign name="VG_controlador.accionClienteSinElementosAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_SIN_ELEMENTOS')}'"/>
            <assign name="VG_controlador.accionClienteBloqueadoCandadoAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_BLOQUEADO_CANDADO')}'"/>
            <assign name="VG_controlador.accionMaxintMenuAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('MAXINT_MENU')}'"/>
            <assign name="VG_controlador.accionMaxintValidAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('MAXINT_VALID')}'"/>
            <assign name="VG_controlador.accionClienteBloqueadoValidAuth" expr="'${controlador.getAutenticacion().getResultadoAccion('CLIENTE_BLOQUEADO_VALID')}'"/>
            
            <assign name="VG_DatosPrecotizacion.tipoOperacion" expr="'${PreCotizacion.tipoOperacion}'"/>
			<assign name="VG_DatosPrecotizacion.idCotiza" expr="'${PreCotizacion.idCotiza}'"/>
            <assign name="VG_DatosPrecotizacion.idSession" expr="'${PreCotizacion.idSession}'"/>
            <assign name="VG_DatosPrecotizacion.idInt" expr="'${PreCotizacion.idint}'"/>
<!--             HARDCORE -->
<!--             <assign name="VG_DatosPrecotizacion.tipoOperacion" expr="'3'"/> -->
            <assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'DCOK')"/>
			<assign name="VG_RetornoCTI.idIntentos" expr="VG_DatosPrecotizacion.idInt"/>
			<assign name="VG_RetornoCTI.tipoOperacion" expr="VG_DatosPrecotizacion.tipoOperacion"/>
			<log label="CONTROLADOR"><value expr="'VG_controlador.codServIVR: ' + VG_controlador.codServIVR"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.nombre: ' + VG_controlador.nombre"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.isReqIdentificacion: ' + VG_controlador.isReqIdentificacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.tipoIdentificacion: ' + VG_controlador.tipoIdentificacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.intentosIdentificacion: ' + VG_controlador.intentosIdentificacion"/></log>
<!-- 			<log label="CONTROLADOR"><value expr="'VG_controlador.accionMaxintMenuIdentificacion: ' + VG_controlador.accionMaxintMenuIdentificacion"/></log> -->
<!-- 			<log label="CONTROLADOR"><value expr="'VG_controlador.accionMaxintValidIdentificacion: ' + VG_controlador.accionMaxintValidIdentificacion"/></log> -->
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionOkIdentificacion: ' + VG_controlador.accionOkIdentificacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.isReqAutenticacion: ' + VG_controlador.isReqAutenticacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.tipoAutenticacion: ' + VG_controlador.tipoAutenticacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.intentosAutenticacion: ' + VG_controlador.intentosAutenticacion"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionClienteSinElementosAuth: ' + VG_controlador.accionClienteSinElementosAuth"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionClienteBloqueadoCandadoAuth: ' + VG_controlador.accionClienteBloqueadoCandadoAuth"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionMaxintMenuAuth: ' + VG_controlador.accionMaxintMenuAuth"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionMaxintValidAuth: ' + VG_controlador.accionMaxintValidAuth"/></log>
			<log label="CONTROLADOR"><value expr="'VG_controlador.accionClienteBloqueadoValidAuth: ' + VG_controlador.accionClienteBloqueadoValidAuth"/></log>
			<log label="CONTROLADOR"><value expr="'FIN - CONTROLADOR-INICIO-PARAMS'"/></log>
			<assign name="VG_loggerServicio.OP" expr="VG_DatosPrecotizacion.TipoOperacion"/>
<!-- 			GestionAutenticacion -->
			<if cond="VG_DatosPrecotizacion.tipoOperacion == '1'">
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionInicialAutenticacion" method="post" 
						namelist="VG_loggerServicio VG_cliente.isIdentificado VG_controlador VG_operativasIVR VG_datosCTI VG_datosUUI VG_DatosPrecotizacion"/>
<!-- 			ZONA DE PAGO -->
			<elseif cond="VG_DatosPrecotizacion.tipoOperacion == '3'" />
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionInicial" method="post" 
						namelist="VG_loggerServicio VG_cliente.isIdentificado VG_controlador VG_operativasIVR VG_datosCTI VG_datosUUI"/>
			<!-- 			entoend -->
			<else/>
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionInicial" method="post" 
						namelist="VG_loggerServicio VG_cliente.isIdentificado VG_controlador VG_operativasIVR VG_datosCTI VG_datosUUI"/>
			</if>
			
		<else/>
		
			<log label="CONTROLADOR"><value expr="'FIN - CONTROLADOR-INICIO-PARAMS'"/></log>
			
			<!-- <assign name="VG_codigoRetorno" expr="'ERROR'"/> -->
			<!-- ABALFARO_20170112 solo si no es transfer (porque el controlador fue null) se asigna a error -->
			<if cond="VG_codigoRetorno != 'TRANSFER'">
				<assign name="VG_codigoRetorno" expr="'ERROR'"/>
			</if>
			<assign name="VG_RetornoCTI" expr="actualizar(VG_RetornoCTI,VG_resultadoOperacion,'DCKO')"/>
			<assign name="VG_resultadoOperacion" expr="PRM_resultadoOperacion"/>
			<assign name="VG_error" expr="PRM_error"/>
<!-- 			<assign name="VG_cliente" expr="''"/> -->
<!-- 			<assign name="VG_esquema" expr="''"/> -->
		
			<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat VG_RetornoCTI"/>
		</if>
	</block>
</form>

</vxml>
