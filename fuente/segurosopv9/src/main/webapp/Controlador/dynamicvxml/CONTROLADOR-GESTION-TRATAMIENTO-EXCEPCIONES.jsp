<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-GESTION-TRATAMIENTO-EXCEPCIONES
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<!-- Definicion parametros locales de salida del servlet -->
<var name="PRM_resultadoOperacion" expr="''"/>
<var name="PRM_error" expr="''"/>


<form id="CONTROLADOR_GESTION_TRATAMIENTO_EXCEPCIONES">
	<block>
		<log label="CONTROLADOR"><value expr="'INICIO - GESTION-TRATAMIENTO-EXCEPCIONES'"/></log>
		<assign name="PRM_resultadoOperacion" expr="'${resultadoOperacion}'"/>
		<assign name="PRM_error" expr="'${error}'"/>
		<log label="CONTROLADOR"><value expr="'PRM_resultadoOperacion: ' + PRM_resultadoOperacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_error: ' + PRM_error"/></log>
		<log label="CONTROLADOR"><value expr="'FIN - GESTION-TRATAMIENTO-EXCEPCIONES'"/></log>
	</block>
</form>

</vxml>
