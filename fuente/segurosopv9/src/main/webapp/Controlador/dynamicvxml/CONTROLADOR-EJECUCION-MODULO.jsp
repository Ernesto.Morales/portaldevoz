<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page pageEncoding="ISO-8859-1" %>
<vxml version="2.1" xmlns="http://www.w3.org/2001/vxml" xml:lang="${sessionScope.lang}" application="${pageContext.request.contextPath}/Controlador/staticvxml/CONTROLADOR-INICIO.jsp">

<!--
 *************************************************************************************************
 *  IDENTIFICADOR:	CONTROLADOR-EJECUCION-MODULO
 *  COPYRIGHT:		Copyright (c) 2019
 *  COMPANIA:		KRANON
 *  AUTOR:			KRANON
 *************************************************************************************************
-->

<meta http-equiv="Expires" content="0"/>

<var name="PRM_ruta" expr="''"/>
<!-- PARAMETROS DE ENTRADA AL MODULO -->
<var name="PRM_IN_MODULO_nombre" expr="''"/>
<var name="PRM_IN_MODULO_ruta" expr="''"/>
<var name="PRM_IN_MODULO_isActivo" expr="''"/>
<var name="PRM_IN_MODULO_rama" expr="''"/>
<var name="PRM_IN_MODULO_isReqIdentificacion" expr="''"/>
<var name="PRM_IN_MODULO_tipoIdentificacion" expr="''"/>
<var name="PRM_IN_MODULO_isReqAutenticacion" expr="''"/>
<var name="PRM_IN_MODULO_tipoAutenticacion" expr="''"/>
<var name="PRM_IN_MODULO_cliente" expr="''"/>
<var name="PRM_IN_MODULO_esProactivo" expr="''"/>
<var name="PRM_IN_MODULO_intentoAutenticacion" expr="''"/>
<!-- ABALFARO_20170601 -->
<var name="PRM_IN_MODULO_historicoEtiqueta" expr="''"/>
<!-- [NMB_201705] Variable para tener visibilidad del CONTROLADOR que invoca al modulo -->
<var name="PRM_IN_MODULO_controlador"/>
<!-- PARAMETROS DE SALIDA DEL MODULO -->
<var name="PRM_OUT_MODULO_resultadoOperacion" expr="''"/>
<var name="PRM_OUT_MODULO_codigoRetorno" expr="''"/>
<var name="PRM_OUT_MODULO_error" expr="''"/>
<var name="PRM_OUT_MODULO_cliente" expr="''"/>
<var name="PRM_OUT_MODULO_esquema" expr="''"/>
<var name="PRM_OUT_MODULO_gestionExcepciones" expr="''"/>
<!-- ABALFARO_20170217 -->
<var name="PRM_OUT_MODULO_stat" expr="''"/>


<form id="CONTROLADOR_EJECUCION_MODULO">
	<block>
		<log label="CONTROLADOR"><value expr="'INICIO - EJECUCION-MODULO'"/></log>
		<!-- QUITAR ********************************************************************* -->		
		<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' CLIENTE - NumCliente: ' + VG_cliente.numCliente +
		' - Nombre: ' + VG_cliente.nombreCliente +
		' - Apellido1: ' + VG_cliente.apellido1 +
		' - Apellido2: ' + VG_cliente.apellido2 +
		' - Rfc: ' + VG_cliente.rfc + 
		' - FechaNac: ' + VG_cliente.fechaNacimiento"/></log>
		
		<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' DIRECCION - Calle: ' + VG_cliente.calle +
		' - Num: ' + VG_cliente.numeroCalle +
		' - Colonia: ' + VG_cliente.colonia +
		' - CodPostal: ' + VG_cliente.codigoPostal +
		' - Municipio: ' + VG_cliente.municipio"/></log>
		
		<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' CONTACTO - Tlfn: ' + VG_cliente.telefonoContacto +
		' - Compania: ' + VG_cliente.companiaTelefono +
		' - Email: ' + VG_cliente.email"/></log>
		
		<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' IDyAUT - EsIdentificado: ' + VG_cliente.isIdentificado +
		' - EsAutenticado: ' + VG_cliente.isAutenticado +
		' - CuentaSeleccionada: ' + VG_cliente.cuentaSeleccionada +
		' - TarjetaSeleccionada: ' + VG_cliente.tarjetaSeleccionada"/></log>
		
		<if	cond="VG_cliente.tiposIdentificacion.length != '0' ">
			<foreach item="tiposIdentificacion" array="VG_cliente.tiposIdentificacion">
				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' IDENTIFICACION - Tipo: ' + tiposIdentificacion.tipo +
				' - Resultado: ' + tiposIdentificacion.resultado"/></log>
			</foreach>
		<else/>
			<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' IDENTIFICACION - NO HAY '"/></log>
		</if>
		
		<if	cond="VG_cliente.tiposAutenticacion.length != '0' ">
			<foreach item="tiposAutenticacion" array="VG_cliente.tiposAutenticacion">
				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' AUTENTICACION - Tipo: ' + tiposAutenticacion.tipo +
				' - Resultado: ' + tiposAutenticacion.resultado"/></log>
			</foreach>
		<else/>
			<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' AUTENTICACION - NO HAY '"/></log>
		</if>
		
		
		<if	cond="VG_cliente.cuentas.length != '0' ">
			<foreach item="cuenta" array="VG_cliente.cuentas">
				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' CUENTA - Num: ' + cuenta.numeroCuenta +
				' - NumeroClabe: ' + cuenta.numeroClabe +
				' - Tipo: ' + cuenta.tipoCuenta +
				' - Moneda: ' + cuenta.moneda +	
				' - Saldo: ' + cuenta.saldo +
				' - SaldoBuenFin: ' + cuenta.saldoBuenFin"/></log>
			</foreach>
		<else/>
			<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' CUENTA - NO HAY '"/></log>
		</if>
		
		<if	cond="VG_cliente.tarjetas.length != '0' ">
			<foreach item="tarjeta" array="VG_cliente.tarjetas">
				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' TARJETAS - Num: ' + tarjeta.numeroTarjeta +
				' - Tipo: ' + tarjeta.tipoTarjeta +
				' - DetalleTarjeta: ' + tarjeta.detalleTarjeta +
				' - Moneda: ' + tarjeta.moneda +
				' - Saldo: ' + tarjeta.saldo +
				' - SaldoInmediato: ' + tarjeta.saldoInmediato +
				' - Status: ' + tarjeta.estadoTarj +
				' - TarjetaAsociada: ' + tarjeta.tarjetaAsociada +
				' - TipoTarifa: ' + tarjeta.tipoTarifa "/></log>
				
				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' TARJETAS DE CREDITO- Sobregiro: ' + tarjeta.sobregiro +
				' - SaldoActual: ' + tarjeta.saldoActual +
				' - SaldoDisponible: ' + tarjeta.saldoDisponible +
				' - PagoNoGenerarIntereses: ' + tarjeta.pagoNoGenerarIntereses +
				' - PagoMinimo: ' + tarjeta.pagoMinimo +
				' - PagoVencido: ' + tarjeta.pagoVencido +
				' - FechaPago: ' + tarjeta.fechaPago"/></log>
			</foreach>
		<else/>
			<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' TARJETAS - NO HAY '"/></log>
		</if>
		
<!-- 		<if	cond="VG_cliente.inversiones.length != '0' "> -->
<!-- 			<foreach item="inversion" array="VG_cliente.inversiones"> -->
<!-- 				<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' INVERSION - Num: ' + inversion.numeroInversion + -->
<!-- 				' - Tipo: ' + inversion.tipoInversion + -->
<!-- 				' - Moneda: ' + inversion.moneda"/></log>				 -->
<!-- 			</foreach> -->
<!-- 		<else/> -->
<!-- 			<log label="DATOS-CLIENTE-PRUEBAS"><value expr="' INVERSION - NO HAY '"/></log>			 -->
<!-- 		</if> -->
		
		<!-- ********************************************************************* -->
		
		<goto next="#SUB_EJECUTAR_MODULO" />
		
	</block>

</form>


<form id="SUB_EJECUTAR_MODULO">
	<block>
<!-- 		<assign name="PRM_IN_MODULO_nombre" expr="VG_infoModulo.nombre"/> -->
<!-- 		<assign name="PRM_IN_MODULO_ruta" expr="VG_infoModulo.ruta"/> -->
<!-- 		<assign name="PRM_IN_MODULO_isActivo" expr="VG_infoModulo.isActivo"/> -->
	
		<assign name="PRM_IN_MODULO_nombre" expr="VG_infoOperativa.infoProceso.nombre"/>
		<assign name="PRM_IN_MODULO_ruta" expr="VG_infoOperativa.infoProceso.ruta"/>
		<assign name="PRM_IN_MODULO_isActivo" expr="VG_infoOperativa.infoProceso.activo"/>
		
<!-- 		<assign name="PRM_IN_MODULO_rama" expr="VG_infoModulo.rama"/> -->
		<assign name="PRM_IN_MODULO_rama" expr="''"/>
	
		
<!-- 		<assign name="PRM_IN_MODULO_isReqIdentificacion" expr="VG_infoModulo.isReqIdentificacion"/> -->
<!-- 		<assign name="PRM_IN_MODULO_tipoIdentificacion" expr="VG_infoModulo.tipoIdentificacion"/> -->
<!-- 		<assign name="PRM_IN_MODULO_isReqAutenticacion" expr="VG_infoModulo.isReqAutenticacion"/> -->
<!-- 		<assign name="PRM_IN_MODULO_tipoAutenticacion" expr="VG_infoModulo.tipoAutenticacion"/> -->
		
		<assign name="PRM_IN_MODULO_isReqIdentificacion" expr="VG_infoOperativa.infoProceso.isReqIdentificacion"/>
		<assign name="PRM_IN_MODULO_tipoIdentificacion" expr="VG_infoOperativa.infoProceso.tipoIdentificacion"/>
		<assign name="PRM_IN_MODULO_isReqAutenticacion" expr="VG_infoOperativa.infoProceso.isReqAutenticacion"/>
		<assign name="PRM_IN_MODULO_tipoAutenticacion" expr="VG_infoOperativa.infoProceso.tipoAutenticacion"/>
		<assign name="PRM_IN_MODULO_cliente" expr="VG_cliente"/>
		<assign name="PRM_IN_MODULO_esProactivo" expr="VG_infoOperativa.infoProceso.proactivo"/>
<!-- 		<assign name="PRM_IN_MODULO_esProactivo" expr="'NO'"/>	 -->
		
		<assign name="PRM_IN_MODULO_intentoAutenticacion" expr="VG_infoOperativa.infoProceso.intentosAutenticacion"/>
		
		<!-- ABALFARO_20170601 -->
		<!-- <assign name="PRM_IN_MODULO_historicoEtiqueta" expr="VG_operativa.historicoEtiqueta"/> -->
		<assign name="PRM_IN_MODULO_historicoEtiqueta" expr="VG_historicoEtiqueta"/>	
		

	
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_nombre: ' + PRM_IN_MODULO_nombre"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_ruta: ' + PRM_IN_MODULO_ruta"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_isActivo: ' + PRM_IN_MODULO_isActivo"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_rama: ' + PRM_IN_MODULO_rama"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_isReqIdentificacion: ' + PRM_IN_MODULO_isReqIdentificacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_tipoIdentificacion: ' + PRM_IN_MODULO_tipoIdentificacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_isReqAutenticacion: ' + PRM_IN_MODULO_isReqAutenticacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_tipoAutenticacion: ' + PRM_IN_MODULO_tipoAutenticacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_intentoAutenticacion: ' + PRM_IN_MODULO_intentoAutenticacion"/></log>
		<log label="CONTROLADOR"><value expr="'PRM_IN_MODULO_historicoEtiqueta: ' + PRM_IN_MODULO_historicoEtiqueta"/></log>
								
		<assign name="PRM_ruta" expr="'${pageContext.request.contextPath}' + PRM_IN_MODULO_ruta"/>
				
		<log label="CONTROLADOR"><value expr="'PRM_ruta: ' + PRM_ruta"/></log>
	
		
	</block>

	<subdialog name="subEjecutarModulo" method="post" srcexpr="PRM_ruta" 
		namelist="VG_loggerServicio PRM_IN_MODULO_nombre PRM_IN_MODULO_ruta PRM_IN_MODULO_isActivo PRM_IN_MODULO_rama PRM_IN_MODULO_isReqIdentificacion 
		PRM_IN_MODULO_tipoIdentificacion PRM_IN_MODULO_isReqAutenticacion PRM_IN_MODULO_tipoAutenticacion PRM_IN_MODULO_esProactivo
		PRM_IN_MODULO_intentoAutenticacion PRM_IN_MODULO_historicoEtiqueta">

	
		<param name="PRM_IN_MODULO_loggerServicio" expr="VG_loggerServicio"/>
		
		<param name="PRM_IN_MODULO_nombre" expr="PRM_IN_MODULO_nombre"/>
		
		<param name="PRM_IN_MODULO_cliente" expr="VG_cliente"/>
		
		<param name="PRM_IN_MODULO_gestionExcepciones" expr="VG_gestionExcepciones"/>
		
		<!-- ABALFARO_20170217 -->
		<param name="PRM_IN_MODULO_stat" expr="VG_stat"/>
		
		<!-- [NMB_201705] Permite tener visibilidad del CONTROLADOR que invoca al modulo -->
		<param name="PRM_IN_MODULO_controlador" expr="VG_controlador"/>
		
		<filled>
			
			<assign name="PRM_OUT_MODULO_resultadoOperacion" expr="subEjecutarModulo.PRM_OUT_MODULO_resultadoOperacion"/>
			<assign name="PRM_OUT_MODULO_codigoRetorno" expr="subEjecutarModulo.PRM_OUT_MODULO_codigoRetorno"/>
			<assign name="PRM_OUT_MODULO_error" expr="subEjecutarModulo.PRM_OUT_MODULO_error"/>
			<assign name="PRM_OUT_MODULO_cliente" expr="subEjecutarModulo.PRM_OUT_MODULO_cliente"/>
			<assign name="PRM_OUT_MODULO_esquema" expr="subEjecutarModulo.PRM_OUT_MODULO_esquema"/>	
			<assign name="PRM_OUT_MODULO_gestionExcepciones" expr="subEjecutarModulo.PRM_OUT_MODULO_gestionExcepciones"/>
			
			<!-- ABALFARO_20170217 -->
			<assign name="PRM_OUT_MODULO_stat" expr="subEjecutarModulo.PRM_OUT_MODULO_stat"/>
			
			<log label="CONTROLADOR"><value expr="'HE VUELTO AL CONTROLADOR'"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_MODULO_resultadoOperacion: ' + PRM_OUT_MODULO_resultadoOperacion"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_MODULO_codigoRetorno: ' + PRM_OUT_MODULO_codigoRetorno"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_MODULO_error: ' + PRM_OUT_MODULO_error"/></log>
			<log label="CONTROLADOR"><value expr="'PRM_OUT_MODULO_esquema: ' + PRM_OUT_MODULO_esquema"/></log>
			
			
			<assign name="VG_resultadoOperacion" expr="PRM_OUT_MODULO_resultadoOperacion"/>
			<assign name="VG_codigoRetorno" expr="PRM_OUT_MODULO_codigoRetorno"/>
			<assign name="VG_error" expr="PRM_OUT_MODULO_error"/>
			<assign name="VG_gestionExcepciones" expr="PRM_OUT_MODULO_gestionExcepciones"/>			
			
			<!-- ABALFARO_20170217 -->
			<assign name="VG_stat" expr="PRM_OUT_MODULO_stat"/>
			
			<!-- Asigno al cliente global el cliente devuelto del modulo -->
			<assign name="VG_cliente" expr="PRM_OUT_MODULO_cliente"/>
			
			<log label="CONTROLADOR"><value expr="'VG_operativasIVR: ' + VG_operativasIVR"/></log>
			
					
			<log label="CONTROLADOR"><value expr="'FIN - EJECUCION-MODULO'"/></log>
			
			

			
			<!-- Primero verifico si se ha producido alguna excepcion de autenticacion (por el caso que autentica internamente en activar tarjeta) -->
				<!-- ******* ANALISIS EXCEPCIONES POST AUTENTICACION ******* -->
					<!-- reviso las excepciones de post autenticacion -->
					<if	cond="VG_gestionExcepciones.postAutenticacion.length != 0">
								
								<var name="nombreOperativaSolicitada" expr="VG_operativa.nombreOperativa"/>
								<log label="CONTROLADOR"><value expr="'nombreOperativaSolicitada: ' + nombreOperativaSolicitada"/></log>
						
								<!-- completo la operativa siguiente con la excepcion dada -->
								<script>
									// ABALFARO_20170619									
									// elimino la operativa actual
									VG_arrayOperativas.shift();
									// aniado la operativa nueva
									var PRM_operativa = new BeanOperativa();
									var numExcepTotal = VG_gestionExcepciones.postAutenticacion.length;
									PRM_operativa.etiquetaRaw =  VG_gestionExcepciones.postAutenticacion[numExcepTotal - 1];
									PRM_operativa.etiquetaRaw = PRM_operativa.etiquetaRaw + "#TYPE#" + nombreOperativaSolicitada;
									VG_arrayOperativas.unshift(PRM_operativa);
								</script>
																   
					<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionPreviaOperativa" method="post" 
											namelist="VG_loggerServicio VG_arrayOperativas VG_gestionExcepciones"/>
											
					</if>
				<!-- ******* ANALISIS EXCEPCIONES POST AUTENTICACION ******* -->
			
			
			<!-- primero verifico si es proactivo, y en ese caso SIEMPRE ire donde me mand� su acci�n, salvo en caso de HANGUP -->
			<if	cond="PRM_IN_MODULO_esProactivo == 'SI' ">
				
				<if	cond="VG_codigoRetorno == 'HANGUP' ">
					
					<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post" 
							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error 
 							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat"/>		
				</if>	
				
				<!-- en otro caso al ser proactivo sigo con la operativa indicada en su accion -->
				<log label="CONTROLADOR"><value expr="'VG_infoOperativa.infoProceso.accion: ' + VG_infoOperativa.infoProceso.accion"/></log>
				<log label="CONTROLADOR"><value expr="'VG_arrayOperativas.length: ' + VG_arrayOperativas.length"/></log>
				
				<!-- ABALFARO_20170619 -->
				<if	cond="VG_infoOperativa.infoProceso.accion == 'GO_TO_MODULO' &amp;&amp; VG_arrayOperativas.length != 0">
					<!-- paso a ejecutar la siguiente operativa del array -->
					<script>
						// ABALFARO_20170619	
						// elimino la operativa ya ejecutada y ya pasar� a ejecutar la siguiente almacenada
						VG_arrayOperativas.shift();
					</script>
					
					
					<if	cond="VG_arrayOperativas.length == 0">
						<!-- en caso de que no tenga ninguna operativa despues, como contingencia vamos a pregunta abierta -->
						<script>						
							// aniado la operativa nueva
							var PRM_operativa = new BeanOperativa();
							PRM_operativa.etiquetaRaw = "BACK_TO_PREGUNTA_ABIERTA";
							VG_arrayOperativas.unshift(PRM_operativa);
						</script>
					</if>
					
				<else/>
					<script>				
						// ABALFARO_20170619	
						// elimino la operativa  ya ejecutada
						VG_arrayOperativas.shift();
						// aniado la operativa nueva
						var PRM_operativa = new BeanOperativa();
						PRM_operativa.etiquetaRaw = VG_infoOperativa.infoProceso.accion;
						VG_arrayOperativas.unshift(PRM_operativa);
					</script>
				</if>
				
															   
				<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionPreviaOperativa" method="post" 
										namelist="VG_loggerServicio VG_arrayOperativas VG_gestionExcepciones"/>
			
			</if>
			
			<!-- ABALFARO_20170608 el que manda para ver donde ir es el codigoRetorno siempre -->
<!-- 			<if	cond="VG_resultadoOperacion == 'OK' "> -->
				
				<if	cond="VG_codigoRetorno == 'OK' ">	
				
					<!-- ABALFARO_20170717 si el cliente esta identificado actualizamos la transferencia al segmento obtenido, que es el controlador -->
					<if	cond="VG_cliente.isIdentificado == 'true'"> 
						<log label="CONTROLADOR"><value expr="'VG_cliente.segmento: ' + VG_cliente.segmento"/></log>
						<log label="CONTROLADOR"><value expr="'VG_controlador.nombre: ' + VG_controlador.nombre"/></log>
						
						<assign name="VG_transfer.segmento" expr="VG_controlador.nombre"/>
						
						
					<else/>
						<!-- retorno el valor de la transferencia al del controlador -->
						<if cond="VG_activacionSegmentoControlador == 'ON' || VG_activacionSegmentoControlador == 'on'">
							<!-- el segmento vuelve a ser el segmento de la entrada -->
							<assign name="VG_transfer.segmento" expr="VG_segmento"/>
						<else/>
							<!-- el segmento vuelve a ser el segmento que indica el controlador -->
							<assign name="VG_transfer.segmento" expr="VG_segmentoTransferControlador"/>
						</if>
					</if>
				
										
						<!-- ******* ANALISIS EXCEPCIONES POST AUTOMATIZACION ******* -->
						<!-- reviso las excepciones de post automatizacion -->
						<if	cond="VG_gestionExcepciones.postAutomatizacion.length != 0">
									<!-- completo la operativa siguiente con la excepcion dada -->
									<script>
										// borro la excepcion que voy a analizar
										VG_gestionExcepciones.postAutomatizacion.splice(numExcepTotal - 1, 1);
										
										// ABALFARO_20170619	
										// elimino la operativa actual
										VG_arrayOperativas.shift();
										// aniado la operativa nueva
										var PRM_operativa = new BeanOperativa();
										var numExcepTotal = VG_gestionExcepciones.postAutomatizacion.length;
										PRM_operativa.etiquetaRaw = VG_gestionExcepciones.postAutomatizacion[numExcepTotal - 1];
										VG_arrayOperativas.unshift(PRM_operativa);
									</script>
																	   
						<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionPreviaOperativa" method="post" 
												namelist="VG_loggerServicio VG_arrayOperativas VG_gestionExcepciones"/>
						</if>
						<!-- ******* ANALISIS EXCEPCIONES POST AUTOMATIZACION ******* -->
				

						<!-- no habia excepciones y la ejecucion ha ido bien -->
						<!-- creo una operativa con la accion a realizar el proceso -->
						<log label="CONTROLADOR"><value expr="'VG_infoOperativa.infoProceso.accion: ' + VG_infoOperativa.infoProceso.accion"/></log>
						<log label="CONTROLADOR"><value expr="'VG_arrayOperativas.length: ' + VG_arrayOperativas.length"/></log>
						
						<!-- ABALFARO_20170619 -->
						<if	cond="VG_infoOperativa.infoProceso.accion == 'GO_TO_MODULO' &amp;&amp; VG_arrayOperativas.length != 0">
							<!-- paso a ejecutar la siguiente operativa del array -->
							<script>
								// ABALFARO_20170619	
								// elimino la operativa ya ejecutada
								VG_arrayOperativas.shift();
							</script>
							
							<if	cond="VG_arrayOperativas.length == 0">
								<!-- en caso de que no tenga ninguna operativa despues, como contingencia vamos a pregunta abierta -->
								<script>						
									// aniado la operativa nueva
									var PRM_operativa = new BeanOperativa();
									PRM_operativa.etiquetaRaw = "BACK_TO_PREGUNTA_ABIERTA";
									VG_arrayOperativas.unshift(PRM_operativa);
								</script>
							</if>
							
						<else/>
							<script>					
								// ABALFARO_20170619	
								// elimino la operativa actual
								VG_arrayOperativas.shift();
								// aniado la operativa nueva
								var PRM_operativa = new BeanOperativa();
								PRM_operativa.etiquetaRaw = VG_infoOperativa.infoProceso.accion;
								VG_arrayOperativas.unshift(PRM_operativa);
							</script>
				
						</if>
																	   
						<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionPreviaOperativa" method="post" 
												namelist="VG_loggerServicio VG_arrayOperativas VG_gestionExcepciones"/>
									
				<else/>
					<!-- VG_codigoRetorno == KO, TRANSFER -->
					
									
					<submit next="${pageContext.request.contextPath}/CONTROLADOR/gestionOperativaPostProceso" method="post" 
									namelist="VG_loggerServicio VG_resultadoOperacion VG_error VG_gestionExcepciones VG_operativa VG_infoOperativa VG_codigoRetorno"/>
				
				</if>
				
			<!-- ABALFARO_20170608 el que manda para ver donde ir es el codigoRetorno siempre -->
<!-- 			<else/> -->
				<!-- VG_resultadoOperacion == KO -->
				
<%-- 					<submit next="${pageContext.request.contextPath}/CONTROLADOR/controladorFin" method="post"  --%>
<!-- 							namelist="VG_loggerServicio VG_codigoRetorno VG_resultadoOperacion VG_error  -->
<!--  							VG_esquema VG_segmento VG_producto VG_operativasIVR VG_operativasIVRTotales VG_stat"/>		 -->

<!-- 			</if> -->
			
		</filled>	
		
	</subdialog>		

</form>


</vxml>
