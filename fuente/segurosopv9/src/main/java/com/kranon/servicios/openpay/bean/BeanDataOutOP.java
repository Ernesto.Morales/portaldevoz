package com.kranon.servicios.openpay.bean;

public class BeanDataOutOP {
	private String id;
	private String authorizationNumber;
	private String creationDate;
	private String operationDate;
	private BeanStatusOP status;
	private BeanPaymentAmountOP paymentAmount;
	private String description;
	private String quotationId;
	private BeanOwnerAccountOPOut customer;
	private BeanCardOPOut card;
	private BeanPointsOP points;
	private BeanInstallmentPlanOPOut installmentPlan;
	private BeanExchangeRateOP exchangeRate;
	private BeanFeeAmountOP feeAmount;
	
	
	public BeanDataOutOP() {
		super();
	}


	public BeanDataOutOP(String id, String authorizationNumber,
			String creationDate, String operationDate, BeanStatusOP status,
			BeanPaymentAmountOP paymentAmount, String description,
			String quotationId, BeanOwnerAccountOPOut customer,
			BeanCardOPOut card, BeanPointsOP points,
			BeanInstallmentPlanOPOut installmentPlan,
			BeanExchangeRateOP exchangeRate, BeanFeeAmountOP feeAmount) {
		super();
		this.id = id;
		this.authorizationNumber = authorizationNumber;
		this.creationDate = creationDate;
		this.operationDate = operationDate;
		this.status = status;
		this.paymentAmount = paymentAmount;
		this.description = description;
		this.quotationId = quotationId;
		this.customer = customer;
		this.card = card;
		this.points = points;
		this.installmentPlan = installmentPlan;
		this.exchangeRate = exchangeRate;
		this.feeAmount = feeAmount;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the authorizationNumber
	 */
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}


	/**
	 * @param authorizationNumber the authorizationNumber to set
	 */
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}


	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return creationDate;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * @return the operationDate
	 */
	public String getOperationDate() {
		return operationDate;
	}


	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}


	/**
	 * @return the status
	 */
	public BeanStatusOP getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(BeanStatusOP status) {
		this.status = status;
	}


	/**
	 * @return the paymentAmount
	 */
	public BeanPaymentAmountOP getPaymentAmount() {
		return paymentAmount;
	}


	/**
	 * @param paymentAmount the paymentAmount to set
	 */
	public void setPaymentAmount(BeanPaymentAmountOP paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the quotationId
	 */
	public String getQuotationId() {
		return quotationId;
	}


	/**
	 * @param quotationId the quotationId to set
	 */
	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}


	/**
	 * @return the customer
	 */
	public BeanOwnerAccountOPOut getCustomer() {
		return customer;
	}


	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(BeanOwnerAccountOPOut customer) {
		this.customer = customer;
	}


	/**
	 * @return the card
	 */
	public BeanCardOPOut getCard() {
		return card;
	}


	/**
	 * @param card the card to set
	 */
	public void setCard(BeanCardOPOut card) {
		this.card = card;
	}


	/**
	 * @return the points
	 */
	public BeanPointsOP getPoints() {
		return points;
	}


	/**
	 * @param points the points to set
	 */
	public void setPoints(BeanPointsOP points) {
		this.points = points;
	}


	/**
	 * @return the installmentPlan
	 */
	public BeanInstallmentPlanOPOut getInstallmentPlan() {
		return installmentPlan;
	}


	/**
	 * @param installmentPlan the installmentPlan to set
	 */
	public void setInstallmentPlan(BeanInstallmentPlanOPOut installmentPlan) {
		this.installmentPlan = installmentPlan;
	}


	/**
	 * @return the exchangeRate
	 */
	public BeanExchangeRateOP getExchangeRate() {
		return exchangeRate;
	}


	/**
	 * @param exchangeRate the exchangeRate to set
	 */
	public void setExchangeRate(BeanExchangeRateOP exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	/**
	 * @return the feeAmount
	 */
	public BeanFeeAmountOP getFeeAmount() {
		return feeAmount;
	}


	/**
	 * @param feeAmount the feeAmount to set
	 */
	public void setFeeAmount(BeanFeeAmountOP feeAmount) {
		this.feeAmount = feeAmount;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanDataOutOP [id=" + id + ", authorizationNumber="
				+ authorizationNumber + ", creationDate=" + creationDate
				+ ", operationDate=" + operationDate + ", status=" + status
				+ ", paymentAmount=" + paymentAmount + ", description="
				+ description + ", quotationId=" + quotationId + ", customer="
				+ customer + ", card=" + card + ", points=" + points
				+ ", installmentPlan=" + installmentPlan + ", exchangeRate="
				+ exchangeRate + ", feeAmount=" + feeAmount + "]";
	}
	
	
}
