package com.kranon.servicios.openpay.bean;

public class BeanInstallmentPlanOPOut {
	private String paymentsNumber;
	private String  paymentsType;
	
	public BeanInstallmentPlanOPOut() {
		super();
	}

	public BeanInstallmentPlanOPOut(String paymentsNumber) {
		super();
		this.paymentsNumber = paymentsNumber;
	}
	
	public BeanInstallmentPlanOPOut(String paymentsNumber, String paymentsType) {
		super();
		this.paymentsNumber = paymentsNumber;
		this.paymentsType = paymentsType;
	}

	/**
	 * @return the paymentsNumber
	 */
	public String getPaymentsNumber() {
		return paymentsNumber;
	}

	/**
	 * @param paymentsNumber the paymentsNumber to set
	 */
	public void setPaymentsNumber(String paymentsNumber) {
		this.paymentsNumber = paymentsNumber;
	}

	
	/**
	 * @return the paymentsType
	 */
	public String getPaymentsType() {
		return paymentsType;
	}

	/**
	 * @param paymentsType the paymentsType to set
	 */
	public void setPaymentsType(String paymentsType) {
		this.paymentsType = paymentsType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanInstallmentPlanOP [paymentsNumber=" + paymentsNumber
				+ ", paymentsType=" + paymentsType + "]";
	}
}
