package com.kranon.servicios.cti;

import ivrtrans.ivrTrans;

import java.util.ArrayList;
import java.util.List;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.cti.bean.BeanDatosCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servicios.cti.bean.ProcesoCTIOutDto;
import com.kranon.singleton.SingletonWebServices;
import com.kranon.util.UtilidadesLoggerKranon;

public class ProcesoCTIBlock {

	private CommonLoggerProcess log;
	private final String loggerFile = "CTI";
	private String url;
	private int timeout;
	private int maxtimeout;
	private int port;
	private int maxIntentos;
	public static String caracterRellenoCTI;
	public static String caracterRellenoUUI;
	private String cadenaCTI;
	private String cadenaUUI;
	// ABALFARO_20170627 nuevo paso de idServicio para poder recuperar el XML de SERVICIOS correspondiente
	private String idServicio;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public static final int GET_CTI = ivrTrans.GET;
	public static final int TRANS_CTI = ivrTrans.XFER;
	public static final int END = ivrTrans.ENDOFSEGMENT;
	public static final int COLGADO_IVR = 1;
	public static final int COLGADO_CLIENTE = 2;
	public static final int TRANSFERIDA = 3;



	public ProcesoCTIBlock(String idInvocacion, String idElemento, String idServicio) {
		// recogemos el idInvocacion e idElemento desde donde se invoca, pero
		// escribiremos en un fichero de Log nuevo
		if (log == null) {
			log = new CommonLoggerProcess(loggerFile + "_" + (idServicio.toUpperCase()));
		}
		logKranon= new CommonLoggerKranon("KRANON-LOG",idInvocacion);
		this.log.inicializar(idInvocacion, idElemento);
		this.idServicio = idServicio;
	}
	
	private void configurarServicio(){

		String idModuloLog = "CONFIGURAR_CTI";
		String resultadoOperacion = "KO";		

		try {
			// 1.- Escribir traza de inicio de modulo
			/** INICIO EVENTO - INICIO MODULO **/		
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url el timeout y el puerto del Xml		
			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}

			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}

			url = datosEntorno.getIntegracionCTI().getUrl();
			timeout = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("timeout") );
			port = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("port") );
			maxtimeout = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("maxtimeout") );
			maxIntentos = Integer.parseInt( datosEntorno.getIntegracionCTI().getValueParametro("maxintentos") );
			
			caracterRellenoCTI = datosEntorno.getIntegracionCTI().getValueParametro("caracterRellenoCTI");
			caracterRellenoUUI = datosEntorno.getIntegracionCTI().getValueParametro("caracterRellenoUUI"); 
			
			resultadoOperacion = "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			// 5.- Escribir traza de Fin
			
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("url", url));
			parametrosSalida.add(new ParamEvent("timeout", String.valueOf(timeout) ));
			parametrosSalida.add(new ParamEvent("port", String.valueOf( port) ));
			parametrosSalida.add(new ParamEvent("maxtimeout", String.valueOf( maxtimeout) ));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			listlogger.add("url"+url);
			listlogger.add("port"+String.valueOf( port) );
			/** FIN EVENTO - FIN MODULO **/
		}

	}

	
	/**
	 * Metodo que prepara los parametros url, port, timeout, y cadena de envo para el servicio del CTI
	 * 
	 * @param beanDatos Objeto {@link BeanDatosCTI} con los valores de entrada para el servicio CTI
	 */
	private void prepararPeticion(BeanDatosCTI beanDatos,BeanDatosUUI beanDatosU) {
		String idModuloLog = "PREPARA_PROCESO_CTI";
		String resultadoOperacion = "KO";		

		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();		
			parametrosEntrada.add( new ParamEvent("CTI", (beanDatos==null)?"null":beanDatos.toString() ) );			
			parametrosEntrada.add( new ParamEvent("UUI", (beanDatosU==null)?null:beanDatosU.toString() ) );
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/
			

			// 3.- Crear la cadena de envo hacia el CTI a partir del bean
			beanDatos = ( beanDatos == null ) ? ( new BeanDatosCTI() ) : beanDatos;
			cadenaCTI = beanDatos.construyeCadena(caracterRellenoCTI);		
			cadenaUUI = (beanDatosU==null) ? "" : beanDatosU.construyeCadena(caracterRellenoUUI);
			
			resultadoOperacion = "OK";
			
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		} finally {
			// 5.- Escribir traza de Fin			
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("cadenaCTI", cadenaCTI));
			parametrosSalida.add(new ParamEvent("cadenaUUI", cadenaUUI));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
	}
//	Metodo para bloquear Los tonos del asesor 
//	Recive el UUID
	public String BlockDTMF(String idllamada){
		logKranon.setIdServicio(idllamada);
		
		int resultado=0;
		String idModuloLog = "BLOCKDTMF_CTI";
		String resultadoOperacion = "KO";
		String error="";
		
		int intentos = 0;
		String cadenaResultado = null;
		// Objeto retorno de la peticion
		BeanDatosCTI datosCTI = null;
		BeanDatosUUI datosUUI = null;
		
		try {
			// 1.- Escribir traza de inicio de modulo			
			/** INICIO EVENTO - INICIO MODULO **/
			this.log.initModuleProcess(idModuloLog, null);
			/** FIN EVENTO - INICIO DE MODULO **/
			
//			prepararPeticion(beanDatos,beanDatosU);
			configurarServicio();
			ivrTrans ivrtrans = new ivrTrans(url,port,timeout,maxtimeout);
//			String cadenaResultado = invocarCTI(GET_CTI);	
//			cadenaResultado=null;
			do{
				this.log.comment("Invocacion CTI intento["+(intentos+1)+"]");
				listlogger.add("Pueerto"+port);
				resultado = ivrtrans.BlockDTMF(port,idllamada);
				intentos++;
			}while( resultado!=1 && intentos < maxIntentos );
			if(resultado==1)
				resultadoOperacion= "OK";
			else
				resultadoOperacion= "KO";
			} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			error=e.toString();
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/
		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("FIN","FIN"));
			parametrosSalida.add(new ParamEvent("respuesta", "" +error) );
			parametrosSalida.add(new ParamEvent("respuesta", ""+resultado) );
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
			listlogger.add("res"+resultado);
			listlogger.add("block"+resultado);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
		}
		return resultadoOperacion;	
	}
}
