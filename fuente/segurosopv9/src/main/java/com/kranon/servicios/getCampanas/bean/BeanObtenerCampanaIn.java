package com.kranon.servicios.getCampanas.bean;

public class BeanObtenerCampanaIn {
	private int idCotiza;
	private String binTarjeta;
	private String canalVta;
	private int longitudTarjeta;
	private String proveedor;
	public BeanObtenerCampanaIn() {
		super();
	}
	
	public BeanObtenerCampanaIn(int idCotiza, String binTarjeta,
			String canalVta, int longitudTarjeta) {
		super();
		this.idCotiza = idCotiza;
		this.binTarjeta = binTarjeta;
		this.canalVta = canalVta;
		this.longitudTarjeta = longitudTarjeta;
		this.proveedor = proveedor;
	}
	/**
	 * @return the canalVta
	 */
	public String getCanalVta() {
		return canalVta;
	}

	/**
	 * @param canalVta the canalVta to set
	 */
	public void setCanalVta(String canalVta) {
		this.canalVta = canalVta;
	}

	/**
	 * @return the idCotiza
	 */
	public int getIdCotiza() {
		return idCotiza;
	}
	/**
	 * @param idCotiza the idCotiza to set
	 */
	public void setIdCotiza(int idCotiza) {
		this.idCotiza = idCotiza;
	}
	/**
	 * @return the binTarjeta
	 */
	public String getBinTarjeta() {
		return binTarjeta;
	}
	/**
	 * @param binTarjeta the binTarjeta to set
	 */
	public void setBinTarjeta(String binTarjeta) {
		this.binTarjeta = binTarjeta;
	}

	
	/**
	 * @return the longitudTarjeta
	 */
	public int getLongitudTarjeta() {
		return longitudTarjeta;
	}


	/**
	 * @param longitudTarjeta the longitudTarjeta to set
	 */
	public void setLongitudTarjeta(int longitudTarjeta) {
		this.longitudTarjeta = longitudTarjeta;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	@Override
	public String toString() {
		return "BeanObtenerCampanaIn [idCotiza=" + idCotiza + ", binTarjeta="
				+ binTarjeta + ", canalVta=" + canalVta + ", longitudTarjeta="
				+ longitudTarjeta + ", proveedor=" + proveedor + "]";
	}
	/*public String toString() {
		return "BeanObtenerCampanaIn [idCotiza=" + idCotiza + ", binTarjeta="
				+ binTarjeta + ", canalVta=" + canalVta + ", longitudTarjeta="
				+ longitudTarjeta + "]";
	}*/
}
