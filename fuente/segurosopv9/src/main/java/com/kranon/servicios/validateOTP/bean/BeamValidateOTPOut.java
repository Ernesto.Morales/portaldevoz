package com.kranon.servicios.validateOTP.bean;

public class BeamValidateOTPOut {
	private String codeReturn;

	/**
	 * @return the codeReturn
	 */
	public String getCodeReturn() {
		return codeReturn;
	}

	/**
	 * @param codeReturn the codeReturn to set
	 */
	public void setCodeReturn(String codeReturn) {
		this.codeReturn = codeReturn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeamValidateOTPOut [codeReturn=" + codeReturn + "]";
	}
	
}
