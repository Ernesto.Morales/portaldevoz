package com.kranon.servicios.saveOTP.bean;

public class BeanOutSaveOTP {
	private String codError;
	private String descError;
	public BeanOutSaveOTP(String codError, String descError) {
		super();
		this.codError = codError;
		this.descError = descError;
	}
	public BeanOutSaveOTP() {
		super();
	}
	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}
	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanOutSaveOTP [codError=" + codError + "]";
	}
	
}
