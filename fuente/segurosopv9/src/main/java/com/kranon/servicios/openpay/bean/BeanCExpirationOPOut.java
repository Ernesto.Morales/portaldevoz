package com.kranon.servicios.openpay.bean;

public class BeanCExpirationOPOut {
	private Boolean allowsCharges;
	private Boolean allowsPayouts;
	
	public BeanCExpirationOPOut() {
		super();
	}

	public BeanCExpirationOPOut(Boolean allowsCharges, Boolean allowsPayouts) {
		super();
		this.allowsCharges = allowsCharges;
		this.allowsPayouts = allowsPayouts;
	}

	/**
	 * @return the allowsCharges
	 */
	public Boolean getAllowsCharges() {
		return allowsCharges;
	}

	/**
	 * @param allowsCharges the allowsCharges to set
	 */
	public void setAllowsCharges(Boolean allowsCharges) {
		this.allowsCharges = allowsCharges;
	}

	/**
	 * @return the allowsPayouts
	 */
	public Boolean getAllowsPayouts() {
		return allowsPayouts;
	}

	/**
	 * @param allowsPayouts the allowsPayouts to set
	 */
	public void setAllowsPayouts(Boolean allowsPayouts) {
		this.allowsPayouts = allowsPayouts;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCExpirationOPOut [allowsCharges=" + allowsCharges
				+ ", allowsPayouts=" + allowsPayouts + "]";
	}
	
	
}
