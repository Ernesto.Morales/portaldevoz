package com.kranon.servicios.grantingticket.bean;

import java.util.List;

/**
 * Datos de Respuesta de Autenticacion
 *
 */
public class AuthenticationResponse {

	private String authenticationState;
	private List<AuthenticationData> authenticationData;

	public String getAuthenticationState() {
		return authenticationState;
	}

	public void setAuthenticationState(String authenticationState) {
		this.authenticationState = authenticationState;
	}

	public List<AuthenticationData> getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(List<AuthenticationData> authenticationData) {
		this.authenticationData = authenticationData;
	}

	@Override
	public String toString() {
		return String.format("AuthenticationResponse [authenticationState=%s, authenticationData=%s]", authenticationState, authenticationData);
	}

}
