package com.kranon.servicios.openpay.bean;

public class BeanCardOPOut {
	private String type;
	private String brand;
	private String number;
	private String holderName;
	private BeanCExpirationOP expiration;
	private BeanBankOP bank;
	private Boolean usePoints;
	private String pointsType;
	public BeanCardOPOut() {
		super();
	}
	public BeanCardOPOut(String type, String brand, String number,
			String holderName, BeanCExpirationOP expiration, BeanBankOP bank,
			Boolean usePoints, String pointsType) {
		super();
		this.type = type;
		this.brand = brand;
		this.number = number;
		this.holderName = holderName;
		this.expiration = expiration;
		this.bank = bank;
		this.usePoints = usePoints;
		this.pointsType = pointsType;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}
	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	/**
	 * @return the expiration
	 */
	public BeanCExpirationOP getExpiration() {
		return expiration;
	}
	/**
	 * @param expiration the expiration to set
	 */
	public void setExpiration(BeanCExpirationOP expiration) {
		this.expiration = expiration;
	}
	/**
	 * @return the bank
	 */
	public BeanBankOP getBank() {
		return bank;
	}
	/**
	 * @param bank the bank to set
	 */
	public void setBank(BeanBankOP bank) {
		this.bank = bank;
	}
	/**
	 * @return the usePoints
	 */
	public Boolean getUsePoints() {
		return usePoints;
	}
	/**
	 * @param usePoints the usePoints to set
	 */
	public void setUsePoints(Boolean usePoints) {
		this.usePoints = usePoints;
	}
	/**
	 * @return the pointsType
	 */
	public String getPointsType() {
		return pointsType;
	}
	/**
	 * @param pointsType the pointsType to set
	 */
	public void setPointsType(String pointsType) {
		this.pointsType = pointsType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCardOPOut [type=" + type + ", brand=" + brand + ", number="
				+ number + ", holderName=" + holderName + ", expiration="
				+ expiration + ", bank=" + bank + ", usePoints=" + usePoints
				+ ", pointsType=" + pointsType + "]";
	}
	
	
}
