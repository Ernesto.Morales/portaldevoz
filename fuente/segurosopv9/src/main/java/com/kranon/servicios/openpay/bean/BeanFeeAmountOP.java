package com.kranon.servicios.openpay.bean;

public class BeanFeeAmountOP {
	private String amount;
	private String currency;
	private BeanTaxOP tax;
	
	public BeanFeeAmountOP() {
		super();
	}

	public BeanFeeAmountOP(String amount, String currency, BeanTaxOP tax) {
		super();
		this.amount = amount;
		this.currency = currency;
		this.tax = tax;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the tax
	 */
	public BeanTaxOP getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(BeanTaxOP tax) {
		this.tax = tax;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanFeeAmountOP [amount=" + amount + ", currency=" + currency
				+ ", tax=" + tax + "]";
	}
	
}
