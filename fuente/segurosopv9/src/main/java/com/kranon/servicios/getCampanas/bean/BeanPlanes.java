package com.kranon.servicios.getCampanas.bean;

public class BeanPlanes {
	private static final String varSplitBy = "_";
	private String valor;
	private String descripcion;
	private String valormenu;
	private String mes;
	private String mesinicio;
	
	public BeanPlanes() {
		super();
	}

	public BeanPlanes(String valor, String descripcion) {
		super();
		this.valor = valor;
		this.descripcion = descripcion;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * @return the valor
	 */
	public String getValor2() {
		String aux="";
		aux=this.mesinicio+"#"+this.mes;
		return aux;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		
		this.valor = valor;
	}

	public void setValor(){
		if(this.valor != null && !this.valor.equals("")){
			if(valor.contains("-")){
				String[]nuevas=this.valor.split("-");
				if(nuevas.length>1){
					this.mesinicio=nuevas[0];
					this.mes=nuevas[1];
				}
			}else{
				this.mes=valor;
			}
			
		} else {
			this.mes=valor;
		}

	}
	
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	/**
	 * @return the valormenu
	 */
	public String getValormenu() {
		this.valormenu=UtilidadesCampanas.convertNumber(mes);
		return valormenu;
	}

	/**
	 * @param valormenu the valormenu to set
	 */
	public void setValormenu(String valormenu) {
		this.valormenu = valormenu;
	}

	
	/**
	 * @return the mes
	 */
	public String getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}

	/**
	 * @return the mesinicio
	 */
	public String getMesinicio() {
		return mesinicio;
	}

	/**
	 * @param mesinicio the mesinicio to set
	 */
	public void setMesinicio(String mesinicio) {
		this.mesinicio = mesinicio;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanPlanes [valor=" + valor + ", descripcion=" + descripcion
				+ "]";
	}
	
	
}
