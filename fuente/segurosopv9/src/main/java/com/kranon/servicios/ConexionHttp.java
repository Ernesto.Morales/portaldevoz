package com.kranon.servicios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.utilidades.UtilidadesBeans;
import javax.net.ssl.HttpsURLConnection;

/**
 * Metodos para consumir un servicio REST con operaciones GET/POST/PUT
 *
 */
public class ConexionHttp {

	private CommonLoggerProcess log;

	public ConexionHttp(String idInvocacion, String idElemento) {
		if (this.log == null) {
			this.log = new CommonLoggerProcess("WEB_SERVICES");
		}
		/** INICIALIZAR LOG **/
		this.log.inicializar(idInvocacion, idElemento);
	}

	public ConexionResponse executeGet(String url, int timeout, HashMap<String, String> header, HashMap<String, String> paramsUrl) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : UtilidadesBeans.cifrarTsec(header.toString(),10)));
		parametrosEntrada.add(new ParamEvent("paramsUrl", (paramsUrl == null ? "null" : paramsUrl.toString())));
		this.log.initModuleProcess("CONEXION_HTTP_GET", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {
			String urlEnvio = url;
			if (paramsUrl != null) {
				urlEnvio = urlEnvio + "?";
				Iterator<Entry<String, String>> it = paramsUrl.entrySet().iterator();
				while (it.hasNext()) {
					Entry<String, String> e = (Map.Entry<String, String>) it.next();
					urlEnvio = urlEnvio + e.getKey() + "=" + e.getValue();
					if (it.hasNext()) {
						urlEnvio = urlEnvio + "&";
					}
				}
			}

			URL urlServicio = new URL(urlEnvio);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setRequestMethod("GET");

			// Configuracion
			conn.setRequestProperty("Accept", "application/json");

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio("");

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				String mensajeError = "";
				if (conn.getErrorStream() != null) {
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(mensajeError);
				} else {
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(null);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeError() != null) {

					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeError());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeError());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeError(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta() != null) {
					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeRespuesta());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeRespuesta());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeRespuesta(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}

			}

		} catch (Exception e) {
			// Posibles errores: Read timed out

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_GET", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());

			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_GET", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executePost(String url, int timeout, String body, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
//		parametrosEntrada.add(new ParamEvent("body", body));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_POST", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {

			URL urlServicio = new URL(url);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(timeout);

			// Configuracion
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Content-enconding", "chunked");
			conn.setRequestProperty("Accept", "application/json");

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio(body);

			// Realizo la conexion
			os = new OutputStreamWriter(conn.getOutputStream());
			os.write(body);
			os.flush();

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				String mensajeError = "";
				if (conn.getErrorStream() != null) {
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
				}
				conexionResponse.setResultado("KO");
				conexionResponse.setMensajeError(mensajeError);
				conexionResponse.setMensajeRespuesta("");

				// ABALFARO_20170522
				if (conexionResponse.getMensajeError() != null) {

					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeError());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeError());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeError(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();

				if (map != null) {
					for (Map.Entry<String, List<String>> entry : map.entrySet()) {
						if (conexionResponse.getHeaderRespuesta() == null) {
							conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
						}
						conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
					}
				}
			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta() !=null) {
					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeRespuesta());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeRespuesta());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeRespuesta(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_POST", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());
			conexionResponse.setMensajeRespuesta("");

			// throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_POST", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executePut(String url, int timeout, String body, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("body", body));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_PUT", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {

			URL urlServicio = new URL(url);

			// Abro la conexion
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");

			// Configuracion
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Content-enconding", "chunked");
			conn.setRequestProperty("Accept", "application/json");

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio(body);

			// Realizo la conexion
			os = new OutputStreamWriter(conn.getOutputStream());
			os.write(body);
			os.flush();

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
				String mensajeError = brError.readLine();

				conexionResponse.setResultado("KO");
				conexionResponse.setMensajeError(mensajeError);

				// ABALFARO_20170522
				if (conexionResponse.getMensajeError() != null) {

					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeError());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeError());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeError(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta()!= null) {
					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeRespuesta());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeRespuesta());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeRespuesta(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "NEW", parametrosAdicionales);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_PUT", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("KO");
			conexionResponse.setMensajeError(e.getMessage());

			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_PUT", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executeDelete(String url, int timeout, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_DELETE", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpsURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {
			
			URL urlServicio = new URL(url);
			
			conn = (HttpsURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setRequestMethod("DELETE");
			// conn.setRequestProperty("Accept", "application/json");
			// si se quieren manipular datos ASCII
			//	conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			
			// Configuracion
			// conn.setRequestProperty("Accept", "application/json");
						

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio("");

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);	

			if (responseCode != 200 && responseCode != 204) {
				// Resultado KO
				String mensajeError = "";
				if (conn.getErrorStream() != null) {
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(mensajeError);
				} else {
					conexionResponse.setResultado("KO");
					conexionResponse.setMensajeError(null);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta() != null) {

					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeError());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeError());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeError(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522
				
				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}

			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta() != null) {
					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeRespuesta());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeRespuesta());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeRespuesta(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}

			}

		} catch (Exception e) {
			// Posibles errores: Read timed out

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_DELETE", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());

			throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_DELETE", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}

	public ConexionResponse executePostHttp(String url, int timeout, String body, HashMap<String, String> header) throws Exception {

		/** INICIO EVENTO - INICIO DE MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("url", url));
		parametrosEntrada.add(new ParamEvent("body", body));
		parametrosEntrada.add(new ParamEvent("header", header == null ? "null" : header.toString()));
		this.log.initModuleProcess("CONEXION_HTTP_POST", parametrosEntrada);
		/** FIN EVENTO - INICIO DE MODULO **/

		HttpURLConnection conn = null;
		BufferedReader br = null;
		BufferedReader brError = null;
		OutputStreamWriter os = null;

		ConexionResponse conexionResponse = new ConexionResponse();

		try {

			URL urlServicio = new URL(url);

			// Abro la conexion
			conn = (HttpURLConnection) urlServicio.openConnection();
			conn.setReadTimeout(timeout);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(timeout);

			// Configuracion
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Content-enconding", "chunked");
			conn.setRequestProperty("Accept", "application/json");

			// Adjunto los header
			if (header != null) {
				Iterator<Entry<String, String>> it = header.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> e = (Map.Entry<String, String>) it.next();
					conn.addRequestProperty(e.getKey(), e.getValue());
				}
			}

			conexionResponse.setHeaderEnvio(header);
			conexionResponse.setMensajeEnvio(body);

			// Realizo la conexion
			os = new OutputStreamWriter(conn.getOutputStream());
			os.write(body);
			os.flush();

			// Obtengo el codigo de la respuesta
			int responseCode = conn.getResponseCode();
			conexionResponse.setCodigoRespuesta(responseCode);

			if (responseCode != 200) {
				// Resultado KO
				String mensajeError = "";
				if (conn.getErrorStream() != null) {
					brError = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
					mensajeError = brError.readLine();
				}
				conexionResponse.setResultado("KO");
				conexionResponse.setMensajeError(mensajeError);
				conexionResponse.setMensajeRespuesta("");

				// ABALFARO_20170522
				if (conexionResponse.getMensajeError() != null) {

					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeError());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeError());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeError(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("mensajeError", conexionResponse.getMensajeError()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();

				if (map != null) {
					for (Map.Entry<String, List<String>> entry : map.entrySet()) {
						if (conexionResponse.getHeaderRespuesta() == null) {
							conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
						}
						conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
					}
				}
			} else {
				// Resultado OK
				conexionResponse.setResultado("OK");
				conexionResponse.setMensajeError("");
				conexionResponse.setMensajeRespuesta("");

				// Recojo la respuesta
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String output;
				while ((output = br.readLine()) != null) {
					conexionResponse.setMensajeRespuesta(conexionResponse.getMensajeRespuesta() + output);
				}

				// ABALFARO_20170522
				if (conexionResponse.getMensajeRespuesta() !=null) {
					// ** CHEQUEA JSON
					boolean checkJsonCompatibility = UtilidadesBeans.checkJsonCompatibility(conexionResponse.getMensajeRespuesta());
					/** INICIO EVENTO - ACCION **/
					this.log.actionEvent("CHECK_JSON", checkJsonCompatibility + "", null);
					/** FIN EVENTO - ACCION **/

					// ** FORMATEA JSON
					String[] formatea = UtilidadesBeans.formateaJson(conexionResponse.getMensajeRespuesta());
					if (formatea[0].equals("true")) {
						// se ha formateado el json
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramOld = new ArrayList<ParamEvent>();
						paramOld.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "OLD", paramOld);
						/** FIN EVENTO - ACCION **/

						conexionResponse.setMensajeRespuesta(formatea[1]);

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> paramNew = new ArrayList<ParamEvent>();
						paramNew.add(new ParamEvent("outputJson", conexionResponse.getMensajeRespuesta()));
						this.log.actionEvent("FORMAT_JSON", "NEW", paramNew);
						/** FIN EVENTO - ACCION **/
					}
				}
				// ABALFARO_20170522

				// Recojo los header de la respuesta
				Map<String, List<String>> map = conn.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					if (conexionResponse.getHeaderRespuesta() == null) {
						conexionResponse.setHeaderRespuesta(new HashMap<String, List<String>>());
					}
					conexionResponse.getHeaderRespuesta().put(entry.getKey(), entry.getValue());
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "CONEXION_HTTP_POST", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());
			conexionResponse.setMensajeRespuesta("");

			// throw e;
		} finally {
			try {
				if (conn != null) {
					conn.disconnect();
				}
				if (os != null) {
					os.close();
				}
				if (br != null) {
					br.close();
				}
				if (brError != null) {
					brError.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse.toString()));
			this.log.endModuleProcess("CONEXION_HTTP_POST", conexionResponse.getResultado(), parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}
		return conexionResponse;
	}
//	public static void main(String[] args) throws Exception {
//		
//		ConexionHttp con = new ConexionHttp("", "");	
//		
//		String url = "http://150.250.140.226:7500/TechArchitecture/mx/grantingTicket/V02/";
//		int timeout = 10000; 
//		HashMap<String, String> header = new HashMap<String, String>();
//		String tsec = "eJxt1MmOq1YQBuBXueot6mbGIPVticlmMDNm2kTAOcxgzGTM08eRkiiL7P8qqVT6v29/hvkvFfz+gCRWpNmJ+aRxAD8pmqI+uYzAP3EWkqDIc8hl4ONXAKe5vg+/P/A/sI+fb7EeKzhJ6ZL+PDJboAw/uuAr9nKcSJ6NKCWTwk4OoU0IjR2P67oZJ45jWULo0VaLqz6UQA1C3xqI9nL3b13uY+zCMRJfq+7Yr7pCjSWLTqD3Wc1TNTMnk5dSH4OKA5Oxoc1zt9PVPiNsxOEXAetqSPTe9tBR2SXVJDr5dxCx4Z6K/Qi6pGF84Fq+HszostwYnIHk3C6GvehPZZvVqjOKIuO56cxFF8clgE1iaVqvlelHDPJyTPLGWoyoDjoThGbm2Ubos6OkM/zzQLpyq7WpPALTzZyTLTRhkdmQKBXdxZ5rTy+R6ygBvkojU+iwFP2T89wZb9W0rbiflUr0XBUVkbjNh1wqF0+L46NyGCnta22fR6zFXzahiOQSpTCNn9OcxOe+4mt+FNnhvN6X6WEkfUC5oqy49j3BF4Mdk50JDtyI97vcsgzjXZO9WodwczZ9FemqPxOFp20k0/PM3k4szneUO+byc9+uWXNXU4hr1DrVMYiB7FtZoilWGlqAkd07T0DhvoTCPi5HcyWGi5hT7VQbCzqUgjw0jcwZt3ht3DyJIkJoXf69AvF2RXEZhtBtly1cC3VYtUl9XzYcYvFbYIvKIew0hghNEiCno61EJdktNBZy6egQQeqgTx+GTGSpd4wL5QgJpA7xNauW8xjacq6ZKSdN51YBPpKsMmQvwYRYtgQXZAuXzO5cmAQ03b0/4dpl9+AVmaFuZJrNw37CywSCtdtv03lN62y69+gGuUeilmc8wbQuTt2n2QvqMdEktR0PDakdJCsgsuG346ZFUMFD++nJoKaORhwv6bCFIXrr9/raKA/cpNM04Bl2EImTMzlgfLHbQ3RvhGq23V3G4ni9GAUYmXAmL5g9wbZ0rg1+SNaTkQpbWFRdrEb6Kkce1TVF8lB1NSNsh+Xe5XwfwWAiJQMsCo8BoYZhVHBP9K2DOsUEfYJop245V4xX1eBS1RXEEjyCwZ4fpmMG/lJZz7p62ikhikE+m0Y0oMZF8BTuvLv3ZY2EKhEMvbHz1qCSzVTdq6EV02MNcY617VxnT1XheMJlPZ8Bp7dDMhk8pjnkhYvriLimncD43QO2eX5iqqGZXnl9e3fJMlW6BY7lIHW5Ra2ZogQbEAc93cqZ9obO0a+MU9pczi9BbUl5exwraH9/o/+B52+EdPjiuzqdf7o5y7blC8CvHu5feV1M6dfyVu4r79Ic/2f03/i3POTTa1zeqvFdeZ/qpep/JNmDAKKiIKK2Lnq0nQJQD+U3+n/hb/QvRH/+BBY4xX0=";
//		header.put("tsec", tsec);

//		
//		
//	}

}


