package com.kranon.servicios.openpay.bean;

public class BeanInstallmentPlanOP {
	private String paymentsNumber;
	
	public BeanInstallmentPlanOP() {
		super();
	}

	public BeanInstallmentPlanOP(String paymentsNumber) {
		super();
		this.paymentsNumber = paymentsNumber;
	}

	/**
	 * @return the paymentsNumber
	 */
	public String getPaymentsNumber() {
		return paymentsNumber;
	}

	/**
	 * @param paymentsNumber the paymentsNumber to set
	 */
	public void setPaymentsNumber(String paymentsNumber) {
		this.paymentsNumber = paymentsNumber;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanInstallmentPlanOP [paymentsNumber=" + paymentsNumber
				+ "]";
	}
	
}
