package com.kranon.servicios.openpay.bean;

import javax.persistence.criteria.CriteriaBuilder.In;

public class BeanCExpirationOP {
	private int year;
	private int month;
	
	public BeanCExpirationOP() {
		super();
	}
	public BeanCExpirationOP(int year, int month) {
		super();
		this.year = year;
		this.month = month;
	}
	
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCExpirationOP [year=" + year + ", month=" + month + "]";
	}
	
	
}
