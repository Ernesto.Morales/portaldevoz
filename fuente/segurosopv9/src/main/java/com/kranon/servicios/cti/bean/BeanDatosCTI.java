package com.kranon.servicios.cti.bean;

/**
 * Bean con los Datos de CTI
 * 
 * @author abalfaro
 *
 */
public class BeanDatosCTI {

	
	/**
	 * Atributos de la cadena CTI. Estos atributos se encuentran en el mismo orden que los campos descritos en la cadena de envio de datos del cti.
	 * No cambiar el orden de declaracion de los atributos debido a que el parseo de campos de la cadena hacia el bean se hace por auto reflexion
	 * parseando la cadena de entrada con cada atribuo de la clase en el orden previsto
	 */
	
	private String ani;
	private String dnis;
	private String numeroIvr;
	private String puertoIvr;
	private String llave;
	private String field; //temporal
	private String vdnOrigen;
	private String vdnDestino;
	private String skill;
	private String asa;
	private String idAplicacion;
	private String xferId;
	private String segmentoCliente;
	private String producto;
	private String llaveAccesoFront;
	private String campania;
	private String canal;
	private String numCliente;
	private String sector;
	private String nombreCliente;
	private String telefonoContacto;
	private String telefonoDinamico;
	private String libre1;	//comentario
	private String libre2;	//metodoAutenticacion
	private String usuarioFront;
	private String transferenciaAsesor;
	private String transferenciaIvr;
	private String numeroSesion;
	private String tiempoLlamada;
	private String llaveSegmento;
	private String ipAsesor;
	private String libre3;
	private String telCtePersonas;
	private String metodoAutenticacion;
	private String libre4;
	private String timestamp;
	private String comentario;


	public String getAni() {
		return ani;
	}

	public void setAni(String ani) {
		this.ani = ani;
	}

	public String getDnis() {
		return dnis;
	}

	public void setDnis(String dnis) {
		this.dnis = dnis;
	}

	public String getNumeroIvr() {
		return numeroIvr;
	}

	public void setNumeroIvr(String numeroIvr) {
		this.numeroIvr = numeroIvr;
	}

	public String getPuertoIvr() {
		return puertoIvr;
	}

	public void setPuertoIvr(String puertoIvr) {
		this.puertoIvr = puertoIvr;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getVdnOrigen() {
		return vdnOrigen;
	}

	public void setVdnOrigen(String vdnOrigen) {
		this.vdnOrigen = vdnOrigen;
	}

	public String getVdnDestino() {
		return vdnDestino;
	}

	public void setVdnDestino(String vdnDestino) {
		this.vdnDestino = vdnDestino;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getAsa() {
		return asa;
	}

	public void setAsa(String asa) {
		this.asa = asa;
	}

	public String getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getXferId() {
		return xferId;
	}

	public void setXferId(String xferId) {
		this.xferId = xferId;
	}

	public String getSegmentoCliente() {
		return segmentoCliente;
	}

	public void setSegmentoCliente(String segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getLlaveAccesoFront() {
		return llaveAccesoFront;
	}

	public void setLlaveAccesoFront(String llaveAccesoFront) {
		this.llaveAccesoFront = llaveAccesoFront;
	}

	public String getCampania() {
		return campania;
	}

	public void setCampania(String campania) {
		this.campania = campania;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getNumCliente() {
		return numCliente;
	}

	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	public String getTelefonoDinamico() {
		return telefonoDinamico;
	}

	public void setTelefonoDinamico(String telefonoDinamico) {
		this.telefonoDinamico = telefonoDinamico;
	}

	public String getLibre1() {
		return libre1;
	}

	public void setLibre1(String libre1) {
		this.libre1 = libre1;
	}

	public String getLibre2() {
		return libre2;
	}

	public void setLibre2(String libre2) {
		this.libre2 = libre2;
	}

	public String getUsuarioFront() {
		return usuarioFront;
	}

	public void setUsuarioFront(String usuarioFront) {
		this.usuarioFront = usuarioFront;
	}

	public String getTransferenciaAsesor() {
		return transferenciaAsesor;
	}

	public void setTransferenciaAsesor(String transferenciaAsesor) {
		this.transferenciaAsesor = transferenciaAsesor;
	}

	public String getTransferenciaIvr() {
		return transferenciaIvr;
	}

	public void setTransferenciaIvr(String transferenciaIvr) {
		this.transferenciaIvr = transferenciaIvr;
	}

	public String getNumeroSesion() {
		return numeroSesion;
	}

	public void setNumeroSesion(String numeroSesion) {
		this.numeroSesion = numeroSesion;
	}

	public String getTiempoLlamada() {
		return tiempoLlamada;
	}

	public void setTiempoLlamada(String tiempoLlamada) {
		this.tiempoLlamada = tiempoLlamada;
	}

	public String getLlaveSegmento() {
		return llaveSegmento;
	}

	public void setLlaveSegmento(String llaveSegmento) {
		this.llaveSegmento = llaveSegmento;
	}

	public String getIpAsesor() {
		return ipAsesor;
	}

	public void setIpAsesor(String ipAsesor) {
		this.ipAsesor = ipAsesor;
	}

	public String getLibre3() {
		return libre3;
	}

	public void setLibre3(String libre3) {
		this.libre3 = libre3;
	}

	public String getTelCtePersonas() {
		return telCtePersonas;
	}

	public void setTelCtePersonas(String telCtePersonas) {
		this.telCtePersonas = telCtePersonas;
	}

	public String getMetodoAutenticacion() {
		return metodoAutenticacion;
	}

	public void setMetodoAutenticacion(String metodoAutenticacion) {
		this.metodoAutenticacion = metodoAutenticacion;
	}

	public String getLibre4() {
		return libre4;
	}

	public void setLibre4(String libre4) {
		this.libre4 = libre4;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}


	@Override
	public String toString() {
		return String.format("BeanDatosCTI [ani=%s, dnis=%s, numeroIvr=%s, puertoIvr=%s, llave=%s, field=%s, vdnOrigen=%s, vdnDestino=%s, skill=%s, asa=%s, idAplicacion=%s,"
				+ " xferId=%s, segmentoCliente=%s, producto=%s, llaveAccesoFront=%s, campania=%s, canal=%s, numCliente=%s, sector=%s, nombreCliente=%s, telefonoContacto=%s, "
				+ "telefonoDinamico=%s, libre1=%s, libre2=%s, usuarioFront=%s, transferenciaAsesor=%s, transferenciaIvr=%s, numeroSesion=%s, tiempoLlamada=%s, "
				+ "llaveSegmento=%s, ipAsesor=%s, libre3=%s, telCtePersonas=%s, metodoAutenticacion=%s, libre4=%s, timestamp=%s, comentario=%s]",
				ani, dnis, numeroIvr, puertoIvr, llave, field, vdnOrigen, vdnDestino, skill, asa, idAplicacion, xferId, segmentoCliente, producto, llaveAccesoFront, 
				campania, canal, numCliente, sector, nombreCliente, telefonoContacto, telefonoDinamico, libre1, libre2, usuarioFront, transferenciaAsesor, 
				transferenciaIvr, numeroSesion, tiempoLlamada, llaveSegmento, ipAsesor, libre3, telCtePersonas, metodoAutenticacion, libre4, timestamp, comentario);
	}

	/**
	 * Construye la cadena cti a partir de los atributos del objeto
	 * 
	 * @return {@link String} cadena cti a guardar
	 */
	public String construyeCadena(String caracterRelleno) {
		String resultado="";
		String esp=new String(new char[612]);
		esp=esp.replace("\0", " ");
		
		caracterRelleno = (caracterRelleno == null) ? "#" : caracterRelleno;

		// para cada campo, si es null o vaco almaceno #
		resultado += (ani== null || ani.equals(""))? (caracterRelleno + "|") : ani + "|";
		resultado += (dnis== null || dnis.equals(""))? (caracterRelleno + "|") : dnis + "|";
		resultado += (numeroIvr== null || numeroIvr.equals(""))? (caracterRelleno + "|") : numeroIvr + "|";
		resultado += (puertoIvr== null || puertoIvr.equals(""))? (caracterRelleno + "|") : puertoIvr + "|";
		resultado += (llave== null || llave.equals(""))? (caracterRelleno + "|") : llave + "|";
		resultado += (field== null || field.equals(""))? (caracterRelleno + "|") : field + "|"; //temporal
		resultado += (vdnOrigen== null || vdnOrigen.equals(""))? (caracterRelleno + "|") : vdnOrigen + "|";
		resultado += (vdnDestino== null || vdnDestino.equals(""))? (caracterRelleno + "|") : vdnDestino + "|";
		resultado += (skill== null || skill.equals(""))? (caracterRelleno + "|") : skill + "|";
		resultado += (asa== null || asa.equals(""))? (caracterRelleno + "|") : asa + "|";
		resultado += (idAplicacion== null || idAplicacion.equals(""))? (caracterRelleno + "|") : idAplicacion + "|";
		resultado += (xferId== null || xferId.equals(""))? (caracterRelleno + "|") : xferId + "|";
		resultado += (segmentoCliente== null || segmentoCliente.equals(""))? (caracterRelleno + "|") : segmentoCliente + "|";
		resultado += (producto== null || producto.equals(""))? (caracterRelleno + "|") : producto + "|";
		resultado += (llaveAccesoFront== null || llaveAccesoFront.equals(""))? (caracterRelleno + "|") : llaveAccesoFront + "|";
		resultado += (campania== null || campania.equals(""))? (caracterRelleno + "|") : campania + "|";
		resultado += (canal== null || canal.equals(""))? (caracterRelleno + "|") : canal + "|";
		resultado += (numCliente== null || numCliente.equals(""))? (caracterRelleno + "|") : numCliente + "|";
		resultado += (sector== null || sector.equals(""))? (caracterRelleno + "|") : sector + "|";
		resultado += (nombreCliente== null || nombreCliente.equals(""))? (caracterRelleno + "|") : nombreCliente + "|";
		resultado += (telefonoContacto== null || telefonoContacto.equals(""))? (caracterRelleno + "|") : telefonoContacto + "|";
		resultado += (telefonoDinamico== null || telefonoDinamico.equals(""))? (caracterRelleno + "|") : telefonoDinamico + "|";
		resultado += (libre1== null || libre1.equals(""))? (caracterRelleno + "|") : libre1 + "|";
		resultado += (libre2== null || libre2.equals(""))? (caracterRelleno + "|") : libre2 + "|";
		resultado += (usuarioFront== null || usuarioFront.equals(""))? (caracterRelleno + "|") : usuarioFront + "|";
		resultado += (transferenciaAsesor== null || transferenciaAsesor.equals(""))? (caracterRelleno + "|") : transferenciaAsesor + "|";
		resultado += (transferenciaIvr== null || transferenciaIvr.equals(""))? (caracterRelleno + "|") : transferenciaIvr + "|";
		resultado += (numeroSesion== null || numeroSesion.equals(""))? (caracterRelleno + "|") : numeroSesion + "|";
		resultado += (tiempoLlamada== null || tiempoLlamada.equals(""))? (caracterRelleno + "|") : tiempoLlamada + "|";
		resultado += (llaveSegmento== null || llaveSegmento.equals(""))? (caracterRelleno + "|") : llaveSegmento + "|";
		resultado += (ipAsesor== null || ipAsesor.equals(""))? (caracterRelleno + "|") : ipAsesor + "|";
		resultado += (libre3== null || libre3.equals(""))? (caracterRelleno + "|") : libre3 + "|";
		resultado += (telCtePersonas== null || telCtePersonas.equals(""))? (caracterRelleno + "|") : telCtePersonas + "|";
		resultado += (metodoAutenticacion== null || metodoAutenticacion.equals(""))? (caracterRelleno + "|") : metodoAutenticacion + "|";
		resultado += (libre4== null || libre4.equals(""))? (caracterRelleno + "|") : libre4 + "|";
		resultado += (timestamp== null || timestamp.equals(""))? (caracterRelleno + "|") : timestamp + "|";
		resultado += (comentario== null || comentario.equals(""))? caracterRelleno : comentario;		
		
		resultado = resultado + esp;
		resultado = resultado.substring(0,612);
		
		return resultado;
	}

	/**
	 * Parsea la cadena CTI al objeto 
	 * 
	 * @param {@link String} cadena cti
	 * @return {@link BeanDatosCTI} objeto relleno con la cadena cti
	 */
	public static BeanDatosCTI parseaCadena(String cadena) {

		BeanDatosCTI bean = null;
		try{
			if (cadena != null && !cadena.equals("")) {

				String[] elemento = cadena.split("\\|");
				bean = new BeanDatosCTI();

				for(int i=0;i<elemento.length;i++){
					BeanDatosCTI.class.getDeclaredFields()[i].set(bean,(elemento[i]==null || elemento[i].equals("#"))?null:elemento[i] );
				}
			}
		}catch(Exception e){

		}
		return bean;
	}

}
