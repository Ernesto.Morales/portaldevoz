package com.kranon.servicios.openpay.bean;

public class BeanTaxOP {
	private String amount;
	private String currency;
	
	public BeanTaxOP() {
		super();
	}

	public BeanTaxOP(String amount, String currency) {
		super();
		this.amount = amount;
		this.currency = currency;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanTaxOP [amount=" + amount + ", currency=" + currency + "]";
	}
		
}
