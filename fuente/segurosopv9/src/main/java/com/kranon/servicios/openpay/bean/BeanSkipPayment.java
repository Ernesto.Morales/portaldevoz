package com.kranon.servicios.openpay.bean;

public class BeanSkipPayment {
	
private String deferredMonthsNumber;
	
	public BeanSkipPayment() {
		super();
	}

	public BeanSkipPayment(String deferredMonthsNumber) {
		super();
		this.deferredMonthsNumber = deferredMonthsNumber;
	}

	/**
	 * @return the paymentsNumber
	 */
	public String getDeferredMonthsNumber() {
		return deferredMonthsNumber;
	}

	/**
	 * @param paymentsNumber the paymentsNumber to set
	 */
	public void setDeferredMonthsNumber(String deferredMonthsNumber) {
		this.deferredMonthsNumber = deferredMonthsNumber;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanSkipPayment [deferredMonthsNumber=" + deferredMonthsNumber
				+ "]";
	}

}
