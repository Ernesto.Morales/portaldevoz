package com.kranon.servicios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.servicios.bean.ErrorResponse;
import com.kranon.servicios.bean.ResponseWarning;

public class UtilidadesResponse {

	private CommonLoggerProcess log;

	public UtilidadesResponse(CommonLoggerProcess log) {
		this.log = log;
	}

	/**
	 * Devuelve el codigo y descripcion de ResponseWarning de un header de
	 * respuesta dado
	 * 
	 * @param HashMap
	 *            <String, List<String>> headerResponse
	 * @return {@link ResponseWarning}
	 */
	public ResponseWarning dameResponseWarning(HashMap<String, List<String>> headerResponse) {
		boolean encontrado = false;
		ResponseWarning responseWarning = new ResponseWarning();
		if (headerResponse != null) {
			Iterator<Entry<String, List<String>>> it = headerResponse.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, List<String>> e = (Map.Entry<String, List<String>>) it.next();
				if (e.getKey() != null && (e.getKey().equalsIgnoreCase("ResponseWarningCode"))) {
					responseWarning.setCode(e.getValue().get(0));
					encontrado = true;
				} else if (e.getKey() != null && (e.getKey().equalsIgnoreCase("ResponseWarningDescription"))) {
					responseWarning.setDescription(e.getValue().get(0));
					encontrado = true;
				}
			}
		}

		if (!encontrado) {
			// no he encontrado ningun valor de response warning en la cabecera,
			// devuelvo null
			return null;
		}
		return responseWarning;
	}

	/**
	 * Parsea la cadena JSON de salida de la peticion del servicio y la
	 * convierte en un objeto {@link ErrorResponse}
	 * 
	 * @param outputJson
	 *            cadena JSON recibida en la peticion
	 * @return {@link .ErrorResponse} objeto resultado
	 */
	public ErrorResponse parserError(String outputJson) {
		ErrorResponse errorResponse = null;
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);

			if (outputJson != null) {
				errorResponse = mapper.readValue(outputJson, ErrorResponse.class);
			}

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputJson", outputJson == null ? "null" : outputJson));
			this.log.actionEvent("PARSEAR_ERROR_RESPONSE", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

		} catch (Exception e) {
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputJson", outputJson == null ? "null" : outputJson));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("PARSEAR_ERROR_RESPONSE", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			

		}
		return errorResponse;
	}

}
