package com.kranon.servicios.openpay.bean;

import java.util.List;

public class BeanOwnerAccountOPOut {
	private String name;
	private List<BeanContactOP> contacts;
	private String lastName;
	private String secondLastName;
	
	public BeanOwnerAccountOPOut() {
		super();
	}

	
	public BeanOwnerAccountOPOut(String name, List<BeanContactOP> contacts,
			String lastName,String secondLastName) {
		super();
		this.name = name;
		this.contacts = contacts;
		this.lastName = lastName;
		this.secondLastName=secondLastName;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contacts
	 */
	public List<BeanContactOP> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(List<BeanContactOP> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	/**
	 * @return the secondLastName
	 */
	public String getSecondLastName() {
		return secondLastName;
	}


	/**
	 * @param secondLastName the secondLastName to set
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[name=" + name + ", contacts=" + contacts
				+ ", lastName=" + lastName + ", secondLastName="
				+ secondLastName + "]";
	}
	
	
}
