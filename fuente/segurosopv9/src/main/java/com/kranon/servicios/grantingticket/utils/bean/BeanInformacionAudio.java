package com.kranon.servicios.grantingticket.utils.bean;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;

/**
 * Informacion relacionada con el audio audio: <p>
 * - <b>tipoArchivo:</b> formato del fichero de audio <p>
 * - <b>tamanoBytes:</b> numero de bytes del fichero de audio <p>
 * - <b>tamanoFrames:</b> numero de frames del fichero de audio <p>
 * - <b>codificacion:</b> tipo de codificacion ({@link AudioFormat.Encoding}) <p>
 * - <b>canales:</b> numero de canales <p>
 * - <b>frecuencia:</b> frecuencia del audio <p>
 * - <b>numBitsMuestra:</b> numero de bits que tiene cada muestra <p>
 * - <b>frameRate:</b> numero de frames en cada muestra <p>
 * - <b>frameSize:</b> tamanyo del frame <p>
 * - <b>esBigEndian:</b> indica el orden en el que estan los bits (bigEndian) <p>
 * - <b>duracion:</b> segundos que dura el audio <p>
 */
public class BeanInformacionAudio {

	private AudioFileFormat.Type tipoArchivo;
	private int tamanoBytes;
	private int tamanoFrames;
	private AudioFormat.Encoding codificacion;
	private int canales;
	private float frecuencia;
	private float numBitsMuestra;
	private float frameRate;
	private int frameSize;
	private boolean esBigEndian;
	private float duracion;
	
	/**
	 * Devuelve el formato del fichero de audio
	 * @return Formato
	 */
	public AudioFileFormat.Type getTipoArchivo() {
		return tipoArchivo;
	}
	
	/**
	 * Almacena el formato del fichero de audio
	 * @param tipoArchivo Formato del fichero
	 */
	public void setTipoArchivo(AudioFileFormat.Type tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
	/**
	 * Devuelve el numero de bytes del fichero de audio
	 * @return Tamanyo audio
	 */
	public int getTamanoBytes() {
		return tamanoBytes;
	}
	
	/**
	 * Almacena el numero de bytes del fichero de audio
	 * @param tamanoBytes Tamanyo
	 */
	public void setTamanoBytes(int tamanoBytes) {
		this.tamanoBytes = tamanoBytes;
	}
	
	/**
	 * Devuelve el numero de frames del fichero de audio
	 * @return Numero de Frames
	 */
	public int getTamanoFrames() {
		return tamanoFrames;
	}
	
	/**
	 * Almacena el numero de frames del fichero de audio
	 * @param tamanoFrames Numero de frames
	 */
	public void setTamanoFrames(int tamanoFrames) {
		this.tamanoFrames = tamanoFrames;
	}
	
	/**
	 * Devuelve el tipo de codificacion
	 * @return Codificacion
	 */
	public AudioFormat.Encoding getCodificacion() {
		return codificacion;
	}
	
	/**
	 * Alamacena el tipo de codificacion
	 * @param codificacion Codificacion
	 */
	public void setCodificacion(AudioFormat.Encoding codificacion) {
		this.codificacion = codificacion;
	}
	
	/**
	 * Devuelve el numero de canales
	 * @return Canales
	 */
	public int getCanales() {
		return canales;
	}
	
	/**
	 * Almacena el numero de canales
	 * @param canales Numero canales
	 */
	public void setCanales(int canales) {
		this.canales = canales;
	}
	
	/**
	 * Devuelve la frecuencia del audio
	 * @return Frecuencia
	 */
	public float getFrecuencia() {
		return frecuencia;
	}
	
	/**
	 * Almacena la frecuencia del audio
	 * @param frecuencia Frecuencia
	 */
	public void setFrecuencia(float frecuencia) {
		this.frecuencia = frecuencia;
	}
	
	/**
	 * Devuelve el numero de bits que tiene cada muestra
	 * @return Numero bits
	 */
	public float getNumBitsMuestra() {
		return numBitsMuestra;
	}
	
	/**
	 * Almacena el numero de bits que tiene cada muestra
	 * @param numBitsMuestra Numero bits
	 */
	public void setNumBitsMuestra(float numBitsMuestra) {
		this.numBitsMuestra = numBitsMuestra;
	}
	
	/**
	 * Devuelve el numero de frames en cada muestra
	 * @return Numero frames
	 */
	public float getFrameRate() {
		return frameRate;
	}
	
	/**
	 * Almacena el numero de frames en cada muestra
	 * @param frameRate Numero frames
	 */
	public void setFrameRate(float frameRate) {
		this.frameRate = frameRate;
	}
	
	/**
	 * Devuelve el tamanyo del frame
	 * @return Tamanyo frame
	 */
	public int getFrameSize() {
		return frameSize;
	}
	
	/**
	 * Almacena el tamanyo del frame
	 * @param frameSize Tamanyo frame
	 */
	public void setFrameSize(int frameSize) {
		this.frameSize = frameSize;
	}
	
	/**
	 * Indica el orden en el que estan los bits (bigEndian)
	 * @return <p>
	 * 		TRUE si es bigEndian <p>
	 * 		FALSE si es littleEndian 
	 */
	public boolean getEsBigEndian() {
		return esBigEndian;
	}
	
	/**
	 * Almacena el orden en el que estan los bits
	 * @param esBigEndian Orden bits (true=bigEndian, false=littleEndian)
	 */
	public void setEsBigEndian(boolean esBigEndian) {
		this.esBigEndian = esBigEndian;
	}
	
	/**
	 * Devuelve los segundos que dura el audio
	 * @return Duracion
	 */
	public float getDuracion() {
		return duracion;
	}
	
	/**
	 * Almacena los segundos que dura el audio
	 * @param duracion Duracion
	 */
	public void setDuracion(float duracion) {
		this.duracion = duracion;
	}
	
	
	@Override
	public String toString() {
		return "BeanInformacionAudio [tipoArchivo=" + tipoArchivo
				+ ", codificacion=" + codificacion + ", canales=" + canales
				+ ", frecuencia=" + frecuencia + ", numBitsMuestra="
				+ numBitsMuestra + ", frameRate=" + frameRate + ", frameSize="
				+ frameSize + ", esBigEndian=" + esBigEndian + ", duracion="
				+ duracion + "]";
	}

}


