package com.kranon.servicios.saveOTP.bean;

public class BeanInSaveOTP {
	private int idCotiza;
	private String folioOtp;
	private String idSession;
	public BeanInSaveOTP(int idCotiza, String folioOtp, String idSession) {
		super();
		this.idCotiza = idCotiza;
		this.folioOtp = folioOtp;
		this.idSession = idSession;
	}
	public BeanInSaveOTP() {
		super();
	}
	/**
	 * @return the idCotiza
	 */
	public int getIdCotiza() {
		return idCotiza;
	}
	/**
	 * @param idCotiza the idCotiza to set
	 */
	public void setIdCotiza(int idCotiza) {
		this.idCotiza = idCotiza;
	}
	/**
	 * @return the folioOtp
	 */
	public String getFolioOtp() {
		return folioOtp;
	}
	/**
	 * @param folioOtp the folioOtp to set
	 */
	public void setFolioOtp(String folioOtp) {
		this.folioOtp = folioOtp;
	}
	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}
	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanInSaveOTP [idCotiza=" + idCotiza + ", folioOtp=" + folioOtp
				+ ", idSession=" + idSession + "]";
	}
	
	
}
