package com.kranon.servicios.cti.bean;

public class ProcesoCTIOutDto {

	private BeanDatosCTI beanDatosCTI;
	private BeanDatosUUI beanDatosUUI;
	private String codigoError;
	private String resultadoOperacion;
	private String destino;	
	
	
	public BeanDatosCTI getBeanDatosCTI() {
		return beanDatosCTI;
	}
	public void setBeanDatosCTI(BeanDatosCTI beanDatosCTI) {
		this.beanDatosCTI = beanDatosCTI;
	}
	public BeanDatosUUI getBeanDatosUUI() {
		return beanDatosUUI;
	}
	public void setBeanDatosUUI(BeanDatosUUI beanDatosUUI) {
		this.beanDatosUUI = beanDatosUUI;
	}
	
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getResultadoOperacion() {
		return resultadoOperacion;
	}
	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}

	@Override
	public String toString(){
		return String.format("ProcesoCTIOutDto [beanDatosCTI=%s, beanDatosUUI=%s, codigoError=%s, destino=%s, resultadoOperacion=%s]",beanDatosCTI,  beanDatosUUI,codigoError,destino, resultadoOperacion);
	}

}
