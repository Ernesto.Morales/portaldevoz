package com.kranon.servicios.grantingticket.bean;

/**
 * Datos de Respuesta a Backend
 *
 */
public class BackendUserResponse {

	private String clientId;
	private String clientStatus;
	private String accountingTerminal;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public String getAccountingTerminal() {
		return accountingTerminal;
	}

	public void setAccountingTerminal(String accountingTerminal) {
		this.accountingTerminal = accountingTerminal;
	}

	@Override
	public String toString() {
		return String.format("BackendUserResponse [clientId=%s, clientStatus=%s, accountingTerminal=%s]", clientId, clientStatus, accountingTerminal);
	}

}
