package com.kranon.servicios.openpay.bean;

public class BeanExchangeRateOP {
	private String date;
	private String value;
	private String baseCurrency;
	private String targetCurrency;
	
	public BeanExchangeRateOP() {
		super();
	}

	public BeanExchangeRateOP(String date, String value, String baseCurrency,
			String targetCurrency) {
		super();
		this.date = date;
		this.value = value;
		this.baseCurrency = baseCurrency;
		this.targetCurrency = targetCurrency;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the baseCurrency
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * @param baseCurrency the baseCurrency to set
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * @return the targetCurrency
	 */
	public String getTargetCurrency() {
		return targetCurrency;
	}

	/**
	 * @param targetCurrency the targetCurrency to set
	 */
	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanExchangeRateOP [date=" + date + ", value=" + value
				+ ", baseCurrency=" + baseCurrency + ", targetCurrency="
				+ targetCurrency + "]";
	}
	
}
