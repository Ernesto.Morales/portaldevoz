package com.kranon.servicios.getCampanas.bean;

import java.util.List;

public class BeanCampana {
	private String opcion;
	private String tipoCampaña;
	private List<BeanPlanes> listaPlanes;
	
	public BeanCampana() {
		super();
	}


	public BeanCampana(String opcion, String tipoCampaña,
			List<BeanPlanes> listaPlanes) {
		super();
		this.opcion = opcion;
		this.tipoCampaña = tipoCampaña;
		this.listaPlanes = listaPlanes;
	}


	/**
	 * @return the opcion
	 */
	public String getOpcion() {
		return opcion;
	}

	/**
	 * @param opcion the opcion to set
	 */
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	/**
	 * @return the tipoCampaña
	 */
	public String getTipoCampaña() {
		return tipoCampaña;
	}

	/**
	 * @param tipoCampaña the tipoCampaña to set
	 */
	public void setTipoCampaña(String tipoCampaña) {
		this.tipoCampaña = tipoCampaña;
	}

	

	/**
	 * @return the listaPlanes
	 */
	public List<BeanPlanes> getListaPlanes() {
		return listaPlanes;
	}


	/**
	 * @param listaPlanes the listaPlanes to set
	 */
	public void setListaPlanes(List<BeanPlanes> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCampana [opcion=" + opcion + ", tipoCampaña=" + tipoCampaña
				+ ", listaPlanes=" + listaPlanes + "]";
	}


	
	
}
