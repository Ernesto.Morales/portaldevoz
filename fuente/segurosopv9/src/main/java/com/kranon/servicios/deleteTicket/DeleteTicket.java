package com.kranon.servicios.deleteTicket;

import java.util.ArrayList;
import java.util.HashMap;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.ConexionHttp;
import com.kranon.servicios.UtilidadesResponse;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.bean.ErrorResponse;
import com.kranon.servicios.bean.ResponseWarning;
import com.kranon.singleton.SingletonWebServices;
import com.kranon.utilidades.UtilidadesBeans;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;

/**
 * Metodos para consumir el Servicio REST de Granting Ticket cuando FINALIZA UNA LLAMADA, borrando el Ticket
 *
 */
public class DeleteTicket {
	private CommonLoggerProcess log;
	private String url;
	private int timeout;
	private HashMap<String, String> header;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String idInvocacion;
	private String idServicio;
	private String nombre;
	private String version;
	private String idWebServices = "DELETE_TICKET";
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	public DeleteTicket(String idInvocacion, String idServicio, String idElemento) {

		if (this.log == null) {
			this.log = new CommonLoggerProcess(this.idWebServices);
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idInvocacion = idInvocacion;
		this.idServicio = idServicio;
	}
	
	/**
	 * Metodo que recoge la url, la cabecera para preparar ejecutaPeticion
	 * 
	 * @param tsec
	 *            Token de seguridad devuelto por Granting Ticket
	 *
	 */

	public void preparaPeticion(String tsec) {

		String idModuloLog = "PREPARA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";
		try {
			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("tsec", UtilidadesBeans.cifrarTsec(tsec, 20)));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
//			listlogger.add(parametrosEntrada.toString());
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url y el timeout del Xml

			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}
			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}
			url = datosEntorno.getDeleteTicket().getUrl();
			timeout = beanWebServices.getTimeout();
			
			// [201704_NMB] Se obtiene el nombre y la version del WS para las trazas de monitorizacion
			nombre = datosEntorno.getDeleteTicket().getNombre();
			version = datosEntorno.getDeleteTicket().getVersion();

			// 4.- Crear el header
			header = new HashMap<String, String>();
			if (tsec != null) {
				header.put("tsec", tsec);
			}
			resultadoOperacion = "OK";
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
//			listlogger.add(parametrosAdicionales.toString());
			/** FIN EVENTO - ERROR **/

		} finally {
			// 5.- Escribir traza de Fin
//			listlogger.add(resultadoOperacion);
//			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
//			listlogger.clear();
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("", ""));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}

	}

	/**
	 * Realiza la peticion DELETE al servicio de Granting Ticket para eliminar el Ticket
	 * 
	 * @return {@link ConexionResponse} objeto con la respuesta
	 */
	public ConexionResponse ejecutaPeticion() {

		String idModuloLog = "EJECUTA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";
		// Objeto retorno de la peticion
		ConexionResponse conexionResponse = null;
		try {
			// 1.- Escribir traza de inicio de modulo

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("url", url));
			parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
			parametrosEntrada.add(new ParamEvent("header", (header == null ? "null" : UtilidadesBeans.cifrarTsec(header.toString(), 20))));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Ejecuta la peticion

			// Creo el objeto de la conexion
			ConexionHttp conexionHttp = new ConexionHttp(this.log.getIdInvocacion(), this.log.getIdElemento());
			// Realizo la conexion
			conexionResponse = conexionHttp.executeDelete(url, timeout, header);

			// 3.- Tratar la salida

			if (conexionResponse == null) {

				this.log.comment("WS_RESPONSE_IS_NULL");

				conexionResponse = new ConexionResponse();
				conexionResponse.setCodigoRespuesta(0);
				conexionResponse.setResultado("ERROR");
				conexionResponse.setMensajeError("WS_RESPONSE_IS_NULL");

				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
				this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************	

			} else {

				if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {

					// la operacion ha ido bien
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeRespuesta=" + conexionResponse.getMensajeError());
					
					// recupero warning de la respuesta
					UtilidadesResponse utils = new UtilidadesResponse(this.log);
					ResponseWarning responseWarning = utils.dameResponseWarning(conexionResponse.getHeaderRespuesta());
					conexionResponse.setResponseWarning(responseWarning);

				} else {
					// la operacion ha ido KO o ERROR
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeError=" + conexionResponse.getMensajeError());

					
					// recupero warning de la respuesta
					UtilidadesResponse utils = new UtilidadesResponse(this.log);
					ResponseWarning responseWarning = utils.dameResponseWarning(conexionResponse.getHeaderRespuesta());
					conexionResponse.setResponseWarning(responseWarning);

					
					if (conexionResponse.getResultado().equalsIgnoreCase("KO")) {
						// si el resultado es KO, es decir, hay respuesta del WS
						// pero no es 200-OK
						// parseo el mensaje de error
//						UtilidadesResponse utils = new UtilidadesResponse(this.log);
						ErrorResponse errorResponse = utils.parserError(conexionResponse.getMensajeError());
						conexionResponse.setErrorResponse(errorResponse);

						// ****************************************************************
						// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
						// ****************************************************************
						if(errorResponse != null){
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							// this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), null);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), errorResponse.getErrorCode(), errorResponse.getErrorMessage(), null);
						} else {
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
						}
					}
					else {
						this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
						this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
					}
					// ****************************************************************
					// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
					// ****************************************************************
				}
			}

			resultadoOperacion = conexionResponse.getResultado();

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			if (conexionResponse == null) {
				conexionResponse = new ConexionResponse();
			}
			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());
			conexionResponse.setCodigoRespuesta(0);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion, Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
			this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************			

		} finally {
			// 4.- Escribir la traza de fin
//			if(conexionResponse!=null){
//				listlogger.add(""+conexionResponse.getCodigoRespuesta());
//				listlogger.add(conexionResponse.getResultado());
//				listlogger.add(conexionResponse.getMensajeError());
//			}
//			else
//				listlogger.add("conexionResponse=null");
//			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
//			listlogger.clear();
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse == null ? "null" : conexionResponse.toString()));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
//			listlogger.add(idModuloLog);
//			listlogger.add(resultadoOperacion);
//			listlogger.add(parametrosSalida.toString());
//			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
//			listlogger.clear();

		}

		return conexionResponse;
	}
}
