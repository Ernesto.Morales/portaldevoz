package com.kranon.servicios.openpay.bean;

public class BeanContactOP {
	private String contactType;
	private String contact;
	
	public BeanContactOP() {
		super();
	}
	public BeanContactOP(String contactType, String contact) {
		super();
		this.contactType = contactType;
		this.contact = contact;
	}
	/**
	 * @return the contactType
	 */
	public String getContactType() {
		return contactType;
	}
	/**
	 * @param contactType the contactType to set
	 */
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[contactType=" + contactType + ", contact="
				+ contact + "]";
	}
	
	
}
