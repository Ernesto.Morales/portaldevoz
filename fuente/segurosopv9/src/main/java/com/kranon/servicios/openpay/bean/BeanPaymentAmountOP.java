package com.kranon.servicios.openpay.bean;

import java.math.BigDecimal;

public class BeanPaymentAmountOP {
	private String currency;
	private BigDecimal amount;
	
	
	public BeanPaymentAmountOP() {
		super();
	}


	public BeanPaymentAmountOP(String currency, BigDecimal amount) {
		super();
		this.currency = currency;
		this.amount = amount;
	}


	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}


	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[currency=" + currency + ", amount="
				+ amount + "]";
	}
	
	
	
}
