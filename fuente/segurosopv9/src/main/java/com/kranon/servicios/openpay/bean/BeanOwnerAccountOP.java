package com.kranon.servicios.openpay.bean;

import java.util.List;

public class BeanOwnerAccountOP {
	private String name;
	private List<BeanContactOP> contacts;
	private String lastName;
	
	public BeanOwnerAccountOP() {
		super();
	}

	
	public BeanOwnerAccountOP(String name, List<BeanContactOP> contacts,
			String lastName) {
		super();
		this.name = name;
		this.contacts = contacts;
		this.lastName = lastName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contacts
	 */
	public List<BeanContactOP> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(List<BeanContactOP> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[name=" + name + ", contacts=" + contacts
				+ ", lastName=" + lastName  + "]";
	}
	
	
}
