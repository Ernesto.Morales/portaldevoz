package com.kranon.servicios.getCampanas.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.kranon.bean.BeanGramatica;
import com.kranon.bean.menu.BeanConfigOpcDtmfMenu;
import com.kranon.bean.menu.BeanConfigOpcMenu;
import com.kranon.bean.menu.BeanOpcionMenu;
import com.kranon.servicios.getCampanas.ObtenerCampana;

public class UtilidadesCampanas {
	private static final String PUNTOS = "MIXED";
	private static final String UNO = "ONE";
	private static final String varSplitBy = "_";
	
	
	private static final String PAGO_CON_PUNTOS="PAGO_CON_PUNTOS";
	private static final String PAGO_CON_PUNTOSA="MIXED";
	private static final String TTS="TTS";
	private static final String GRAMAR="builtin:dtmf/number?length=";
	private static final String TEXPAGO_UNICO="Pago en una sola exhibición marca";
	private static final String BULTING="BULTING";
	private static final String MARCA=",marca";
	private static final String PAGO="Pago";
	private static final String TEX_PUNTOS="pago con puntos";

	
	private static String mesNumero(String mes){
		String[] variablesMenuArray;
		if(mes != null && !mes.equals("")){
			variablesMenuArray = mes.split(varSplitBy);
		} else {
			variablesMenuArray = new String[0];
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yy MMMMM dd");	
		Calendar calendar = new GregorianCalendar(13,10,28);	
		return mes;
		
		
	}
	    private static final String[] UNIDADES = { "NULL", "ONE", "TWO", "THREE",
	            "FOUR", "FIVE", "SIX", "SIEVEN", "EIGHT", "NINE", "TEN ",
	            "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FITFTEEN", "SIXTEEN",
	            "SEVENTEEN", "EIGHTEEN", "NINETEEN", "TWENTY" };

	    private static final String[] DECENAS = { "TWENTY ", "thirty ", "forty ",
	            "fifty ", "sixty ", "seventy ", "eighty ", "ninety ",
	            "CIEN " };

	    private static final String[] CENTENAS = { "one hundred ", "DOSCIENTOS ",
	            "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ",
	            "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS " };
	    /**
	     * Convierte los trios de numeros que componen las unidades, las decenas y
	     * las centenas del numero.
	     * @param number Numero a convetir en digitos
	     * @return Numero convertido en letras
	     */
	    static String convertNumber(String number) {

	        if (number.length() > 3)
	            throw new NumberFormatException(
	                    "La longitud maxima debe ser 3 digitos");

	        // Caso especial con el 100
	        if (number.equals("100")) {
	            return "CIEN ";
	        }

	        StringBuilder output = new StringBuilder();
	        if (getDigitAt(number, 2) != 0)
	            output.append(CENTENAS[getDigitAt(number, 2) - 1]);

	        int k = Integer.parseInt(String.valueOf(getDigitAt(number, 1))
	                + String.valueOf(getDigitAt(number, 0)));

	        if (k <= 20)
	            output.append(UNIDADES[k]);
	        else if (k > 30 && getDigitAt(number, 0) != 0)
	            output.append(DECENAS[getDigitAt(number, 1) - 2])
	                    .append("_ ")
	                    .append(UNIDADES[getDigitAt(number, 0)]);
	        else
	            output.append(DECENAS[getDigitAt(number, 1) - 2])
	                    .append(UNIDADES[getDigitAt(number, 0)]);

	        return output.toString();
	    }

	    /**
	     * Retorna el digito numerico en la posicion indicada de derecha a izquierda
	     * @param origin Cadena en la cual se busca el digito
	     * @param position Posicion de derecha a izquierda a retornar
	     * @return Digito ubicado en la posicion indicada
	     */
	    private static int getDigitAt(String origin, int position) {
	        if (origin.length() > position && position >= 0)
	            return origin.charAt(origin.length() - position - 1) - 48;
	        return 0;
	    }
	    public static String parserEntrada(BeanCampanas beanCampanas) throws JsonProcessingException {

			// Convierto el objeto JSON
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
			String inputJson = mapper.writeValueAsString(beanCampanas);

			return inputJson;
		}
	    /**
		 * Parsea la cadena JSON de salida de la peticion del servicio
		 * 
		 * @param outputJson
		 *            cadena JSON recibida en la peticion
		 * @return {@link BeanObtenerCampanaOut} objeto parseado
		 */
		public static BeanCampanas parserSalida(String outputJson) throws Exception {

			BeanCampanas beanCampanas = null;

			try {
				ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
				mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);

				if (outputJson != null) {
					beanCampanas = mapper.readValue(outputJson,BeanCampanas.class);
				} else {
					beanCampanas = null;
				}


			} catch (Exception e) {
				beanCampanas = null;
				/** INICIO EVENTO - ACCION **/
				/** FIN EVENTO - ACCION **/
				throw e;
			}

			return beanCampanas;
		}
//	public static void main(String[] args) throws Exception {
//		 int aux=1;
//	}
	
}
