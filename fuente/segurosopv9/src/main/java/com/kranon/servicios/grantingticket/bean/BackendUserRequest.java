package com.kranon.servicios.grantingticket.bean;

import com.kranon.utilidades.UtilidadesBeans;

/**
 * Datos de Acceso a Backend
 *
 */
public class BackendUserRequest {

	private String userId;
	private String accessCode;
	private String dialogId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getDialogId() {
		return dialogId;
	}

	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}

	@Override
	public String toString() {
		return toStringCifrado();
//		return "BackendUserRequest [userId=" + userId + ", accessCode="
//				+ accessCode + ", dialogId=" + dialogId + "]";
	}
	
	
	public String toStringCifrado() {
		return "BackendUserRequest [userId=" + userId + ", accessCode="
		+ UtilidadesBeans.cifrar(accessCode,5) + ", dialogId=" + dialogId + "]";
	}

}
