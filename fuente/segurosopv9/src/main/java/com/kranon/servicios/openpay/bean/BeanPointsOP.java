package com.kranon.servicios.openpay.bean;

public class BeanPointsOP {
	 private String used;
	 private String remaining;
	 private BeanEquivalentAmount equivalentAmount;
	 
	public BeanPointsOP() {
		super();
	}

	

	public BeanPointsOP(String used, String remaining,
			BeanEquivalentAmount equivalentAmount) {
		super();
		this.used = used;
		this.remaining = remaining;
		this.equivalentAmount = equivalentAmount;
	}



	/**
	 * @return the used
	 */
	public String getUsed() {
		return used;
	}

	/**
	 * @param used the used to set
	 */
	public void setUsed(String used) {
		this.used = used;
	}

	/**
	 * @return the remaining
	 */
	public String getRemaining() {
		return remaining;
	}

	/**
	 * @param remaining the remaining to set
	 */
	public void setRemaining(String remaining) {
		this.remaining = remaining;
	}



	/**
	 * @return the equivalentAmount
	 */
	public BeanEquivalentAmount getEquivalentAmount() {
		return equivalentAmount;
	}



	/**
	 * @param equivalentAmount the equivalentAmount to set
	 */
	public void setEquivalentAmount(BeanEquivalentAmount equivalentAmount) {
		this.equivalentAmount = equivalentAmount;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanPointsOP [used=" + used + ", remaining=" + remaining
				+ ", equivalentAmount=" + equivalentAmount + "]";
	}

	
}
