package com.kranon.servicios.openpay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.cotizacion.bean.BeanCotizacion;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.ConexionHttp;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.openpay.bean.BeanCExpirationOP;
import com.kranon.servicios.openpay.bean.BeanCardOpenPay;
import com.kranon.servicios.openpay.bean.BeanContactOP;
import com.kranon.servicios.openpay.bean.BeanInstallmentPlanOP;
import com.kranon.servicios.openpay.bean.BeanOpenPayIn;
import com.kranon.servicios.openpay.bean.BeanOpenPayout;
import com.kranon.servicios.openpay.bean.BeanOwnerAccountOP;
import com.kranon.servicios.openpay.bean.BeanPaymentAmountOP;
import com.kranon.servicios.openpay.bean.BeanSkipPayment;
import com.kranon.servicios.getCampanas.bean.BeanPlanes;
import com.kranon.singleton.SingletonWebServices;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

public class OpenPay {
		private static String PUNTOS="MIXED";
		private static String PAGO_UNICO="PAGOUNICO";
		private String idInvocacion;
		private CommonLoggerProcess log;
		private String url;
		private int timeout;
		private HashMap<String, String> paramsUrl;
		private String paramsUrlCodificado;
		private HashMap<String, String> header;
		private String sessionGlobal;
		private String tsecGlobal;
		private String operationType = "OPENPAY";

		// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
		private CommonLoggerMonitoring logWarning;
		private String idServicio;
		private String nombre;
		private String version;
		private String idWebServices = "OpenPay";
		private String inputJsonCreate;
		private String inputJsonUpdate;
		private String inputJsonCodificado;
		private String numTarjeta;
		// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
		private CommonLoggerKranon logKranon;
		private List<String> listlogger = new ArrayList<String>();

		public OpenPay(String idInvocacion, String idServicio,
				String idElemento) {
			System.out.println("ENTRANDO A OPENPAY?????????????:");
			if (this.log == null) {
				this.log = new CommonLoggerProcess("WEB_SERVICES");
			}
			this.log.inicializar(idInvocacion, idElemento);
			this.idServicio = idServicio;
			this.idInvocacion = idInvocacion;
			logKranon = new CommonLoggerKranon("KRANON-LOG", idInvocacion);
			// listlogger.add("viometricss");
		}

		// * Metodo que prepara el JSON de entrada al Servicio de
		// GetAuthenticationFactor con los parametros de entrada
		public void preparaPeticion(String tsec, BeanCotizacion cotizacion, String card, String ano, String mes, String ccv,String campana) {
			String idModuloLog = "PREPARA".concat("_").concat(this.idWebServices);
			String resultadoOperacion = "KO";
			String service = "";
			
			BeanPlanes plan = new BeanPlanes();
			String mes1 = plan.getMesinicio();
			//System.out.println("VALOR DE CAMPANA EN OPENPAY??????????: " + campana);
			
			//MODIFICACIÓN BMR(04042018	)
			this.tsecGlobal = tsec;
			//TERMINA MODIFICACIÓN BMR(04042018	)
			listlogger.add(UtilidadesLoggerKranon.envia + "OpenPay");
			try {
				// 1.- Escribir traza de inicio de modulo
				/** INICIO EVENTO - INICIO MODULO **/
				// solo mostramos en la traza los ultimos 5 digitos
				ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
				parametrosEntrada.add(new ParamEvent("BeanCotizacion", cotizacion.toString()));
				this.log.initModuleProcess(idModuloLog, parametrosEntrada);
				// listlogger.add(parametrosEntrada.toString());
				/** FIN EVENTO - INICIO MODULO **/

				// 2.- Leer la url y el timeout del Xml
				/** UNMARSHALL XML DE SERVICIOS WEB **/
				BeanWebServices beanWebServices = null;
				SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
				if (instanceWebServices != null) {
					beanWebServices = instanceWebServices.getWebServices(idServicio);
				}
				if (beanWebServices == null) {
					// error al recuperar los datos del webservice
					throw new Exception("WS_NULL");
				}
				/** Recupero el entorno **/
				String entorno = "";
				try {
					entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
				} catch (PropiedadNoEncontradaExcepcion e) {
					// en caso de error, no queremos que ocurra en produccion
					entorno = "pr";
				}

				BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
				if (datosEntorno == null) {
					// no hay datos para este entorno
					throw new Exception("DATOS_NULL");
				}
				this.url = datosEntorno.getApiOpenPay().getUrl();
				this.timeout = beanWebServices.getTimeout();
				// [201704_NMB] Se obtiene el nombre y la version del WS para las
				// trazas de monitorizacion
				nombre = datosEntorno.getApiOpenPay().getNombre();
				version = datosEntorno.getApiOpenPay().getVersion();

				// Recupero parametros fijos
//				String segment = datosEntorno.getApiOpenPay().getValueParametro("segment");
//
//				service = datosEntorno.getApiOpenPay().getValueParametro("service");
//
//				String individualType = datosEntorno.getApiOpenPay().getValueParametro("individualType");
//
//				/** INICIO EVENTO - ACCION **/
//				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
//				parametrosAdicionales.add(new ParamEvent("segment", segment));
//				parametrosAdicionales.add(new ParamEvent("service", service));
//				parametrosAdicionales.add(new ParamEvent("individualType",individualType));
//				this.log.actionEvent("GET_PARAMS_FIJOS", "OK",parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				// 3.- Construyendo el JSON de entrada
				//MODIFICACIÓN BMR(04042018)
				BeanOpenPayIn beanOpenPayIn = new BeanOpenPayIn();
				beanOpenPayIn.setAfiliationId(cotizacion.getAfiliacionId());
				//beanOpenPayIn.setAfiliationId("ECOMMER");
				String name=cotizacion.getNombreCliente()+cotizacion.getApellidosCliente();
				BeanCExpirationOP beanCExpirationOP= new BeanCExpirationOP(Integer.valueOf(ano),Integer.valueOf(mes));
				beanOpenPayIn.setCard(new BeanCardOpenPay(card,beanCExpirationOP, name, ccv));
				if(campana.equals(PUNTOS)){
					beanOpenPayIn.setCardPointsUse(campana);
					beanOpenPayIn.setInstallmentPlan(null);
				}else if(campana.equals(PAGO_UNICO)){
					beanOpenPayIn.setCardPointsUse(null);
					beanOpenPayIn.setInstallmentPlan(null);
				}else if(campana.equals("SKIP_PAYMENT")){
					beanOpenPayIn.setCardPointsUse(null);
					//BeanSkipPayment skipPayment = new BeanSkipPayment();
					//skipPayment.equals(mes1);
					beanOpenPayIn.setSkipPayment(new BeanSkipPayment(mes1));
				}
				else{
					beanOpenPayIn.setCardPointsUse(null);
					beanOpenPayIn.setInstallmentPlan(new BeanInstallmentPlanOP(campana));
				}
//				beanOpenPayIn.setCardPointsUse(null);
				
				BeanContactOP beanContactOP2=new BeanContactOP("EMAIL",cotizacion.getCorreo());
				BeanContactOP beanContactOP=new BeanContactOP("MOBILE",cotizacion.getTelefono());
				List<BeanContactOP> contacts=new ArrayList<BeanContactOP>();
				contacts.add(beanContactOP2);
				contacts.add(beanContactOP);
				BeanOwnerAccountOP beanOwnerAccountOP= new BeanOwnerAccountOP(cotizacion.getNombreCliente(), contacts, cotizacion.getApellidosCliente());
				beanOpenPayIn.setOwnerAccount(beanOwnerAccountOP);
				BigDecimal montoB = new BigDecimal(cotizacion.getMontoCobro());
				BeanPaymentAmountOP beanPaymentAmountOP=new BeanPaymentAmountOP(cotizacion.getMonedaCobro(),montoB);
				beanOpenPayIn.setPaymentAmount(beanPaymentAmountOP);
				beanOpenPayIn.setQuotationId(cotizacion.getIdCotiza());
				beanOpenPayIn.setDescription(cotizacion.getDescripcion());
				beanOpenPayIn.setSessionId(cotizacion.getIdSession());
				
				
				inputJsonCreate = this.parserEntrada(beanOpenPayIn);
				//TERMINA MODIFICACIÓN BMR(04042018	)
				
				
				// Creamos un params codificado para no mostrar datos sensibles
				inputJsonCodificado = inputJsonCreate;
				// inputJsonCodificado = inputJsonCodificado.replaceAll(contrat,
				// UtilidadesBeans.cifrar(contrat,5));
				inputJsonCodificado =inputJsonCodificado.replace(card,UtilidadesBeans.cifrar(card,4));
				inputJsonCodificado =inputJsonCodificado.replace(ano,UtilidadesBeans.cifrar(ano,1));
				inputJsonCodificado =inputJsonCodificado.replace(ccv,UtilidadesBeans.cifrar(ccv,1));
				inputJsonCodificado =inputJsonCodificado.replace(mes,UtilidadesBeans.cifrar(mes,1));
				
				listlogger.add("entrada"+inputJsonCodificado);
				// 4.- Crear el header
				header = new HashMap<String, String>();
				if (tsec != null) {
					header.put("tsec", tsec);
				}

				resultadoOperacion = "OK";
			} catch (Exception e) {
				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales,
						e);
				/** FIN EVENTO - ERROR **/

			} finally {
				/** INICIO EVENTO - FIN MODULO **/
				ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
				parametrosSalida.add(new ParamEvent("paramsUrl",paramsUrlCodificado));
				this.log.endModuleProcess(idModuloLog, resultadoOperacion,parametrosSalida, null);
				logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
				listlogger.clear();
				/** FIN EVENTO - FIN MODULO **/
			}

		}
		/**
		 * Realiza la peticion POST al servicio de getElectronicTelephoneANI
		 * 
		 * @return {@link ConexionResponse} objeto con la respuesta
		 */
		public ConexionResponse ejecutaPeticion() {
			String idModuloLog = "EJECUTA".concat("_").concat(this.idWebServices);
			String resultadoOperacion = "KO";
			// Objeto retorno de la peticion
			ConexionResponse conexionResponse = null;
			try {
				// 1.- Escribir traza de inicio de modulo
				/** INICIO EVENTO - INICIO MODULO **/
				ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
				parametrosEntrada.add(new ParamEvent("url", url));
				parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
				parametrosEntrada.add(new ParamEvent("paramsUrl",(paramsUrl == null ? "null" : paramsUrlCodificado)));
				parametrosEntrada.add(new ParamEvent("header",(header == null ? "null" : header.toString())));
				this.log.initModuleProcess(idModuloLog, parametrosEntrada);
				/** FIN EVENTO - INICIO MODULO **/

				// 2.- Ejecuta la peticion

				// Creo el objeto de la conexion
				ConexionHttp conexionHttp = new ConexionHttp(this.log.getIdInvocacion(), this.log.getIdElemento());
				// Realizo la conexion
				conexionResponse = conexionHttp.executePost(url, timeout,inputJsonCreate, header);

				// 3.- Si el tsec ha caducado se pide otro y se vuelve a lanzar la
				// peticion
				// Tratar el caso de error
				// conexionResponse.setTsecValido(tsec);

				// 4.- Tratar la salida
				if (conexionResponse == null) {
					this.log.comment("WS_RESPONSE_IS_NULL");
					conexionResponse.setCodigoRespuesta(0);
					conexionResponse.setResultado("ERROR");
					conexionResponse.setMensajeError("WS_RESPONSE_IS_NULL");
					
					// ****************************************************************
					// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE
					// MONITORIZACION
					// ****************************************************************
					this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
					this.logWarning.warningWS(this.idServicio, this.version,this.nombre,String.valueOf(conexionResponse.getCodigoRespuesta()),null);
					// ****************************************************************
					// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
					// ****************************************************************

				} else {
					if (conexionResponse.getResultado().equals("OK")) {
						// la operacion ha ido bien
						this.log.comment("resultado="+ conexionResponse.getResultado());
						this.log.comment("codigoHttp="+ conexionResponse.getCodigoRespuesta());
						this.log.comment("mensajeRespuesta="+ conexionResponse.getMensajeRespuesta());
					} else {
						// la operacion ha ido KO o ERROR
						this.log.comment("resultado="+ conexionResponse.getResultado());
						this.log.comment("codigoHttp="+ conexionResponse.getCodigoRespuesta());
						this.log.comment("mensajeError="+ conexionResponse.getMensajeError());
					}
				}

				resultadoOperacion = conexionResponse.getResultado();

			} catch (Exception e) {
				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales,e);
				/** FIN EVENTO - ERROR **/

				if (conexionResponse == null) {
					conexionResponse = new ConexionResponse();
				}
				conexionResponse.setResultado("ERROR");
				conexionResponse.setMensajeError(e.getMessage());
				conexionResponse.setCodigoRespuesta(0);

				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
				this.logWarning.warningWS(this.idServicio, this.version, this.nombre,String.valueOf(conexionResponse.getCodigoRespuesta()), null);
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************

			} finally {
				if (conexionResponse != null) {
					listlogger.add("" + conexionResponse.getCodigoRespuesta());
					listlogger.add(conexionResponse.getResultado());
					listlogger.add(conexionResponse.getMensajeError());
				} else
					listlogger.add("conexionResponse=null");
					logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
					listlogger.clear();
				/** INICIO EVENTO - FIN MODULO **/
				ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
				parametrosSalida.add(new ParamEvent("conexionResponse",conexionResponse == null ? "null" : conexionResponse.toString()));
				this.log.endModuleProcess(idModuloLog, resultadoOperacion,parametrosSalida, null);
				/** FIN EVENTO - FIN MODULO **/
				//
				// listlogger.add(resultadoOperacion);
				// listlogger.add(parametrosSalida.toString());
				// logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
				// listlogger.clear();
			}

			return conexionResponse;
		}

		
		/**
		 * Parsea el objeto de entrada a una cadena JSON, SIN incluir el wrapper
		 * 
		 * @param {@link GetElectronicTelephoneANIRestInDto} objecto a convertir a
		 *        JSON sin el objeto wrapper
		 * @return cadena JSON
		 * @throws JsonProcessingException
		 */
		private String parserEntrada(BeanOpenPayIn beanOpenPayIn) throws JsonProcessingException {

			// Convierto el objeto JSON
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
			String inputJson = mapper.writeValueAsString(beanOpenPayIn);

			return inputJson;
		}

		/**
		 * Parsea la cadena JSON de salida de la peticion del servicio
		 * 
		 * @param outputJson
		 *            cadena JSON recibida en la peticion
		 * @return {@link GetElectronicTelephoneANIRestOutDto} objeto parseado
		 */
		public BeanOpenPayout parserSalida(String outputJson) throws Exception {

			BeanOpenPayout beanOpenPayout = null;

			try {
				ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
				mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,true);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);

				if (outputJson != null) {
					beanOpenPayout = mapper.readValue(outputJson,BeanOpenPayout.class);
				} else {
					beanOpenPayout = null;
				}
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("OPENPAY",beanOpenPayout == null ? "null": beanOpenPayout.toString()));
				this.log.actionEvent("PARSEAR_SALIDA", "OK", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

			} catch (Exception e) {
				beanOpenPayout = null;
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("outputJson",outputJson == null ? "null" : outputJson));
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				this.log.actionEvent("PARSEAR_SALIDA", "KO", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				throw e;
			}

			return beanOpenPayout;
		}

		/**
		 * Parsea el objeto de salida a una cadena JSON
		 * 
		 * @param authenticationFactorRestOutDto
		 *            objeto de tipo {@link AuthenticationFactorRestOutDto} a
		 *            convertir a JSON
		 * @return cadena JSON
		 * @throws JsonProcessingException
		 */
		private String salida2Json(BeanOpenPayout beanOpenPayout) throws JsonProcessingException {

			// Convierto el objeto JSON
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
			String outputJson = mapper.writeValueAsString(beanOpenPayout);

			return outputJson;
		}

}
