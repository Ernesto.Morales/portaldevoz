package com.kranon.servicios.openpay.bean;

public class BeanCardOpenPay {
	 private String number;
	 private BeanCExpirationOP expiration;
	 private String holderName;
	 private String cvv2;
	 
	public BeanCardOpenPay() {
		super();
	}
	public BeanCardOpenPay(String number, BeanCExpirationOP expiration,
			String holderName, String cvv2) {
		super();
		this.number = number;
		this.expiration = expiration;
		this.holderName = holderName;
		this.cvv2 = cvv2;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the expiration
	 */
	public BeanCExpirationOP getExpiration() {
		return expiration;
	}
	/**
	 * @param expiration the expiration to set
	 */
	public void setExpiration(BeanCExpirationOP expiration) {
		this.expiration = expiration;
	}
	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}
	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	/**
	 * @return the cvv2
	 */
	public String getCvv2() {
		return cvv2;
	}
	/**
	 * @param cvv2 the cvv2 to set
	 */
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCardOpenPay [number=" + number + ", expiration="
				+ expiration + ", holderName=" + holderName + ", cvv2=" + cvv2
				+ "]";
	}
}
