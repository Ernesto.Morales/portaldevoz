package com.kranon.servicios.getCotizacion.bean;

import com.kranon.utilidades.UtilidadesBeans;

public class BeanObtenerCotizacionOut {
	private String nombreCliente;
	private String apellidoCliente;
	private String correo;
	private String telefono;
	private String monto;
	private String moneda;
	private String codError;
	private String descProd;
	private String descError;
	
	public BeanObtenerCotizacionOut() {
		super();
	}

	public BeanObtenerCotizacionOut(String nombreCliente,
			String apellidoCliente, String correo, String telefono,
			String monto, String moneda, String codError, String descProd,
			String descError) {
		super();
		this.nombreCliente = nombreCliente;
		this.apellidoCliente = apellidoCliente;
		this.correo = correo;
		this.telefono = telefono;
		this.monto = monto;
		this.moneda = moneda;
		this.codError = codError;
		this.descProd = descProd;
		this.descError = descError;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @return the apellidoCliente
	 */
	public String getApellidoCliente() {
		return apellidoCliente;
	}

	/**
	 * @param apellidoCliente the apellidoCliente to set
	 */
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}

	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the monto
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * @param monto the monto to set
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}



	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}



	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}

	/**
	 * @return the descProd
	 */
	public String getDescProd() {
		return descProd;
	}

	/**
	 * @param descProd the descProd to set
	 */
	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanObtenerCotizacionOut [nombreCliente=" + nombreCliente + ", apellidoCliente="
				+ apellidoCliente + ", correo=" + correo + ", telefono="
				+ telefono + ", monto=" + monto + ", moneda=" + moneda
				+ ", codError=" + codError + ", descProd=" + descProd
				+ ", descError=" + descError + "]";
	}
	
	/**
	 * Crea y devuelve un {@code String} con la representaci&oacute;n completa de este bean, i.e. sus atributos
	 * con sus respectivos valores. La informaci&oacute;n sensible contenida <strong>directamente</strong> en 
	 * este objeto <strong>se entrega enmascarada</strong>. El enmascaramiento en aquella información que se 
	 * encuentra en otros <strong>objetos referenciados es delegado</strong> a dichos objetos. 
	 * @return un {@code String} con la representaci&oacute;n completa de este bean, i.e. sus atributos
	 * con sus respectivos valores. La informaci&oacute;n sensible se entrega enmascarada de acuerdo al
	 * criterio descrito anteriormente.
	 */
	public String toMaskedString(){
		return "BeanObtenerCotizacionOut [nombreCliente=" + nombreCliente + ", apellidoCliente="
		+  (apellidoCliente == null ? "null" :"*")  + 
		", correo=" + (correo == null ? "null" : UtilidadesBeans.cifrar(correo, 4)) +
		", telefono="+ (telefono == null ? "null" : UtilidadesBeans.cifrar(telefono, 4)) + 
		", monto=" + monto + 
		", moneda=" + moneda
		+ ", codError=" + codError +
		", descProd=" + descProd
		+ ", descError=" + descError + "]";
	}
	
}
