package com.kranon.servicios.validateOTP.bean;
import java.util.HashMap;
public class BeamValidateOTPIn {
	private String operationType;
	private String otp;
	private String cellphoneNumber;
	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}
	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	/**
	 * @return the otp
	 */
	public String getOtp() {
		return otp;
	}
	/**
	 * @param otp the otp to set
	 */
	public void setOtp(String otp) {
		this.otp = otp;
	}
	/**
	 * @return the cellphoneNumber
	 */
	public String getCellphoneNumber() {
		return cellphoneNumber;
	}
	/**
	 * @param cellphoneNumber the cellphoneNumber to set
	 */
	public void setCellphoneNumber(String cellphoneNumber) {
		this.cellphoneNumber = cellphoneNumber;
	}
	
	public HashMap<String, String> dameParametros() {
        HashMap<String, String> parametros = new HashMap<String, String>();
        // ejemplo peticion
        parametros.put("operationType", this.getOperationType());
        parametros.put("otp", this.getOtp());
        parametros.put("cellphoneNumber", this.getCellphoneNumber());    
        return parametros;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeamValidateOTPIn [operationType=" + operationType + ", otp="
				+ otp + ", cellphoneNumber=" + cellphoneNumber + "]";
	}
	
}
