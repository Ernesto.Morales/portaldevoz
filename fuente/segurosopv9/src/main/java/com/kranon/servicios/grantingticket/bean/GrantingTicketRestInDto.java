package com.kranon.servicios.grantingticket.bean;

/**
 * Clase de la peticion al Servicio Granting Ticket
 * Este objeto es un Wrapper del Body de la Request.
 * NO debe aparecer en el JSON de entrada
 *
 */
public class GrantingTicketRestInDto {

	private AuthenticationRequest authentication;
	private BackendUserRequest backendUserRequest;

	public AuthenticationRequest getAuthentication() {
		return authentication;
	}

	public void setAuthentication(AuthenticationRequest authentication) {
		this.authentication = authentication;
	}

	public BackendUserRequest getBackendUserRequest() {
		return backendUserRequest;
	}

	public void setBackendUserRequest(BackendUserRequest backendUserRequest) {
		this.backendUserRequest = backendUserRequest;
	}

	@Override
	public String toString() {
		return String.format("GrantingTicketRestInDto [authentication=%s, backendUserRequest=%s]", authentication, backendUserRequest);
	}

}
