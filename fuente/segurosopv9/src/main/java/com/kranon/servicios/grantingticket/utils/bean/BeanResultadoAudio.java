package com.kranon.servicios.grantingticket.utils.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * Resultado de formatear el audio: <p>
 * - <b>coResultado:</b> <i>OK</i>=todos formateados / <i>KO</i>=alguno no formateado / <i>ERROR</i>=error al ejecutar el proceso <p>
 * - <b>stMensajeError:</b> ruta incorrecta / audio no formateado / excepcion capturada <p>
 * - <b>listadoErrores:</b>  lista de los errores ({@link ErrorFormateo})  que se han dado al hacer el formeteo <p>
 */
public class BeanResultadoAudio {

	private String coResultado;
	private String stMensaje;
	private List<ErrorFormateo> listadoErrores;

	/**
	 * Devuelve el resultado del formateo
	 * @return  <p>
	 * 		OK todos formateados <p>
	 * 		KO alguno no formateado <p>
	 * 		ERROR error al ejecutar el proceso
	 */
	public String getCoResultado() {
		return coResultado;
	}

	/**
	 * Almacena el resultado del formateo
	 * @param coResultado Resultado formateo
	 */
	public void setCoResultado(String coResultado) {
		this.coResultado = coResultado;
	}

	/**
	 * Devuelve el mensaje de error
	 * @return Mensaje error
	 */
	public String getStMensaje() {
		return stMensaje;
	}

	/**
	 * Almacena el mensaje de error
	 * @param stMensaje Mensaje error
	 */
	public void setStMensaje(String stMensaje) {
		this.stMensaje = stMensaje;
	}

	/**
	 * Devuelve el listado con los errores que se han dado al formatear
	 * @return {@link List} Lista errores
	 */
	public List<ErrorFormateo> getListadoErrores() {
		if(listadoErrores == null){
			listadoErrores = new ArrayList<ErrorFormateo>();
		}
		return listadoErrores;
	}

	/**
	 * Almacena el listado de errores que se han dado al formatear
	 * @param listadoErrores {@link List}  Lista errores
	 */
	public void setListadoErrores(List<ErrorFormateo> listadoErrores) {
		this.listadoErrores = listadoErrores;
	}

	@Override
	public String toString() {
		return String.format("ResultadoOperacion [coResultado=%s, stMensaje=%s, listadoErrores=%s]", coResultado, stMensaje, listadoErrores);
	}

	/**
	 * Error al formatear un audio: <p>
	 * - <b>codigoError:</b> punto donde ha dado el error <p>
	 * - <b>nombreWav:</b> path + nombreAudio <p>
	 * - <b>mensajeError:</b> excepcion capturada
	 */
	public class ErrorFormateo{

		private String codigoError;
		private String nombrewav;
		private String mensajeError;
		
		/**
		 * Devuelve el codigo de error
		 * @return Codigo error
		 */
		public String getCodigoError() {
			return codigoError;
		}
		
		/**
		 * Almacena el codigo de error
		 * @param codigoError Codigo error
		 */
		public void setCodigoError(String codigoError) {
			this.codigoError = codigoError;
		}
		
		/**
		 * Devuelve el nombre del wav que ha dado error al formatear
		 * @return Nombre del audio
		 */
		public String getNombrewav() {
			return nombrewav;
		}
		
		/**
		 * Almacena el nombre del wav que ha dado error al formatear
		 * @param nombrewav Nombre del audio
		 */
		public void setNombrewav(String nombrewav) {
			this.nombrewav = nombrewav;
		}
		
		/**
		 * Devuelve el mensaje de error que se ha dado
		 * @return Mensaje de error
		 */
		public String getMensajeError() {
			return mensajeError;
		}
		
		/**
		 * Almacena el mensaje de error que se ha dado
		 * @param mensajeError Mensaje de error
		 */
		public void setMensajeError(String mensajeError) {
			this.mensajeError = mensajeError;
		}
		
		public String toString() {
			return String.format("ResultadoOperacion [codigoError=%s, nombrewav=%s, mensajeError=%s]", codigoError, nombrewav, mensajeError);
		}
	}

}


