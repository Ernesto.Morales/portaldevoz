package com.kranon.servicios.openpay.bean;

public class BeanOpenPayout {
	private BeanDataOutOP data;

	
	public BeanOpenPayout() {
		super();
	}

	public BeanOpenPayout(BeanDataOutOP data) {
		super();
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public BeanDataOutOP getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(BeanDataOutOP data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanOpenPayout [data=" + data + "]";
	}
	
}
