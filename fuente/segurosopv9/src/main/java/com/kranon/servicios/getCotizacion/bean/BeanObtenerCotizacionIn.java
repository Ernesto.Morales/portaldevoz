package com.kranon.servicios.getCotizacion.bean;

public class BeanObtenerCotizacionIn {
	private int idCotiza;
	public BeanObtenerCotizacionIn(int idCotiza) {
		super();
		this.idCotiza = idCotiza;
	}
	public BeanObtenerCotizacionIn() {
		super();
	}
	/**
	 * @return the idCotiza
	 */
	public int getIdCotiza() {
		return idCotiza;
	}
	/**
	 * @param idCotiza the idCotiza to set
	 */
	public void setIdCotiza(int idCotiza) {
		this.idCotiza = idCotiza;
	}
	@Override
	public String toString() {
		return "BeanObtenerCotizacionIn [idCotiza=" + idCotiza + "]";
	}
	
	
	
}
