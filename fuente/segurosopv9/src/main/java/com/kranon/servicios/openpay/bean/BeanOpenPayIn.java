package com.kranon.servicios.openpay.bean;

public class BeanOpenPayIn {
	private String afiliationId;
	private BeanCardOpenPay card;
	private String cardPointsUse;
	private BeanInstallmentPlanOP installmentPlan;
	private BeanSkipPayment skipPayment;
	private BeanOwnerAccountOP ownerAccount;
	private BeanPaymentAmountOP paymentAmount;
	private String quotationId;
	private String description;
	private String sessionId;
	
	public BeanOpenPayIn() {
		super();
	}

	public BeanOpenPayIn(String afiliationId, BeanCardOpenPay card,
			String cardPointsUse, BeanInstallmentPlanOP installmentPlan, BeanSkipPayment skipPayment,
			BeanOwnerAccountOP ownerAccount, BeanPaymentAmountOP paymentAmount,
			String quotationId, String description, String sessionId) {
		super();
		this.afiliationId = afiliationId;
		this.card = card;
		this.cardPointsUse = cardPointsUse;
		this.installmentPlan = installmentPlan;
		this.skipPayment = skipPayment;
		this.ownerAccount = ownerAccount;
		this.paymentAmount = paymentAmount;
//		TODO QUITAR PARA PRUEBAS CON COTIZACION
//		this.quotationId = quotationId;
		this.description = description;
		this.sessionId = sessionId;
	}

	/**
	 * @return the afiliationId
	 */
	public String getAfiliationId() {
		return afiliationId;
	}

	/**
	 * @param afiliationId the afiliationId to set
	 */
	public void setAfiliationId(String afiliationId) {
		this.afiliationId = afiliationId;
	}

	/**
	 * @return the card
	 */
	public BeanCardOpenPay getCard() {
		return card;
	}

	/**
	 * @param card the card to set
	 */
	public void setCard(BeanCardOpenPay card) {
		this.card = card;
	}

	/**
	 * @return the cardPointsUse
	 */
	public String getCardPointsUse() {
		return cardPointsUse;
	}

	/**
	 * @param cardPointsUse the cardPointsUse to set
	 */
	public void setCardPointsUse(String cardPointsUse) {
		this.cardPointsUse = cardPointsUse;
	}

	/**
	 * @return the installmentPlan
	 */
	public BeanInstallmentPlanOP getInstallmentPlan() {
		return installmentPlan;
	}

	/**
	 * @param installmentPlan the installmentPlan to set
	 */
	public void setInstallmentPlan(BeanInstallmentPlanOP installmentPlan) {
		this.installmentPlan = installmentPlan;
	}
	
	

	public BeanSkipPayment getSkipPayment() {
		return skipPayment;
	}

	public void setSkipPayment(BeanSkipPayment skipPayment) {
		this.skipPayment = skipPayment;
	}

	/**
	 * @return the ownerAccount
	 */
	public BeanOwnerAccountOP getOwnerAccount() {
		return ownerAccount;
	}

	/**
	 * @param ownerAccount the ownerAccount to set
	 */
	public void setOwnerAccount(BeanOwnerAccountOP ownerAccount) {
		this.ownerAccount = ownerAccount;
	}

	/**
	 * @return the paymentAmount
	 */
	public BeanPaymentAmountOP getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount the paymentAmount to set
	 */
	public void setPaymentAmount(BeanPaymentAmountOP paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * @return the quotationId
	 */
	public String getQuotationId() {
		return quotationId;
	}

	/**
	 * @param quotationId the quotationId to set
	 */
	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "BeanOpenPayIn [afiliationId=" + afiliationId + ", card=" + card
				+ ", cardPointsUse=" + cardPointsUse + ", installmentPlan="
				+ installmentPlan + ", ownerAccount=" + ownerAccount
				+ ", paymentAmount=" + paymentAmount + ", skipPayment="
				+ skipPayment + ", quotationId=" + quotationId
				+ ", description=" + description + ", sessionId=" + sessionId
				+ "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */


	
}
