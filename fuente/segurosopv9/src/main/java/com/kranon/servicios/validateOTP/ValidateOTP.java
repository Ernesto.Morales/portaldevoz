package com.kranon.servicios.validateOTP;

import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.servicios.validateOTP.bean.BeamValidateOTPIn;
import com.kranon.servicios.validateOTP.bean.BeamValidateOTPOut;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kranon.bean.webservices.BeanEntorno;
import com.kranon.bean.webservices.BeanWebServices;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.ConexionHttp;
import com.kranon.servicios.UtilidadesResponse;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.bean.ErrorResponse;
import com.kranon.singleton.SingletonWebServices;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;
public class ValidateOTP {
	private CommonLoggerProcess log;
	private String url;
	private int timeout;
	private HashMap<String, String> paramsUrl;
	private String paramsUrlCodificado;
	private HashMap<String, String> header;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String idServicio;
	private String idInvocacion;
	private String nombre;
	private String version;
	private String idWebServices = "validateOTP";
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	
	public ValidateOTP(String idInvocacion, String idServicio, String idElemento) {

		if (this.log == null) {
			this.log = new CommonLoggerProcess(this.idWebServices);
		}
		this.log.inicializar(idInvocacion, idElemento);
		this.idServicio = idServicio;
		this.idInvocacion = idInvocacion;
		logKranon= new CommonLoggerKranon("KRANON-LOG",idInvocacion);
//		listlogger.add("AuthenticationFactor");
	}

	/**
	 * Metodo que prepara el JSON de entrada al Servicio de GetAuthenticationFactor con los parametros de entrada
	 * 
	 * @param tSEC
	 *            Token de seguridad devuelto por Granting Ticket
	 * @param channel
	 *            Identificador del contractId a resolver con la operacion
	 * @param service
	 *            Identificador del contractId a resolver con la operacion
	 * @param individualType
	 *            Identificador de invidualType a resolver con la operacion
	 * @param user
	 *            Identificador de user a resolver con la operacion
	 * @param cardNumber
	 *            Identificador del contractId a resolver con la operacion
	 */
	public void preparaPeticion(String tsec, String otp,String cel) {
		String idModuloLog = "PREPARA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";
		String service="";
		listlogger.add(UtilidadesLoggerKranon.envia+"ValidateOTP");
		try {
			// 1.- Escribir traza de inicio de modulo
			/** INICIO EVENTO - INICIO MODULO **/
			// solo mostramos en la traza los ultimos 5 digitos
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("otp", otp));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
//			listlogger.add(parametrosEntrada.toString());
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Leer la url y el timeout del Xml
			/** UNMARSHALL XML DE SERVICIOS WEB **/
			BeanWebServices beanWebServices = null;
			SingletonWebServices instanceWebServices = SingletonWebServices.getInstance(log);
			if (instanceWebServices != null) {
				beanWebServices = instanceWebServices.getWebServices(idServicio);
			}
			if (beanWebServices == null) {
				// error al recuperar los datos del webservice
				throw new Exception("WS_NULL");
			}
			/** Recupero el entorno **/
			String entorno = "";
			 try {
			 entorno = ArqSpringContext.getPropiedad("VAR.ENTORNO");
			 } catch (PropiedadNoEncontradaExcepcion e) {
			 // en caso de error, no queremos que ocurra en produccion
			 entorno = "pr";
			 }

			BeanEntorno datosEntorno = beanWebServices.getDatosEntorno(entorno);
			if (datosEntorno == null) {
				// no hay datos para este entorno
				throw new Exception("DATOS_NULL");
			}
			this.url = datosEntorno.getValidateOTP().getUrl();
			this.timeout = beanWebServices.getTimeout();
			// [201704_NMB] Se obtiene el nombre y la version del WS para las trazas de monitorizacion
			nombre = datosEntorno.getValidateOTP().getNombre();
			version = datosEntorno.getValidateOTP().getVersion();

			// Recupero parametros fijos
			String operationType = datosEntorno.getValidateOTP().getValueParametro("operationType");
									

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("operationType", operationType));
			this.log.actionEvent("GET_PARAMS_FIJOS", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			// 3.- Crear un hasMap con los parametros que hay incluir en la URL
			BeamValidateOTPIn beamValidateOTPIn = new BeamValidateOTPIn();
			beamValidateOTPIn.setOperationType(operationType);
			beamValidateOTPIn.setOtp(otp);
			beamValidateOTPIn.setCellphoneNumber(cel);
			
			this.paramsUrl = beamValidateOTPIn.dameParametros();

			// Creamos un params codificado para no mostrar datos sensibles
			paramsUrlCodificado = beamValidateOTPIn.dameParametros().toString();
		

			// 4.- Crear el header
			header = new HashMap<String, String>();
			if (tsec != null) {
				header.put("tsec", tsec);
			}

			resultadoOperacion = "OK";
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

		} finally {
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("paramsUrl", paramsUrlCodificado));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
//			listlogger.add("Url"+this.url);
			listlogger.add("paramsUrl"+UtilidadesBeans.cifrarTsec(paramsUrl.toString(),10));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			/** FIN EVENTO - FIN MODULO **/
		}

	}

	/**
	 * Realiza la peticion POST al servicio de getElectronicTelephoneANI
	 * 
	 * @return {@link ConexionResponse} objeto con la respuesta
	 */
	public ConexionResponse ejecutaPeticion() {
		String idModuloLog = "EJECUTA".concat("_").concat(this.idWebServices);
		String resultadoOperacion = "KO";
		listlogger.add("EJECUTA ValidateOTP");
		// Objeto retorno de la peticion
		ConexionResponse conexionResponse = null;
		try {
			// 1.- Escribir traza de inicio de modulo
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("url", url));
			parametrosEntrada.add(new ParamEvent("timeout", timeout + ""));
			parametrosEntrada.add(new ParamEvent("paramsUrl", (paramsUrl == null ? "null" : UtilidadesBeans.cifrarTsec(paramsUrlCodificado,10))));
			parametrosEntrada.add(new ParamEvent("header", (header == null ? "null" : UtilidadesBeans.cifrarTsec(header.toString(),10))));
			this.log.initModuleProcess(idModuloLog, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// 2.- Ejecuta la peticion
			// Creo el objeto de la conexion
			ConexionHttp conexionHttp = new ConexionHttp(this.log.getIdInvocacion(), this.log.getIdElemento());
			// Realizo la conexion
			conexionResponse = conexionHttp.executeGet(url, timeout, header, paramsUrl);

			// 3.- Si el tsec ha caducado se pide otro y se vuelve a lanzar la peticion
			// Tratar el caso de error
			// conexionResponse.setTsecValido(tsec);

			// 4.- Tratar la salida
			if (conexionResponse == null) {
				this.log.comment("WS_RESPONSE_IS_NULL");

				conexionResponse = new ConexionResponse();
				conexionResponse.setCodigoRespuesta(0);
				conexionResponse.setResultado("ERROR");
				conexionResponse.setMensajeError("WS_RESPONSE_IS_NULL");
				// ****************************************************************
				// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************
				this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
				this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
				// ****************************************************************
				// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
				// ****************************************************************	

			} else {
				if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {
					// Parseo de los datos de salida que fueran necesarios
					BeamValidateOTPOut beamValidateOTPOut = parserSalida(conexionResponse.getMensajeRespuesta());
//					conexionResponse.setMensajeRespuestaCodificado(salida2Json(listCustomerOut));

					// la operacion ha ido bien
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeRespuesta=" + conexionResponse.getMensajeRespuesta());

				} else {
					// la operacion ha ido KO o ERROR
					this.log.comment("resultado=" + conexionResponse.getResultado());
					this.log.comment("codigoHttp=" + conexionResponse.getCodigoRespuesta());
					this.log.comment("mensajeError=" + conexionResponse.getMensajeError());

					if (conexionResponse.getResultado().equalsIgnoreCase("KO")) {
						// si el resultado es KO, es decir, hay respuesta del WS
						// pero no es 200-OK
						// parseo el mensaje de error
						UtilidadesResponse utils = new UtilidadesResponse(this.log);
						ErrorResponse errorResponse = utils.parserError(conexionResponse.getMensajeError());
						conexionResponse.setErrorResponse(errorResponse);

						// ****************************************************************
						// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
						// ****************************************************************
						if(errorResponse != null){
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							// this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), null);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, errorResponse.getHttpStatus(), errorResponse.getErrorCode(), errorResponse.getErrorMessage(), null);
						} else {
							this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
							this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
						}
					}
					else {
						this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
						this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
					}
					// ****************************************************************
					// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
					// ****************************************************************
				}
			}
			resultadoOperacion = conexionResponse.getResultado();

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModuloLog, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			if (conexionResponse == null) {
				conexionResponse = new ConexionResponse();
			}
			conexionResponse.setResultado("ERROR");
			conexionResponse.setMensajeError(e.getMessage());		
			conexionResponse.setCodigoRespuesta(0);
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(this.idInvocacion,Constantes.FICHERO_SERVICIO_IVR + this.idServicio);
			this.logWarning.warningWS(this.idServicio, this.version, this.nombre, String.valueOf(conexionResponse.getCodigoRespuesta()), null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************		

		} finally {
			if(conexionResponse!=null){
				listlogger.add(""+conexionResponse.getCodigoRespuesta());
				listlogger.add(conexionResponse.getResultado());
				listlogger.add(conexionResponse.getMensajeError());
			}
			else{
				listlogger.add("conexionResponse=null");
			}
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("conexionResponse", conexionResponse == null ? "null" : conexionResponse.toString()));
			this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
//			
//			listlogger.add(resultadoOperacion);
//			listlogger.add(parametrosSalida.toString());
//			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
//			listlogger.clear();
		}
		return conexionResponse;
	}

	/**
	 * Parsea el objeto de entrada a una cadena JSON, SIN incluir el wrapper
	 * 
	 * @param {@link GetElectronicTelephoneANIRestInDto} objecto a convertir a JSON sin el objeto wrapper
	 * @return cadena JSON
	 * @throws JsonProcessingException
	 */
	// private String parserEntrada(AuthenticationFactorRestInDto object) throws JsonProcessingException {
	//
	// // Convierto el objeto JSON, sin incluir el wrapper GetElectronicTelephoneANIRestInDto
	// ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
	// mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
	// String inputJson = mapper.writeValueAsString(object);
	// return inputJson;
	// }

	/**
	 * Parsea la cadena JSON de salida de la peticion del servicio
	 * 
	 * @param outputJson
	 *            cadena JSON recibida en la peticion
	 * @return {@link GetElectronicTelephoneANIRestOutDto} objeto parseado
	 */
	public BeamValidateOTPOut parserSalida(String outputJson) throws Exception {
//		Map<String, Object> listCustomerOut = new HashMap<String, Object>();
		BeamValidateOTPOut beamValidateOTPOut=new BeamValidateOTPOut();
		try {
			ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			if (outputJson != null) {
				// convert JSON string to Map
//				listCustomerOut = mapper.readValue(outputJson, new TypeReference<Map<String, String>>(){});
//				proxyValueCipherBeanOut = mapper.readValue(outputJson, ProxyValueCipherBeanOut.class);
//				System.out.println(listCustomerOut);
				beamValidateOTPOut = mapper.readValue(outputJson, BeamValidateOTPOut.class);
			} else {
				beamValidateOTPOut = null;
			}
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("GetValidateOTPOut", beamValidateOTPOut == null ? "null" : beamValidateOTPOut.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

		} catch (Exception e) {
			beamValidateOTPOut = null;
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("outputJson", outputJson == null ? "null" : outputJson));
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("PARSEAR_SALIDA", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			throw e;

		}

		return beamValidateOTPOut;
	}


	/**
	 * Parsea el objeto de salida a una cadena JSON
	 * @param authenticationFactorRestOutDto objeto de tipo {@link AuthenticationFactorRestOutDto} a convertir a JSON
	 * @return cadena JSON
	 * @throws JsonProcessingException
	 */
	private String salida2Json(BeamValidateOTPOut beamValidateOTPOut) throws JsonProcessingException {

		// Convierto el objeto JSON
		ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.ALWAYS);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String outputJson = mapper.writeValueAsString(beamValidateOTPOut);

		return outputJson;
	}

}
