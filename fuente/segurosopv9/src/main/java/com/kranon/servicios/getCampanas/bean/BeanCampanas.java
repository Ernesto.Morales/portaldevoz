package com.kranon.servicios.getCampanas.bean;
import java.util.List;

import com.kranon.bean.menu.BeanOpcionMenu;

public class BeanCampanas {
	private List<BeanOpcionMenu> opciones;

	public BeanCampanas(List<BeanOpcionMenu> opciones) {
		super();
		this.opciones = opciones;
	}

	public BeanCampanas() {
		super();
	}

	/**
	 * @return the opciones
	 */
	public List<BeanOpcionMenu> getOpciones() {
		return opciones;
	}

	/**
	 * @param opciones the opciones to set
	 */
	public void setOpciones(List<BeanOpcionMenu> opciones) {
		this.opciones = opciones;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCampanas [opciones=" + opciones + "]";
	}
	

}
