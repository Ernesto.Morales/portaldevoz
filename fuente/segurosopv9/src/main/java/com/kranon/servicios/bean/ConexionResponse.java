package com.kranon.servicios.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.kranon.logger.enmasc.Enmascaramiento;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Objeto de Respuesta de la peticion HTTP
 * 
 * @author abalfaro
 *
 */
public class ConexionResponse {
	// Codigo HTTP de Respuesta
	private int codigoRespuesta;

	// Entradas a la peticion
	private String mensajeEnvio;
	private String mensajeEnvioCodificado;
	private HashMap<String, String> headerEnvio;

	// Salidas de la peticion
	private String mensajeRespuesta;
	private String mensajeRespuestaCodificado;
	private HashMap<String, List<String>> headerRespuesta;

	// parseo de los header de respuesta de warning
	private ResponseWarning responseWarning;

	// Resultado OK o KO o ERROR
	private String resultado;
	// Mensaje de error en caso de resultado KO o ERROR
	private String mensajeError;
	// Parseo del mensaje de error
	private ErrorResponse errorResponse;

	/**
	 * Arreglo con la definici�n de sensibilidad de los campos en el response para
	 * darles adecuado tratamiento. <strong>Tiene el siguiente orden:</strong>
	 * <ol>
	 * <li>MSG_IN_IS_SENSIBLE</li>
	 * <li>HEADER_IN_IS_SENSIBLE</li>
	 * <li>MSG_OUT_IS_SENSIBLE</li>
	 * <li>HEADER_OUT_IS_SENSIBLE</li>
	 * <li>RESP_WARN_IS_SENSIBLE</li> 
	 * <li>ERR_MSG_IS_SENSIBLE</li>
	 * <li>ERR_RESP_IS_SENSIBLE</li>
	 * </ol>
	 */
	boolean[] sensibleFields;
	
	public int getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(int codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getMensajeEnvio() {
		return mensajeEnvio;
	}

	public void setMensajeEnvio(String mensajeEnvio) {
		this.mensajeEnvio = mensajeEnvio;
	}

	public HashMap<String, String> getHeaderEnvio() {
		return headerEnvio;
	}

	public void setHeaderEnvio(HashMap<String, String> headerEnvio) {
		this.headerEnvio = headerEnvio;
	}

	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}

	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}

	public HashMap<String, List<String>> getHeaderRespuesta() {
		return headerRespuesta;
	}

	public void setHeaderRespuesta(HashMap<String, List<String>> headerRespuesta) {
		this.headerRespuesta = headerRespuesta;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public ResponseWarning getResponseWarning() {
		return responseWarning;
	}

	public void setResponseWarning(ResponseWarning responseWarning) {
		this.responseWarning = responseWarning;
	}

	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}

	public String getMensajeEnvioCodificado() {
		return mensajeEnvioCodificado;
	}

	public void setMensajeEnvioCodificado(String mensajeEnvioCodificado) {
		this.mensajeEnvioCodificado = mensajeEnvioCodificado;
	}

	public String getMensajeRespuestaCodificado() {
		return mensajeRespuestaCodificado;
	}

	public void setMensajeRespuestaCodificado(String mensajeRespuestaCodificado) {
		this.mensajeRespuestaCodificado = mensajeRespuestaCodificado;
	}

	@Override
	public String toString() {

		return String
				.format("ConexionResponse [codigoRespuesta=%s, mensajeEnvio=%s, headerEnvio=%s, mensajeRespuesta=%s, headerRespuesta=%s, responseWarning=%s, resultado=%s, mensajeError=%s, errorResponse=%s]",
						codigoRespuesta,
						(((mensajeEnvioCodificado == null) || (mensajeEnvioCodificado == "")) ? mensajeEnvio : mensajeEnvioCodificado),
						(headerEnvio == null ? "null" : getHeaderEnvioCodificado2String()),
						(((mensajeRespuestaCodificado == null) || (mensajeRespuestaCodificado == "")) ? mensajeRespuesta : mensajeRespuestaCodificado),
						(headerRespuesta == null ? "null" : getHeaderRespuestaCodificado2String()), responseWarning, resultado, mensajeError,
						(errorResponse == null ? "null" : errorResponse.toString()));

	}

	public String getHeaderEnvioCodificado2String() {
		try {
			if (headerEnvio == null) {
				return null;
			} else {
				if (headerEnvio.containsKey("tsec")) {
					HashMap<String, String> headerEnvioCodificado = new HashMap<String, String>();
					// recorro el header
					Iterator<Entry<String, String>> it = headerEnvio.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, String> e = (Map.Entry<String, String>) it.next();
						String clave = e.getKey();
						String valor = e.getValue();
						if (clave != null && clave.equalsIgnoreCase("tsec")) {
							valor = UtilidadesBeans.cifrarTsec(valor, 20);
						}
						headerEnvioCodificado.put(clave, valor);
					}
					return headerEnvioCodificado.toString();
				}
			}
			return headerEnvio.toString();
		} catch (Exception e) {
			// en caso de error que no nos frene pintar una traza
			return "";
		}
	}

	public String getHeaderRespuestaCodificado2String() {
		try {
			if (headerRespuesta == null) {
				return null;
			} else {
				if (headerRespuesta.containsKey("tsec")) {
					HashMap<String, List<String>> headerRespCodificado = new HashMap<String, List<String>>();
					// recorro el header
					Iterator<Entry<String, List<String>>> it = headerRespuesta.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, List<String>> e = (Map.Entry<String, List<String>>) it.next();
						String clave = e.getKey();
						List<String> valor = e.getValue();
						if (clave != null && clave.equalsIgnoreCase("tsec")) {
							String tsecCifrado = UtilidadesBeans.cifrarTsec(valor.get(0), 20);
							List<String> nuevoValor = new ArrayList<String>();
							nuevoValor.add(tsecCifrado);
							valor = nuevoValor;
						}
						headerRespCodificado.put(clave, valor);
					}
					return headerRespCodificado.toString();
				}
			}
			return headerRespuesta.toString();
		} catch (Exception e) {
			// en caso de error que no nos frene pintar una traza
			return "";
		}
	}
	
	/**
	 * Compone una cadena con la descripci�n de este objeto, listando los atributos
	 * que lo componen acompa�ados de sus respectivos valores, excepto cuando el arreglo
	 * que define la sensibilidad de ellos indique lo contrario, en cuyo caso �stos
	 * se enmascaran por completo, siendo reeplazado por un asterisco.
	 * @param wsResponseSensibleFields Arreglo con la definici�n de sensibilidad de
	 * los campos en el response para darles adecuado tratamiento. <strong>Tiene el
	 * siguiente orden:</strong>
	 * <ol>
	 * <li>MSG_IN_IS_SENSIBLE</li>
	 * <li>HEADER_IN_IS_SENSIBLE</li>
	 * <li>MSG_OUT_IS_SENSIBLE</li>
	 * <li>HEADER_OUT_IS_SENSIBLE</li>
	 * <li>RESP_WARN_IS_SENSIBLE</li> 
	 * <li>ERR_MSG_IS_SENSIBLE</li>
	 * <li>ERR_RESP_IS_SENSIBLE</li>
	 * </ol>
	 * 
	 * @return un {@code String} con la representaci�n textual de este objeto.
	 * Los atributos que se deban enmascarar de acuerdo a la definici�n en
	 * {@code postResponseIsSensible} se entregan de esta forma, siendo reeplazados
	 * por un asterisco.
	 */
	public String toMaskedString(boolean[] wsResponseSensibleFields){
		
		if(wsResponseSensibleFields == null){
			// no se especific� una definici�n de sensibilidad para los campos del response.
			return this.toString();
			
		}else{
			this.sensibleFields = wsResponseSensibleFields;
			
			String[] respPartes = new String[9];
			
			respPartes[0] = String.valueOf(this.codigoRespuesta);
			respPartes[1] = this.sensibleFields[0] == true ? (this.mensajeEnvio == null ? "null" : this.mensajeEnvio.equals("") ? ""
				: (this.mensajeEnvioCodificado != null && !this.mensajeEnvioCodificado.equals("") ? this.mensajeEnvioCodificado : "*")) 
				: this.mensajeEnvio;
			respPartes[2] = this.sensibleFields[1] == true ? Enmascaramiento.headerReq2MaskedStr(this.headerEnvio) : this.headerEnvio != null ?
				this.headerEnvio.toString() : "null";
			respPartes[3] = this.sensibleFields[2] == true ? (this.mensajeRespuesta == null ? "null" : this.mensajeRespuesta.equals("") ? 
				"" : (this.mensajeRespuestaCodificado != null && !this.mensajeRespuestaCodificado.equals("") ?
				this.mensajeRespuestaCodificado : "*")) : this.mensajeRespuestaCodificado;
			respPartes[4] = this.sensibleFields[3] == true ? Enmascaramiento.headerResp2MaskedStr(this.headerRespuesta) : this.headerRespuesta != null ?
				this.headerRespuesta.toString() : "null";
			respPartes[5] = this.sensibleFields[4] == true ? (this.responseWarning == null ? "null" : "*") : this.responseWarning == null ? "null" : 
				this.responseWarning.toString();
			respPartes[6] = this.resultado;
			respPartes[7] = this.sensibleFields[5] == true ? (this.mensajeError == null ? "null" : "*") : this.mensajeError;
			respPartes[8] = this.sensibleFields[6] == true ? (this.errorResponse == null ? "null" : "*") : this.errorResponse == null ? "null" :  
				this.errorResponse.toString();
			
			return String.format("ConexionResponse [codigoRespuesta=%s, mensajeEnvio=%s, headerEnvio=%s, " +
				"mensajeRespuesta=%s, headerRespuesta=%s, responseWarning=%s, resultado=%s, mensajeError=%s, " +
				"errorResponse=%s]",
				respPartes[0], respPartes[1], respPartes[2], respPartes[3], respPartes[4], respPartes[5],
				respPartes[6], respPartes[7], respPartes[8] );
		}
	}
	
	/**
	 * Invoca al m�todo {@link ConexionResponse#toMaskedString(boolean[] wsResponseSensibleFields)}
	 * indicando que TODOS los campos en el response que pueden ser sensibles lo son.
	 * @return la cadena resultante.
	 */
	public String toMaskedString(){	
		return this.toMaskedString(new boolean[] {true, true, true, true, true, true, true});
	}
}
