package com.kranon.servicios.getCampanas.bean;

import java.util.List;

public class BeanObtenerCampanaOut {
	private String codError;
	private List<BeanCampana> listaCampanya;
	private String descError;
	private String afiliacion;
	private String tipoTarjeta;
	
	
	public BeanObtenerCampanaOut(String codError,
			List<BeanCampana> listaCampanya) {
		super();
		this.codError = codError;
		this.listaCampanya = listaCampanya;
	}

	public BeanObtenerCampanaOut(String codError, String descError) {
		super();
		this.codError = codError;
		this.descError = descError;
	}

	public BeanObtenerCampanaOut(String codError,
			List<BeanCampana> listaCampanya, String descError) {
		super();
		this.codError = codError;
		this.listaCampanya = listaCampanya;
		this.descError = descError;
	}

	public BeanObtenerCampanaOut(String codError,
			List<BeanCampana> listaCampanya, String descError,
			String tipoTarjeta) {
		super();
		this.codError = codError;
		this.listaCampanya = listaCampanya;
		this.descError = descError;
		this.tipoTarjeta = tipoTarjeta;
	}

	
	public BeanObtenerCampanaOut(String codError,
			List<BeanCampana> listaCampanya, String descError,
			String afiliacion, String tipoTarjeta) {
		super();
		this.codError = codError;
		this.listaCampanya = listaCampanya;
		this.descError = descError;
		this.afiliacion = afiliacion;
		this.tipoTarjeta = tipoTarjeta;
	}

	public BeanObtenerCampanaOut() {
		super();
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the listaCampanya
	 */
	public List<BeanCampana> getListaCampanya() {
		return listaCampanya;
	}

	/**
	 * @param listaCampanya the listaCampanya to set
	 */
	public void setListaCampanya(List<BeanCampana> listaCampanya) {
		this.listaCampanya = listaCampanya;
	}

	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}

	/**
	 * @return the tipoTarjeta
	 */
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	/**
	 * @param tipoTarjeta the tipoTarjeta to set
	 */
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	@Override
	public String toString() {
		return "BeanObtenerCampanaOut [codError=" + codError
				+ ", listaCampanya=" + listaCampanya + ", descError="
				+ descError + ", afiliacion=" + afiliacion + ", tipoTarjeta="
				+ tipoTarjeta + "]";
	}

			
}
