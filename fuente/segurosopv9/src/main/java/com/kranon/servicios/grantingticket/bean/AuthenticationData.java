package com.kranon.servicios.grantingticket.bean;

import java.util.List;

import com.kranon.utilidades.UtilidadesBeans;

public class AuthenticationData {

	private String idAuthenticationData;
	private List<String> authenticationData;

	public String getIdAuthenticationData() {
		return idAuthenticationData;
	}

	public void setIdAuthenticationData(String idAuthenticationData) {
		this.idAuthenticationData = idAuthenticationData;
	}

	public List<String> getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(List<String> authenticationData) {
		this.authenticationData = authenticationData;
	}

	@Override
	public String toString() {
		return toStringCifrado();
//		return "AuthenticationData [idAuthenticationData="
//				+ idAuthenticationData + ", authenticationData="
//				+ authenticationData + "]";
	}
	
	
	public String toStringCifrado() {
		
		String resultadoCifrado = "AuthenticationData [idAuthenticationData="
				+ idAuthenticationData + ", authenticationData=";
		
		if (idAuthenticationData.equalsIgnoreCase("password") 
				|| idAuthenticationData.equalsIgnoreCase("audio")
				|| idAuthenticationData.equalsIgnoreCase("otp-token")
				|| idAuthenticationData.equalsIgnoreCase("NIP")
				|| idAuthenticationData.equalsIgnoreCase("otp-token"))
		{
			resultadoCifrado = resultadoCifrado + UtilidadesBeans.cifrar(authenticationData.toString(),0);
		}
		else
		{
			resultadoCifrado = resultadoCifrado + authenticationData;
		}
		
		resultadoCifrado = resultadoCifrado + "]";
		
		return resultadoCifrado;
		
		
	}

}
