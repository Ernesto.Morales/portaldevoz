package com.kranon.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.kranon.bean.BeanControlHorario;
import com.kranon.bean.BeanHorario;
import com.kranon.bean.BeanTipoHorario;
import com.kranon.bean.gestoperativas.BeanInfoExcepcion;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

public class UtilidadesGestionOperativa {

	/**
	 * Separa una etiqueta en Nombre Operativa mas metadatos
	 * 
	 * @param {@link CommonLoggerService} logger para el traceo
	 * @param {@link String} etiqueta
	 * @return {@link BeanDesgloseOperativa}
	 */
	public static BeanDesgloseOperativa separaEtiqueta(CommonLoggerService log, String etiqueta) {

		String idModulo = "SEPARA_ETIQUETA";
		String resultadoOperacion = "KO";

		BeanDesgloseOperativa operativa = new BeanDesgloseOperativa();
		try {

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("etiqueta", etiqueta));
			log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			if (etiqueta == null || etiqueta.equals("")) {
				return null;
			}

			String[] etiquetaSplit = etiqueta.split("#");
			String tipoMetadato = "";
			for (int i = 0; i < etiquetaSplit.length; i++) {
				String dato = etiquetaSplit[i];

				if (!dato.equals("")) {

					if (!tipoMetadato.equals("")) {
						// si el tipo metadato viene relleno es porque nos llega un valor
						if (tipoMetadato.equalsIgnoreCase("ACTION")) {
							operativa.setAction(dato);
						} else if (tipoMetadato.equalsIgnoreCase("TYPE")) {
							operativa.setType(dato);
						} else if (tipoMetadato.equalsIgnoreCase("BALANCE")) {
							operativa.setBalance(dato);
						} else if (tipoMetadato.equalsIgnoreCase("FOLIO")) {
							operativa.setFolio(dato);
						} else if (tipoMetadato.equalsIgnoreCase("INSURANCE")) {
							operativa.setInsurance(dato);
						} else if (tipoMetadato.equalsIgnoreCase("DATA")) {
							operativa.setData(dato);
						}
						// reinicio tipometadato
						tipoMetadato = "";
					}

					if (i == 0) {
						// es el nombre de la operativa
						operativa.setNombreOperativa(dato);
						// reinicio tipometadato
						tipoMetadato = "";

					} else {
						if (i % 2 != 0) {
							// es tipo metadato
							tipoMetadato = dato;
						}
					}
				}
			}

			resultadoOperacion = "OK";

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;
		} finally {
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("beanDesgloseOperativa", operativa.toString()));
			log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}

		return operativa;

	}

	/**
	 * Busca si la excepcion que se da en el listado existe en la operativa. En ese caso devuelve el nombre de la excepcion
	 * Si borrado true, esa excepcin encontrada se borra de la lista de excepciones, si es false no se borrar
	 * 
	 * @param infoExcepciones
	 * @param listaGestionExcep
	 * @param borrado
	 * @return
	 */
	public static String buscaExcepcion(BeanInfoExcepcion[] infoExcepciones, ArrayList<String> listaGestionExcep, boolean borrado) {
		String nuevoNombreOperativa = null;
		if(infoExcepciones == null || infoExcepciones.length == 0){
			return null;
		}
		if (listaGestionExcep == null || listaGestionExcep.size() == 0) {
			return null;
		} else {
			// hay excepciones de preproceso en el flujo, veo si casan con la operativa

			for (int i = listaGestionExcep.size() - 1; i >= 0; i--) {
				// para cada excepcion a tratar, empezando por el final
				for (BeanInfoExcepcion info : infoExcepciones) {
					// recorro cada excepcion de la operativa

					if (info.getNombre().equals(listaGestionExcep.get(i))) {
						// miro si la operativa tiene la excepcion a tratar, y en ese caso paro
						nuevoNombreOperativa = info.getNombre();

						if(borrado){
							// elimino la excepcion ya tratada
							listaGestionExcep.remove(i);
						}
						return nuevoNombreOperativa;
					}
				}
			}
		}
		return nuevoNombreOperativa;
	}
	
	public static String comprobarHorario(CommonLoggerService log, BeanControlHorario controlHorario) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO", "", null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";
		try {
			resulHorario = UtilidadesGestionOperativa.comprobarHorario(log, controlHorario, "");

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	public static String comprobarHorario(CommonLoggerService log, BeanControlHorario controlHorario, String param) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO", param, null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";
		try {
			if (controlHorario == null) {
				// no hay horario definido
				resulHorario = UtilidadesGestionOperativa.comprobarHorarioInterno(log, null);

			} else {
				if (controlHorario.getHorarioParam() != null) {
					// hay un horario con parametrizacion definido, llamo al de por defecto
					// se devolver el resultado del horario asociado a <param> o a vacio (default) en caso de no encontrarse
					resulHorario = UtilidadesGestionOperativa.comprobarHorarioParam(log, controlHorario.getHorarioParam(), param);

				} else {
					// hay un horario sin parametrizacion definido
					resulHorario = UtilidadesGestionOperativa.comprobarHorarioInterno(log, controlHorario.getHorario());
				}
			}
		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	private static String comprobarHorarioParam(CommonLoggerService log, BeanTipoHorario[] controlHorarioParam, String param) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO_PARAM", param, null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";

		try {
			if (controlHorarioParam == null || controlHorarioParam.length == 0) {
				// no hay control horario
				return "DH";
			}
			boolean encontrado = false;
			for (BeanTipoHorario tipoHorario : controlHorarioParam) {
				if (tipoHorario.getParam() != null && !tipoHorario.getParam().equals("")) {
					if (tipoHorario.getParam().equalsIgnoreCase(param)) {
						// es el horario que estamos buscando para el segmento dado
						resulHorario = UtilidadesGestionOperativa.comprobarHorarioInterno(log, tipoHorario.getHorario());
						encontrado = true;
						break;
					}
				}
			}

			if (!encontrado) {
				// si no he encontrado el segmento, busco si hay uno por defecto
				/** INICIO EVENTO - ACCION **/
				log.actionEvent("BUSCA_HORARIO_PARAM", "DEFAULT", null);
				/** FIN EVENTO - ACCION **/
				
				for (BeanTipoHorario tipoHorario : controlHorarioParam) {
					if (tipoHorario.getParam() != null && tipoHorario.getParam().equals("")) {
						// es el horario por defecto
						resulHorario = UtilidadesGestionOperativa.comprobarHorarioInterno(log, tipoHorario.getHorario());
						encontrado = true;
						break;
					}
				}
			}

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO_PARAM", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;
	}

	/**
	 * Comprueba si el dia y hora actual esta dentro de horario o no Si el control horario es vacio, devuelvo DH porque no se controla
	 * 
	 * @param {@link CommonLoggerService} logger para el traceo
	 * @param {@link BeanHorario[]} control de horario
	 * @return {@link String} DH o FH, para dentro y fuera de horario
	 * @throws Exception
	 */
	private static String comprobarHorarioInterno(CommonLoggerService log, BeanHorario[] controlHorario) throws Exception {

		/** INICIO EVENTO - ACCION **/
		log.actionEvent("BUSCA_HORARIO_INTERNO", "", null);
		/** FIN EVENTO - ACCION **/

		String resulHorario = "FH";

		try {
			if (controlHorario == null || controlHorario.length == 0) {
				// no hay control horario
				return "DH";
			}

			// datos de hoy
			String[] dias = { "D", "L", "M", "X", "J", "V", "S" };

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			int diaSemanaInt = calendar.get(Calendar.DAY_OF_WEEK);

			String diaSemana = dias[diaSemanaInt - 1];
			String anyoMesYDia = (new SimpleDateFormat("yyyy/MM/dd")).format(new Date());
			String mesYDia = (new SimpleDateFormat("MM/dd")).format(new Date());
			String horaHoy = (new SimpleDateFormat("HH:mm")).format(new Date());

			log.comment("anyoMesYDia=" + anyoMesYDia);
			log.comment("mesYDia=" + mesYDia);

			boolean encontrado = false;
			int i = 0;
			// BUSCO UN HORARIO CON LA FECHA CONCRETA DE HOY
			while (!encontrado && i < controlHorario.length) {
				BeanHorario horario = controlHorario[i];

				if (horario.getTipoHorario().equals("F")) {
					// es una fecha concreta
					if (horario.getValorHorario().length() > 5) {
						// es una fecha en formato YYYY/MM/DD
						if (horario.getValorHorario().equals(anyoMesYDia)) {
							// la fecha de hoy coincide
							encontrado = true;
						}
					} else {
						// es una fecha en formato MM/DD
						if (horario.getValorHorario().equals(mesYDia)) {
							// la fecha de hoy coincide
							encontrado = true;
						}
					}
				}

				// si coincide la fecha, miro las franjas
				if (encontrado) {

					String[] franjas = new String[3];
					franjas[0] = horario.getFranja1();
					franjas[1] = horario.getFranja2();
					franjas[2] = horario.getFranja3();

					resulHorario = UtilidadesGestionOperativa.comprobarFranjas(franjas, horaHoy);

				}

				// paso al siguiente horario
				i = i + 1;
			} // while

			if (!encontrado) {
				// el dia actual no coincide con ninguna fecha del horario,
				// busco por dia de la semana
				encontrado = false;
				i = 0;
				while (!encontrado && i < controlHorario.length) {
					BeanHorario horario = controlHorario[i];

					if (horario.getTipoHorario().equals("D")) {
						// es un dia de la semana
						if (horario.getValorHorario().equals(diaSemana)) {
							// coincide el dia de la semana
							encontrado = true;
						}
					}

					// si coincide el dia de la semana, miro las franjas
					if (encontrado) {

						String[] franjas = new String[3];
						franjas[0] = horario.getFranja1();
						franjas[1] = horario.getFranja2();
						franjas[2] = horario.getFranja3();

						resulHorario = UtilidadesGestionOperativa.comprobarFranjas(franjas, horaHoy);

					}
					i = i + 1;
				} // while

			} // if !encontrado

		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "COMPROBAR_HORARIO_INTERNO", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			return null;

		}
		return resulHorario;

	}

	/**
	 * Comprueba si las franjas estan dentro de la hora actual Si alguna de las franjas es CLOSED, se devuelve FH, independientemente de las horas
	 * 
	 * @param {@link String[]} franjas
	 * @return {@link String} DH o FH
	 */
	private static String comprobarFranjas(String[] franjas, String horaActual) {
		String resul = "FH";

		for (int i = 0; i < franjas.length; i++) {
			if (franjas[i] != null && !franjas[i].equals("")) {
				if (franjas[i].equals("CLOSED")) {
					// si alguna de las franjas es CLOSED, hemos acabado y el
					// resultado es fuera de horario
					resul = "FH";
					return resul;
				}
			}
		}

		// ninguna franja es closed, miro las horas
		for (int i = 0; i < franjas.length; i++) {
			if (franjas[i] != null && !franjas[i].equals("")) {
				// hay dos horas en la franjas
				String[] horasFranja = franjas[i].split("\\-");

				String[] horasInit = horasFranja[0].split("\\:");
				String[] horasFin = horasFranja[1].split("\\:");
				String[] horasHoy = horaActual.split("\\:");

				Calendar calInit = Calendar.getInstance();
				calInit.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasInit[0]));
				calInit.set(Calendar.MINUTE, Integer.parseInt(horasInit[1]));

				Calendar calFin = Calendar.getInstance();
				calFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasFin[0]));
				calFin.set(Calendar.MINUTE, Integer.parseInt(horasFin[1]));

				Calendar calHoy = Calendar.getInstance();
				calHoy.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horasHoy[0]));
				calHoy.set(Calendar.MINUTE, Integer.parseInt(horasHoy[1]));

				if (calInit.before(calHoy) && calHoy.before(calFin)) {
					// si la hora actual esta entre las horas de alguna franja,
					// devuelvo dentro de horario
					resul = "DH";
					break;
				}
			}
		}

		return resul;
	}

}
