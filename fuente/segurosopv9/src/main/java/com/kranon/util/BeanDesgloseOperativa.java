package com.kranon.util;


public class BeanDesgloseOperativa {

	private String nombreOperativa;

	private String action;

	private String type;

	private String balance;

	private String folio;

	private String insurance;

	private String data;
	
	public BeanDesgloseOperativa (){
		// el constructor inicializa todo a vacio, los metadatos no pueden quedarse a null
		this.nombreOperativa = "";
		this.action = "";
		this.type = "";
		this.balance = "";
		this.folio = "";
		this.insurance = "";
		this.data = "";
	}

	public String getNombreOperativa() {
		return nombreOperativa;
	}

	public void setNombreOperativa(String nombreOperativa) {
		this.nombreOperativa = nombreOperativa;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return String.format("BeanDesgloseOperativa [nombreOperativa=%s, action=%s, type=%s, balance=%s, folio=%s, insurance=%s, data=%s]",
				nombreOperativa, action, type, balance, folio, insurance, data);
	}

}
