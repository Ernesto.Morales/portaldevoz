package com.kranon.util;

public class UtilidadesCodigoSegmento {
	private static final String PATRIMONIAL = "PAT";
	private static final String PATRIMONIALVIP = "PAT";
	private static final String PERSONALNO = "PER";
	private static final String PERSONALCA = "PER";
	private static final String UNIVERSAL = "UNI";
	private static final String EXPRESS = "EXP";
	private static final String PYMES = "EMP";
	private static final String ERROR = "DES";
	
	
	public static String obtenerSegmento(String segmentoId)
	{
		if(segmentoId.equals("P9"))
			return PERSONALNO;
		else if(segmentoId.equals("Q1"))
			return PERSONALCA;
		else if(segmentoId.equals("R2") || segmentoId.equals("R3") || segmentoId.equals("R1") ||
					segmentoId.equals("R4"))
			return EXPRESS;	
		else if(segmentoId.equals("B1") || segmentoId.equals("B2") || segmentoId.equals("B3") ||
				segmentoId.equals("B4") || segmentoId.equals("B5") || segmentoId.equals("B6") ||
				segmentoId.equals("BB")|| segmentoId.equals("BC") || segmentoId.equals("BF") ||
				segmentoId.equals("C0") || segmentoId.equals("G0"))
			return UNIVERSAL;
		else if(segmentoId.equals("F1") || segmentoId.equals("F2") || segmentoId.equals("F3") ||
				segmentoId.equals("F4") || segmentoId.equals("F5") || segmentoId.equals("F6") ||
				segmentoId.equals("F7") || segmentoId.equals("F8") || segmentoId.equals("N1") ||
				segmentoId.equals("R5") || segmentoId.equals("R6") || segmentoId.equals("S")  ||
				segmentoId.equals("S0") || segmentoId.equals("S1"))
			return UNIVERSAL;
		else if(segmentoId.equals("AL") || segmentoId.equals("E0") || segmentoId.equals("E1") ||
					segmentoId.equals("E2") || segmentoId.equals("E3") || segmentoId.equals("E0") ||
					segmentoId.equals("D1")|| segmentoId.equals("D3") || segmentoId.equals("D4") ||
					segmentoId.equals("D7") || segmentoId.equals("D8") || segmentoId.equals("D9") ||
					segmentoId.equals("FF") || segmentoId.equals("FM") || segmentoId.equals("G3") ||
					segmentoId.equals("Y1") || segmentoId.equals("Y2"))
			return PYMES;
		else if(segmentoId.equals("P1") || segmentoId.equals("P2") || segmentoId.equals("P3")||
				segmentoId.equals("P4")||segmentoId.equals("P5")||segmentoId.equals("P6")||segmentoId.equals("P7")
				||segmentoId.equals("P8")||segmentoId.equals("P9"))
				return PATRIMONIAL;
		else if(segmentoId.equals("P0")||segmentoId.equals("P0"))
				return PATRIMONIALVIP;
		else
			return ERROR;
	}

}
