package com.kranon.util;

import java.util.List;

public class UtilidadesLoggerKranon {
		public static String mensaje="Mensaje";
		public static String envia="En";
		public static String responde="Respuesta";
		public static String obtiene="Obtiene";
		public static String tarjeta="Tarjeta:";
		public static String digitado="Introducido:";
		public static String linea="Linea Shortcut";
		public static String error="ERROR";
		public static String autenticado="AUTENTICADO";
		public static String noAutenticado="NO_AUTENTICADO";
		public static String deleteTsec="Borro Tsec";
		public static String nodeleteTsec="No borro Tsec";
		public static String puntos="...";
		
		public static String getTrazaLista(List<String> list) {
			String log = "";
			try {
			for (String cadena : list) {
				if(cadena!=null){
				if(!(cadena.equals(" ") || cadena.equals(""))){
					log=log+"|"+cadena;
				}
				}
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return log;
		}
		
		public static String tarjetaCuatro(String tarjeta){
			if(tarjeta.length()>=16){
				tarjeta="***"+tarjeta.substring(12,tarjeta.length());
			}
			return tarjeta;
		}
}
