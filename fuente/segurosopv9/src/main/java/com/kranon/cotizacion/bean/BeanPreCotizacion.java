package com.kranon.cotizacion.bean;

public class BeanPreCotizacion {
	private String tipoOperacion;
	private String idCotiza;
	private String idSession;
	private String idint;
	public BeanPreCotizacion(String tipoOperacion, String idCotiza,
			String idSession, String idint) {
		super();
		this.tipoOperacion = tipoOperacion;
		this.idCotiza = idCotiza;
		this.idSession = idSession;
		this.idint = idint;
	}
	public BeanPreCotizacion() {
		super();
	}
	
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the idCotiza
	 */
	public String getIdCotiza() {
		return idCotiza;
	}
	/**
	 * @param idCotiza the idCotiza to set
	 */
	public void setIdCotiza(String idCotiza) {
		this.idCotiza = idCotiza;
	}
	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}
	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}
	/**
	 * @return the idint
	 */
	public String getIdint() {
		return idint;
	}
	/**
	 * @param idint the idint to set
	 */
	public void setIdint(String idint) {
		this.idint = idint;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanPreCotizacion [tipoOperacion=" + tipoOperacion
				+ ", idCotiza=" + idCotiza + ", idSession=" + idSession
				+ ", idint=" + idint + "]";
	}
	
	

}
