package com.kranon.cotizacion.bean;

import com.kranon.utilidades.UtilidadesBeans;

public class BeanCotizacion {

	private String idCotiza;
	private String idSession;
	private String montoCobro;
	private String monedaCobro;
	private String nombreCliente;
	private String apellidosCliente;
	private String telefono;
	private String correo;
	private String afiliacionId;
	private String descripcion;
	
	public BeanCotizacion() {
		super();
	}

	public BeanCotizacion(String idCotiza, String idSession, String montoCobro,
			String monedaCobro, String nombreCliente, String apellidosCliente,
			String telefono, String correo, String afiliacionId, String descripcion) {
		super();
		this.idCotiza = idCotiza;
		this.idSession = idSession;
		this.montoCobro = montoCobro;
		this.monedaCobro = monedaCobro;
		this.nombreCliente = nombreCliente;
		this.apellidosCliente = apellidosCliente;
		this.telefono = telefono;
		this.correo = correo;
		this.afiliacionId = afiliacionId;
		this.descripcion=descripcion;
	}

	/**
	 * @return the idCotiza
	 */
	public String getIdCotiza() {
		return idCotiza;
	}
	/**
	 * @param idCotiza the idCotiza to set
	 */
	public void setIdCotiza(String idCotiza) {
		this.idCotiza = idCotiza;
	}
	
	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}

	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}

	/**
	 * @return the montoCobro
	 */
	public String getMontoCobro() {
		return montoCobro;
	}
	/**
	 * @param montoCobro the montoCobro to set
	 */
	public void setMontoCobro(String montoCobro) {
		this.montoCobro = montoCobro;
	}
	/**
	 * @return the monedaCobro
	 */
	public String getMonedaCobro() {
		return monedaCobro;
	}
	/**
	 * @param monedaCobro the monedaCobro to set
	 */
	public void setMonedaCobro(String monedaCobro) {
		this.monedaCobro = monedaCobro;
	}
	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return the apellidosCliente
	 */
	public String getApellidosCliente() {
		return apellidosCliente;
	}
	/**
	 * @param apellidosCliente the apellidosCliente to set
	 */
	public void setApellidosCliente(String apellidosCliente) {
		this.apellidosCliente = apellidosCliente;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return the afiliacionId
	 */
	public String getAfiliacionId() {
		return afiliacionId;
		
	}
	/**
	 * @param afiliacionId the afiliacionId to set
	 */
	public void setAfiliacionId(String afiliacionId) {
		this.afiliacionId = afiliacionId;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanCotizacion [idCotiza=" + idCotiza + 
				", idSession="+ idSession +
				", montoCobro=" + montoCobro +
				", monedaCobro="+ monedaCobro + 
				", nombreCliente=" + nombreCliente
				+ ", apellidosCliente=" + (apellidosCliente == null ? "null" : "*")  + 
				", telefono="+ (telefono == null ? "null" : UtilidadesBeans.cifrar(telefono, 4)) + 
				", correo=" +(correo == null ? "null" : UtilidadesBeans.cifrar(correo, 4)) + 
				", afiliacionId="+ afiliacionId +
				", descripcion=" + descripcion + "]";
	}

	
}
