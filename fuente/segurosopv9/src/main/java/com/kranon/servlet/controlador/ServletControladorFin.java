package com.kranon.servlet.controlador;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de CONTROLADOR-FIN en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletControladorFin extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletControladorFin() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// aado el codigo retorno a devolver
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("resultadoOperacion", "KO");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_CONTROLADOR_FIN).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("================================================[ CONTROLADOR FIN ]===============================================");
		String idAccion = "CONTROLADOR_FIN";

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		try {
			String segmentoId = request.getParameter("VG_transfer.segmentoId");
			String codigoRetorno = request.getParameter("VG_codigoRetorno");
			String tipo = request.getParameter("VG_loggerServicio.tipo");
			String resultadoOperacion = request.getParameter("VG_resultadoOperacion");
			String error = request.getParameter("VG_error");
			String esquema = request.getParameter("VG_esquema");
			String segmento = request.getParameter("VG_segmento");
			String producto = request.getParameter("VG_producto");
			String operativasIVR = request.getParameter("VG_operativasIVR");
			String operativasIVRTotales = request.getParameter("VG_operativasIVRTotales");
//			Boolean bandera = true;
//			while(bandera)
//			{
//				
//				int contador = 0;
//				String cadena = "VG_operativasIVRTotales." + contador + ".nombre";
//				String requestCadena =request.getParameter(cadena);
//				if (requestCadena == null || requestCadena == "")
//					bandera = false;
//			}
			
			
				if(codigoRetorno.equals("HANGUP")){
					error = "";
					codigoRetorno = "HANGUP";
					resultadoOperacion = "KO";
				}
				else if(codigoRetorno.equals("TRANSFER")){
					error = "";
					codigoRetorno = "TRANSFER";
					resultadoOperacion = "KO";
				}
				else{
					error = "";
					codigoRetorno = "OK";
					resultadoOperacion = "OK";
				}

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			
			
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
			
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosOperativasTotales = new ArrayList<ParamEvent>();
			parametrosOperativasTotales.add(new ParamEvent("operativasIVRTotales", operativasIVRTotales));
			this.log.actionEvent("OPERATIVAS_IVR_TOTALES", "OK", parametrosOperativasTotales);
			/** FIN EVENTO - ACCION **/
			
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			parametrosAdicionales.add(new ParamEvent("esquema", esquema));
			parametrosAdicionales.add(new ParamEvent("segmento", segmento));
			parametrosAdicionales.add(new ParamEvent("producto", producto));
			parametrosAdicionales.add(new ParamEvent("operativasIVRTotales", operativasIVRTotales));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/
	
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (error != null && !error.equals("") && !error.equals("MAXINT_MENU" ) && !error.equals("ERROR_HV" )) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("operativasIVR", operativasIVR);
			
			/** INICIO EVENTO - FIN DE CONTROLADOR **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("nombreControlador", idElemento));
			parametrosAdicionalesStat.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionalesStat.add(new ParamEvent("operativasIVR", operativasIVR));
			if(error != null && !error.equals("")){
				parametrosAdicionalesStat.add(new ParamEvent("error", error));
			}
			this.log.statisticEvent("CONTROLADOR_FIN", resultadoOperacion, parametrosAdicionalesStat);
			/** FIN EVENTO - FIN DE CONTROLADOR **/
			
			
			
//			ArrayList<String> parameterNames = new ArrayList<String>(); 
//			Enumeration enumeration = request.getParameterNames(); 
//			Integer contador = 0;
//			while (enumeration.hasMoreElements()) 
//			{ 
//				String parameterName = (String) enumeration.nextElement(); 
//				contador = contador + 1;
//				parameterNames.add(parameterName); 
//			}
//
//			ArrayList<String> parameterNames1 = new ArrayList<String>(); 
//			Enumeration enumeration1 = request.getAttributeNames(); 
//			Integer contador1 = 0;
//			while (enumeration1.hasMoreElements()) 
//			{ 
//				String parameterName1 = (String) enumeration1.nextElement(); 
//				contador1 = contador1 + 1;
//				parameterNames.add(parameterName1); 
//			}
			
			
			
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_CONTROLADOR_FIN).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			// aado el codigo retorno a devolver
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("resultadoOperacion", "KO");

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			String error = "ERROR_EXT(" + e.getMessage() + ")";
//			this.logWarning = new CommonLoggerMonitoring(Constantes.FICHERO_SERVICIO_IVR + idServicio);
//			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
//					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_CONTROLADOR_FIN).forward(request, response);

		}
	}

}
