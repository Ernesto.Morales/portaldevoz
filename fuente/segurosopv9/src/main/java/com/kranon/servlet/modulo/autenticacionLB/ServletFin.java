package com.kranon.servlet.modulo.autenticacionLB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.util.UtilidadesCuadroMando;

/**
 * Servlet para la peticion de FIN en el Modulo de Autenticacion
 * 
 * @author aarce
 *
 */
public class ServletFin extends ServletBaseModuloAutenticacion {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	
	public ServletFin() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_FIN).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_AUTENTICACION";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			
			String codigoRetornoModulo = request.getParameter("PRM_OUT_AUT_codigoRetorno");
			String resultadoOperacionModulo = request.getParameter("PRM_OUT_AUT_resultadoOperacion");
			String errorModulo = request.getParameter("PRM_OUT_AUT_error");
			String isAutenticado = request.getParameter("PRM_OUT_AUT_isAutenticado");
			String tipoAutenticacion = request.getParameter("PRM_OUT_AUT_tipoAutenticacion");
			String info = request.getParameter("PRM_OUT_AUT_info");
			String cliente = request.getParameter("PRM_OUT_AUT_candado");
			
			ArrayList<String> listaExcepPostAutenticacion = new ArrayList<String>();
			ArrayList<String> listaExcepPostInformacion = new ArrayList<String>();

			Enumeration<?> enumeration = request.getParameterNames();
			while (enumeration.hasMoreElements()) {
				String parameterName = (String) enumeration.nextElement();
				String value = request.getParameter(parameterName);

				if (parameterName.contains("PRM_OUT_AUT_gestionExcepciones.postAutenticacion")) {
					listaExcepPostAutenticacion.add(value);
				} else if (parameterName.contains("PRM_OUT_AUT_gestionExcepciones.postInformacion")) {
					listaExcepPostInformacion.add(value);
				}
			}
	
			// [201705_NMB] Recojemos la fecha de hoy para luego incluirla en el VG_stat del JSP
			Date fechaHoy = new Date();
			Long milisecFinOperacion = fechaHoy.getTime();
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
	
			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (errorModulo != null && !errorModulo.equals("") && !errorModulo.equals("MAXINT_MENU") && !errorModulo.equals("ERROR_HV")) {
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
				this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
						UtilidadesCuadroMando.getStat(this.log, request, idServicio)), errorModulo, "", null);
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetornoModulo));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", errorModulo));
			}
			parametrosSalida.add(new ParamEvent("isAutenticado", isAutenticado));
			parametrosSalida.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosSalida.add(new ParamEvent("info", info));
			parametrosSalida.add(new ParamEvent("cliente", cliente));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("excepPostAutenticacion", listaExcepPostAutenticacion.toString()));
			parametrosAdicionales.add(new ParamEvent("excepPostInformacion", listaExcepPostInformacion.toString()));
			this.log.endModuleService(idModulo, resultadoOperacionModulo, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			
			/** INICIO EVENTO - STAT FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("resultadoOperacion", resultadoOperacionModulo));
			parametrosAdicionalesStat.add(new ParamEvent("codigoRetorno", codigoRetornoModulo));
			if (errorModulo != null && !errorModulo.equals("")) {
				parametrosAdicionalesStat.add(new ParamEvent("error", errorModulo));
			}
			parametrosAdicionalesStat.add(new ParamEvent("isAutenticado", isAutenticado));
			parametrosAdicionalesStat.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosAdicionalesStat.add(new ParamEvent("info", info));
			parametrosAdicionalesStat.add(new ParamEvent("cliente", cliente));
			parametrosAdicionalesStat.add(new ParamEvent("excepPostAutenticacion", listaExcepPostAutenticacion.toString()));
			parametrosAdicionalesStat.add(new ParamEvent("excepPostInformacion", listaExcepPostInformacion.toString()));
			this.log.statisticEvent(idModulo, "END", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT FIN DE MODULO **/
	
			// Aado parametros de salida a la request
			request.setAttribute("milisecFinOperacion", milisecFinOperacion);
			request.setAttribute("isAutenticado",isAutenticado);
			
			
			ArrayList<String> parameterNames = new ArrayList<String>();
			Enumeration enumeration2 = request.getParameterNames();
			Integer contador = 0;
			while (enumeration2.hasMoreElements()) { 
				String parameterName = (String) enumeration2.nextElement(); 
				parameterNames.add(parameterName); 
				contador = contador + 1;
				
			}
			
			ArrayList<String> parameterNames1 = new ArrayList<String>();
			Enumeration enumeration1 = request.getAttributeNames();
			Integer contador1 = 0;
			while (enumeration1.hasMoreElements()) { 
				String parameterName1 = (String) enumeration1.nextElement(); 
				parameterNames1.add(parameterName1); 
				contador1 = contador1 + 1;
				
			}


			
			
			
			
			
			
			
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_FIN).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
			this.logWarning.warningWAS(idServicio, 	UtilidadesCuadroMando.getContadorCuadroMando(this.log, idServicio, 
					UtilidadesCuadroMando.getStat(this.log, request, idServicio)), error, "", null);
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			
			/** INICIO EVENTO - STAT FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosAdicionalesStat.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionalesStat.add(new ParamEvent("error", error));
			this.log.statisticEvent(idModulo, "END", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT FIN DE MODULO **/
		
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_FIN).forward(request, response);

		}
	}

}
