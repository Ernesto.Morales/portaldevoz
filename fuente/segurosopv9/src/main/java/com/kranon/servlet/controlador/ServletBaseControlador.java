package com.kranon.servlet.controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.serv.CommonLoggerService;

public abstract class ServletBaseControlador extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA LINEA BANCOMER
	protected CommonLoggerService log; 
	
	private static String directorioControlador = "/Controlador";

	protected static final String PAGE_INICIO_PARAMS = directorioControlador + "/dynamicvxml/CONTROLADOR-INICIO-PARAMS.jsp";
	protected static final String PAGE_CONTROLADOR_FIN = directorioControlador + "/dynamicvxml/CONTROLADOR-FIN.jsp";
	// SUBDIALOGOS - CONTROLADOR-FIN
	protected static final String PAGE_DELETE_TICKET = directorioControlador + "/subdialogos/SUB-CONTROLADOR-DELETE-TICKET.jsp";
	protected static final String PAGE_DELETE_TSEC = directorioControlador + "/subdialogos/SUB-CONTROLADOR-DELETE-TSEC.jsp";
	protected static final String PAGE_SUB_DESPEDIDA= directorioControlador + "/subdialogos/SUB-CONTROLADOR-DESPEDIDA.jsp";
	protected static final String PAGE_GESTION_INICIAL = directorioControlador + "/dynamicvxml/CONTROLADOR-GESTION-INICIAL.jsp";
	protected static final String PAGE_GESTION_AUTENTICACION = directorioControlador + "/dynamicvxml/CONTROLADOR-GESTION-AUTENTICACION.jsp";
	protected static final String PAGE_GESTION_USAC = directorioControlador + "/dynamicvxml/CONTROLADOR-PRUEBAS.jsp";
	// SUBDIALOGOS - GESTION-INICIAL
	protected static final String PAGE_BLOQUEARTI = directorioControlador + "/subdialogos/SUB-CONTROADOR-BLOQUEARCTI.jsp";
	protected static final String PAGE_SUB_BIENVENIDA = directorioControlador + "/subdialogos/SUB-CONTROLADOR-BIENVENIDA.jsp";
	protected static final String PAGE_SUB_AVISO_PRIVACIDAD = directorioControlador + "/subdialogos/SUB-CONTROLADOR-AVISO-PRIVACIDAD.jsp";
	protected static final String PAGE_SUB_MENU_PRINCIPAL = directorioControlador + "/subdialogos/SUB-CONTROLADOR-MENU-PRINCIPAL.jsp";
	protected static final String PAGE_SUB_SUBMENU_NETCASH_INGLES = directorioControlador + "/subdialogos/SUB-CONTROLADOR-SUBMENU-NETCASH-INGLES.jsp";
	protected static final String PAGE_SUB_SUBMENU_NETCASH = directorioControlador + "/subdialogos/SUB-CONTROLADOR-SUBMENU-NETCASH.jsp";
	protected static final String PAGE_SUB_IDENTIFICACION = directorioControlador + "/subdialogos/SUB-CONTROLADOR-IDENTIFICACION.jsp";
	protected static final String PAGE_SUB_CONTROLADOR_RESPUESTA_MAU= directorioControlador + "/subdialogos/SUB-CONTROLADOR-RESPUESTA-MAU.jsp";
	// SUBDIALOGOS - GESTION-PRINCIPAL-PA

	protected static final String PAGE_GESTION_PREVIA_OPERATIVA = directorioControlador + "/dynamicvxml/CONTROLADOR-GESTION-PREVIA-OPERATIVA.jsp";
	protected static final String PAGE_GESTION_OPERATIVA = directorioControlador + "/dynamicvxml/CONTROLADOR-GESTION-OPERATIVA.jsp";
	protected static final String PAGE_SUB_GESTION_TRATAMIENTO_TRANSFER = directorioControlador + "/subdialogos/SUB-CONTROLADOR-GESTION-TRATAMIENTO-TRANSFER.jsp";
	
	protected static final String PAGE_GESTION_OPERATIVA_POST = directorioControlador + "/dynamicvxml/CONTROLADOR-GESTION-OPERATIVA-POST.jsp";

	protected static final String PAGE_EJECUCION_MODULO = directorioControlador + "/dynamicvxml/CONTROLADOR-EJECUCION-MODULO.jsp";

	public static final String PAGE_SUB_RECUPERA_CONTROLADOR = directorioControlador + "/subdialogos/SUB-CONTROLADOR-RECUPERA-CONT.jsp";

	protected static final String PAGE_SUB_CONTROLADOR_PROMS= directorioControlador + "/subdialogos/SUB-CONTROLADOR-PROMS.jsp";
	
	protected static final String PAGE_SUB_CONTROLADOR_MENSAJES_ERROR= directorioControlador + "/subdialogos/SUB-CONTROLADOR-MENSAJES-ERROR.jsp";
		
	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
