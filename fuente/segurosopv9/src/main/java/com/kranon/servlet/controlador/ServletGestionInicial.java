package com.kranon.servlet.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.monit.CommonLoggerMonitoring;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.logger.utilidades.Constantes;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.grantingticket.GrantingTicket;
import com.kranon.singleton.SingletonServicio;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para la peticion de GESTION-INICIAL en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletGestionInicial extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerMonitoring logWarning;
	private String errorWarning;
	private String tsecCodificado;
	// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletGestionInicial() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_GESTION_INICIAL).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("==================[ SEGUROS... ]======================");
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SEGUROS_END_TO_END";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = "";
		String pregAbiertaActivo = "";
		String modoInteraccionServicio = "";
		String tsec = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
//		listlogger.add(idModulo);
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String isIdentificadoCliente = request.getParameter("VG_cliente.isIdentificado");
			String isReqIdentificacion = request.getParameter("VG_controlador.isReqIdentificacion");
			String tipoIdentificacion = request.getParameter("VG_controlador.tipoIdentificacion");
			String isReqAutenticacion = request.getParameter("VG_controlador.isReqAutenticacion");
			String tipoAutenticacion = request.getParameter("VG_controlador.tipoAutenticacion");
			String accionOkIdentificacion = request.getParameter("VG_controlador.accionOkIdentificacion");

			String operativasIVR = request.getParameter("VG_operativasIVR");
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			if(operativasIVR != null && !operativasIVR.equals("")){
				// escribo una traza para indicar una vuelta completa de ejecucion desde que se solicita
				
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("operativasIVR", operativasIVR));
				this.log.actionEvent("VUELTA_GESTION_OPERATIVA", "OK", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/
			}
			
			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("isIdentificadoCliente", isIdentificadoCliente));
			parametrosEntrada.add(new ParamEvent("isReqIdentificacion", isReqIdentificacion));
			parametrosEntrada.add(new ParamEvent("tipoIdentificacion", tipoIdentificacion));
			parametrosEntrada.add(new ParamEvent("isReqAutenticacion", isReqAutenticacion));
			parametrosEntrada.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosEntrada.add(new ParamEvent("accionOkIdentificacion", accionOkIdentificacion));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			

			// Recuperamos el Bean del Servicio en cuestion
			BeanServicio servicio = null;
			SingletonServicio instance = SingletonServicio.getInstance(log);
			if (instance != null) {
				servicio = instance.getServicio(idServicio);
			}

			if (servicio == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(SERV_NULL)";

			} else {
				// hemos recuperado bien el servicio

				/**
				 * INICIO EVENTO - COMENTARIO this.log.comment("GET_SERVICIO=" +
				 * servicio.toString()); FIN EVENTO - COMENTARIO
				 **/

				pregAbiertaActivo = servicio.getConfiguracion().getPregAbiertaActivo();
				modoInteraccionServicio = servicio.getConfiguracion().getModoInteraccion();

				resultadoOperacion = "OK";
				codigoRetorno = "OK";
				error = "";

				// *****************************************************
				// ***** INTEGRACIoN WS OPERACIoN GrantingTicket
				// *****************************************************
				ConexionResponse conexionResponse = null;

				// Datos FIJOS para la llamada inicial cuando no se tienen datos
				// del
				// cliente
				String backendSessionId = null;
				String clientId = null;
				String userId = "";
				String accessCode = "";
				String dialogId = "";

				/** LLAMO AL SERVICIO **/
				// [20161216-NMB] SE INCLUYE EL idServicio PARA ESCRIBIR LAS TRAZAS DE MONITORIZACION
				GrantingTicket gt = new GrantingTicket(idLlamada, idServicio, idElemento);
				gt.preparaPeticion(userId, accessCode, dialogId, backendSessionId, clientId);
				conexionResponse = gt.ejecutaPeticion();
//				listlogger.add(UtilidadesLoggerKranon.envia+"GT");
//				if(conexionResponse!=null){
//					listlogger.add(""+conexionResponse.getCodigoRespuesta());
//					if(conexionResponse.getMensajeError()!=null || conexionResponse.getMensajeError()!=""){
//						listlogger.add(conexionResponse.getMensajeError());
//					}
//					
//				}
				if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {
					// la peticion ha ido bien

					if (conexionResponse.getCodigoRespuesta() == 200) {
						tsec = gt.dameTsec(conexionResponse.getHeaderRespuesta());
						tsecCodificado = (UtilidadesBeans.cifrarTsec(tsec,20));
						/** INICIO EVENTO - ACCION **/
						// aqui si imprimo el tsec completo
						this.log.actionEvent("NEW_TSEC", tsecCodificado, null);
						/** FIN EVENTO - ACCION **/

						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";
					} else {
						resultadoOperacion = "KO";
						codigoRetorno = "KO";
						error = "";
						
					}

				} else if (conexionResponse.getResultado().equalsIgnoreCase("KO") || resultadoOperacion.equals("KO")) {
					// se ha recibido una respuesta de KO
					resultadoOperacion = "KO";
					codigoRetorno = "KO";
					error = "";

				} else {
					// ha habido un error
					resultadoOperacion = "KO";
					codigoRetorno = "ERROR";
					error = "ERROR_EXT(WS_GRANTING_TICKET)";
				}
			}
		
// TODO Trucado para pruebas
//resultadoOperacion = "OK";
//codigoRetorno = "OK";
//error = "";
//tsec = "TSEC_TRUCADO_PARA_PRUEBAS_DFGHSDGJHSDGHSDHGSDHGSDGH";			
// -----------------------		

			// *****************************************************

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("pregAbiertaActivo", pregAbiertaActivo));
			parametrosSalida.add(new ParamEvent("modoInteraccionServicio", modoInteraccionServicio));
			parametrosSalida.add(new ParamEvent("accionOkIdentificacion", accionOkIdentificacion));
			parametrosSalida.add(new ParamEvent("tsec", UtilidadesBeans.cifrarTsec(tsec, 10)));	
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** INICIO EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("pregAbiertaActivo", pregAbiertaActivo);
			request.setAttribute("modoInteraccionServicio", modoInteraccionServicio);
			request.setAttribute("tsec", tsec);
			request.setAttribute("accionOkIdentificacion", accionOkIdentificacion);

			// ****************************************************************
			// [201704_NMB] INICIO - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
			if (pregAbiertaActivo == null) {
				this.errorWarning = "<preg_abierta_activo>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);	
			}
			if (modoInteraccionServicio == null) {
				this.errorWarning = "<modo_interaccion>";
				this.logWarning = new CommonLoggerMonitoring(idLlamada, Constantes.FICHERO_SERVICIO_IVR + idServicio);
				this.logWarning.warningCFG(idServicio, idElemento, this.errorWarning, "key", null);	
			}
			// ****************************************************************
			// [201704_NMB] FIN - SE INCLUYEN LAS TRAZAS DE MONITORIZACION
			// ****************************************************************
//			listlogger.add(resultadoOperacion);
//			listlogger.add(codigoRetorno);
//			listlogger.add(error);
			listlogger.add("tsec:"+UtilidadesBeans.cifrarTsec(tsec, 10)+UtilidadesLoggerKranon.puntos);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_GESTION_INICIAL).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("pregAbiertaActivo", pregAbiertaActivo));
			parametrosSalida.add(new ParamEvent("modoInteraccionServicio", modoInteraccionServicio));
			parametrosSalida.add(new ParamEvent("tsec", UtilidadesBeans.cifrarTsec(tsec, 20)));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			listlogger.add(UtilidadesLoggerKranon.envia+"GT"+UtilidadesLoggerKranon.error);
			listlogger.add(parametrosSalida.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(codigoRetorno);
			listlogger.add(error);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();			
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_GESTION_INICIAL).forward(request, response);

		}
	}


}
