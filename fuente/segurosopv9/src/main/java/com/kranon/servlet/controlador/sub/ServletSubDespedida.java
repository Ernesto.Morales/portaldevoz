package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.controlador.BeanControlador;
import com.kranon.bean.controlador.BeanSaludoControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonControlador;

/**
 * Servlet para la peticion de SUB-DESPEDIDA en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletSubDespedida extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	public ServletSubDespedida() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionDespedida", "");
			request.setAttribute("modoDespedida", "");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_DESPEDIDA).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SUB_DESPEDIDA";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		
		String locucionDespedida = "";
		String modoDespedida = "";

		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			// TODO que valen estos datos??
			String vdn = request.getParameter("PRM_vdn");
			String tipoCliente = request.getParameter("PRM_tipoCliente");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("vdn", vdn));
			parametrosEntrada.add(new ParamEvent("tipoCliente", tipoCliente));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean del Controlador en cuestion
			BeanControlador controlador = null;
			SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
			if (instanceControlador != null) {
				controlador = instanceControlador.getControlador(idServicio, idElemento);
			}

			if (controlador == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				error = "";

				throw new Exception("CONT_NULL");

			} else {
				// hemos recuperado bien el controlador

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("Controlador recuperado: " +
				// controlador.toString());
				/** FIN EVENTO - COMENTARIO **/

				// busco la despedida a reproducir por
				// defecto en el controlador
				BeanSaludoControlador despedida = controlador.getSaludo(controlador.getDespedidas(), vdn, tipoCliente);

				if (despedida == null) {
					// no hay despedida definida
					resultadoOperacion = "OK";
					error = "";				
				} else {
					// hay una despedida en el controlador
					
					/** INICIO EVENTO - ACCION **/
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					parametrosAdicionales.add(new ParamEvent("vdn", vdn));
					parametrosAdicionales.add(new ParamEvent("tipoCliente", tipoCliente));
					this.log.actionEvent("DESPEDIDA_DEFAULT", "OK", parametrosAdicionales);
					/** FIN EVENTO - ACION **/
					
					locucionDespedida = despedida.getValue();
					modoDespedida = despedida.getModoReproduccion();

					/** INICIO EVENTO - COMENTARIO **/
					// this.log.comment("Despedida recuperada: " +
					// despedida.toString());
					/** FIN EVENTO - COMENTARIO **/

					resultadoOperacion = "OK";
					error = "";
				}
			}

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("modoDespedida", modoDespedida);
			request.setAttribute("locucionDespedida", locucionDespedida);
			
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("locucionDespedida", locucionDespedida));
			parametrosSalida.add(new ParamEvent("modoDespedida", modoDespedida));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

	
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_DESPEDIDA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			locucionDespedida = "";
			modoDespedida = "";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("locucionDespedida", locucionDespedida);
			request.setAttribute("modoDespedida", modoDespedida);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("locucionDespedida", locucionDespedida));
			parametrosSalida.add(new ParamEvent("modoDespedida", modoDespedida));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_DESPEDIDA).forward(request, response);

		}
	}

}
