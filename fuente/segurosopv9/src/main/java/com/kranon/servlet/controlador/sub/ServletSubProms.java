package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonPromps;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.bean.promp.BeanProm;
import com.kranon.bean.promp.BeanPromps;

/**
 * Servlet para la peticion de SUB-BIENVENIDA en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletSubProms extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	
	public ServletSubProms() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionSaludo", "");
			request.setAttribute("modoSaludo", "");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_BIENVENIDA).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SUB_BIENVENIDA";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String datoShortcut = "";
		String modoSaludo = "";
		String locucionSaludo = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String resultadoProm=request.getParameter("PRM_PROM");
			//resultadoMAU="false";
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			SimpleDateFormat formatDia = new SimpleDateFormat("MMdd");
			String diaHoy = formatDia.format(new Date());

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("diaHoy", diaHoy));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
//			Unmarshall unmarshallProcess = new Unmarshall("123456", idElemento);
//			BeanRespuestasMau controlador = unmarshallProcess.unmarshallMauRespuestas("LINEABANCOMERv2");
			// Recuperamos el Bean del Servicio en cuestion
			BeanPromps controlador  = null;
			SingletonPromps instance = SingletonPromps.getInstance(log);
			if (instance != null) {
				controlador = instance.getServicio(idServicio);
			}
				if (controlador == null) {
					// error al recuperar el servicio
					resultadoOperacion = "KO";
					error = "";

					throw new Exception("CONT_NULL");

				} else {
					// hemos recuperado bien el controlador

					
					// busco el mensaje a reproducir por
					// defecto en el controlador
					BeanProm respuesta=null;
					for(BeanProm bean:controlador.getRespuesta()){
						if(bean.getOpcion().equals(resultadoProm)){
							respuesta=bean;
						}
					}
					if (respuesta == null) {
						// no hay bienvenida definida
						resultadoOperacion = "KO";
						error = "";				
					} else {
						// hay una bienvenida en el controlador

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("resultado", "AQUIRESULTADO"));
						this.log.actionEvent("RESULTADO", "OK", parametrosAdicionales);
						/** FIN EVENTO - ACION **/

						locucionSaludo = respuesta.getValue();
						modoSaludo = respuesta.getModoReproduccion();

						/** INICIO EVENTO - COMENTARIO **/
						// this.log.comment("Bienvenida recuperada: " +
						// bienvenida.toString());
						/** FIN EVENTO - COMENTARIO **/

						/** INICIO EVENTO - LOCUCION **/
						ArrayList<ParamEvent> parametrosAdicionaleLoc = new ArrayList<ParamEvent>();
						parametrosAdicionaleLoc.add(new ParamEvent("bienvenidaControlador", respuesta.toString()));
						this.log.speechEvent(modoSaludo, locucionSaludo, parametrosAdicionaleLoc);
						/** FIN EVENTO - LOCUCION **/

						resultadoOperacion = "OK";
						error = "";
					}
				
			}

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("locucionSaludo", locucionSaludo);
			request.setAttribute("modoSaludo", modoSaludo);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("locucionSaludo", locucionSaludo));
			parametrosSalida.add(new ParamEvent("modoSaludo", modoSaludo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			listlogger.add(UtilidadesLoggerKranon.mensaje+locucionSaludo);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_CONTROLADOR_PROMS).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			locucionSaludo = "";
			modoSaludo = "";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("locucionSaludo", locucionSaludo);
			request.setAttribute("modoSaludo", modoSaludo);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("locucionSaludo", locucionSaludo));
			parametrosSalida.add(new ParamEvent("modoSaludo", modoSaludo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			listlogger.add(parametrosSalida.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(error);
			listlogger.add(e.getMessage().substring(20));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();	
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_CONTROLADOR_RESPUESTA_MAU).forward(request, response);

		}
	}

}
