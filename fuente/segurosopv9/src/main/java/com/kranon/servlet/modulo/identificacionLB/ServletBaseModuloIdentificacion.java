package com.kranon.servlet.modulo.identificacionLB;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.serv.CommonLoggerService;

public abstract class ServletBaseModuloIdentificacion extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA MODULO IDENTIFICACION
	protected CommonLoggerService log; 
	
	private static String directorioModulo = "/Modulo-IdentificacionLB";

	protected static final String PAGE_INICIO_PARAMS = directorioModulo + "/dynamicvxml/MODULO-IDENTIF-INICIO-PARAMS.jsp";

	protected static final String PAGE_EJECUTAR = directorioModulo + "/dynamicvxml/MODULO-IDENTIF-EJECUTAR.jsp";
	
	protected static final String PAGE_SUB_TARJETA= directorioModulo + "/subdialogos/SUB-MODULO-IDENTIF-TARJETA.jsp";
	
	protected static final String PAGE_SUB_IDENTIFICA_CLIENTE_TARJETA = directorioModulo + "/subdialogos/SUB-MODULO-IDENTIF-IDENTIFICA-CLIENTE-TARJETA.jsp";
	
	protected static final String PAGE_FIN = directorioModulo + "/dynamicvxml/MODULO-IDENTIF-FIN.jsp";

	protected static final String PAGE_SUB_RECUPERA_TIPO_CANDADO = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-TIPO-CANDADO.jsp";
	
	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
