package com.kranon.servlet.modulo.autenticacionLB;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.serv.CommonLoggerService;

public abstract class ServletBaseModuloAutenticacion extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA MODULO AUTENTICACION
	protected CommonLoggerService log; 
	
	private static String directorioModulo = "/Modulo-AutenticacionLB";

	protected static final String PAGE_INICIO_PARAMS = directorioModulo + "/dynamicvxml/MODULO-AUTENTICACION-INICIO-PARAMS.jsp";
	
	protected static final String PAGE_EJECUTAR = directorioModulo + "/dynamicvxml/MODULO-AUTENTICACION-EJECUTAR.jsp";
	
	protected static final String PAGE_FIN = directorioModulo + "/dynamicvxml/MODULO-AUTENTICACION-FIN.jsp";

	protected static final String PAGE_SUB_BIOMETRIA = directorioModulo + "/subdialogos/SUB-MODULO-BIOMETRIA.jsp";

	protected static final String PAGE_SUB_BIOMETRIA_SOLICITA_HV = directorioModulo + "/subdialogos/SUB-MODULO-SOLICITA-HV.jsp";
	
	protected static final String PAGE_SUB_SOLICITA_REFERENCIA_SERIE = directorioModulo + "/subdialogos/SUB-MODULO-SOLICITA-REFERENCIA-SERIE.jsp";
	
	protected static final String PAGE_SUB_RECUPERA_TIPO_CANDADO = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-TIPO-CANDADO.jsp";
	
	protected static final String PAGE_SUB_RECUPERA_AUTENTICACION_SATISFACTORIA = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-AUTENTICACION-SATISFACTORIA.jsp";
	
	protected static final String PAGE_SUB_VALIDA_CANDADO = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-VALIDA-CANDADO.jsp";
	protected static final String PAGE_SUB_SALVAR_OTP = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-SAVE_OTP.jsp";

	protected static final String PAGE_SUB_RECUPERA_POSICION_GLOBAL = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-POSICION-GLOBAL.jsp";
	
	protected static final String PAGE_SUB_RECUPERA_ELECTRONIC_INFO_MAIL = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-ELECTRONIC-INFO-MAIL.jsp";
	protected static final String PAGE_SUB_RECUPERA_ELECTRONIC_INFO_TELF = directorioModulo + "/subdialogos/SUB-MODULO-AUTENTICACION-RECUPERA-ELECTRONIC-INFO-TELF.jsp";
	
	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
