package com.kranon.servlet.modulo.zonaPago;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kranon.bean.controlador.BeanControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.singleton.SingletonControlador;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de INICIO-PARAMS del MoDULO DE OBTENER COTIZACION
 *
 * @author aarce
 *
 */
public class ServletInicioParams extends ServletBaseModuloZonaPago {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletInicioParams() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", "MODULO-ZONA-PAGO-OPEN-PAY");
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO-ZONA-PAGO-OPEN-PAY";
		String intentosCliente;
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		String maxIntentosIdsssesion="3";
		try {

			// ** PARAMETROS DE ENTRADA
			//System.out.println("Entrando al inicioParams HOLAAAAA");
			//System.out.println("RUTA " + ServletBaseModuloZonaPago.PAGE_INICIO_PARAMS);
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
//			idElemento = idModulo;
			String lang = request.getParameter("VG_loggerServicio.idioma");
			intentosCliente= request.getParameter("VG_DatosPrecotizacion.idInt");
			// incluyo en sesion el idioma
			HttpSession session = request.getSession();
			session.setAttribute("lang", lang);
//			System.out.println("INTENTOSSSSSSS"+intentosCliente);
			// [201705_NMB] Recojemos la fecha de hoy para luego incluirla en el VG_stat del JSP
			Date fechaHoy = new Date();
			Long milisecInicioOperacion = fechaHoy.getTime();
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
			BeanControlador controlador = null;
			SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
			if (instanceControlador != null) {
				controlador = instanceControlador.getControlador(idServicio,idElemento);
			}

			if (controlador == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				error = "";

				throw new Exception("CONT_NULL");

			} else {
				// hemos recuperado bien el controlador

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("Controlador recuperado: " +
				// controlador.toString());
				/** FIN EVENTO - COMENTARIO **/

				maxIntentosIdsssesion = controlador.getIntentosIdSession();
//				System.out.println("INTENTOSSSSSSSARCHIVO"+maxIntentosIdsssesion);
				if ((maxIntentosIdsssesion != null)) {
					maxIntentosIdsssesion="3";
//					int intentosIdsssesionIn=Integer.parseInt(IntentosIdsssesion);
//					int intentosClienteIn=Integer.parseInt(intentosCliente);
//					if (intentosClienteIn<intentosIdsssesionIn) {
//						resultadoOperacion = "OK";
//						error = "";
//					}
//					else{
//						// no hay aviso de privacidad activado
//						resultadoOperacion = "OK";
//						error = "MAX_INT";
//					}
					

				} else {
					// hay aviso de privacidad activado en el controlador
					resultadoOperacion = "OK";
					error = "";
				}
			}
			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			/** INICIO EVENTO - STAT INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			this.log.statisticEvent(idModulo, "INIT", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE MODULO **/

			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("milisecInicioOperacion", milisecInicioOperacion);
			request.setAttribute("maxIntentosIdsssesion", maxIntentosIdsssesion);
			
			listlogger.add(resultadoOperacion);
			listlogger.add(intentosCliente);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			System.out.println("Antes del dispatcher " + request);
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_INICIO_PARAMS).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("maxIntentosIdsssesion", "3");
			/** INICIO EVENTO - INICIO DE ACCION **/
			String idAccion = "INICIO_PARAMS";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - FIN DE ACCION **/
			//System.out.println("Antes del dispatcher del catch" + request);
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_INICIO_PARAMS).forward(request, response);

		}
	}

}
