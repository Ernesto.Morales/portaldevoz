package com.kranon.servlet.modulo.identificacionLB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

/**
 * Servlet para la peticion de INICIO-PARAMS del MoDULO DE IDENTIFICACIoN
 *
 * @author aarce
 *
 */
public class ServletInicioParams extends ServletBaseModuloIdentificacion {

	private static final long serialVersionUID = 1L;

	public ServletInicioParams() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", "MODULO_IDENTIFICACION");
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_IDENTIFICACION";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			//idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idElemento = idModulo;

			String lang = request.getParameter("VG_loggerServicio.idioma");
			// incluyo en sesion el idioma
			HttpSession session = request.getSession();
			session.setAttribute("lang", lang);
			
			String tipoIdentificacion = request.getParameter("PRM_IN_IDENT_tipoIdentificacion");
			String intentosIdentificacion = request.getParameter("PRM_IN_IDENT_intentosIdentificacion");
			
			// [201705_NMB] Recojemos la fecha de hoy para luego incluirla en el VG_stat del JSP
			Date fechaHoy = new Date();
			Long milisecInicioOperacion = fechaHoy.getTime();
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("tipoIdentificacion", tipoIdentificacion));
			parametrosEntrada.add(new ParamEvent("intentosIdentificacion", intentosIdentificacion));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			/** INICIO EVENTO - STAT INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("tipoIdentificacion", tipoIdentificacion));
			parametrosAdicionalesStat.add(new ParamEvent("intentosIdentificacion", intentosIdentificacion));
			this.log.statisticEvent(idModulo, "INIT", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE MODULO **/

			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("milisecInicioOperacion", milisecInicioOperacion);

			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_INICIO_PARAMS).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			/** INICIO EVENTO - INICIO DE ACCION **/
			String idAccion = "INICIO_PARAMS";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - FIN DE ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_INICIO_PARAMS).forward(request, response);

		}
	}

}
