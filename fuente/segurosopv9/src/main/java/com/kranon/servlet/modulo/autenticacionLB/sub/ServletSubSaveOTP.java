package com.kranon.servlet.modulo.autenticacionLB.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.saveOTP.SaveOTP;
import com.kranon.servicios.saveOTP.bean.BeanOutSaveOTP;
import com.kranon.servlet.modulo.autenticacionLB.ServletBaseModuloAutenticacion;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para VALIDAR EL CANDADO en el Modulo de Autenticacion
 * 
 * @author aarce
 *
 */
public class ServletSubSaveOTP extends ServletBaseModuloAutenticacion {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletSubSaveOTP() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SALVAR_OTP).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "SAVE_OTP";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add("Peticion SAVE OTP");
		String tsecPrivado = "";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = "";
		String tipoError = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			String datoIntroducido = request.getParameter("PRM_datoIntroducido");
			String tsec=request.getParameter("VG_cliente.tsec");
			String idSession=request.getParameter("VG_cliente.idSession");
//			String telefono=request.getParameter("VG_cliente.telefonoContacto");
			String idCotiza=request.getParameter("VG_cliente.contratoIdentificacion");
			
			//truqueado para pasar autenticacion OK
//			numTarjeta = "4152313305021318";
//			numCliente = "D0145517";
			
			listlogger.add("SAVE OTP:"+UtilidadesLoggerKranon.tarjetaCuatro(datoIntroducido));
			listlogger.add(datoIntroducido);
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
//			da mensaje nip 
			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("OTP", UtilidadesBeans.cifrar(datoIntroducido,4)));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// *****************************************************
			// ***** INTEGRACIoN WS OPERACIoN GrantingTicket
			// *****************************************************

			ConexionResponse conexionResponse = null;

			// Datos FIJOS para la llamada inicial cuando no se tienen datos del cliente
			String backendSessionId = null;
			String clientId = null;
			String dialogId = "";
			String idSessionGenerado = "";

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("EXECUTING_VALIDATE_OTP", "", null);
			/** FIN EVENTO - ACCION **/
		
			
			/** LLAMO AL SERVICIO **/
			SaveOTP saveOTP= new SaveOTP(idLlamada, idServicio, idElemento);
			saveOTP.preparaPeticion(tsec, datoIntroducido,idCotiza,idSession);
			conexionResponse = saveOTP.ejecutaPeticion();

//			System.out.println("Despues de ejecutar otp....."+conexionResponse.toString());
//			System.out.println("Mensaje Error....."+conexionResponse.getMensajeError().toString());
			
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesAccion = new ArrayList<ParamEvent>();
			parametrosAdicionalesAccion.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
			if (conexionResponse.getErrorResponse() != null) {
				parametrosAdicionalesAccion.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
			}
			this.log.actionEvent("EXECUTED_VALIDATE_OTP", conexionResponse.getResultado(), parametrosAdicionalesAccion);
			/** FIN EVENTO - ACCION **/
			
			listlogger.add(""+conexionResponse.getCodigoRespuesta());
			/****/
//			conexionResponse.setResultado("KO");
//			ErrorResponse errorResponse = new ErrorResponse();
//			conexionResponse.setErrorResponse(errorResponse);
//			conexionResponse.setCodigoRespuesta(409);
//			conexionResponse.getErrorResponse().setErrorCode("160");
//			conexionResponse.getErrorResponse().setErrorMessage("0NUMERO DE INTENTOS SUPERADO, CLIENTE BLOQUEADO, Codigo: QYE0108");
//			conexionResponse.setResultado("403");
			/****/
			
			if (conexionResponse.getResultado().equals("OK")) {
				if (conexionResponse.getCodigoRespuesta() == 200) {
					BeanOutSaveOTP beanOutSaveOTP=saveOTP.parserSalida (conexionResponse.getMensajeRespuesta());
					if(beanOutSaveOTP.getCodError().equals("00000")){
//					tsecPrivado = gt.dameTsec(conexionResponse.getHeaderRespuesta());
//					llamar a Guardar OTP
					resultadoOperacion = "OK";
					codigoRetorno = "OK";
					error = "";
					tipoError = "";
					}
					else {
						resultadoOperacion = "KO";
						codigoRetorno = "KO";
						error = "";
						tipoError = "DATOS_INCORRECTOS";
					}
				} else {
					resultadoOperacion = "KO";
					codigoRetorno = "KO";
					error = "";
					tipoError = "DATOS_INCORRECTOS";
				}
			}  
			else if (conexionResponse.getCodigoRespuesta() == 409){
				// ha habido un error
				resultadoOperacion = "KO";
				codigoRetorno = "KO";
				tipoError = "DATOS_INCORRECTOS";
				error = "ERROR";
			}
			else {
				// ha habido un error
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR";
				tipoError = "ERROR_GENERICO";
			}

			
//			 TODO HARDCORE
//			resultadoOperacion = "OK";
//			codigoRetorno = "OK";
//			resultadoOperacion = "KO";
//			codigoRetorno = "KO";
//			error = "";
//			tipoError="";
//			tsecPrivado="TSEC_TRUCADO_PARA_PRUEBAS_FGAJLGKHKFGHURGHSKLADGHKSGHASDGH";
//			tipoError = "TOKEN_BLOQUEADO";
//			conexionResponse.setResultado("KO");
//			conexionResponse.setCodigoRespuesta(409);
			
//			resultadoOperacion = "KO";
//			codigoRetorno = "KO";
//			error = "";
//			tsecPrivado="TSEC_TRUCADO_PARA_PRUEBAS_FGAJLGKHKFGHURGHSKLADGHKSGHASDGH";
//			conexionResponse.setResultado("KO");
//			conexionResponse.setCodigoRespuesta(403);
//			tipoError = "TOKEN_BLOQUEADO";
			
//			System.out.println("ERROR = " + tipoError);
//			System.out.println("OTP resultado Operacion = " + resultadoOperacion);
//			System.out.println("OTP codigo Retorno = " + codigoRetorno);
			// TODO trucado para pruebas
			
			
			
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
//			request.setAttribute("tsecPrivado", tsecPrivado);
			request.setAttribute("tipoError", tipoError);
			

			/** INICIO EVENTO - FIN MODULO **/
//			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
//			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
//			parametrosSalida.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
//			if (conexionResponse.getErrorResponse() != null) {
//				parametrosSalida.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
//				parametrosSalida.add(new ParamEvent("errorMessage", conexionResponse.getErrorResponse().getErrorMessage()));
//			}
//			if (error != null && !error.equals("")) {
//				parametrosSalida.add(new ParamEvent("error", error));
//			}
//			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
//			if (conexionResponse.getResultado().equalsIgnoreCase("KO") || resultadoOperacion.equals("KO")) {
//				// si el resultado es KO, hubo una respuesta
//				parametrosAdicionales.add(new ParamEvent("tipoError", tipoError));
//				parametrosAdicionales.add(new ParamEvent("respuestaKO", conexionResponse.getErrorResponse() == null ? "null" : conexionResponse
//						.getErrorResponse().toString()));
//			}
//			parametrosSalida.add(new ParamEvent("tsecPrivado", UtilidadesBeans.cifrarTsec(tsecPrivado, 20)));			
//			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN MODULO **/
			listlogger.add("resultadoOperacion:"+resultadoOperacion);
			listlogger.add("codigoRetorno:"+ codigoRetorno);
			listlogger.add("error:"+ error);
			listlogger.add("tipoError:"+tipoError);
			logKranon.imprimeLogKranon(listlogger.toString());
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SALVAR_OTP).forward(request, response);

		} catch (final Exception e) {
			System.out.println("Entró a catch....");
			System.out.println(e.getMessage());
			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN MODULO **/
			listlogger.add(parametrosSalida.toString());
			listlogger.add(e.getMessage());
//			logKranon.imprimeLogKranon(listlogger.toString());
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SALVAR_OTP).forward(request, response);

		}
	}

}
