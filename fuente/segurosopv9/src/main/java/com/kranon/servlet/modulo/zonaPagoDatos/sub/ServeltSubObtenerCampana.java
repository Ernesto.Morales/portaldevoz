package com.kranon.servlet.modulo.zonaPagoDatos.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.BeanGramatica;
import com.kranon.bean.controlador.BeanControlador;
import com.kranon.bean.menu.BeanConfigOpcDtmfMenu;
import com.kranon.bean.menu.BeanConfigOpcMenu;
import com.kranon.bean.menu.BeanOpcionMenu;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.getCampanas.ObtenerCampana;
import com.kranon.servicios.getCampanas.bean.BeanCampana;
import com.kranon.servicios.getCampanas.bean.BeanCampanas;
import com.kranon.servicios.getCampanas.bean.BeanObtenerCampanaOut;
import com.kranon.servicios.getCampanas.bean.BeanPlanes;
import com.kranon.servicios.getCampanas.bean.UtilidadesCampanas;
import com.kranon.servlet.modulo.zonaPagoDatos.ServletBaseModuloZonaPagoDatos;
import com.kranon.singleton.SingletonControlador;

public class ServeltSubObtenerCampana extends ServletBaseModuloZonaPagoDatos {

	private static final long serialVersionUID = 1L;
	private static final String DATO_INCORRECTO="AUT-CLI-INFO-DATOS-NOMACTH";
	private static final String MAX_INT="AUT-CLI-INFO-DATOS-MAXIMOINT";
	private static final String ERROR="COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP";
	private CommonLoggerKranon logKranon;
	private static String varSplitBy = "\\|";
	private List<String> listlogger = new ArrayList<String>();
	public ServeltSubObtenerCampana() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_OBTENER_CAMPANA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_OBTENER_CAMPANA";
		String tsec="";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno="";
		String infoerror="";
		String opcionesjson="";
		int longTarjeta;
		String tarjeta;
		String opcionretorno="";
		String maxIntentosIdsssesion="3";
		String afiliacionId="";
		try {
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			String idCotiza=request.getParameter("VG_DatosCotizacion.idCotiza");
			tarjeta=request.getParameter("VG_DatosCotizacion.tarjeta");
			tsec = request.getParameter("VG_cliente.tsec");
			longTarjeta=(tarjeta!=null)?tarjeta.length():0;
//			BeanOpcionMenu opcion= llenaropcion("1", PAGO_UNICO, PAGO_UNICO, GRAMAR+"1", TEXPAGO_UNICO,"0");
//			opciones.add(opcion);
			ConexionResponse conexionResponse = null;
			ObtenerCampana obtenerCampana= new ObtenerCampana(idLlamada, idServicio, idElemento);
			obtenerCampana.preparaPeticion(tsec,idCotiza,tarjeta.substring(0,6),longTarjeta);
			conexionResponse = obtenerCampana.ejecutaPeticion();
//			HARCORE
//			conexionResponse.setResultado("OK");
			
			if (conexionResponse.getResultado().equals("OK")) {		
//				HARDCORE
//				String jsonhardcore="{\"listaCampanya\":[{\"opcion\":\"1\",\"tipoCampaña\":\"MESES_SIN_INTERESES\",\"listaPlanes\":[{\"valor\":\"0-12\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-3\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-6\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-9\",\"descripcion\":\"Meses Sin Intereses\"}]},{\"opcion\":\"2\",\"tipoCampaña\":\"PAGO_CON_PUNTOS\",\"listaPlanes\":[{\"valor\":\"0\",\"descripcion\":\"Puntos\"}]}],\"codError\":\"00000\"}";
//				BeanObtenerCampanaOut beanObtenerCampanaOut=obtenerCampana.parserSalida(jsonhardcore);
//				beanObtenerCampanaOut.setCodError("00032");
				BeanObtenerCampanaOut beanObtenerCampanaOut=obtenerCampana.parserSalida(conexionResponse.getMensajeRespuesta());
				afiliacionId=beanObtenerCampanaOut.getAfiliacion();
				if(beanObtenerCampanaOut.getCodError().equals("00000")){
				// la peticion ha ido OK-200
					
//					TODO  agregar verificar el bean de AMEX
					if(longTarjeta==15 && beanObtenerCampanaOut.getTipoTarjeta().equals("AMEX")){
						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";	
					}
					else if(longTarjeta==16){
						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";	
					}
					else{
						infoerror=DATO_INCORRECTO;
						resultadoOperacion = "ERROR";
						codigoRetorno = "KO";
						error = "ERRORC";
					}
//					HARDCORE
//					opcionesjson=jsonhardcore;
					opcionesjson=conexionResponse.getMensajeRespuesta();
					opcionesjson=opcionesjson.replaceAll("\"", "T1LDE");
					opcionesjson=opcionesjson.replaceAll(",", "COM4S");
				}
				else{
//				
					infoerror=DATO_INCORRECTO;
					resultadoOperacion = "ERROR";
					codigoRetorno = "KO";
					error = "ERRORC";
				}
//				if(longTarjeta==15 && beanObtenerCampanaOut.getTipoTarjeta()=="AMEX"){
//					resultadoOperacion = "OK";
//					codigoRetorno = "OK";
//					error = "";	
//				}
//				else{
//					infoerror="";
//					resultadoOperacion = "OK";
//					codigoRetorno = "OK";
//					error = " ";
//				}
			}
			
			else if(conexionResponse.getCodigoRespuesta()== 404 || conexionResponse.getCodigoRespuesta()== 500){
				resultadoOperacion = "KO";
				codigoRetorno = "KO";
				error = "ERROR";
				infoerror=ERROR;
			}
			
			else{
				resultadoOperacion = "KO";
				codigoRetorno = "KO";
				error = "ERROR";
				infoerror=ERROR;
			}
			if(error=="ERROR"){
				BeanControlador controlador = null;
				SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
				if (instanceControlador != null) {
					controlador = instanceControlador.getControlador(idServicio,idElemento);
				}

				if (controlador != null) {
					// hemos recuperado bien el controlador
					maxIntentosIdsssesion = controlador.getIntentosIdSession();
					} 	
				infoerror=ERROR;
			}
			/** INICIO EVENTO - ACCION **/
//			String idAccion = "SUB_TARJETA";
//			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
//			parametrosAdicionales.add(new ParamEvent("","" ));
//			if (error != null && !error.equals("")) {
//				parametrosAdicionales.add(new ParamEvent("error", error));
//			}
//			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/
			request.setAttribute("resultadoOperacion", resultadoOperacion);	
			request.setAttribute("codigoRetorno", codigoRetorno);	
			request.setAttribute("error", error);
			request.setAttribute("opcionesjson", opcionesjson);	
			request.setAttribute("infoerror", infoerror);	
			request.setAttribute("maxint", maxIntentosIdsssesion);
			request.setAttribute("afiliacionId", afiliacionId);
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_OBTENER_CAMPANA).forward(request, response);

		} catch (final Exception e) {
			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);	
			request.setAttribute("codigoRetorno", codigoRetorno);	
			request.setAttribute("error", error);
			request.setAttribute("opcionesjson", opcionesjson);
			request.setAttribute("infoerror", infoerror);
			request.setAttribute("maxint", maxIntentosIdsssesion);
			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_OBTENER_CAMPANA).forward(request, response);

		}
	}
	
	
	
}
