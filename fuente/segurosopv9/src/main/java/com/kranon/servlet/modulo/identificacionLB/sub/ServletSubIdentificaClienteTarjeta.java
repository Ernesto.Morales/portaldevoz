package com.kranon.servlet.modulo.identificacionLB.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servlet.modulo.identificacionLB.ServletBaseModuloIdentificacion;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para IDENTIFICAR AL CLIENTE en el Modulo de Identificacion
 * 
 * @author aarce
 *
 */
public class ServletSubIdentificaClienteTarjeta extends ServletBaseModuloIdentificacion {

	private static final long serialVersionUID = 1L;
	protected CommonLoggerKranon logKranon;
	protected List<String> listlogger = new ArrayList<String>();
	public ServletSubIdentificaClienteTarjeta() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_SUB_IDENTIFICA_CLIENTE_TARJETA)
					.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "IDENTIFICA_PROXY_SERVICE_WYH5";
		String tsec = "";
		String idError="";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = " ";
		String bloqueado = "NO";
		String tipoError = " ";
		String intento="0";
//		ProxyServiceWYH5RestOutDto datosCliente = null;
	
		try {
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			tsec = request.getParameter("PRM_cliente.tsec");
			intento=request.getParameter("PRM_numIntentos");
			String numTarjetaIntroducido="";
			numTarjetaIntroducido = request.getParameter("PRM_numTarjetaIntroducido");
			listlogger.add(UtilidadesLoggerKranon.tarjeta+UtilidadesLoggerKranon.tarjetaCuatro(numTarjetaIntroducido));
			listlogger.add("intento:"+(intento!=null?Integer.parseInt(intento)+1:intento));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("numTarjetaIntroducido", UtilidadesBeans.cifrar(numTarjetaIntroducido,5)));
			parametrosEntrada.add(new ParamEvent("tsec", UtilidadesBeans.cifrarTsec(tsec, 20)));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// *****************************************************
			// ***** INTEGRACIoN WS OPERACIoN ProxyServiceWHY5
			// *****************************************************
			ConexionResponse conexionResponse = null;

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("EXECUTING_PROXY_SERVICE_WYH5", "", null);
			
			/** FIN EVENTO - ACCION **/

			/** LLAMO AL SERVICIO **/
			// [20161216-NMB] SE INCLUYE EL idServicio PARA ESCRIBIR LAS TRAZAS DE MONITORIZACION
			listlogger.add(UtilidadesLoggerKranon.envia+"WYH5");
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			listlogger.add(UtilidadesLoggerKranon.responde+"WYH5");
//			ProxyServiceWYH5 proxy = new ProxyServiceWYH5(idLlamada, idServicio, idElemento);
//			proxy.preparaPeticion(tsec, numTarjetaIntroducido, "", "");
//			conexionResponse = proxy.ejecutaPeticion();

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesAccion = new ArrayList<ParamEvent>();
			parametrosAdicionalesAccion.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
			if (conexionResponse.getErrorResponse() != null) {
				parametrosAdicionalesAccion.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
			}
			this.log.actionEvent("EXECUTED_PROXY_SERVICE_WYH5", conexionResponse.getResultado(), parametrosAdicionalesAccion);
			/** FIN EVENTO - ACCION **/
//			-------------------------
//			conexionResponse.setResultado("OK");
//			conexionResponse.setCodigoRespuesta(404);
//			____________________________________
			listlogger.add(""+conexionResponse.getCodigoRespuesta());
			listlogger.add(conexionResponse.getMensajeError());		
			if(conexionResponse.getCodigoRespuesta()==404 || conexionResponse.getCodigoRespuesta()==500){
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(WS_PROXY_SERVICE_WHY5)";
				idError="ERROR_IDENT";
//				listlogger.add(resultadoOperacion);
//				listlogger.add(""+conexionResponse.getCodigoRespuesta());
//				listlogger.add(conexionResponse.getMensajeRespuesta());
//				listlogger.add(conexionResponse.getMensajeError());
				
				
			}
			if(conexionResponse.getCodigoRespuesta()==409){
				resultadoOperacion = "KO";
				codigoRetorno = " ";
				error = "ERROR_EXT(WS_PROXY_SERVICE_WHY5)";
				idError="ERROR_IDENT_TAR";
//				listlogger.add(resultadoOperacion);
//				listlogger.add(""+conexionResponse.getCodigoRespuesta());
//				listlogger.add(conexionResponse.getMensajeRespuesta());
//				listlogger.add(conexionResponse.getMensajeError());
			}
			
//			else if (conexionResponse.getResultado().equals("OK")) {
//				resultadoOperacion = "OK";
//				codigoRetorno = "OK";
//				error = "";
////				datosCliente = proxy.parserSalida(conexionResponse.getMensajeRespuesta());
//				this.log.actionEvent("DATOS_CLIENTE", datosCliente.toString(), null);
//				
//				if(datosCliente.getKndbish5().get(0).getNumecte() == null || datosCliente.getKndbish5().get(0).getNumecte().equals("")){
					// hemos obtenido un 200-OK pero el id de cliente llega a nulo o vacio, no podemos dar por valida la identificacion			
					// ABALFARO_20170405
//					resultadoOperacion = "KO";
//					codigoRetorno = "KO";
//					error = "";
//					idError="ERROR_IDENT";
////					listlogger.add(resultadoOperacion);
////					listlogger.add(conexionResponse.getMensajeRespuesta());
////					listlogger.add(conexionResponse.getMensajeError());
////					if(datosCliente!=null){
////						listlogger.add(datosCliente.toString());
////					}
//				}else {
//					// hemos obtenido un 200-OK y el id cliente viene relleno
//					// comprobamos que el resto de datos no sean nulos, y en ese caso asignaremos a vacio
//					if(datosCliente.getKndbish5().get(0).getNombcte() == null){
//						datosCliente.getKndbish5().get(0).setNombcte("");
//					}
//					if(datosCliente.getKndbish5().get(0).getPoblaci() == null){
//						datosCliente.getKndbish5().get(0).setPoblaci("");
//					}
//					if(datosCliente.getKndbish5().get(0).getCalldom() == null){
//						datosCliente.getKndbish5().get(0).setCalldom("");
//					}
//					if(datosCliente.getKndbish5().get(0).getNumeint() == null){
//						datosCliente.getKndbish5().get(0).setNumeint("");
//					}
//					if(datosCliente.getKndbish5().get(0).getCodesta() == null){
//						datosCliente.getKndbish5().get(0).setCodesta("");
//					}
//					if(datosCliente.getKndbish5().get(0).getCodpost() == null){
//						datosCliente.getKndbish5().get(0).setCodpost("");
//					}
//					if(datosCliente.getKndbish5().get(0).getSegment() == null){
//						datosCliente.getKndbish5().get(0).setSegment("");
//					}
//					if(datosCliente.getKndbish5().get(0).getRfc() == null){
//						datosCliente.getKndbish5().get(0).setRfc("");
//					}
//					if(datosCliente.getKndbish5().get(0).getHomonim() == null){
//						datosCliente.getKndbish5().get(0).setHomonim("");
//					}
//				}
//				if(resultadoOperacion.equals("OK")){
//					resultadoOperacion = "OK";
//					codigoRetorno = "OK";
//					error = "";
//					idError="";
//					// la identificacion ha ido bien
//
//					String nombreCliente = datosCliente.getKndbish5().get(0).getNombcte();
//					// ABALFARO_20170330 rellenamos el cliente a vacio en caso de que llegue nulo
//					if(nombreCliente == null){
//						nombreCliente = "";
//					}
//					nombreCliente = nombreCliente.trim();
//					this.log.comment("nombreCliente=" + nombreCliente);
//
//					StringTokenizer nombreClienteST = new StringTokenizer(nombreCliente);
//					String nombreClienteFinal = "";
//					while (nombreClienteST.hasMoreElements()) {
//						if (!nombreClienteFinal.equals("")) {
//							nombreClienteFinal = nombreClienteFinal + " ";
//						}
//						nombreClienteFinal = nombreClienteFinal + nombreClienteST.nextElement();
//					}
//					this.log.comment("nombreClienteFinal=" + nombreClienteFinal);
//
//					datosCliente.getKndbish5().get(0).setNombcte(nombreClienteFinal);
////					listlogger.add(resultadoOperacion);
////					if(datosCliente!=null){
////						listlogger.add(datosCliente.toString());
////					}
//				}
////				-------------DATOSPRUEBA CLIENTE INVALIDO----------------------------------
////				datosCliente.getKndbish5().get(0).setErrcont("MCE0107");
////				----------------------------------------------
//				if(datosCliente.getKndbish5().get(0).getErrcont()!= null)
//					if(datosCliente.getKndbish5().get(0).getErrcont().equals("MCE0107")){
////						datosCliente.getKndbish5().get(0).getDescerr();
//						resultadoOperacion = "KO";
//						codigoRetorno = "KO";
//						error = "";
//						idError="ERROR_IDENT_TAR";
//						listlogger.add("TARJETA NO EXISTE ");
////						listlogger.add(conexionResponse.getMensajeRespuesta());
////						listlogger.add(conexionResponse.getMensajeError());
////						if(datosCliente!=null){
////							listlogger.add(datosCliente.toString());
////						}
//						
//					}				
//			}
//			else{
//				resultadoOperacion = "KO";
//				codigoRetorno = "ERROR";
//				error = "ERROR_EXT(WS_PROXY_SERVICE_WHY5)";
//				idError="ERROR_IDENT";
////				listlogger.add(resultadoOperacion);
////				listlogger.add(idError);
////				listlogger.add(""+conexionResponse.getCodigoRespuesta());
////				listlogger.add(conexionResponse.getMensajeError());
//			}
//			
//			
//
//			// TODO Trucado para pruebas
////			 String salida =
////			 "{\"KNDBISH5\":[{\"numecte\":\"D0094312\",\"nombcte\":\"ERYK                FIGUEROA          MTP\",\"calldom\":\"AZUL                     VERDE\",\"numeint\":\"3\",\"poblaci\":\"CUAUHTEMOC\",\"codesta\":\"DF\",\"codpost\":\"06470\"}]}";
////			 resultadoOperacion = "OK";
////			 codigoRetorno = "OK";
////			 error = "";
////			 datosCliente = proxy.parserSalida(salida);
////			 idError="";
//			// -----------------------
//
//			// Aado parametros de salida a la request
//			request.setAttribute("datosCliente", datosCliente);
//			request.setAttribute("resultadoOperacion", resultadoOperacion);
//			request.setAttribute("codigoRetorno", codigoRetorno);
//			request.setAttribute("error", error);
//			request.setAttribute("bloqueado", bloqueado);
//			request.setAttribute("numTarjetaIntroducido", numTarjetaIntroducido);
//
//			
//			request.setAttribute("datosCliente", datosCliente);
//			request.setAttribute("resultadoOperacion", resultadoOperacion);
//			request.setAttribute("codigoRetorno", codigoRetorno);
//			request.setAttribute("error", error);
//			request.setAttribute("tipoError", tipoError);
//			request.setAttribute("idError", idError);
//			listlogger.add(resultadoOperacion);
//			listlogger.add(error);
//			listlogger.add(tipoError);
//			if(datosCliente!=null){
//				if(datosCliente.getKndbish5().get(0).getErrcont() != null){
//					listlogger.add("Errcont: "+datosCliente.getKndbish5().get(0).getErrcont());
//				}
//				else{
//					listlogger.add("datosCliente: "+datosCliente.getKndbish5().get(0));
//				}
//				if(datosCliente.getKndbish5().get(0).getDescerr() != null)
//					listlogger.add("Descerr: "+datosCliente.getKndbish5().get(0).getDescerr());
//				else{
//					listlogger.add("datosCliente: "+datosCliente.getKndbish5().get(0));
//				}
////				listlogger.add("datosCliente: "+datosCliente.getKndbish5().get(0));
//			}
//			else{
//				listlogger.add("datosCliente: no trae datos");
//			}
//			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_SUB_IDENTIFICA_CLIENTE_TARJETA)
					.forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			idError="ERROR_IDENT";
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("bloqueado", bloqueado);
			request.setAttribute("idError", idError);
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
//			parametrosAdicionales.add(new ParamEvent("datosCliente", datosCliente == null ? "null" : datosCliente.toString()));
			parametrosSalida.add(new ParamEvent("bloqueado", bloqueado));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN MODULO **/
			listlogger.add(resultadoOperacion);
			listlogger.add(idError);
			listlogger.add("bloquead:"+bloqueado);
			listlogger.add(e.getMessage().substring(20));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseModuloIdentificacion.PAGE_SUB_IDENTIFICA_CLIENTE_TARJETA)
					.forward(request, response);

		}
	}

}
