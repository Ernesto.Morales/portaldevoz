package com.kranon.servlet.modulo.zonaPagoDatos;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.serv.CommonLoggerService;

public abstract class ServletBaseModuloZonaPagoDatos extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA MODULO IDENTIFICACION
	protected CommonLoggerService log; 
	
	private static String directorioModulo = "/Modulo-ZonaPagoDatos";
	
	
	protected static final String PAGE_INICIO_PARAMS = directorioModulo + "/dynamicvxml/MODULO-ZPDATOS-INICIO-PARAMS.jsp";

	protected static final String PAGE_EJECUTAR = directorioModulo + "/dynamicvxml/MODULO-ZPDATOS-EJECUTAR.jsp";
	
	protected static final String PAGE_SUB_CONFIRMA_MENU= directorioModulo + "/subdialogos/SUB-MODULO-ZPDATOS-CONFIRMARMONTO.jsp";
	
	protected static final String PAGE_SUB_TARJETA= directorioModulo + "/subdialogos/SUB-MODULO-ZPDATOS-TARJETA.jsp";
	
	protected static final String PAGE_SUB_MESTARJETA= directorioModulo + "/subdialogos/SUB-MODULO-ZPDATOS-MESTARJETA.jsp";
	
	protected static final String PAGE_SUB_ANOTARJETA= directorioModulo + "/subdialogos/SUB-MODULO-ZPDATOS-ANOTARJETA.jsp";
	
	protected static final String PAGE_SUB_CCVTARJETA= directorioModulo + "/subdialogos/SUB-MODULO-ZPDATOS-CVVTARJETA.jsp";
	
	protected static final String PAGE_SUB_OBTENER_CAMPANA= directorioModulo + "/subdialogos/SUB-MODULO-ZPOBTENER-CAMPANA.jsp";
	
	protected static final String PAGE_SUB_SELECCIONAR_CAMPANA= directorioModulo + "/subdialogos/SUB-MODULO-ZPDSELECCIONAR-CAMPANA.jsp";
	
	protected static final String PAGE_FIN = directorioModulo + "/dynamicvxml/MODULO-ZPDATOS-FIN.jsp";
	
	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
