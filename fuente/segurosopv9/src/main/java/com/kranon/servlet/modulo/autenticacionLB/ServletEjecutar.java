package com.kranon.servlet.modulo.autenticacionLB;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

/**
 * Servlet para la peticion de EJECUTAR en el Modulo de Autenticacion
 * 
 * @author aarce
 *
 */
public class ServletEjecutar extends ServletBaseModuloAutenticacion {

	private static final long serialVersionUID = 1L;

	public ServletEjecutar() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_EJECUTAR).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_AUTENTICACION";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");

			String tipoAutenticacion = request.getParameter("VG_tipoAutenticacion");
			String intentosAutenticacion = request.getParameter("VG_intentosAutenticacion");
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - ACCION **/
			String idAccion = "GET_INFO_EJECUTAR";
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosEntrada.add(new ParamEvent("intentosAutenticacion", intentosAutenticacion));
			this.log.actionEvent(idAccion, "OK", parametrosEntrada);
			/** FIN EVENTO - ACCION **/

			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";
			// aado el controlador a la request
			request.setAttribute("tipoAutenticacion", tipoAutenticacion);
			request.setAttribute("intentosAutenticacion", intentosAutenticacion);
			request.setAttribute("idServicio", idServicio);
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_EJECUTAR).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - ACCION **/
			String idAccion = "GET_INFO_EJECUTAR";
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosSalida);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_EJECUTAR).forward(request, response);

		}
	}

}
