package com.kranon.servlet.controlador;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;

/**
 * Servlet para la descarga de archivos Wav
 *
 * @author mrramirez
 *
 */
public class ServletDescargaWav  extends ServletBaseControlador{
	
	private static final long serialVersionUID = 1L;

	public ServletDescargaWav() {

		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el idLlamada
					
			this.doPost(request, response);

		} catch (final Exception e) {

			// como el servlet no realiza ninguna operacion, saltamos a la parte VXML
			//this.getServletContext().getRequestDispatcher(ServletBaseEnrutador.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fileNameWav ="";
		String nomFileWav = "";
		String path = "";	
		FileInputStream fis = null;
		OutputStream os = null;
		File fileWav = null;
		String idModulo = "DESCARGA_FILE_WAV";
		String idServicio = "";
		String resultadoOperacion = "KO";
		String[] datos = null;
		String idLlamada = "";
		String idElemento = "USAC";

		try {	
			datos = request.getParameter("fileWav").split("\\|");
			
			// Obtengo el nombre del servicio
			idServicio = datos[0];
			// Obtengo el nombre del archivo wav
			nomFileWav =  datos[1];
			
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
			
			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("idServicio", idServicio));
			parametrosEntrada.add(new ParamEvent("nomFileWav", nomFileWav));
			log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
					
			path = ArqSpringContext.getPropiedad("PATH_FILE_WAV");			
			fileNameWav =  path + nomFileWav;
			int BUFF_SIZE = 4096;
			byte[] buffer = new byte[BUFF_SIZE];
			fileWav = new File(fileNameWav);
			fis = new FileInputStream(fileWav);
			
			
			response.setHeader("Cache-Control", "private,no-cache,no-store");
			response.setContentType("audio/wav");
			//response.setHeader("Content-Disposition", "attachment; filename=Identificacion.wav");
			response.setHeader("Content-Disposition", "filename=\""+fileWav.getName()+"");
			response.setHeader("Accept-Ranges", "bytes");
			response.setContentLength((int) fileWav.length());
			os = response.getOutputStream();
			
			long  byteRead = 0L;
			for(int n=0; -1 != (n=fis.read(buffer)); ){
				os.write(buffer, 0, n);
				byteRead +=n;
			}
		    os.flush();
		    resultadoOperacion = "OK";
		   
			

		} catch (final Exception e) {
						
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("error", e.toString()));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - ERROR **/
			
		}finally{
			os.close();
			fis.close();
			/** INICIO EVENTO - FIN DE MODULO **/
			log.endModuleService(idModulo, resultadoOperacion, null, null);
			/** FIN EVENTO - FIN DE MODULO **/
		}

	}
}
