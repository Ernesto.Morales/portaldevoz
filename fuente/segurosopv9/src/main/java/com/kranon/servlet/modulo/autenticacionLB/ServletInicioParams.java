package com.kranon.servlet.modulo.autenticacionLB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de INICIO-PARAMS del MoDULO DE AUTENTICACIoN
 *
 * @author abalfaro
 *
 */
public class ServletInicioParams extends ServletBaseModuloAutenticacion {
	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletInicioParams() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada
			this.doPost(request, response);
		} catch (final Exception e) {
			// Aado parametros de salida a la request
			request.setAttribute("idElemento", "MODULO_AUTENTICACION");
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_AUTENTICACION";
		String bandera="false";
		String numTar="";
		String numClien="";
		String idServicioShortcut="";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		String datoShortcut = "";
		try {
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			//idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idElemento = idModulo;
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			String lang = request.getParameter("VG_loggerServicio.idioma");
			// incluyo en sesion el idioma
			HttpSession session = request.getSession();
			session.setAttribute("lang", lang);

			String tipoAutenticacion = request.getParameter("PRM_IN_AUT_tipoAutenticacion");
			String intentosAutenticacion = request.getParameter("PRM_IN_AUT_intentosAutenticacion");
			datoShortcut = request.getParameter("VG_Short");
			idServicioShortcut=request.getParameter("VG_serv");
			idServicioShortcut=idServicio(request);
			
			ArrayList<String> parameterNames = new ArrayList<String>();
			Enumeration enumeration = request.getParameterNames();
			Integer contador = 0;
			while (enumeration.hasMoreElements()) {
				String parameterName = (String) enumeration.nextElement(); 
				parameterNames.add(parameterName); 
				contador = contador + 1;
			}
			
			ArrayList<String> parameterNames1 = new ArrayList<String>();
			Enumeration enumeration1 = request.getAttributeNames();
			Integer contador1 = 0;
			while (enumeration1.hasMoreElements()) {
				String parameterName1 = (String) enumeration1.nextElement();
				parameterNames1.add(parameterName1);
				contador1 = contador1 + 1;
			}
			
			// [201705_NMB] Recojemos la fecha de hoy para luego incluirla en el VG_stat del JSP
			Date fechaHoy = new Date();
			Long milisecInicioOperacion = fechaHoy.getTime();
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosEntrada.add(new ParamEvent("intentosAutenticacion", intentosAutenticacion));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/
			
			/** INICIO EVENTO - STAT INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosAdicionalesStat = new ArrayList<ParamEvent>();
			parametrosAdicionalesStat.add(new ParamEvent("tipoAutenticacion", tipoAutenticacion));
			parametrosAdicionalesStat.add(new ParamEvent("intentosAutenticacion", intentosAutenticacion));
			this.log.statisticEvent(idModulo, "INIT", parametrosAdicionalesStat);
			/** FIN EVENTO - STAT INICIO DE MODULO **/

			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("milisecInicioOperacion", milisecInicioOperacion);
			
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_INICIO_PARAMS).forward(request, response);

		} catch (final Exception e) {
			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			
			/** INICIO EVENTO - INICIO DE ACCION **/
			String idAccion = "INICIO_PARAMS";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - FIN DE ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}
	
	private String idServicio(ServletRequest request){
		String idServicioShortcut;
		String llaveservicioCTI=request.getParameter("VG_datosUUI.campania");
		String llaveservicioUUI=request.getParameter("VG_datosCTI.campania");
		if(llaveservicioCTI==null || llaveservicioCTI==""){
			if(llaveservicioUUI==null || llaveservicioUUI==""){
				idServicioShortcut=request.getParameter("VG_serv");
				listlogger.add("Servicio: "+idServicioShortcut);
			}
			else{
				if(llaveservicioUUI.length()==4 && !llaveservicioUUI.equals("####")){
					idServicioShortcut=llaveservicioUUI;
					listlogger.add("Servicio: "+idServicioShortcut);
				}
				else{
					idServicioShortcut=request.getParameter("VG_serv");
					listlogger.add("Servicio: "+idServicioShortcut);
				}
			}
		}
		else{
			if(llaveservicioCTI.length()==4 && !llaveservicioCTI.endsWith("####")){
				idServicioShortcut=llaveservicioCTI;
				listlogger.add("Servicio: "+idServicioShortcut);
			}
			else{
				idServicioShortcut=request.getParameter("VG_serv");
				listlogger.add("Servicio: "+idServicioShortcut);
			}
		}
		return idServicioShortcut;
	}
}
