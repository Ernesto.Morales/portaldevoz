package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.cti.ProcesoCTIBlock;
import com.kranon.servicios.cti.bean.BeanDatosCTI;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.util.UtilidadesLoggerKranon;

public class ServletSubBloquearCTI  extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;
	private String llamadaId = "";
	private String elementoId = "";
	private String servicioId = "";
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletSubBloquearCTI() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionSaludo", "");
			request.setAttribute("modoSaludo", "");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_BLOQUEARTI).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idElemento = "";
		String idTipo="";
		String idServicio="";
		String tsec="";
		String opc="";

		String idModulo = "SUB_BLOCK_CTI";
		
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String codigoRetorno = "";
		String error = "";
		try {
			
//			listlogger.add(UtilidadesLoggerKranon.obtiene+);
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idTipo = request.getParameter("VG_loggerServicio.tipo");
			logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
			logKranon.setIdServicio(idLlamada);
			listlogger.add(idModulo);
			llamadaId = idLlamada;
			elementoId = idElemento;
			servicioId = idServicio;
			
			BeanDatosCTI beanDatosCTI=verificarRetornoCTI(request);
			BeanDatosUUI beaDatosUUI=verificarDAtosRetornoUUi(request);

			ProcesoCTIBlock procCti = new ProcesoCTIBlock(idLlamada, idElemento, idServicio);
			//Se invoca al servicio del CTI
			String respuesta = procCti.BlockDTMF(beaDatosUUI.getLlave());
//			String respuesta ="OK";
//			System.out.println("-----------respuesta"+respuesta);
			if(respuesta == null){
				resultadoOperacion = "KO";
				error = "ERROR_EXT(DATOS_CTI_BLOCK)";				
			}
			else if(respuesta.equals("OK"))
			{
				//Se obtienen las respuestas
				resultadoOperacion = "OK";
				error = "";		
				codigoRetorno="OK";
			}
			else{
				resultadoOperacion = "KO";
				codigoRetorno="ERROR";
				error = "ERROR_EXT(DATOS_CTI_BLOCK)";
			}
//			HARCORE
			resultadoOperacion ="OK";
				listlogger.add(resultadoOperacion);
				listlogger.add(error);
				logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
				listlogger.clear();
				request.setAttribute("resultadoOperacion",resultadoOperacion);
				request.setAttribute("codigoRetorno",codigoRetorno);
				request.setAttribute("error",error);
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_BLOQUEARTI).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion",resultadoOperacion);
			request.setAttribute("codigoRetorno",codigoRetorno);
			request.setAttribute("error",error);
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			listlogger.add(parametrosSalida.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_BLOQUEARTI).forward(request, response);

		}
	}

	private BeanDatosCTI verificarRetornoCTI(HttpServletRequest request){
		BeanDatosCTI beanDatosCTI = new BeanDatosCTI();
		beanDatosCTI.setAni(request.getParameter("VG_datosCTI.ani"));
		beanDatosCTI.setDnis(request.getParameter("VG_datosCTI.dnis"));
		beanDatosCTI.setNumeroIvr(request.getParameter("VG_datosCTI.numeroIvr"));
		beanDatosCTI.setPuertoIvr(request.getParameter("VG_datosCTI.puertoIvr"));
		beanDatosCTI.setLlave(request.getParameter("VG_datosCTI.llave"));
		beanDatosCTI.setField(request.getParameter("VG_datosCTI.field"));
		beanDatosCTI.setVdnOrigen(request.getParameter("VG_datosCTI.vdnOrigen"));
		beanDatosCTI.setVdnDestino(request.getParameter("VG_datosCTI.vdnDestino"));
		beanDatosCTI.setSkill(request.getParameter("VG_datosCTI.skill"));
		beanDatosCTI.setAsa(request.getParameter("VG_datosCTI.asa"));
		beanDatosCTI.setIdAplicacion(request.getParameter("VG_datosCTI.idAplicacion"));
		beanDatosCTI.setXferId(request.getParameter("VG_datosCTI.xferId"));
		beanDatosCTI.setSegmentoCliente(request.getParameter("VG_datosCTI.segmentoCliente"));
		beanDatosCTI.setProducto(request.getParameter("VG_datosCTI.producto"));
		beanDatosCTI.setLlaveAccesoFront(request.getParameter("VG_datosCTI.llaveAccesoFront"));
		beanDatosCTI.setNumCliente(request.getParameter("VG_datosCTI.numCliente"));
		beanDatosCTI.setTiempoLlamada(request.getParameter("VG_datosCTI.tiempoLlamada"));
		beanDatosCTI.setLlaveSegmento(request.getParameter("VG_datosCTI.llaveSegmento"));
		beanDatosCTI.setIpAsesor(request.getParameter("VG_datosCTI.ipAsesor"));
		beanDatosCTI.setLibre3(request.getParameter("VG_datosCTI.libre3"));
		beanDatosCTI.setTelCtePersonas(request.getParameter("VG_datosCTI.telCtePersonas"));
		beanDatosCTI.setMetodoAutenticacion(request.getParameter("VG_datosCTI.metodoAutenticacion"));
		beanDatosCTI.setLibre4(request.getParameter("VG_datosCTI.libre4"));
		beanDatosCTI.setTimestamp(request.getParameter("VG_datosCTI.timestamp"));
		beanDatosCTI.setComentario(request.getParameter("VG_datosCTI.comentario"));
		beanDatosCTI.setCampania(request.getParameter("VG_datosCTI.campania"));
		return beanDatosCTI;
	}
	private BeanDatosUUI verificarDAtosRetornoUUi(HttpServletRequest request){
		BeanDatosUUI beanDatosUUI = new BeanDatosUUI();
		beanDatosUUI.setLlave(request.getParameter("VG_datosUUI.llave"));
		beanDatosUUI.setLlaveSegmento(request.getParameter("VG_datosUUI.llaveSegmento"));
		beanDatosUUI.setXferId(request.getParameter("VG_datosUUI.xferId"));
		beanDatosUUI.setSegmentoCliente(request.getParameter("VG_datosUUI.segmentoCliente"));
		beanDatosUUI.setCanal(request.getParameter("VG_datosUUI.canal"));
		beanDatosUUI.setNumCliente(request.getParameter("VG_datosUUI.numCliente"));
		beanDatosUUI.setLlaveAccesoFront(request.getParameter("VG_datosUUI.llaveAccesoFront"));
		beanDatosUUI.setMetodoAutenticacion(request.getParameter("VG_datosUUI.metodoAutenticacion"));
		beanDatosUUI.setNumeroSesion(request.getParameter("VG_datosUUI.numeroSesion"));
		beanDatosUUI.setCampania(request.getParameter("VG_datosUUI.campania"));
		return beanDatosUUI;
	}
	

}
