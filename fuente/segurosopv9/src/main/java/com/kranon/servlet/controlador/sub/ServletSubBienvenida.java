package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.controlador.BeanControlador;
import com.kranon.bean.controlador.BeanSaludoControlador;
import com.kranon.bean.serv.BeanSaludo;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonControlador;
import com.kranon.singleton.SingletonServicio;
import com.kranon.util.UtilidadesLoggerKranon;

/**
 * Servlet para la peticion de SUB-BIENVENIDA en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletSubBienvenida extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletSubBienvenida() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionSaludo", "");
			request.setAttribute("modoSaludo", "");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_BIENVENIDA).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SUB_BIENVENIDA";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String datoShortcut = "";
		String modoSaludo = "";
		String locucionSaludo = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add(UtilidadesLoggerKranon.mensaje+"BIENVENIDA");
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			String sas = request.getParameter("VG_loggerServicio.idioma");
			datoShortcut = request.getParameter("VG_Short");
			
			// TODO que valen estos datos??
			String vdn = request.getParameter("PRM_vdn");
			String tipoCliente = request.getParameter("PRM_tipoCliente");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			SimpleDateFormat formatDia = new SimpleDateFormat("MMdd");
			String diaHoy = formatDia.format(new Date());

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("diaHoy", diaHoy));
			parametrosEntrada.add(new ParamEvent("vdn", vdn));
			parametrosEntrada.add(new ParamEvent("tipoCliente", tipoCliente));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean del Servicio en cuestion
			BeanServicio servicio = null;
			SingletonServicio instance = SingletonServicio.getInstance(log);
			if (instance != null) {
				servicio = instance.getServicio(idServicio);
			}

			BeanSaludo saludoActivo = null;
			if (servicio != null) {
				// hemos recuperado bien el servicio, vamos a ver si hay un
				// saludo temporal activo

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("Servicio recuperado: " +
				// servicio.toString());
				/** FIN EVENTO - COMENTARIO **/

				saludoActivo = servicio.getSaludo(diaHoy);
			}

			if (saludoActivo != null) {
				// hay un saludo temporal activo

				locucionSaludo = saludoActivo.getWavSaludo();
				modoSaludo = saludoActivo.getModoReproduccion();

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("Saludo temporal recuperado: " +
				// saludoActivo.toString());
				/** FIN EVENTO - COMENTARIO **/

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("diaHoy", diaHoy));
				parametrosAdicionales.add(new ParamEvent("Inicio", saludoActivo.getFechaInicio()));
				parametrosAdicionales.add(new ParamEvent("Fin", saludoActivo.getFechaFin()));
				this.log.actionEvent("SALUDO_TEMPORAL", "OK", parametrosAdicionales);
				/** FIN EVENTO - ACION **/

				/** INICIO EVENTO - LOCUCION **/
				ArrayList<ParamEvent> parametrosAdicionalesLoc = new ArrayList<ParamEvent>();
				parametrosAdicionalesLoc.add(new ParamEvent("saludoTemporalActivo", saludoActivo.toString()));
				this.log.speechEvent(modoSaludo, locucionSaludo, parametrosAdicionalesLoc);
				/** FIN EVENTO - LOCUCION **/
				
				resultadoOperacion = "OK";
				error = "";

			} else {
				// no hay ningun saludo temporal activo o la recuperacion del
				// servicio fallo, busco el saludo por
				// defecto del controlador

				// Recuperamos el Bean del Controlador en cuestion
				BeanControlador controlador = null;
				SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
				if (instanceControlador != null) {
					controlador = instanceControlador.getControlador(idServicio, idElemento);
				}

				if (controlador == null) {
					// error al recuperar el servicio
					resultadoOperacion = "KO";
					error = "";

					throw new Exception("CONT_NULL");

				} else {
					// hemos recuperado bien el controlador

					/** INICIO EVENTO - COMENTARIO **/
					// this.log.comment("Controlador recuperado: " +
					// controlador.toString());
					/** FIN EVENTO - COMENTARIO **/

					// busco el saludo de bienvenida a reproducir por
					// defecto en el controlador
					BeanSaludoControlador bienvenida = controlador.getSaludo(controlador.getBienvenidas(), vdn, tipoCliente);

					if (bienvenida == null) {
						// no hay bienvenida definida
						resultadoOperacion = "OK";
						error = "";				
					} else {
						// hay una bienvenida en el controlador

						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("vdn", vdn));
						parametrosAdicionales.add(new ParamEvent("tipoCliente", tipoCliente));
						this.log.actionEvent("SALUDO_DEFAULT", "OK", parametrosAdicionales);
						/** FIN EVENTO - ACION **/

						locucionSaludo = bienvenida.getValue();
						modoSaludo = bienvenida.getModoReproduccion();

						/** INICIO EVENTO - COMENTARIO **/
						// this.log.comment("Bienvenida recuperada: " +
						// bienvenida.toString());
						/** FIN EVENTO - COMENTARIO **/

						/** INICIO EVENTO - LOCUCION **/
						ArrayList<ParamEvent> parametrosAdicionaleLoc = new ArrayList<ParamEvent>();
						parametrosAdicionaleLoc.add(new ParamEvent("bienvenidaControlador", bienvenida.toString()));
						this.log.speechEvent(modoSaludo, locucionSaludo, parametrosAdicionaleLoc);
						/** FIN EVENTO - LOCUCION **/

						resultadoOperacion = "OK";
						error = "";
					}
				}
			}

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("locucionSaludo", locucionSaludo);
			request.setAttribute("modoSaludo", modoSaludo);
			listlogger.add(resultadoOperacion);
			listlogger.add(error);
			listlogger.add(locucionSaludo);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("locucionSaludo", locucionSaludo));
			parametrosSalida.add(new ParamEvent("modoSaludo", modoSaludo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_BIENVENIDA).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			locucionSaludo = "";
			modoSaludo = "";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("locucionSaludo", locucionSaludo);
			request.setAttribute("modoSaludo", modoSaludo);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("locucionSaludo", locucionSaludo));
			parametrosSalida.add(new ParamEvent("modoSaludo", modoSaludo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			listlogger.add(parametrosSalida.toString());
			listlogger.add(resultadoOperacion);
			listlogger.add(error);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();	
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_BIENVENIDA).forward(request, response);

		}
	}

}
