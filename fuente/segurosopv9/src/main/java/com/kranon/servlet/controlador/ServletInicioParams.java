package com.kranon.servlet.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.kranon.cotizacion.bean.BeanPreCotizacion;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.servicios.cti.bean.BeanDatosUUI;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.kranon.bean.controlador.BeanControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.singleton.SingletonControlador;


/**
 * Servlet para la peticion de INICIO-PARAMS en el controlador en el servicio
 * LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletInicioParams extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;
	private BeanPreCotizacion beanPreCotizacion;
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	public ServletInicioParams() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", "CONTROLADOR");
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_INICIO_PARAMS).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "INICIO_PARAMS";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			logKranon.setIdServicio(idLlamada);
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("PRM_IN_CONTROLADOR_nombre");
			String isActivo = request.getParameter("PRM_IN_CONTROLADOR_isActivo");
//			Verificamos que nos lleguen los datos de forma correcta por el UUI y se genera el objeto Precotizacion
			BeanDatosUUI beaDatosUUI=verificarDAtosRetornoUUi(request);
			String lang = request.getParameter("VG_loggerServicio.idioma");
			setPrecotizacion(beaDatosUUI);
			// incluyo en sesion el idioma
			HttpSession session = request.getSession();
			session.setAttribute("lang", lang);
			
			// ABALFARO_20170516
			String contextoEnrutador = request.getParameter("PRM_IN_CONTROLADOR_contextoEnrutador");
			// incluyo en sesion el contexto del enrutador llamante
			session.setAttribute("contextoEnrutador", contextoEnrutador);	

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);


			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("nombreControlador", idElemento));
			parametrosEntrada.add(new ParamEvent("isActivo", isActivo));
			parametrosEntrada.add(new ParamEvent("contextoEnrutador", contextoEnrutador));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			BeanControlador controlador = null;
			// compruebo si el controlador esta activo
			if (isActivo == null || !isActivo.equalsIgnoreCase("ON")) {
				// el controlador noe sta activo
				resultadoOperacion = "KO";
				codigoRetorno = "TRANSFER";
				error = "";
			} else {

				// Recuperamos el Bean del Controlador en cuestion
				SingletonControlador instance = SingletonControlador.getInstance(log);
				if (instance != null) {
					controlador = instance.getControlador(idServicio, idElemento);
				}

				if (controlador == null) {
					// error al recuperar el servicio
					resultadoOperacion = "KO";
					codigoRetorno = "TRANSFER";
					error = "";

				} else {
					// hemos recuperado bien el controlador

//					verificamos nos lleguen datos Del CTI
					
					if(beanPreCotizacion.getIdCotiza().length()>0){
						resultadoOperacion = "OK";
						codigoRetorno = "OK";
						error = "";
					}
					else{
						resultadoOperacion = "KO";
						codigoRetorno = "ERROR";
						error = "ERRORDATOSCTI";
					}
					/** INICIO EVENTO - COMENTARIO 
					this.log.comment("GET_CONTROLADOR=" + controlador.toString());
					FIN EVENTO - COMENTARIO **/
					
					// aado el controlador a la request
					request.setAttribute("controlador", controlador);
					
					resultadoOperacion = "OK";
					codigoRetorno = "OK";
					error = "";
				}
			}
			
			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("PreCotizacion",beanPreCotizacion);
			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			if(error != null && !error.equals("")){
				parametrosSalida.add(new ParamEvent("error", error));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/
			listlogger.add("PreCotizacion:"+beanPreCotizacion.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_INICIO_PARAMS).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}
			
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(),  idModulo, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("idElemento", idElemento);
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);
			request.setAttribute("PreCotizacion",beanPreCotizacion);
			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - INICIO DE MODULO **/


			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_INICIO_PARAMS).forward(request, response);

		}
	}
	
	private BeanDatosUUI verificarDAtosRetornoUUi(HttpServletRequest request){
		BeanDatosUUI beanDatosUUI = new BeanDatosUUI();
		beanDatosUUI.setLlave(request.getParameter("VG_datosUUI.llave"));
		beanDatosUUI.setLlaveSegmento(request.getParameter("VG_datosUUI.llaveSegmento"));
		beanDatosUUI.setXferId(request.getParameter("VG_datosUUI.xferId"));
		beanDatosUUI.setSegmentoCliente(request.getParameter("VG_datosUUI.segmentoCliente"));
		beanDatosUUI.setCanal(request.getParameter("VG_datosUUI.canal"));
		beanDatosUUI.setNumCliente(request.getParameter("VG_datosUUI.numCliente"));
		beanDatosUUI.setLlaveAccesoFront(request.getParameter("VG_datosUUI.llaveAccesoFront"));
		beanDatosUUI.setMetodoAutenticacion(request.getParameter("VG_datosUUI.metodoAutenticacion"));
		beanDatosUUI.setNumeroSesion(request.getParameter("VG_datosUUI.numeroSesion"));
		beanDatosUUI.setCampania(request.getParameter("VG_datosUUI.campania"));
//		System.out.println(beanDatosUUI.toString());
		return beanDatosUUI;
	}
	
//	Metodo que recibe
//	String cadenainicial= cadena a limpiar
//	int tamaño esperado
//	int sobrante lo que sobra de la cadena
//	limpia la cadena y la deja al tamaño requerido
	private static String limpiarCadena(String cadenainicial){
		String nuevaCadena=cadenainicial;
		String CERO="0";
		String GATO="#";
		String NADA=" ";
		cadenainicial=cadenainicial.trim();
		int x=0;
		int y=0;
		try{
		//escapar y agregar limites de palabra completa - case-insensitive
		Pattern regex = Pattern.compile(GATO+"+");
		Matcher matchincio = regex.matcher(cadenainicial);
		//la palabra está en el texto??
		if (matchincio.find()) {  
		    int inicio=matchincio.start();
		    int termino=matchincio.end();
		    if(inicio==0)
		    	
		    	nuevaCadena=cadenainicial.substring(termino,cadenainicial.length());
		    else
		    	nuevaCadena=cadenainicial.substring(0,inicio);
		} else {
				nuevaCadena=cadenainicial;
		}}catch (Exception e) {
			// TODO: handle exception
		}
		return nuevaCadena;	
	}
	
	
	private void setPrecotizacion(BeanDatosUUI beanDatosUUI){
		beanPreCotizacion=new BeanPreCotizacion();
		String aux1IdCotiza;
		String aux2IdCotiza;
		String tipoOperacion = "";
		String intentos = "";
		String aux1IdSession;
		String aux2IdSession;
		String aux3IdSession;
		String idSession;
		if(beanDatosUUI==null){
//			System.out.println("Error");
		}
		else{
		aux1IdCotiza=(beanDatosUUI.getCanal()!=null?limpiarCadena(beanDatosUUI.getCanal()):null);
		aux2IdCotiza=(beanDatosUUI.getCampania()!=null?limpiarCadena(beanDatosUUI.getCampania()):null);
		aux1IdSession=(beanDatosUUI.getNumCliente()!=null?limpiarCadena(beanDatosUUI.getNumCliente()):null);
		aux2IdSession=(beanDatosUUI.getLlaveAccesoFront()!=null?limpiarCadena(beanDatosUUI.getLlaveAccesoFront()):null);
		aux3IdSession=(beanDatosUUI.getNumeroSesion()!=null?limpiarCadena(beanDatosUUI.getNumeroSesion()):null);
		String[]nuevas=(beanDatosUUI.getSegmentoCliente()!=null?beanDatosUUI.getSegmentoCliente().split("_"):new String[0]);
		if(nuevas.length>1){
			tipoOperacion=nuevas[0];
			intentos=nuevas[1];
		}
//		aux1IdCotiza.replaceAll(" ","#");
//		System.out.println("tipo"+tipoOperacion);
//		System.out.println("tipo"+intentos);
//		tipoOperacion=(beanDatosUUI.getSegmentoCliente()!=null?beanDatosUUI.getSegmentoCliente().substring(0,1):null);
//		intentos=(beanDatosUUI.getSegmentoCliente().length()>2?beanDatosUUI.getSegmentoCliente().substring(2):null);
		idSession=aux1IdSession+aux2IdSession+aux3IdSession;
		beanPreCotizacion.setIdCotiza(aux1IdCotiza+aux2IdCotiza);
		beanPreCotizacion.setIdint(intentos);
		beanPreCotizacion.setIdSession(idSession);
		beanPreCotizacion.setTipoOperacion(tipoOperacion);
//		System.out.println(beanPreCotizacion.toString());
		}
	}
}
