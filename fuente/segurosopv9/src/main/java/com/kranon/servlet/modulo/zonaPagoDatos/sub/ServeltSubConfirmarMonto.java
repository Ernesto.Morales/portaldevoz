package com.kranon.servlet.modulo.zonaPagoDatos.sub;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.modulo.zonaPagoDatos.ServletBaseModuloZonaPagoDatos;

public class ServeltSubConfirmarMonto extends ServletBaseModuloZonaPagoDatos {

	private static final long serialVersionUID = 1L;
	private static String MXN="pesos";
	public ServeltSubConfirmarMonto() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_CONFIRMA_MENU).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_ZPDATOS";
		String monto ="";
		String moneda="";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			monto=request.getParameter("VG_DatosCotizacion.montoCobro");
			moneda=request.getParameter("VG_DatosCotizacion.monedaCobro");
			if(moneda.equals("MXN")){
				moneda=MXN;
			}
			monto=monto+","+moneda;
			String maxIntentosCandado = request.getParameter("PRM_intentosCandado");
//			System.out.println("MONTO"+monto);
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
			
			request.setAttribute("idServicio", idServicio);

			resultadoOperacion = "OK";
			error = "";
			
			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("maxIntentosCandado", maxIntentosCandado));
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/
			request.setAttribute("monto", monto);
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_CONFIRMA_MENU).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			
			request.setAttribute("monto", monto);
			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_CONFIRMA_MENU).forward(request, response);

		}
	}

}
