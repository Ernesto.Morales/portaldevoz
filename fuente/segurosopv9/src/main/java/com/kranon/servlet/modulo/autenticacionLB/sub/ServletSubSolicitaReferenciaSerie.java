package com.kranon.servlet.modulo.autenticacionLB.sub;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.modulo.autenticacionLB.ServletBaseModuloAutenticacion;

/**
 * Servlet para la peticion de TARJETA en el Modulo de Identificacion
 * 
 * @author aarce
 *
 */
public class ServletSubSolicitaReferenciaSerie extends ServletBaseModuloAutenticacion {

	private static final long serialVersionUID = 1L;

	public ServletSubSolicitaReferenciaSerie() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SOLICITA_REFERENCIA_SERIE).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_AUTENTICACION";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");

			String maxIntentosCandado = request.getParameter("PRM_intentosCandado");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);
			
			request.setAttribute("idServicio", idServicio);

			resultadoOperacion = "OK";
			error = "";

			// calculo el momento del dia para dar los buenos dias/tardes/noches al cliente si se identifica correctamente
			String momentoDia = "";
			SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
			String horaActual = sdf.format(new Date());
			int horaActualInt = Integer.parseInt(horaActual);
			this.log.comment("horaActualInt=" + horaActualInt);
			if(horaActualInt> 60000 && horaActualInt <= 125959){
				// de 06:00:00 a 12:59:59 se daran los buenos dias
				momentoDia = "dias";
			} else if (horaActualInt> 130000 && horaActualInt <= 195959){
				// de 13:00:00 a 19:59:59 se daran las buenos tardes
				momentoDia = "tardes";
			} else {
				// de 20:00:00 a 05:59:59 se daran las buenos noches
				momentoDia = "noches";
			}

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("momentoDia", momentoDia);
			request.setAttribute("error", error);

			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("maxIntentosCandado", maxIntentosCandado));
			parametrosAdicionales.add(new ParamEvent("momentoDia", momentoDia));
			if (error != null && !error.equals("")) {
				parametrosAdicionales.add(new ParamEvent("error", error));
			}
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SOLICITA_REFERENCIA_SERIE).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);

			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloAutenticacion.PAGE_SUB_SOLICITA_REFERENCIA_SERIE).forward(request, response);

		}
	}

}
