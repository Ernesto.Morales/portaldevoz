package com.kranon.servlet.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;

public class ServletEjecucionModulo extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	public ServletEjecucionModulo() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_EJECUCION_MODULO).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idAccion = "EJECUCION_MODULO";

		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			
//			String nombreModulo = request.getParameter("VG_infoModulo.nombre");
//			String rutaModulo = request.getParameter("VG_infoModulo.ruta");
			
			String nombreModulo = request.getParameter("VG_infoOperativa.infoProceso.nombre");
			String rutaModulo = request.getParameter("VG_infoOperativa.infoProceso.ruta");
			
	
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("nombreModulo", nombreModulo));
			parametrosAdicionales.add(new ParamEvent("rutaModulo", rutaModulo));
			this.log.actionEvent(idAccion, "OK", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_EJECUCION_MODULO).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idAccion, parametrosAdicionales,e);
			/** FIN EVENTO - ERROR **/
	
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_EJECUCION_MODULO).forward(request, response);

		}
	}

}

