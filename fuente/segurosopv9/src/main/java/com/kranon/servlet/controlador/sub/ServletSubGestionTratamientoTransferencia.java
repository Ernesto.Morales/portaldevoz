package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.gestoperativas.BeanGestionOperativas;
import com.kranon.bean.gestoperativas.BeanInfoOperativa;
import com.kranon.bean.gestoperativas.BeanOperativa;
import com.kranon.bean.transfer.BeanTransfer;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonGestionOperativas;
import com.kranon.singleton.SingletonTransfer;
import com.kranon.util.UtilidadesGestionOperativa;

/**
 * Servlet para la peticion de SUB-GESTION-TRATAMIENTO-TRANSFERENCIA en el controlador en el servicio LINEA BANCOMER
 *
 * @author aalfaro
 *
 */
public class ServletSubGestionTratamientoTransferencia extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	public ServletSubGestionTratamientoTransferencia() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_GESTION_TRATAMIENTO_TRANSFER).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "GESTION_TRATAMIENTO_TRANSFER";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";

		// datos de la transferencia
		String ctiTransfer = "";
		String vdnTransfer = "";
		String isDentroHorario = "";
		String locFH = "";
		String modoLocFH = "";
		String controlHorarioTransfer = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			// ***************************************** //
			// *** RECUPERACION ETIQUETA + METADATOS *** //
			// ***************************************** //
			// recupero la info de la etiqueta
			String nombreOperativa = request.getParameter("VG_operativa.nombreOperativa");
			String action = request.getParameter("VG_operativa.action");
			String type = request.getParameter("VG_operativa.type");
			String balance = request.getParameter("VG_operativa.balance");
			String folio = request.getParameter("VG_operativa.folio");
			String insurance = request.getParameter("VG_operativa.insurance");
			String data = request.getParameter("VG_operativa.data");

			// ********************************************* //

			String segmentoTransfer = request.getParameter("VG_transfer.segmento");
			// ABALFARO_20170529 el segmento del cliente traducido se corresponde con el nombre del controlador actual
			String segmentoControlador = request.getParameter("VG_controlador.nombre");
			String segmentoCliente = request.getParameter("VG_cliente.segmento");
			String segmentoEntrada = request.getParameter("VG_segmento");

			ArrayList<String> listaExcepPostAutomatizacion = new ArrayList<String>();
			Enumeration<?> enumeration = request.getParameterNames();
			while (enumeration.hasMoreElements()) {
				String parameterName = (String) enumeration.nextElement();
				String value = request.getParameter(parameterName);

				if (parameterName.contains("VG_gestionExcepciones.postAutomatizacion")) {
					listaExcepPostAutomatizacion.add(value);
				}
			}

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("segmentoCliente", segmentoCliente));
			parametrosEntrada.add(new ParamEvent("segmentoControlador", segmentoControlador));
			parametrosEntrada.add(new ParamEvent("segmentoEntrada", segmentoEntrada));
//			parametrosEntrada.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
			parametrosEntrada.add(new ParamEvent("excepPostAutomatizacion", listaExcepPostAutomatizacion.toString()));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// creo excepiones de setTransferencia
			ArrayList<String> listaExcepPostSetTransferencia = new ArrayList<String>();

			// Recuperamos el Bean de Gestion de Operativas en cuestion
			BeanGestionOperativas gestionOperativas = null;
			SingletonGestionOperativas instance = SingletonGestionOperativas.getInstance(log);
			if (instance != null) {
				gestionOperativas = instance.getGestionOperativas(idServicio);
			}

			if (gestionOperativas == null) {
				// error al recuperar la instancia
				resultadoOperacion = "KO";
				error = "GEST_OPERATIVA_NULL";

				throw new Exception(error);

			}

			// ******************************* //
			// *** OBTENCION de OPERATIVA **** //
			// ******************************* //
			// recupero la operativas
			BeanOperativa beanOperativa = gestionOperativas.getTablaOperativas().get(nombreOperativa);

			if (beanOperativa == null) {
				// error al recuperar la operativa
				resultadoOperacion = "KO";
				error = "OPERATIVA_NULL";
				throw new Exception(error);
			}

			// recupero la operativa con metadatos
			BeanInfoOperativa beanInfoOperativa = beanOperativa.buscaInfoMetadato(action, type, balance, folio, insurance, data);

			if (beanInfoOperativa == null) {
				// error al recuperar la info operativa
				resultadoOperacion = "KO";
				error = "INFO_OPERATIVA_NULL";
				throw new Exception(error);
			}

			// ********************************************* //

			// ******************************** //
			// *** PRIORIDAD TRANSFERENCIA **** //
			// ******************************** //
			// ******* Directo > Segmento de Cliente > Segmento de Entrada > Por defecto Operativa > Por defecto Servicio

			// primero compruebo que tenga definido un postProceso y transferencia, si no es que tengo que transferir al segmento de entrada
			if (beanInfoOperativa.getPostProceso() == null || beanInfoOperativa.getPostProceso().getTransferencia() == null) {
				// No tenemos definida la transferencia, transferiremos al por defecto Servicio

				segmentoTransfer = "DEFAULT_SERV";

				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
				this.log.actionEvent("CALCULA_TRANSFER", "KO", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				resultadoOperacion = "KO";
				error = "";
			} else {
				
				// 1. Transferencia Directa
				segmentoTransfer = beanInfoOperativa.getPostProceso().getTransferencia().getDirecta();

				if (segmentoTransfer != null && !segmentoTransfer.equals("")) {
					/** INICIO EVENTO - ACCION **/
					ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
					parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
					this.log.actionEvent("CALCULA_TRANSFER", "DIRECTO", parametrosAdicionales);
					/** FIN EVENTO - ACCION **/

				} else {

					// 2. Segmento Cliente
					// ABALFARO_20170529 el segmento del cliente traducido se corresponde con el nombre del controlador actual
					segmentoTransfer = beanInfoOperativa.getPostProceso().getTransferencia().getTransferPorSegmentoCliente(segmentoControlador);

					if (segmentoTransfer != null && !segmentoTransfer.equals("")) {
						/** INICIO EVENTO - ACCION **/
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("segmentoCliente", segmentoCliente));
						parametrosAdicionales.add(new ParamEvent("segmentoControlador", segmentoControlador));
						parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
						this.log.actionEvent("CALCULA_TRANSFER", "VARIABLE", parametrosAdicionales);
						/** FIN EVENTO - ACCION **/

					} else {

						// 3. Segmento Entrada
						segmentoTransfer = beanInfoOperativa.getPostProceso().getTransferencia().getTransferPorSegmentoEntrada(segmentoEntrada);

						if (segmentoTransfer != null && !segmentoTransfer.equals("")) {
							/** INICIO EVENTO - ACCION **/
							ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
							parametrosAdicionales.add(new ParamEvent("segmentoEntrada", segmentoEntrada));
							parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
							this.log.actionEvent("CALCULA_TRANSFER", "VARIABLE", parametrosAdicionales);
							/** FIN EVENTO - ACCION **/

						} else {
							// 4. Por defecto Operativa
							segmentoTransfer = beanInfoOperativa.getPostProceso().getTransferencia().getDefecto();

							if (segmentoTransfer != null && !segmentoTransfer.equals("")) {

								/** INICIO EVENTO - ACCION **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
								this.log.actionEvent("CALCULA_TRANSFER", "DEFECTO", parametrosAdicionales);
								/** FIN EVENTO - ACCION **/

							} else {
								// 5. Por defecto Servicio

								segmentoTransfer = "DEFAULT_SERV";

								/** INICIO EVENTO - ACCION **/
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
								this.log.actionEvent("CALCULA_TRANSFER", "KO", parametrosAdicionales);
								/** FIN EVENTO - ACCION **/

								resultadoOperacion = "KO";
								error = "";
							}
						}
					}
				}
			}

			if (!segmentoTransfer.equals("DEFAULT_SERV")) {
				// Recuperamos el Bean de la Transferencia en cuestion (idServicio - segmento)
				BeanTransfer transfer = null;
				SingletonTransfer instanceTransfer = SingletonTransfer.getInstance(log);
				if (instanceTransfer != null) {
					transfer = instanceTransfer.getTransfer(idServicio, segmentoTransfer);
				}

				if (transfer == null) {
					// error al recuperar la transferencia
					resultadoOperacion = "KO";
					error = "ERROR_EXT(TRANSFER_NULL)";

				} else {

					// COMPROBAR HORARIO DE LA TRANSFERENCIA
					controlHorarioTransfer = transfer.getConfiguracion().getControlHorario();
					String horarioTransfer = "DH";
					if (controlHorarioTransfer != null && controlHorarioTransfer.equalsIgnoreCase("ON")) {
						// el control horario esta activo, miro si estoy DH
						// o FH
						horarioTransfer = UtilidadesGestionOperativa.comprobarHorario(this.log, transfer.getHorario());

						if (horarioTransfer != null && horarioTransfer.equalsIgnoreCase("DH")) {
							// estamos dentro de horario
							isDentroHorario = "true";

							// ADD-EXCEPCION POST SET TRANSFERENCIA
							listaExcepPostSetTransferencia.add("DH");
						} else {
							// fuera de horario
							isDentroHorario = "false";
							locFH = transfer.getConfiguracion().getWavTransferOutHour();
							modoLocFH = transfer.getConfiguracion().getModoReproduccion();

							// ADD-EXCEPCION POST SET TRANSFERENCIA
							listaExcepPostSetTransferencia.add("FH");
						}
					} else {
						// no tiene control horario
						isDentroHorario = "OFF";

						// estamos dentro de horario entonces
						// ADD-EXCEPCION POST SET TRANSFERENCIA
						listaExcepPostSetTransferencia.add("DH");
					}

					/** INICIO EVENTO - COMENTARIO **/
					this.log.comment("controlHorarioTransfer=" + controlHorarioTransfer + "|" + "isDentroHorario=" + isDentroHorario);
					/** FIN EVENTO - COMENTARIO **/

					vdnTransfer = transfer.getConfiguracion().getDestinoTransfer();
					ctiTransfer = transfer.getConfiguracion().getCtiActivo();

					resultadoOperacion = "OK";
					error = "";
				}

			}

			String nuevoNombreOperativa = "";
			// compruebo si hay excepciones de automatizacion
			if (listaExcepPostAutomatizacion.size() != 0) {
				// hay excepciones de automatizacion
				// compruebo que esta excepcion contenga la excepcion de postSetTransferencia para proceder a analizarla
				String excepAutomatiz = listaExcepPostAutomatizacion.get(listaExcepPostAutomatizacion.size() - 1);
				String excepTransfe = listaExcepPostSetTransferencia.get(listaExcepPostSetTransferencia.size() - 1);
				if (excepAutomatiz.contains(excepTransfe)) {
					// debo tratar esa excepcion
					nuevoNombreOperativa = excepAutomatiz;

					// elimino la excep de automatiz tratada
					listaExcepPostAutomatizacion.remove(listaExcepPostAutomatizacion.size() - 1);
				} else {
					// seguire el curso de la transferencia habitual
				}
			}

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("segmentoTransfer", segmentoTransfer));
			parametrosSalida.add(new ParamEvent("vdnTransfer", vdnTransfer));
			parametrosSalida.add(new ParamEvent("ctiTransfer", ctiTransfer));
			parametrosSalida.add(new ParamEvent("isDentroHorario", isDentroHorario));
			parametrosSalida.add(new ParamEvent("locFH", locFH));
			parametrosSalida.add(new ParamEvent("modoLocFH", modoLocFH));
			parametrosSalida.add(new ParamEvent("nuevoNombreOperativa", nuevoNombreOperativa));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("excepPostSetTransferencia", listaExcepPostSetTransferencia.toString()));
			parametrosAdicionales.add(new ParamEvent("excepPostAutomatizacion", listaExcepPostAutomatizacion.toString()));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("segmentoTransfer", segmentoTransfer);
			request.setAttribute("vdnTransfer", vdnTransfer);
			request.setAttribute("ctiTransfer", ctiTransfer);
			request.setAttribute("isDentroHorario", isDentroHorario);
			request.setAttribute("locFH", locFH);
			request.setAttribute("modoLocFH", modoLocFH);
			request.setAttribute("nuevoNombreOperativa", nuevoNombreOperativa);
			request.setAttribute("listaExcepPostSetTransferencia", listaExcepPostSetTransferencia);
			request.setAttribute("listaExcepPostAutomatizacion", listaExcepPostAutomatizacion);

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_GESTION_TRATAMIENTO_TRANSFER).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_GESTION_TRATAMIENTO_TRANSFER).forward(request, response);

		}
	}
}
