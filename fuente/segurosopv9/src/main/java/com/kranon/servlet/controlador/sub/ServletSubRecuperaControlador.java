package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.controlador.BeanControlador;
import com.kranon.bean.gestioncontroladores.BeanConfigControlador;
import com.kranon.bean.gestioncontroladores.BeanGestionControladores;
import com.kranon.bean.gestioncontroladores.BeanInfoControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonControlador;
import com.kranon.singleton.SingletonGestionControladores;

/**
 * Servlet para la peticion de SUB-RECUPERA-CONTROLADOR en el controlador en el servicio LINEA BANCOMER
 *
 * @author abalfaro
 *
 */
public class ServletSubRecuperaControlador extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	public ServletSubRecuperaControlador() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_RECUPERA_CONTROLADOR).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SUB_RECUPERA_CONTROLADOR_CLIENTE";

		String nombreControlador = "";
		String segmentoCliente = "";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			nombreControlador = request.getParameter("VG_controlador.nombre");			
			segmentoCliente = request.getParameter("VG_cliente.segmento");
			
			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("controlador", nombreControlador));
			parametrosEntrada.add(new ParamEvent("segmentoCliente", segmentoCliente));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			// Recuperamos el Bean de Gestion Controladores
			BeanControlador controlador = null;
			String nombreNuevoControlador = "";
			BeanGestionControladores gestionControladores = null;
			SingletonGestionControladores instanceGestion = SingletonGestionControladores.getInstance(log);
			if (instanceGestion != null) {
				gestionControladores = instanceGestion.getGestionControladores(idServicio);
			}
			
			if (gestionControladores == null) {
				// error al recuperar el controlador
				resultadoOperacion = "KO";
				codigoRetorno = "ERROR";
				error = "ERROR_EXT(GEST_CONTROL_NULL)";
			} else {
				// Hemos recuperado bien la gestion de controladores
				// Buscamos el controlador que nos interesa por el nombre del segmento del Cliente
				for (BeanConfigControlador config : gestionControladores.getConfiguracion()) {
					if (config.getSegmentoCliente() != null && config.getSegmentoCliente().equals(segmentoCliente)) {
						
						// es el controlador que queremos usar
						nombreNuevoControlador = config.getNombreControlador();
						
						/** INICIO EVENTO - ACCION **/
						String idAccion = "GET_CONTROLADOR_POR_SEGMENTO";
						ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
						parametrosAdicionales.add(new ParamEvent("segmentoCliente", segmentoCliente));
						parametrosAdicionales.add(new ParamEvent("nombreControlador", nombreNuevoControlador));
						this.log.actionEvent(idAccion, "OK", parametrosAdicionales);
						/** FIN EVENTO - ACCION **/
						break;
					}				
				}

				if (!nombreNuevoControlador.equals("")) {

					// Buscamos el controlador por el nombre
					BeanInfoControlador infoControlador = null;
					for (BeanInfoControlador c : gestionControladores.getControladores()) {
						if (c.getNombre().equals(nombreNuevoControlador)) {
							// es el controlador que queremos usar
							infoControlador = c;
							break;
						}
					}
					if(infoControlador != null){
	
						// Tenemos la informacion del controlador
						// Evaluamos si esta activo
						if (infoControlador.getActivo().equalsIgnoreCase("ON")) {
		
							SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
							if (instanceControlador != null) {
								controlador = instanceControlador.getControlador(idServicio, nombreNuevoControlador);
							}

							if (controlador != null) {

								// hemos recuperado bien el controlador
								// aniado el controlador a la request
								request.setAttribute("controlador", controlador);

								/** INICIO EVENTO - ACCION **/
								String idAccion = "CAMBIO_CONTROLADOR";
								ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
								parametrosAdicionales.add(new ParamEvent("controlador(as-is)", nombreControlador));
								parametrosAdicionales.add(new ParamEvent("controlador(to-be)", nombreNuevoControlador));
								this.log.actionEvent(idAccion, "OK", parametrosAdicionales);
								/** FIN EVENTO - ACCION **/
								
								resultadoOperacion = "OK";
								codigoRetorno = "OK";
								error = "";
							}
							else {
								// Error al recuperar el controlador porque no se encuentra el XML asociado.
								resultadoOperacion = "KO";
								codigoRetorno = "ERROR";
								error = "ERROR_EXT(GET_CONTROLADOR)";
							}								
						}
						else{
							// El controlador al que habria que cambiar, no esta activo.
							resultadoOperacion = "KO";
							codigoRetorno = "ERROR";
							error = "ERROR_EXT(CONTROLADOR_NO_ACTIVO)";							
						}						
					}
					else {
						// Error al recuperar el infoControlador del XML de gestion.
						resultadoOperacion = "KO";
						codigoRetorno = "ERROR";
						error = "ERROR_EXT(INFO_CONTROLADOR_NULL)";
					}
				}
				else {
					// No hay ningun controlador definido para ese segmento de Cliente.
					resultadoOperacion = "KO";
					codigoRetorno = "ERROR";
					error = "ERROR_EXT(SEGMENTO_SIN_CONTROLADOR)";
				}
			}
						
			
			// Anado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));		
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			if (controlador != null) {
				parametrosSalida.add(new ParamEvent("controlador", controlador.getNombreControlador()));
			}
			else {
				parametrosSalida.add(new ParamEvent("controlador", nombreControlador));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_RECUPERA_CONTROLADOR).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			codigoRetorno = "ERROR";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("codigoRetorno", codigoRetorno);
			request.setAttribute("error", error);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosSalida.add(new ParamEvent("error", error));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_RECUPERA_CONTROLADOR).forward(request, response);

		}
	}

}
