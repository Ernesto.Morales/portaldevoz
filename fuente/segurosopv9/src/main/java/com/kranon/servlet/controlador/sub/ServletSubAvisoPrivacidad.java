package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.controlador.BeanControlador;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.singleton.SingletonControlador;

/**
 * Servlet para la peticion de SUB-AVISO-PRIVACIDAD en el controlador en el servicio LINEA BANCOMER
 *
 * @author aarce
 *
 */
public class ServletSubAvisoPrivacidad extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;

	public ServletSubAvisoPrivacidad() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("activo", "");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_AVISO_PRIVACIDAD).forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";

		String idModulo = "SUB_AVISO_PRIVACIDAD";

		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";

		String activo = "";

		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO DE MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO DE MODULO **/

			BeanControlador controlador = null;
			SingletonControlador instanceControlador = SingletonControlador.getInstance(log);
			if (instanceControlador != null) {
				controlador = instanceControlador.getControlador(idServicio, idElemento);
			}

			if (controlador == null) {
				// error al recuperar el servicio
				resultadoOperacion = "KO";
				error = "";

				throw new Exception("CONT_NULL");

			} else {
				// hemos recuperado bien el controlador

				/** INICIO EVENTO - COMENTARIO **/
				// this.log.comment("Controlador recuperado: " +
				// controlador.toString());
				/** FIN EVENTO - COMENTARIO **/

				String activoMenuAvisoPrivacidad = controlador.getActivacionMenuAvisoPrivacidad();

				if ((activoMenuAvisoPrivacidad == null) || !activoMenuAvisoPrivacidad.equalsIgnoreCase("ON")) {
					// no hay aviso de privacidad activado
					resultadoOperacion = "OK";
					error = "";
					activo = "NO";

				} else {
					// hay aviso de privacidad activado en el controlador

					resultadoOperacion = "OK";
					error = "";
					activo = "SI";
				}
			}

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("activo", activo);
			request.setAttribute("idServicio", idServicio);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			parametrosSalida.add(new ParamEvent("activo", activo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_AVISO_PRIVACIDAD).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			activo = "";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("activo", activo);
			request.setAttribute("idServicio", idServicio);

			/** INICIO EVENTO - FIN DE MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("resultadoOperacion", resultadoOperacion));
			parametrosSalida.add(new ParamEvent("error", error));
			parametrosSalida.add(new ParamEvent("activo", activo));
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, null);
			/** FIN EVENTO - FIN DE MODULO **/

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_SUB_AVISO_PRIVACIDAD).forward(request, response);

		}
	}

}
