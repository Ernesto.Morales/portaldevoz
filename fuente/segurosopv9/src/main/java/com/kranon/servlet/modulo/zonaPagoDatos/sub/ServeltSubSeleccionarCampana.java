package com.kranon.servlet.modulo.zonaPagoDatos.sub;

import java.util.Calendar;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.bean.BeanGramatica;
import com.kranon.bean.menu.BeanConfigOpcDtmfMenu;
import com.kranon.bean.menu.BeanConfigOpcMenu;
import com.kranon.bean.menu.BeanOpcionMenu;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.getCampanas.ObtenerCampana;
import com.kranon.servicios.getCampanas.bean.BeanCampana;
import com.kranon.servicios.getCampanas.bean.BeanCampanas;
import com.kranon.servicios.getCampanas.bean.BeanObtenerCampanaOut;
import com.kranon.servicios.getCampanas.bean.BeanPlanes;
import com.kranon.servicios.getCampanas.bean.UtilidadesCampanas;
import com.kranon.servlet.modulo.zonaPagoDatos.ServletBaseModuloZonaPagoDatos;

public class ServeltSubSeleccionarCampana extends ServletBaseModuloZonaPagoDatos {

	private static final long serialVersionUID = 1L;
	private static final String PAGO_CON_PUNTOS="PAGO_CON_PUNTOS";
	private static String PAGO_UNICO="PAGOUNICO";
	private static final String PAGO_CON_PUNTOSA="MIXED";
	private static final String TTS="TTS";
	private static final String GRAMAR="builtin:dtmf/number?length=";
	private static final String TEXPAGO_UNICO="Pago en una sola exhibición, marca";
	private static final String TEXPAGO_UNICO2= "En una sola exhibición";
	private static final String BULTING="BULTING";
	private static final String MARCA=",marca";
	private static final String PAGO="Pago";
	private static final String TEX_PUNTOS="Pago con puntos, marca";
	private CommonLoggerKranon logKranon;
	private static String varSplitBy = "\\|";
	private List<String> listlogger = new ArrayList<String>();
	public ServeltSubSeleccionarCampana() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("codigoRetorno", "ERROR");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_SELECCIONAR_CAMPANA).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "MODULO_OBTENER_CAMPANA";
		String monto ="";
		String moneda="";
		String tsec="";
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		String codigoRetorno="";
		String posi="";
		String opcionesjson="";
		int aux=1;
		List<BeanOpcionMenu> opciones= new ArrayList<BeanOpcionMenu>();
		String opcionretorno="";
		try {
			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			String idCotiza=request.getParameter("VG_DatosCotizacion.idCotiza");
			String tarjeta=request.getParameter("VG_DatosCotizacion.tarjeta");
			int tamanioTarjeta = tarjeta.length();
			tsec = request.getParameter("VG_cliente.tsec");
			String campañas=request.getParameter("PRM_campanas");
			
			
			
			
			
			
			
			
//			idLlamada = "1235";
//			idElemento = "SEGUROSOPV1";
//			idServicio = "SEGUROSOPV1";
//			String idCotiza= "1962022";
//			String tarjeta= "42424242424242424242";
//			int tamanioTarjeta = tarjeta.length();
//			tsec = "dslkjlkcjdlasjlkdklcnsldknx";
//			String campañas= "{\"listaCampanya\":[{\"opcion\":\"1\",\"tipoCampaña\":\"MESES_SIN_INTERESES\",\"listaPlanes\":[{\"valor\":\"0-3\",\"descripcion\":\"Meses Sin Intereses - Hogar\"},{\"valor\":\"0-6\",\"descripcion\":\"Meses Sin Intereses - Hogar\"},{\"valor\":\"0-9\",\"descripcion\":\"Meses Sin Intereses - Hogar\"},{\"valor\":\"0-12\",\"descripcion\":\"Meses Sin Intereses - Hogar\"}]},{\"opcion\":\"2\",\"tipoCampaña\":\"SKIP_PAYMENT\",\"listaPlanes\":[{\"valor\":\"2\",\"descripcion\":\"Paga hasta 2 meses\"},{\"valor\":\"3\",\"descripcion\":\"Paga hasta 3 meses\"},{\"valor\":\"4\",\"descripcion\":\"Paga hasta 4 meses\"}]},{\"opcion\":\"3\",\"tipoCampaña\":\"SKIP_PAYMENT_MSI\",\"listaPlanes\":[{\"valor\":\"1-3\",\"descripcion\":\"Paga en 1 meses y a 3MSI\"},{\"valor\":\"1-6\",\"descripcion\":\"Paga en 1 meses y a 6MSI\"},{\"valor\":\"2-3\",\"descripcion\":\"Paga en 2 meses y a 3MSI\"},{\"valor\":\"2-6\",\"descripcion\":\"Paga en 2 meses y a 6MSI\"},{\"valor\":\"3-3\",\"descripcion\":\"Paga en 3 meses y a 3MSI\"},{\"valor\":\"3-6\",\"descripcion\":\"Paga en 3 meses y a 6MSI\"},{\"valor\":\"3-9\",\"descripcion\":\"Paga en 3 meses y a 9MSI\"},{\"valor\":\"4-3\",\"descripcion\":\"Paga en 4 meses y a 3MSI\"},{\"valor\":\"4-6\",\"descripcion\":\"Paga en 4 meses y a 6MSI\"}]},{\"opcion\":\"4\",\"tipoCampaña\":\"PAGO_CON_PUNTOS\",\"listaPlanes\":[{\"valor\":\"0\",\"descripcion\":\"Puntos\"}]}],\"afiliacion\":\"ECOMMER\",\"tipoTarjeta\":\"VISA\",\"codError\":\"00000\"}";
//			
			
			
			
			
			
			
			
			
			BeanOpcionMenu opcion= llenaropcion("1", PAGO_UNICO, PAGO_UNICO, GRAMAR+"1", TEXPAGO_UNICO,"0");
			opciones.add(opcion);
			//ConexionResponse conexionResponse = null;
			ObtenerCampana obtenerCampana= new ObtenerCampana(idLlamada, idServicio, idElemento);
			//obtenerCampana.preparaPeticion(tsec,idCotiza,tarjeta.substring(0,6),tamanioTarjeta);
			//conexionResponse = obtenerCampana.ejecutaPeticion();
//			HARCORE
//			conexionResponse.setResultado("OK");
			if(campañas != null && !campañas.equals("")){
				opcionesjson=campañas.replaceAll("T1LDE","\"");
				opcionesjson=opcionesjson.replaceAll("COM4S",",");
//			if (conexionResponse.getResultado().equals("OK")) {		
//				HARDCORE
//				String jsonhardcore="{\"listaCampanya\":[{\"opcion\":\"1\",\"tipoCampaña\":\"MESES_SIN_INTERESES\",\"listaPlanes\":[{\"valor\":\"0-12\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-3\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-6\",\"descripcion\":\"Meses Sin Intereses\"},{\"valor\":\"0-9\",\"descripcion\":\"Meses Sin Intereses\"}]},{\"opcion\":\"2\",\"tipoCampaña\":\"PAGO_CON_PUNTOS\",\"listaPlanes\":[{\"valor\":\"0\",\"descripcion\":\"Puntos\"}]}],\"codError\":\"00000\"}";
				BeanObtenerCampanaOut beanObtenerCampanaOut=obtenerCampana.parserSalida(opcionesjson);
//				BeanObtenerCampanaOut beanObtenerCampanaOut=obtenerCampana.parserSalida(conexionResponse.getMensajeRespuesta());
//				if(beanObtenerCampanaOut.getCodError().equals("00000")){
//				System.out.println("BEANNNNN"+beanObtenerCampanaOut.toString());	
				// la peticion ha ido OK-200
					resultadoOperacion = "OK";
					codigoRetorno = "OK";
					error = "";	
					for (BeanCampana campana : beanObtenerCampanaOut.getListaCampanya()) {
						
						if(campana.getTipoCampaña().equals(PAGO_CON_PUNTOS)){
							aux++;
							posi=Integer.toString(aux);
							BeanOpcionMenu opcionaux= llenaropcion(posi,PAGO_CON_PUNTOSA, PAGO_CON_PUNTOSA, GRAMAR+posi.length(), TEX_PUNTOS,"0_0");
							opciones.add(opcionaux);
//							System.out.println("opc"+opciones.toString());
							
						}else{
							for (BeanPlanes plan : campana.getListaPlanes()) {
								aux++;
								posi=Integer.toString(aux);
								plan.setValor();
								String accion=plan.getValormenu();
								if(campana.getTipoCampaña().equals("SKIP_PAYMENT")){
									// Creamos una instancia del calendario
									Calendar fecha = Calendar.getInstance();
							 
									//System.out.println("Fecha actual: " + (fecha.get(Calendar.MONTH)+1)+ "-"+ fecha.get(Calendar.DATE)+ "-"+ fecha.get(Calendar.YEAR));
							 
									// Array con los meses del año
									String[] meses = new String[]{"Enero", "Febebro","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

									int mesSkeep = Integer.parseInt(plan.getMesinicio());
									
									int empiezaAPagarEn = mesSkeep + (fecha.get(Calendar.MONTH));
									
									String texto="Compra hoy y empieza a pagar en " + meses[empiezaAPagarEn] + MARCA;
									BeanOpcionMenu opcionaux= llenaropcion(posi,campana.getTipoCampaña()+posi, accion, GRAMAR+posi.length(), texto,plan.getValor());
									opciones.add(opcionaux);
								}
								else if(campana.getTipoCampaña().equals("SKIP_PAYMENT_MSI")){
									// Creamos una instancia del calendario
									Calendar fecha = Calendar.getInstance();

									// Array con los meses del año
									String[] meses = new String[]{"Enero", "Febebro","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

									int mesSkeep = Integer.parseInt(plan.getMes());
									
									int msi = Integer.parseInt(plan.getMesinicio());
									
									int empiezaAPagarEn = mesSkeep + (fecha.get(Calendar.MONTH));
									
									String texto="Compra hoy y empieza a pagar en " + meses[empiezaAPagarEn]+  " y a " + msi + " meses sin intereses " + MARCA;
									BeanOpcionMenu opcionaux= llenaropcion(posi,campana.getTipoCampaña()+posi, accion, GRAMAR+posi.length(), texto,plan.getValor());
									opciones.add(opcionaux);
								}else {
									String texto=PAGO+" a "+plan.getMes()+" "+plan.getDescripcion()+MARCA;
									BeanOpcionMenu opcionaux= llenaropcion(posi,campana.getTipoCampaña()+posi, accion, GRAMAR+posi.length(), texto,plan.getValor());
									opciones.add(opcionaux);
								}
//								System.out.println("1opcion"+opcionaux.toString());
							}
						}
					}
					//System.out.println("OPCIONES______:"+opciones);
					BeanCampanas beanCampanas=new BeanCampanas(opciones);
					opcionesjson=UtilidadesCampanas.parserEntrada(beanCampanas);
					opcionesjson=opcionesjson.replaceAll("\"", "T1LDE");
					opcionesjson=opcionesjson.replaceAll(",", "COM4S");
//					System.out.println("antes"+opcionesjson);
			}
			else{
				resultadoOperacion = "KO";
				codigoRetorno = "KO";
				error = "ERROR";
			}
			/** INICIO EVENTO - ACCION **/
//			String idAccion = "SUB_TARJETA";
//			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
//			parametrosAdicionales.add(new ParamEvent("","" ));
//			if (error != null && !error.equals("")) {
//				parametrosAdicionales.add(new ParamEvent("error", error));
//			}
//			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
//			System.out.println("LISTAAAAAAAAAAAAAAAAAA"+opcionesjson);
			/** FIN EVENTO - ACCION **/
			request.setAttribute("resultadoOperacion", resultadoOperacion);	
			request.setAttribute("codigoRetorno", codigoRetorno);	
			request.setAttribute("error", error);
			request.setAttribute("opcion", PAGO_UNICO+"#0");
			request.setAttribute("texto", TEXPAGO_UNICO2);
			request.setAttribute("opcionesjson", opcionesjson);	
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_SELECCIONAR_CAMPANA).forward(request, response);

		} catch (final Exception e) {
//			System.out.println("ERROROROROROROROR");
			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("resultadoOperacion", resultadoOperacion);	
			request.setAttribute("codigoRetorno", codigoRetorno);	
			request.setAttribute("error", error);
			request.setAttribute("opcion", PAGO_UNICO+"#0");
			request.setAttribute("texto", TEXPAGO_UNICO2);
			request.setAttribute("opcionesjson", opcionesjson);
			/** INICIO EVENTO - ACCION **/
			String idAccion = "SUB_TARJETA";
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", error));
			this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_SELECCIONAR_CAMPANA).forward(request, response);

		}
	}
	
	private BeanOpcionMenu llenaropcion(String posicion,String descripcion,String Accion,String Gramatica,String prom, String opcione){
		System.out.println("ACCION____:"+Accion);
		BeanOpcionMenu opcion=new BeanOpcionMenu();
		BeanConfigOpcMenu configuracionmenu=new BeanConfigOpcMenu();
		configuracionmenu.setPosicion(posicion);
		configuracionmenu.setOpcionActiva("ON");
		configuracionmenu.setOpcionVisible("ON");
		configuracionmenu.setOpcionMenuDesc(descripcion);
		configuracionmenu.setModoReproduccion(TTS);
		//if(Accion.equals("NULL")){configuracionmenu.setAccion(msi+"#"+opcione);}else{configuracionmenu.setAccion(Accion+"#"+opcione);}
		configuracionmenu.setAccion(Accion+"#"+opcione);
		BeanConfigOpcDtmfMenu opcDtmfMenu = new BeanConfigOpcDtmfMenu();
		BeanGramatica gramatica =new BeanGramatica();
		gramatica.setId(1);
		gramatica.setTipo(BULTING);
		gramatica.setTonosActivos("0");
		gramatica.setValue(Gramatica);
		opcDtmfMenu.setGramaticaDtmf(gramatica);
		opcDtmfMenu.setPromptDtmf(prom);
		//if(Accion.equals("NULL")){opcion.setCodOpcion(msi+"#"+opcione);}else{opcion.setCodOpcion(Accion+"#"+opcione);}
		opcion.setCodOpcion(Accion+"#"+opcione);
		opcion.setConfiguracion(configuracionmenu);
		opcion.setConfigDtmf(opcDtmfMenu);
		return opcion;
	}
	
}
