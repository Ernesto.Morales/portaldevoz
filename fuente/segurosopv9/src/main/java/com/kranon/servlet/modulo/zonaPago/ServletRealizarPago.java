package com.kranon.servlet.modulo.zonaPago;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.cotizacion.bean.BeanCotizacion;
import com.kranon.loccomunes.BeanLocucionesComunes;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.openpay.OpenPay;
import com.kranon.servicios.openpay.bean.BeanOpenPayout;
import com.kranon.singleton.SingletonLocucionesComunes;
import com.kranon.util.UtilidadesLoggerKranon;

public class ServletRealizarPago extends ServletBaseModuloZonaPago{
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	private static String PUNTOS="MIXED";
	public ServletRealizarPago(){
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionSaludo", "");
			request.setAttribute("modoSaludo", "");
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_REALIZAR_PAGO).forward(request, response);

		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idServicio = "";
		String idLlamada = "";
		String idElemento="";
		String tsec="";
		String resultadoOperacion = "OK";
		String codigoRetorno = "OK";
		String error = "";
		String mensaje="";
		String auxCampana="";
		String pused="";
		String premain="";
		String pamount="";
		String pcurren="";
		BeanCotizacion beanCotizacion=new BeanCotizacion();
		try {
		idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
//		logKranon.setIdServicio(idLlamada);
		idServicio = request.getParameter("VG_loggerServicio.idServicio");
		idElemento = request.getParameter("VG_loggerServicio.idElemento");
		tsec = request.getParameter("VG_cliente.tsec");
		String idCotiza=request.getParameter("VG_DatosCotizacion.idCotiza");
		String idSession=request.getParameter("VG_DatosCotizacion.idSession");
		String montoCobro=request.getParameter("VG_DatosCotizacion.montoCobro");
		String monedaCobro=request.getParameter("VG_DatosCotizacion.monedaCobro");
		String nombreCliente=request.getParameter("VG_DatosCotizacion.nombreCliente");
		String apellidosCliente=request.getParameter("VG_DatosCotizacion.apellidosCliente");
		String telefono=request.getParameter("VG_DatosCotizacion.telefono");
		String correo=request.getParameter("VG_DatosCotizacion.correo");
		String afiliacionId=request.getParameter("VG_DatosCotizacion.afiliacionId");	
		String tarjeta=request.getParameter("VG_DatosCotizacion.tarjeta");
		String ano=request.getParameter("VG_DatosCotizacion.anotarjeta");
		String mes=request.getParameter("VG_DatosCotizacion.mestarjeta");
		String ccv=request.getParameter("VG_DatosCotizacion.ccv");
		String campana=request.getParameter("VG_DatosCotizacion.campana");
		String descripcion=request.getParameter("VG_DatosCotizacion.descripcionp");
		String[]nuevas=campana.split("#");
//		System.out.println(nuevas.length);
//		System.out.println("tarjeta: "+tarjeta+"mes: "+mes+ "año: "+ano);
		if(nuevas.length>1){
			campana=nuevas[0];
			auxCampana=nuevas[1];
		}
//		System.out.println(campana);
		beanCotizacion.setIdCotiza(idCotiza);
		beanCotizacion.setIdSession(idSession);
		beanCotizacion.setMontoCobro(montoCobro);
		beanCotizacion.setMonedaCobro(monedaCobro);
		beanCotizacion.setNombreCliente(nombreCliente);
		beanCotizacion.setApellidosCliente(apellidosCliente);
		beanCotizacion.setTelefono(telefono);
		beanCotizacion.setCorreo(correo);
		beanCotizacion.setAfiliacionId(afiliacionId);
		beanCotizacion.setDescripcion("IVR"+descripcion);
		/** INICIALIZAR LOG **/
		this.log = new CommonLoggerService(idElemento + "_" + idServicio);
		this.log.inicializar(idLlamada, idServicio, idElemento);
		ConexionResponse conexionResponse = null;
		
//		HARDCORE***********************************************************************
//		beanCotizacion=new BeanCotizacion();
//		beanCotizacion.setIdCotiza("12345678");
//		beanCotizacion.setIdSession("12345678901234567890123456789012");
//		beanCotizacion.setMontoCobro("2000");
//		beanCotizacion.setMonedaCobro("pesos");
//		beanCotizacion.setNombreCliente("cliente");
//		beanCotizacion.setApellidosCliente("Apellido");
//		beanCotizacion.setTelefono("55354127");
//		beanCotizacion.setCorreo("uncorreo@correo.com");
//		beanCotizacion.setAfiliacionId("12345678901234567890123");
//		beanCotizacion.setDescripcion("Vida Segura");
		
		//*****************************************************************************
//#		TODO
		/** LLAMO AL SERVICIO **/
		OpenPay openPay= new OpenPay(idLlamada, idServicio, idElemento);
		openPay.preparaPeticion(tsec,beanCotizacion,tarjeta,ano,mes,ccv,campana);
		conexionResponse = openPay.ejecutaPeticion();
//	LLAMDO A SERVICIO GET COTIZACION	
		
		if (conexionResponse.getResultado().equals("OK")) {
//			HARCORE
//			String hardcore="{\"data\":{\"id\":\"trehwr2zarltvae56vxl\",\"authorizationNumber\":\"432239943\",\"creationDate\":\"2019-01-05T18:29:35-06:00\",\"operationDate\":\"2010-01-05T18:29:35-06:00\",\"status\":{\"id\":\"COMPLETED\"},\"paymentAmount\":{\"amount\":500.00,\"currency\":\"MXN\"},\"description\":\"Pago de seguro\",\"quotationId\":\"oid-00051\",\"customer\":{\"firstName\":\"Carlos\",\"lastName\":\"Lopez\",\"secondLastName\":\"Martinez\",\"contacts\":[{\"contactType\":\"EMAIL\",\"contact\":\"carlos.lopez@correo.com\"},{\"contactType\":\"MOBILE\",\"contact\":\"5537283912\"}]},\"card\":{\"type\":\"debit\",\"brand\":\"visa\",\"number\":\"411111XXXXXX1111\",\"holderName\":\"Juan Perez Ramirez\",\"expiration\":{\"month\":12,\"year\":20},\"allowsCharges\":true,\"allowsPayouts\":true,\"bank\":{\"id\":\"002\",\"name\":\"Banamex\"},\"usePoints\":false,\"pointsType\":\"BANCOMER\"},\"points\":{\"used\":1000,\"remaining\":2000,\"amount\":200.00},\"installmentPlan\":{\"paymentsNumber\":\"TREE\",\"paymentsType\":\"WITHOUT_INTEREST\"},\"exchangeRate\":{\"date\":\"2019-01-05\",\"value\":13.61,\"baseCurrency\":\"USD\",\"targetCurrency\":\"MXN\"},\"feeAmount\":{\"amount\":2.0,\"currency\":\"MXN\",\"tax\":{\"amount\":0,\"currency\":\"MXN\"}}}}";
//			BeanOpenPayout beanCotizacionOut=openPay.parserSalida (hardcore);
			
			BeanOpenPayout beanCotizacionOut=openPay.parserSalida (conexionResponse.getMensajeRespuesta());
//			System.out.println("salida"+beanCotizacionOut.toString());
			if(beanCotizacionOut.getData().getPoints()!=null && campana.equals(PUNTOS) ){
				pused=beanCotizacionOut.getData().getPoints().getUsed();
				premain=beanCotizacionOut.getData().getPoints().getRemaining();
				pamount=beanCotizacionOut.getData().getPoints().getEquivalentAmount().getAmount();
				pcurren=beanCotizacionOut.getData().getPoints().getEquivalentAmount().getCurrency();
			}
			
			// la peticion ha ido OK-200
			resultadoOperacion = "OK";
			codigoRetorno = "OK";
			error = "";	
			mensaje="AUT-CLI-INFO-PAGO-OK";
		}
		else if(conexionResponse.getCodigoRespuesta()== 404 || conexionResponse.getCodigoRespuesta()== 500){
//			System.out.println("erorororro"+conexionResponse.getCodigoRespuesta());
			resultadoOperacion = "KO";
			codigoRetorno = "KO";
			error = "ERROR";
			mensaje="AUT-CLI-INFO-PAGO-KO";
		}
		
		else{
//			System.out.println("otro errororororcororororororoor "+conexionResponse.getCodigoRespuesta());
			resultadoOperacion = "KO";
			codigoRetorno = "KO";
			error = "ERROR";
			mensaje="COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP";
		}
		
//		BeanLocucionesComunes locComunes = null;
//		SingletonLocucionesComunes instanceLocComunes;
//		
//			instanceLocComunes = SingletonLocucionesComunes.getInstance(log);
//			if (instanceLocComunes != null) {
//				locComunes = instanceLocComunes.getLocucionesComunes(idServicio);
//			}
//			if (locComunes != null) {
//				// hemos recuperado bien las locuciones comunes
//				/**
//				 * INICIO EVENTO - COMENTARIO this.log.comment("LocucionesComunes recuperadas: " + locComunes.toString()); FIN EVENTO - COMENTARIO
//				 **/
//				// Aniado parametros de salida a la request
//				request.setAttribute("locComunes", locComunes);
//			}	
		Date fechaHoy = new Date();
		Long milisecFinOperacion = fechaHoy.getTime();
		request.setAttribute("milisecFinOperacion", milisecFinOperacion);	
		request.setAttribute("resultadoOperacion",resultadoOperacion);
		request.setAttribute("codigoRetorno",codigoRetorno);
		request.setAttribute("error",error);
		request.setAttribute("mensaje",mensaje);
		request.setAttribute("auxCampana",auxCampana);
		request.setAttribute("pused",pused);
		request.setAttribute("premain",premain);
		request.setAttribute("pamount",pamount);
		request.setAttribute("pcurren",pcurren);
//		request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
		listlogger.add("resultadoOperacion:"+resultadoOperacion);
		listlogger.add("bean:"+beanCotizacion.toString());
		logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
		listlogger.clear();
		this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_REALIZAR_PAGO).forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			listlogger.add(e.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			Date fechaHoy = new Date();
			Long milisecFinOperacion = fechaHoy.getTime();
			request.setAttribute("milisecFinOperacion", milisecFinOperacion);	
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("Mensaje","ERROR");
//			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			listlogger.add("resultadoOperacion:"+resultadoOperacion);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPago.PAGE_REALIZAR_PAGO).forward(request, response);		
		}
	}
}
