package com.kranon.servlet.controlador.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.bean.ResponseWarning;
import com.kranon.servicios.deleteTicket.DeleteTicket;
import com.kranon.servlet.controlador.ServletBaseControlador;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;

/**
 * Servlet para EJECUTAR EL DELETE TICKET CUANDO LA LLAMADA LLEGA AL CONTROLADOR COMO FIN
 * 
 * @author asernag
 *
 */
public class ServletSubDeleteTicket extends ServletBaseControlador {

	private static final long serialVersionUID = 1L;
	protected CommonLoggerKranon logKranon;
	protected List<String> listlogger = new ArrayList<String>();
	public ServletSubDeleteTicket() {

		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_DELETE_TICKET).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idLlamada = "";
		String idServicio = "";
		String idElemento = "";
		String idModulo = "DELETE_TICKET";
		String tsecPrivado;
		String tsecAnulado = "NO";
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
		listlogger.add("DELETE_TICKET");
		// ** PARAMETROS DE SALIDA
		String resultadoOperacion = "";
		String error = "";
		try {

			// ** PARAMETROS DE ENTRADA
			idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
			idElemento = request.getParameter("VG_loggerServicio.idElemento");
			idServicio = request.getParameter("VG_loggerServicio.idServicio");
			tsecPrivado = request.getParameter("VG_cliente.tsecPrivado");
			logKranon.setIdServicio(idLlamada);
			String codigoRetorno = request.getParameter("VG_codigoRetorno");
			
			String objetoIdentif = request.getParameter("PRM_objetoIdentif");
			String idCliente = request.getParameter("PRM_idCliente");

			/** INICIALIZAR LOG **/
			this.log = new CommonLoggerService(idServicio);
			this.log.inicializar(idLlamada, idServicio, idElemento);

			/** INICIO EVENTO - INICIO MODULO **/
			ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
			parametrosEntrada.add(new ParamEvent("codigoRetorno", codigoRetorno));
			parametrosEntrada.add(new ParamEvent("tsecPrivado", UtilidadesBeans.cifrarTsec(tsecPrivado, 20)));
			this.log.initModuleService(idModulo, parametrosEntrada);
			/** FIN EVENTO - INICIO MODULO **/

			// *****************************************************
			// ***** INTEGRACIoN WS Delete Ticket con el GrantingTicket
			// *****************************************************

			ConexionResponse conexionResponse = null;

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("EXECUTING_DELETE_TICKET", "", null);
			/** FIN EVENTO - ACCION **/

			/** LLAMO AL SERVICIO **/
			// [20161216-NMB] SE INCLUYE EL idServicio PARA ESCRIBIR LAS TRAZAS DE MONITORIZACION
			DeleteTicket dt = new DeleteTicket(idLlamada, idServicio, idElemento);
			dt.preparaPeticion(tsecPrivado);
			conexionResponse = dt.ejecutaPeticion();

			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionalesAccion = new ArrayList<ParamEvent>();
			parametrosAdicionalesAccion.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
			if (conexionResponse.getErrorResponse() != null) {
				parametrosAdicionalesAccion.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
			}
			this.log.actionEvent("EXECUTED_DELETE_TICKET", conexionResponse.getResultado(), parametrosAdicionalesAccion);
//			listlogger.add("EXECUTED_DELETE_TICKET");
			if(conexionResponse!=null){
				listlogger.add(conexionResponse.getResultado());
				listlogger.add(""+conexionResponse.getCodigoRespuesta());
			}
			
			/** FIN EVENTO - ACCION **/

			if (conexionResponse.getResultado().equalsIgnoreCase("OK")) {

				// la peticion ha ido bien, ha dado un 200 o un 204
				this.log.comment("getCodigoRespuesta=" + conexionResponse.getCodigoRespuesta());

				// estudiamos si el ResponseWarning es un "OPERACION EXITOSA"
				ResponseWarning responseWarning = conexionResponse.getResponseWarning();

				if (responseWarning != null) {

					this.log.actionEvent("responseWarning", responseWarning.getCode() + "|" + responseWarning.getDescription(), null);

					if (responseWarning.getCode().equals("J2A0001") && responseWarning.getDescription().equalsIgnoreCase("OPERACION EXITOSA")) {
						// Responsewarningcode: J2A0001
						// Responsewarningdescription: OPERACION EXITOSA
						this.log.comment("responseWarningCode=" + responseWarning.getCode());
						this.log.comment("responseWarningDescription=" + responseWarning.getDescription());

						resultadoOperacion = "OK";
						error = "";

						tsecAnulado = "SI";
					}
				}
			}

			if (tsecAnulado.equals("NO")) {
				// no se ha anulado el tsec correctamente
				resultadoOperacion = "KO";
				error = "ERROR_EXT(WS_DELETE_TICKET)";

				tsecAnulado = "NO";
			}

			/** INICIO EVENTO - STATISTICS **/
			ArrayList<ParamEvent> parametrosAdicStat = new ArrayList<ParamEvent>();
			parametrosAdicStat.add(new ParamEvent("idCliente", idCliente));
			parametrosAdicStat.add(new ParamEvent("objetoIdentif", UtilidadesBeans.cifrar(objetoIdentif, 5)));
			this.log.statisticEvent("DELETE_TICKET", tsecAnulado.equals("SI") ? "OK" : "KO", parametrosAdicStat);
			listlogger.add(tsecAnulado.equals("SI") ? UtilidadesLoggerKranon.deleteTsec : UtilidadesLoggerKranon.nodeleteTsec);
			/** FIN EVENTO - STATISTICS **/


			// TODO trucado para pruebas
//			 resultadoOperacion = "OK";
//			 error = "";
			// conexionResponse.setResultado("KO");
			// conexionResponse.setCodigoRespuesta(409);
			// TODO trucado para pruebas

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("tsecAnulado", tsecAnulado);
			
			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("responseCode", conexionResponse.getCodigoRespuesta() + ""));
			parametrosSalida.add(new ParamEvent("tsecAnulado", tsecAnulado));
			if (conexionResponse.getErrorResponse() != null) {
				parametrosSalida.add(new ParamEvent("errorCode", conexionResponse.getErrorResponse().getErrorCode()));
			}
			if (error != null && !error.equals("")) {
				parametrosSalida.add(new ParamEvent("error", error));
			}
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			if (conexionResponse.getResultado().equalsIgnoreCase("KO") || resultadoOperacion.equals("KO")) {
				// si el resultado es KO, hubo una respuesta
				parametrosAdicionales.add(new ParamEvent("respuestaKO", conexionResponse.getErrorResponse() == null ? "null" : conexionResponse
						.getErrorResponse().toString()));
			}
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN MODULO **/
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_DELETE_TICKET).forward(request, response);

		} catch (final Exception e) {

			if (this.log == null) {
				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
			}

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
			parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError, e);
			/** FIN EVENTO - ERROR **/

			resultadoOperacion = "KO";
			error = "ERROR_EXT(" + e.getMessage() + ")";

			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", resultadoOperacion);
			request.setAttribute("error", error);
			request.setAttribute("tsecAnulado", tsecAnulado);

			/** INICIO EVENTO - FIN MODULO **/
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("tsecAnulado", tsecAnulado));
			parametrosSalida.add(new ParamEvent("error", error));
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			this.log.endModuleService(idModulo, resultadoOperacion, parametrosSalida, parametrosAdicionales);
			/** FIN EVENTO - FIN MODULO **/
			listlogger.add("resultadoOperacion:"+resultadoOperacion);
			listlogger.add(parametrosSalida.toString());
			listlogger.add(e.getMessage().substring(20));
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServletBaseControlador.PAGE_DELETE_TICKET).forward(request, response);

		}
	}

}
