package com.kranon.servlet.modulo.zonaPago;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.serv.CommonLoggerService;

public abstract class  ServletBaseModuloZonaPago extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	// LOGGER IVR PARA MODULO IDENTIFICACION
	protected CommonLoggerService log; 
	
	private static String directorioModulo = "/Modulo-ZonaPago-OP";
	
	protected static final String PAGE_REALIZAR_PAGO = directorioModulo + "/subdialogos/MODULO-ZONAPAGO-OPENPAY.jsp";
	
	protected static final String PAGE_INICIO_PARAMS = directorioModulo + "/dynamicvxml/MODULO-ZONA-PAGO-OPEN-PAY-INICIO-PARAMS.jsp";

	protected static final String PAGE_EJECUTAR = directorioModulo + "/dynamicvxml/MODULO-ZONA-PAGO-OPEN-PAY-EJECUTAR.jsp";
		
	protected static final String PAGE_FIN = directorioModulo + "/dynamicvxml/MODULO-ZONA-PAGO-OPEN-PAY-FIN.jsp";
	

	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
