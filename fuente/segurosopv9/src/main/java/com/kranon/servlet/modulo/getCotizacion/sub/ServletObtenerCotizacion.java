package com.kranon.servlet.modulo.getCotizacion.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.cotizacion.bean.BeanCotizacion;
import com.kranon.loccomunes.BeanLocucionesComunes;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.servicios.bean.ConexionResponse;
import com.kranon.servicios.getCotizacion.ObtenerCotizacion;
import com.kranon.servicios.getCotizacion.bean.BeanObtenerCotizacionOut;
import com.kranon.servlet.modulo.getCotizacion.ServeltBaseModuloGetCotizacion;
import com.kranon.singleton.SingletonLocucionesComunes;
import com.kranon.util.UtilidadesLoggerKranon;

public class ServletObtenerCotizacion extends ServeltBaseModuloGetCotizacion{
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	
	public ServletObtenerCotizacion(){
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// todavia no podemos meter trazas de log porque no tenemos el
			// idLlamada

			this.doPost(request, response);

		} catch (final Exception e) {
			// Aado parametros de salida a la request
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			request.setAttribute("locucionSaludo", "");
			request.setAttribute("modoSaludo", "");
			this.getServletContext().getRequestDispatcher(ServeltBaseModuloGetCotizacion.PAGE_GET_COTIZACION).forward(request, response);

		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idServicio = "";
		String idLlamada = "";
		String idElemento="";
		String tsec="";
		String resultadoOperacion = "OK";
		String codigoRetorno = "OK";
		String error = "";
		String mensaje="";
		String idCotizacion="";
		BeanCotizacion beanCotizacion=new BeanCotizacion();
		try {
		idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
		logKranon= new CommonLoggerKranon("KRANON-LOG",idLlamada);
//		logKranon.setIdServicio(idLlamada);
		idServicio = request.getParameter("VG_loggerServicio.idServicio");
		idElemento = request.getParameter("VG_loggerServicio.idElemento");
		tsec = request.getParameter("VG_cliente.tsec");
		idCotizacion= request.getParameter("VG_DatosPrecotizacion.idCotiza");
		String idSession=request.getParameter("VG_DatosPrecotizacion.idSession");
//		System.out.println("idcotiza"+idCotizacion);
//		System.out.println("idseesion"+idSession);
		/** INICIALIZAR LOG **/
		this.log = new CommonLoggerService(idElemento + "_" + idServicio);
		this.log.inicializar(idLlamada, idServicio, idElemento);
		ConexionResponse conexionResponse = null;
		
//		HARDCORE para pruebas************************************************************
//		beanCotizacion=new BeanCotizacion();
//		beanCotizacion.setIdCotiza("12345678");
//		beanCotizacion.setIdSession("12345678901234567890123456789012");
//		beanCotizacion.setMontoCobro("2000");
//		beanCotizacion.setMonedaCobro("pesos");
//		beanCotizacion.setNombreCliente("cliente");
//		beanCotizacion.setApellidosCliente("Apellido");
//		beanCotizacion.setTelefono("55354127");
//		beanCotizacion.setCorreo("uncorreo@correo.com");
//		beanCotizacion.setAfiliacionId("12345678901234567890123");
		//*****************************************************************************
//#		TODO
//	/** LLAMO AL SERVICIO **/
		ObtenerCotizacion obtenerCotizacion= new ObtenerCotizacion(idLlamada, idServicio, idElemento);
		obtenerCotizacion.preparaPeticion(tsec,idCotizacion);
		conexionResponse = obtenerCotizacion.ejecutaPeticion();
		
		if (conexionResponse.getResultado().equals("OK")) {
			BeanObtenerCotizacionOut beanCotizacionOut=obtenerCotizacion.parserSalida (conexionResponse.getMensajeRespuesta());
			if(beanCotizacionOut.getCodError().equals("00000")){
				beanCotizacion.setIdCotiza(idCotizacion);
				beanCotizacion.setIdSession(idSession);
				beanCotizacion.setMontoCobro(beanCotizacionOut.getMonto());
				beanCotizacion.setMonedaCobro(beanCotizacionOut.getMoneda());
				beanCotizacion.setNombreCliente(beanCotizacionOut.getNombreCliente());
				beanCotizacion.setApellidosCliente(beanCotizacionOut.getApellidoCliente());
				beanCotizacion.setTelefono(beanCotizacionOut.getTelefono());
				beanCotizacion.setCorreo(beanCotizacionOut.getCorreo());
//				beanCotizacion.setAfiliacionId(beanCotizacionOut.getAfiliacion());
				beanCotizacion.setDescripcion(beanCotizacionOut.getDescProd());
				// la peticion ha ido OK-200
				resultadoOperacion = "OK";
				codigoRetorno = "OK";
				error = "";	
				listlogger.add("bean:"+beanCotizacion.toString());
			}
			else{
				resultadoOperacion = "ERROR";
				codigoRetorno = "KO";
				error = "ERROR";
				mensaje="COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP";
				listlogger.add(beanCotizacionOut.getCodError());
			}
		}
		else if(conexionResponse.getCodigoRespuesta()== 404 || conexionResponse.getCodigoRespuesta()== 500){
			resultadoOperacion = "ERROR";
			codigoRetorno = "KO";
			error = "ERROR";
			mensaje="COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP";
		}
		
		else{
			resultadoOperacion = "KO";
			codigoRetorno = "KO";
			error = "ERROR";
			mensaje="COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP";
		}
		Date fechaHoy = new Date();
		Long milisecFinOperacion = fechaHoy.getTime();
		request.setAttribute("milisecFinOperacion", milisecFinOperacion);	
		request.setAttribute("resultadoOperacion",resultadoOperacion);
		request.setAttribute("codigoRetorno",codigoRetorno);
		request.setAttribute("error",error);
		request.setAttribute("Cotizacion",beanCotizacion);
		request.setAttribute("mensaje",mensaje);
//		request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
		listlogger.add("resultadoOperacion:"+resultadoOperacion);
		
		logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
		listlogger.clear();
		this.getServletContext().getRequestDispatcher(ServeltBaseModuloGetCotizacion.PAGE_GET_COTIZACION).forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			listlogger.add(e.toString());
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			Date fechaHoy = new Date();
			Long milisecFinOperacion = fechaHoy.getTime();
			request.setAttribute("milisecFinOperacion", milisecFinOperacion);	
			request.setAttribute("resultadoOperacion", "KO");
			request.setAttribute("Mensaje","COM-SEGUROSOPV9-INFO-SISTEMA-NO-DISP");
//			request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");
			listlogger.add("resultadoOperacion:"+resultadoOperacion);
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();
			this.getServletContext().getRequestDispatcher(ServeltBaseModuloGetCotizacion.PAGE_GET_COTIZACION).forward(request, response);		
		}
	}
}
