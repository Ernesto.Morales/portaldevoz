package com.kranon.servlet.modulo.zonaPagoDatos.sub;

import com.kranon.servlet.modulo.zonaPagoDatos.ServletBaseModuloZonaPagoDatos;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.serv.CommonLoggerService;


public class ServletSubMesTarjeta extends ServletBaseModuloZonaPagoDatos {

		private static final long serialVersionUID = 1L;

		public ServletSubMesTarjeta() {

			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			try {
				// todavia no podemos meter trazas de log porque no tenemos el
				// idLlamada

				this.doPost(request, response);

			} catch (final Exception e) {

				// Aado parametros de salida a la request
				request.setAttribute("resultadoOperacion", "KO");
				request.setAttribute("codigoRetorno", "ERROR");
				request.setAttribute("error", "ERROR_EXT(" + e.getMessage() + ")");

				this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_MESTARJETA).forward(request, response);
			}
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			String idLlamada = "";
			String idServicio = "";
			String idElemento = "";
			String idModulo = "MODULO_ZPDATOS";

			// ** PARAMETROS DE SALIDA
			String resultadoOperacion = "";
			String error = "";
			try {

				// ** PARAMETROS DE ENTRADA
				idLlamada = request.getParameter("VG_loggerServicio.idLlamada");
				idElemento = request.getParameter("VG_loggerServicio.idElemento");
				idServicio = request.getParameter("VG_loggerServicio.idServicio");

				String maxIntentosCandado = request.getParameter("PRM_intentosCandado");

				/** INICIALIZAR LOG **/
				this.log = new CommonLoggerService(idServicio);
				this.log.inicializar(idLlamada, idServicio, idElemento);
				
				request.setAttribute("idServicio", idServicio);

				resultadoOperacion = "OK";
				error = "";
				
				/** INICIO EVENTO - ACCION **/
				String idAccion = "SUB_TARJETA";
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("maxIntentosCandado", maxIntentosCandado));
				if (error != null && !error.equals("")) {
					parametrosAdicionales.add(new ParamEvent("error", error));
				}
				this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_MESTARJETA).forward(request, response);

			} catch (final Exception e) {

				if (this.log == null) {
					/** INICIALIZAR LOG **/
					this.log = new CommonLoggerService(idServicio);
					this.log.inicializar(idLlamada, idServicio, idElemento);
				}

				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionalesError = new ArrayList<ParamEvent>();
				parametrosAdicionalesError.add(new ParamEvent("error", e.toString()));
				this.log.error(e.getMessage(), idModulo, parametrosAdicionalesError,e);
				/** FIN EVENTO - ERROR **/

				resultadoOperacion = "KO";
				error = "ERROR_EXT(" + e.getMessage() + ")";

				// Aado parametros de salida a la request
				request.setAttribute("resultadoOperacion", resultadoOperacion);
				request.setAttribute("error", error);

				/** INICIO EVENTO - ACCION **/
				String idAccion = "SUB_TARJETA";
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", error));
				this.log.actionEvent(idAccion, resultadoOperacion, parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				this.getServletContext().getRequestDispatcher(ServletBaseModuloZonaPagoDatos.PAGE_SUB_MESTARJETA).forward(request, response);

			}
		}
}
