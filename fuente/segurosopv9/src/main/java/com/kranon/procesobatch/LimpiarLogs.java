package com.kranon.procesobatch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.logger.all.CommonLoggerKranon;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.util.UtilidadesLoggerKranon;
import com.kranon.utilidades.UtilidadesBeans;


public class LimpiarLogs {
	private File[] listaArchivos=null;
	private File[] listaDirectorios=null;
	private CommonLoggerProcess log;
	private String idElemento="CargaContadores";
	private String idLlamada;
	private String idInvocacion;
	private static  LimpiarLogs instancia;	
	private boolean procesar;
	private static final String ARCHIVO_LOCK = "pidL.lock";
	private String fechaEjecucion;
	private String rutaArchivos;
	private Integer contador=0;
	private String resultadoOperacion = "KO";
	private CommonLoggerKranon logKranon;
	private List<String> listlogger = new ArrayList<String>();
	
	private LimpiarLogs(String idInvocacion){
		log = new CommonLoggerProcess("OBTENER_LOGS"); 
		log.inicializar(idInvocacion, idElemento); // el null de idInvocacieeeeen es como el IdLlamada
		idLlamada = idInvocacion;

	}

	public static LimpiarLogs getInstance(String idInvocacion){
		if( instancia==null ){
			instancia = new LimpiarLogs(idInvocacion);
		}
		else{
			return null;
		}

		return instancia;
	}

	public static void releaseInstance(){
		instancia = null;
	}

	
	/*
	 *Se Obtienen los archivos y directorios encontrados en logs 
	 */
	private void ProcesarArchivos(String ruta){

		String rutaArchivos=ruta;
		String idModuloLog = "OBTENER_LOGS";
		
		int numArchivosEncontrados = 0;

		/** INICIO EVENTO - INICIO DE MoDULO **/
//		log.initModuleProcess(idModuloLog, null);
		/** FIN EVENTO - INICIO DE MoDULO **/
		//Declaracion de variables
		File directorioTrabajo = null;		
//		FiltroArchivosCM filtroArchivos = null;
		try{
		File[] listaArchivosEncontrados = null;
		directorioTrabajo = new File(rutaArchivos);
		if( directorioTrabajo.isDirectory() ){
			listaArchivosEncontrados = directorioTrabajo.listFiles();
		}
		numArchivosEncontrados = (listaArchivosEncontrados!=null)? listaArchivosEncontrados.length:0;		
		resultadoOperacion = "OK";
		if(numArchivosEncontrados!=0){
			for(File file: listaArchivosEncontrados){
				if(file.isDirectory()){
					if(file.toString().contains("SEGUROSOPV9") || file.toString().contains("MONITORIZACION")){
//						System.out.println("ProcesaArchivos = " + file.toString());
						ProcesarArchivos(file.getPath());
					} 
//					else{
//						System.out.println("NoProcesaArchivos = " + file.toString());
//					}
				}
				else{
					if(Eliminar(file)){
						contador++;
					}
				}
			}
		}
		}catch(Exception ex){			
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), idModuloLog,	parametrosAdicionales, ex);
			resultadoOperacion = "KO";
		}
//		return null;
	}
	
	public static boolean Eliminar(File file){
		Boolean variable=false;
		int ant=2;
		long fileDateModifated = file.lastModified();
		Date dateMod = new Date(fileDateModifated);
		int mesesN = calcularMesesAFecha(dateMod,new Date());
		if (mesesN >= ant) {
			try{
//			strResultado = strResultado + "CumpleCriteriosElimina:["+ficheros[i]+"]-";
			file.delete();
			variable=true;
			}catch(SecurityException e){
				variable=false;
			}
			
		}
		return variable;
	}
	
	// Calcula la difrencia de meses entre una fecha y otra
	public static int calcularMesesAFecha(Date fechaInicio, Date fechaFin) {
		try {
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(fechaInicio);
			Calendar endCalendar = Calendar.getInstance();
			endCalendar.setTime(fechaFin);
			int startMes = (startCalendar.get(Calendar.YEAR) * 12)+ startCalendar.get(Calendar.MONTH);
			int endMes = (endCalendar.get(Calendar.YEAR) * 12)+ endCalendar.get(Calendar.MONTH);
			int diffMonth = endMes - startMes;
			return diffMonth;
		} catch (Exception e) {
			return 0;
		}
	}
	
//	 public static void main(String[] args) {
//
//		 ProcesarArchivos("W:/Desarrollo/mots_pvo/online/multipais/web/logs/borrar");
//		 
//	}

	public void limpiar() {
		String idModuloLog = "PROCESAR";
		resultadoOperacion = "KO";
		String idProcActual = "";
		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess(idModuloLog, null);
		/** FIN EVENTO - INICIO DE MoDULO **/
		logKranon= new CommonLoggerKranon("CRON-LOG",idLlamada);
		
		//1. Inicializar variables
		inicializarVariables();
		if( procesar ){
			ProcesarArchivos(rutaArchivos);	
			//Se elimina archivo pid
			try{
				File f = new File(rutaArchivos+"/" + ARCHIVO_LOCK);
				if( f.exists() )
					f.delete();
			}catch(SecurityException se){
				/** INICIO EVENTO - ERROR **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", se.toString()));
				this.log.error(se.getMessage(), idModuloLog,	parametrosAdicionales, se);
				/** FIN EVENTO - ERROR **/
				listlogger.add( se.toString());
				logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
				listlogger.clear();	
			}	
		}
		else{
			listlogger.add("error doble proceso");
			logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
			listlogger.clear();	
		}
		ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
		parametrosAdicionales.add(new ParamEvent("AD", contador.toString()));
		parametrosAdicionales.add(new ParamEvent("res", resultadoOperacion));
		this.log.actionEvent("borrararchivos", resultadoOperacion, parametrosAdicionales);
		listlogger.add(contador.toString());
		listlogger.add(resultadoOperacion);
		logKranon.imprimeLogKranon(UtilidadesLoggerKranon.getTrazaLista(listlogger));
		listlogger.clear();	
	}
	
	
	/*
	 * Se obtienen las variables a leer del archivo properties como:
	 * * Ruta de archivos a procesar
	 *
	 */
	private void inicializarVariables(){

		String idModuloLog = "INICIALIZAR";
		resultadoOperacion = "KO";

		/** INICIO EVENTO - INICIO DE MoDULO **/
		log.initModuleProcess(idModuloLog, null);
		/** FIN EVENTO - INICIO DE MoDULO **/

		UtilidadesBeans util = new UtilidadesBeans(log);
		rutaArchivos = util.leeParametroUnmarshall("RUTA_ARCHIVOS_LOGS");
//		System.out.println(rutaArchivos);
		//Se genera la fecha de ejecucion en base al dia de hoy formato YYYYMMDD
		SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd", new Locale("es", "MX") );
		Date fechaHoy  = new Date();
		fechaEjecucion = formateador.format( fechaHoy );
		/*
		 * Se genera un archivo pid para que no existan dos procesos corriendo a la vez
		 */
		try{
			File f = new File(rutaArchivos+"/" + ARCHIVO_LOCK);
			if( !f.exists()){
				FileOutputStream outputStream = new FileOutputStream(f);				
				byte[] strToBytes = this.idLlamada.getBytes();
				outputStream.write(strToBytes);			 
				outputStream.close();
				procesar = true;
			}			
			resultadoOperacion = "OK";
		}catch(IOException ioe){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ioe.toString()));
			this.log.error(ioe.getMessage(), idModuloLog,	parametrosAdicionales, ioe);
			resultadoOperacion = "KO";
			/** FIN EVENTO - ERROR **/			
		}
		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("procesar", String.valueOf(procesar)));
		parametrosSalida.add(new ParamEvent("idLlamada", this.idLlamada));
		parametrosSalida.add(new ParamEvent("FI:", fechaEjecucion));
		this.log.endModuleProcess(idModuloLog, resultadoOperacion, parametrosSalida, null);
		/** FIN EVENTO - FIN MODULO **/
	}
}
