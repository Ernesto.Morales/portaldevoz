package com.kranon.procesobatch;

import java.util.ArrayList;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import org.springframework.scheduling.annotation.Scheduled;

//@Component
//@EnableAsync
//@EnableScheduling
//@Configuration
public class GestionLogsBatch {
//	@Scheduled(cron="${scheduling.job.cron.lg}")
//  @Scheduled(fixedDelay = 2000)	
	public void iniciarCarga() throws Exception{
		// creo el objeto de logger
		String idModulo = "INICIAR_GESTION_LOGS";
		CommonLoggerProcess log = new CommonLoggerProcess("OBTENER_LOGS");
		// envio el idInvocacion a null para que me genere uno
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), "OBTENER_LOGS");
		log.initModuleProcess(idModulo, null);
		LimpiarLogs limpiarLogs = null;
		try {
			
			synchronized(this){
				limpiarLogs = LimpiarLogs.getInstance(log.getIdInvocacion());
			}
			if (limpiarLogs!=null){
				limpiarLogs.limpiar();
			}
			
		} catch (Exception e) {
			//** INICIO EVENTO - ERROR **//
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "OBTENER_LOGS", parametrosAdicionales, e);
			//** FIN EVENTO - ERROR **//
		}finally{
			/** INICIO EVENTO - FIN MODULO **/			
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add(new ParamEvent("fueProcesado", (limpiarLogs==null)?"NO":"SI"));
			log.endModuleProcess(idModulo, "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
			synchronized(this){
				if( limpiarLogs != null ){		
					limpiarLogs = null;
					LimpiarLogs.releaseInstance();
				}
			}
		}
	}
}
