package com.kranon.menuplantilla;


/**
 * Tipo de objeto para la matriz de locuciones
 * 
 *
 */
public class PromptMenu {

	public String prompt;
	public String modo;
	public String uso;
	
	
	public PromptMenu() {
		
	}
	
	public PromptMenu(String prompt, String modo, String uso) {
		this.prompt = prompt;
		this.modo = modo;
		this.uso = uso;
	}
	
	
	
	
}
