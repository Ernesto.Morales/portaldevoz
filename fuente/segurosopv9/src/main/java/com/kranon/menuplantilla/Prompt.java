package com.kranon.menuplantilla;

public class Prompt {

	private String tipo;
	private String contenido;
	
	public Prompt() {
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getContenido() {
		return contenido;
	}

}
