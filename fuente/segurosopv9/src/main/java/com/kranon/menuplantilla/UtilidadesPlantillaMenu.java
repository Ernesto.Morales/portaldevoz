package com.kranon.menuplantilla;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class UtilidadesPlantillaMenu {

	private static String PREFIJO_INTENTO = "PRM_OUT_MENU_trazas.intentos.";
	private static String PREFIJO_ELEMENTO = ".elementos.";

	public static ArrayList<String> ordenaArrayTrazas(ArrayList<String> listaTrazas) {

		ArrayList<String> listaTrazasOrdenado = null;
		try {

			if (listaTrazas == null) {
				return null;
			}
			listaTrazasOrdenado = new ArrayList<String>();
			// copia de seguridad
			for (int i = 0; i < listaTrazas.size(); i++) {
				listaTrazasOrdenado.add(new String(listaTrazas.get(i)));
			}

			Collections.sort(listaTrazasOrdenado, new Comparator<String>() {

				@Override
				public int compare(String param1, String param2) {

					// Saco el INTENTO y ELEMENTO del param1
					String auxParam1 = param1.substring(param1.indexOf(PREFIJO_INTENTO) + PREFIJO_INTENTO.length() - 1,
							param1.indexOf(PREFIJO_ELEMENTO) + 1);
					int indexInicioIntentoParam1 = auxParam1.indexOf(".");
					int indexFinIntentoParam1 = auxParam1.lastIndexOf(".");
					String intentoParam1 = auxParam1.substring(indexInicioIntentoParam1 + 1, indexFinIntentoParam1);
					int intentoIntParam1 = Integer.parseInt(intentoParam1);

					auxParam1 = param1.substring(param1.indexOf(PREFIJO_ELEMENTO) + PREFIJO_ELEMENTO.length() - 1);

					int indexInicioElementoParam1 = auxParam1.indexOf(".");
					int indexFinElementoParam1 = auxParam1.lastIndexOf(".");
					String elementoParam1 = auxParam1.substring(indexInicioElementoParam1 + 1, indexFinElementoParam1);
					int elementoIntParam1 = Integer.parseInt(elementoParam1);
					// Saco el INTENTO y ELEMENTO del param2
					String auxParam2 = param2.substring(param2.indexOf(PREFIJO_INTENTO) + PREFIJO_INTENTO.length() - 1,
							param2.indexOf(PREFIJO_ELEMENTO) + 1);
					int indexInicioIntentoParam2 = auxParam2.indexOf(".");
					int indexFinIntentoParam2 = auxParam2.lastIndexOf(".");
					String intentoParam2 = auxParam2.substring(indexInicioIntentoParam2 + 1, indexFinIntentoParam2);
					int intentoIntParam2 = Integer.parseInt(intentoParam2);

					auxParam2 = param2.substring(param2.indexOf(PREFIJO_ELEMENTO) + PREFIJO_ELEMENTO.length() - 1);

					int indexInicioElementoParam2 = auxParam2.indexOf(".");
					int indexFinElementoParam2 = auxParam2.lastIndexOf(".");
					String elementoParam2 = auxParam2.substring(indexInicioElementoParam2 + 1, indexFinElementoParam2);
					int elementoIntParam2 = Integer.parseInt(elementoParam2);

					if (intentoIntParam1 < intentoIntParam2) {
						// el parametro 1 va antes que el parametro 2
						return -1;
					} else if (intentoIntParam1 > intentoIntParam2) {
						// el parametro 1 va despues que el parametro 2
						return 1;
					} else {
						// son el mismo intento
						// los intentos son iguales, comparo los elementos
						if (elementoIntParam1 < elementoIntParam2) {
							// el parametro 1 va antes que el parametro 2

							return -1;
						} else if (elementoIntParam1 > elementoIntParam2) {
							// el parametro 1 va despues que el parametro 2

							return 1;
						} else {
							// son el mismo intento y el mismo elemento

							return 0;
						}
					}
				}

			});
		} catch (Exception e) {
			// devuelvo la lista original
			return listaTrazas;
		}

		return listaTrazasOrdenado;
	}

}
