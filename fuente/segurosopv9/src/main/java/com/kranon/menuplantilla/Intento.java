package com.kranon.menuplantilla;

import java.util.Vector;

public class Intento {

	// CONJUNTO DE PROMPTS QUE SE OFRECEN POR ASR
	private Vector<Prompt> prompts_voz;
	private Prompt prompt_nomatch_voz;
	private Prompt prompt_noinput_voz;
	
	// CONJUNTO DE PROMPTS QUE SE OFRECEN POR DTMF
	private Vector<Prompt> prompts_tono;
	private Prompt prompt_nomatch_tono;
	private Prompt prompt_noinput_tono;	
	
	public Intento() {
	}
		
	// ***********
	// PROMPTS ASR
	// ***********	
	public void setPrompts_voz(Vector<Prompt> promptsVoz) {
		this.prompts_voz = promptsVoz;
	}
	public Vector<Prompt> getPrompts_voz() {
		return prompts_voz;
	}
	public Prompt getPrompt_nomatch_voz() {
		return prompt_nomatch_voz;
	}
	public void setPrompt_nomatch_voz(Prompt promptNoMatchVoz) {
		this.prompt_nomatch_voz = promptNoMatchVoz;
	}
	public Prompt getPrompt_noinput_voz() {
		return prompt_noinput_voz;
	}
	public void setPrompt_noinput_voz(Prompt promptNoInputVoz) {
		this.prompt_noinput_voz = promptNoInputVoz;
	}


	
	
	// ************
	// PROMPTS DTMF
	// ************	

	public void setPrompts_tono(Vector<Prompt> promptsTonos) {
		this.prompts_tono = promptsTonos;
	}	
	public Vector<Prompt> getPrompts_tono() {
		return prompts_tono;
	}		

	public Prompt getPrompt_nomatch_tono() {
		return prompt_nomatch_tono;
	}
	public void setPrompt_nomatch_tono(Prompt promptNoMatchTono) {
		this.prompt_nomatch_tono = promptNoMatchTono;
	}
	public Prompt getPrompt_noinput_tono() {
		return prompt_noinput_tono;
	}
	public void setPrompt_noinput_tono(Prompt promptNoInputTono) {
		this.prompt_noinput_tono = promptNoInputTono;
	}

}
