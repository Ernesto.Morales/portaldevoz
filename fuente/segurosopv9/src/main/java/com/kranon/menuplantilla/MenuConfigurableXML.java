package com.kranon.menuplantilla;

import java.util.ArrayList;

import com.kranon.bean.BeanGramatica;
import com.kranon.bean.menu.BeanConfigOpcAsrMenu;
import com.kranon.bean.menu.BeanConfigOpcDtmfMenu;
import com.kranon.bean.menu.BeanConfigOpcMenu;
import com.kranon.bean.menu.BeanIntento;
import com.kranon.bean.menu.BeanMenu;
import com.kranon.bean.menu.BeanOpcionMenu;
import com.kranon.bean.menu.BeanPromptGenerico;
import com.kranon.bean.menu.BeanPrompts;
import com.kranon.bean.serv.BeanServicio;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;
import com.kranon.logger.serv.CommonLoggerService;
import com.kranon.singleton.SingletonMenu;
import com.kranon.singleton.SingletonServicio;

public class MenuConfigurableXML {

	private String logFile = "MENU";
	private CommonLoggerProcess log;

	public CommonLoggerProcess getLog() {
		return log;
	}

	public MenuConfigurableXML(String idLlamada, String idElemento) {

		this.log = new CommonLoggerProcess(logFile);

		this.log.inicializar(idLlamada, idElemento);

		this.log.comment("idLlamada: " + idLlamada);
		this.log.comment("idElemento: " + idElemento);
	}

	/**
	 * Obtencion del menu
	 * 
	 * @param codMenu
	 * @return {@link BeanMenu}
	 * @throws Exception
	 */
	public BeanMenu obtenerMenu(String idServicio, String codMenu) {

		/** INICIO EVENTO - INICIO MODULO **/
		ArrayList<ParamEvent> parametrosEntrada = new ArrayList<ParamEvent>();
		parametrosEntrada.add(new ParamEvent("idServicio", idServicio));
		parametrosEntrada.add(new ParamEvent("codMenu", codMenu));
		this.log.initModuleProcess("OBTENER_MENU", parametrosEntrada);
		/** FIN EVENTO - INICIO MODULO **/

		// recupero la info del menu, de su XML
		BeanMenu menu = null;
		try {

			// Unmarshall unmarshall = new
			// Unmarshall(this.log.getIdInvocacion(), this.log.getIdElemento());
			// menu = unmarshall.unmarshallMenu(idServicio, codMenu);

			// Recuperamos el Bean del Menu en cuestion
			// BeanMenu menu = null;
			SingletonMenu instance = SingletonMenu.getInstance(log);
			if (instance != null) {
				menu = instance.getMenu(idServicio, codMenu);
			}

			/** ORDENAMOS LAS OPCIONES POR ID **/
			if (menu != null) {
				// ordenar opciones por posicion
				if (menu.getOpciones() != null && menu.getOpciones().length > 1) {
					BeanOpcionMenu[] opcionesOrdenadas = ordenarOpcionesPorPosicion(menu.getOpciones());
					menu.setOpciones(opcionesOrdenadas);
				}
				/** ORDENAMOS LOS INTENTOS ASR POR ID **/
				if (menu.getConfigAsr() != null && menu.getConfigAsr().getPrompts() != null && menu.getConfigAsr().getPrompts().getIntentos() != null) {
					BeanIntento[] intentosOrdenados = ordenarIntentosPorId(menu.getConfigAsr().getPrompts().getIntentos());
					menu.getConfigAsr().getPrompts().setIntentos(intentosOrdenados);

					/** ORDENAMOS LOS PROMPT ASR POR ID **/
					for (int i = 0; i < (menu.getConfigAsr().getPrompts().getIntentos().length); i++) {
						// por cada intento, ordenamos sus prompt por id
						BeanPromptGenerico[] promptsOrdenados = ordenarPromptsPorId(menu.getConfigAsr().getPrompts().getIntentos()[i].getPrompts());
						menu.getConfigAsr().getPrompts().getIntentos()[i].setPrompts(promptsOrdenados);
					}
				}
				/** ORDENAMOS LOS INTENTOS DTMF POR ID **/
				if (menu.getConfigDtmf() != null && menu.getConfigDtmf().getPrompts() != null
						&& menu.getConfigDtmf().getPrompts().getIntentos() != null) {
					BeanIntento[] intentosOrdenados = ordenarIntentosPorId(menu.getConfigDtmf().getPrompts().getIntentos());
					menu.getConfigDtmf().getPrompts().setIntentos(intentosOrdenados);

					/** ORDENAMOS LOS PROMPT DTMF POR ID **/
					for (int i = 0; i < (menu.getConfigDtmf().getPrompts().getIntentos().length); i++) {
						// por cada intento, ordenamos sus prompt por id
						BeanPromptGenerico[] promptsOrdenados = ordenarPromptsPorId(menu.getConfigDtmf().getPrompts().getIntentos()[i].getPrompts());
						menu.getConfigDtmf().getPrompts().getIntentos()[i].setPrompts(promptsOrdenados);
					}
				}
				
				
				/** OBTENEMOS EL MODO INTERACCION DEL SERVICIO **/
				// [20161019] KRANON-ES
				// Recuperamos el Bean del Servicio en cuestion
				BeanServicio servicio = null;
				// inicializo el log de servicio
				CommonLoggerService logServ = new CommonLoggerService(idServicio);
				logServ.inicializar(log.getIdInvocacion(), idServicio, log.getIdElemento());
				SingletonServicio instanceServ = SingletonServicio.getInstance(logServ);
				if (instanceServ != null) {
					servicio = instanceServ.getServicio(idServicio);
				}
				// si no recuperamos bien el servicio, mantendremos el modo interaccion que mande el menu
				if (servicio != null) {
					String modoInteraccServ = servicio.getConfiguracion().getModoInteraccion();
					String modoInteraccMenu = menu.getConfiguracion().getModoInteraccion();
					if(!modoInteraccServ.equals(modoInteraccMenu)){
						 // si no coinciden es porque
						// serv=ASR y menu=DTMF -> luego debo coger el mas restrictivo, DTMF
						// serv=DTMF y menu=ASR -> luego debo coger el mas restrictivo, DTMF
						if(!modoInteraccMenu.equals("DTMF")){
							menu.getConfiguracion().setModoInteraccion("DTMF");
							
							/** INICIO EVENTO - ACCION **/
							ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
							parametrosAdicionales.add(new ParamEvent("modoInteraccFinal",  menu.getConfiguracion().getModoInteraccion()));
							parametrosAdicionales.add(new ParamEvent("modoInteraccServ", modoInteraccServ));
							parametrosAdicionales.add(new ParamEvent("modoInteraccMenu", modoInteraccMenu));
							this.log.actionEvent("CAMBIO_MODO_INTERACCION_SERV", "OK", parametrosAdicionales);
							/** FIN EVENTO - ACCION **/
						}
					}

				}
			}

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "OBTENER_MENU", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - FIN MODULO **/
			this.log.endModuleProcess("OBTENER_MENU", "KO", null, null);
			/** FIN EVENTO - FIN MODULO **/

			return null;
		}

		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
		parametrosSalida.add(new ParamEvent("menu", menu.toString()));
		this.log.endModuleProcess("OBTENER_MENU", "OK", parametrosSalida, null);
		/** FIN EVENTO - FIN MODULO **/

		return menu;
	}

	/**
	 * Ordena el array de opciones del menu segun la posicion de cada una, de 1
	 * a 9 y ultimo la 0; y * en la pocisión 10
	 * 
	 * @param opciones
	 * @return
	 * @throws Exception
	 */
	private BeanOpcionMenu[] ordenarOpcionesPorPosicion(BeanOpcionMenu[] opciones) throws Exception {

		this.log.comment("opciones=" + opciones.toString());

		BeanOpcionMenu[] opcionesOrdenadas;
		try {
			opcionesOrdenadas = opciones;

			for (int i = 0; i < (opcionesOrdenadas.length - 1); i++) {
				for (int j = i + 1; j < opcionesOrdenadas.length; j++) {
					int posicionI = (opcionesOrdenadas[i].getConfiguracion().getPosicion().equals("."))?new Integer(10):Integer.parseInt(opcionesOrdenadas[i].getConfiguracion().getPosicion());
					int posicionJ = (opcionesOrdenadas[j].getConfiguracion().getPosicion().equals("."))?new Integer(10):Integer.parseInt(opcionesOrdenadas[j].getConfiguracion().getPosicion());
					if (posicionI > posicionJ) {
						// Intercambiamos valores
						BeanOpcionMenu variableauxiliar = opcionesOrdenadas[i];
						opcionesOrdenadas[i] = opcionesOrdenadas[j];
						opcionesOrdenadas[j] = variableauxiliar;
					}
				}
			}

			if (Integer.parseInt(opcionesOrdenadas[0].getConfiguracion().getPosicion()) == 0) {
				// si la primera opcion tiene posicion 0, tengo que moverla al
				// final
				// me guardo esta opcion 0
				BeanOpcionMenu opcion0 = opcionesOrdenadas[0];
				for (int i = 0; i < (opcionesOrdenadas.length - 1); i++) {
					// muevo todos los elementos una posicion a la izquierda
					opcionesOrdenadas[i] = opcionesOrdenadas[i + 1];
				}
				// guardo la opcion 0 al final
				opcionesOrdenadas[opcionesOrdenadas.length - 1] = opcion0;
			}

			this.log.comment("opcionesOrdenadas=" + opcionesOrdenadas.toString());
		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "ordenarOpcionesPorPosicion", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("ordenarOpcionesPorPosicion", "KO", null);
			/** FIN EVENTO - ACCION **/

			throw e;
		}
		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("ordenarOpcionesPorPosicion", "OK", null);
		/** FIN EVENTO - ACCION **/

		return opcionesOrdenadas;
	}

	/**
	 * Ordenar los intentos por orden de su id de menor a mayor
	 * 
	 * @param {@link BeanIntento[]} intentos
	 * @return {@link BeanIntento[]}
	 * @throws Exception
	 */
	private BeanIntento[] ordenarIntentosPorId(BeanIntento[] intentos) throws Exception {
		this.log.comment("intentos=" + intentos.toString());

		BeanIntento[] intentosOrdenados;

		try {
			intentosOrdenados = intentos;

			for (int i = 0; i < (intentosOrdenados.length - 1); i++) {
				for (int j = i + 1; j < intentosOrdenados.length; j++) {
					int posicionI = Integer.parseInt(intentosOrdenados[i].getId());
					int posicionJ = Integer.parseInt(intentosOrdenados[j].getId());
					if (posicionI > posicionJ) {
						BeanIntento variableauxiliar = intentosOrdenados[i];
						intentosOrdenados[i] = intentosOrdenados[j];
						intentosOrdenados[j] = variableauxiliar;

					}
				}
			}

			this.log.comment("intentosOrdenados=" + intentosOrdenados.toString());
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "ordenarIntentosPorId", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("ordenarIntentosPorId", "KO", null);
			/** FIN EVENTO - ACCION **/

			throw e;
		}

		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("ordenarIntentosPorId", "OK", null);
		/** FIN EVENTO - ACCION **/

		return intentosOrdenados;
	}

	/**
	 * Ordenar los prompt de un intento por id de menor a mayor
	 * 
	 * @param {@link BeanPromptGenerico[]} prompts
	 * @return {@link BeanPromptGenerico[]}
	 * @throws Exception
	 */
	private BeanPromptGenerico[] ordenarPromptsPorId(BeanPromptGenerico[] prompts) throws Exception {
		this.log.comment("prompts=" + prompts.toString());

		BeanPromptGenerico[] promptsOrdenados;

		try {
			promptsOrdenados = prompts;

			for (int i = 0; i < (promptsOrdenados.length - 1); i++) {
				for (int j = i + 1; j < promptsOrdenados.length; j++) {
					int posicionI = Integer.parseInt(promptsOrdenados[i].getId());
					int posicionJ = Integer.parseInt(promptsOrdenados[j].getId());
					if (posicionI > posicionJ) {
						BeanPromptGenerico variableauxiliar = promptsOrdenados[i];
						promptsOrdenados[i] = promptsOrdenados[j];
						promptsOrdenados[j] = variableauxiliar;

					}
				}
			}

			this.log.comment("promptsOrdenados=" + promptsOrdenados.toString());
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "ordenarPromptsPorId", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("ordenarPromptsPorId", "KO", null);
			/** FIN EVENTO - ACCION **/

			throw e;
		}
		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("ordenarPromptsPorId", "OK", null);
		/** FIN EVENTO - ACCION **/

		return promptsOrdenados;
	}

	/**
	 * Devuelve true si hay alguna gramatica configurada para el modo de
	 * interaccion, en otro caso devuelve falso
	 * 
	 * @param {@link BeanOpcionMenu[]} opciones
	 * @param {@link String} modo
	 * @return {@link boolean} true si hay gramaticas
	 * @throws Exception
	 */
	public boolean hayGrammarEnOpciones(BeanOpcionMenu[] opciones, String modo) throws Exception {

		if (opciones.length == 0) {
			return false;
		} else {
			try {
				for (BeanOpcionMenu opcion : opciones) {
					if (modo.equals("DTMF")) {
						String grammarDtmf = opcion.getConfigDtmf().getGramaticaDtmf().getValue();
						if (grammarDtmf != null && !grammarDtmf.equals("")) {
							// hay al menos una gramatica de tonos
							return true;
						}
					} else {
						String grammarAsr = opcion.getConfigAsr().getGramaticaAsr().getValue();
						if (grammarAsr != null && !grammarAsr.equals("")) {
							// hay al menos una gramatica de voz
							return true;
						}
					}

				}
			} catch (Exception e) {
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				this.log.actionEvent("hayGrammarEnOpciones", "ERROR", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				throw e;
			}
		}
		return false;
	}

	/**
	 * Compruebo que las variables recibidas en el subdialogo coincidan en
	 * numero con las definidas en el primer intento
	 * 
	 * @param {@link BeanIntento[]} intentos
	 * @param numVariablesRecibidas
	 * @return
	 * @throws Exception
	 */
	public boolean comprobarVariablesDinamicas(BeanIntento[] intentos, int numVariablesRecibidas) throws Exception {

		int numVariablesPrompt = 0;
		try {
			for (BeanIntento intento : intentos) {
				numVariablesPrompt = 0;
				// por cada intento
				for (BeanPromptGenerico p : intento.getPrompts()) {
					// para cada prompt generico del intento, cuento cuantos de
					// ellos son variables
					if (p.getUso() != null && !p.getUso().equals("")) {
						// es variable
						numVariablesPrompt = numVariablesPrompt + 1;
					}
				}
				if (numVariablesPrompt != numVariablesRecibidas) {
					// si no coinciden, devuelvo falso. En otro caso sigo
					return false;
				}
			}
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "comprobarVariablesDinamicas", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			/** INICIO EVENTO - ACCION **/
			this.log.actionEvent("comprobarVariablesDinamicas", "KO", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			throw e;
		}
		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("comprobarVariablesDinamicas", "OK", null);
		/** FIN EVENTO - ACCION **/

		return numVariablesPrompt == numVariablesRecibidas;

	}

	/**
	 * 
	 * Rellena la matriz de locuciones en funcion de la configuracion
	 * (ASR/DTMF), el numero de intentos para este modo, y los parametros de
	 * entrada
	 * 
	 * @param {@link BeanConfigReconocimiento} configuracion
	 * @param {@link int} numIntentos
	 * @param {@link String[]} variablesMenuArray
	 * @return {@link PromptMenu[][]} matriz de locuciones
	 * @throws Exception
	 */
	// public PromptMenu[][] creaMatrizLocuciones (BeanConfigReconocimiento
	// configuracion, int numIntentos, String[] variablesMenuArray){
	public PromptMenu[][] creaMatrizLocuciones(BeanPrompts promptsMenu, int numIntentos, String[] variablesMenuArray) throws Exception {

		PromptMenu[][] prompts = null;

		try {
			// creo la matriz con los prompt
			prompts = new PromptMenu[numIntentos][promptsMenu.getIntentos()[0].getPrompts().length];

			// Recupero los prompts genericos de los intentos
			if (promptsMenu != null) {
				BeanIntento[] intentos = promptsMenu.getIntentos();
				// por cada intento, recupero prompts
				// i: se refiere a cada intento
				for (int i = 0; i < numIntentos; i++) {
					BeanIntento beanIntento;
					if (i >= intentos.length) {
						// he copiado ya todos los voz, me quedo con el ultimo
						beanIntento = intentos[intentos.length - 1];
					} else {
						beanIntento = intentos[i];
					}

					int contadorVariables = 0;
					// j: se refiere a cada prompt del intento i
					for (int j = 0; j < beanIntento.getPrompts().length; j++) {
						BeanPromptGenerico beanPrompt = beanIntento.getPrompts()[j];

						String promptValue = beanPrompt.getValue();
						if (beanPrompt.getUso() != null && !beanPrompt.getUso().equals("")) {
							// el uso del prompt esta relleno, voy a usar una
							// variable
							promptValue = variablesMenuArray[contadorVariables];
							// dejo listo para la siguiente variable
							contadorVariables = contadorVariables + 1;

						}

						// LOGGER.info("Relleno prompt INTENTO " + i +
						// " numero prompt: " + j);
						// LOGGER.info("Prompt value: " + promptValue);
						// LOGGER.info("Prompt modo: " + beanPrompt.getModo());
						// LOGGER.info("Prompt uso: " + beanPrompt.getUso());
						// LOGGER.info("***********");

						// relleno la matriz
						prompts[i][j] = new PromptMenu();
						prompts[i][j].prompt = promptValue;
						prompts[i][j].modo = beanPrompt.getModo();
						prompts[i][j].uso = beanPrompt.getUso();

					}
				}
			}
		} catch (Exception e) {

			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "creaMatrizLocuciones", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			throw e;
		}

		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("creaMatrizLocuciones", "OK", null);
		/** FIN EVENTO - ACCION **/

		return prompts;
	}

	// public PromptMenu[][] creaMatrizLocucionesOpciones (String modo,
	// BeanConfigReconocimiento configuracion, BeanOpcionMenu[] opciones, int
	// numIntentos, String[] variablesMenuArray){
	public PromptMenu[][] creaMatrizLocucionesOpciones(String modo, BeanPrompts promptsMenu, BeanOpcionMenu[] opciones, int numIntentos,
			String[] variablesMenuArray) throws Exception {

		PromptMenu[][] prompts = null;

		ArrayList<BeanOpcionMenu> listaOpcionesVisibles = filtraOpcionesActivasYVisibles(opciones);
		int numeroOpcionesVisibles = listaOpcionesVisibles == null ? 0 : listaOpcionesVisibles.size();

		int numeroPrompts = promptsMenu.getIntentos()[0].getPrompts().length;

		int numeroPromptsTotal = 0;
		if (modo != null && modo.equalsIgnoreCase("ASR")) {
			// por opcion tendremos (intro, normal y outro)
			numeroPromptsTotal = numeroPrompts + (3 * numeroOpcionesVisibles);
		} else {
			// por opcion tendremos (intro, normal, outro y posicion)
			numeroPromptsTotal = numeroPrompts + (4 * numeroOpcionesVisibles);
		}

		try {
			// creo la matriz con los prompt
			prompts = new PromptMenu[numIntentos][numeroPromptsTotal];

			// Recupero los prompts genericos de los intentos
			if (promptsMenu != null) {
				BeanIntento[] intentos = promptsMenu.getIntentos();

				PromptMenu[][] promptOpciones = null;
				if (numeroOpcionesVisibles > 0) {
					// PRIMERO PREPARO PROMPTS OPCIONES PARA LUEGO CONCATENAR
					promptOpciones = preparaPromptsOpciones(modo, listaOpcionesVisibles);
				}

				// INTENTOS
				// por cada intento, recupero prompts
				// i: se refiere a cada intento
				for (int i = 0; i < numIntentos; i++) {
					BeanIntento beanIntento;
					if (i >= intentos.length) {
						// he copiado ya todos los voz, me quedo con el ultimo
						beanIntento = intentos[intentos.length - 1];
					} else {
						beanIntento = intentos[i];
					}

					int contadorVariables = 0;

					// j: se refiere a cada prompt del intento i
					for (int j = 0; j < numeroPrompts; j++) {
						BeanPromptGenerico beanPrompt = beanIntento.getPrompts()[j];

						String promptValue = beanPrompt.getValue();
						if (beanPrompt.getUso() != null && !beanPrompt.getUso().equals("")) {
							// el uso del prompt esta relleno, voy a usar una
							// variable
							promptValue = variablesMenuArray[contadorVariables];
							// dejo listo para la siguiente variable
							contadorVariables = contadorVariables + 1;

						}

						// relleno la matriz
						prompts[i][j] = new PromptMenu();
						prompts[i][j].prompt = promptValue;
						prompts[i][j].modo = beanPrompt.getModo();
						prompts[i][j].uso = beanPrompt.getUso();

					}
					// aado las opciones visibles en el intento
					if (numeroOpcionesVisibles > 0 && promptOpciones != null) {
						// recorro cada opcion
						for (int op = 0; op < promptOpciones.length; op++) {
							// recorro las locuciones de cada opcion
							int lengthPrompOpc = promptOpciones[op].length;
							for (int loc = 0; loc < lengthPrompOpc; loc++) {
								prompts[i][numeroPrompts + loc + (op * lengthPrompOpc)] = new PromptMenu();
								prompts[i][numeroPrompts + loc + (op * lengthPrompOpc)].prompt = promptOpciones[op][loc].prompt;
								prompts[i][numeroPrompts + loc + (op * lengthPrompOpc)].modo = promptOpciones[op][loc].modo;
								prompts[i][numeroPrompts + loc + (op * lengthPrompOpc)].uso = promptOpciones[op][loc].uso;

							}
						}
					}
				}
			}

		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.error(e.getMessage(), "creaMatrizLocucionesOpciones", parametrosAdicionales, e);
			/** FIN EVENTO - ERROR **/

			throw e;
		}

		/** INICIO EVENTO - ACCION **/
		this.log.actionEvent("creaMatrizLocucionesOpciones", "OK", null);
		/** FIN EVENTO - ACCION **/

		return prompts;
	}

	/**
	 * Filtra las opciones que estan activas y visibles
	 */
	public ArrayList<BeanOpcionMenu> filtraOpcionesActivasYVisibles(BeanOpcionMenu[] opciones) throws Exception {

		// LOGGER.info("****** FILTRA OPCIONES: ");
		if (opciones == null) {
			// no hay opciones en el menu
			// LOGGER.info("No hay opciones. Menu estatico");
			return null;
		} else {
			ArrayList<BeanOpcionMenu> opcionesVisibles = null;
			try {
				// hay opciones, voy mirando si estan visibles
				opcionesVisibles = new ArrayList<BeanOpcionMenu>();
				for (BeanOpcionMenu opcion : opciones) {
					BeanConfigOpcMenu configOpcion = opcion.getConfiguracion();
					String visible = configOpcion.getOpcionVisible();
					String activa = configOpcion.getOpcionActiva();
					if (visible != null && visible.equalsIgnoreCase("ON") && activa != null && activa.equalsIgnoreCase("ON")) {
						// la opcion esta visible
						// LOGGER.info("Opcion activa y visible: " +
						// getKeyValue(configOpcion, "opcion_menu_desc"));
						opcionesVisibles.add(opcion);
					}
				}

			} catch (Exception e) {
				/** INICIO EVENTO - ACCION **/
				ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
				parametrosAdicionales.add(new ParamEvent("error", e.toString()));
				this.log.actionEvent("filtraOpcionesActivasYVisibles", "ERROR", parametrosAdicionales);
				/** FIN EVENTO - ACCION **/

				throw e;
			}
			return opcionesVisibles;
		}
	}

	/**
	 * Matriz de prompts para todas las opciones [opciones][prompts]
	 * 
	 * @param {@link String} modo
	 * @param {@link ArrayList<BeanOpcionMenu>} opciones
	 * @return {@link PromptMenu[][]}
	 */
	public PromptMenu[][] preparaPromptsOpciones(String modo, ArrayList<BeanOpcionMenu> opciones) throws Exception {

		if (modo == null) {
			return null;
		}
		PromptMenu[][] matrizPromptOpciones;
		try {

			if (modo.equalsIgnoreCase("ASR")) {
				matrizPromptOpciones = new PromptMenu[opciones.size()][3];
			} else {
				matrizPromptOpciones = new PromptMenu[opciones.size()][4];
			}
			// recorro las opciones
			for (int i = 0; i < opciones.size(); i++) {
				BeanOpcionMenu opc = opciones.get(i);

				// recojo modo reproduccion
				String modoRepro = opc.getConfiguracion().getModoReproduccion();

				// recojo prompts
				String promptIntro;
				String promptNormal;
				String promptOutro;

				if (modo != null && modo.equalsIgnoreCase("ASR")) {
					BeanConfigOpcAsrMenu configAsr = opc.getConfigAsr();

					promptIntro = configAsr.getPromptIntroAsr();
					promptNormal = configAsr.getPromptAsr();
					promptOutro = configAsr.getPromptOutroAsr();

				} else {
					BeanConfigOpcDtmfMenu configDtmf = opc.getConfigDtmf();

					promptIntro = configDtmf.getPromptIntroDtmf();
					promptNormal = configDtmf.getPromptDtmf();
					promptOutro = configDtmf.getPromptOutroDtmf();

				}

				// creo el array de prompts para la opcion actual
				if (modo.equalsIgnoreCase("ASR")) {
					matrizPromptOpciones[i] = new PromptMenu[3];
				} else {
					matrizPromptOpciones[i] = new PromptMenu[4];
				}
				matrizPromptOpciones[i][0] = new PromptMenu(promptIntro, modoRepro, "");
				matrizPromptOpciones[i][1] = new PromptMenu(promptNormal, modoRepro, "");
				matrizPromptOpciones[i][2] = new PromptMenu(promptOutro, modoRepro, "");
				String promptPosicion;
				if (modo.equalsIgnoreCase("DTMF")) {
					// es dtmf
					promptPosicion = opc.getConfiguracion().getPosicion();
					matrizPromptOpciones[i][3] = new PromptMenu(promptPosicion, modoRepro, "");
				}

			}
		} catch (Exception e) {
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("preparaPromptsOpciones", "ERROR", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			throw e;
		}

		return matrizPromptOpciones;

	}

	/**
	 * Devuelve las gramaticas de las opciones activas para el modo indicado
	 * 
	 * @param {@link String} modo
	 * @param {@link BeanOpcionMenu[]} opciones
	 * @return {@link BeanGramatica[]}
	 */
	public BeanGramatica[] obtenerGrammarOpciones(String modo, BeanOpcionMenu[] opciones) throws Exception {

		BeanGramatica[] grammars = null;
		try {

			ArrayList<BeanGramatica> arrayGrammars = new ArrayList<BeanGramatica>();

			this.log.comment("Obtener gramaticas de opciones para el modo: " + modo);

			int id = 0;
			for (BeanOpcionMenu opcion : opciones) {
				BeanConfigOpcMenu configOpcion = opcion.getConfiguracion();
				BeanConfigOpcAsrMenu configAsr = opcion.getConfigAsr();
				BeanConfigOpcDtmfMenu configDtmf = opcion.getConfigDtmf();

				String activa = configOpcion.getOpcionActiva();
				if (activa != null && activa.equalsIgnoreCase("ON")) {

					BeanGramatica grammar = new BeanGramatica();
					// [20160822] cambio las opciones de las gramaticas pueden
					// ir vacias y usar solo gramatica general
					// if (modo.equalsIgnoreCase("ASR")) {
					if (modo.equalsIgnoreCase("ASR") && configAsr.getGramaticaAsr() != null) {
						grammar = new BeanGramatica();
						grammar.setId(id);
						grammar.setTipo(configAsr.getGramaticaAsr().getTipo());
						grammar.setMinLength(configAsr.getGramaticaAsr().getMinLength());
						grammar.setMaxLength(configAsr.getGramaticaAsr().getMaxLength());
						grammar.setTonosActivos(configAsr.getGramaticaAsr().getTonosActivos());
						grammar.setValue(configAsr.getGramaticaAsr().getValue());

						arrayGrammars.add(grammar);
						// [20160822] cambio las opciones de las gramaticas
						// pueden ir vacias y usar solo gramatica general
						// } else
					} else if (configDtmf.getGramaticaDtmf() != null) {
						grammar = new BeanGramatica();
						grammar.setId(id);
						grammar.setTipo(configDtmf.getGramaticaDtmf().getTipo());
						grammar.setMinLength(configDtmf.getGramaticaDtmf().getMinLength());
						grammar.setMaxLength(configDtmf.getGramaticaDtmf().getMaxLength());
						grammar.setTonosActivos(configDtmf.getGramaticaDtmf().getTonosActivos());
						grammar.setValue(configDtmf.getGramaticaDtmf().getValue());

						arrayGrammars.add(grammar);

						// [20160822] cambio las opciones de las gramaticas
						// pueden ir vacias y usar solo gramatica general
					} else {
						arrayGrammars.add(null);
					}

				} else {
					// 07062016 - cambio para luego tener en cuenta los tonos
					// activos en gramaticas builtin
					// Si no hay gramatica en una de las opciones o no esta
					// activa, inserto null para que cuadren los dos arrays de
					// opciones y gramaticas
					// opcion[i] con su gramaticaOpciones[i]
					arrayGrammars.add(null);
				}
				id = id + 1;
			}
			if (arrayGrammars.size() != 0) {
				grammars = new BeanGramatica[arrayGrammars.size()];
				grammars = arrayGrammars.toArray(grammars);
			}

		} catch (Exception e) {
			/** INICIO EVENTO - ACCION **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			this.log.actionEvent("obtenerGrammarOpciones", "ERROR", parametrosAdicionales);
			/** FIN EVENTO - ACCION **/

			throw e;
		}
		return grammars;
	}
}
