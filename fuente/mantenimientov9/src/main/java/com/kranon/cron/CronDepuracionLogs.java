package com.kranon.cron;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.kranon.controlador.ControladorMantenimiento;
import com.kranon.util.UtilidadesMantenimiento;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;

@Component
@EnableAsync
@EnableScheduling
@Configuration
public class CronDepuracionLogs {

	private String rutaArchivoLock = null;
	private String nombreArchivoLock = null;			
	private String identificadorArchivo = "";

	@Scheduled(cron="${scheduling.job.cron.depurar}")
//	@Scheduled(fixedDelay = 20000)
	public void iniciarDepuracion() throws Exception{
		// creo el objeto de logger
		//System.out.println("*************************> INICIA CRON["+new java.util.Date()+"]");
		String idModulo = "CRON_DEPURACION_ARCHIVOS";
		String modulo = idModulo + "_INICIAR_DEPURACION";
		CommonLoggerProcess log = new CommonLoggerProcess(idModulo);		
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), idModulo);
		log.initModuleProcess(modulo, null);
		boolean procesoIniciado = false;
		try{
			if(procesoIniciado=inicializarProceso()){
				//Para que inicie el proceso.
				ControladorMantenimiento controlador = new ControladorMantenimiento(log);			
				controlador.iniciar("PROCESO_DEPURACION");
			}

		} catch (Exception e) {
			//** INICIO EVENTO - ERROR **//			
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", e.toString()));
			log.error(e.getMessage(), "BATCH_CUADROMANDO", parametrosAdicionales, e);
			//** FIN EVENTO - ERROR **//
		}finally{
			/** INICIO EVENTO - FIN MODULO **/		
			eliminarArchivo( rutaArchivoLock+nombreArchivoLock.replaceAll("<ID>", identificadorArchivo) );
			ArrayList<ParamEvent> parametrosSalida = new ArrayList<ParamEvent>();
			parametrosSalida.add( new ParamEvent("procesoIniciado",String.valueOf(procesoIniciado) ));
			log.endModuleProcess(idModulo, "OK", parametrosSalida, null);
			/** FIN EVENTO - FIN MODULO **/
		}
	}

	public String generadorNumerosAleatorios(int numeroIntento) throws Exception{
		String numeroAleatorio = "0";
		double numeroDouble = 0;

		//El Primer intento siempre regresar� el numero de hilo de ejcucion
		if( numeroIntento == 1){
			Thread.sleep(Thread.currentThread().getId());
			numeroAleatorio = String.valueOf(Thread.currentThread().getId() );
		}
		else{
			if( numeroIntento == 2){
				//Se genera un n�mero aleatorio 
				Random generador = new Random();
				numeroDouble = generador.nextDouble();

			}
			else if( numeroIntento == 3){
				//Se toman los nanosegundos del System convertidos a double
				Random generador = new Random();
				numeroDouble = (double)System.nanoTime() / generador.nextDouble();
			}
			else{
				//Se realiza una combinacion de los otros 3 metodos
				Random generador = new Random();
				numeroDouble = generador.nextDouble() + new Random(System.currentTimeMillis()).nextDouble() + (System.nanoTime() * generador.nextInt());

			}			
			numeroAleatorio = String.valueOf(numeroDouble*numeroDouble);
		}


		return numeroAleatorio;
	}

	public boolean crearArchivo(String archivoLock){
		// Se genera un archivo pid para que no existan dos procesos corriendo a la vez
		File f = null;
		BufferedWriter writer = null;
		boolean archivoCreado = false;


		try{
			f = new File(archivoLock);			
			writer = new BufferedWriter(new FileWriter(f));
			writer.close();
			archivoCreado = true;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return archivoCreado;

	}

	public boolean eliminarArchivo(String archivoLock){

		// Se genera un archivo pid para que no existan dos procesos corriendo a la vez
		File f = null;
		boolean archivoEliminado = false;


		try{
			f = new File(archivoLock);		
			if( f.isFile() && f.exists() ){
				f.delete();
				archivoEliminado = true;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return archivoEliminado;

	}

	public File[] obtenerArregloArchivos(String directorioArchivos,String filtro){
		File[] arregloArchivos = null;
		File f = null;
		if( directorioArchivos != null && !directorioArchivos.equals("") ){
			f = new File(directorioArchivos);
			if( f.isDirectory() ){
				arregloArchivos = f.listFiles( new FiltroArchivosLock(filtro) );
			}
		}
		
		return arregloArchivos;
	}
	
	public String obtenerIdentificadorMayor(File[] arregloArchivosLock){
		BigDecimal mayor = null;
		BigDecimal menor = null;
		int indiceMayor = 0;
		int indiceMenor = 0;
		int tamanioArreglo = 0;
		int valorComparacion = 0;
		String identificadorMayor = null;
		String identificadorMenor = null;
		String nombreAux = null;
		UtilidadesMantenimiento util = new UtilidadesMantenimiento();
		String idServicioProperty = util.leerParametro("SERVICIO_BATCH");
		
		
		if( arregloArchivosLock != null ){
			tamanioArreglo = arregloArchivosLock.length;
			indiceMenor = 1;
			
			//Se recorre el arreglo de archivos para comparar los nombres
			do{
				//Estraccion de la parte numerica del nombre del primer archivo lock
				nombreAux = arregloArchivosLock[indiceMayor].getName().replaceAll(idServicioProperty + "_","");
				identificadorMayor = nombreAux.replaceAll("_cron.depuracion.lock", "");
				mayor = new BigDecimal( identificadorMayor );
				
				//Estraccion de la parte numerica del nombre del segundo archivo lock
				nombreAux = arregloArchivosLock[indiceMenor].getName().replaceAll(idServicioProperty + "_","");
				identificadorMenor = nombreAux.replaceAll("_cron.depuracion.lock", "");
				menor = new BigDecimal( identificadorMenor );

				//Se compara el valor de cada uno
				valorComparacion = mayor.compareTo(menor);
				if( valorComparacion > 0 ){
					//Se recorre el indiceMenor
					if( indiceMayor > indiceMenor )
						indiceMenor = indiceMayor+1;
					else
						indiceMenor = indiceMenor+1;
				}
				else if( valorComparacion < 0 ){
					indiceMayor = indiceMenor;
					indiceMenor = indiceMayor+1;
				}
			}while( indiceMayor < tamanioArreglo && indiceMenor < tamanioArreglo );
			
			nombreAux = arregloArchivosLock[indiceMayor].getName().replaceAll(idServicioProperty + "_","");
			identificadorMayor = nombreAux.replaceAll("_cron.depuracion.lock", "");
			mayor = new BigDecimal( identificadorMayor );
		}
		return identificadorMayor;
	}
	
	public class FiltroArchivosLock implements FileFilter {

		private String filtro;
		
		public FiltroArchivosLock(){
			this(null);
		}
				
		
		public FiltroArchivosLock(String filtro){
			this.filtro = filtro;
		}
		
		@Override
		public boolean accept(File pathname) {
			boolean isAccepted = false;
			if( pathname!=null && !pathname.isDirectory() ){
				isAccepted = (filtro!=null && !filtro.equals("") ) ? (pathname.getName().contains(filtro))? true : false :true;
			}
			return isAccepted;
		}

		public String getArchivo() {
			return filtro;
		}

		public void setArchivo(String filtro) {
			this.filtro = filtro;
		}

	}
	
	  public boolean inicializarProceso() throws Exception{
	        boolean inicializaProceso = false;
	        boolean archivoCreado = false;
	        File[] arregloArchivosLock = null;
	        UtilidadesMantenimiento util = new UtilidadesMantenimiento();
	        
	        String identificadorMayor = "";        

	        rutaArchivoLock = util.leerParametro("RUTA_ARCHIVO_LOCK");
	        nombreArchivoLock = util.leerParametro("ARCHIVO_LOCK");
	        String idServicioProperty = util.leerParametro("SERVICIO_BATCH");
	        String varEntorno = util.leerParametro("VAR.ENTORNO");
	        
	        if(!varEntorno.equals("de")){//solo para los ambientes que tengan instancias clones
	            //1. Se genera el archivo lock
	            identificadorArchivo = System.getProperty("cloneId");//Para pruebas en ambiente local, comentar
//	            identificadorArchivo = "3";//Para pruebas en ambiente local, descomentar
	            archivoCreado = crearArchivo(rutaArchivoLock+nombreArchivoLock.replaceAll("<ID>", identificadorArchivo));
	            
	            if( archivoCreado ){
	                //Esperamos 10 segundos por el resto de los archivos
	                Thread.currentThread().sleep(util.MILISEGUNDOS_ESPERA);
	                
	                //2. Se obtiene la lista de archivos lock generados hasta el momento
	                arregloArchivosLock = obtenerArregloArchivos(rutaArchivoLock,nombreArchivoLock.replaceAll(idServicioProperty + "_<ID>_", "") );                
	                
	                if( arregloArchivosLock!=null && arregloArchivosLock.length > 1){
	                    
	                    //3. Obtener el identificador mayor
	                    identificadorMayor = obtenerIdentificadorMayor(arregloArchivosLock);
	                    
	                    //4. Se compara el identificador mayor con el generado
	                    if( identificadorMayor.equals(identificadorArchivo) ){
	                        //Si el identificador mayor es el del proceso se inicia con el procesamiento
	                        inicializaProceso = true;
	                    }
	                    else{
	                        //Si hay otro proceso con un ID mayor se elimina nuestro archivo y termina el procesamiento
	                        eliminarArchivo( rutaArchivoLock+nombreArchivoLock.replaceAll("<ID>", identificadorArchivo) );
	                        inicializaProceso = false;
	                    }                    
	                }                
	                else{
	                    //Se inicia el proceso
	                    inicializaProceso = true;
	                }
	            } //FIN archivoCreado
	        }else{
	        	inicializaProceso = true;
	        }
	        
	        return inicializaProceso;
	    }

}

