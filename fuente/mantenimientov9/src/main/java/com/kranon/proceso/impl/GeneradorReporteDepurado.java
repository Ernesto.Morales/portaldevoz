package com.kranon.proceso.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.kranon.proceso.GeneradorReporte;
import com.kranon.proceso.bean.depuracion.ArchivoDepuradoBean;
import com.kranon.proceso.bean.depuracion.ReporteDepuracionArchivosBean;
import com.kranon.util.UtilidadesMantenimiento;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;

public class GeneradorReporteDepurado extends GeneradorReporte{

	private String archivoReporte;
	private List<String[]> renglonesReporte;

	private static final String idModuloLog = "REPORTE_DEPURADOR_LOGS"; 


	public GeneradorReporteDepurado(){
		CommonLoggerProcess log = new CommonLoggerProcess("idModuloLog");
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), idModuloLog);
	}

	public GeneradorReporteDepurado(CommonLoggerProcess log){
		this.log = log;	
	}



	@Override
	protected void inicializar() {

		String modulo = idModuloLog + "_INICIALIZAR";
		String resultado = "KO";
		/** INICIO EVENTO - INICIO DE MoDULO **/		
		log.initModuleProcess(modulo, null);
		/** FIN EVENTO - INICIO DE MoDULO **/				

		//Se leen las variables necesarias para la generacion de reportes variables.properties
		UtilidadesMantenimiento utilidadesVariables = new UtilidadesMantenimiento();		
		archivoReporte = utilidadesVariables.leerParametro("ARCHIVO_REPORTE");

		//Se lee el archivo de resultado del proceso
		ReporteDepuracionArchivosBean reporte = (ReporteDepuracionArchivosBean)this.getResultadoProceso();
		try{		
			if( reporte != null ){
				//Se llena la tabla de valores
				renglonesReporte = new ArrayList<String[]>();
				String[] arregloRenglon = new String[2];

				arregloRenglon[0] = "Fecha y hora de inicio: ";
				arregloRenglon[1] = reporte.getFechaHoraInicio();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "Fecha y hora de fin: ";
				arregloRenglon[1] = reporte.getFechaHoraFin();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "N�mero de archivos depurados: ";
				arregloRenglon[1] = reporte.getNumeroArchivosDepuradosTotales();
				renglonesReporte.add(arregloRenglon);


				arregloRenglon = new String[2];
				arregloRenglon[0] = "N�mero de archivos depurados exitosamente: ";
				arregloRenglon[1] = reporte.getNumeroArchivosDepuradosExitosos();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "N�mero de archivos depurados fallidos: ";
				arregloRenglon[1] = reporte.getNumeroArchivosDepuradosFallidos();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "Espacio antes de la depuraci�n: ";
				arregloRenglon[1] = reporte.getEspacioAntesDepurado();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "Espacio despues de la depuraci�n: ";
				arregloRenglon[1] = reporte.getEspacioDepuesDepurado();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "Tipo de proceso: ";
				arregloRenglon[1] = reporte.getTipoProceso();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "�Se realiz� respaldo en TAR?: ";
				arregloRenglon[1] = reporte.getRespaldoTar();
				renglonesReporte.add(arregloRenglon);

				arregloRenglon = new String[2];
				arregloRenglon[0] = "Archivo de respaldo: ";
				arregloRenglon[1] = reporte.getArhivoTarRespaldo();
				renglonesReporte.add(arregloRenglon);


				List<ArchivoDepuradoBean> listaArchivoDepurado =  reporte.getListaArchivoDepurado();
				if( listaArchivoDepurado != null ){

					StringBuilder sb = new StringBuilder("");
					arregloRenglon = new String[2];
					arregloRenglon[0] = "Lista de archivos eliminados:  ";
					arregloRenglon[1] = "";
					renglonesReporte.add(arregloRenglon);

					for( ArchivoDepuradoBean archivoDepuradoBean : listaArchivoDepurado ){
						sb.append( archivoDepuradoBean.getNombreArchivo() );
						sb.append( " " );
						sb.append( archivoDepuradoBean.getExitoso() );
						sb.append( " " );
						sb.append( archivoDepuradoBean.getError() );
						sb.append( "\n" );
					}				

					arregloRenglon = new String[2];
					arregloRenglon[0] = sb.toString();
					arregloRenglon[1] = "";
					renglonesReporte.add(arregloRenglon);
				}


				//Se estable correctamente el nombre del archivo
				archivoReporte = archivoReporte.replaceAll("<TIPO_PROCESO>", reporte.getTipoProceso());
				archivoReporte = archivoReporte.replaceAll("<DDMMYYYY>", reporte.getFechaHoraInicio().replace(" ", "_").replaceAll(":", ".") );
			}
			resultado = "OK";
		}catch(Exception ex){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), modulo,	parametrosAdicionales, ex);
			/** FIN EVENTO - ERROR **/			
		}

		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> paramEvent = new ArrayList<ParamEvent>();
		paramEvent.add(new ParamEvent("reporte", reporte!=null? reporte.toString(): "null") );
		paramEvent.add(new ParamEvent("renglonesReporte", renglonesReporte!=null? String.valueOf(renglonesReporte.size()) : "null") );
		paramEvent.add(new ParamEvent("archivoReporte", renglonesReporte!=null? archivoReporte : "null") );		
		this.log.endModuleProcess(modulo, resultado, paramEvent, null);
		/** FIN EVENTO - FIN MODULO **/
	}

	@Override
	protected void procesar() {

		String modulo = idModuloLog + "_INICIALIZAR";
		String resultado = "KO";

		/** INICIO EVENTO - INICIO DE MoDULO **/		
		log.initModuleProcess(modulo, null);
		/** FIN EVENTO - INICIO DE MoDULO **/				

		//Se valida el resultado del proceso
		File fileReporte = null;
		BufferedWriter bw = null;

		try{
			if( renglonesReporte != null ){
				fileReporte = new File(archivoReporte);
				bw = new BufferedWriter( new FileWriter(fileReporte) );

				for(String[] string: renglonesReporte){
					bw.write( string[0] );
					bw.write( string[1] );
					bw.newLine();
				}
			}
			resultado = "OK";
		}catch(Exception ex){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), modulo,	parametrosAdicionales, ex);
			/** FIN EVENTO - ERROR **/			
		}finally{
			try{
				if(bw!=null)
					bw.close();
			}catch(Exception ex){}
		}

		/** INICIO EVENTO - FIN MODULO **/
		this.log.endModuleProcess(modulo, resultado, null, null);
		/** FIN EVENTO - FIN MODULO **/

	}


	@Override
	protected void finalizar(){
		//Se limpian todas las referencias de variables de instancia
		archivoReporte = null;
		renglonesReporte = null;

		System.gc();
	}

	public String getArchivoReporte() {
		return archivoReporte;
	}

	public void setArchivoReporte(String archivoReporte) {
		this.archivoReporte = archivoReporte;
	}

}
