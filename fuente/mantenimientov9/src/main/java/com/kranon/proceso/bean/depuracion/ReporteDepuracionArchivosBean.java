package com.kranon.proceso.bean.depuracion;

import java.util.List;

import com.kranon.proceso.bean.ResultadoProcesoBean;

public class ReporteDepuracionArchivosBean implements ResultadoProcesoBean{

	private String fechaHoraInicio;
	private String fechaHoraFin;
	private String numeroArchivosDepuradosTotales;
	private String numeroArchivosDepuradosExitosos;
	private String numeroArchivosDepuradosFallidos;
	private String espacioAntesDepurado;
	private String espacioDepuesDepurado;
	private String tipoProceso;
	private String arhivoTarRespaldo;
	private String respaldoTar;
	private List<ArchivoDepuradoBean> listaArchivoDepurado;
	
	public String getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(String fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}
	public String getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(String fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}
	public String getNumeroArchivosDepuradosTotales() {
		return numeroArchivosDepuradosTotales;
	}
	public void setNumeroArchivosDepuradosTotales(
			String numeroArchivosDepuradosTotales) {
		this.numeroArchivosDepuradosTotales = numeroArchivosDepuradosTotales;
	}
	public String getNumeroArchivosDepuradosExitosos() {
		return numeroArchivosDepuradosExitosos;
	}
	public void setNumeroArchivosDepuradosExitosos(
			String numeroArchivosDepuradosExitosos) {
		this.numeroArchivosDepuradosExitosos = numeroArchivosDepuradosExitosos;
	}
	public String getNumeroArchivosDepuradosFallidos() {
		return numeroArchivosDepuradosFallidos;
	}
	public void setNumeroArchivosDepuradosFallidos(
			String numeroArchivosDepuradosFallidos) {
		this.numeroArchivosDepuradosFallidos = numeroArchivosDepuradosFallidos;
	}
	public String getEspacioAntesDepurado() {
		return espacioAntesDepurado;
	}
	public void setEspacioAntesDepurado(String espacioAntesDepurado) {
		this.espacioAntesDepurado = espacioAntesDepurado;
	}
	public String getEspacioDepuesDepurado() {
		return espacioDepuesDepurado;
	}
	public void setEspacioDepuesDepurado(String espacioDepuesDepurado) {
		this.espacioDepuesDepurado = espacioDepuesDepurado;
	}
	public String getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public String getArhivoTarRespaldo() {
		return arhivoTarRespaldo;
	}
	public void setArhivoTarRespaldo(String arhivoTarRespaldo) {
		this.arhivoTarRespaldo = arhivoTarRespaldo;
	}
	public String getRespaldoTar() {
		return respaldoTar;
	}
	public void setRespaldoTar(String respaldoTar) {
		this.respaldoTar = respaldoTar;
	}
	public List<ArchivoDepuradoBean> getListaArchivoDepurado() {
		return listaArchivoDepurado;
	}
	public void setListaArchivoDepurado(List<ArchivoDepuradoBean> listaArchivoDepurado) {
		this.listaArchivoDepurado = listaArchivoDepurado;
	}
		
	@Override
	public String toString(){
		return String.format(" ReporteDepuracionArchivosBean [fechaHoraInicio=%s, fechaHoraFin=%s, numeroArchivosDepuradosTotales=%s, numeroArchivosDepuradosExitosos=%s, numeroArchivosDepuradosFallidos=%s, espacioAntesDepurado=%s, espacioDepuesDepurado=%s, tipoProceso=%s, arhivoTarRespaldo=%s, respaldoTar=%s, listaArchivoDepurado=%s ]", fechaHoraInicio, fechaHoraFin, numeroArchivosDepuradosTotales, numeroArchivosDepuradosExitosos, numeroArchivosDepuradosFallidos, espacioAntesDepurado, espacioDepuesDepurado, tipoProceso, arhivoTarRespaldo, respaldoTar, listaArchivoDepurado!=null?String.valueOf(listaArchivoDepurado.size()):"null");
	}
}
