package com.kranon.proceso;

import com.kranon.proceso.Proceso;
import com.kranon.proceso.bean.ResultadoProcesoBean;

public abstract class GeneradorReporte extends Proceso{
	
	private ResultadoProcesoBean resultadoProceso;

	@Override
	protected abstract void inicializar();

	@Override
	protected abstract void procesar();

	@Override
	protected abstract void finalizar();

	@Override
	protected void generarReporte() {}

	public ResultadoProcesoBean getResultadoProceso() {
		return resultadoProceso;
	}

	public void setResultadoProceso(ResultadoProcesoBean resultadoProceso) {
		this.resultadoProceso = resultadoProceso;
	}

	
}
