package com.kranon.proceso;

import com.kranon.logger.proc.CommonLoggerProcess;

public abstract class Proceso {

	protected String inicio;
	protected String fin;
	protected String resultado;
	protected String tipoProceso;
	
	protected CommonLoggerProcess log;
	
	
	public void ejecutar(){
		
		inicializar();
		procesar();
		generarReporte();
		finalizar();
		
	}
	
	protected abstract void inicializar();
	protected abstract void procesar();
	protected abstract void generarReporte();
	protected abstract void finalizar();

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public CommonLoggerProcess getLog() {
		return log;
	}

	public void setLog(CommonLoggerProcess log) {
		this.log = log;
	}
	
}
