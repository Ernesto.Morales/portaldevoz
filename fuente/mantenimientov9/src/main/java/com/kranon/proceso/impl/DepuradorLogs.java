package com.kranon.proceso.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.kranon.controlador.InstanciaFactory;
import com.kranon.proceso.GeneradorReporte;
import com.kranon.proceso.Proceso;
import com.kranon.proceso.bean.ResultadoProcesoBean;
import com.kranon.proceso.bean.depuracion.ArchivoDepuradoBean;
import com.kranon.proceso.bean.depuracion.ReporteDepuracionArchivosBean;
import com.kranon.util.UtilidadesMantenimiento;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;

public class DepuradorLogs extends Proceso{

	private String pathDepurarLogs;
	private String directoriosDepurar;
	private String porcentajeMaxFilesytem;
	private String respaldoTar;
	private String rutaRespaldoTar;
	private String tiempoMaxArchivos;
	private String tipoProcesoDepuracion;

	private Date fechaHoy;
	long espacioDisponible;

	private UtilidadesMantenimiento utilidadesVariables;
	List<ArchivoDepuradoBean> listaEstadoArchivosDepurados;


	private static final String idModuloLog = "DEPURADOR_LOGS"; 


	public DepuradorLogs(){
		CommonLoggerProcess log = new CommonLoggerProcess("idModuloLog");
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), idModuloLog);
	}

	public DepuradorLogs(CommonLoggerProcess log){
		this.log = log;	
	}


	@Override
	protected void inicializar() {


		String modulo = idModuloLog + "_INICIALIZAR";
		String resultado = "KO";
		/** INICIO EVENTO - INICIO DE MoDULO **/		
		log.initModuleProcess(modulo, null);
		/** FIN EVENTO - INICIO DE MoDULO **/				
		try{
			//Se leen las variables necesarias para el proceso de depuracion del archivo variables.properties
			utilidadesVariables = new UtilidadesMantenimiento();

			pathDepurarLogs = utilidadesVariables.leerParametro("PATH_DEPURAR_LOGS");
			directoriosDepurar = utilidadesVariables.leerParametro("DIRECTORIOS_DEPURAR");
			porcentajeMaxFilesytem = utilidadesVariables.leerParametro("PORCENTAJE_MAX_FILESYTEM");
			respaldoTar = utilidadesVariables.leerParametro("RESPALDO_TAR");
			rutaRespaldoTar = utilidadesVariables.leerParametro("RUTA_RESPALDO_TAR");
			tiempoMaxArchivos = utilidadesVariables.leerParametro("TIEMPO_MAX_ARCHIVOS");
			tipoProcesoDepuracion = utilidadesVariables.leerParametro("TIPO_PROCESO_DEPURACION");
			
			//Se establece fecha y hora de inicio de proceso en formato DDMMYYYY HH24:MI:SS
			SimpleDateFormat formateador = new SimpleDateFormat("ddMMyyyy HH:mm:ss:SSS", new Locale("es", "MX") );
			fechaHoy = new Date();
			inicio = formateador.format( fechaHoy );
			resultado = "OK";
		}catch(Exception ex){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), modulo,	parametrosAdicionales, ex);
			/** FIN EVENTO - ERROR **/			
		}

		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> paramEvent = new ArrayList<ParamEvent>();
		paramEvent.add(new ParamEvent("pathDepurarLogs", pathDepurarLogs ) );		
		paramEvent.add(new ParamEvent("directoriosDepurar", directoriosDepurar ) );
		paramEvent.add(new ParamEvent("porcentajeMaxFilesytem", porcentajeMaxFilesytem ) );
		paramEvent.add(new ParamEvent("respaldoTar", respaldoTar ) );
		paramEvent.add(new ParamEvent("rutaRespaldoTar", rutaRespaldoTar ) );
		paramEvent.add(new ParamEvent("tiempoMaxArchivos", tiempoMaxArchivos ) );
		paramEvent.add(new ParamEvent("tipoProcesoDepuracion", tipoProcesoDepuracion ) );
		paramEvent.add(new ParamEvent("fechaHoy", fechaHoy.toString() ) );
		paramEvent.add(new ParamEvent("inicio", inicio ) );
		this.log.endModuleProcess(modulo, resultado, paramEvent, null);
		/** FIN EVENTO - FIN MODULO **/


	}

	@Override
	protected void procesar() {		

		String modulo = idModuloLog + "_PROCESAR";
		String resultado = "KO";
		/** INICIO EVENTO - INICIO DE MoDULO **/		
		log.initModuleProcess(modulo, null);
		/** FIN EVENTO - INICIO DE MoDULO **/				

		//Verificar % de uso de sistema
		espacioDisponible = 0;
		long espacioTotal = 0;
		float porcentajeUso = 0;

		try{
			espacioDisponible = utilidadesVariables.getEspacioDisponibleDisco(pathDepurarLogs);
			espacioTotal = utilidadesVariables.getTamanioDisco(pathDepurarLogs);
			porcentajeUso = (espacioTotal - espacioDisponible) * 100 / espacioTotal;
			//Valida si el porcentaje es igual o mayor al establecido por porcentajeMaxFilesytem
			if ( porcentajeUso >= Float.parseFloat(porcentajeMaxFilesytem) ){
				//Se verifican las carpetas a ser analizadas
				String[] carpetas = directoriosDepurar.split(",");
				List<File> listaArchivosDepurar = new ArrayList<File>();
				for(String carpeta : carpetas ){
					listaArchivosDepurar.addAll( obtenerListaArchivosDepurar(pathDepurarLogs + "/" + carpeta) );
				}

				if( listaArchivosDepurar.size() > 0 ){
					//Si la lista tiene elementos se procede a realizar el borrado

					//Se verifica si se necesita el respaldo en tar
					if( respaldoTar.equalsIgnoreCase("SI") ){
						//Proceso de generacion de TAR
					}

					listaEstadoArchivosDepurados = new ArrayList<ArchivoDepuradoBean>(); 
					boolean archivoEliminado = false;
					String mensajeError = "";
					for(File f: listaArchivosDepurar ){
						try{
							archivoEliminado = f.delete();						
							mensajeError = !archivoEliminado ? "Error desconocido" : "";
						}catch(SecurityException  se){
							mensajeError = se.getMessage();
						}
						listaEstadoArchivosDepurados.add( new ArchivoDepuradoBean(f.getAbsolutePath(),archivoEliminado ? "OK" : "KO", mensajeError ) );
						mensajeError = "";					
					}
				}

			}

			SimpleDateFormat formateador = new SimpleDateFormat("ddMMyyyy HH:mm:ss:SSS", new Locale("es", "MX") );		
			fin = formateador.format( new Date() );
			resultado = "OK";
		}catch(Exception ex){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), modulo,	parametrosAdicionales, ex);
			/** FIN EVENTO - ERROR **/			
		}

		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> paramEvent = new ArrayList<ParamEvent>();
		paramEvent.add(new ParamEvent("espacioDisponible", String.valueOf(espacioDisponible) ) );		
		paramEvent.add(new ParamEvent("espacioTotal", String.valueOf(espacioTotal) ) );
		paramEvent.add(new ParamEvent("porcentajeUso", String.valueOf(porcentajeUso) ) );
		paramEvent.add(new ParamEvent("listaEstadoArchivosDepurados", listaEstadoArchivosDepurados != null ? String.valueOf(listaEstadoArchivosDepurados.size()) : "0" ) );
		paramEvent.add(new ParamEvent("fin", fin ) );
		this.log.endModuleProcess(modulo, resultado, paramEvent, null);
		/** FIN EVENTO - FIN MODULO **/

	}

	@Override
	protected void generarReporte() {
		String modulo = idModuloLog + "_REPORTE";
		String resultado = "KO";
		/** INICIO EVENTO - INICIO DE MoDULO **/		
		log.initModuleProcess(modulo, null);
		/** FIN EVENTO - INICIO DE MoDULO **/				

		//Se obtiene el generador de reporte
		Proceso generadorReporte = InstanciaFactory.getProceso("PROCESO_REPORTE_DEPURACION",log);
		ResultadoProcesoBean resultadoProceso = null;
		try{
			if( generadorReporte != null  ){

				//Se llenan los beans necesarios para el reporte
				resultadoProceso = InstanciaFactory.getResultadoProcesoBean("RESULTADO_DEPURACION");

				((ReporteDepuracionArchivosBean)resultadoProceso).setArhivoTarRespaldo(rutaRespaldoTar);
				((ReporteDepuracionArchivosBean)resultadoProceso).setEspacioAntesDepurado(String.valueOf(espacioDisponible) + " " + UtilidadesMantenimiento.NOMENCLATURA_MEGABYTES );
				((ReporteDepuracionArchivosBean)resultadoProceso).setEspacioDepuesDepurado( String.valueOf(utilidadesVariables.getEspacioDisponibleDisco(pathDepurarLogs) )  + " " + UtilidadesMantenimiento.NOMENCLATURA_MEGABYTES  );
				((ReporteDepuracionArchivosBean)resultadoProceso).setFechaHoraFin(fin);
				((ReporteDepuracionArchivosBean)resultadoProceso).setFechaHoraInicio(inicio);
				((ReporteDepuracionArchivosBean)resultadoProceso).setRespaldoTar(respaldoTar);
				((ReporteDepuracionArchivosBean)resultadoProceso).setTipoProceso(tipoProcesoDepuracion);			

				((ReporteDepuracionArchivosBean)resultadoProceso).setListaArchivoDepurado(listaEstadoArchivosDepurados);
				int contadorExitosos = 0;
				int contadorErrores = 0;
				int contadorTotales = 0;

				if( listaEstadoArchivosDepurados != null ){
					for(ArchivoDepuradoBean archivoDepurado : listaEstadoArchivosDepurados ){
						contadorExitosos += archivoDepurado.getExitoso().equals("OK") ? 1 : 0;
						contadorErrores += archivoDepurado.getExitoso().equals("KO") ? 1 : 0;
					}

					contadorTotales = listaEstadoArchivosDepurados.size();					
				}


				((ReporteDepuracionArchivosBean)resultadoProceso).setNumeroArchivosDepuradosExitosos( String.valueOf(contadorExitosos) );
				((ReporteDepuracionArchivosBean)resultadoProceso).setNumeroArchivosDepuradosFallidos( String.valueOf(contadorErrores) );
				((ReporteDepuracionArchivosBean)resultadoProceso).setNumeroArchivosDepuradosTotales( String.valueOf(contadorTotales ) );


				((GeneradorReporte)generadorReporte).setResultadoProceso( resultadoProceso );
				resultado = (contadorErrores > 0 ) ? ( contadorExitosos > 0 ) ? "Proceso terminado OK con algunos errores" : "Proceso terminado con error" : "Proceso terminado OK";
				this.setResultado(resultado);
				//Se invoca el proceso de generacion de reporte
				generadorReporte.ejecutar();
				resultado = "OK";
			}
		} catch(Exception ex){
			/** INICIO EVENTO - ERROR **/
			ArrayList<ParamEvent> parametrosAdicionales = new ArrayList<ParamEvent>();
			parametrosAdicionales.add(new ParamEvent("error", ex.toString()));
			this.log.error(ex.getMessage(), modulo,	parametrosAdicionales, ex);
			/** FIN EVENTO - ERROR **/			
		}

		/** INICIO EVENTO - FIN MODULO **/
		ArrayList<ParamEvent> paramEvent = new ArrayList<ParamEvent>();
		paramEvent.add(new ParamEvent("resultadoProceso", resultadoProceso.toString() ) );		
		this.log.endModuleProcess(modulo, resultado, paramEvent, null);
		/** FIN EVENTO - FIN MODULO **/

	}

	@Override
	protected void finalizar(){
		//Se limpian todas las referencias de variables de instancia
		pathDepurarLogs=null;
		directoriosDepurar=null;
		porcentajeMaxFilesytem=null;
		respaldoTar=null;
		rutaRespaldoTar=null;
		tiempoMaxArchivos=null;
		tipoProcesoDepuracion=null;		
		fechaHoy=null;
		utilidadesVariables=null;
		listaEstadoArchivosDepurados=null;

		System.gc();
	}

	private List<File> obtenerListaArchivosDepurar(String rutaBase){

		List<File> listaArchivosDepurar = new ArrayList<File>();
		File fileBase = new File(rutaBase);
		File[] arregloArchivos = null;

		//Se iteran los archivos del directorio
		arregloArchivos = fileBase.listFiles(); 
		for(File f:arregloArchivos){
			if( f.isFile() ){				
				//Si es archivo se verifica si la fecha de modificacion es igual o mayor al establecido como maximo (tiempoMaxArchivos)
				if ( (fechaHoy.getTime() - f.lastModified()) >= UtilidadesMantenimiento.MILISEGUNDOS_POR_DIA * Integer.parseInt(tiempoMaxArchivos) ){
					//El archivo se encuentra en el rango de ser eliminado, se a�ade a la lista
					listaArchivosDepurar.add(f);
				}
			}
			else{
				//Si no es archivo se realiza una llamada recursiva
				listaArchivosDepurar.addAll( obtenerListaArchivosDepurar(f.getAbsolutePath()) );
			}			
		}

		return listaArchivosDepurar;
	}

}
