package com.kranon.proceso.bean.depuracion;

public class ArchivoDepuradoBean {

	private String nombreArchivo;
	private String exitoso;
	private String error;
	
	public ArchivoDepuradoBean(){
		this(null,null,null);
	}
	
	public ArchivoDepuradoBean(String nombreArchivo, String exitoso, String error){
		this.nombreArchivo = nombreArchivo;
		this.exitoso = exitoso;
		this.error = error;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getExitoso() {
		return exitoso;
	}
	public void setExitoso(String exitoso) {
		this.exitoso = exitoso;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@Override
	public String toString(){
		return String.format("EstadoDepuradoBean [nombreArchivo=%s, exitoso=%s, error=%s]", nombreArchivo,exitoso,error);
	}
}
