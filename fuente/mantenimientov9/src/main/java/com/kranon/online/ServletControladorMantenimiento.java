package com.kranon.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kranon.controlador.ControladorMantenimiento;
import com.kranon.logger.proc.CommonLoggerProcess;

/**
 * Servlet implementation class ServletControladorMantenimiento
 */
public class ServletControladorMantenimiento extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String idModuloLog = "CONTROLADOR_MANTENIMIENTO_ONLINE";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletControladorMantenimiento() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoggerProcess log = new CommonLoggerProcess(idModuloLog);
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), idModuloLog);

		ControladorMantenimiento controlador = new ControladorMantenimiento(log);
		
		controlador.iniciar("PROCESO_DEPURACION");

	}

}
