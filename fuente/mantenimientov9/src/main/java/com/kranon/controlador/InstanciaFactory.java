package com.kranon.controlador;

import com.kranon.proceso.Proceso;
import com.kranon.proceso.bean.ResultadoProcesoBean;
import com.kranon.util.UtilidadesMantenimiento;
import com.kranon.logger.proc.CommonLoggerProcess;

public class InstanciaFactory {
	
	public static Proceso getProceso(String tipoProceso){
		return getProceso(tipoProceso,null);
	}
	
	public static Proceso getProceso(String tipoProceso,CommonLoggerProcess log){
		Proceso instancia = null;
		UtilidadesMantenimiento utilidadesVariables = new UtilidadesMantenimiento();
		String nombreCanonico = null;
		
		if( tipoProceso != null && !tipoProceso.equals("") ){
			nombreCanonico = utilidadesVariables.leerParametro(tipoProceso);			
			try{
				instancia = nombreCanonico!=null && !nombreCanonico.equals("") ? (Proceso)Class.forName(nombreCanonico).newInstance():instancia;
			}catch(Exception ex){
					ex.printStackTrace();
			}
			
			if( instancia!=null ) {
				instancia.setTipoProceso(tipoProceso);
				instancia.setLog(log);
			}
		}		
		return instancia;
		
	}
	
	
	public static ResultadoProcesoBean getResultadoProcesoBean(String tipoResultado){
		ResultadoProcesoBean instancia = null;
		UtilidadesMantenimiento utilidadesVariables = new UtilidadesMantenimiento();
		String nombreCanonico = null;
		
		if( tipoResultado != null && !tipoResultado.equals("") ){
			nombreCanonico = utilidadesVariables.leerParametro(tipoResultado);			
			try{
				instancia = nombreCanonico!=null && !nombreCanonico.equals("") ? (ResultadoProcesoBean)Class.forName(nombreCanonico).newInstance():instancia;
				
			}catch(Exception ex){
					ex.printStackTrace();
			}			
		}		
		return instancia;
	}

}
