package com.kranon.controlador;

import java.util.ArrayList;

import com.kranon.proceso.Proceso;
import com.kranon.logger.event.ParamEvent;
import com.kranon.logger.proc.CommonLoggerProcess;

public class ControladorMantenimiento {

	private CommonLoggerProcess log;
	private static final String idModuloLog = "CONTORLADOR_MANTENIMIENTO"; 
	
	
	public ControladorMantenimiento(){
		CommonLoggerProcess log = new CommonLoggerProcess(idModuloLog);
		log.inicializar("Proc-"+String.valueOf( Thread.currentThread().getId() ), idModuloLog);
	}

	public ControladorMantenimiento(CommonLoggerProcess log){
		this.log = log;	
	}

	
	public void iniciar(String tipoProceso){
		String modulo = idModuloLog + "_INICIAR";
		/** INICIO EVENTO - INICIO DE MoDULO **/		
		ArrayList<ParamEvent> paramEvent = new ArrayList<ParamEvent>();
		paramEvent.add( new ParamEvent("tipoProceso",tipoProceso) );
		log.initModuleProcess(modulo, paramEvent);
		/** FIN EVENTO - INICIO DE MoDULO **/				

		Proceso proceso = InstanciaFactory.getProceso(tipoProceso,log);
		String inicio = null;
		String fin = null;
		String resultado = null;
		if( proceso != null ){
			//Se ejecuta el proceso indicado
			proceso.ejecutar();
			
			inicio = proceso.getInicio();
			fin = proceso.getFin();
			resultado = proceso.getResultado();
			tipoProceso = proceso.getTipoProceso();
		}
		
		/** INICIO EVENTO - FIN MODULO **/
		paramEvent.clear();
		paramEvent.add(new ParamEvent("inicio", inicio ) );		
		paramEvent.add(new ParamEvent("fin", fin ) );
		paramEvent.add(new ParamEvent("resultado", resultado ) );
		paramEvent.add(new ParamEvent("tipoProceso", tipoProceso ) );
		this.log.endModuleProcess(modulo, resultado, paramEvent, null);
		/** FIN EVENTO - FIN MODULO **/
	}
	
}

