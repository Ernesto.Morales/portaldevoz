package com.kranon.util;

import java.io.File;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;

public class UtilidadesMantenimiento {
	
	public static final long FACTOR_MEGABYTES = 1000000;
	public static final String NOMENCLATURA_MEGABYTES = "MB";
	public static final long MILISEGUNDOS_POR_DIA = 86400000;
	public static final long MILISEGUNDOS_ESPERA = 10000;
	
	//public CommonLoggerProcess log;

	public UtilidadesMantenimiento(){
		//this(null);
	}
	/*
	public UtilidadesMantenimiento(CommonLoggerProcess log) {
		this.log = log;
	}
*/

	/**
	 * Lee el dato de configuracion del archivo variables.properties
	 * 
	 * @param {@link String} key
	 * @return {@link String}
	 */
	public String leerParametro(String key) {
		String value = null;
		try {

			value = ArqSpringContext.getPropiedad(key);
		} catch (Exception e) {
			/** INICIO EVENTO - ERROR **/
			//this.log.error(e.getMessage(), "LEER PARAMETRO " + key , null, e);
			e.printStackTrace();
			/** FIN EVENTO - ERROR **/
		}
		return value;
	}

	
	public long getEspacioDisponibleDisco(String particion){
		
		long espacioDisponible = 0;
		File fileSystem = new File(particion);
		
		
		if( fileSystem != null && fileSystem.exists() && fileSystem.isDirectory()  ){
			espacioDisponible = fileSystem.getUsableSpace()/FACTOR_MEGABYTES;
		}
				
		return espacioDisponible;

	}
	
	public long getTamanioDisco(String particion){
		
		long tamanioDisco = 0;
		File fileSystem = new File(particion);
		
		
		if( fileSystem != null && fileSystem.exists() && fileSystem.isDirectory()  ){
			tamanioDisco = fileSystem.getTotalSpace()/FACTOR_MEGABYTES;
		}
		
		return tamanioDisco;
		
	}
}
